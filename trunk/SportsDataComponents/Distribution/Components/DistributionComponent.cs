﻿using System;
using System.ComponentModel;
using System.ServiceModel;
using System.Threading;
using log4net;
using NTB.SportsData.Classes.Enums;
using NTB.SportsData.Components.Components;
using NTB.SportsData.Components.Nff;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;
// Logging

// Other Sports Data libraries


// Adding support for methods in the Utilities namespace

namespace NTB.SportsData.Components.Distribution.Components
{
    public partial class DistributionComponent : Component, IBaseSportsDataInterfaces
    {
         #region Logging
        /// <summary>
        /// Static logger
        /// </summary>
        static readonly ILog Logger = LogManager.GetLogger(typeof(NffGathererComponent));

        /// <summary>
        /// Initializes the <see>
        ///                     <cref>GathererComponent</cref>
        ///                 </see>
        ///     class.
        /// </summary>
        static DistributionComponent()
        {
            //Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
                log4net.Config.BasicConfigurator.Configure();
        }

        #endregion


        #region Common class variables

        /// <summary>
        /// The Configured job name for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="InstanceName"/></remarks>
        protected String Name;

        /// <summary>
        /// Flag to indicate sucessful configure. Instance will not start polling nor handle messages if not properly Configured
        /// </summary>
        /// <remarks>Holds the Configured status of the instance. <c>True</c> means successfully Configured</remarks>
        protected Boolean Configured = false;

        /// <summary>
        /// Error-Retry flag
        /// </summary>
        /// <remarks>The flag is being set if a network or EWS error is encountered. Current operations are being aborted for later retry.</remarks>
        protected Boolean ErrorRetry = false;

        /// <summary>
        /// Send Email-notifications when a new messages is processed
        /// </summary>
        /// <remarks>Set to an email address. Multiple adresses are supported, separate with <c>;</c></remarks>
        protected String EmailNotification;

        /// <summary>
        /// Subject for email notifications
        /// </summary>
        /// <remarks>The subject is built during processing and used when sending the email when processing completes</remarks>
        protected String EmailSubject;

        /// <summary>
        /// Body for email notifications
        /// </summary>
        /// <remarks>The body is built during processing and used when sending the email when processing completes</remarks>
        protected String EmailBody;

        protected ScheduleType ScheduleType;

        #endregion

        #region Polling settings

        /// <summary>
        /// The polling style for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="PollStyle"/></remarks>
        protected PollStyle pollStyle;

        /// <summary>
        /// schedulerFactory to be used to create scheduled tasks
        /// </summary>
        
        InstanceContext instanceContext;

        /// <summary>
        /// The Configured interval for this instance, used for Continous polling
        /// </summary>
        /// <remarks>
        ///   <para>Indicates the interval time in seconds for <c>Continous</c>polling. 60 means that the job runs every minute.</para>
        ///   <para>Default value: <c>60</c></para>
        /// </remarks>
        protected int interval = 60;

        #endregion
        public DistributionComponent()
        {
            this.InitializeComponent();
        }

        public DistributionComponent(IContainer container)
        {
            container.Add(this);

            this.InitializeComponent();
        }

        public void Configure(System.Xml.XmlNode configNode)
        {
            Logger.Debug("Node: " + configNode.Name);
            Logger.Debug("Name attribut: " + configNode.Attributes.GetNamedItem("Name"));

            // Some values that we need.

            //Basic configuration sanity check
            if (configNode == null || configNode.Name != "DistributionComponent" || configNode.Attributes.GetNamedItem("Name") == null)
            {
                throw new ArgumentException("The XML configuration node passed is invalid", "configNode");
            }

            if (Thread.CurrentThread.Name == null)
            {
                Thread.CurrentThread.Name = configNode.Attributes.GetNamedItem("Name").ToString();
            }


            #region Basic config

            // Getting the name of this Component instance
            try
            {
                this.Name = configNode.Attributes["Name"].Value;
                this.enabled = Convert.ToBoolean(configNode.Attributes["Enabled"].Value);
                log4net.ThreadContext.Stacks["NDC"].Push(this.InstanceName);

                // This value is used to tell that we shall insert into database... I just wonder if we shall do it
                // on the schedule-level... Well, we try here first
                // TODO: Consider using schedule-level to decide if we are to insert into database or not


            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to configure this job instance!", ex);
            }

            //Basic operation
            try
            {
                this.operationMode = (OperationMode)Enum.Parse(typeof(OperationMode), configNode.Attributes["OperationMode"].Value, true);
            }
            catch (Exception ex)
            {
                log4net.ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing OperationMode values in XML configuration node", ex);
            }

            try
            {
                this.pollStyle = (PollStyle)Enum.Parse(typeof(PollStyle), configNode.Attributes["PollStyle"].Value, true);
            }
            catch (Exception ex)
            {
                log4net.ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing PollStyle values in XML configuration node", ex);
            }


            try
            {
                this.ScheduleType = (ScheduleType)Enum.Parse(typeof(ScheduleType), configNode.Attributes["ScheduleType"].Value, true); // ScheduleType = (ScheduleT
            }
            catch (Exception ex)
            {
                log4net.ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing ScheduleType values in XML configuration node", ex);
            }

            #endregion
        }

        public void Start()
        {
            throw new NotImplementedException();
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the Enabled status of the instance.
        /// </summary>
        /// <value>The Enabled status.</value>
        /// <remarks><c>True</c> if the job instance is Enabled.</remarks>
        public bool Enabled
        {
            get { return this.enabled; }
        }

        /// <summary>
        /// Gets the name of the instance.
        /// </summary>
        /// <value>The name of the instance.</value>
        /// <remarks>The Configured instance job name.</remarks>
        public string InstanceName
        {
            get { return this.Name; }
        }

        /// <summary>
        /// Gets the operation mode. 
        /// </summary>
        /// <value>The operation mode.</value>
        /// <remarks><c>Gatherer</c> is the only valid mode, its hard coded for this component.</remarks>
        
        public OperationMode OperationMode
        {
            get { return OperationMode.Gatherer; }
        }


        /// <summary>
        /// Gets the poll style.
        /// </summary>
        /// <value>The poll style.</value>
        /// <remarks>Contionous, Scheduled and FileSystemWatch are valid for <c>Gatherer</c> objects.</remarks>
        public PollStyle PollStyle
        {
            get { return this.pollStyle; }
        }

        public ComponentState ComponentState { get; set; }


        public bool DatabasePopulated { get; set; }

        /// <summary>
        /// The get component state.
        /// </summary>
        /// <returns>
        /// The <see cref="ComponentState"/>.
        /// </returns>
        public ComponentState GetComponentState(bool componentState)
        {
            return ComponentState.Running;
        }
    }
}
