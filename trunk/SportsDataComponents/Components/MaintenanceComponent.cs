﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Threading;
using System.Timers;
using System.Xml;
using log4net;
using log4net.Config;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Classes.Enums;
using NTB.SportsData.Components.Maintenance;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;

namespace NTB.SportsData.Components.Components
{
    // Adding support for Log4Net

    // Adding Nif namespace
    // using NTB.SportsData.Components.Nif;

    /// <summary>
    ///     The maintenance component.
    /// </summary>
    public partial class MaintenanceComponent : Component, IBaseSportsDataInterfaces
    {
        /// <summary>
        ///     Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(MaintenanceComponent));

        /// <summary>
        ///     Busy status event
        /// </summary>
        private readonly AutoResetEvent _busyEvent = new AutoResetEvent(true);

        /// <summary>
        ///     Exit control event
        /// </summary>
        private readonly AutoResetEvent _stopEvent = new AutoResetEvent(false);

        /// <summary>
        ///     Buffer size for the file watcher
        /// </summary>
        /// <remarks>
        ///     <para>
        ///         Defines the internal buffer size the FileSystemWatcher when using this polling.
        ///         Should not be set to high. The instance will fall back to contious polling on buffer overflows.
        ///     </para>
        ///     <para>Default value: <c>4096</c></para>
        /// </remarks>
        protected int BufferSize = 4096;

        /// <summary>
        ///     Flag to indicate sucessful configure. Instance will not start polling nor handle messages if not properly
        ///     Configured
        /// </summary>
        /// <remarks>Holds the Configured status of the instance. <c>True</c> means successfully Configured</remarks>
        protected bool Configured;

        /// <summary>
        ///     Body for email notifications
        /// </summary>
        /// <remarks>The body is built during processing and used when sending the email when processing completes</remarks>
        protected string EmailBody;

        /// <summary>
        ///     Send Email-notifications when a new messages is processed
        /// </summary>
        /// <remarks>Set to an email address. Multiple adresses are supported, separate with <c>;</c></remarks>
        protected string EmailNotification;

        /// <summary>
        ///     Subject for email notifications
        /// </summary>
        /// <remarks>The subject is built during processing and used when sending the email when processing completes</remarks>
        protected string EmailSubject;

        /// <summary>
        ///     Error-Retry flag
        /// </summary>
        /// <remarks>
        ///     The flag is being set if a network or EWS error is encountered. Current operations are being aborted for later
        ///     retry.
        /// </remarks>
        protected bool ErrorRetry = false;

        /// <summary>
        ///     Input file filter
        /// </summary>
        /// <remarks>
        ///     <para>Defines what file types are being read from <see cref="FileInputFolder" />.</para>
        ///     <para>Default value: <c>NTBIDType.None</c></para>
        /// </remarks>
        protected string FileFilter = "*.xml";

        /// <summary>
        ///     Watch subdirs for new files as well
        /// </summary>
        /// <remarks>
        ///     <para>Defines if the instance should look in subdirectories within <see cref="FileInputFolder" />.</para>
        ///     <para>Default value: <c>False</c></para>
        /// </remarks>
        protected bool IncludeSubdirs = false;

        /// <summary>
        ///     The Configured interval for this instance, used for Continous polling
        /// </summary>
        /// <remarks>
        ///     <para>Indicates the interval time in seconds for <c>Continous</c>polling. 60 means that the job runs every minute.</para>
        ///     <para>Default value: <c>60</c></para>
        /// </remarks>
        protected int Interval = 60;

        /// <summary>
        ///     The Configured job name for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="InstanceName" /></remarks>
        protected string Name;

        /// <summary>
        ///     The output folder.
        /// </summary>
        protected string OutputFolder = string.Empty;

        /// <summary>
        ///     Defining the Schedule Type for this Component
        /// </summary>
        protected ScheduleType ScheduleType;

        /// <summary>
        ///     Initializes static members of the <see cref="MaintenanceComponent" /> class.
        /// </summary>
        static MaintenanceComponent()
        {
            XmlConfigurator.Configure();
            if (!LogManager.GetRepository()
                .Configured)
            {
                BasicConfigurator.Configure();
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="MaintenanceComponent" /> class.
        /// </summary>
        public MaintenanceComponent()
        {
            this.InitializeComponent();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="MaintenanceComponent" /> class.
        /// </summary>
        /// <param name="container">
        ///     The container.
        /// </param>
        public MaintenanceComponent(IContainer container)
        {
            container.Add(this);

            this.InitializeComponent();

            this.filesWatcher.Error += new ErrorEventHandler(this.OnError);
        }

        /// <summary>
        ///     Gets or sets the Federation Id.
        /// </summary>
        protected int FederationId { get; set; }

        /// <summary>
        ///     Gets or sets the Sport Id.
        /// </summary>
        protected int SportId { get; set; }

        /// <summary>
        ///     Gets or sets the file input folder.
        /// </summary>
        protected string FileInputFolder { get; set; }

        /// <summary>
        ///     Gets or sets the file done folder.
        /// </summary>
        protected string FileDoneFolder { get; set; }

        /// <summary>
        ///     Gets or sets the file error folder.
        /// </summary>
        protected string FileErrorFolder { get; set; }

        /// <summary>
        ///     Gets or sets the file output folder.
        /// </summary>
        protected string FileOutputFolder { get; set; }

        /// <summary>
        ///     Gets or sets the different file output folders
        /// </summary>
        protected List<OutputFolder> FileOutputFolders { get; set; }

        /// <summary>
        ///     Gets or sets the poll delay.
        /// </summary>
        protected int PollDelay { get; set; }

        /// <summary>
        /// The get component state.
        /// </summary>
        /// <returns>
        /// The <see cref="ComponentState"/>.
        /// </returns>
        public ComponentState GetComponentState(bool componentState)
        {
            return ComponentState.Running;
        }

        /// <summary>
        ///     Gets a value indicating whether the Enabled status of the instance.
        /// </summary>
        /// <value>The Enabled status.</value>
        /// <remarks><c>True</c> if the job instance is Enabled.</remarks>
        public bool Enabled
        {
            get { return this.enabled; }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether database populated.
        /// </summary>
        public bool DatabasePopulated { get; set; }

        /// <summary>
        ///     Gets the instance name.
        /// </summary>
        /// <summary>
        ///     Gets the name of the instance.
        /// </summary>
        /// <value>The name of the instance.</value>
        /// <remarks>The Configured instance job name.</remarks>
        public string InstanceName
        {
            get { return this.Name; }
        }

        /// <summary>
        ///     Gets the operation mode.
        /// </summary>
        /// <value>The operation mode.</value>
        /// <remarks><c>Gatherer</c> is the only valid mode, its hard coded for this component.</remarks>
        public OperationMode OperationMode
        {
            get { return OperationMode.Gatherer; }
        }

        /// <summary>
        ///     Gets the poll style.
        /// </summary>
        /// <value>The poll style.</value>
        /// <remarks>Contionous, Scheduled and FileSystemWatch are valid for <c>Gatherer</c> objects.</remarks>
        public PollStyle PollStyle
        {
            get { return this.pollStyle; }
        }

        /// <summary>
        ///     Gets or sets the component state.
        /// </summary>
        public ComponentState ComponentState { get; set; }

        /// <summary>
        ///     Gets or sets the maintenance mode.
        /// </summary>
        public MaintenanceMode MaintenanceMode { get; set; }

        /// <summary>
        ///     The configure.
        /// </summary>
        /// <param name="configNode">
        ///     The config node.
        /// </param>
        /// <exception cref="ArgumentException">
        ///     Returns an exception if we cannot configure the component
        /// </exception>
        public void Configure(XmlNode configNode)
        {
            // Basic configuration sanity check
            if (configNode.Attributes != null && (configNode == null || configNode.Name != "MaintenanceComponent" || configNode.Attributes.GetNamedItem("Name") == null))
            {
                throw new ArgumentException("The XML configuration node passed is invalid", "configNode");
            }

            if (Thread.CurrentThread.Name == null && configNode.Attributes != null)
            {
                Thread.CurrentThread.Name = configNode.Attributes.GetNamedItem("Name")
                    .ToString();
            }

            #region Basic config

            // Getting the name of this Component instance
            try
            {
                if (configNode.Attributes != null)
                {
                    this.Name = configNode.Attributes["Name"].Value;
                    this.enabled = Convert.ToBoolean(configNode.Attributes["Enabled"].Value);
                }

                ThreadContext.Stacks["NDC"].Push(this.InstanceName);

                if (configNode.Attributes != null && configNode.Attributes["InsertDatabase"] != null)
                {
                    this.DatabasePopulated = Convert.ToBoolean(configNode.Attributes["InsertDatabase"].Value);
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to configure this job instance!", ex);
            }

            // Basic operation
            try
            {
                if (configNode.Attributes != null)
                {
                    this.operationMode = (OperationMode) Enum.Parse(typeof(OperationMode), configNode.Attributes["OperationMode"].Value, true);
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing OperationMode values in XML configuration node", ex);
            }

            try
            {
                if (configNode.Attributes != null)
                {
                    this.pollStyle = (PollStyle) Enum.Parse(typeof(PollStyle), configNode.Attributes["PollStyle"].Value, true);
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing PollStyle values in XML configuration node", ex);
            }

            // Setting PollDelay to default three seconds
            this.PollDelay = 3;
            try
            {
                if (configNode.Attributes != null)
                {
                    this.PollDelay = Convert.ToInt32(configNode.Attributes["Delay"].Value);
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing PollStyle values in XML configuration node", ex);
            }

            #endregion

            try
            {
                // Find the file folder to work with
                var folderNode = configNode.SelectSingleNode("Folders/FileInputFolder");

                // configNode.SelectSingleNode("Folder").Attributes("XMLOutputFolder");
                if (folderNode != null)
                {
                    this.FileInputFolder = folderNode.InnerText;

                    // Check if folder exists
                    var di = new DirectoryInfo(this.FileInputFolder);
                    if (!di.Exists)
                    {
                        di.Create();
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Fatal("Not possible to get input-folder", exception);
            }

            this.FileOutputFolders = new List<OutputFolder>();
            // Find the file folder to work with
            try
            {
                // We shall get the output folders

                var folderNodes = configNode.SelectNodes("Folders/FileOutputFolders/FileOutputFolder");

                if (folderNodes != null)
                {
                    foreach (XmlNode node in folderNodes)
                    {
                        if (node.Attributes != null)
                        {
                            var outputFolder = new OutputFolder
                            {
                                OutputPath = node.InnerText,
                                OrgId = Convert.ToInt32(node.Attributes.GetNamedItem("orgId")
                                    .Value),
                                SportId = Convert.ToInt32(node.Attributes.GetNamedItem("sportId")
                                    .Value),
                                DisciplineId = node.Attributes.GetNamedItem("disciplineId") != null
                                    ? Convert.ToInt32(node.Attributes.GetNamedItem("disciplineId")
                                        .Value)
                                    : 0
                            };

                            // Check if folder exists
                            var di = new DirectoryInfo(outputFolder.OutputPath);
                            if (!di.Exists)
                            {
                                try
                                {
                                    di.Create();
                                }
                                catch (Exception exception)
                                {
                                    Logger.ErrorFormat("Could not create directory {0}", outputFolder.OutputPath);
                                    Logger.Error(exception.Message);
                                    Logger.Error(exception.StackTrace);
                                }
                            }

                            this.FileOutputFolders.Add(outputFolder);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Fatal("Not possible to get output-folders", exception);
            }

            // Find the file folder to work with
            try
            {
                var folderNode = configNode.SelectSingleNode("Folders/FileErrorFolder");

                if (folderNode != null)
                {
                    this.FileErrorFolder = folderNode.InnerText;

                    // Check if folder exists
                    var di = new DirectoryInfo(this.FileErrorFolder);
                    if (!di.Exists)
                    {
                        di.Create();
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Fatal("Not possible to get error-folder", exception);
            }

            // Find the file folder to work with
            try
            {
                var folderNode = configNode.SelectSingleNode("Folders/FileDoneFolder");

                if (folderNode != null)
                {
                    this.FileDoneFolder = folderNode.InnerText;

                    // Check if folder exists
                    var di = new DirectoryInfo(this.FileDoneFolder);
                    if (!di.Exists)
                    {
                        di.Create();
                    }
                }
                /*
                Logger.InfoFormat(
                    "Converter job - Polling: {0} / File input folder: {1} / Enabled: {2}",
                    Enum.GetName(typeof(PollStyle), pollStyle),
                    FileInputFolder,
                    enabled);
                 */
            }
            catch (Exception exception)
            {
                Logger.Fatal("Not possible to get done-folder", exception);
            }

            #region Set up polling

            try
            {
                // Switch on pollstyle
                switch (this.pollStyle)
                {
                    case PollStyle.Continous:
                        this.Interval = Convert.ToInt32(configNode.Attributes["Interval"].Value);
                        if (this.Interval == 0)
                        {
                            this.Interval = 5000;
                        }

                        this.pollTimer.Interval = this.Interval*1000; // short startup - interval * 1000;
                        Logger.DebugFormat("Poll interval: {0} seconds", this.Interval);

                        break;

                    case PollStyle.Scheduled:

                        /* 
                        schedule = configNode.Attributes["Schedule"].Value;
                        TimeSpan s = Utilities.GetScheduleInterval(schedule);
                        logger.DebugFormat("Schedule: {0} Calculated time to next run: {1}", schedule, s);
                        pollTimer.Interval = s.TotalMilliseconds;
                        */
                        throw new NotSupportedException("Invalid polling style for this job type");

                    case PollStyle.FileSystemWatch:

                        // Check for config overrides
                        if (configNode.Attributes.GetNamedItem("BufferSize") != null)
                        {
                            this.BufferSize = Convert.ToInt32(configNode.Attributes["BufferSize"].Value);
                        }

                        Logger.DebugFormat("FileSystemWatcher buffer size: {0}", this.BufferSize);

                        // Set values
                        this.filesWatcher.Path = this.FileInputFolder;
                        this.filesWatcher.Filter = this.FileFilter;
                        this.filesWatcher.IncludeSubdirectories = this.IncludeSubdirs;
                        this.filesWatcher.InternalBufferSize = this.BufferSize;

                        // Do not start the event watcher here, wait until after the startup cleaning job
                        this.pollTimer.Interval = 5000; // short startup;

                        break;

                    default:

                        // Unsupported pollstyle for this object
                        throw new NotSupportedException("Invalid polling style for this job type");
                }

                this.operationMode = OperationMode.Maintenance;

                this.Configured = true;
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                this.Configured = false;
            }

            #endregion
        }

        /// <summary>
        ///     The start.
        /// </summary>
        /// <exception cref="NotImplementedException">
        ///     If the component has not been implemented, an exception will occur
        /// </exception>
        public void Start()
        {
            if (!this.Configured)
            {
                throw new SportsDataException("MaintenanceComponent is not properly Configured. MaintenanceComponent::Start() Aborted!");
            }

            if (!this.Enabled)
            {
                throw new SportsDataException("MaintenanceComponent is not Enabled. MaintenanceComponent::Start() Aborted!");
            }

            Logger.InfoFormat("In Start() {0}.", this.InstanceName);

            this._stopEvent.Reset();
            this._busyEvent.Set();

            this.ComponentState = ComponentState.Running;

            this.pollTimer.Start();
        }

        /// <summary>
        ///     The stop.
        /// </summary>
        /// <exception cref="NotImplementedException">
        ///     If the component has not been implemented, an exception will occur
        /// </exception>
        public void Stop()
        {
            if (!this.Configured)
            {
                throw new SportsDataException("NFFWSComponent is not properly Configured. NFFComponent::Stop() Aborted");
            }

            if (!this.Enabled)
            {
                throw new SportsDataException("NFFWSComponent is not properly Configured. NFFComponent::Stop() Aborted");
            }

            // Signal Stop
            this._stopEvent.Set();

            // Stop events
            this.filesWatcher.EnableRaisingEvents = false;

            if (!this._busyEvent.WaitOne(30000))
            {
                Logger.InfoFormat("Instance did not complete properly. Data may be inconsistent. Job: {0}", this.InstanceName);
            }

            this.ComponentState = ComponentState.Halted;

            // Kill polling
            this.pollTimer.Stop();
        }

        /// <summary>
        /// The on error.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void OnError(object sender, ErrorEventArgs e)
        {
            if (e.GetException()
                .GetType() == typeof(InternalBufferOverflowException))
            {
                Logger.ErrorFormat("Error: File System Watcher internal buffer overflow at {0}", DateTime.Now);
            }
            else
            {
                Logger.ErrorFormat("Error: Watched directory not accessible at {0}", DateTime.Now);
            }

            this.NotAccessibleError(this.filesWatcher, e);
        }

        /// <summary>
        /// The not accessible error.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void NotAccessibleError(FileSystemWatcher source, ErrorEventArgs e)
        {
            const int MaxAttempts = 120;
            var timeOut = 30000;
            var i = 0;
            while (source != null && ((!Directory.Exists(source.Path) || source.EnableRaisingEvents == false) && i < MaxAttempts))
            {
                i += 1;
                try
                {
                    source.EnableRaisingEvents = false;
                    if (!Directory.Exists(source.Path))
                    {
                        Logger.ErrorFormat("Directory Inaccesible {0} at {1}", source.Path, DateTime.Now.ToString("HH:mm:ss"));
                        Thread.Sleep(timeOut);
                    }
                    else
                    {
                        // ReInitialize the Component
                        source.Dispose();
                        source = null;
                        source = new FileSystemWatcher();
                        ((ISupportInitialize) source).BeginInit();
                        source.EnableRaisingEvents = true;
                        source.Filter = this.FileFilter;
                        source.Path = this.FileInputFolder;
                        source.NotifyFilter = NotifyFilters.FileName;
                        source.Created += new FileSystemEventHandler(this.filesWatcher_Created);
                        source.Error += new ErrorEventHandler(this.OnError);
                        ((ISupportInitialize) source).EndInit();
                        Logger.ErrorFormat("Try to Restart RaisingEvents Watcher at {0}", DateTime.Now.ToString("HH:mm:ss"));
                    }
                }
                catch (Exception error)
                {
                    Logger.ErrorFormat("Error trying Restart Service {0} at {1}", error.StackTrace, DateTime.Now.ToString("HH:mm:ss"));
                    if (source != null)
                    {
                        source.EnableRaisingEvents = false;
                    }

                    Thread.Sleep(timeOut);
                }
            }
        }

        /// <summary>
        ///     The files watcher_ created.
        /// </summary>
        /// <param name="sender">
        ///     The sender.
        /// </param>
        /// <param name="e">
        ///     The e.
        /// </param>
        private void filesWatcher_Created(object sender, FileSystemEventArgs e)
        {
            // Wait for the busy state
            this._busyEvent.WaitOne();

            try
            {
                ThreadContext.Properties["JOBNAME"] = this.InstanceName;
                Logger.InfoFormat("MaintenanceComponent::filesWatcher_Created() hit with file data: {0}", e.FullPath);

                if (this.PollDelay > 0)
                {
                    // We take the value and multiply with thousand - to get the seconds... 
                    var pollTime = this.PollDelay*1000;

                    Logger.DebugFormat("We are waiting: {0} Seconds before starting to maintain!", this.PollDelay);

                    Thread.Sleep(pollTime);
                }

                try
                {
                    

                    // Adding files to the list and then sending that list to the converter
                    // @done: Create a method that takes string as argument also
                    // @done: change XMLToNITFConverter so that it does not loop, but does only one file at the time
                    var files = new List<string> {e.FullPath};

                    // Stopping timer to make sure it is not running while we are doing this
                    this.pollTimer.Stop();
                    this.StartRouter(files);

                    // Not sure if we want to start it since we are in File System Watch mode...
                    this.pollTimer.Start();
                }
                catch (Exception exception)
                {
                    this.filesWatcher.EnableRaisingEvents = false;
                    Logger.Debug("Something happened while doing filesWatcher_Created!", exception);
                    this.filesWatcher.EnableRaisingEvents = true;
                }
            }
            catch (StackOverflowException exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                if (exception.InnerException != null)
                {
                    Logger.Error(exception.InnerException.Message);
                    Logger.Error(exception.InnerException.StackTrace);
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                if (exception.InnerException != null)
                {
                    Logger.Error(exception.InnerException.Message);
                    Logger.Error(exception.InnerException.StackTrace);
                }
            }

            // Set the busy state
            this._busyEvent.Set();
        }

        private void StartRouter(List<string> files)
        {
            var router = new Router {OutputFolders = this.FileOutputFolders};

            foreach (var file in files)
            {
                try
                {
                    router.TournamentRouter(file);

                    // Moving to done folder
                    Logger.Info("Moving file to done folder");

                    var fileInfo = new FileInfo(file);
                    var filename = fileInfo.Name;

                    var moveFilename = this.FileDoneFolder + @"\" + filename;
                    var fileInfoMove = new FileInfo(moveFilename);

                    if (fileInfoMove.Exists)
                    {
                        fileInfoMove.Delete();
                    }

                    fileInfo.MoveTo(moveFilename);

                    this._busyEvent.Set();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    // Moving to error folder
                    Logger.Info("Moving file to error folder");

                    var fileInfo = new FileInfo(file);
                    var filename = fileInfo.Name;

                    var moveFilename = this.FileErrorFolder + @"\" + filename;
                    var fileInfoMove = new FileInfo(moveFilename);

                    if (fileInfoMove.Exists)
                    {
                        fileInfoMove.Delete();
                    }

                    fileInfo.MoveTo(moveFilename);
                }
            }
        }

        /// <summary>
        ///     The poll timer_ elapsed.
        /// </summary>
        /// <param name="sender">
        ///     The sender.
        /// </param>
        /// <param name="e">
        ///     The e.
        /// </param>
        private void pollTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            /*
             * We can consider adding getting data here if the time (day + hour + minute) hits. Otherwize we shall do nothing.
             * 
             */
            ThreadContext.Properties["JOBNAME"] = this.InstanceName;

            Logger.Debug("MaintenanceComponent::pollTimer_Elapsed() hit");
            Logger.DebugFormat("We have the following settings: Interval: {0}", this.pollTimer.Interval);

            // Reset flags and pollers
            this._busyEvent.WaitOne();
            this.pollTimer.Stop();

            try
            {
                Logger.DebugFormat("InputFolder for {0}: {1}", this.InstanceName, this.FileInputFolder);

                // Process any waiting files
                var files = new List<string>(Directory.GetFiles(this.FileInputFolder, "*.xml", SearchOption.TopDirectoryOnly));

                this.StartRouter(files);

                // Some logging
                Logger.DebugFormat("MaintenanceComponent::pollTimer_Elapsed() Maintenance items scheduled: {0}", files.Count);

                // Enable the event listening
                if (this.pollStyle == PollStyle.FileSystemWatch && !this.ErrorRetry)
                {
                    Logger.Debug("Setting filesWatcher.EnableRaisingEvents to true");
                    this.filesWatcher.EnableRaisingEvents = true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("MaintenanceComponent::pollTimer_Elapsed() error: " + ex.Message, ex);
            }

            // Check error state
            if (this.ErrorRetry)
            {
                this.filesWatcher.EnableRaisingEvents = false;
            }

            // Restart
            this.pollTimer.Interval = this.Interval*1000;
            if (this.pollStyle != PollStyle.FileSystemWatch || (this.pollStyle == PollStyle.FileSystemWatch && this.filesWatcher.EnableRaisingEvents == false))
            {
                this.pollTimer.Start();
            }

            this._busyEvent.Set();
        }
    }
}