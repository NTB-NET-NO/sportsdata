﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PurgeComponent.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The purge component.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Timers;
using System.Xml;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Classes.Enums;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;
using Quartz;
using Quartz.Impl;
using Quartz.Impl.Matchers;

namespace NTB.SportsData.Components.Components
{
    // Logging

    // Quartz support

    /// <summary>
    /// The purge component.
    /// </summary>
    public partial class PurgeComponent : Component, IBaseSportsDataInterfaces
    {
        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(PurgeComponent));

        /// <summary>
        /// Busy status event
        /// </summary>
        private readonly AutoResetEvent busyEvent = new AutoResetEvent(true);

        /// <summary>
        /// Exit control event
        /// </summary>
        private readonly AutoResetEvent stopEvent = new AutoResetEvent(false);

        /// <summary>
        /// schedulerFactory to be used to create scheduled tasks
        /// </summary>
        private readonly StdSchedulerFactory schedulerFactory = new StdSchedulerFactory();

        /// <summary>
        /// Flag to indicate sucessful configure. Instance will not start polling nor handle messages if not properly Configured
        /// </summary>
        /// <remarks>Holds the Configured status of the instance. <c>True</c> means successfully Configured</remarks>
        private bool configured;

        /// <summary>
        /// The error retry
        /// </summary>
        private bool errorRetry;

        /// <summary>
        /// The Configured job name for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="InstanceName"/></remarks>
        private string name;

        /// <summary>
        /// The scheduler.
        /// </summary>
        private IScheduler scheduler;

        /// <summary>
        /// The schedule type.
        /// </summary>
        private ScheduleType scheduleType;

        /// <summary>
        /// Initializes static members of the <see cref="PurgeComponent"/> class. 
        /// </summary>
        static PurgeComponent()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PurgeComponent"/> class.
        /// </summary>
        public PurgeComponent()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PurgeComponent"/> class.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        public PurgeComponent(IContainer container)
        {
            container.Add(this);

            this.InitializeComponent();
        }

        /// <summary>
        /// Gets a value indicating whether enabled.
        /// </summary>
        public bool Enabled
        {
            get
            {
                return this.enabled;
            }
        }

        /// <summary>
        /// Gets the instance name.
        /// </summary>
        public string InstanceName
        {
            get
            {
                return this.name;
            }
        }

        /// <summary>
        /// Gets the operation mode. 
        /// </summary>
        /// <value>The operation mode.</value>
        /// <remarks><c>Gatherer</c> is the only valid mode, its hard coded for this component.</remarks>
        public OperationMode OperationMode
        {
            get
            {
                return OperationMode.Purge;
            }
        }

        /// <summary>
        /// Gets the poll style.
        /// </summary>
        /// <value>The poll style.</value>
        /// <remarks>Contionous, Scheduled and FileSystemWatch are valid for <c>Gatherer</c> objects.</remarks>
        public PollStyle PollStyle
        {
            get
            {
                return this.pollStyle;
            }
        }

        /// <summary>
        /// Gets or sets the component state.
        /// </summary>
        public ComponentState ComponentState { get; set; }

        /// <summary>
        /// Gets or sets the maintenance mode.
        /// </summary>
        public MaintenanceMode MaintenanceMode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether database populated.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// Throws an exception as it is not implemented
        /// </exception>
        public bool DatabasePopulated
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Gets or sets the schedule group name.
        /// </summary>
        private string ScheduleGroupName { get; set; }

        /// <summary>
        /// The configure.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        public void Configure(XmlNode configNode)
        {
            Logger.Debug("Node: " + configNode.Name);

            // Some values that we need.
            this.errorRetry = false;

            // This boolean variable is used to tell us that the database shall be populated or not

            // Basic configuration sanity check
            if (configNode.Attributes != null && (configNode == null || configNode.Name != "PurgeComponent" || configNode.Attributes.GetNamedItem("Name") == null))
            {
                throw new ArgumentException("The XML configuration node passed is invalid", "configNode");
            }

            if (Thread.CurrentThread.Name == null)
            {
                if (configNode.Attributes != null)
                {
                    Thread.CurrentThread.Name = configNode.Attributes.GetNamedItem("Name").ToString();
                }
            }

            this.ScheduleGroupName = "PurgeGroup";
            // Getting the name of this Component instance
            try
            {
                if (configNode.Attributes != null)
                {
                    this.SetJobName(configNode);
                    this.ConfigureJobEnabled(configNode);
                    ThreadContext.Stacks["NDC"].Push(this.InstanceName);
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to configure this job instance!", ex);
                ThreadContext.Stacks["NDC"].Pop();
                throw;
            }

            this.configured = this.ConfigureOperationMode(configNode);

            this.configured = this.ConfigurePollStyleType(configNode);

            this.configured = this.ConfigureScheduleType(configNode);

            if (this.enabled)
            {
                this.ConfigurePollStyle(configNode);
            }

            // Finish configuration
            ThreadContext.Stacks["NDC"].Pop();
            this.configured = true;
        }

        /// <summary>
        /// The start.
        /// </summary>
        /// <exception cref="SportsDataException">
        /// Throws an exception if something goes wrong
        /// </exception>
        public void Start()
        {
            if (!this.configured)
            {
                throw new SportsDataException("PurgeComponent is not properly Configured. PurgeComponent::Start() Aborted!");
            }

            if (!this.enabled)
            {
                throw new SportsDataException("PurgeComponent is not Enabled. PurgeComponent::Start() Aborted!");
            }

            try
            {
                if (this.pollStyle == PollStyle.CalendarPoll)
                {
                    Logger.Info("Setting up jobListener and scheduler");

                    this.scheduler = this.schedulerFactory.GetScheduler();

                    var jobGroupNames = this.scheduler.GetJobGroupNames();

                    foreach (var jobGroupName in jobGroupNames)
                    {
                        Logger.Debug("JobGroupName: " + jobGroupName);

                        if (jobGroupName != this.ScheduleGroupName)
                        {
                            continue;
                        }

                        Logger.Debug("Key: " + this.scheduler.GetJobKeys(GroupMatcher<JobKey>.GroupContains(jobGroupName)));

                        var groupMatcher = GroupMatcher<JobKey>.GroupContains(jobGroupName);
                        var jobKeys = this.scheduler.GetJobKeys(groupMatcher);

                        foreach (var jobKey in jobKeys)
                        {
                            Logger.Debug("Name: " + jobKey.Name);
                            Logger.Debug("Group: " + jobKey.Group);
                        }
                    }

                    var triggerGroupNames = this.scheduler.GetTriggerGroupNames();

                    foreach (var triggerGroupName in triggerGroupNames)
                    {
                        Logger.Debug("TriggerGroupName: " + triggerGroupName);
                    }

                    var listOfJobs = this.scheduler.GetJobGroupNames();
                    foreach (var job in listOfJobs)
                    {
                        Logger.Debug("Job: " + job);
                    }

                    Logger.Debug("This Component name: " + this.InstanceName);

                    // Starting scheduler
                    this.scheduler.Start();
                }
            }
            catch (SchedulerException se)
            {
                Logger.Debug(se);
            }

            this.stopEvent.Reset();
            this.busyEvent.Set();

            // Starting poll-timer
            // pollTimer.Interval = 2000;
            this.pollTimer.Start();
        }

        /// <summary>
        /// The stop.
        /// </summary>
        /// <exception cref="SportsDataException">
        /// throws an exception if something goes wrong
        /// </exception>
        public void Stop()
        {
            if (!this.configured)
            {
                throw new SportsDataException("PurgeComponent is not properly Configured. NFFComponent::Stop() Aborted");
            }

            if (!this.enabled)
            {
                throw new SportsDataException("PurgeComponent is not properly Configured. NFFComponent::Stop() Aborted");
            }

            if (this.pollTimer.Enabled == false)
            {
                Logger.Debug("pollTimer is disabled");
            }
            else
            {
                Logger.Debug("pollTimer is Enabled");
            }

            if (this.pollTimer.Interval == 5000)
            {
                Logger.Debug("pollTimer has an interval of 5000 milliseconds");
            }
            else
            {
                Logger.Debug("pollTimer DOES NOT have an interval of 5000 milliseconds");
            }

            if (this.errorRetry)
            {
                Logger.Debug("We are in ErrorRetry mode!");
            }
            else
            {
                Logger.Debug("We are NOT in ErrorRetry mode!");
            }

            if (this.pollStyle == PollStyle.FileSystemWatch)
            {
                Logger.Debug("pollStyle is of style FileSystemWatch");
            }
            else
            {
                Logger.Debug("pollStyle is NOT of style FileSystemWatch");
            }

            // Check status - Handle busy polltimer loops
            if (this.pollStyle == PollStyle.CalendarPoll)
            {
                try
                {
                    Logger.Debug("Deleting / stopping jobs!");
                    ISchedulerFactory sf = new StdSchedulerFactory();
                    var sched = sf.GetScheduler();

                    // Shutting down scheduler
                    var groups = sched.GetJobGroupNames();

                    var keys = new List<JobKey>(sched.GetJobKeys(GroupMatcher<JobKey>.GroupEquals(this.InstanceName)));

                    // Line below marked out as it gives us less information
                    // sched.DeleteJobs(new List<JobKey>(keys));

                    // Looping through each key as we'd like to know when things go right and not!
                    keys.ForEach(
                        key =>
                            {
                                var detail = sched.GetJobDetail(key);
                                sched.DeleteJob(key);

                                Logger.Debug("Shutting down: " + detail.Description + "(" + key.Name + ")" + " Group: " + key.Group);
                            });

                    sched.Shutdown();
                }
                catch (SchedulerException sex)
                {
                    Logger.Fatal(sex);
                }
                catch (Exception ex)
                {
                    Logger.Error("Problems closing WebClients!", ex);
                }
            }

            if (!this.pollTimer.Enabled && (this.errorRetry || this.pollStyle != PollStyle.FileSystemWatch || this.pollTimer.Interval == 5000))
            {
                Logger.InfoFormat("Waiting for instance to complete work. Job {0}", this.InstanceName);
            }

            // Signal Stop
            this.stopEvent.Set();

            if (!this.busyEvent.WaitOne(30000))
            {
                Logger.InfoFormat("Instance did not complete properly. Data may be inconsistent. Job: {0}", this.InstanceName);
            }

            // Kill polling
            this.pollTimer.Stop();
        }

        /// <summary>
        /// The configure schedule type.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        /// <exception cref="ArgumentException">
        /// throws an exception if something goes wrong
        /// </exception>
        /// <returns>
        /// true if everything goes well
        /// </returns>
        public bool ConfigureScheduleType(XmlNode configNode)
        {
            try
            {
                if (configNode.Attributes == null)
                {
                    return false;
                }

                this.scheduleType = (ScheduleType)Enum.Parse(typeof(ScheduleType), configNode.Attributes["ScheduleType"].Value, true);
                return true;
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing ScheduleType values in XML configuration node", ex);
            }
        }

        /// <summary>
        /// The configure poll style.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        /// <exception cref="ArgumentException">
        /// throws an exception if something goes wrong
        /// </exception>
        /// <returns>
        /// true if everything goes well
        /// </returns>
        public bool ConfigurePollStyleType(XmlNode configNode)
        {
            try
            {
                if (configNode.Attributes == null)
                {
                    return false;
                }

                this.pollStyle = (PollStyle)Enum.Parse(typeof(PollStyle), configNode.Attributes["PollStyle"].Value, true);
                return true;
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing PollStyle values in XML configuration node", ex);
            }
        }

        /// <summary>
        /// The configure operation mode.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        /// <exception cref="ArgumentException">
        /// Throws an exception if something goes wrong
        /// </exception>
        /// <returns>
        /// true if everything goes well
        /// </returns>
        public bool ConfigureOperationMode(XmlNode configNode)
        {
            try
            {
                if (configNode.Attributes == null)
                {
                    return false;
                }

                this.operationMode = (OperationMode)Enum.Parse(typeof(OperationMode), configNode.Attributes["OperationMode"].Value, true);
                return true;
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing OperationMode values in XML configuration node", ex);
            }
        }

        /// <summary>
        /// The set job enabled.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        /// <returns>
        /// true if everything goes well
        /// </returns>
        public bool ConfigureJobEnabled(XmlNode configNode)
        {
            if (configNode.Attributes == null)
            {
                return false;
            }

            this.enabled = Convert.ToBoolean(configNode.Attributes["Enabled"].Value);
            return true;
        }

        /// <summary>
        /// The set job name.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        /// <returns>
        /// returns true if everything goes well
        /// </returns>
        public bool SetJobName(XmlNode configNode)
        {
            if (configNode.Attributes == null)
            {
                return false;
            }

            this.name = configNode.Attributes["Name"].Value;
            return true;
        }

        /// <summary>
        /// The configure poll style.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        /// <exception cref="NotSupportedException">
        /// throws an exception if something goes wrong
        /// </exception>
        /// <exception cref="ArgumentException">
        /// throws an exception if something goes wrong
        /// </exception>
        public void ConfigurePollStyle(XmlNode configNode)
        {
            try
            {
                // Switch on pollstyle
                switch (this.pollStyle)
                {
                    case PollStyle.CalendarPoll:
                        // Getting the times for this job
                        this.ConfigurePollStyleCalendarPoll(configNode);
                        break;

                    case PollStyle.Scheduled:
                        throw new NotSupportedException("Invalid polling style for this job type");

                    case PollStyle.FileSystemWatch:
                        throw new NotSupportedException("Invalid polling style for this job type");

                    case PollStyle.Continous:
                        break;
                    case PollStyle.PullSubscribe:
                        break;
                    case PollStyle.PushSubscribe:
                        break;
                    case PollStyle.SynchDistributor:
                        break;
                    default:

                        // Unsupported pollstyle for this object
                        throw new NotSupportedException("Invalid polling style for this job type (" + this.pollStyle + ")");
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing PollStyle-specific values in XML configuration node: " + ex.Message, ex);
            }
        }

        /// <summary>
        /// The configure poll style calendar poll.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ConfigurePollStyleCalendarPoll(XmlNode configNode)
        {
            try
            {
                Logger.Info("Setting up schedulers");
                Logger.Info("--------------------------------------------------------------");

                var nodeList = this.GetNumberJobs(configNode);

                Logger.DebugFormat("Schedule of type: {0}", this.scheduleType);

                // Getting the scheduler
                this.scheduler = this.schedulerFactory.GetScheduler();

                switch (this.scheduleType)
                {
                    case ScheduleType.Daily:
                        this.ConfigureDailyQuartzJob(nodeList);
                        break;

                    case ScheduleType.Weekly:
                        this.ConfigureWeeklyQuartzJob(nodeList);
                        break;

                    case ScheduleType.Monthly:
                        // doing Monthly stuff here
                        break;

                    case ScheduleType.Yearly:
                        // doing yearly Stuff here
                        break;

                    case ScheduleType.Continous:
                        break;

                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            catch (SchedulerConfigException sce)
            {
                Logger.Debug("A SchedulerConfigException has occured: ", sce);
                return false;
            }
            catch (SchedulerException se)
            {
                Logger.Debug("A schedulerException has occured: ", se);
                return false;
            }
            catch (Exception e)
            {
                Logger.Debug("An exception has occured", e);
                return false;
            }

            return true;
        }

        /// <summary>
        /// The configure weekly quartz job.
        /// </summary>
        /// <param name="nodeList">
        /// The node list.
        /// </param>
        public void ConfigureWeeklyQuartzJob(XmlNodeList nodeList)
        {
            IJobDetail weeklyJobDetail = new JobDetailImpl();
            ITrigger weeklyTrigger = null;

            foreach (XmlNode node in nodeList)
            {
                if (node.Attributes == null)
                {
                    continue;
                }

                var scheduleId = this.GetScheduleId(node);

                var duration = this.ConfigureQuartzJobDuration(node);

                var scheduleTime = this.CreateScheduleTime(node);

                var hours = Convert.ToInt32(scheduleTime.Hour);
                var minutes = Convert.ToInt32(scheduleTime.Minutes);

                var purgeWeeks = string.Empty;
                if (node.Attributes["Week"] != null)
                {
                    purgeWeeks = node.Attributes["Week"].Value;
                }

                var scheduleDay = string.Empty;
                if (node.Attributes["DayOfWeek"] != null)
                {
                    scheduleDay = node.Attributes["DayOfWeek"].Value;
                }

                var numDayOfWeek = this.GetNumDayOfWeek();

                // Checking if numDayOfWeek is smaller than ScheduleDay
                DateTimeOffset dto = DateTime.Today;

                // Get the scheduled day
                var scheduledDay = Convert.ToInt32(scheduleDay);

                int days;
                const int Weekdays = 7;
                if (numDayOfWeek <= scheduledDay)
                {
                    // It is, so we need to find out when the next scheduling should happen
                    days = scheduledDay - numDayOfWeek; // this can be zero

                    dto = DateTime.Today.AddDays(days).AddHours(hours).AddMinutes(minutes);
                }
                else if (numDayOfWeek > scheduledDay)
                {
                    var daysLeft = Weekdays - numDayOfWeek;
                    days = daysLeft + scheduledDay;

                    dto = DateTime.Today.AddDays(days).AddHours(hours).AddMinutes(minutes);
                }

                // Creating a simple Scheduler
                var calendarIntervalSchedule = CalendarIntervalScheduleBuilder.Create();

                // Creating the weekly Trigger using the stuff that we've calculated
                var jobIdentity = string.Format("{0}_{1}", this.InstanceName, scheduleId);
                weeklyTrigger = TriggerBuilder.Create()
                    .WithDescription(this.InstanceName)
                    .WithIdentity(jobIdentity, this.ScheduleGroupName)
                    .StartAt(dto)
                    .WithSchedule(calendarIntervalSchedule.WithIntervalInWeeks(Convert.ToInt32(purgeWeeks)))
                    .Build();

                // This part might be moved
                if (this.operationMode == OperationMode.Purge)
                {
                    var weeklyDetailIdentity = string.Format("job_", jobIdentity);
                    weeklyJobDetail = JobBuilder.Create<PurgeSportsData>()
                        .WithIdentity(weeklyDetailIdentity, this.ScheduleGroupName)
                        .WithDescription(this.InstanceName)
                        .Build();
                }

                weeklyJobDetail.JobDataMap["ScheduleType"] = this.scheduleType.ToString();
                weeklyJobDetail.JobDataMap["Weeks"] = purgeWeeks;
                weeklyJobDetail.JobDataMap["Duration"] = duration;
            }

            if (weeklyTrigger == null)
            {
                return;
            }

            Logger.DebugFormat("Setting up and starting weeklyJobDetail job {0}, using trigger: {1}", weeklyJobDetail.Description, weeklyTrigger.Description);
            this.scheduler.ScheduleJob(weeklyJobDetail, weeklyTrigger);
        }

        /// <summary>
        /// The get schedule id.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public string GetScheduleId(XmlNode node)
        {
            return node.Attributes == null ? string.Empty : node.Attributes["Id"].Value;
        }

        /// <summary>
        /// The get num day of week.
        /// </summary>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int GetNumDayOfWeek()
        {
            // If we use the DateTimeOffset it means that we have to find out if the day has passed or not.
            // So we have to find out which day it is today
            var today = DateTime.Today.DayOfWeek;
            var numDayOfWeek = 0;
            if (today.ToString() == "Monday")
            {
                numDayOfWeek = 1;
            }
            else if (today.ToString() == "Tuesday")
            {
                numDayOfWeek = 2;
            }
            else if (today.ToString() == "Wednesday")
            {
                numDayOfWeek = 3;
            }
            else if (today.ToString() == "Thursday")
            {
                numDayOfWeek = 4;
            }
            else if (today.ToString() == "Friday")
            {
                numDayOfWeek = 5;
            }
            else if (today.ToString() == "Saturday")
            {
                numDayOfWeek = 6;
            }
            else if (today.ToString() == "Sunday")
            {
                numDayOfWeek = 0;
            }

            return numDayOfWeek;
        }

        /// <summary>
        /// The configure daily quartz job.
        /// </summary>
        /// <param name="nodeList">
        /// The node list.
        /// </param>
        public void ConfigureDailyQuartzJob(XmlNodeList nodeList)
        {
            foreach (XmlNode node in nodeList)
            {
                var scheduleId = this.GetScheduleId(node);
                var scheduleTime = this.CreateScheduleTime(node);

                // Checking if there is a setting for Duration
                var duration = this.ConfigureQuartzJobDuration(node);

                /*
                 * Creating cron expressions and more. 
                 * This is the cron syntax:
                 *  Seconds
                 *  Minutes
                 *  Hours
                 *  Day-of-Month
                 *  Month
                 *  Day-of-Week
                 *  Year (optional field)
                 */

                // Creating the daily cronexpression
                var stringCronExpression = string.Format("0 {0} {1} ? * * ", scheduleTime.Minutes, scheduleTime.Hour);

                // Setting up the CronTrigger
                Logger.Debug("Setting up the CronTrigger with following pattern: " + stringCronExpression);

                // dailyCronTrigger = new CronTrigger(InstanceName + ScheduleID, InstanceName);
                // dailyCronTrigger.CronExpression = cronExpression;
                var dailyCronTrigger = TriggerBuilder.Create().WithIdentity(this.InstanceName + "_" + scheduleId, "Group1").WithDescription(this.InstanceName).WithCronSchedule(stringCronExpression).Build();

                Logger.Debug("dailyCronTrigger: " + dailyCronTrigger.Description);

                // Creating the jobDetail 
                var dailyJobDetail = JobBuilder.Create<PurgeSportsData>().WithIdentity("job_" + this.InstanceName + "_" + scheduleId, "group1").WithDescription(this.InstanceName).Build();

                dailyJobDetail.JobDataMap["ScheduleType"] = this.scheduleType;
                dailyJobDetail.JobDataMap["Duration"] = duration;

                Logger.DebugFormat("Setting up and starting dailyJobDetail job {0} using trigger: {1}", dailyJobDetail.Description, dailyCronTrigger.Description);

                // scheduler.ScheduleJob(dailyJobDetail, dailyCronTrigger[a]);
                this.scheduler.ScheduleJob(dailyJobDetail, dailyCronTrigger);
            }
        }

        /// <summary>
        /// The configure quartz job duration.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int ConfigureQuartzJobDuration(XmlNode node)
        {
            if (node.Attributes == null)
            {
                return 0;
            }

            var xmlAttribute = node.Attributes["Duration"];
            return xmlAttribute != null ? Convert.ToInt32(xmlAttribute.Value) : 0;
        }

        /// <summary>
        /// Sets the schedule time for the quartz scheduler
        /// </summary>
        /// <param name="node">
        /// the xml node 
        /// </param>
        /// <returns>
        /// a schedule time object
        /// </returns>
        public ScheduleTime CreateScheduleTime(XmlNode node)
        {
            var scheduleDateTime = string.Empty;
            if (node.Attributes != null)
            {
                scheduleDateTime = node.Attributes["Time"].Value;
            }

            if (scheduleDateTime == string.Empty)
            {
                throw new Exception("The Time for deleting is not defined! Please check your configuration file. Cannot continue");
            }

            if (!scheduleDateTime.Contains(":"))
            {
                throw new Exception("Format of time is not correct. The time shall be defined in HH:MM format. Please check your configuration file. Cannot continue");
            }

            // Splitting the time into two values so that we can create a cron job
            var scheduleTimeArray = scheduleDateTime.Split(':');

            var scheduleTime = new ScheduleTime { Minutes = scheduleTimeArray[1], Hour = scheduleTimeArray[0] };

            // If minutes contains two zeros (00), we change it to one zero (0) 
            // If not, scheduler won't understand
            if (scheduleTime.Minutes == "00")
            {
                scheduleTime.Minutes = "0";
            }

            // Doing the same thing for hours
            if (scheduleTime.Hour == "00")
            {
                scheduleTime.Hour = "0";
            }

            // returning the value
            return scheduleTime;
        }

        /// <summary>
        /// The get component state.
        /// </summary>
        /// <returns>
        /// The <see cref="ComponentState"/>.
        /// </returns>
        public ComponentState GetComponentState(bool componentState)
        {
            return ComponentState.Running;
        }

        /// <summary>
        /// Getting the number of jobs to create
        /// </summary>
        /// <param name="configNode">
        /// the config node
        /// </param>
        /// <returns>
        /// number of jobs
        /// </returns>
        private XmlNodeList GetNumberJobs(XmlNode configNode)
        {
            // Getting the number of jobs to create
            var xPathExpression = string.Format("../PurgeComponent[@Name='{0}']/Schedules/Schedule", this.InstanceName);
            var nodeList = configNode.SelectNodes(xPathExpression);

            var jobs = 0;
            if (nodeList != null)
            {
                jobs = nodeList.Count;
            }

            Logger.DebugFormat("Number of jobs: {0}", jobs);
            return nodeList;
        }

        /// <summary>
        /// The poll timer_ elapsed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void PollTimerElapsed(object sender, ElapsedEventArgs e)
        {
            /*
             * We can consider adding getting data here if the time (day + hour + minute) hits. Otherwize we shall do nothing.
             * 
             */
            ThreadContext.Properties["JOBNAME"] = this.InstanceName;
            Logger.DebugFormat("PurgeComponent::pollTimer_Elapsed() hit");

            // _busyEvent.WaitOne();
            this.pollTimer.Stop();

            switch (this.pollStyle)
            {
                // For continuous and scheduled do a simple folder item traversal
                case PollStyle.Scheduled:
                case PollStyle.Continous:
                    if (this.operationMode == OperationMode.Distributor)
                    {
                        // this.foo();
                    }

                    this.pollTimer.Start();

                    break;

                case PollStyle.CalendarPoll:

                    // @todo: need to do some work here
                    Logger.Debug("Disabling pollTimer");
                    this.pollTimer.Enabled = false;

                    // _busyEvent.WaitOne();
                    break;
            }
        }
    }
}