// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NFFGathererComponent.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the NffGathererComponent type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Timers;
using System.Xml;
using log4net;
using log4net.Config;
using NTB.SportsData.Classes.Enums;
using NTB.SportsData.Components.Maintenance;
using NTB.SportsData.Components.Nff;
using NTB.SportsData.Components.Nff.Configurators;
using NTB.SportsData.Components.Nff.DataFileCreators;
using NTB.SportsData.Components.Nff.Facade;
using NTB.SportsData.Components.Nff.Models;
using NTB.SportsData.Components.PublicRepositories;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;
using NTB.SportsData.Utilities;
using Quartz;
using Quartz.Impl;
using Quartz.Impl.Matchers;

namespace NTB.SportsData.Components.Components
{
    // Add support for logging

    // Adding enums
    // Adding interfaces

    // Adding utilities

    // Adding support for Quarts.Net

    /// <summary>
    ///     The nff gatherer component.
    /// </summary>
    public partial class NffGathererComponent : Component, IBaseSportsDataInterfaces
    {
        /// <summary>
        ///     Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(NffGathererComponent));

        /// <summary>
        ///     Busy status event
        /// </summary>
        private readonly AutoResetEvent _busyEvent = new AutoResetEvent(true);

        /// <summary>
        ///     Exit control event
        /// </summary>
        private readonly AutoResetEvent _stopEvent = new AutoResetEvent(false);

        /// <summary>
        ///     The result field that we use to store if this is a result file or not.
        /// </summary>
        private bool _result;

        /// <summary>
        ///     Flag to indicate sucessful configure. Instance will not start polling nor handle messages if not properly
        ///     Configured
        /// </summary>
        /// <remarks>Holds the Configured status of the instance. <c>True</c> means successfully Configured</remarks>
        protected bool Configured;

        /// <summary>
        ///     The Content Type
        /// </summary>
        protected string ContentType;

        /// <summary>
        ///     Body for email notifications
        /// </summary>
        /// <remarks>The body is built during processing and used when sending the email when processing completes</remarks>
        protected string EmailBody;

        /// <summary>
        ///     Send Email-notifications when a new messages is processed
        /// </summary>
        /// <remarks>Set to an email address. Multiple adresses are supported, separate with <c>;</c></remarks>
        protected string EmailNotification;

        /// <summary>
        ///     Subject for email notifications
        /// </summary>
        /// <remarks>The subject is built during processing and used when sending the email when processing completes</remarks>
        protected string EmailSubject;

        /// <summary>
        ///     Error-Retry flag
        /// </summary>
        /// <remarks>
        ///     The flag is being set if a network or EWS error is encountered. Current operations are being aborted for later
        ///     retry.
        /// </remarks>
        protected bool ErrorRetry = false;

        /// <summary>
        ///     The Configured job name for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="InstanceName" /></remarks>
        protected string Name;

        /// <summary>
        ///     The schedule type.
        /// </summary>
        protected ScheduleType ScheduleType;

        /// <summary>
        ///     The season id.
        /// </summary>
        protected int SeasonId = 0;

        /// <summary>
        ///     Initializes static members of the <see cref="NffGathererComponent" /> class.
        /// </summary>
        static NffGathererComponent()
        {
            // Set up logger
            XmlConfigurator.Configure();
            if (!LogManager.GetRepository()
                .Configured)
            {
                BasicConfigurator.Configure();
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="NffGathererComponent" /> class.
        /// </summary>
        public NffGathererComponent()
        {
            this.InitializeComponent();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="NffGathererComponent" /> class.
        /// </summary>
        /// <param name="container">
        ///     The container.
        /// </param>
        public NffGathererComponent(IContainer container)
        {
            container.Add(this);

            this.InitializeComponent();
        }

        /// <summary>
        ///     Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        ///     Gets or sets the federation id.
        /// </summary>
        public int FederationId { get; set; }

        public string FixtureType { get; set; }

        /// <summary>
        ///     Gets a value indicating whether the Enabled status of the instance.
        /// </summary>
        /// <value>The Enabled status.</value>
        /// <remarks><c>True</c> if the job instance is Enabled.</remarks>
        public bool Enabled
        {
            get { return this.enabled; }
        }

        /// <summary>
        ///     Gets or sets the component state.
        /// </summary>
        public ComponentState ComponentState { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether database populated.
        /// </summary>
        public bool DatabasePopulated { get; set; }

        /// <summary>
        ///     Gets or sets the maintenance mode.
        /// </summary>
        public MaintenanceMode MaintenanceMode { get; set; }

        /// <summary>
        ///     The configure.
        /// </summary>
        /// <param name="configNode">
        ///     The config node.
        /// </param>
        /// <exception cref="ArgumentException">
        ///     Argument Exception
        /// </exception>
        /// <exception cref="NotSupportedException">
        ///     Not supported exception
        /// </exception>
        /// <exception cref="Exception">
        ///     Exception when something wrong happens
        /// </exception>
        public void Configure(XmlNode configNode)
        {
            Logger.Debug("Node: " + configNode.Name);

            // This string is used to set the name of the job
            // Getting the name of this Component instance
            try
            {
                if (configNode.Attributes != null)
                {
                    this.Name = configNode.Attributes["Name"].Value;
                    this.enabled = Convert.ToBoolean(configNode.Attributes["Enabled"].Value);
                }

                ThreadContext.Stacks["NDC"].Push(this.InstanceName);

                // This value is used to tell that we shall insert into database... I just wonder if we shall do it
                // on the schedule-level... Well, we try here first
                // Done: Consider using schedule-level to decide if we are to insert into database or not

                // Check if we are to insert the data into the database
                if (configNode.Attributes != null && configNode.Attributes["InsertDatabase"] != null)
                {
                    this.PopulateDatabase = Convert.ToBoolean(configNode.Attributes["InsertDatabase"].Value);
                }

                // Check if we are to create an XML-File
                if (configNode.Attributes != null && configNode.Attributes["XML"] != null)
                {
                    this.CreateXml = Convert.ToBoolean(configNode.Attributes["XML"].Value);
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to configure this job instance!", ex);
            }

            if (this.enabled == false)
            {
                // Finish configuration
                ThreadContext.Stacks["NDC"].Pop();
                this.Configured = true;

                return;
            }

            if (configNode.Attributes != null)
            {
                Logger.Debug("Name attribut: " + configNode.Attributes.GetNamedItem("Name"));

                #region Basic configuration

                // Basic configuration sanity check
                if (configNode.Name != "NFFComponent" || configNode.Attributes.GetNamedItem("Name") == null)
                {
                    throw new ArgumentException("The XML configuration node passed is invalid", "configNode");
                }

                if (Thread.CurrentThread.Name == null)
                {
                    Thread.CurrentThread.Name = configNode.Attributes.GetNamedItem("Name")
                        .ToString();
                }

                #endregion

                #region Basic operation

                // Getting the OperationMode which tells if this what type of OperationMode this is
                try
                {
                    this.operationMode =
                        (OperationMode)
                            Enum.Parse(typeof(OperationMode), configNode.Attributes["OperationMode"].Value, true);
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing OperationMode values in XML configuration node", ex);
                }

                if (configNode.Attributes != null && configNode.Attributes["MaintenanceMode"] != null)
                {
                    try
                    {
                        this.maintenanceMode =
                            (MaintenanceMode)
                                Enum.Parse(typeof(MaintenanceMode), configNode.Attributes["MaintenanceMode"].Value, true);
                    }
                    catch (Exception ex)
                    {
                        ThreadContext.Stacks["NDC"].Pop();
                        throw new ArgumentException(
                            "Invalid or missing OperationMode values in XML configuration node", ex);
                    }
                }

                // Getting the PollStyle which tells if this what type of PollStyle this is
                try
                {
                    this.pollStyle =
                        (PollStyle) Enum.Parse(typeof(PollStyle), configNode.Attributes["PollStyle"].Value, true);
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing PollStyle values in XML configuration node", ex);
                }

                // Getting the ScheduleType which tells if this what type of scheduling we are doing
                try
                {
                    if (configNode.Attributes != null && configNode.Attributes["ScheduleType"] != null)
                    {
                        this.ScheduleType =
                            (ScheduleType)
                                Enum.Parse(typeof(ScheduleType), configNode.Attributes["ScheduleType"].Value, true);
                    }
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing ScheduleType values in XML configuration node", ex);
                }

                try
                {
                    if (configNode.Attributes != null && configNode.Attributes["ContentType"] != null)
                    {
                        this.ContentType = configNode.Attributes["ContentType"].ToString();
                    }
                }
                catch (Exception exception)
                {
                    Logger.Info("Not able to get Content Type of message.");
                    Logger.Info("No critical error");
                    Logger.Debug(exception);
                }

                try
                {
                    if (configNode.Attributes != null && configNode.Attributes["FixtureType"] != null)
                    {
                        this.FixtureType = configNode.Attributes["FixtureType"].ToString();
                    }
                }
                catch (Exception exception)
                {
                    Logger.Info("Not able to get Content Type of message.");
                    Logger.Info("No critical error");
                    Logger.Debug(exception);
                }
            }

            #endregion

            // Some values that we need.

            // This boolean variable is used to tell us that the database shall be populated or not
            this.PopulateDatabase = false;

            const bool purge = false;

            this.CreateXml = false;

            // Getting and setting the Federation Id and the Sport Id
            try
            {
                var federationNode = configNode.SelectSingleNode("Federation");

                if (federationNode != null && federationNode.Attributes["orgId"] != null)
                {
                    if (federationNode.Attributes["orgId"] != null)
                    {
                        this.FederationId = Convert.ToInt32(federationNode.Attributes["orgId"].Value);
                    }

                    if (federationNode.Attributes["sportId"] != null)
                    {
                        this.SportId = Convert.ToInt32(federationNode.Attributes["sportId"].Value);
                    }
                }
            }
            catch (Exception exception)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException(
                    "Invalid or missing Federation if and sport id values in XML configuration node", exception);
            }

            try
            {
                // Find the file folder to work with
                // Done: This could be a nodelist though - need to check if we get more than one result
                var folderNode = configNode.SelectSingleNode("Folders/Folder[@Type='XMLOutputFolder']");

                if (folderNode != null)
                {
                    this.FileOutputFolder = folderNode.InnerText;
                }

                Logger.InfoFormat(
                    "Gatherer job - Polling: {0} / File output folder: {1} / Enabled: {2}",
                    Enum.GetName(typeof(PollStyle), this.pollStyle), this.FileOutputFolder, this.enabled);
            }
            catch (Exception exception)
            {
                Logger.Fatal("Not possible to get output-folder", exception);
            }

            if (this.enabled)
            {
                // Checking if file folders exists
                try
                {
                    if (this.FileOutputFolder != null)
                    {
                        if (!Directory.Exists(this.FileOutputFolder))
                        {
                            Directory.CreateDirectory(this.FileOutputFolder);
                        }
                    }
                }
                catch (Exception exception)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid, unknown or missing file folder: " + this.FileOutputFolder,
                        exception);
                }


                // Creating Instance Name
                try
                {
                    // Switch on pollstyle
                    switch (this.pollStyle)
                    {
                        case PollStyle.Continous:
                            this.Interval = Convert.ToInt32(configNode.Attributes["Interval"].Value)*1000;
                            if (this.Interval == 0)
                            {
                                this.Interval = 5000;
                            }

                            this.pollTimer.Interval = this.Interval; // short startup - interval * 1000;
                            Logger.DebugFormat("Poll interval: {0} seconds", this.Interval);

                            break;

                        // Code for Pubnub push technology
                        // CONTINUE HERE TOMORROW! (Is this done, what am I to do here tomorrow) (2012-03-09)
                        case PollStyle.PushSubscribe:

                            this._channel = "THE_FIKS_PRODUCTION_CHANNEL";
                            try
                            {
                                Logger.Debug("Setting up Pubnub");

                                // Done: Add subscription string to app.config
                                var pubnubSubscriberKey = ConfigurationManager.AppSettings["pubnub_subscriberkey"];
                                this._pubnub = new PubnubNff(string.Empty, pubnubSubscriberKey, string.Empty, false);

                                Logger.Debug("Done setting up Pubnub");
                            }
                            catch (Exception exception)
                            {
                                Logger.Error(exception.Message);
                                Logger.Error(exception);
                            }

                            break;


                        case PollStyle.CalendarPoll:
                            // Getting the times for this job
                            try
                            {
                                IJobDetail weeklyJobDetail = null;

                                ITrigger weeklyTrigger = null;

                                Logger.Info("Setting up schedulers");
                                Logger.Info("--------------------------------------------------------------");

                                // Getting the number of jobs to create
                                var nodeList =
                                    configNode.SelectNodes("../NFFComponent[@Name='" + this.InstanceName +
                                                           "']/Schedules/Schedule");

                                var jobs = nodeList.Count;
                                Logger.Info("Number of jobs: " + jobs);

                                // Creating a string to store the ScheduleID
                                string scheduleId;

                                // Creating an integer value to be used to tell the duration of the gathering of data
                                var duration = 0;

                                // Creating a boolean variable to store if this is a schedule-message og not
                                var scheduleMessage = false;

                                // Creating integer for offset
                                // This variable is used to tell that we are to get tomorrows matches or yesterdays matches
                                // Used to create the 'Todays matches' for newspapers
                                // So a positive number is most commonly used
                                var offset = 0;

                                // Creating bool variable to tell if we are to create a total schedule or not
                                var totalSchedule = false;

                                var dateTimeOffset = DateTime.Today;
                                if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                                {
                                    dateTimeOffset =
                                        Convert.ToDateTime(ConfigurationManager.AppSettings["nfftestingdate"]);
                                }


                                Logger.Debug("Schedule of type: " + this.ScheduleType);

                                // Getting the scheduler
                                this.Scheduler = this.SchedulerFactory.GetScheduler();

                                switch (this.ScheduleType)
                                {
                                    case ScheduleType.Daily:
                                        foreach (XmlNode node in nodeList)
                                        {
                                            // Creating the offset time 
                                            if (node.Attributes["Total"] != null)
                                            {
                                                totalSchedule = Convert.ToBoolean(node.Attributes["Total"].Value);
                                            }

                                            // If the value is larger or less than 0
                                            if (offset > 0 || offset < 0)
                                            {
                                                if (offset > 0)
                                                {
                                                    dateTimeOffset = DateTime.Today.AddDays(offset);
                                                }
                                                else if (offset < 0)
                                                {
                                                    var fromDaysOffset = TimeSpan.FromDays(offset);
                                                    dateTimeOffset = DateTime.Today.Subtract(fromDaysOffset);
                                                }
                                            }

                                            scheduleId = node.Attributes["Id"].Value;

                                            // Creating the offset time 
                                            if (node.Attributes["Offset"] != null)
                                            {
                                                offset = Convert.ToInt32(node.Attributes["Offset"].Value);
                                            }

                                            // If the value is larger or less than 0
                                            if (offset > 0 || offset < 0)
                                            {
                                                if (offset > 0)
                                                {
                                                    dateTimeOffset = DateTime.Today.AddDays(offset);
                                                }
                                                else if (offset < 0)
                                                {
                                                    var fromDaysOffset = TimeSpan.FromDays(offset);
                                                    dateTimeOffset = DateTime.Today.Subtract(fromDaysOffset);
                                                }
                                            }

                                            // Splitting the time into two values so that we can create a cron job
                                            var scheduleTimeArray =
                                                node.Attributes["Time"].Value.Split(':');

                                            // If minutes contains two zeros (00), we change it to one zero (0) 
                                            // If not, scheduler won't understand
                                            if (scheduleTimeArray[1] == "00")
                                            {
                                                scheduleTimeArray[1] = "0";
                                            }

                                            // Doing the same thing for hours
                                            if (scheduleTimeArray[0] == "00")
                                            {
                                                scheduleTimeArray[0] = "0";
                                            }

                                            // Checking if there is a setting for results
                                            if (node.Attributes["Results"] != null)
                                            {
                                                this._result = Convert.ToBoolean(node.Attributes["Results"].Value);
                                                scheduleMessage = !this._result;
                                            }

                                            // Checking if there is a setting for Duration
                                            if (node.Attributes["Duration"] != null)
                                            {
                                                duration = Convert.ToInt32(node.Attributes["Duration"].Value);
                                            }

                                            /*
                                             * Creating cron expressions and more. 
                                             * This is the cron syntax:
                                             *  Seconds
                                             *  Minutes
                                             *  Hours
                                             *  Day-of-Month
                                             *  Month
                                             *  Day-of-Week
                                             *  Year (optional field)
                                             */

                                            // Creating the daily cronexpression
                                            var stringCronExpression = "0 " +
                                                                       scheduleTimeArray[1] + " " +
                                                                       scheduleTimeArray[0] + " " +
                                                                       "? " +
                                                                       "* " +
                                                                       "* ";

                                            // Setting up the CronTrigger
                                            Logger.Debug("Setting up the CronTrigger with following pattern: " +
                                                         stringCronExpression);

                                            // Creating the daily Cron Trigger
                                            var dailyCronTrigger = TriggerBuilder.Create()
                                                .WithIdentity(this.InstanceName + "_" + scheduleId, "GroupNFF")
                                                .WithDescription(this.InstanceName)
                                                .WithCronSchedule(stringCronExpression)
                                                .Build();

                                            Logger.Debug("dailyCronTrigger: " + dailyCronTrigger.Description);

                                            // Creating the jobDetail 
                                            var dailyJobDetail = JobBuilder.Create<NffJobBuilder>()
                                                .WithIdentity("job_" + this.InstanceName + "_" + scheduleId, "GroupNFF")
                                                .WithDescription(this.InstanceName)
                                                .Build();

                                            if (this.FileOutputFolder != null)
                                            {
                                                dailyJobDetail.JobDataMap["OutputFolder"] = this.FileOutputFolder;
                                            }

                                            dailyJobDetail.JobDataMap["InsertDatabase"] = this.PopulateDatabase;
                                            dailyJobDetail.JobDataMap["ScheduleType"] = this.ScheduleType.ToString();
                                            dailyJobDetail.JobDataMap["ContentType"] = this.ContentType;
                                            dailyJobDetail.JobDataMap["FixtureType"] = this.FixtureType;
                                            dailyJobDetail.JobDataMap["Results"] = this._result;
                                            dailyJobDetail.JobDataMap["OperationMode"] = this.operationMode.ToString();
                                            dailyJobDetail.JobDataMap["ScheduleMessage"] = scheduleMessage;
                                            dailyJobDetail.JobDataMap["Duration"] = duration;
                                            dailyJobDetail.JobDataMap["Purge"] = purge;
                                            dailyJobDetail.JobDataMap["CreateXML"] = this.CreateXml;
                                            dailyJobDetail.JobDataMap["DateOffset"] = dateTimeOffset;
                                            dailyJobDetail.JobDataMap["JobName"] = "job_" + this.InstanceName + "_" +
                                                                                   scheduleId;
                                            dailyJobDetail.JobDataMap["TotalSchedule"] = totalSchedule;
                                            dailyJobDetail.JobDataMap["SportId"] = this.SportId;

                                            if (dailyJobDetail == null)
                                            {
                                                continue;
                                            }
                                            Logger.Debug("Setting up and starting dailyJobDetail job " +
                                                         dailyJobDetail.Description +
                                                         " using trigger : " + dailyCronTrigger.Description);

                                            this.Scheduler.ScheduleJob(dailyJobDetail, dailyCronTrigger);
                                        }

                                        break;

                                        #region ScheduleType.Weekly

                                    case ScheduleType.Weekly:
                                        foreach (XmlNode node in nodeList)
                                        {
                                            scheduleId = node.Attributes["Id"].Value;

                                            // Splitting the time into two values so that we can create a cron job
                                            var scheduleTimeArray =
                                                node.Attributes["Time"].Value.Split(':');

                                            // If minutes contains two zeros (00), we change it to one zero (0) 
                                            // If not, scheduler won't understand
                                            if (scheduleTimeArray[1] == "00")
                                            {
                                                scheduleTimeArray[1] = "0";
                                            }

                                            var minutes = Convert.ToInt32(scheduleTimeArray[1]);

                                            // Doing the same thing for hours
                                            if (scheduleTimeArray[0] == "00")
                                            {
                                                scheduleTimeArray[0] = "0";
                                            }

                                            var hours = Convert.ToInt32(scheduleTimeArray[0]);

                                            // Checking if there is a setting for results
                                            if (node.Attributes["Results"] != null)
                                            {
                                                this._result = Convert.ToBoolean(node.Attributes["Results"].Value);
                                                scheduleMessage = !this._result;
                                            }

                                            // Checking if there is a setting for Duration
                                            if (node.Attributes["Duration"] != null)
                                            {
                                                duration = Convert.ToInt32(node.Attributes["Duration"].Value);
                                            }

                                            var scheduleWeek = string.Empty;

                                            if (node.Attributes["Week"] != null)
                                            {
                                                scheduleWeek =
                                                    node.Attributes["Week"].Value;
                                            }

                                            var scheduleDay = string.Empty;
                                            if (node.Attributes["DayOfWeek"] != null)
                                            {
                                                scheduleDay = node.Attributes["DayOfWeek"].Value;
                                            }

                                            var numDayOfWeek = NumDayOfWeek();

                                            // Checking if numDayOfWeek is smaller than ScheduleDay
                                            DateTimeOffset dto = DateTime.Today;

                                            // Get the scheduled day
                                            var scheduledDay = Convert.ToInt32(scheduleDay);

                                            int days;

                                            if (numDayOfWeek <= scheduledDay)
                                            {
                                                // It is, so we need to find out when the next scheduling should happen
                                                days = scheduledDay - numDayOfWeek; // this can be zero

                                                dto = DateTime.Today.AddDays(days)
                                                    .AddHours(hours)
                                                    .AddMinutes(minutes)
                                                    .ToUniversalTime();
                                            }
                                            else if (numDayOfWeek > scheduledDay)
                                            {
                                                const int weekdays = 7;
                                                var daysLeft = weekdays - numDayOfWeek;
                                                days = daysLeft + scheduledDay;

                                                dto = DateTime.Today.AddDays(days)
                                                    .AddHours(hours)
                                                    .AddMinutes(minutes)
                                                    .ToUniversalTime();
                                            }

                                            // Creating a simple Scheduler
                                            var calendarIntervalSchedule =
                                                CalendarIntervalScheduleBuilder.Create();

                                            // Creating the weekly Trigger using the stuff that we've calculated
                                            weeklyTrigger = TriggerBuilder.Create()
                                                .WithDescription(this.InstanceName)
                                                .WithIdentity(this.InstanceName + "_" + scheduleId, "GroupNFF")
                                                .StartAt(dto)
                                                .WithSchedule(
                                                    calendarIntervalSchedule.WithIntervalInWeeks(
                                                        Convert.ToInt32(scheduleWeek)))
                                                .Build();

                                            // This part might be moved
                                            if (OperationMode.Gatherer == this.operationMode)
                                            {
                                                weeklyJobDetail = JobBuilder.Create<NffJobBuilder>()
                                                    .WithIdentity("job_" + this.InstanceName + "_" + scheduleId,
                                                        "GroupNFF")
                                                    .WithDescription(this.InstanceName)
                                                    .Build();
                                            }
                                            else if (this.operationMode == OperationMode.Purge)
                                            {
                                                weeklyJobDetail = JobBuilder.Create<PurgeSportsData>()
                                                    .WithIdentity("job_" + this.InstanceName + "_" + scheduleId,
                                                        "GroupNFF")
                                                    .WithDescription(this.InstanceName)
                                                    .Build();
                                            }
                                            else if (this.operationMode == OperationMode.Update)
                                            {
                                                weeklyJobDetail = JobBuilder.Create<PurgeSportsData>()
                                                    .WithIdentity("job_" + this.InstanceName + "_" + scheduleId,
                                                        "GroupNFF")
                                                    .WithDescription(this.InstanceName)
                                                    .Build();
                                            }
                                            else if (this.operationMode == OperationMode.Distributor)
                                            {
                                                // Nothing to do here
                                            }
                                            else if (this.OperationMode == OperationMode.Hold)
                                            {
                                                // Nothing to do here
                                            }

                                            if (this.FileOutputFolder != null || this.FileOutputFolder != string.Empty)
                                            {
                                                weeklyJobDetail.JobDataMap["OutputFolder"] = this.FileOutputFolder;
                                            }

                                            weeklyJobDetail.JobDataMap["InsertDatabase"] = this.PopulateDatabase;
                                            weeklyJobDetail.JobDataMap["ScheduleType"] = this.ScheduleType.ToString();
                                            weeklyJobDetail.JobDataMap["ContentType"] = this.ContentType;
                                            weeklyJobDetail.JobDataMap["FixtureType"] = this.FixtureType;
                                            weeklyJobDetail.JobDataMap["Results"] = this._result;
                                            weeklyJobDetail.JobDataMap["ScheduleMessage"] = scheduleMessage;
                                            weeklyJobDetail.JobDataMap["Duration"] = duration;
                                            weeklyJobDetail.JobDataMap["Purge"] = purge;
                                            weeklyJobDetail.JobDataMap["CreateXML"] = this.CreateXml;
                                            weeklyJobDetail.JobDataMap["JobName"] = "job_" + this.InstanceName + "_" +
                                                                                    scheduleId;
                                            weeklyJobDetail.JobDataMap["SportId"] = this.SportId;
                                        }

                                        if (weeklyJobDetail != null)
                                        {
                                            Logger.Debug("Setting up and starting weeklyJobDetail job " +
                                                         weeklyJobDetail.Description +
                                                         " using trigger : " + weeklyTrigger.Description);
                                            this.Scheduler.ScheduleJob(weeklyJobDetail, weeklyTrigger);
                                        }

                                        break;

                                        #endregion

                                    case ScheduleType.Monthly:
                                        // doing Monthly stuff here
                                        throw new NotSupportedException("Monthly scheduletype is not implemented!");
                                    case ScheduleType.Yearly:
                                        // doing yearly Stuff here
                                        throw new NotSupportedException("Yearly scheduletype is not implemented!");
                                }

                                // We might move the code regarding jobdetails here... 

                                // Configuring the datafile-object so that we know that the databases are up and running

                                // We don't have to check for datafile stuff when we only do purge...
                                if (this.operationMode != OperationMode.Purge)
                                {
                                    var configurator = new DataFileConfigurator();
                                    configurator.SportId = this.SportId;
                                    
                                    if (this.PopulateDatabase)
                                    {
                                        if (configurator.Configure() == false)
                                        {
                                            throw new SoccerGathererComponentException("Problems initiating database tables");
                                        }
                                    }

                                    this.DatabasePopulated = true;
                                }

                                // Catching exceptions that might occur
                            }
                            catch (SchedulerConfigException sce)
                            {
                                Logger.Debug("A SchedulerConfigException has occured: ", sce);
                            }
                            catch (SchedulerException se)
                            {
                                Logger.Debug("A schedulerException has occured: ", se);
                            }
                            catch (Exception e)
                            {
                                Logger.Debug("An exception has occured", e);
                            }

                            break;


                        case PollStyle.Scheduled:
                            /* 
                            schedule = configNode.Attributes["Schedule"].Value;
                            TimeSpan s = Utilities.GetScheduleInterval(schedule);
                            logger.DebugFormat("Schedule: {0} Calculated time to next run: {1}", schedule, s);
                            pollTimer.Interval = s.TotalMilliseconds;
                            */
                            throw new NotSupportedException("Invalid polling style for this job type");

                            #region PollStyle.FileSystemWatch

                        case PollStyle.FileSystemWatch:
                            // Check for config overrides
                            /*
                            if (configNode.Attributes.GetNamedItem("BufferSize") != null)
                                bufferSize = Convert.ToInt32(configNode.Attributes["BufferSize"].Value);
                            logger.DebugFormat("FileSystemWatcher buffer size: {0}", bufferSize);

                            //Set values
                            filesWatcher.Path = fileInputFolder;
                            filesWatcher.Filter = fileFilter;
                            filesWatcher.IncludeSubdirectories = includeSubdirs;
                            filesWatcher.InternalBufferSize = bufferSize;

                            //Do not start the event watcher here, wait until after the startup cleaning job
                            pollTimer.Interval = 5000; // short startup;
                            */
                            throw new NotSupportedException("Invalid polling style for this job type");

                            #endregion

                        default:
                            // Unsupported pollstyle for this object
                            throw new NotSupportedException("Invalid polling style for this job type (" + this.pollStyle +
                                                            ")");
                    }
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException(
                        "Invalid or missing PollStyle-specific values in XML configuration node: " + ex.Message, ex);
                }
            }

            // Finish configuration
            ThreadContext.Stacks["NDC"].Pop();
            this.Configured = true;
        }

        /// <summary>
        ///     The start.
        /// </summary>
        /// <exception cref="SportsDataException">
        ///     Sports Data Exception if something goes wrong
        /// </exception>
        public void Start()
        {
            ServicePointManager.ServerCertificateValidationCallback =
                ValidateServerCertificate;

            if (!this.Configured)
            {
                throw new SportsDataException(
                    "NFFWSComponent is not properly Configured. NFFWSComponent::Start() Aborted!");
            }

            if (!this.enabled)
            {
                throw new SportsDataException("NFFWSComponent is not Enabled. NFFWSComponent::Start() Aborted!");
            }

            // Whatever we do, we must check two database-tables and see if they are populated or not
            // These are:
            // Matches

            // Creating the NffSportsDataDatabase object
            var matchRepository = new MatchRepository();

            var numberOfMatches = matchRepository.GetNumberofMatches(this.SportId);

            if (numberOfMatches == 0)
            {
                Logger.Info("No matches in database. Needs to be populated!");
            }

            // Starting up the different pollstyles
            try
            {
                // Continous mode means that we are checking on an interval
                if (this.pollStyle == PollStyle.Continous)
                {
                    // Not sure if we have to do anything
                    Logger.Info("Starting Continous PollStyle component");
                    this.pollTimer.Enabled = true;
                }

                if (this.pollStyle == PollStyle.CalendarPoll)
                {
                    Logger.Info("Setting up jobListener and scheduler");

                    // starting the scheduler
                    var nffScheduler = this.SchedulerFactory.GetScheduler();

                    var jobGroupNames = nffScheduler.GetJobGroupNames();

                    foreach (var jobGroupName in jobGroupNames)
                    {
                        Logger.Debug("JobGroupName: " + jobGroupName);

                        if (jobGroupName.ToLower() != "groupnff") continue;
                        Logger.Debug("JobGroupName: " + jobGroupName);

                        Logger.Debug("Key: " + nffScheduler.GetJobKeys(GroupMatcher<JobKey>.GroupContains(jobGroupName)));

                        var groupMatcher = GroupMatcher<JobKey>.GroupContains(jobGroupName);
                        var jobKeys = nffScheduler.GetJobKeys(groupMatcher);

                        foreach (var jobKey in jobKeys)
                        {
                            Logger.Debug("Name: " + jobKey.Name);
                            Logger.Debug("Group: " + jobKey.Group);
                        }
                    }

                    var triggerGroupNames = nffScheduler.GetTriggerGroupNames();

                    foreach (var triggerGroupName in triggerGroupNames)
                    {
                        Logger.Debug("TriggerGroupName: " + triggerGroupName);
                    }

                    var jobs = nffScheduler.GetJobGroupNames();
                    foreach (var job in jobs)
                    {
                        Logger.Debug("Job: " + job);
                    }

                    Logger.Debug("This Component name: " + this.Name);

                    // Starting scheduler
                    nffScheduler.Start();
                }

                if (this.pollStyle == PollStyle.PushSubscribe)
                {
                    Logger.Debug("Running BackgroundWorker, checking for Configured state");
                    if (this.Configured)
                    {
                        // Extract History on successful Connection
                        var pubnubHistoryMessages =
                            Convert.ToInt32(ConfigurationManager.AppSettings["pubnub_history_items"]);

                        Logger.Info("Getting history - latest " + pubnubHistoryMessages);
                        var pubnubMessage = this._pubnub.History(this._channel, pubnubHistoryMessages);

                        foreach (var message in pubnubMessage)
                        {
                            Logger.Debug(message.MessageType);
                            this.PushCallback(message);
                        }

                        Logger.Info("Done calling history (latest " + pubnubHistoryMessages + ")");

                        Logger.Info("Calling PubNubNFF");

                        Logger.Info("Starting Backgroundworker");
                        this.backgroundWorker1 = new BackgroundWorker
                        {
                            WorkerReportsProgress = true,
                            WorkerSupportsCancellation = true
                        };
                        this.backgroundWorker1.ProgressChanged += this.backgroundWorker1_ProgressChanged;

                        this.backgroundWorker1.DoWork += this.backgroundWorker1_DoWork;
                        if (!this.backgroundWorker1.IsBusy)
                        {
                            this.backgroundWorker1.RunWorkerAsync();
                        }

                        // pubnub.subscribe
                        Logger.Info("Done calling PubNubNFF");
                    }
                }
            }
            catch (SchedulerException se)
            {
                Logger.Debug(se);
            }

            this._stopEvent.Reset();
            this._busyEvent.Set();

            // Starting poll-timer
            // pollTimer.Interval = 2000;

            // Setting ComponentState to running
            this.ComponentState = ComponentState.Running;

            this.pollTimer.Start();
        }

        /// <summary>
        ///     The get component state.
        /// </summary>
        /// <returns>
        ///     The <see cref="ComponentState" />.
        /// </returns>
        public ComponentState GetComponentState(bool componentState)
        {
            return ComponentState.Running;
        }

        /// <summary>
        ///     The stop.
        /// </summary>
        /// <exception cref="SportsDataException">
        ///     If an error occurs, a Sports Data Exception will fire
        /// </exception>
        public void Stop()
        {
            if (!this.Configured)
            {
                throw new SportsDataException("NFFWSComponent is not properly Configured. NFFComponent::Stop() Aborted");
            }

            if (!this.enabled)
            {
                throw new SportsDataException("NFFWSComponent is not properly Configured. NFFComponent::Stop() Aborted");
            }

            #region DEBUG

            Logger.Debug(this.pollTimer.Enabled == false ? "pollTimer is disabled" : "pollTimer is Enabled");

            const double epsilon = 0;
            Logger.Debug(Math.Abs(this.pollTimer.Interval - 5000) < epsilon
                ? "pollTimer has an interval of 5000 milliseconds"
                : "pollTimer DOES NOT have an interval of 5000 milliseconds");
            Logger.Debug(this.ErrorRetry ? "We are in ErrorRetry mode!" : "We are NOT in ErrorRetry mode!");

            Logger.Debug(this.pollStyle == PollStyle.FileSystemWatch
                ? "pollStyle is of style FileSystemWatch"
                : "pollStyle is NOT of style FileSystemWatch");

            #endregion

            #region Stopping Scheduler

            // Check status - Handle busy polltimer loops
            if (this.pollStyle == PollStyle.CalendarPoll)
            {
                try
                {
                    Logger.Info("Deleting / stopping jobs!");
                    ISchedulerFactory sf = new StdSchedulerFactory();
                    var sched = sf.GetScheduler();

                    // Shutting down scheduler
                    sched.GetJobGroupNames();

                    var keys = new
                        List<JobKey>(sched.GetJobKeys(GroupMatcher<JobKey>.GroupEquals(this.InstanceName)));

                    // Looping through each key as we'd like to know when things go right and not!
                    keys.ForEach(key =>
                    {
                        var detail = sched.GetJobDetail(key);
                        sched.DeleteJob(key);

                        Logger.Info("Shutting down: " + detail.Description + "(" + key.Name + ")" + " Group: " +
                                    key.Group);
                    });

                    sched.Shutdown();
                }
                catch (SchedulerException sex)
                {
                    Logger.Fatal(sex);
                }
                catch (Exception ex)
                {
                    Logger.Error("Problems closing NFF WebClients!", ex);
                }
            }

            #endregion

            if (this.pollStyle == PollStyle.PushSubscribe)
            {
                /**
                 * We must try and work in the background-wroker to see if we can use this to make getting messages async.
                 * 
                 */

                Logger.Info("Stopping Backgroundworker");

                // Displosing background worker
                this.backgroundWorker1.Dispose();
            }

            if (!this.pollTimer.Enabled && (this.ErrorRetry || this.pollStyle !=
                                            PollStyle.FileSystemWatch ||
                                            Math.Abs(this.pollTimer.Interval - 5000) < epsilon))
            {
                Logger.InfoFormat("Waiting for instance to complete work. Job {0}", this.InstanceName);
            }

            // Signal Stop
            this._stopEvent.Set();

            // Stop events
            if (this.filesWatcher != null)
            {
                this.filesWatcher.EnableRaisingEvents = false;

                if (!this._busyEvent.WaitOne(30000))
                {
                    Logger.InfoFormat(
                        "Instance did not complete properly. Data may be inconsistent. Job: {0}", this.InstanceName);
                }
            }
            this.ComponentState = ComponentState.Halted;

            // Kill polling
            this.pollTimer.Stop();
        }

        #region Certificate

        /**
         * This region contains functions and other information that is needed to 
         * connect to the test and probably also the stage webservice-server
         */

        /// <summary>
        ///     The validate server certificate.
        /// </summary>
        /// <param name="sender">
        ///     The sender.
        /// </param>
        /// <param name="certificate">
        ///     The certificate.
        /// </param>
        /// <param name="chain">
        ///     The chain.
        /// </param>
        /// <param name="sslPolicyErrors">
        ///     The ssl policy errors.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        public static bool ValidateServerCertificate(
            object sender,
            X509Certificate certificate,
            X509Chain chain,
            SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        #endregion

        /// <summary>
        ///     The num day of week.
        /// </summary>
        /// <returns>
        ///     The <see cref="int" />.
        /// </returns>
        private static int NumDayOfWeek()
        {
            // If we use the DateTimeOffset it means that we have to find out if the day has passed or not.
            // So we have to find out which day it is today
            var today = DateTime.Today.DayOfWeek;
            var numDayOfWeek = 0;
            switch (today.ToString())
            {
                case "Monday":
                    numDayOfWeek = 1;
                    break;
                case "Tuesday":
                    numDayOfWeek = 2;
                    break;

                case "Wednesday":
                    numDayOfWeek = 3;
                    break;

                case "Thursday":
                    numDayOfWeek = 4;
                    break;

                case "Friday":
                    numDayOfWeek = 5;
                    break;

                case "Saturday":
                    numDayOfWeek = 6;
                    break;

                case "Sunday":
                    numDayOfWeek = 0;
                    break;
            }

            return numDayOfWeek;
        }

        /// <summary>
        ///     The background worker 1_ progress changed.
        /// </summary>
        /// <param name="sender">
        ///     The sender.
        /// </param>
        /// <param name="e">
        ///     The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        ///     this method is not implemented, and therefor a Not Implemented Exception will occur - it will throw such an
        ///     exception
        /// </exception>
        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Logger.Info("backgroundWorker1_ProgressChanged - Not implemented");
            throw new NotImplementedException();
        }

        /// <summary>
        ///     The poll timer_ elapsed.
        /// </summary>
        /// <param name="sender">
        ///     The sender.
        /// </param>
        /// <param name="e">
        ///     The e.
        /// </param>
        private void pollTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            /*
             * We can consider adding getting data here if the time (day + hour + minute) hits. Otherwize we shall do nothing.
             * 
             */
            ThreadContext.Properties["JOBNAME"] = this.InstanceName;

            Logger.Debug("NFFGathererComponent::pollTimer_Elapsed() hit");
            Logger.Debug("We have the following settings: Interval: " + this.pollTimer.Interval);

            this._busyEvent.WaitOne();
            this.pollTimer.Stop();

            switch (this.pollStyle)
            {
                // For continuous and scheduled do a simple folder item traversal
                case PollStyle.Scheduled:
                case PollStyle.Continous:
                    if (this.operationMode == OperationMode.Distributor)
                    {
                        // We aren't really doing anything here, and I am not sure if we will either
                        // Consider moving this check
                    }

                    if (this.operationMode == OperationMode.Maintenance)
                    {
                        // We are to gather data
                        // Is checking the database for matches not found a gatherer? 
                        switch (this.maintenanceMode)
                        {
                            case MaintenanceMode.CheckMatches:
                            {
                                var maintentance = new Router();
                                maintentance.CheckMissingMatches();
                            }

                                break;
                            case MaintenanceMode.CheckAllMatches:
                            {
                                var maintentance = new Router();
                                maintentance.CheckMissingMatches();
                            }

                                break;
                        }
                    }

                    if (this.operationMode == OperationMode.Gatherer)
                    {
                        var model = new DataFileModel();
                        model.OutputFolder = this.FileOutputFolder;
                        model.JobName = this.Name;
                        model.SportId = this.SportId;
                        
                        Logger.Debug("Outputfolder: " + this.FileOutputFolder);

                        if (this.PopulateDatabase)
                        {
                            Logger.Debug("We shall insert data in the database");
                            model.UpdateDatabase = true;
                        }

                        // This parameter tells us if we are to add results-tags to the XML
                        Logger.Debug("We shall also render result");
                        model.RenderResult = true;

                        if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                        {
                            model.DateStart = DateTime.Parse(ConfigurationManager.AppSettings["nfftestingdate"]);
                            model.DateEnd = DateTime.Parse(ConfigurationManager.AppSettings["nfftestingdate"]);
                        }
                        else
                        {
                            model.DateStart = DateTime.Today;
                            model.DateEnd = DateTime.Today;

                            Logger.Debug("Date Start: " + model.DateStart);
                            Logger.Debug("Date End: " + model.DateEnd);
                        }

                        Logger.Debug("We are about to create XML File");
                        model.CreateXml = this.CreateXml;

                        var dataFileCreator = new DistrictDataFileCreator();
                        dataFileCreator.Model = model;
                        dataFileCreator.CreateDataFileByDistrict();
                    }
    
                    this.pollTimer.Start();

                    break;

                case PollStyle.CalendarPoll:

                    // @done: need to do some work here
                    Logger.Debug("Disabling pollTimer");
                    this.pollTimer.Enabled = false;

                    break;

                case PollStyle.PushSubscribe:

                    // Subscriber key =             sub-4fb246b0-982a-11e1-ad0b-e19db246ca40
                    // Staging channel =          THE_FIKS_STAGING_CHANNEL

                    // Done: We need to check the service state
                    this.pollTimer.Start();
                    break;

                /*
                 * M� legge inn i databasen om denne turneringen er push eller pull enablet.
                 * Sjekke om vi skal bruke den andre l�sningen for push...
                 * Anders g�r over om dagens nye l�sning er 1:1 gammel l�sning.
                 */
            }

            this._busyEvent.Set();
        }

        #region PushCallback function

        /// <summary>
        ///     The push callback.
        /// </summary>
        /// <param name="message">
        ///     The message.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        private bool PushCallback(PubNubMessage message)
        {
            
            ThreadContext.Properties["JOBNAME"] = this.InstanceName;
            Logger.Info("Running callback method!");

            Logger.Info("MessageType: " + message.MessageType);
            Logger.Info("ObjectId: " + message.ObjectId);
            Logger.Info("ObjectType: " + message.ObjectType);
            Logger.Debug("TimeStamp: " + message.TimeStamp);

            // TODO: Work more on the JSON deserializion... Chose the ServiceStack.Text library
            // var jsonObj = JsonSerializer.DeserializeFromString<Dictionary<string, object>>>(message);

            // Consider doing this as a method on it's own, if it is an object, we can call the same method...
            int matchId;
            var model = new DataFileModel();

            switch (message.MessageType.ToLower())
            {
                case "update":

                    var matchRepository = new MatchRepository();
                    var tournamentRepository = new TournamentRepository();
                    

                    switch (message.ObjectType)
                    {
                        case "Match":
                            matchId = Convert.ToInt32(message.ObjectId);
                            if (matchId > 0)
                            {
                                matchRepository.GetMatchByMatchIdAndSportId(matchId, this.SportId);

                            }

                            break;

                        case "Tournament":
                            var tournamentId = Convert.ToInt32(message.ObjectId);
                            if (tournamentId > 0)
                            {
                                var tournament = tournamentRepository.Get(tournamentId);
                                if (tournament.Id == 0)
                                {
                                    // we need to get the tournament information
                                    var facade = new SoccerFacade();
                                    // Getting the Tournament Object based on the TournamentId
                                    tournament = facade.GetTournamentById(tournamentId);
                                    if (tournament != null)
                                    {
                                        tournament.SportId = this.SportId;
                                        tournament.DisciplineId = 16;
                                        tournamentRepository.InsertOne(tournament);
                                    }
                                }
                                else
                                {
                                    tournament.SportId = this.SportId;
                                    tournament.DisciplineId = 16;
                                    tournamentRepository.Update(tournament);
                                }
                            }

                            break;

                        case "MatchResult":
                            foreach (var kvp in message.Properties)
                            {
                                Logger.InfoFormat("key: {0} - value: {1}", kvp.Key, kvp.Value);

                                // I don't bother with the result, as I am to get that from calling the NFF WebService
                                if (kvp.Key.ToLower() != "matchid")
                                {
                                    continue;
                                }

                                matchId = Convert.ToInt32(kvp.Value);

                                // We are to use the matchId to change the data in the database
                                if (matchId <= 0)
                                {
                                    continue;
                                }

                                // Now we create the datafile from MatchId
                                Logger.Info("We are calling the Push method to get and create the datafile");
                                
                                model.MatchId = matchId;

                                model.OutputFolder = this.FileOutputFolder;
                                var outputFolders = new List<string>()
                                {
                                    this.FileOutputFolder
                                };
                                model.FileOutputFolder = outputFolders;

                                var creator = new PushDatafileCreator();
                                creator.MatchId = matchId;
                                creator.Model = model;
                                creator.CreateDataFileFromPush(this.SportId);
                            }

                            break;
                    }

                    break;

                case "insert":
                    matchId = 0;
                    var homeTeamGoals = 0;
                    var awayTeamGoals = 0;
                    
                    model.RenderResult = true;

                    if (message.ObjectType.ToLower() == "matchresult")
                    {
                        // Consider adding hometeamgoals and awayteamgoals. This so we can use that information to loop over in order to get the result
                        foreach (var kvp in message.Properties)
                        {
                            Logger.Info("key: " + kvp.Key + " - value: " + kvp.Value);

                            if (kvp.Key.ToLower() == "hometeamgoals")
                            {
                                homeTeamGoals = Convert.ToInt32(kvp.Value);
                            }

                            if (kvp.Key.ToLower() == "awayteamgoals")
                            {
                                awayTeamGoals = Convert.ToInt32(kvp.Value);
                            }
                            // I don't bother with the result, as I am to get that from calling the NFF WebService
                            if (kvp.Key.ToLower() != "matchid")
                            {
                                continue;
                            }

                            matchId = Convert.ToInt32(kvp.Value);

                            Logger.Info("MatchId: " + matchId);
                        }

                        /*
                         * First we find out from which tournament this result is in. If anyone needs these
                         * results we shall continue, if not we shall exit. 
                         * 
                         * We start by using the MatchID to find the tournament, which we either get from the database
                         * or we get it from the service.
                         */

                        /* 
                        * What we shall do now is to get the result, then get the table standings 
                        * Then we find out if there are anyone out
                        */

                        // Now we create the datafile from MatchId
                        Logger.Info("We are calling the Push method to get and create the datafile");
                        model.MatchId = matchId;
                        model.HomeTeamGoals = homeTeamGoals;
                        model.AwayTeamGoals = awayTeamGoals;

                        model.OutputFolder = this.FileOutputFolder;

                        var creator = new PushDatafileCreator();
                        creator.Model = model;
                        creator.CreateDataFileFromPush(this.SportId);
                    }

                    break;
            }

            Logger.Info("Done running callback method!");

            Logger.Info("Setting keep listening to true!");

            
            return true;
        }

        #endregion

        /// <summary>
        ///     The background worker 1_ do work.
        /// </summary>
        /// <param name="sender">
        ///     The sender.
        /// </param>
        /// <param name="e">
        ///     The e.
        /// </param>
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            
            ThreadContext.Properties["JOBNAME"] = this.InstanceName;

            Logger.Debug("Running BackgroundWorker, checking for Configured state");
            if (!this.Configured) return;
            var worker = sender as BackgroundWorker;

            while (worker != null && !worker.CancellationPending)
            {
                Logger.Info("Calling PubNubNFF");

                // Setting up the call to pubnub
                this._pubnub.Subscribe(this._channel, this.PushCallback);

                Logger.Info("Done calling PubNubNFF");
            }

            
        }

/*
        /// <summary>
        /// The background worker 1_ run worker completed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Logger.Info("Backgroundworker run completed!");

            // Done: Find out if we are in a closing state
            backgroundWorker1.RunWorkerAsync();
        }
*/

        /// <summary>
        ///     The trust all certificate policy.
        /// </summary>
        public class TrustAllCertificatePolicy : ICertificatePolicy
        {
            /// <summary>
            ///     The check validation result.
            /// </summary>
            /// <param name="sp">
            ///     The sp.
            /// </param>
            /// <param name="cert">
            ///     The cert.
            /// </param>
            /// <param name="req">
            ///     The req.
            /// </param>
            /// <param name="problem">
            ///     The problem.
            /// </param>
            /// <returns>
            ///     The <see cref="bool" />.
            /// </returns>
            public bool CheckValidationResult(ServicePoint sp, X509Certificate cert, WebRequest req, int problem)
            {
                return true;
            }
        }

        #region Polling settings

        /// <summary>
        ///     schedulerFactory to be used to create scheduled tasks
        /// </summary>
        protected StdSchedulerFactory SchedulerFactory = new StdSchedulerFactory();

        /// <summary>
        ///     The scheduler.
        /// </summary>
        protected IScheduler Scheduler;

        /// <summary>
        ///     The Configured interval for this instance, used for Continous polling
        /// </summary>
        /// <remarks>
        ///     <para>Indicates the interval time in seconds for <c>Continous</c>polling. 60 means that the job runs every minute.</para>
        ///     <para>Default value: <c>60</c></para>
        /// </remarks>
        protected int Interval = 60;

        /// <summary>
        ///     The testing tournament.
        /// </summary>
        protected bool TestingTournament = Convert.ToBoolean(ConfigurationManager.AppSettings["TestingTournament"]);

        /// <summary>
        ///     The testing tournament id.
        /// </summary>
        protected int TestingTournamentId = Convert.ToInt32(ConfigurationManager.AppSettings["TestingTournamentId"]);

        #endregion

        #region File folders

        /// <summary>
        ///     Output file folder
        /// </summary>
        /// <remarks>
        ///     File folder where files shall be plased after being created.
        /// </remarks>
        protected string FileOutputFolder;

        /// <summary>
        ///     Error file folder
        /// </summary>
        /// <remarks>
        ///     <para>File folder where failing files are stored.</para>
        ///     <para>If this is not set, failing files are deleted instead of saved.</para>
        /// </remarks>
        protected string FileErrorFolder;

        /// <summary>
        ///     File folder where completed files are stored
        /// </summary>
        /// <remarks>
        ///     <para>File folder where completed files are archived. Subdirectories for years, months and dates are created.</para>
        ///     <para>If this is not set, imported files are deleted instead of archived.</para>
        /// </remarks>
        protected string FileDoneFolder;

        #endregion

        #region Push_PubNub

        /// <summary>
        ///     The _pubnub.
        /// </summary>
        private PubnubNff _pubnub;

        /// <summary>
        ///     The _channel.
        /// </summary>
        private string _channel = string.Empty;

        /// <summary>
        ///     The Populate Database field - tells if the job shall populate the database or not
        /// </summary>
        protected bool PopulateDatabase;

        /// <summary>
        ///     The Create XML field - tells if we are to create an XML file or not
        /// </summary>
        protected bool CreateXml;

        #endregion Push_PubNub

        #region Common Components variables (no need to check)

        /// <summary>
        ///     Gets the name of the instance.
        /// </summary>
        /// <value>The name of the instance.</value>
        /// <remarks>The Configured instance job name.</remarks>
        public string InstanceName
        {
            get { return this.Name; }
        }

        /// <summary>
        ///     Gets the operation mode.
        /// </summary>
        /// <value>The operation mode.</value>
        /// <remarks><c>Gatherer</c> is the only valid mode, its hard coded for this component.</remarks>
        public OperationMode OperationMode
        {
            get { return OperationMode.Gatherer; }
        }

        /// <summary>
        ///     Gets the poll style.
        /// </summary>
        /// <value>The poll style.</value>
        /// <remarks>Contionous, Scheduled and FileSystemWatch are valid for <c>Gatherer</c> objects.</remarks>
        public PollStyle PollStyle
        {
            get { return this.pollStyle; }
        }

        #endregion
    }
}