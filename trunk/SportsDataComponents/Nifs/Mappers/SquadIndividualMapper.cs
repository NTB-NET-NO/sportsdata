﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SquadIndividualMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using Glue;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Mappers;
using NTB.SportsData.Components.NIFConnectService;

namespace NTB.SportsData.Components.Nifs.Mappers
{
    /// <summary>
    /// The squad individual mapper.
    /// </summary>
    public class SquadIndividualMapper : BaseMapper<SquadIndividual, Player>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<SquadIndividual, Player> mapper)
        {
            mapper.Relate(x => x.MatchId, y => y.MatchId, this.NullableIntToIntConverter());
            mapper.Relate(x => x.FirstName, y => y.FirstName);
            mapper.Relate(x => x.LastName, y => y.LastName);
            mapper.Relate(x => x.IsCaptain, y => y.TeamCaptain, this.BoolToStringConverter());
            mapper.Relate(x => x.IsDebutant, y => y.Debutant, this.BoolToStringConverter());
            mapper.Relate(x => x.HasPlayed, y => y.Played, this.BoolToStringConverter());
            mapper.Relate(x => x.PersonId, y => y.Id);
            mapper.Relate(x => x.OrgId, y => y.TeamId, this.NullableIntToIntConverter());
            mapper.Relate(x => x.StartingLineup, y => y.StartingLineUp, this.BoolToStringConverter());
            mapper.Relate(x => x.ShirtNo, y => y.PlayerShirtNumber);
            mapper.Relate(x => x.PlayerOfTheMatch, y => y.PlayerOfTheMatch, this.BoolToStringConverter());
            mapper.Relate(x => x.SquadIndividualTypeId, y => y.PositionId, this.NullableIntToIntConverter());
            mapper.Relate(x => x.SquadIndividualTypeName, y => y.Position);
            mapper.Relate(x => x.SquadIndividualCategoryId, y => y.SquadIndividualCategoryId);
            mapper.Relate(x => x.SquadIndividualCategoryName, y => y.SquadIndividualCategoryName);
        }
    }
}