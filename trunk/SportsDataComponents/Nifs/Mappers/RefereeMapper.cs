﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RefereeMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using Glue;
using NTB.SportsData.Components.Mappers;
using NTB.SportsData.Components.NIFConnectService;

namespace NTB.SportsData.Components.Nifs.Mappers
{
    /// <summary>
    /// The referee mapper.
    /// </summary>
    public class RefereeMapper : BaseMapper<Referee, SportsData.Classes.Classes.Referee>
    {
        
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<Referee, SportsData.Classes.Classes.Referee> mapper)
        {
            mapper.Relate(x => x.Id, y => y.Id);
            mapper.Relate(x => x.RefereeName, y => y.RefereeName);
            mapper.Relate(x => x.RefereeType, y => y.RefereeType);
            mapper.Relate(x => x.OrgId, y => y.ClubId);
            mapper.Relate(x => x.OrgName, y => y.ClubName);
        }

    }
}