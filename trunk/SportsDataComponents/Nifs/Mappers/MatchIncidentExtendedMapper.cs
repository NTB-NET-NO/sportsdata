﻿using Glue;
using NTB.SportsData.Components.Mappers;
using NTB.SportsData.Components.NIFConnectService;

namespace NTB.SportsData.Components.Nifs.Mappers
{
    using MatchIncident = SportsData.Classes.Classes.MatchIncident;

    class MatchIncidentExtendedMapper : BaseMapper<MatchIncidentExtended, MatchIncident>
    {
        protected override void SetUpMapper(Mapping<MatchIncidentExtended, MatchIncident> mapper)
        {
            mapper.Relate(x => x.FirstName, y => y.FirstName);
            mapper.Relate(x => x.LastName, y=>y.LastName);
            mapper.Relate(x => x.IncidentShort, y => y.IncidentShort);
            mapper.Relate(x => x.IncidentType, y => y.IncidentType);
            mapper.Relate(x => x.IncidentTypeId, y => y.IncidentTypeId);
            mapper.Relate(x => x.MatchId, y => y.MatchId);
            mapper.Relate(x => x.Period, y => y.Period);
            mapper.Relate(x => x.Reason, y => y.Reason);
            mapper.Relate(x => x.Time, y => y.Time);
            mapper.Relate(x => x.Value, y => y.Value);
            
        }
    }
}
