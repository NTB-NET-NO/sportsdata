﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SportEventMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using Glue;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Mappers;
using NTB.SportsData.Components.NIFConnectService;

namespace NTB.SportsData.Components.Nifs.Mappers
{
    /// <summary>
    /// The sport event mapper.
    /// </summary>
    public class SportEventMapper : BaseMapper<EventSearchAdvancedResult, SportEvent>
    {
        #region Methods

        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<EventSearchAdvancedResult, SportEvent> mapper)
        {
            mapper.Relate(x => x.ActivityId, y => y.ActivityId);
            mapper.Relate(x => x.ActivityName, y => y.ActivityName);
            mapper.Relate(x => x.AdmOrgId, y => y.AdministrativeOrgId);
            mapper.Relate(x => x.AdministrationOrganizationName, y => y.AdministrativeOrgName);
            mapper.Relate(x => x.EventId, y => y.EventId);
            mapper.Relate(x => x.EventName, y => y.EventName);
            mapper.Relate(x => x.StartDate, y => y.EventDateStart);
            mapper.Relate(x => x.EndDate, y => y.EventDateEnd);
            mapper.Relate(x => x.VenueUnitName, y => y.VenueUnitName);
        }

        #endregion
    }
}