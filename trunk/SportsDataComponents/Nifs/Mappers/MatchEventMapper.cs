﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchIncidentTaMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using Glue;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Mappers;
using MatchIncident = NTB.SportsData.Components.NIFConnectService.MatchIncident;

namespace NTB.SportsData.Components.Nifs.Mappers
{
    /// <summary>
    /// The match incident ta mapper.
    /// </summary>
    public class MatchEventMapper : BaseMapper<MatchIncident, SportsData.Classes.Classes.MatchEvent>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<MatchIncident, MatchEvent> mapper)
        {
            mapper.Relate(x => x.MatchIncidentId, y => y.MatchEventId);
            mapper.Relate(x => x.IncidentShort, y => y.MatchEventShort);
            mapper.Relate(x => x.IncidentType, y => y.MatchEventType);
            mapper.Relate(x => x.IncidentTypeId, y => y.MatchEventTypeId);
            mapper.Relate(x => x.MatchId, y => y.MatchId);
            mapper.Relate(x => x.PersonId, y => y.PersonId);
            mapper.Relate(x => x.FirstName, y => y.FirstName);
            mapper.Relate(x => x.LastName, y => y.LastName);
            mapper.Relate(x => x.Time, y => y.Time);
            mapper.Relate(x => x.ParentId, y => y.ParentId);
            mapper.Relate(x => x.OrgId, y => y.TeamId, this.NullableIntToIntConverter());
            mapper.Relate(x => x.Value, y => y.Value);
        }
    }
}