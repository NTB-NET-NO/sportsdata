﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PartialResultMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The partial result mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Glue;
using NTB.SportsData.Components.Mappers;
using NTB.SportsData.Components.NIFConnectService;

namespace NTB.SportsData.Components.Nifs.Mappers
{
    /// <summary>
    /// The partial result mapper.
    /// </summary>
    public class PartialResultMapper : BaseMapper<PartialResult, SportsData.Classes.Classes.PartialResult>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<PartialResult, SportsData.Classes.Classes.PartialResult> mapper)
        {
            mapper.Relate(x => x.AwayGoals, y => y.AwayGoals);
            mapper.Relate(x => x.MatchId, y => y.MatchId);
            mapper.Relate(x => x.HomeGoals, y => y.HomeGoals);
            mapper.Relate(x => x.PartialMatchResult, y => y.PartialMatchResult);
            mapper.Relate(x => x.PartialResultId, y => y.PartialResultId);
            mapper.Relate(x => x.PartialResultTypeId, y => y.PartialResultTypeId);
        }
    }
}