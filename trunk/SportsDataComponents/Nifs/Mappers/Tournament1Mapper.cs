﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The tournament mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Glue;
using NTB.SportsData.Components.Mappers;
using NTB.SportsData.Components.NIFConnectService;
using Tournament = NTB.SportsData.Classes.Classes.Tournament;

namespace NTB.SportsData.Components.Nifs.Mappers
{
    /// <summary>
    /// The tournament mapper.
    /// </summary>
    public class Tournament1Mapper : BaseMapper<Tournament1, Tournament>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<Tournament1, Tournament> mapper)
        {
            mapper.Relate(x => x.TournamentId, y => y.Id);
            mapper.Relate(x => x.TournamentName, y => y.Name);
            mapper.Relate(x => x.SeasonId, y => y.SeasonId);
            mapper.Relate(x => x.Division, y => y.Division);
            mapper.Relate(x => x.TournamentTypeId, y => y.TournamentTypeId);
            mapper.Relate(x => x.PublicResult, y => y.PublishResult, this.NullableBoolToBoolConverter());
            mapper.Relate(x => x.PublicTable, y => y.PublishTournamentTable, this.NullableBoolToBoolConverter());
            mapper.Relate(x => x.ActivityId, y => y.DisciplineId);
        }
    }
}