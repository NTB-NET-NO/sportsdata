﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchIncidentTaMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using Glue;
using NTB.SportsData.Components.Mappers;
using NTB.SportsData.Components.NIFConnectService;

namespace NTB.SportsData.Components.Nifs.Mappers
{
    /// <summary>
    /// The match incident ta mapper.
    /// </summary>
    public class MatchIncidentTaMapper : BaseMapper<MatchIncident, SportsData.Classes.Classes.MatchIncident>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<MatchIncident, SportsData.Classes.Classes.MatchIncident> mapper)
        {
            mapper.Relate(x => x.MatchIncidentId, y => y.MatchIncidentId);
            mapper.Relate(x => x.IncidentType, y => y.IncidentType);
            mapper.Relate(x => x.IncidentTypeId, y => y.IncidentTypeId);
            mapper.Relate(x => x.IncidentSubType, y => y.IncidentSubType);
            mapper.Relate(x => x.IncidentSubTypeId, y => y.IncidentSubTypeId);
            mapper.Relate(x => x.MatchId, y => y.MatchId);
            mapper.Relate(x => x.PersonId, y => y.PersonId);
            mapper.Relate(x => x.FirstName, y => y.FirstName);
            mapper.Relate(x => x.LastName, y => y.LastName);
            mapper.Relate(x => x.Time, y => y.Time);
            mapper.Relate(x => x.ParentId, y => y.ParentId, this.NullableIntToIntConverter());
            mapper.Relate(x => x.OrgId, y => y.TeamId, this.NullableIntToIntConverter());
            mapper.Relate(x => x.Value, y => y.Value, this.NullableIntToIntConverter());
        }
    }
}