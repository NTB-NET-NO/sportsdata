﻿using Glue;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Mappers;

namespace NTB.SportsData.Components.Nifs.Mappers
{
    class FederationSeasonMapper : BaseMapper<NIFConnectService.Season, Season>
    {
        protected override void SetUpMapper(Mapping<NIFConnectService.Season, Season> mapper)
        {
            mapper.Relate(x => x.SeasonId, y => y.Id);
            mapper.Relate(x => x.SeasonName, y => y.Name);
            mapper.Relate(x => x.FromDate, y => y.StartDate);
            mapper.Relate(x => x.ToDate, y => y.EndDate);
            mapper.Relate(x => x.SeasonStatusId, y => y.StatusId);
        }
    }
}
