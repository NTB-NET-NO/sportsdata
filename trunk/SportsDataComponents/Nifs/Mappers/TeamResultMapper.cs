﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TeamResultMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using Glue;
using NTB.SportsData.Components.Mappers;
using NTB.SportsData.Components.NIFConnectService;

namespace NTB.SportsData.Components.Nifs.Mappers
{
    /// <summary>
    /// The team result mapper.
    /// </summary>
    public class TeamResultMapper : BaseMapper<TeamResult, SportsData.Classes.Classes.TeamResult>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<TeamResult, SportsData.Classes.Classes.TeamResult> mapper)
        {
            // General
            mapper.Relate(x => x.OrgId, y => y.OrgId);
            mapper.Relate(x => x.OrgName, y => y.OrgName);
            mapper.Relate(x => x.Dispensation, y => y.Dispensation);
            mapper.Relate(x => x.EntryId, y => y.EntryId);
            mapper.Relate(x => x.Sort, y => y.Sort);
            mapper.Relate(x => x.Position, y => y.Position);

            // Matches
            mapper.Relate(x => x.Matches, y => y.Matches);
            mapper.Relate(x => x.MatchesHome, y => y.MatchesHome);
            mapper.Relate(x => x.MatchesAway, y => y.MatchesAway);

            // Draws
            mapper.Relate(x => x.Draws, y => y.Draws);
            mapper.Relate(x => x.DrawsAway, y => y.DrawsAway);
            mapper.Relate(x => x.DrawsHome, y => y.DrawsHome);

            // Goals
            mapper.Relate(x => x.GoalDifference, y => y.GoalDifference);
            mapper.Relate(x => x.GoalsScoredAway, y => y.GoalsScoredAway);
            mapper.Relate(x => x.GoalsAwayFormatted, y => y.GoalsAwayFormatted);
            mapper.Relate(x => x.GoalRatio, y => y.GoalRatio);
            mapper.Relate(x => x.GoalsScored, y => y.GoalsScored);
            mapper.Relate(x => x.GoalsScoredHome, y => y.GoalsScoredHome);
            mapper.Relate(x => x.GoalsConcededAway, y => y.GoalsConcededAway);
            mapper.Relate(x => x.GoalsConceeded, y => y.GoalsConceeded);
            mapper.Relate(x => x.GoalsConcededHome, y => y.GoalsConcededHome);
            mapper.Relate(x => x.GoalsHomeFormatted, y => y.GoalsHomeFormatted);
            mapper.Relate(x => x.GoalsAwayFormatted, y => y.GoalsAwayFormatted);

            // Totals
            mapper.Relate(x => x.TotalGoals, y => y.TotalGoals);
            mapper.Relate(x => x.TotalGoalsFormatted, y => y.TotalGoalsFormatted);
            mapper.Relate(x => x.TotalPoints, y => y.TotalPoints);
            mapper.Relate(x => x.TotalMatches, y => y.TotalMatches);

            // Losses
            mapper.Relate(x => x.Losses, y => y.Losses);
            mapper.Relate(x => x.LossesAway, y => y.LossesAway);
            mapper.Relate(x => x.LossesHome, y => y.LossesHome);
            mapper.Relate(x => x.LossesFulltimeAway, y => y.LossesFulltimeAway);
            mapper.Relate(x => x.LossesFulltimeHome, y => y.LossesFulltimeHome);
            mapper.Relate(x => x.LossesFulltimeTotal, y => y.LossesFulltimeTotal);
            mapper.Relate(x => x.LossesOvertimeAway, y => y.LossesOvertimeAway);
            mapper.Relate(x => x.LossesOvertimeHome, y => y.LossesOvertimeHome);
            mapper.Relate(x => x.LossesOvertimeTotal, y => y.LossesOvertimeTotal);

            // Points
            mapper.Relate(x => x.PointsHome, y => y.PointsHome);
            mapper.Relate(x => x.PointsAway, y => y.PointsAway);
            mapper.Relate(x => x.PointsStart, y => y.PointsStart);
            mapper.Relate(x => x.BonusPointsAway, y => y.BonusPointsAway);
            mapper.Relate(x => x.BonusPointsHome, y => y.BonusPointsHome);
            mapper.Relate(x => x.BonusPointsTotal, y => y.BonusPointsTotal);

            // Partial Points Conceded
            mapper.Relate(x => x.PartialPointsConceded, y => y.PartialPointsConceded);
            mapper.Relate(x => x.PartialPointsConcededAway, y => y.PartialPointsConcededAway);
            mapper.Relate(x => x.PartialPointsConcededHome, y => y.PartialPointsConcededHome);

            // Partial Points Difference
            mapper.Relate(x => x.PartialPointsDifference, y => y.PartialPointsDifference);
            mapper.Relate(x => x.PartialPointsDifferenceAway, y => y.PartialPointsDifferenceAway);
            mapper.Relate(x => x.PartialPointsDifferenceHome, y => y.PartialPointsDifferenceHome);

            // Partial Points Scored
            mapper.Relate(x => x.PartialPointsScored, y => y.PartialPointsScored);
            mapper.Relate(x => x.PartialPointsScoredAway, y => y.PartialPointsScoredAway);
            mapper.Relate(x => x.PartialPointsScoredHome, y => y.PartialPointsScoredHome);

            // Team Penalty
            mapper.Relate(x => x.TeamPenalty, y => y.TeamPenalty);
            mapper.Relate(x => x.TeamPenaltyNegative, y => y.TeamPenaltyNegative);
            mapper.Relate(x => x.TeamPenaltyPositive, y => y.TeamPenaltyPositive);

            // Victories
            mapper.Relate(x => x.Victories, y => y.Victories);
            mapper.Relate(x => x.VictoriesAway, y => y.VictoriesAway);
            mapper.Relate(x => x.VictoriesHome, y => y.VictoriesHome);
            mapper.Relate(x => x.VictoriesFulltimeAway, y => y.VictoriesFulltimeAway);
            mapper.Relate(x => x.VictoriesFulltimeHome, y => y.VictoriesFulltimeHome);
            mapper.Relate(x => x.VictoriesFulltimeTotal, y => y.VictoriesFulltimeTotal);
            mapper.Relate(x => x.VictoriesOvertimeAway, y => y.VictoriesOvertimeAway);
            mapper.Relate(x => x.VictoriesOvertimeHome, y => y.VictoriesOvertimeHome);
            mapper.Relate(x => x.VictoriesOvertimeTotal, y => y.VictoriesOvertimeTotal);
            mapper.Relate(x => x.VictoriesPenaltiesAway, y => y.VictoriesPenaltiesAway);
            mapper.Relate(x => x.VictoriesPenaltiesHome, y => y.VictoriesPenaltiesHome);
            mapper.Relate(x => x.VictoriesPenaltiesTotal, y => y.VictoriesPenaltiesTotal);
        }
    }
}