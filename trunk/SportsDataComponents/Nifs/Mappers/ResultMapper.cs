﻿using Glue;
using NTB.SportsData.Components.Mappers;
using NTB.SportsData.Components.NIFConnectService;
using Result = NTB.SportsData.Classes.Classes.Result;

namespace NTB.SportsData.Components.Nifs.Mappers
{
    /// <summary>
    /// The result 1 data mapper.
    /// </summary>
    public class ResultMapper : BaseMapper<Result1, Result>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<Result1, Result> mapper)
        {
            // General
            mapper.Relate(x => x.MatchId, y => y.MatchId);
            mapper.Relate(x => x.AwayGoals, y => y.AwayGoals);
            mapper.Relate(x => x.HomeGoals, y => y.HomeGoals);
            mapper.Relate(x => x.AwayTeamNotMet, y => y.AwayTeamNotMet);
            mapper.Relate(x => x.AwayTeamWalkover, y => y.AwayTeamWalkover);
            mapper.Relate(x => x.CountingAwayGoals, y => y.CountingAwayGoals);
            mapper.Relate(x => x.CountingAwayPoints, y => y.CountingAwayPoints);
            mapper.Relate(x => x.CountingBaseAwayPoints, y => y.CountingBaseAwayPoints);
            mapper.Relate(x => x.CountingBaseHomePoints, y => y.CountingBaseHomePoints);
            mapper.Relate(x => x.CountingBonusAwayPoints, y => y.CountingBonusAwayPoints);
            mapper.Relate(x => x.CountingBonusHomePoints, y => y.CountingBonusHomePoints);
            mapper.Relate(x => x.CountingHomeGoals, y => y.CountingHomeGoals);
            mapper.Relate(x => x.CountingMatchResult, y => y.CountingMatchResult);
            mapper.Relate(x => x.CountingHomePoints, y => y.CountingHomePoints);
            mapper.Relate(x => x.HomeGoals, y => y.HomeGoals);
            mapper.Relate(x => x.HomeTeamNotMet, y => y.HomeTeamNotMet);
            mapper.Relate(x => x.HomeTeamWalkover, y => y.HomeTeamWalkover);
            mapper.Relate(x => x.ResultStatusId, y => y.ResultStatusId);
            mapper.Relate(x => x.ResultId, y => y.ResultId);
            mapper.Relate(x => x.ResultTypeId, y => y.ResultTypeId);
            mapper.Relate(x => x.CountingResultTypeId, y => y.CountingResultTypeId);
            mapper.Relate(x => x.MatchResult, y => y.MatchResult);
            mapper.Relate(x => x.Spectators, y => y.Specators);
        }
    }
}