﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchSearchResultMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using Glue;
using NTB.SportsData.Components.Mappers;
using NTB.SportsData.Components.NIFConnectService;

namespace NTB.SportsData.Components.Nifs.Mappers
{
    using Match = SportsData.Classes.Classes.Match;

    /// <summary>
    ///     The match search result mapper.
    /// </summary>
    public class MatchSearchResultMapper : BaseMapper<MatchSearchResult, Match>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<MatchSearchResult, Match> mapper)
        {
            mapper.Relate(x => x.MatchId, y => y.Id);
            mapper.Relate(x => x.MatchDate, y => y.MatchDate);
            mapper.Relate(x => x.MatchEndTime, y => y.MatchEndTime);
            mapper.Relate(x => x.MatchStartTime, y => y.MatchStartTime);
            mapper.Relate(x => x.MatchNo, y => y.MatchNo);
            mapper.Relate(x => x.AwayteamId, y => y.AwayTeamId);
            mapper.Relate(x => x.AwayteamName, y => y.AwayTeamName);
            mapper.Relate(x => x.AwayteamShortName, y => y.AwayTeamShortName);
            mapper.Relate(x => x.HometeamId, y => y.HomeTeamId);
            mapper.Relate(x => x.HometeamName, y => y.HomeTeamName);
            mapper.Relate(x => x.HometeamShortName, y => y.HomeTeamShortName);
            mapper.Relate(x => x.GoalsAway, y => y.AwayTeamGoals);
            mapper.Relate(x => x.GoalsHome, y => y.HomeTeamGoals);
            mapper.Relate(x => x.ResultStatusId, y => y.ResultStatusId);
            mapper.Relate(x => x.Spectators, y => y.Spectators, this.NullableIntToIntConverter());
            mapper.Relate(x => x.MatchPublished, y => y.MatchPublished);
            mapper.Relate(x => x.TournamentId, y => y.TournamentId);
            mapper.Relate(x => x.TournamentName, y => y.TournamentName);
            mapper.Relate(x => x.RoundId, y => y.RoundId);
            mapper.Relate(x => x.RoundName, y => y.RoundName);
        }
    }
}