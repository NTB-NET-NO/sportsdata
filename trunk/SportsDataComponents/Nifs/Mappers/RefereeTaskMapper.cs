﻿using Glue;
using NTB.SportsData.Components.Mappers;
using NTB.SportsData.Components.NIFConnectService;
using Referee = NTB.SportsData.Classes.Classes.Referee;

namespace NTB.SportsData.Components.Nifs.Mappers
{
    public class RefereeTaskMapper : BaseMapper<RefereeTask1, Referee>
    {
        protected override void SetUpMapper(Mapping<RefereeTask1, Referee> mapper)
        {
            mapper.Relate(x => x.RefereeId, y => y.Id);
            mapper.Relate(x => x.RefereeFirstName, y => y.FirstName);
            mapper.Relate(x => x.RefereeLastName, y => y.LastName);
            mapper.Relate(x => x.RefereeTaskId, y => y.RefereeTaskId);
            mapper.Relate(x => x.RefereeTaskTypeId, y => y.RefereeTypeId);
            mapper.Relate(x => x.RefereeTaskTypeName, y => y.RefereeType);
        }
    }
}
