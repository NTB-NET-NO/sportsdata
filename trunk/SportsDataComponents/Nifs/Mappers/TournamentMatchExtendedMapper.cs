﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentMatchExtendedMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using Glue;
using NTB.SportsData.Components.Mappers;
using NTB.SportsData.Components.NIFConnectService;

namespace NTB.SportsData.Components.Nifs.Mappers
{
    using Match = SportsData.Classes.Classes.Match;

    /// <summary>
    /// The tournament match extended mapper.
    /// </summary>
    internal class TournamentMatchExtendedMapper : BaseMapper<TournamentMatchExtended, Match>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<TournamentMatchExtended, Match> mapper)
        {
            mapper.Relate(x => x.MatchId, y => y.Id);
            mapper.Relate(x => x.MatchDate, y => y.MatchDate);
            mapper.Relate(x => x.MatchNo, y => y.MatchNo);
            mapper.Relate(x => x.MatchStartTime, y => y.MatchStartTime);
            mapper.Relate(x => x.AwayteamId, y => y.AwayTeamId);
            mapper.Relate(x => x.Awayteam, y => y.AwayTeamName);
            mapper.Relate(x => x.HometeamId, y => y.HomeTeamId);
            mapper.Relate(x => x.Hometeam, y => y.HomeTeamName);
            mapper.Relate(x => x.MatchEndTime, y => y.MatchEndTime);
            mapper.Relate(x => x.RoundId, y => y.RoundId);
            mapper.Relate(x => x.RoundName, y => y.RoundName);
            mapper.Relate(x => x.HomeGoals, y => y.HomeTeamGoals);
            mapper.Relate(x => x.AwayGoals, y => y.AwayTeamGoals);
            mapper.Relate(x => x.PartialResult, y => y.PartialResult);
            mapper.Relate(x => x.MatchResult, y => y.MatchResult);
            mapper.Relate(x => x.ResultFormatted, y => y.ResultFormatted);
            mapper.Relate(x => x.StatusCode, y => y.StatusCode);
            mapper.Relate(x => x.VenueUnitName, y => y.VenueName);
            mapper.Relate(x => x.VenueUnitNo, y => y.VenueUnitId, this.NullableIntToIntConverter());
        }
    }
}