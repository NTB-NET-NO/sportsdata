﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using log4net;
using NTB.SportsData.Components.Nifs.Classes;
using NTB.SportsData.Components.Nifs.Classes.SportsMl;

namespace NTB.SportsData.Components.Nifs.Data.DataMappers.Players
{
    public class PlayerDataMapper
    {
        /// <summary>
        /// The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(PlayerDataMapper));

        public Player GetPlayerMetaDataById(int id)
        {
            var player = new Player();
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportLite_GetPlayerMetaDataById", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@PlayerId", id));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return new Player();
                    }

                    while (sqlDataReader.Read())
                    {
                        var names = new Name[2];
                        var name = new Name
                        {
                            Role = "nrol:display",
                            FullName = sqlDataReader["OfficialName"].ToString()
                        };

                        var alternativeName = new Name
                        {
                            Role = "nrol:alternate",
                            FullName = sqlDataReader["AlternativeName"].ToString()
                        };

                        names[0] = name;
                        names[1] = alternativeName;

                        player.PlayerMetaData = new PlayerMetaData();
                        player.PlayerMetaData.Key = "p:" + sqlDataReader["Id"];
                        player.PlayerMetaData.PlayerNames = new Name[2];
                        player.PlayerMetaData.PlayerNames = names;
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return player;
            }
        }

        public List<Classes.SportsMl.Team> SearchPlayer(string query)
        {
            var teams = new List<Classes.SportsMl.Team>();
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportLite_SearchPlayer", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@SearchQuery", query));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return new List<Classes.SportsMl.Team>();
                    }

                    while (sqlDataReader.Read())
                    {
                        var names = new Name[2];
                        var name = new Name
                        {
                            Role = "nrol:display",
                            FullName = sqlDataReader["PlayerName"].ToString()
                        };

                        var alternativeName = new Name
                        {
                            Role = "nrol:alternate",
                            FullName = sqlDataReader["AlternativeName"].ToString()
                        };

                        names[0] = name;
                        names[1] = alternativeName;

                        var player = new Player();
                        player.Id = "p." + sqlDataReader["Id"];

                        player.PlayerMetaData = new PlayerMetaData();
                        player.PlayerMetaData.Key = "p:" + sqlDataReader["Id"];
                        player.PlayerMetaData.PlayerNames = new Name[2];
                        player.PlayerMetaData.PlayerNames = names;

                        var team = new Classes.SportsMl.Team();
                        team.TeamId = "t." + sqlDataReader["TeamId"] + ".p." + sqlDataReader["Id"];
                        team.TeamMetaData = new TeamMetaData();
                        team.TeamMetaData.TeamNames = new Name[1];
                        var teamName = new Name();
                        teamName.FullName = sqlDataReader["TeamName"].ToString();
                        team.TeamMetaData.TeamNames[0] = teamName;
                        team.Player = new Player[1];
                        team.Player[0] = player;

                        teams.Add(team);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return teams;
            }
        }

        public void UpdatePlayerMetaData(PlayerRequestMetaData playerMetaData)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportLite_UpdatePlayerMetaData", sqlConnection)
                    {
                        CommandType =
                            CommandType
                                .StoredProcedure
                    };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("@PlayerId", playerMetaData.PlayerId));
                    sqlCommand.Parameters.Add(new SqlParameter("@PlayerName", playerMetaData.Name));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }
    }
}