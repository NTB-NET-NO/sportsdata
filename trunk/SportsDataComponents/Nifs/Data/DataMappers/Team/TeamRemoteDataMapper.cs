﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TeamRemoteDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The team remote data mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using NTB.SportsData.Components.Nifs.Classes.Nifs;

namespace NTB.SportsData.Components.Nifs.Data.DataMappers.Team
{
    /// <summary>
    /// The team remote data mapper.
    /// </summary>
    public class TeamRemoteDataMapper : ITeamRemoteDataMapper
    {
        /// <summary>
        /// The get tournament teams.
        /// </summary>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Classes.Team> GetTournamentTeams(Tournament tournament)
        {
            //IApiFacade apifacade;
            //if (tournament.SportId == 16)
            //{
            //    apifacade = new SoccerApiFacade();
            //}
            //else
            //{
            //    apifacade = new SportApiFacade();
            //}

            //return apifacade.GetTournamentTeams(tournament.TournamentId);
            return null;
        }
    }
}