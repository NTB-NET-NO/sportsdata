﻿using System.Collections.Generic;

namespace NTB.SportsData.Components.Nifs.Data.DataMappers.Team
{
    /// <summary>
    /// The TeamDataMapper interface.
    /// </summary>
    public interface ITeamRemoteDataMapper
    {
        /// <summary>
        /// The get tournament teams.
        /// </summary>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Classes.Team> GetTournamentTeams(Classes.Nifs.Tournament tournament);
    }
}
