﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TeamDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The team data mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.Nifs.Data.DataMappers.Team
{
    /// <summary>
    /// The team data mapper.
    /// </summary>
    internal class TeamDataMapper
    {
    }
}