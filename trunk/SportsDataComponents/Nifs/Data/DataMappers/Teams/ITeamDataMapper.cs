﻿namespace NTB.SportsData.Components.Nifs.Data.DataMappers.Teams
{
    /// <summary>
    /// The TeamDataMapper interface.
    /// </summary>
    public interface ITeamDataMapper
    {
        /// <summary>
        /// The delete team contact map.
        /// </summary>
        /// <param name="teamId">
        /// The team id.
        /// </param>
        /// <param name="contactId">
        /// The contact id.
        /// </param>
        void DeleteTeamContactMap(int teamId, int contactId);

        /// <summary>
        /// The insert team contact map.
        /// </summary>
        /// <param name="teamId">
        /// The team id.
        /// </param>
        /// <param name="contactId">
        /// The contact id.
        /// </param>
        void InsertTeamContactMap(int teamId, int contactId);

        Classes.Team GetNifsTeamById(int teamId);
    }
}
