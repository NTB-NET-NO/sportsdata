﻿using NTB.SportsData.Components.Nifs.Classes.Nifs;

namespace NTB.SportsData.Components.Nifs.Data.DataMappers.Teams
{
    public interface ITeamRoleDataMapper
    {
        /// <summary>
        /// The delete team role.
        /// </summary>
        /// <param name="functionId">
        /// The function id.
        /// </param>
        void DeleteTeamRole(int functionId);

        /// <summary>
        /// The insert team role.
        /// </summary>
        /// <param name="function">
        /// The function.
        /// </param>
        void InsertTeamRole(Function function);

        /// <summary>
        /// The get team role by id.
        /// </summary>
        /// <param name="functionId">
        /// The function id.
        /// </param>
        /// <returns>
        /// The <see cref="Function"/>.
        /// </returns>
        Function GetTeamRoleById(int functionId);
    }
}
