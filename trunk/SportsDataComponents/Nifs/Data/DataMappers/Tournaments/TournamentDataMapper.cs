﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The tournament data mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using log4net;
using NTB.SportsData.Components.Nifs.Classes;
using NTB.SportsData.Components.Nifs.Classes.Nifs;
using NTB.SportsData.Components.Nifs.Classes.SportsMl;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;

namespace NTB.SportsData.Components.Nifs.Data.DataMappers.Tournaments
{
    using Tournament = SportsData.Classes.Classes.Tournament;
    
    /// <summary>
    ///     The tournament data mapper.
    /// </summary>
    public class TournamentDataMapper : IRepository<Tournament>, IDisposable, ITournamentDataMapper
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(TournamentDataMapper));

        /// <summary>
        ///     The dispose.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     The insert one.
        /// </summary>
        /// <param name="domainobject">
        ///     The domainobject.
        /// </param>
        /// <returns>
        ///     The <see cref="int" />.
        /// </returns>
        public int InsertOne(Tournament domainobject)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_InsertTournament", sqlConnection)
                                         {
                                             CommandType = CommandType.StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", domainobject.DistrictId));
                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", domainobject.SeasonId));
                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", domainobject.Id));
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentName", domainobject.Name));
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentNumber", domainobject.TournamentNumber));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return 1;
            }
        }

        /// <summary>
        ///     The insert all.
        /// </summary>
        /// <param name="domainobject">
        ///     The domainobject.
        /// </param>
        public void InsertAll(List<Tournament> domainobject)
        {
            foreach (var tournament in domainobject)
            {
                this.InsertOne(tournament);
            }
        }

        /// <summary>
        ///     The update.
        /// </summary>
        /// <param name="domainobject">
        ///     The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Update(Tournament domainobject)
        {
        }

        /// <summary>
        ///     The delete.
        /// </summary>
        /// <param name="domainobject">
        ///     The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Delete(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     The get all.
        /// </summary>
        /// <returns>
        ///     The <see cref="IQueryable" />.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public IQueryable<Tournament> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     The get.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        ///     The <see cref="Classes.SportsMl.Tournament" />.
        /// </returns>
        public Tournament Get(int id)
        {
            var tournament = new Tournament();
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsDataInput_GetTournamentById", sqlConnection)
                                         {
                                             CommandType = CommandType.StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", id));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return new Tournament();
                    }

                    while (sqlDataReader.Read())
                    {
                        tournament.Id = Convert.ToInt32(sqlDataReader["TournamentId"]);
                        tournament.DistrictId = Convert.ToInt32(sqlDataReader["DistrictId"]);
                        tournament.SeasonId = Convert.ToInt32(sqlDataReader["SeasonId"]);
                        tournament.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                        tournament.Name = sqlDataReader["TournamentName"].ToString();
                        tournament.TournamentNumber = sqlDataReader["TournamentNumber"].ToString();
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return tournament;
            }
        }

        /// <summary>
        ///     The search tournament.
        /// </summary>
        /// <param name="query">
        ///     The query.
        /// </param>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Classes.SportsMl.Tournament> SearchTournament(string query)
        {
            var tournaments = new List<Classes.SportsMl.Tournament>();
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportLite_SearchTournament", sqlConnection)
                                         {
                                             CommandType = CommandType.StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@SearchQuery", query));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return new List<Classes.SportsMl.Tournament>();
                    }

                    while (sqlDataReader.Read())
                    {
                        var names = new Name[2];
                        var name = new Name
                                       {
                                           Role = "nrol:display",
                                           FullName = sqlDataReader["OfficialName"].ToString()
                                       };

                        var alternativeName = new Name
                                                  {
                                                      Role = "nrol:alternate",
                                                      FullName = sqlDataReader["AlternativeName"].ToString()
                                                  };

                        names[0] = name;
                        names[1] = alternativeName;

                        var tournament = new Classes.SportsMl.Tournament();
                        tournament.Id = "t." + sqlDataReader["Id"];
                        tournament.TournamentMetaData = new Classes.SportsMl.TournamentMetaData();
                        tournament.TournamentMetaData.Id = "t." + sqlDataReader["Id"];
                        tournament.TournamentMetaData.TournamentKey = "t:" + sqlDataReader["Id"];
                        tournament.TournamentMetaData.TournamentNames = new Name[2];
                        tournament.TournamentMetaData.TournamentNames = names;
                        tournament.TournamentMetaData.SportsProperty = new SportsProperty();
                        tournament.TournamentMetaData.SportsProperty.FormalName = "country";
                        tournament.TournamentMetaData.SportsProperty.Value = sqlDataReader["Country"].ToString();

                        tournaments.Add(tournament);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return tournaments;
            }
        }

        public string GetTournamentFootNoteById(int id)
        {
            var footnote = string.Empty;
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportLite_GetTournamentFootNoteById", sqlConnection)
                                         {
                                             CommandType = CommandType.StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", id));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        footnote = string.Empty;
                    }

                    while (sqlDataReader.Read())
                    {
                        if (sqlDataReader["TableFootNote"] != null)
                        {
                            footnote = sqlDataReader["TableFootNote"].ToString();
                        }
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }

                }
            }
            return footnote;
            
        }

        public int PresentComingMatchesByTournamentId(int id)
        {
            var comingMatches = 0;
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportLite_PresentComingMatchesByTournamentId", sqlConnection)
                                         {
                                             CommandType = CommandType.StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", id));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        comingMatches = 0;
                    }

                    while (sqlDataReader.Read())
                    {
                        if (sqlDataReader["ComingMatches"] != DBNull.Value)
                        {
                            comingMatches = Convert.ToInt32(sqlDataReader["ComingMatches"]);
                        }
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
            return comingMatches;
        }

        public List<SportEventType> GetAllSportEventTypes()
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportLite_GetAllEventTypes", sqlConnection)
                                         {
                                             CommandType = CommandType.StoredProcedure
                                         };


                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return new List<SportEventType>();
                    }

                    var sportEventTypes = new List<SportEventType>();
                    while (sqlDataReader.Read())
                    {
                        var sportEventType = new SportEventType();
                        if (sqlDataReader["EventTypeId"] != DBNull.Value)
                        {
                            sportEventType.Id = Convert.ToInt32(sqlDataReader["EventTypeId"]);
                        }

                        if (sqlDataReader["EventTypeName"] != DBNull.Value)
                        {
                            sportEventType.Name = sqlDataReader["EventTypeName"].ToString();
                        }

                        sportEventTypes.Add(sportEventType);
                    }

                    return sportEventTypes;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void InsertTournament(Tournament tournament, int countryId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportLite_InsertTournament", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@Id", tournament.Id));
                    sqlCommand.Parameters.Add(new SqlParameter("@OfficialName", tournament.Name));
                    sqlCommand.Parameters.Add(new SqlParameter("@SportCode", tournament.SportId));
                    sqlCommand.Parameters.Add(new SqlParameter("@SportName", tournament.SportName));
                    sqlCommand.Parameters.Add(new SqlParameter("@CountryId", countryId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public Tournament GetTournamentById(int tournamentId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                var tournament = new Tournament();
                try
                {
                    var sqlCommand = new SqlCommand("SportLite_GetTournamentById", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@Id", tournamentId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return new Tournament();
                    }

                    while (sqlDataReader.Read())
                    {
                        tournament.Name = sqlDataReader["OfficialName"].ToString();
                        tournament.Id = Convert.ToInt32(sqlDataReader["Id"]);
                        tournament.SportName = sqlDataReader["SportName"].ToString();
                        var sportCode = sqlDataReader["SportCode"].ToString();
                        tournament.SportId = Convert.ToInt32(sportCode.Split(':').Last());
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
                return tournament;
            }
        }

        public void InsertTournamentStage(TournamentStage stage)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportLite_InsertStage", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    Logger.DebugFormat("Inserting stage with id {0}", stage.Id);
                    sqlCommand.Parameters.Add(new SqlParameter("@Id", stage.Id));
                    sqlCommand.Parameters.Add(new SqlParameter("@OfficialName", stage.Name));
                    sqlCommand.Parameters.Add(new SqlParameter("@TypeId", stage.StageTypeId));
                    sqlCommand.Parameters.Add(new SqlParameter("@YearStart", stage.YearStart));
                    sqlCommand.Parameters.Add(new SqlParameter("@YearEnd", stage.YearEnd));
                    sqlCommand.Parameters.Add(new SqlParameter("@DateStart", stage.DateStart));
                    sqlCommand.Parameters.Add(new SqlParameter("@DateEnd", stage.DateEnd));
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", stage.Tournament.Id));


                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

            }
        }

        public TournamentStage GetTournamentStageById(int id)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportLite_GetTournamentStageById", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };


                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return new TournamentStage();
                    }

                    var tournamentStage = new TournamentStage();
                    while (sqlDataReader.Read())
                    {
                        if (sqlDataReader["Id"] != DBNull.Value)
                        {
                            tournamentStage.Id = Convert.ToInt32(sqlDataReader["Id"]);
                        }

                        if (sqlDataReader["OfficialName"] != DBNull.Value)
                        {
                            tournamentStage.Name = sqlDataReader["OfficialName"].ToString();
                        }

                        if (sqlDataReader["TournamentId"] != DBNull.Value)
                        {
                            tournamentStage.Tournament = new Classes.Nifs.Tournament();
                            tournamentStage.Tournament.Id = Convert.ToInt32(sqlDataReader["TournamentId"]);
                        }

                        if (sqlDataReader["DateEnd"] != DBNull.Value)
                        {
                            tournamentStage.DateEnd = Convert.ToDateTime(sqlDataReader["DateEnd"].ToString());
                        }

                        if (sqlDataReader["DateStart"] != DBNull.Value)
                        {
                            tournamentStage.DateStart = Convert.ToDateTime(sqlDataReader["DateStart"].ToString());
                        }

                        if (sqlDataReader["YearStart"] != DBNull.Value)
                        {
                            tournamentStage.YearStart = Convert.ToInt32(sqlDataReader["YearStart"].ToString());
                        }

                        if (sqlDataReader["YearEnd"] != DBNull.Value)
                        {
                            tournamentStage.YearEnd = Convert.ToInt32(sqlDataReader["YearEnd"].ToString());
                        }
                    }

                    return tournamentStage;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public List<TournamentPattern> GetTournamentPatternsByTournamentId(int id)
        {
            var tournamentPatterns = new List<TournamentPattern>();
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("[SportLite_GetTournamentPatternsById]", sqlConnection)
                                         {
                                             CommandType = CommandType.StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", id));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    while (sqlDataReader.Read())
                    {
                        var tournamentPattern = new TournamentPattern();
                        if (sqlDataReader["Id"] != DBNull.Value)
                        {
                            tournamentPattern.Id = Convert.ToInt32(sqlDataReader["Id"]);
                        }

                        if (sqlDataReader["ProductName"] != DBNull.Value)
                        {
                            tournamentPattern.ProductName = sqlDataReader["ProductName"].ToString();
                        }

                        if (sqlDataReader["ProductId"] != DBNull.Value)
                        {
                            tournamentPattern.ProductId = Convert.ToInt32(sqlDataReader["ProductId"]);
                        }

                        if (sqlDataReader["InfoLine"] != DBNull.Value)
                        {
                            tournamentPattern.InfoLine = sqlDataReader["InfoLine"].ToString();
                        }

                        if (sqlDataReader["SlugLine"] != DBNull.Value)
                        {
                            tournamentPattern.SlugLine = sqlDataReader["SlugLine"].ToString();
                        }

                        if (sqlDataReader["IntroLine"] != DBNull.Value)
                        {
                            tournamentPattern.IntroLine = sqlDataReader["IntroLine"].ToString();
                        }

                        tournamentPatterns.Add(tournamentPattern);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return tournamentPatterns;
            }
        }

        /// <summary>
        ///     The get tournament patterns.
        /// </summary>
        /// <returns>
        ///     The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<TournamentPattern> GetTournamentPatterns()
        {
            var tournamentPatterns = new List<TournamentPattern>();
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportLite_GetTournamentPatterns", sqlConnection)
                                         {
                                             CommandType = CommandType.StoredProcedure
                                         };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    while (sqlDataReader.Read())
                    {
                        var tournamentPattern = new TournamentPattern();
                        if (sqlDataReader["Id"] != DBNull.Value)
                        {
                            tournamentPattern.Id = Convert.ToInt32(sqlDataReader["Id"]);
                        }

                        if (sqlDataReader["ProductName"] != DBNull.Value)
                        {
                            tournamentPattern.ProductName = sqlDataReader["ProductName"].ToString();
                        }

                        if (sqlDataReader["ProductId"] != DBNull.Value)
                        {
                            tournamentPattern.ProductId = Convert.ToInt32(sqlDataReader["ProductId"]);
                        }

                        if (sqlDataReader["InfoLine"] != DBNull.Value)
                        {
                            tournamentPattern.InfoLine = sqlDataReader["InfoLine"].ToString();
                        }

                        if (sqlDataReader["SlugLine"] != DBNull.Value)
                        {
                            tournamentPattern.InfoLine = sqlDataReader["SlugLine"].ToString();
                        }

                        if (sqlDataReader["IntroLine"] != DBNull.Value)
                        {
                            tournamentPattern.InfoLine = sqlDataReader["IntroLine"].ToString();
                        }

                        tournamentPatterns.Add(tournamentPattern);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return tournamentPatterns;
            }
        }

        public void UpdateTournamentMetaData(Classes.Nifs.TournamentMetaData tournamentMetaData)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("[SportLite_UpdateTournamentMetaData]", sqlConnection)
                                         {
                                             CommandType = CommandType.StoredProcedure
                                         };

                    if (tournamentMetaData.DateLine == null)
                    {
                        tournamentMetaData.DateLine = string.Empty;
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("@Id", tournamentMetaData.Id));
                    sqlCommand.Parameters.Add(new SqlParameter("@Name", tournamentMetaData.Name));
                    sqlCommand.Parameters.Add(new SqlParameter("@Gender", tournamentMetaData.GenderId));
                    sqlCommand.Parameters.Add(new SqlParameter("@DateLine", tournamentMetaData.DateLine));
                    sqlCommand.Parameters.Add(new SqlParameter("@Owner", tournamentMetaData.Owner));
                    sqlCommand.Parameters.Add(new SqlParameter("@CountryId", tournamentMetaData.CountryId));
                    sqlCommand.Parameters.Add(new SqlParameter("@EventTypeId", tournamentMetaData.EventTypeId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public Tournament GetTournamentByStageId(int stageId)
        {
            
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                var tournament = new Tournament();
                try
                {
                    var sqlCommand = new SqlCommand("SportLite_GetTournamentByStageId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    var dataOwner = ConfigurationManager.AppSettings["DataOwner"];

                    sqlCommand.Parameters.Add(new SqlParameter("@StageId", stageId));
                    sqlCommand.Parameters.Add(new SqlParameter("@Owner", dataOwner));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return new Tournament();
                    }

                    while (sqlDataReader.Read())
                    {
                        tournament.Name = sqlDataReader["TournamentName"].ToString();
                        tournament.Id = Convert.ToInt32(sqlDataReader["Id"]);
                        tournament.SportName = sqlDataReader["SportName"].ToString();
                        var sportCode = sqlDataReader["SportCode"].ToString();
                        tournament.SportId = Convert.ToInt32(sportCode.Split(':').Last());
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
                return tournament;
            }
        }

        public SportEventType GetTournamentEventTypeByTournamentId(int id)
        {
            var sportEventType = new SportEventType();
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportLite_GetSportEventTypeByTournamentId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@Id", id));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return new SportEventType();
                    }
                    

                    while (sqlDataReader.Read())
                    {


                        if (sqlDataReader["EventTypeId"] != DBNull.Value)
                        {
                            sportEventType.Id = Convert.ToInt32(sqlDataReader["EventTypeId"]);
                        }

                        if (sqlDataReader["EventTypeName"] != DBNull.Value)
                        {
                            sportEventType.Name = sqlDataReader["EventTypeName"].ToString();
                        }
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                    
                    return new SportEventType();
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
                return sportEventType;
            }
        }

        public SportsData.Classes.Classes.Country GetTournamentCountryByTournamentId(int id)
        {
            var country = new SportsData.Classes.Classes.Country();
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportLite_GetCountryByTournamentId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@Id", id));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return new SportsData.Classes.Classes.Country();
                    }


                    while (sqlDataReader.Read())
                    {
                        if (sqlDataReader["Id"] != DBNull.Value)
                        {
                            country.Id = sqlDataReader["Id"].ToString();
                        }

                        if (sqlDataReader["Name"] != DBNull.Value)
                        {
                            country.Name = sqlDataReader["Name"].ToString();
                        }
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    return new SportsData.Classes.Classes.Country();
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
                return country;
            }
        }

        /// <summary>
        ///     The get tournament meta data by id.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        ///     The <see cref="SportsContent" />.
        /// </returns>
        public SportsContent GetTournamentMetaDataById(int id)
        {
            var sportsContent = new SportsContent();
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportLite_GetTournamentMetaDataById", sqlConnection)
                                         {
                                             CommandType = CommandType.StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@Id", id));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return new SportsContent();
                    }

                    while (sqlDataReader.Read())
                    {
                        var names = new Name[2];
                        var name = new Name
                                       {
                                           Role = "nrol:display",
                                           FullName = sqlDataReader["TournamentName"].ToString()
                                       };

                        var alternativeName = new Name
                                                  {
                                                      Role = "nrol:alternate",
                                                      FullName = sqlDataReader["AlternativeName"].ToString()
                                                  };

                        names[0] = name;
                        names[1] = alternativeName;

                        sportsContent.SportsMetaData = new SportsMetaData();
                        // sportsContent.SportsMetaData.Slug = sqlDataReader["Slugline"].ToString();
                        sportsContent.SportsMetaData.SportsTitle = sqlDataReader["AlternativeName"].ToString();
                        sportsContent.SportsMetaData.SportsContentCodes = new SportsContentCodes();

                        var sportsContentCodeSport = new SportsContentCode
                                                         {
                                                             CodeKey = sqlDataReader["SportCode"].ToString(),
                                                             CodeName = sqlDataReader["SportName"].ToString()
                                                         };

                        var sportsContentCodeCountry = new SportsContentCode();
                        if (sqlDataReader["CountryId"] != DBNull.Value)
                        {
                            sportsContentCodeCountry.CodeKey = "country:" + sqlDataReader["CountryId"];
                        }
                        else if (sqlDataReader["TournamentCountryId"] != DBNull.Value)
                        {
                            sportsContentCodeCountry.CodeKey = "country:" + sqlDataReader["TournamentCountryId"];
                        }

                        if (sqlDataReader["CountryName"] != DBNull.Value)
                        {
                            sportsContentCodeCountry.CodeName = sqlDataReader["CountryName"].ToString();
                        } else if (sqlDataReader["TournamentCountryName"] != DBNull.Value)
                        {
                            sportsContentCodeCountry.CodeName = sqlDataReader["TournamentCountryName"].ToString();
                        }

                        // Get the Event Type
                        var sportsContentCodeEventType = new SportsContentCode();
                        if (sqlDataReader["EventTypeId"] != DBNull.Value)
                        {
                            sportsContentCodeEventType.CodeKey = "eventType:" + sqlDataReader["EventTypeId"];
                        }

                        if (sqlDataReader["EventTypeName"] != DBNull.Value)
                        {
                            sportsContentCodeEventType.CodeName = sqlDataReader["EventTypeName"].ToString();
                        }
                       
                        var sportsContentCodeDateline = new SportsContentCode
                                                            {
                                                                CodeKey = "ntbspcd:dateline",
                                                                CodeName = sqlDataReader["DateLine"].ToString()
                                                            };

                        var sportsContentCodeGender = new SportsContentCode();
                        if (sqlDataReader["Gender"] != DBNull.Value)
                        {
                            switch (sqlDataReader["Gender"].ToString()
                                .ToLower())
                            {
                                case "male":
                                    sportsContentCodeGender.CodeKey = "squ:15000001";
                                    sportsContentCodeGender.CodeName = "male";
                                    break;
                                case "female":
                                    sportsContentCodeGender.CodeKey = "squ:15000002";
                                    sportsContentCodeGender.CodeName = "female";
                                    break;
                                default:
                                    if (sqlDataReader["Gender"].ToString()
                                            .ToLower() == "mixed")
                                    {
                                        sportsContentCodeGender.CodeKey = "squ:15000003";
                                        sportsContentCodeGender.CodeName = "mixed";
                                    }
                                    break;
                            }
                        }

                        sportsContent.SportsMetaData.SportsContentCodes.SportsContentCode = new SportsContentCode[5];
                        sportsContent.SportsMetaData.SportsContentCodes.SportsContentCode[0] = sportsContentCodeSport;
                        sportsContent.SportsMetaData.SportsContentCodes.SportsContentCode[1] = sportsContentCodeCountry;
                        sportsContent.SportsMetaData.SportsContentCodes.SportsContentCode[2] = sportsContentCodeDateline;
                        sportsContent.SportsMetaData.SportsContentCodes.SportsContentCode[3] = sportsContentCodeGender;
                        sportsContent.SportsMetaData.SportsContentCodes.SportsContentCode[4] = sportsContentCodeEventType;

                        sportsContent.Tournament = new Classes.SportsMl.Tournament[1];
                        var tournament = new Classes.SportsMl.Tournament();
                        tournament.Id = "t." + sqlDataReader["Id"];
                        tournament.TournamentMetaData = new Classes.SportsMl.TournamentMetaData();
                        tournament.TournamentMetaData.Id = "t." + sqlDataReader["Id"];
                        tournament.TournamentMetaData.TournamentKey = "t:" + sqlDataReader["Id"];
                        tournament.TournamentMetaData.TournamentNames = new Name[2];
                        tournament.TournamentMetaData.TournamentNames = names;
                        sportsContent.Tournament[0] = tournament;
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return sportsContent;
            }
        }
    }
}