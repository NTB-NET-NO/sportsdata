﻿using System.Collections.Generic;
using NTB.SportsData.Components.Nifs.Classes;
using NTB.SportsData.Components.Nifs.Classes.Nifs;

namespace NTB.SportsData.Components.Nifs.Data.DataMappers.Tournaments
{
    public interface ITournamentDataMapper
    {
        /// <summary>
        /// The search tournament.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<Classes.SportsMl.Tournament> SearchTournament(string query);

        string GetTournamentFootNoteById(int id);

        int PresentComingMatchesByTournamentId(int id);

        List<SportEventType> GetAllSportEventTypes();

        void InsertTournamentStage(TournamentStage stage);

        TournamentStage GetTournamentStageById(int id);
    }
}
