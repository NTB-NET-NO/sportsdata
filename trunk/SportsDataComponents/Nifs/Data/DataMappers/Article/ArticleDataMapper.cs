﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using log4net;

namespace NTB.SportsData.Components.Nifs.Data.DataMappers.Article
{
    public class ArticleDataMapper : IArticleDataMapper
    {
        public ArticleDataMapper(int tournamentId)
        {
            this.TournamentId = tournamentId;
        }

        /// <summary>
        /// The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(ArticleDataMapper));


        private int TournamentId { get; set; }

        public string GetLedeByExportId(int id)
        {
            var returnString = string.Empty;
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportLite_GetTournamentIntroLineByExportId", sqlConnection)
                                         {
                                             CommandType = CommandType.StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@ExportId", id));

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", this.TournamentId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();
                    if (!sqlDataReader.HasRows)
                    {
                        return string.Empty;
                    }

                    while (sqlDataReader.Read())
                    {
                        returnString = sqlDataReader["IntroLine"].ToString();
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return returnString;
            }
        }

        public string GetTitleByExportId(int id)
        {
            var returnString = string.Empty;
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportLite_GetTournamentInfoLineByExportId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@ExportId", id));

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", this.TournamentId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();
                    if (!sqlDataReader.HasRows)
                    {
                        return string.Empty;
                    }

                    while (sqlDataReader.Read())
                    {
                        returnString = sqlDataReader["InfoLine"].ToString();
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return returnString;
            }
        }

        public string GetLocationByHomeTeamId(int id)
        {
            throw new NotImplementedException();
        }

        public string GetDistributorByExportId(int id)
        {
            return ConfigurationManager.AppSettings["DataOwner"];
        }

        public string GetSluglineByExportId(int id)
        {
            var returnString = string.Empty;
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportLite_GetTournamentSlugLineByExportId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@ExportId", id));

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", this.TournamentId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();
                    if (!sqlDataReader.HasRows)
                    {
                        return string.Empty;
                    }

                    while (sqlDataReader.Read())
                    {
                        returnString = sqlDataReader["SlugLine"].ToString();
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return returnString;
            }
        }

        public string GetDateLineByExportId(int id)
        {
            var returnString = string.Empty;
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportLite_GetTournamentDateLineByExportId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@ExportId", id));

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", this.TournamentId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();
                    if (!sqlDataReader.HasRows)
                    {
                        return string.Empty;
                    }

                    while (sqlDataReader.Read())
                    {
                        if (sqlDataReader["DateLine"] != DBNull.Value)
                        {
                            returnString = sqlDataReader["DateLine"].ToString();
                        }
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return returnString;
            }
        }

        public string GetDateLineByTournamentId(int id)
        {
            var returnString = string.Empty;
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportLite_GetTournamentDateLineByTournamentId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@Id", this.TournamentId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlCommand.Connection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();
                    if (!sqlDataReader.HasRows)
                    {
                        return string.Empty;
                    }

                    while (sqlDataReader.Read())
                    {
                        if (sqlDataReader["DateLine"] != DBNull.Value)
                        {
                            returnString = sqlDataReader["DateLine"].ToString();
                        }
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return returnString;
            }
        }
    }
}
