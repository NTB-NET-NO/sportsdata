﻿namespace NTB.SportsData.Components.Nifs.Data.DataMappers.Article
{
    public interface IArticleDataMapper
    {
        string GetLedeByExportId(int id);

        string GetTitleByExportId(int id);

        string GetLocationByHomeTeamId(int id);

        string GetDistributorByExportId(int id);

        string GetSluglineByExportId(int id);

        string GetDateLineByExportId(int id);

        string GetDateLineByTournamentId(int id);
    }
}
