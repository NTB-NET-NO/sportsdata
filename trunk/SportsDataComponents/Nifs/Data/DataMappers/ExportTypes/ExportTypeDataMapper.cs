﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExportTypeDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The export type data mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using log4net;
using NTB.SportsData.Components.Nifs.Classes;

namespace NTB.SportsData.Components.Nifs.Data.DataMappers.ExportTypes
{
    /// <summary>
    /// The export type data mapper.
    /// </summary>
    public class ExportTypeDataMapper : IExportTypeDataMapper
    {
        /// <summary>
        /// The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(ExportTypeDataMapper));

        /// <summary>
        /// The get export types.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<ExportType> GetExportTypes()
        {
            var exportTypes = new List<ExportType>();

            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportLite_GetAllExportTypes", sqlConnection)
                                         {
                                             CommandType = CommandType.StoredProcedure
                                         };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            var exportType = new ExportType
                                                 {
                                                     Id = Convert.ToInt32(sqlDataReader["Id"]),
                                                     Url = sqlDataReader["Url"].ToString(), 
                                                     ServerPath = sqlDataReader["ServerPath"].ToString(), 
                                                     Label = sqlDataReader["Label"].ToString()
                                                 };

                            exportTypes.Add(exportType);
                        }
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return exportTypes;
            }
        }

        /// <summary>
        /// The insert export type.
        /// </summary>
        /// <param name="exportType">
        /// The export type.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// throws an exception because this method has not yet been implemented
        /// </exception>
        public void InsertExportType(ExportType exportType)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The update export type.
        /// </summary>
        /// <param name="exportType">
        /// The export type.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// throws an exception because this method has not yet been implemented
        /// </exception>
        public void UpdateExportType(ExportType exportType)
        {
            throw new NotImplementedException();
        }
    }
}