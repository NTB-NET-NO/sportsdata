﻿using System.Collections.Generic;
using NTB.SportsData.Components.Nifs.Classes;

namespace NTB.SportsData.Components.Nifs.Data.DataMappers.ExportTypes
{
    /// <summary>
    /// The ExportTypeDataMapper interface.
    /// </summary>
    public interface IExportTypeDataMapper
    {
        /// <summary>
        /// The get export types.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<ExportType> GetExportTypes();

        /// <summary>
        /// The insert export type.
        /// </summary>
        /// <param name="exportType">
        /// The export type.
        /// </param>
        void InsertExportType(ExportType exportType);

        /// <summary>
        /// The update export type.
        /// </summary>
        /// <param name="exportType">
        /// The export type.
        /// </param>
        void UpdateExportType(ExportType exportType);
    }
}
