﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Classes.Enums;
using NTB.SportsData.Components.Nifs.Gatherers.DataGatherers;
using NTB.SportsData.Components.Nifs.Gatherers.Interfaces;
using NTB.SportsData.Components.Nifs.Helpers;
using NTB.SportsData.Components.Nifs.Models;
using NTB.SportsData.Components.PublicRepositories;
using Match = NTB.SportsData.Classes.Classes.Match;
using PartialResult = NTB.SportsData.Classes.Classes.PartialResult;
using Tournament = NTB.SportsData.Classes.Classes.Tournament;
using Venue = NTB.SportsData.Classes.Classes.Venue;

namespace NTB.SportsData.Components.Nifs.Creators
{
    public class MatchResultCreator
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(MatchResultCreator));

        /// <summary>
        ///     Gets or sets the data params.
        /// </summary>
        /// 
        public DataFileParams DataParams { get; set; }

        private readonly INifsGatherer _gatherer;

        public MatchResultCreator()
        {
            this._gatherer = new NifsGatherer();
        }

        /// <summary>
        /// The create match results.
        /// </summary>
        /// <param name="tournament">
        /// The tournaments.
        /// </param>
        public List<IGrouping<DateTime?, Match>> GetMatchesGroupedByDate(Tournament tournament)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The compute grouped matches.
        /// </summary>
        /// <param name="groupedMatches">
        /// The grouped matches.
        /// </param>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// ///
        public List<MatchModel> ProcessGroupedMatches(List<IGrouping<DateTime?, Match>> groupedMatches, Tournament tournament)
        {
            // @TODO: Need to work on this as it does two things. 
            // @todo: Remove the teamsportxmldocument-generation here. Just populate the model and return it
            // @todo: then call a GenerateXmlDocument-method with the model so it can be generated.
            // Set the match repository
            var matchModels = new List<MatchModel>();
            foreach (var matchGroup in groupedMatches)
            {
                // Check if we have results
                // Removed because it checks if the value is larger than zero. that means it will not support 0-0...
                //this._xmlTeamSportDocument.Result = this.HasResults(matchGroup);

                matchModels = this.GenerateMatchModel(matchGroup);
            }

            return matchModels;
        }

        public DateTime GetMatchDate(IGrouping<DateTime?, Match> matchGroup)
        {
            var matchDate = new DateTime();
            try
            {
                var foundMatchDates = (from mg in matchGroup where mg.MatchDate == matchGroup.Key select mg).ToList();

                if (foundMatchDates.Any())
                {
                    if (matchGroup.Key != null)
                    {
                        matchDate = matchGroup.Key.Value;
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.ErrorFormat("An error occured while getting the date of the match: {0}", exception);
            }
            return matchDate;
        }

        public List<MatchModel> GenerateMatchModelFromSync(List<Match> matches)
        {
            var matchModels = new List<MatchModel>();

            //if (matches == null)
            //{
            //    return null;
            //}
            foreach (var match in matches)
            {
                var matchModel = new MatchModel();
                var currentMatch = match;
                currentMatch.SportId = this.DataParams.SportId;
                // Set the match repository
                var databaseMatch = this.IngestMatch(currentMatch);

                currentMatch.Downloaded = this.CheckDownloadedMatch(currentMatch, databaseMatch);
                if (match.Downloaded)
                {
                    continue;
                }

                // I need to get the pause results
                
                matchModels.Add(matchModel);
            }

            return matchModels;
        }
        /// <summary>
        /// The compute match group.
        /// </summary>
        /// <param name="matchGroup">
        /// The match group.
        /// </param>
        public List<MatchModel> GenerateMatchModel(IGrouping<DateTime?, Match> matchGroup)
        {
            var matchModels = new List<MatchModel>();
            
            foreach (var match in matchGroup)
            {
                var matchModel = new MatchModel();
                var currentMatch = match;
                // Set the match repository
                var databaseMatch = this.IngestMatch(currentMatch);

                currentMatch.Downloaded = this.CheckDownloadedMatch(currentMatch, databaseMatch);
                if (match.Downloaded)
                {
                    continue;
                }

                

                matchModels.Add(matchModel);
            }

            return matchModels;
        }

        public List<MatchEvent> GetMatchIncidents(int matchId, List<Player> matchPlayers )
        {
            throw new NotImplementedException();
        }

        public List<Referee> GetMatchReferees(int matchId)
        {
            throw new NotImplementedException();
        }

        public Venue GetMatchVenue(Match matchInfo)
        {
            throw new NotImplementedException();
        }

        public List<PartialResult> GetPartialResultsForMatch(Match matchInfo)
        {
            throw new NotImplementedException();
        }

        public Match IngestMatch(Match currentMatch)
        {
            var matchRepository = new MatchRepository();

            // Let's check if the match is in the database
            var databaseMatch = this.DataParams.Results
                ? matchRepository.GetMatchByMatchIdAndSportId(currentMatch.Id, currentMatch.SportId)
                : matchRepository.GetScheduledMatchByMatchIdAndSportId(currentMatch.Id, currentMatch.SportId);

            if (databaseMatch == null)
            {
                // We must add the match to database
                Logger.InfoFormat("Inserting match {0}  into database", currentMatch.Id);

                matchRepository.InsertOne(currentMatch);
            }
            return databaseMatch;
        }

        /// <summary>
        /// The check downloaded match.
        /// </summary>
        /// <param name="match">
        /// The match.
        /// </param>
        /// <param name="matchStoredLocally">
        /// The database match.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool CheckDownloadedMatch(Match match, Match matchStoredLocally)
        {
            // Setting downloaded to false
            var returnValue = false;
            if (this.DataParams.OperationMode != OperationMode.Maintenance)
            {
                //if (matchStoredLocally != null && this.RemoteMatches.Count == this.LocalMatches.Count)
                //{
                //    Logger.InfoFormat("Match {0}: {1} - {2} already downloaded. No need processing.", match.Id, match.HomeTeamName.Trim(), match.AwayTeamName.Trim());
                //    match.Downloaded = true;

                //    returnValue = true;
                //}

                // Just making sure downloaded is false now that we are in maintenance mode
            }

            return returnValue;
        }

        /// <summary>
        /// The get filtered matches.
        /// </summary>
        /// <param name="matches">
        /// The matches.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Match> GetFilteredMatches(List<Match> matches)
        {
            Logger.InfoFormat("We have found {0} matches.", matches.Count);

            var filteredMatches = this.FilterOnlyAcceptedAndRegisteredMatches(matches);
            filteredMatches = this.FilterTodaysMatches(filteredMatches);

            Logger.InfoFormat("After filtering the information from NIF, we have {0} matches to process", filteredMatches.Count);
            return filteredMatches.Any() ? filteredMatches : null;
        }

        ///// <summary>
        ///// The has results.
        ///// </summary>
        ///// <param name="matches">
        ///// The matches.
        ///// </param>
        ///// <returns>
        ///// The <see cref="bool"/>.
        ///// </returns>
        // private static bool HasResults(List<Match> matches)
        // {
        // return matches.Any(match => match.HomeTeamGoals > 0 || match.AwayTeamGoals > 0);
        // }

        
        /// <summary>
        /// The filter matches.
        ///     1   G   Godkjent
        ///     2   IR  Ikke registrert
        ///     3   R   Registrert
        /// </summary>
        /// <param name="matches">
        /// The matches.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Match> FilterOnlyAcceptedAndRegisteredMatches(List<Match> matches)
        {
            try
            {
                var filteredMatches = this.DataParams.Results ? (from m in matches where m.ResultStatusId == 3 || m.ResultStatusId == 1 select m).ToList() : matches;

                if (!filteredMatches.Any())
                {
                    filteredMatches = this.DataParams.Results ? (from m in matches where m.StatusCode == "R" || m.StatusCode == "G" select m).ToList() : matches;
                }

                return filteredMatches;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);

                return new List<Match>();
            }
        }

        public List<Match> FilterTodaysMatches(List<Match> matches)
        {
            // Also filter only todays matches, if startdate and end-date are equal
            return this.DataParams.DateStart == this.DataParams.DateEnd ? matches.Where(x => x.MatchDate == this.DataParams.DateStart).ToList() : matches;
        }

        public List<Match> GetDatabaseMatches(Tournament tournament)
        {
            var testing = Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]);

            var matchRepository = new MatchRepository();

            var startDate = this.SetDate(testing);
            var endDate = this.SetDate(testing);

            var databaseMatches = matchRepository.GetMatchesByDateInterval(startDate, endDate, tournament.Id);

            return databaseMatches;
        }
        /// <summary>
        /// The get filtered database matches.
        /// </summary>
        /// <param name="matches">
        /// The list of matches we've found in the database.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Match> GetFilteredDatabaseMatches(List<Match> matches)
        {
            var filteredDatabaseMatches = new List<Match>();
            if (matches != null)
            {
                filteredDatabaseMatches = (from m in matches where m.Downloaded select m).ToList();
            }

            Logger.Debug("Number of files downloaded: " + filteredDatabaseMatches.Count);

            return filteredDatabaseMatches;
        }

        /// <summary>
        /// The set date.
        /// </summary>
        /// <param name="testing">
        /// The testing.
        /// </param>
        /// <returns>
        /// The <see cref="DateTime"/>.
        /// </returns>
        public DateTime SetDate(bool testing)
        {
            var startDate = DateTime.Today;

            if (testing)
            {
                startDate = DateTime.Parse(ConfigurationManager.AppSettings["niftestingdate"]);
            }

            return startDate;
        }

        /// <summary>
        /// The compare local and remote lists.
        /// </summary>
        /// <param name="remoteMatches">
        /// The remote matches.
        /// </param>
        /// <param name="localMatches">
        /// The local matches.
        /// </param>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool CompareLocalAndRemoteLists(List<Match> remoteMatches, List<Match> localMatches, Tournament tournament)
        {
            if (localMatches.Count != remoteMatches.Count)
            {
                return false;
            }

            Logger.InfoFormat("All matches for tournament {0} ({1}) ) has been downloaded and processed", tournament.Name, tournament.Id);
            return true;
        }
    }
}
