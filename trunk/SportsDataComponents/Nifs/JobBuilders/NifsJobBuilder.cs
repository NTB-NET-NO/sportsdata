using System;
using System.Collections.Generic;
using System.Configuration;
using log4net;
using NTB.SportsData.Classes.Classes;
using Quartz;

namespace NTB.SportsData.Components.Nifs.JobBuilders
{
    /// <summary>
    /// The nif job builder.
    /// </summary>
    public class NifsJobBuilder : IJob
    {
        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(NifsJobBuilder));

        /// <summary>
        /// Initializes static members of the <see cref="NifsJobBuilder"/> class.
        /// </summary>
        static NifsJobBuilder()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository()
                .Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        // This class shall be used to get the matches from Norwegian Soccer Federation

        /// <summary>
        /// The execute.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public void Execute(IJobExecutionContext context)
        {
            Logger.Info("Executing JobExecutionContext: " + context.JobDetail.Description);
            var outputFolder = new List<string>();

            var createXml = false;
            var insertDatabase = false;
            var results = false;
            var dateOffset = DateTime.Now;
            var duration = 0;
            var sportId = 0;
            var federationId = 0;

            // Mapping information sent from Component
            var dataMap = context.JobDetail.JobDataMap;

            // Populate variables to be used to 
            try
            {
                outputFolder.Add(dataMap.GetString("OutputFolder"));
                insertDatabase = dataMap.GetBoolean("InsertDatabase");
                Logger.Debug(insertDatabase ? "InsertDatabase: true" : "InsertDatabase: false");

                var scheduleType = dataMap.GetString("ScheduleType");
                Logger.Debug("ScheduleType: " + scheduleType);

                results = dataMap.GetBoolean("Results");
                Logger.Debug(results ? "Results: true" : "Results: false");

                var scheduleMessage = dataMap.GetBoolean("ScheduleMessage");
                Logger.Debug(scheduleMessage ? "ScheduleMessage: true" : "ScheduleMessage: false");

                duration = dataMap.GetInt("Duration");
                Logger.Debug("Duration: " + duration);

                createXml = dataMap.GetBoolean("CreateXML");
                Logger.Debug(createXml ? "CreateXML: true" : "CreateXML: false");

                sportId = dataMap.GetInt("SportId");

                federationId = dataMap.GetInt("FederationId");

                if (dataMap.Keys.Contains("DateOffset"))
                {
                    dateOffset = dataMap.GetDateTime("DateOffset");
                    Logger.Debug("DateOffset: " + dateOffset.ToShortDateString());
                }
            }
            catch (Exception exception)
            {
                Logger.Debug(exception);
                Logger.Debug(exception.StackTrace);
            }

            Logger.Debug("Outputfolder: " + outputFolder);

            if (insertDatabase)
            {
                Logger.Debug("We shall insert data in the database");

                // NifSingleSportDataFileCreator.UpdateDatabase = true;
            }

            // This parameter tells us if we are to add results-tags to the XML
            if (results)
            {
                Logger.Debug("We shall also render result");

                // NifSingleSportDataFileCreator.RenderResult = true;
            }

            // Creating the DataFileCreator object
            var dataFileParams = new DataFileParams
            {
                FileOutputFolder = outputFolder,
                SportId = sportId,
                FederationId = federationId,
                CreateXml = createXml
            };

            Logger.Debug("Duration: " + duration);
            if (duration > 0)
            {
                Logger.Debug("We shall create a longer period of matches");
                dataFileParams.Duration = duration;

                if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                {
                    dataFileParams.DateStart = DateTime.Parse(ConfigurationManager.AppSettings["nifstestingdate"]);
                }

                dataFileParams.DateEnd = dataFileParams.DateStart.AddDays(duration);
            }
            else if (duration < 0)
            {
                var datestart = DateTime.Now.AddDays(duration);

                dataFileParams.DateStart = datestart;

                dataFileParams.DateEnd = DateTime.Today;
            }
            else
            {
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                {
                    dataFileParams.DateStart = DateTime.Parse(ConfigurationManager.AppSettings["niftestingdate"]);
                    dataFileParams.DateEnd = DateTime.Parse(ConfigurationManager.AppSettings["niftestingdate"]);
                }
                else
                {
                    dataFileParams.DateStart = DateTime.Today;
                    dataFileParams.DateEnd = DateTime.Today;

                    if (dateOffset != DateTime.Today)
                    {
                        dataFileParams.DateStart = dateOffset;
                        dataFileParams.DateEnd = dateOffset;
                    }

                    Logger.Debug("Date Start: " + dataFileParams.DateStart);
                    Logger.Debug("Date End: " + dataFileParams.DateEnd);
                }
            }


            if (createXml)
            {
                Logger.Debug("We are about to create XML File");
            }

            if (results)
            {
                return;
            }
        }
    }
}