﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TeamSportFacade.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nifs.Facade.Interfaces;
using NTB.SportsData.Components.RemoteRepositories.DataMappers;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;
using AgeCategoryMapper = NTB.SportsData.Components.Nifs.Mappers.AgeCategoryMapper;
using ChangeInfo = NTB.SportsData.Classes.Classes.ChangeInfo;
using Match = NTB.SportsData.Classes.Classes.Match;
using PartialResult = NTB.SportsData.Classes.Classes.PartialResult;
using Referee = NTB.SportsData.Classes.Classes.Referee;
using Result = NTB.SportsData.Classes.Classes.Result;
using TeamResult = NTB.SportsData.Classes.Classes.TeamResult;
using Tournament = NTB.SportsData.Classes.Classes.Tournament;

namespace NTB.SportsData.Components.Nifs.Facade
{
    /// <summary>
    ///     The team sport facade.
    /// </summary>
    public class NifsFacade : INifsFacade
    {
        /// <summary>
        ///     The sync remote data mapper
        /// </summary>
        private readonly ISynchRemoteRepository _syncRemoteDataMapper;

        /// <summary>
        ///     The match incident data mapper.
        /// </summary>
        private readonly IMatchIncidentDataMapper _matchIncidentDataMapper;

        /// <summary>
        ///     The match remote data mapper.
        /// </summary>
        private readonly IMatchRemoteDataMapper _matchRemoteDataMapper;

        /// <summary>
        ///     The referee remote repository.
        /// </summary>
        private readonly IRefereeRemoteDataMapper _refereeRemoteRepository;

        /// <summary>
        ///     The team remote data mapper.
        /// </summary>
        private readonly ITeamRemoteDataMapper _teamRemoteDataMapper;

        /// <summary>
        ///     The team result data mapper.
        /// </summary>
        private readonly ITeamResultDataMapper _teamResultDataMapper;

        /// <summary>
        ///     The tournament remote data mapper.
        /// </summary>
        private readonly ITournamentRemoteDataMapper _tournamentRemoteDataMapper;

        /// <summary>
        ///     Initializes a new instance of the <see cref="NifsFacade" /> class.
        ///     Initializes a new instance of the <see cref="SingleSportFacade" /> class.
        /// </summary>
        public NifsFacade()
        {
            this._syncRemoteDataMapper = new SynchRemoteRepository();
            this._teamResultDataMapper = new TeamResultRemoteRepository();
            this._matchRemoteDataMapper = new MatchRemoteRepository();
            this._refereeRemoteRepository = new RefereeRemoteRepository();
            this._tournamentRemoteDataMapper = new TournamentRemoteRepository();
            this._matchIncidentDataMapper = new MatchIncidentRemoteRepository();
            this._teamRemoteDataMapper = new TeamRemoteRepository();
        }

        /// <summary>
        ///     The get tournament by id.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        ///     The <see cref="NTB.SportsData.Classes.Classes.Tournament" />.
        /// </returns>
        public Tournament GetTournamentById(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     The get partial results by match id.
        /// </summary>
        /// <param name="matchId">
        ///     The match id.
        /// </param>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<PartialResult> GetPartialResultsByMatchId(int matchId)
        {
            throw new NotImplementedException();
        }

        public List<AgeCategory> GetTournamentAgeCategory(int tournamentId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     The get changes team sport.
        /// </summary>
        /// <param name="userKey">
        ///     The user key.
        /// </param>
        /// <param name="clientPassword">
        ///     The client password.
        /// </param>
        /// <param name="eventStartDate">
        ///     The event start date.
        /// </param>
        /// <param name="eventEndDate">
        ///     The event end date.
        /// </param>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<ChangeInfo> GetMatchStatus(DateTime eventStartDate,
            DateTime eventEndDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     The get match result by result id.
        /// </summary>
        /// <param name="resultId">
        ///     The result id.
        /// </param>
        /// <returns>
        ///     The <see cref="NTB.SportsData.Classes.Classes.Result" />.
        /// </returns>
        public Result GetMatchResultByResultId(int resultId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     The get match info by match id.
        /// </summary>
        /// <param name="matchId">
        ///     The match id.
        /// </param>
        /// <returns>
        ///     The <see cref="NTB.SportsData.Classes.Classes.Match" />.
        /// </returns>
        public Match GetMatchInfoByMatchId(int matchId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     The get match referees.
        /// </summary>
        /// <param name="matchId">
        ///     The match id.
        /// </param>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Referee> GetMatchReferees(int matchId)
        {
            throw new NotImplementedException();
        }

        public List<Match> SearchMatchesByDateAndTournamentId(int tournamentId, DateTime startDate, DateTime endDate)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        ///     The get matches by tournament id and date.
        /// </summary>
        /// <param name="tournamentId">
        ///     The tournament id.
        /// </param>
        /// <param name="dateStart">
        ///     The date start.
        /// </param>
        /// <param name="dateEnd">
        ///     The date end.
        /// </param>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Match> GetMatchesByTournamentIdAndDate(int tournamentId, DateTime dateStart, DateTime dateEnd)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     The get tournament standing by tournament id.
        /// </summary>
        /// <param name="tournamentId">
        ///     The tournament id.
        /// </param>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<TeamResult> GetTournamentStandingByTournamentId(int tournamentId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     The get match incident by match id.
        /// </summary>
        /// <param name="matchId">
        ///     The match id.
        /// </param>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<MatchEvent> GetMatchIncidentByMatchId(int matchId)
        {
            throw new NotImplementedException();

        }

        /// <summary>
        ///     The get match players by match id.
        /// </summary>
        /// <param name="matchId">
        ///     The match id.
        /// </param>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Player> GetMatchPlayersByMatchId(int matchId)
        {
            throw new NotImplementedException();
        }

        #region Static Fields

        /// <summary>
        ///     Creating the static Error Logger
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        /// <summary>
        ///     Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(NifsFacade));

        #endregion
    }
}