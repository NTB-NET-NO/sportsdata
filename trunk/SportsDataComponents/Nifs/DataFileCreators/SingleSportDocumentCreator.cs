﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NifSingleSportDocumentCreator.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The nif single sport document creator.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using log4net;
using NTB.SportsData.Classes.Classes;

namespace NTB.SportsData.Components.Nifs.DataFileCreators
{
    /// <summary>
    /// The nif single sport document creator.
    /// </summary>
    public class SingleSportDocumentCreator
    {
        #region Static Fields

        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SingleSportDocumentCreator));

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets a value indicating whether render result.
        /// </summary>
        public bool RenderResult { get; set; }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the athlete node.
        /// </summary>
        private XmlNode AthleteNode { get; set; }

        /// <summary>
        /// Gets or sets the customer node.
        /// </summary>
        private XmlNode CustomerNode { get; set; }

        /// <summary>
        /// Gets or sets the document type node.
        /// </summary>
        private XmlNode DocumentTypeNode { get; set; }

        /// <summary>
        /// Gets or sets the event class node.
        /// </summary>
        private XmlNode AgeCategoryNode { get; set; }

        /// <summary>
        /// Gets or sets the event node.
        /// </summary>
        private XmlNode EventNode { get; set; }

        /// <summary>
        /// Gets or sets the file node.
        /// </summary>
        private XmlNode FileNode { get; set; }

        /// <summary>
        /// Gets or sets the header node.
        /// </summary>
        private XmlNode HeaderNode { get; set; }

        /// <summary>
        /// Gets or sets the organization node.
        /// </summary>
        private XmlNode OrganizationNode { get; set; }

        /// <summary>
        /// Gets or sets the ranking node.
        /// </summary>
        private XmlNode RankingNode { get; set; }

        /// <summary>
        /// Gets or sets the sport node.
        /// </summary>
        private XmlNode SportNode { get; set; }

        /// <summary>
        /// Gets or sets the VenueExtended node.
        /// </summary>
        private XmlNode VenueNode { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Create Customer Node creates a list of customers, those who shall receive these results
        /// </summary>
        /// <param name="customers">
        /// List of customers
        /// </param>
        public void CreateCustomerNode(List<SingleSportCustomer> customers)
        {
            Logger.Debug("Creating Customer Node");
            var doc = new XmlDocument();
            var xmlCustomers = doc.CreateElement("Customers");

            // Now we shall add customers
            if (customers == null)
            {
                return;
            }

            foreach (var customer in customers)
            {
                var xmlElement = doc.CreateElement("Customer");
                var xmlCustomerName = doc.CreateElement("CustomerName");
                xmlCustomerName.SetAttribute("Id", customer.Id.ToString());
                xmlCustomerName.SetAttribute("RemoteCustomerId", customer.RemoteCustomerId.ToString());
                var xmlText = doc.CreateTextNode(customer.Name);
                xmlCustomerName.AppendChild(xmlText);
                xmlElement.AppendChild(xmlCustomerName);
                xmlCustomers.AppendChild(xmlElement);
            }

            doc.AppendChild(xmlCustomers);

            this.CustomerNode = doc;
        }

        /// <summary>
        /// Create Filename Base creates the base of the filename. So we can add what type of result this is later
        /// </summary>
        /// <param name="organization">
        /// which organization is organizing the sport
        /// </param>
        /// <param name="sportEvent">
        /// which event this is the result for
        /// </param>
        /// <param name="dateTime">
        /// date time of file generation / date of event
        /// </param>
        /// <returns>
        /// string containing the filename base
        /// </returns>
        public string CreateFileNameBase(Federation organization, SportEvent sportEvent, DateTime dateTime)
        {
            // Creeate the file date string with 0'pads to the left
            var fileDate = dateTime.Year + dateTime.Month.ToString().PadLeft(2, '0') + dateTime.Day.ToString().PadLeft(2, '0');

            // Return the Filename string
            return "NTB_SportsData_" + fileDate + "_" + organization.NameShort + "_E" + sportEvent.EventId + "_C" + sportEvent.ClassExerciseId;
        }

        /// <summary>
        ///     Create Header Node creates the main Header root node
        /// </summary>
        public void CreateHeaderNode()
        {
            var doc = new XmlDocument();

            var xmlElement = doc.CreateElement("Header");

            doc.AppendChild(xmlElement);

            this.HeaderNode = doc;
        }

        /// <summary>
        /// Create Organization Node creates the XML part with information about the organization.
        ///     This information is later used when creating the file
        /// </summary>
        /// <param name="organization">
        /// Organization Object containing the information
        /// </param>
        public void CreateOrganizationNode(Federation organization)
        {
            var doc = new XmlDocument();
            var xmlElement = doc.CreateElement("Organisation");
            xmlElement.SetAttribute("name", organization.NameShort);
            xmlElement.SetAttribute("id", organization.Id.ToString());
            xmlElement.InnerText = organization.Name;

            doc.AppendChild(xmlElement);

            this.OrganizationNode = doc;
        }

        /// <summary>
        /// The get document type.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetDocumentType()
        {
            return string.Empty;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The create athlete nodes.
        /// </summary>
        /// <param name="list">
        /// The list of athletes.
        /// </param>
        /// <param name="singleSportCustomers">
        /// The list of customers.
        /// </param>
        internal void CreateAthleteNodes(Dictionary<int, List<SportResult>> list, List<SingleSportCustomer> singleSportCustomers)
        {
            var doc = new XmlDocument();

            var classExerciseNode = doc.CreateElement("ClassExercise");
            foreach (var kvp in list)
            {
                var xmlElement = doc.CreateElement("Rankings");
                var classExerciseId = kvp.Key;
                var sportResults = kvp.Value;

                classExerciseNode.SetAttribute("id", classExerciseId.ToString());

                var classExerciseName = sportResults[0].ClassExerciseName;
                classExerciseNode.SetAttribute("name", classExerciseName);

                foreach (var sportResult in sportResults)
                {
                    var xmlRanking = doc.CreateElement("Ranking");
                    xmlRanking.SetAttribute("rank", sportResult.Rank.ToString());

                    var xmlAthlete = doc.CreateElement("Athlete");
                    xmlAthlete.SetAttribute("firstname", sportResult.FirstName.Trim());
                    xmlAthlete.SetAttribute("lastname", sportResult.LastName.Trim());

                    xmlAthlete.SetAttribute("id", sportResult.PersonId.ToString());
                    xmlAthlete.SetAttribute("competitorid", sportResult.CompetitorId.ToString());
                    xmlAthlete.SetAttribute("startnumber", sportResult.StartNo.ToString());
                    xmlAthlete.SetAttribute("gender", sportResult.PersonGender);
                    xmlAthlete.SetAttribute("clubname", sportResult.Club.Trim());
                    xmlAthlete.SetAttribute("clubid", sportResult.ClubId.ToString());

                    var athlete1 = sportResult;
                    foreach (var c in from c in singleSportCustomers from d in c.CustomerRegions where d.Name.Trim() == athlete1.RegionName.Trim() select c)
                    {
                        var xmlAthleteCustomer = doc.CreateElement("AthleteCustomer");

                        xmlAthleteCustomer.SetAttribute("customer", c.Name);
                        xmlAthleteCustomer.SetAttribute("customerid", c.Id.ToString());
                        xmlAthleteCustomer.SetAttribute("customerremoteid", c.RemoteCustomerId.ToString());

                        xmlAthlete.AppendChild(xmlAthleteCustomer);
                    }

                    var xmlTime = doc.CreateElement("Time");
                    xmlTime.SetAttribute("time", sportResult.Time.ToString());
                    xmlTime.SetAttribute("timebehind", sportResult.TimeBehind.ToString());

                    xmlRanking.AppendChild(xmlAthlete);
                    xmlRanking.AppendChild(xmlTime);

                    xmlElement.AppendChild(xmlRanking);
                    
                }
                classExerciseNode.AppendChild(xmlElement);
            }

            doc.AppendChild(classExerciseNode);

            if (this.AthleteNode == null)
            {
                var xmlDoc = new XmlDocument();
                var rootElement = xmlDoc.CreateElement("ClassExercises");
                xmlDoc.AppendChild(rootElement);
                
                var selectedNode = doc.SelectSingleNode("ClassExercise");
                if (selectedNode != null)
                {
                    var classNode = xmlDoc.ImportNode(selectedNode, true);
                    rootElement.AppendChild(classNode);
                }

                this.AthleteNode = xmlDoc;
            }
            else
            {
                var xmlNodes = this.AthleteNode.SelectNodes("ClassExercises");
                var xmlString = new StringBuilder();
                if (xmlNodes != null)
                {
                    foreach (XmlNode xmlNode in xmlNodes)
                    {
                        xmlString.Append(xmlNode.OuterXml);
                    }
                }

                if (xmlString.ToString() == string.Empty)
                {
                    return;
                }

                var tempDoc = new XmlDocument();
                tempDoc.LoadXml(xmlString.ToString());

                var xmlFragment = tempDoc.CreateDocumentFragment();
                xmlFragment.InnerXml = doc.OuterXml;

                if (tempDoc.DocumentElement != null)
                {
                    tempDoc.DocumentElement.AppendChild(xmlFragment);
                }

                this.AthleteNode = tempDoc;
            }
        }

        /// <summary>
        ///     Create Document creates the root node and the rest of the document.
        /// </summary>
        /// <returns>Returns the complete XML Document</returns>
        internal XmlDocument CreateDocument()
        {
            var doc = new XmlDocument();
            var xmlElement = doc.CreateElement("SportsData");
            XmlNode xmlEvent = null;
            XmlNode xmlHeader = null;

            if (this.EventNode.SelectSingleNode("Event") != null)
            {
                xmlEvent = doc.ImportNode(this.EventNode.SelectSingleNode("Event"), true);

                xmlElement.AppendChild(xmlEvent);
            }

            if (this.HeaderNode.SelectSingleNode("Header") != null)
            {
                xmlHeader = doc.ImportNode(this.HeaderNode.SelectSingleNode("Header"), true);
                if (xmlEvent != null)
                {
                    xmlEvent.AppendChild(xmlHeader);
                }
            }

            // if (this.AgeCategoryNode.SelectSingleNode("AgeCategory") != null)
            // {
            // var xmlAgeCategory = doc.ImportNode(this.AgeCategoryNode.SelectSingleNode("AgeCategory"), true);

            // if (xmlHeader != null)
            // {
            // xmlHeader.AppendChild(xmlAgeCategory);
            // }
            // }
            if (this.VenueNode.SelectSingleNode("VenueExtended") != null)
            {
                var xmlVenueNode = doc.ImportNode(this.VenueNode.SelectSingleNode("VenueExtended"), true);

                if (xmlHeader != null)
                {
                    xmlHeader.AppendChild(xmlVenueNode);
                }
            }

            if (this.OrganizationNode.SelectSingleNode("Organisation") != null)
            {
                var xmlOrganization = doc.ImportNode(this.OrganizationNode.SelectSingleNode("Organisation"), true);

                if (xmlHeader != null)
                {
                    xmlHeader.AppendChild(xmlOrganization);
                }
            }

            if (this.CustomerNode != null)
            {
                if (this.CustomerNode.SelectSingleNode("Customers") != null)
                {
                    var xmlCustomerNode = doc.ImportNode(this.CustomerNode.SelectSingleNode("Customers"), true);
                    if (xmlHeader != null)
                    {
                        xmlHeader.AppendChild(xmlCustomerNode);
                    }
                }
            }

            if (this.SportNode.SelectSingleNode("Sport") != null)
            {
                var xmlSportNode = doc.ImportNode(this.SportNode.SelectSingleNode("Sport"), true);
                if (xmlHeader != null)
                {
                    xmlHeader.AppendChild(xmlSportNode);
                }
            }

            if (this.DocumentTypeNode.SelectSingleNode("DocumentType") != null)
            {
                var documentTypeNode = doc.ImportNode(this.DocumentTypeNode.SelectSingleNode("DocumentType"), true);
                if (xmlHeader != null)
                {
                    xmlHeader.AppendChild(documentTypeNode);
                }
            }

            if (this.FileNode.SelectSingleNode("File") != null)
            {
                var fileNode = doc.ImportNode(this.FileNode.SelectSingleNode("File"), true);
                if (xmlHeader != null)
                {
                    xmlHeader.AppendChild(fileNode);
                }
            }

            if (this.AthleteNode != null && this.AthleteNode.SelectSingleNode("ClassExercises") != null)
            {

                var athleteNode = doc.ImportNode(this.AthleteNode.SelectSingleNode("ClassExercises"), true);

                xmlElement.AppendChild(athleteNode);
            }

            // xmlElement.AppendChild(AthleteNode);
            doc.AppendChild(xmlElement);

            // XmlNode athleteNode = doc.ImportNode(AthleteNode.SelectSingleNode("Rankings"), true);
            // doc.AppendChild(athleteNode);
            return doc;
        }

        /// <summary>
        ///     Create Document Node contains information about what type of document this is
        /// </summary>
        internal void CreateDocumentTypeNode()
        {
            var doc = new XmlDocument();
            var xmlElement = doc.CreateElement("DocumentType");

            xmlElement.SetAttribute("type", "sportsresults");

            doc.AppendChild(xmlElement);

            this.DocumentTypeNode = doc;
        }

        /// <summary>
        /// The create age category node.
        /// </summary>
        /// <param name="eventClass">
        /// The event class.
        /// </param>
        internal void CreateAgeCategoryNode(EventClass eventClass)
        {
            var doc = new XmlDocument();
            var xmlElement = doc.CreateElement("AgeCategory");

            if (eventClass.ClassName.Contains("Senior"))
            {
                xmlElement.SetAttribute("name", eventClass.ClassName);
                xmlElement.SetAttribute("id", eventClass.ClassId.ToString());
                xmlElement.InnerText = "senior";
            }
            else
            {
                xmlElement.SetAttribute("name", eventClass.ClassName);
                xmlElement.SetAttribute("id", eventClass.ClassId.ToString());
                xmlElement.InnerText = "aldersbestemt";
            }

            doc.AppendChild(xmlElement);
            this.AgeCategoryNode = doc;
        }

        /// <summary>
        /// The create event node.
        /// </summary>
        /// <param name="eventClass">
        /// The event class.
        /// </param>
        /// <param name="publishDateTime">
        /// The event publish date time.
        /// </param>
        internal void CreateEventNode(EventClass eventClass, DateTime publishDateTime)
        {
            var doc = new XmlDocument();
            var xmlEvent = doc.CreateElement("Event");

            var docid = "E" + eventClass.EventId + "C" + eventClass.ClassId + "_" + publishDateTime.Year + publishDateTime.Month.ToString().PadLeft(2, '0') + publishDateTime.Day.ToString().PadLeft(2, '0');
            xmlEvent.SetAttribute("doc-id", docid);

            xmlEvent.SetAttribute("type", this.RenderResult ? "resultat" : "terminliste");

            var xmlEventMetaData = doc.CreateElement("Event-MetaInfo");
            xmlEventMetaData.SetAttribute("id", eventClass.EventId.ToString());
            xmlEventMetaData.SetAttribute("name", eventClass.EventName);
            
            xmlEventMetaData.SetAttribute("date", publishDateTime.ToString("yyyy-MM-dd") + "T" + publishDateTime.ToString("hh:mm:ss"));

            var xmlClass = doc.CreateElement("Class");
            xmlClass.SetAttribute("id", eventClass.ClassId.ToString());
            xmlClass.SetAttribute("name", eventClass.ClassName);

            xmlEventMetaData.AppendChild(xmlClass);
            xmlEvent.AppendChild(xmlEventMetaData);
            doc.AppendChild(xmlEvent);

            this.EventNode = doc;
        }

        /// <summary>
        /// Create File Node contains the File Element with information about the File
        /// </summary>
        /// <param name="document">
        /// The document object
        /// </param>
        internal void CreateFileNode(SingleSportDocument document)
        {
            var doc = new XmlDocument();
            var xmlElement = doc.CreateElement("File");
            var xmlElementName = doc.CreateElement("Name");
            xmlElementName.SetAttribute("FullName", document.FullName);
            xmlElementName.SetAttribute("Type", "xml");
            xmlElementName.InnerText = document.Filename;

            var xmlCreatedDateTime = doc.CreateElement("Created");
            xmlCreatedDateTime.InnerText = this.CreateDateTimeString(document.Created);

            var xmlVersion = doc.CreateElement("Version");
            xmlVersion.InnerText = document.Version.ToString();

            xmlElement.AppendChild(xmlElementName);
            xmlElement.AppendChild(xmlCreatedDateTime);
            xmlElement.AppendChild(xmlVersion);
            doc.AppendChild(xmlElement);

            this.FileNode = doc;
        }

        /// <summary>
        /// Create Sport Node creates the sport element. We are using the sport inside the organization object
        /// </summary>
        /// <param name="organization">
        /// Organization Object containing the information
        /// </param>
        internal void CreateSportNode(Federation organization)
        {
            if (organization == null)
            {
                return;
            }

            var doc = new XmlDocument();
            var xmlElement = doc.CreateElement("Sport");
            xmlElement.SetAttribute("id", organization.SportId.ToString());
            xmlElement.InnerText = organization.Sport;

            doc.AppendChild(xmlElement);

            this.SportNode = doc;
        }

        /// <summary>
        /// Create VenueExtended Node creates an XML Element containing information about the VenueExtended where the event took place.
        /// </summary>
        /// <param name="venueExtended">
        /// The VenueExtended object
        /// </param>
        internal void CreateVenueNode(VenueExtended venueExtended)
        {
            var doc = new XmlDocument();
            var xmlElement = doc.CreateElement("VenueExtended");
            xmlElement.SetAttribute("place", venueExtended.Place);

            if (venueExtended.Id > 0)
            {
                xmlElement.InnerText = venueExtended.Name.Trim();
            }

            doc.AppendChild(xmlElement);

            this.VenueNode = doc;
        }

        /// <summary>
        /// Write Document will write the document to disk
        /// </summary>
        /// <param name="doc">
        /// XML Document with all information
        /// </param>
        /// <param name="path">
        /// Where we are to store the file
        /// </param>
        internal void WriteDocument(XmlDocument doc, string path)
        {
            var node = doc.SelectSingleNode("/SportsData/Event/Header/File/Name/@FullName");
            var filename = "NTB_SportsData_" + DateTime.Today.Year + DateTime.Today.Month + DateTime.Today.Day;
            if (node != null)
            {
                filename = node.Value;
            }

            var fullname = path + @"\" + filename;

            // Now we shall get path
            doc.Save(fullname);
        }

        /// <summary>
        /// Cretae Date Time String is an internal/private method to create a date time string for the XML in XML format
        /// </summary>
        /// <param name="dateTime">
        /// DateTime object
        /// </param>
        /// <returns>
        /// string with XML-formatted datetime
        /// </returns>
        private string CreateDateTimeString(DateTime dateTime)
        {
            var monthNumber = dateTime.Month.ToString();
            if (monthNumber.Length == 1)
            {
                monthNumber = "0" + dateTime.Month;
            }

            var dayNumber = dateTime.Day.ToString();
            if (dayNumber.Length == 1)
            {
                dayNumber = "0" + dateTime.Day;
            }

            return dateTime.Year + "-" + monthNumber + "-" + dayNumber + "T" + dateTime.ToShortTimeString() + ":00";
        }

        #endregion
    }
}