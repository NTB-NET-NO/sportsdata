﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nifs.Helpers;
using NTB.SportsData.Components.Nifs.Mappers;
using NTB.SportsData.Components.PublicRepositories;
using NTB.SportsData.Components.RemoteRepositories.DataMappers;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;
using NTB.SportsData.Utilities;

namespace NTB.SportsData.Components.Nifs.DataFileCreators
{
    /// <summary>
    /// The nif tournament data file creator.
    /// </summary>
    public class TournamentDataFileCreator : IDataFileCreator<Tournament>
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(TournamentDataFileCreator));


        /// <summary>
        /// Gets or sets the team data file params.
        /// </summary>
        public DataFileParams TeamDataFileParams { get; set; }

        /// <summary>
        /// The configure.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// throws an exception if something goes wrong
        /// </exception>
        public bool Configure()
        {
            if (this.TeamDataFileParams == null)
            {
                throw new SportsDataException("DataFileParams is not set!");
            }

            try
            {
                Logger.Debug("Configuring DatafileCreator");

                // Check if Season table is populated
                if (!FindSeasonBySportId(this.TeamDataFileParams.SportId))
                {
                    var remoteSeasonRepository = new SeasonRemoteRepository();
                    var federationSeasons = remoteSeasonRepository.GetSeasonsByOrgId(this.TeamDataFileParams.FederationId);

                    var seasonMapper = new FederationSeasonMapper();
                    var seasons = federationSeasons.Select(row => seasonMapper.Map(row, new Season()))
                        .ToList();

                    var seasonRepository = new SeasonRepository();
                    seasonRepository.InsertAll(seasons);
                }

                // need to check if we have an active season, otherwise we must tell the administrator to fix this in admin
                var listOfSeasons = GetSeasonsBySportId(this.TeamDataFileParams.SportId)
                    .ToList();

                if (!listOfSeasons.Any())
                {
                    throw new SportsDataException("No seasons found!");
                }

                Logger.DebugFormat("Number of seasons: {0}", listOfSeasons.Count());

                var filteredSeason = new Season();
                var seasonHelper = new SeasonHelper();
                try
                {
                    if (this.TeamDataFileParams.DisciplineId > 0)
                    {
                        seasonHelper.DisciplineId = this.TeamDataFileParams.DisciplineId;
                        filteredSeason = seasonHelper.GetActiveSeasonWithDisciplineId(listOfSeasons);
                    }
                    else
                    {
                        // filteredSeason = (from f in listOfSeasons where f.Active select f).Single();
                        filteredSeason = seasonHelper.GetActiveSeasonWithDisciplineId(listOfSeasons);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    filteredSeason = null;
                    var mailMessage = string.Format("An error occured while getting season for federation with Id: {0}. Job {1}",
                        this.TeamDataFileParams.FederationId, this.TeamDataFileParams.InstanceName);
                    Mail.SendMail(mailMessage);
                    Mail.SendException(exception);
                }

                if (filteredSeason == null)
                {
                    // We cannot procede with no active seasons. So we throw an exception which will trigger an e-mail to administrator
                    throw new SportsDataException("No active seasons for this federation. You have to go to Admin and set up the seasons for this federation!");
                }

                // Check if Tournament table for this organization or sport is populated
                if (!FindTournamentsBySportId(this.TeamDataFileParams.SportId))
                {
                    // No tournaments were found for this sport, we'll add them to the database
                    var remoteTournamentRepository = new TournamentRemoteRepository();
                    var seasonTournaments = remoteTournamentRepository.GetSeasonTournaments(filteredSeason.Id);

                    var tournamentRepository = new TournamentRepository();
                    var seasonTournamentMapper = new SeasonTournamentMapper();
                    var tournaments = seasonTournaments.Select(row => seasonTournamentMapper.Map(row, new Tournament()))
                        .ToList();

                    var items = new List<Tournament>();
                    foreach (var tournament in tournaments)
                    {
                        tournament.SportId = this.TeamDataFileParams.SportId;
                        items.Add(tournament);
                    }

                    tournamentRepository.InsertAll(items);
                }

                if (!FindTournamentsBySeasonId(filteredSeason.Id))
                {
                    // No tournaments were found for this sport, we'll add them to the database
                    var remoteTournamentRepository = new TournamentRemoteRepository();
                    var seasonTournaments = remoteTournamentRepository.GetSeasonTournaments(filteredSeason.Id);

                    var tournamentRepository = new TournamentRepository();
                    var seasonTournamentMapper = new SeasonTournamentMapper();
                    var tournaments = seasonTournaments.Select(row => seasonTournamentMapper.Map(row, new Tournament()))
                        .ToList();

                    var items = new List<Tournament>();
                    foreach (var tournament in tournaments)
                    {
                        tournament.SportId = this.TeamDataFileParams.SportId;
                        items.Add(tournament);
                    }

                    tournamentRepository.InsertAll(items);
                }

                return true;
            }
            catch (Exception e)
            {
                Logger.Error("An exception has occured: " + e);
                return false;
            }
        }

        /// <summary>
        /// The find.
        /// </summary>
        /// <param name="remoteTournaments">
        /// The remote tournaments.
        /// </param>
        /// <param name="tournaments">
        /// The tournaments.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// throws an exception because the method is not implemented
        /// </exception>
        public List<Tournament> Find(List<Tournament> remoteTournaments, List<Tournament> tournaments)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get remote by org id.
        /// </summary>
        /// <param name="dateStart">
        /// The date start.
        /// </param>
        /// <param name="dateEnd">
        /// The date end.
        /// </param>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetRemoteByOrgId(DateTime dateStart, DateTime dateEnd, int orgId)
        {
            var organizationRepository = new FederationRepository();
            var organization = organizationRepository.GetFederationById(orgId);

            var seasonRepository = new SeasonRepository();
            var seasons = seasonRepository.GetSeasonsBySportId(organization.SportId);

            var seasonId = (from s in seasons where s.Active select s.Id).Single();

            var tournamentRemoteRepository = new TournamentRemoteRepository();
            var seasonTournaments = tournamentRemoteRepository.GetSeasonTournaments(seasonId);

            var seasonTournamentMapper = new SeasonTournamentMapper();
            var tournaments = seasonTournaments.Select(row => seasonTournamentMapper.Map(row, new Tournament()))
                .ToList();

            return tournaments;
        }

        /// <summary>
        /// The get local by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// throws an exception because the method is not implemented
        /// </exception>
        public List<Tournament> GetLocalBySportId(int sportId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// throws an exception because the method is not implemented
        /// </exception>
        List<Tournament> IDataFileCreator<Tournament>.Get()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Insert is used to populate the Tournament Table in the database
        /// </summary>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        public void InsertOne(Tournament tournament)
        {
            var tournamentRepository = new TournamentRepository();
            tournamentRepository.InsertOne(tournament);
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="tournaments">
        /// The tournaments.
        /// </param>
        public void InsertAll(List<Tournament> tournaments)
        {
            var tournamentRepository = new TournamentRepository();
            tournamentRepository.InsertAll(tournaments);
        }

        /// <summary>
        /// The create result.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// throws an exception because the method is not implemented
        /// </exception>
        public void CreateResult(List<Tournament> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get customer tournaments.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetCustomerTournaments(int sportId, int seasonId)
        {
            var repository = new TournamentRepository();
            return repository.GetCustomerTournaments(sportId, seasonId);
        }

        /// <summary>
        /// The get seasons by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>IEnumerable</cref>
        ///     </see>
        ///     .
        /// </returns>
        private static IEnumerable<Season> GetSeasonsBySportId(int sportId)
        {
            var repository = new SeasonRepository();

            return repository.GetSeasonsBySportId(sportId);
        }

        /// <summary>
        /// The find season by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool FindSeasonBySportId(int sportId)
        {
            var repository = new SeasonRepository();
            var seasons = repository.GetSeasonsBySportId(sportId);

            return seasons.Any();
        }

        /// <summary>
        /// The find tournaments by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool FindTournamentsBySportId(int sportId)
        {
            var repository = new TournamentRepository();
            var tournaments = repository.GetTournamentsBySportId(sportId);

            return tournaments != null && tournaments.Any();
        }

        /// <summary>
        /// The find tournaments by season id.
        /// </summary>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool FindTournamentsBySeasonId(int seasonId)
        {
            var repository = new TournamentRepository();
            var tournaments = repository.GetTournamentsBySeasonId(seasonId);

            return tournaments != null && tournaments.Any();
        }
    }
}