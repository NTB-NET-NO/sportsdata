﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NIFSingleSportDataFileCreator.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The nif data file creator.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using log4net;
using log4net.Config;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Classes.Enums;
using NTB.SportsData.Components.Nif.Facade;
using NTB.SportsData.Components.PublicRepositories;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;

namespace NTB.SportsData.Components.Nifs.DataFileCreators
{
    using EventClass = Classes.Classes.EventClass;
    using VenueExtended = Classes.Classes.VenueExtended;

    /// <summary>
    /// The nif data file creator.
    /// </summary>
    public class NifSingleSportDataFileCreator : IDataFileCreator<SportEvent>
    {
        #region Static Fields

        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(NifSingleSportDataFileCreator));

        #endregion

        #region Fields

        /// <summary>
        /// The _facade.
        /// </summary>
        private readonly ISingleSportFacade facade;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="NifSingleSportDataFileCreator"/> class.
        /// </summary>
        public NifSingleSportDataFileCreator()
        {
            this.facade = new SingleSportFacade();

            // Set up logger
            XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                BasicConfigurator.Configure();
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the data file params.
        /// </summary>
        public DataFileParams DataFileParams { get; set; }

        #endregion

        /// <summary>
        /// The get event information.
        /// </summary>
        /// <param name="sportEvent">
        /// The sport event.
        /// </param>
        /// <returns>
        /// The <see cref="SportEvent"/>.
        /// </returns>
        public SportEvent GetEventInformation(SportEvent sportEvent)
        {
            var sportEventInfo = this.facade.GetEventInfo(sportEvent.EventId);

            sportEvent.EventLocation = sportEventInfo.EventLocation;
            sportEvent.EventTimeStart = sportEventInfo.EventTimeStart;
            sportEvent.EventTimeEnd = sportEventInfo.EventTimeEnd;
            sportEvent.ActivityId = sportEventInfo.ActivityId;
            sportEvent.EventOrganizerId = sportEventInfo.EventOrganizerId;
            sportEvent.EventName = sportEventInfo.EventName;
            sportEvent.EventDateStart = sportEventInfo.EventDateStart;
            sportEvent.EventDateEnd = sportEventInfo.EventDateEnd;

            return sportEvent;
        }

        /// <summary>
        /// The get todays events.
        /// </summary>
        /// <param name="federationKey">
        /// The federation key.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public Dictionary<SportEvent, List<SportResult>> GetTodaysResults(int federationKey)
        {
            try
            {
                var userName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                var userPassword = ConfigurationManager.AppSettings["NIFServicePassword"];
                var userKey = ConfigurationManager.AppSettings["NIFServiceKey"];

                var federationUserKey = userName.Replace(userKey, federationKey.ToString());

                var resultCheck = this.facade.CheckSyncSportResult(federationUserKey, userPassword, this.DataFileParams.DateStart, this.DataFileParams.DateEnd);

                // if we return true, we shall return null
                if (resultCheck)
                {
                    Logger.Info("We have received a list of events. We wait another round and check again");
                    return null;
                }

                // Get the results
                this.DataFileParams.DateStart = DateTime.Parse(this.DataFileParams.DateStart.ToShortDateString());
                var results = this.facade.GetSyncSportResult(federationUserKey, userPassword, this.DataFileParams.DateStart, this.DataFileParams.DateEnd);

                if (!results.Any())
                {
                    // there wasn't any results today, so we are returning
                    Logger.Info("There are no results registered so far today");
                    return null;
                }

                // Get the event based on the result
                var eventId = results.First().EventId;

                // Now we shall get information about the event
                var sportEvent = this.facade.GetEventInfo(eventId);

                var dic = new Dictionary<SportEvent, List<SportResult>> { { sportEvent, results } };

                return dic;
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return new Dictionary<SportEvent, List<SportResult>>();
            }
        }

        #region Public Methods and Operators

        /// <summary>
        /// The configure.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Throws an exception on error
        /// </exception>
        public bool Configure()
        {
            try
            {
                Logger.Debug("Configuring DatafileCreator");

                if (this.DataFileParams == null)
                {
                    throw new Exception("DataFileParams is not set!");
                }

                return true;
            }
            catch (Exception e)
            {
                Logger.Error("An exception has occured: " + e);
                return false;
            }
        }

        /// <summary>
        /// The create event result.
        /// </summary>
        /// <param name="sportEventDictionary">
        /// The sport event dictionary.
        /// </param>
        public void CreateEventResult(Dictionary<SportEvent, List<SportResult>> sportEventDictionary)
        {
            var singleSportCustomers = this.GetSingleSportCustomers();

            foreach (var kvp in sportEventDictionary)
            {
                var sportEvent = kvp.Key;

                // Get the organization information
                var organizationRepository = new FederationRepository();
                var organization = organizationRepository.GetFederationById(sportEvent.AdministrativeOrgId);

                if (organization == null)
                {
                    organization = new Federation { Name = sportEvent.AdministrativeOrgName, SportId = this.DataFileParams.SportId, Id = sportEvent.AdministrativeOrgId, Sport = sportEvent.ActivityName, NameShort = sportEvent.AdministrativeOrgShortName };
                    if (sportEvent.AdministrativeOrgShortName == null)
                    {
                        organization.NameShort = this.CreateShortOrganizationName(organization.Name);
                    }
                }

                // var eventResult = this.facade.GetEventResults(kvp.Key.EventId, this.DataFileParams.FederationId);

                // Create other data
                var venue = new VenueExtended { Name = sportEvent.VenueUnitName, Id = sportEvent.VenueUnitId, Place = sportEvent.EventLocation };

                var creator = new SingleSportDocumentCreator();
                creator.RenderResult = this.DataFileParams.PushResults;

                // Create header node
                creator.CreateHeaderNode();

                // Create the Event Node
                var eventClass = new EventClass { EventId = sportEvent.EventId, EventName = sportEvent.EventName };

                if (sportEvent.EventDateStart != null)
                {
                    creator.CreateEventNode(eventClass, sportEvent.EventDateStart.Value);
                }

                // Set the venue node values
                creator.CreateVenueNode(venue);

                // Create the sport node
                creator.CreateSportNode(organization);

                // Create the organization node
                creator.CreateOrganizationNode(organization);

                // Ordering the results
                var orderedResults = kvp.Value.OrderBy(x => x.ClassExerciseId).ToList();

                var groupedList = (from or in orderedResults group or by or.ClassExerciseId into groupedListOfResults select groupedListOfResults).ToDictionary(gdc => gdc.Key, gdc => gdc.ToList());

                // Looping over the dictionary. 
                foreach (var kvp2 in groupedList)
                {
                    // Let's check if we are to write a result document or something else
                    var customers = new List<SingleSportCustomer>();

                    if (this.DataFileParams.PushResults)
                    {
                        var filteredCustomers = new List<SingleSportCustomer>();

                        foreach (var c in singleSportCustomers)
                        {
                            foreach (var r in c.CustomerRegions)
                            {
                                filteredCustomers.AddRange(from a in kvp2.Value where a.RegionId == r.Id select c);
                            }
                        }

                        customers = filteredCustomers
                            .Select(singleSportCustomer =>
                                new SingleSportCustomer
                                {
                                    Id = singleSportCustomer.Id,
                                    Name = singleSportCustomer.Name,
                                    RemoteCustomerId = singleSportCustomer.RemoteCustomerId,
                                    CustomerRegions = singleSportCustomer.CustomerRegions
                                })
                            .ToList();
                    }
                    else
                    {
                        foreach (var singlesportcustomer in singleSportCustomers)
                        {
                            var customer = new SingleSportCustomer { Id = singlesportcustomer.Id, Name = singlesportcustomer.Name, RemoteCustomerId = singlesportcustomer.RemoteCustomerId };

                            customers.Add(customer);
                        }
                    }

                    // If there are no customers, then we shall just continue.
                    if (!customers.Any())
                    {
                        continue;
                    }

                    // Creating the customer node
                    creator.CreateCustomerNode(customers);

                    var resultEvent = kvp2.Value;

                    var athleteNodeDic = new Dictionary<int, List<SportResult>>()
                                             {
                                                 {
                                                     kvp2.Key,
                                                     kvp2.Value
                                                 }
                                             };

                    creator.CreateAthleteNodes(athleteNodeDic, customers);
                }


                    // Create the document type node
                    creator.CreateDocumentTypeNode();

                    var filename = creator.CreateFileNameBase(organization, sportEvent, this.DataFileParams.DateStart);

                    // now let's check if we have this document in the database
                    var repository = new SingleSportDocumentRepository();
                    var version = repository.GetDocumentVersionByEventId(sportEvent.EventId);

                    var fullFilename = string.Empty;
                    var documentType = DocumentType.Result;
                    if (this.DataFileParams.Results)
                    {
                        if (this.DataFileParams.Results)
                        {
                            fullFilename = filename + "_Res_V" + version + ".xml";
                        }
                    }
                    else if (this.DataFileParams.Results == false)
                    {
                        fullFilename = filename + "_Sched_V" + version + ".xml";
                        documentType = DocumentType.Schedule;
                    }

                    var document = new SingleSportDocument
                    {
                        Created = DateTime.Today,
                        Filename = filename,
                        SportId = this.DataFileParams.SportId,
                        EventId = sportEvent.EventId,
                        Version = version,
                        FullName = fullFilename,
                        DocumentType = documentType
                    };

                    if (version == 1)
                    {
                        repository.InsertOne(document);
                    }
                    else
                    {
                        repository.Update(document);
                    }
                
                    // Create the File node
                    creator.CreateFileNode(document);

                    var doc = creator.CreateDocument();

                foreach (var outputFolder in this.DataFileParams.FileOutputFolders)
                {
                    creator.WriteDocument(doc, outputFolder.OutputPath);
                }
                    
                }
            }

        /// <summary>
        /// The create event result.
        /// </summary>
        /// <param name="sportEventDictionary">
        /// The results in a dictionary.
        /// </param>
        public void OldCreateEventResult(Dictionary<SportEvent, List<SportResult>> sportEventDictionary)
        {
            // Get single sport customers
            var singleSportCustomers = this.GetSingleSportCustomers();

            foreach (var kvp in sportEventDictionary)
            {
                var sportEvent = kvp.Key;

                // Get the organization information
                var organizationRepository = new FederationRepository();
                var organization = organizationRepository.GetFederationById(sportEvent.AdministrativeOrgId);

                if (organization == null)
                {
                    organization = new Federation { Name = sportEvent.AdministrativeOrgName, SportId = this.DataFileParams.SportId, Id = sportEvent.AdministrativeOrgId, Sport = sportEvent.ActivityName, NameShort = sportEvent.AdministrativeOrgShortName };
                    if (sportEvent.AdministrativeOrgShortName == null)
                    {
                        organization.NameShort = this.CreateShortOrganizationName(organization.Name);
                    }
                }

                // Create other data
                var venue = new VenueExtended { Name = sportEvent.VenueUnitName, Id = sportEvent.VenueUnitId, Place = sportEvent.StartCouncil };

                var sportResults = from k in kvp.Value orderby k.ClassExerciseId select k;

                var groupedResults = from sr in sportResults group sr by sr.ClassExerciseId into sportGroup orderby sportGroup.Key select sportGroup;

                foreach (var groupedResult in groupedResults)
                {
                    var classExerciseId = groupedResult.Key;

                    var results = groupedResult;

                    foreach (var result in results)
                    {
                        var creator = new SingleSportDocumentCreator();
                        creator.RenderResult = this.DataFileParams.PushResults;

                        // Create header node
                        creator.CreateHeaderNode();

                        var eventClass = new EventClass() { EventId = sportEvent.EventId, ClassId = result.ClassExerciseId, ClassName = result.ClassExerciseName, EventName = sportEvent.EventName };
                        creator.CreateAgeCategoryNode(eventClass);

                        // Create the Event Node
                        creator.CreateEventNode(eventClass, this.DataFileParams.DateStart);

                        // Set the venue node values
                        creator.CreateVenueNode(venue);

                        // Create the sport node
                        creator.CreateSportNode(organization);

                        // Create the organization node
                        creator.CreateOrganizationNode(organization);

                        if (!this.DataFileParams.PushResults)
                        {
                            continue;
                        }

                        var filteredCustomers = (from c in singleSportCustomers from r in c.CustomerRegions where r.Id == result.RegionId select c).ToList();

                        // var customerAthletes = (from c in singleSportCustomers from r in c.CustomerRegions where c.CustomerRegions.Where(x => x.Id == r.Id) select r).ToList();

                        // Create the Athlete Node
                        // creator.CreateAthleteNodes(results.ToList(), filteredCustomers);
                    }
                }
            }
        }

        /// <summary>
        /// The create result.
        /// </summary>
        /// <param name="sportEvents">
        /// The sport events.
        /// </param>
        public void CreateResult(List<SportEvent> sportEvents)
        {
            var singleSportCustomers = this.GetSingleSportCustomers();
            foreach (var sportEvent in sportEvents)
            {
                var result = this.facade.GetEventResults(sportEvent.EventId, this.DataFileParams.FederationId);

                // Get the organization information
                var organization = new FederationRepository().GetFederationById(sportEvent.AdministrativeOrgId);

                // Create other data
                var venue = new VenueExtended { Name = sportEvent.VenueUnitName, Id = sportEvent.VenueUnitId, Place = sportEvent.StartCouncil };

                foreach (var kvp in result)
                {
                    kvp.Key.EventId = sportEvent.EventId;
                    kvp.Key.EventName = sportEvent.EventName;

                    var creator = new SingleSportDocumentCreator();
                    creator.RenderResult = this.DataFileParams.PushResults;

                    // Create header node
                    creator.CreateHeaderNode();

                    creator.CreateAgeCategoryNode(kvp.Key);

                    // Create the Event Node
                    creator.CreateEventNode(kvp.Key, this.DataFileParams.DateStart);

                    // Set the venue node values
                    creator.CreateVenueNode(venue);

                    // Create the sport node
                    creator.CreateSportNode(organization);

                    // Create the organization node
                    creator.CreateOrganizationNode(organization);

                    // Let's check if we are to write a result document or something else
                    var customers = new List<SingleSportCustomer>();
                    if (this.DataFileParams.PushResults)
                    {
                        var filteredCustomers = new List<SingleSportCustomer>();

                        foreach (var c in singleSportCustomers)
                        {
                            foreach (var r in c.CustomerRegions)
                            {
                                filteredCustomers.AddRange(from a in kvp.Value where a.Club.RegionName == r.Name select c);
                            }
                        }

                        // Create the Athlete Node
                        // creator.CreateAthleteNodes(result.ToList(), singleSportCustomers);

                        // todo: add to athlete which customer this athlete "belongs" to.

                        /*
                         * 
                         * foreach (var singleSportCustomer in filteredCustomers)
                        {
                            var customer = new Customer
                            {
                                Id = singleSportCustomer.Id,
                                Name = singleSportCustomer.Name,
                                RemoteCustomerId = singleSportCustomer.RemoteCustomerId
                            };

                            customers.Add(customer);
                        }
                         * The LINQ below replaces the foreach above
                         */
                        customers = filteredCustomers.Select(singleSportCustomer => new SingleSportCustomer(){ Id = singleSportCustomer.Id, Name = singleSportCustomer.Name, RemoteCustomerId = singleSportCustomer.RemoteCustomerId }).ToList();
                    }
                    else
                    {
                        foreach (var singlesportcustomer in singleSportCustomers)
                        {
                            var customer = new SingleSportCustomer() { Id = singlesportcustomer.Id, Name = singlesportcustomer.Name, RemoteCustomerId = singlesportcustomer.RemoteCustomerId };

                            customers.Add(customer);
                        }
                    }

                    // If there are no customers, then we shall just continue.
                    if (!customers.Any())
                    {
                        continue;
                    }

                    // Creating the customer node
                    creator.CreateCustomerNode(customers);

                    // Creating the file name base
                    sportEvent.ClassExerciseId = kvp.Key.ClassId;
                    sportEvent.ClassExerciseName = kvp.Key.ClassName;

                    var filename = creator.CreateFileNameBase(organization, sportEvent, this.DataFileParams.DateStart);

                    // now let's check if we have this document in the database
                    var repository = new SingleSportDocumentRepository();
                    var version = repository.GetDocumentVersionByEventId(sportEvent.EventId);

                    var fullFilename = string.Empty;
                    var documentType = DocumentType.Result;
                    if (this.DataFileParams.Results)
                    {
                        if (this.DataFileParams.Results)
                        {
                            fullFilename = filename + "_Res_V" + version + ".xml";
                        }
                    }
                    else if (this.DataFileParams.Results == false)
                    {
                        fullFilename = filename + "_Sched_V" + version + ".xml";
                        documentType = DocumentType.Schedule;
                    }

                    var document = new SingleSportDocument { Created = DateTime.Today, Filename = filename, SportId = this.DataFileParams.SportId, EventId = sportEvent.EventId, Version = version, FullName = fullFilename, DocumentType = documentType };

                    if (version == 1)
                    {
                        repository.InsertOne(document);
                    }
                    else
                    {
                        repository.Update(document);
                    }

                    // Create the document type node
                    creator.CreateDocumentTypeNode();

                    // Create the File node
                    creator.CreateFileNode(document);

                    var doc = creator.CreateDocument();

                    foreach (var outputFolder in this.DataFileParams.FileOutputFolder)
                    {
                        creator.WriteDocument(doc, outputFolder);
                    }
                    
                }
            }
        }

        /// <summary>
        /// Finds events. If all are found we return an empty list
        /// </summary>
        /// <param name="remoteEvents">
        /// List of remote events
        /// </param>
        /// <param name="events">
        /// List of events we have in database
        /// </param>
        /// <returns>
        /// List of events we have not added to the database
        /// </returns>
        public List<SportEvent> Find(List<SportEvent> remoteEvents, List<SportEvent> events)
        {
            return (from remoteSportEvent in remoteEvents from sportEvent in events where sportEvent.EventId == remoteSportEvent.EventId select sportEvent).ToList();
        }

        /// <summary>
        /// This method shall get todays events from the database
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<SportEvent> Get()
        {
            // Check if there are any events today. If there is, we shall get results, if not we shall not do a thing
            var eventRespository = new EventRepository();
            var events = eventRespository.GetEventsByOrgIdAndSportId(this.DataFileParams.FederationId, this.DataFileParams.SportId);

            return events;
        }

        /// <summary>
        /// The get local by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public List<SportEvent> GetLocalBySportId(int sportId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get remote by org id.
        /// </summary>
        /// <param name="dateStart">
        /// The date start.
        /// </param>
        /// <param name="dateEnd">
        /// The date end.
        /// </param>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<SportEvent> GetRemoteByOrgId(DateTime dateStart, DateTime dateEnd, int orgId)
        {
            var events = this.facade.GetOrgEvents(orgId, dateStart, dateEnd);

            return events;
        }

        /// <summary>
        /// The insert.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Insert()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        public void InsertAll(List<SportEvent> domainobject)
        {
            foreach (var sportEvent in domainobject)
            {
                this.InsertOne(sportEvent);
            }
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        public void InsertOne(SportEvent domainobject)
        {
            var repository = new EventRepository();
            repository.InsertOne(domainobject);
        }

        /// <summary>
        /// The update one.
        /// </summary>
        /// <param name="domainObject">
        /// The domain object.
        /// </param>
        public void UpdateOne(SportEvent domainObject)
        {
            var repository = new EventRepository();
            repository.Update(domainObject);
        }

        /// <summary>
        ///     Create a short organization name
        /// </summary>
        /// <param name="orgName">string holding the long name</param>
        /// <returns>a short version of the name</returns>
        public string CreateShortOrganizationName(string orgName)
        {
            // Norges Judoforbund = NJF

            var parts = orgName.Split(' ');

            var shortName = parts.Aggregate(string.Empty, (current, part) => current + part[0].ToString().ToUpper());

            if (parts.Last().Contains("forbund"))
            {
                shortName += "F";
            }

            return shortName;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get single sport customers.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        private List<SingleSportCustomer> GetSingleSportCustomers()
        {
            var customerRepository = new CustomerRepository();
            var customers = customerRepository.GetCustomerBySportId(this.DataFileParams.SportId);

            var singleSportCustomers = new List<SingleSportCustomer>();
            foreach (var customer in customers)
            {
                var customerRegions = customerRepository.GetCustomerRegionsBySportId(this.DataFileParams.SportId, customer.Id);
                if (!customerRegions.Any() && ConfigurationManager.AppSettings["livetesting"] == "true")
                {
                    var regionRepository = new RegionRepository();
                    customerRegions = regionRepository.GetAllRegions();
                }

                var singleSportCustomer = new SingleSportCustomer { Id = customer.Id, Name = customer.Name, RemoteCustomerId = customer.RemoteCustomerId, CustomerRegions = customerRegions };

                singleSportCustomers.Add(singleSportCustomer);
            }

            return singleSportCustomers;
        }

        /// <summary>
        /// The get sport by org id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see cref="Sport"/>.
        /// </returns>
        private Sport GetSportByOrgId(int orgId)
        {
            ISportDataMapper sportRepository = new SportRepository();
            return sportRepository.GetSportByOrgId(orgId);
        }

        /// <summary>
        /// Get the list of events by the federation user Id
        /// </summary>
        /// <param name="federationUserId">
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<SportEvent> GetEventList(string federationUserId)
        {
            var facade = new SingleSportFacade();
            var clientPassword = ConfigurationManager.AppSettings["NIFServicePassword"];
            var eventList = facade.GetSyncSportEvents(federationUserId, clientPassword, this.DataFileParams.DateStart, this.DataFileParams.DateEnd);

            // Now we must go over each event and check if the event has happened today or not
            var sportEvents = new List<SportEvent>();
            foreach (var sportEvent in eventList)
            {
                var eventInfo = facade.GetEventInfo(sportEvent.EventId);
                sportEvents.Add(eventInfo);
            }

            // returning a list of events that are happening today
            return (List<SportEvent>)sportEvents.Where(x => x.EventDateStart != null && x.EventDateStart.Value.Date == DateTime.Today.Date);
        }

        /// <summary>
        /// The is event in database.
        /// </summary>
        /// <param name="eventId">
        /// The event id.
        /// </param>
        /// <returns>
        /// The <see cref="SportEvent"/>.
        /// </returns>
        public SportEvent IsEventInDatabase(int eventId)
        {
            var repository = new EventRepository();

            var entry = repository.Get(eventId);

            if (entry == null)
            {
                return null;
            }

            var sportEvent = new SportEvent { EventId = entry.EventId, ActivityId = entry.ActivityId, EventOrganizerId = entry.EventOrganizerId, EventName = entry.EventName, EventLocation = entry.EventLocation, EventDateStart = entry.EventDateStart, EventDateEnd = entry.EventDateEnd };

            return sportEvent;
        }

        /// <summary>
        /// The is event changed.
        /// </summary>
        /// <param name="sportEvent">
        /// The sport event.
        /// </param>
        /// <param name="orgSportEvent">
        /// The org sport event.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsEventChanged(SportEvent sportEvent, SportEvent orgSportEvent)
        {
            if (sportEvent.EventName != orgSportEvent.EventName)
            {
                return true;
            }

            if (sportEvent.EventDateStart != orgSportEvent.EventDateStart)
            {
                return true;
            }

            if (sportEvent.EventDateEnd != orgSportEvent.EventDateEnd)
            {
                return true;
            }

            if (sportEvent.EventLocation != orgSportEvent.EventLocation)
            {
                return true;
            }

            return false;
        }

        #endregion
    }
}