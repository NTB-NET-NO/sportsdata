﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using log4net;
using Newtonsoft.Json;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nifs.Classes.SportsMl;

namespace NTB.SportsData.Components.Nifs.Storages
{
    public class FileStorage
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void SaveJsonFile(SportsContent sportsContent, List<OutputFolder> exportPaths, int stageId,
            Dictionary<string, Article> articles)
        {
            try
            {
                // var json = JsonConvert.SerializeObject(sportsContent, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });

                foreach (var exportPath in exportPaths)
                {
                    var currentSportContent = sportsContent;

                    var currentExportPath = exportPath;

                    Logger.DebugFormat("Current export format: {0}", currentExportPath.OutputPath);
                    if (articles.Keys.Contains(exportPath.Product))
                    {
                        var currentArticle = (from a in articles where a.Key == exportPath.Product select a.Value).Single();

                        currentSportContent.SportsMetaData.Slug = currentArticle.Nitf.Head.Meta[0].Content;

                        currentSportContent.Article = currentArticle;

                        try
                        {
                            var json = JsonConvert.SerializeObject(currentSportContent);

                            var directoryPath = exportPath.OutputPath;

                            var di = new DirectoryInfo(directoryPath);
                            if (!di.Exists)
                            {
                                di.Create();
                            }

                            var filename = string.Format("{0}-{1}.json", currentSportContent.SportsMetaData.DocId, exportPath.PostFix);
                            var outputPath = Path.Combine(directoryPath, filename);

                            Logger.DebugFormat("Writing to: {0}", outputPath);
                            File.WriteAllText(outputPath, json);

                        }
                        catch (Exception exception)
                        {
                            Logger.Error(exception);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }
        }

        public string CreateSlugLine(OutputFolder exportPath, string slugLine)
        {
            var slugLineParts = slugLine.Split('-');
            var parts = slugLineParts.Length-1;

            
            slugLineParts[parts] = exportPath.PostFix;
            var returnString = string.Join("-", slugLineParts);
            
            return returnString;
        }

        public void SaveXmlFile(SportsContent sportsContent, List<OutputFolder> exportPaths, int stageId, Dictionary<string, Article> articles)
        {
            try
            {
                // var json = JsonConvert.SerializeObject(sportsContent, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });

                foreach (var exportPath in exportPaths)
                {
                    var currentSportContent = sportsContent;

                    var currentExportPath = exportPath;

                    Logger.DebugFormat("Current export format: {0}", currentExportPath.OutputPath);
                    if (!articles.Keys.Contains(exportPath.Product))
                    {
                        Logger.InfoFormat("Key {0} was not found, we continue to check for export", exportPath.Product);
                        continue;
                    }
                    var currentArticle = (from a in articles where a.Key == currentExportPath.Product select a.Value).Single();

                    var slugLine = this.CreateSlugLine(exportPath, currentSportContent.SportsMetaData.Slug);

                    if (currentArticle.Nitf.Head.Meta[0].Content != string.Empty)
                    {
                        slugLine = currentArticle.Nitf.Head.Meta[0].Content;
                    }

                    currentSportContent.SportsMetaData.Slug = slugLine;

                    currentSportContent.Article = currentArticle;

                    var stringWriter = new StringWriter();
                    using (var xmlWriter = new XmlTextWriter(stringWriter))
                    {
                        var xmlSerializer = new XmlSerializer(typeof(SportsContent));
                        var ns = new XmlSerializerNamespaces();
                        // ns.Add("sportsml", "http://iptc.org/std/SportsML/2008-04-01/");
                        ns.Add("", "");

                        xmlWriter.Formatting = System.Xml.Formatting.Indented;
                        try
                        {
                            xmlSerializer.Serialize(xmlWriter, currentSportContent, ns);

                            // var rootPath = ConfigurationManager.AppSettings["RootPath"];
                            var directoryPath = exportPath.OutputPath;

                            var di = new DirectoryInfo(directoryPath);
                            if (!di.Exists)
                            {
                                di.Create();
                            }
                            var versionNumber = this.GetVersionNumber(currentSportContent);

                            var filename = string.Format("{0}_V{1}_{2}.xml", currentSportContent.SportsMetaData.DocId, versionNumber, exportPath.PostFix);
                            var outputPath = Path.Combine(directoryPath, filename);

                            var xmlDoc = new XmlDocument();
                            xmlDoc.LoadXml(stringWriter.ToString());

                            using (TextWriter textWriter = new StreamWriter(outputPath, false, Encoding.UTF8))
                            {
                                Logger.InfoFormat("Saving XML-document: {0}", outputPath);
                                xmlDoc.Save(textWriter);
                            }
                        }
                        catch (Exception exception)
                        {
                            Logger.Error(exception);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }
        }

        public string GetVersionNumber(SportsContent currentSportContent)
        {
            var versionContentCode = new SportsContentCode();
            foreach (var sportsContentCode in currentSportContent.SportsMetaData.SportsContentCodes.SportsContentCode)
            {
                if (sportsContentCode == null)
                {
                    continue;
                }

                if (sportsContentCode.CodeType != "ntbspcode:version")
                {
                    continue;
                }

                versionContentCode = sportsContentCode;
                break;
            }

            var versionNumber = "1";

            // Making sure it has a value
            if (versionContentCode.CodeName != null || versionContentCode.CodeName != string.Empty)
            {
                versionNumber = versionContentCode.CodeName;
            }
            return versionNumber;
        }
    }
}
