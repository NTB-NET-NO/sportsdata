﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes
{
    /// <summary>
    /// The affiliation.
    /// </summary>
    [XmlRoot("official")]
    [JsonObject("official")]
    public class Affiliation
    {
        /// <summary>
        /// Gets or sets the membership key.
        /// </summary>
        [XmlAttribute("membership-key")]
        [JsonProperty("membershipKey", NullValueHandling = NullValueHandling.Ignore)]
        public string MembershipKey { get; set; }

        /// <summary>
        /// Gets or sets the membership type.
        /// </summary>
        [XmlAttribute("membership-type")]
        [JsonProperty("membershipType", NullValueHandling = NullValueHandling.Ignore)]
        public string MembershipType { get; set; }

        /// <summary>
        /// Gets or sets the membership name.
        /// </summary>
        [XmlAttribute("membership-name")]
        [JsonProperty("membershipName", NullValueHandling = NullValueHandling.Ignore)]
        public string MembershipName { get; set; }
    }
}