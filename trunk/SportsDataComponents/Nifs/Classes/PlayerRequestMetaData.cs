﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsData.Components.Nifs.Classes
{
    public class PlayerRequestMetaData
    {
        public int PlayerId { get; set; }

        public string Name { get; set; }
    }
}
