﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    public class TournamentMetaData
    {
        public int Id { get; set; }

        public int GenderId { get; set; }

        public string SportId { get; set; }

        public string Name { get; set; }

        public string DateLine { get; set; }

        public string Owner { get; set; }

        public int CountryId { get; set; }

        public int EventTypeId { get; set; }
    }
}
