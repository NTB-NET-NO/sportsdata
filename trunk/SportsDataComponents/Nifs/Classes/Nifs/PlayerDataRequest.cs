﻿using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    public class PlayerDataRequest
    {
        /// <summary>
        /// Gets or sets the tournament data.
        /// </summary>
        [JsonProperty("playerData", NullValueHandling = NullValueHandling.Ignore)]
        public PlayerData PlayerData { get; set; }

    }
}