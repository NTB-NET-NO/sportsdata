﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchTeam.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The match away team.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    /// <summary>
    /// The match away team.
    /// </summary>
    public class MatchTeam
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the uid.
        /// </summary>
        [JsonProperty("uid", NullValueHandling = NullValueHandling.Ignore)]
        public string Uid { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the persons.
        /// </summary>
        [JsonProperty("persons", NullValueHandling = NullValueHandling.Ignore)]
        public List<Person> Persons { get; set; }

        /// <summary>
        /// Gets or sets the captain.
        /// </summary>
        [JsonProperty("captain", NullValueHandling = NullValueHandling.Ignore)]
        public Captain Captain { get; set; }

        /// <summary>
        /// Gets or sets the result id.
        /// </summary>
        [JsonProperty("resultId", NullValueHandling = NullValueHandling.Ignore)]
        public int? ResultId { get; set; }

        /// <summary>
        /// Gets or sets the result type id.
        /// </summary>
        [JsonProperty("resultTypeId", NullValueHandling = NullValueHandling.Ignore)]
        public int? ResultTypeId { get; set; }

        ///// <summary>
        ///// Gets or sets the kit.
        ///// </summary>
        // [JsonProperty("kit")]
        // public List<Kit> Kit { get; set; }

        /// <summary>
        /// Gets or sets the logo.
        /// </summary>
        [JsonProperty("logo", NullValueHandling = NullValueHandling.Ignore)]
        public Image Logo { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        [JsonProperty("country", NullValueHandling = NullValueHandling.Ignore)]
        public Country Country { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether club team.
        /// </summary>
        [JsonProperty("clubTeam", NullValueHandling = NullValueHandling.Ignore)]
        public bool ClubTeam { get; set; }
    }
}