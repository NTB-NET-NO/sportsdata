﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Stage.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The stage.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    /// <summary>
    /// The stage.
    /// </summary>
    public class TournamentStage
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the stage type id.
        /// </summary>
        [JsonProperty("stageTypeId")]
        public int StageTypeId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the full name.
        /// </summary>
        [JsonProperty("fullName")]
        public string FullName { get; set; }

        /// <summary>
        /// Gets or sets the group name.
        /// </summary>
        [JsonProperty("groupName")]
        public string GroupName { get; set; }

        /// <summary>
        /// Gets or sets the year start.
        /// </summary>
        [JsonProperty("yearStart")]
        public int YearStart { get; set; }

        /// <summary>
        /// Gets or sets the year end.
        /// </summary>
        [JsonProperty("yearEnd")]
        public int YearEnd { get; set; }

        /// <summary>
        /// Gets or sets the date start.
        /// </summary>
        [JsonProperty("dateStart")]
        public DateTime? DateStart { get; set; }

        /// <summary>
        /// Gets or sets the date end.
        /// </summary>
        [JsonProperty("dateEnd")]
        public DateTime? DateEnd { get; set; }

        /// <summary>
        /// Gets or sets the tournament.
        /// </summary>
        [JsonProperty("tournament")]
        public Tournament Tournament { get; set; }

        /// <summary>
        /// Gets or sets the visibility id.
        /// </summary>
        [JsonProperty("visibilityId")]
        public int VisibilityId { get; set; }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        [JsonProperty("data")]
        public Data Data { get; set; }

        /// <summary>
        /// Gets or sets the comment.
        /// </summary>
        [JsonProperty("comment")]
        public string Comment { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the uid.
        /// </summary>
        [JsonProperty("uid")]
        public string Uid { get; set; }
    }
}