﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Referees.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The referees.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    /// <summary>
    /// The referees.
    /// </summary>
    public class Referee
    {
        /// <summary>
        /// Gets or sets the referee type id.
        /// </summary>
        [JsonProperty("refereeTypeId", NullValueHandling = NullValueHandling.Ignore)]
        public int RefereeTypeId { get; set; }

        /// <summary>
        /// Gets or sets the match id.
        /// </summary>
        [JsonProperty("matchId", NullValueHandling = NullValueHandling.Ignore)]
        public int MatchId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the uid.
        /// </summary>
        [JsonProperty("uid", NullValueHandling = NullValueHandling.Ignore)]
        public string Uid { get; set; }

        [JsonProperty("team", NullValueHandling = NullValueHandling.Ignore)]
        public MatchTeam MatchTeam { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the person id.
        /// </summary>
        [JsonProperty("personId", NullValueHandling = NullValueHandling.Ignore)]
        public int PersonId { get; set; }
    }
}