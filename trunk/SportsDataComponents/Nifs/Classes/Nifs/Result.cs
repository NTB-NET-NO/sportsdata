﻿using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    /// <summary>
    /// The result.
    /// </summary>
    public class Result
    {
        /// <summary>
        /// Gets or sets the home score 90.
        /// </summary>
        [JsonProperty("homeScore90")]
        public int? HomeScore90 { get; set; }

        /// <summary>
        /// Gets or sets the home score 45.
        /// </summary>
        [JsonProperty("homeScore45")]
        public int? HomeScore45 { get; set; }

        /// <summary>
        /// Gets or sets the home score 105.
        /// </summary>
        [JsonProperty("homeScore105")]
        public int? HomeScore105 { get; set; }

        /// <summary>
        /// Gets or sets the home score 120.
        /// </summary>
        [JsonProperty("homeScore120")]
        public int? HomeScore120 { get; set; }

        /// <summary>
        /// Gets or sets the home score penalties.
        /// </summary>
        [JsonProperty("homeScorePenalties")]
        public int? HomeScorePenalties { get; set; }

        /// <summary>
        /// Gets or sets the away score 90.
        /// </summary>
        [JsonProperty("awayScore90")]
        public int? AwayScore90 { get; set; }

        /// <summary>
        /// Gets or sets the away score 45.
        /// </summary>
        [JsonProperty("awayScore45")]
        public int? AwayScore45 { get; set; }

        /// <summary>
        /// Gets or sets the away score 105.
        /// </summary>
        [JsonProperty("awayScore105")]
        public int? AwayScore105 { get; set; }

        /// <summary>
        /// Gets or sets the away score 120.
        /// </summary>
        [JsonProperty("awayScore120")]
        public int? AwayScore120 { get; set; }

        /// <summary>
        /// Gets or sets the away score penalties.
        /// </summary>
        [JsonProperty("awayScorePenalties")]
        public int? AwayScorePenalties { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the uid.
        /// </summary>
        [JsonProperty("uid")]
        public string Uid { get; set; }
    }
}