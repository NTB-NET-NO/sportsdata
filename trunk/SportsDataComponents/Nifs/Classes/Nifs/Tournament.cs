﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Tournament.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The tournament.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    /// <summary>
    /// The tournament.
    /// </summary>
    public class Tournament
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the level.
        /// </summary>
        public int Level { get; set; }

        /// <summary>
        /// Gets or sets the stages.
        /// </summary>
        public List<TournamentStage> Stages { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        public Country Country { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether neutral venues.
        /// </summary>
        public bool NeutralVenues { get; set; }

        /// <summary>
        /// Gets or sets the tournament type id.
        /// </summary>
        public int TournamentTypeId { get; set; }

        /// <summary>
        /// Gets or sets the visibility id.
        /// </summary>
        public int VisibilityId { get; set; }

        /// <summary>
        /// Gets or sets the uid.
        /// </summary>
        public string Uid { get; set; }
    }
}