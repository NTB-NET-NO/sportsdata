﻿using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    public class RegisteredPattern
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("infoLine")]
        public string InfoLine { get; set; }

        [JsonProperty("introLine")]
        public string IntroLine { get; set; }

        [JsonProperty("slugLine")]
        public string SlugLine { get; set; }

        [JsonProperty("productId")]
        public int ProductId { get; set; }
    }
}
