﻿using System.Collections.Generic;

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    public class MatchListModel
    {
        public List<MatchModel> MatchList { get; set; }
    }

    public class MatchModel
    {
        public string Id { get; set; }
    }
}