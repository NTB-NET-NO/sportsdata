﻿namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    public class TournamentDivision
    {
        public string Name { get; set; }

        public int Id { get; set; }

        public string Key { get; set; }
    }
}