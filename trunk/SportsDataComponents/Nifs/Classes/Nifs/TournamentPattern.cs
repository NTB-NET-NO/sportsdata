﻿namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    public class TournamentPattern
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the product id.
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// Gets or sets the product name.
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// Gets or sets the info line.
        /// </summary>
        public string InfoLine { get; set; }

        /// <summary>
        /// Gets or sets the slug line.
        /// </summary>
        public string SlugLine { get; set; }

        /// <summary>
        /// Gets or sets the intro line.
        /// </summary>
        public string IntroLine { get; set; }

        /// <summary>
        /// Gets or sets the date line.
        /// </summary>
        public string DateLine { get; set; }
    }
}
