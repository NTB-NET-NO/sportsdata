﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentData.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The tournament data.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    /// <summary>
    /// The tournament data.
    /// </summary>
    [JsonObject("tournamentData")]
    public class TournamentData
    {
        /// <summary>
        /// Gets or sets the tournament id.
        /// </summary>
        [JsonProperty("tournamentId", NullValueHandling = NullValueHandling.Ignore)]
        public int TournamentId { get; set; }
        /// <summary>
        /// Gets or sets the selected date line.
        /// </summary>
        [JsonProperty("selectedDateLine", NullValueHandling = NullValueHandling.Ignore)]
        public string SelectedDateLine { get; set; }

        /// <summary>
        /// Gets or sets the selected alternate name.
        /// </summary>
        [JsonProperty("selectedAlternateName", NullValueHandling = NullValueHandling.Ignore)]
        public string SelectedAlternateName { get; set; }

        /// <summary>
        /// Gets or sets the selected gender.
        /// </summary>
        [JsonProperty("selectedGender", NullValueHandling = NullValueHandling.Ignore)]
        public string SelectedGender { get; set; }

        /// <summary>
        /// Gets or sets the selected sport.
        /// </summary>
        [JsonProperty("selectedSport", NullValueHandling = NullValueHandling.Ignore)]
        public string SelectedSport { get; set; }

        /// <summary>
        /// Gets or sets the selected organization.
        /// </summary>
        [JsonProperty("selectedOrganization", NullValueHandling = NullValueHandling.Ignore)]
        public string SelectedOrganization { get; set; }

        /// <summary>
        /// Gets or sets the registered patterns.
        /// </summary>
        [JsonProperty("registeredPatterns", NullValueHandling = NullValueHandling.Ignore)]
        public RegisteredPattern[] RegisteredPatterns { get; set; }

        /// <summary>
        /// Gets or sets the selected country.
        /// </summary>
        [JsonProperty("selectedCountry", NullValueHandling = NullValueHandling.Ignore)]
        public int SelectedCountry { get; set; }

        /// <summary>
        /// Gets or sets the selected event type.
        /// </summary>
        [JsonProperty("selectedEventType", NullValueHandling = NullValueHandling.Ignore)]
        public int SelectedEventType { get; set; }
    }
}