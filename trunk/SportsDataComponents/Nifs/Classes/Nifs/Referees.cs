﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Referees.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The referees.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    /// <summary>
    /// The referees.
    /// </summary>
    [JsonObject("Referees")]
    public class Referees
    {
        /// <summary>
        /// Gets or sets the referee.
        /// </summary>
        public Referee Referee { get; set; }
    }
}