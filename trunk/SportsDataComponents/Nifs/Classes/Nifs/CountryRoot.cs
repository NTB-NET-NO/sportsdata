﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CountryRoot.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The country root.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    /// <summary>
    /// The country root.
    /// </summary>
    public class CountryRoot
    {
        /// <summary>
        /// The countries.
        /// </summary>
        public CountryTournamentView[] CountriesTournamentView;
    }
}