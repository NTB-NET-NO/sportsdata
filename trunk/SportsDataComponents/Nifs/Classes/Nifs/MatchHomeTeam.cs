﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    /// <summary>
    /// The match home team.
    /// </summary>
    public class MatchHomeTeam
    {
        /// <summary>
        /// Gets or sets the persons.
        /// </summary>
        [JsonProperty("persons")]
        public List<Person> Persons { get; set; }

        /// <summary>
        /// Gets or sets the captain.
        /// </summary>
        [JsonProperty("captain")]
        public Captain Captain { get; set; }

        /// <summary>
        /// Gets or sets the result id.
        /// </summary>
        [JsonProperty("resultId")]
        public int? ResultId { get; set; }

        /// <summary>
        /// Gets or sets the result type id.
        /// </summary>
        [JsonProperty("resultTypeId")]
        public int? ResultTypeId { get; set; }

        /// <summary>
        /// Gets or sets the kit.
        /// </summary>
        [JsonProperty("kit")]
        public List<Kit> Kit { get; set; }

        /// <summary>
        /// Gets or sets the logo.
        /// </summary>
        [JsonProperty("logo")]
        public Image Logo { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the uid.
        /// </summary>
        [JsonProperty("uid")]
        public string Uid { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }
    }
}