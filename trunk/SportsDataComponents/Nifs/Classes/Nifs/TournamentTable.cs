﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentTable.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The tournament table.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    /// <summary>
    /// The tournament table.
    /// </summary>
    public class TournamentTable
    {
        /// <summary>
        /// Gets or sets the tournament table team.
        /// </summary>
        [JsonProperty("teams")]
        public TournamentTableTeam[] TournamentTableTeams { get; set; }

        /// <summary>
        /// Gets or sets the tournament table dividing lines.
        /// </summary>
        [JsonProperty("tableDividingLines")]
        public TournamentTableDividingLines[] TournamentTableDividingLines { get; set; }
    }
}