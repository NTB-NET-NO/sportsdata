﻿using System.Collections.Generic;

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    public class PathObject
    {
        public List<int> Paths { get; set; }
    }
}