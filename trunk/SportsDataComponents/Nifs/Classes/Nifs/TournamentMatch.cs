﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentMatch.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The tournament match.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    /// <summary>
    /// The tournament match.
    /// </summary>
    [Serializable]
    [DataContract(Name = "tournamentMatch")]
    public class TournamentMatch
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [JsonProperty]
        [DataMember(Name = "id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [JsonProperty]
        [DataMember(Name = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the match date time.
        /// </summary>
        [JsonProperty]
        [DataMember(Name = "timestamp")]
        public DateTime MatchDateTime { get; set; }

        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        [JsonProperty]
        [DataMember(Name = "result")]
        public MatchResult Result { get; set; }

        /// <summary>
        /// Gets or sets the home team.
        /// </summary>
        [JsonProperty]
        [DataMember(Name = "hometeam")]
        public MatchTeam HomeMatchTeam { get; set; }

        /// <summary>
        /// Gets or sets the away team.
        /// </summary>
        [JsonProperty]
        [DataMember(Name = "awayteam")]
        public MatchTeam AwayMatchTeam { get; set; }

        /// <summary>
        /// Gets or sets the match status id.
        /// </summary>
        [JsonProperty]
        [DataMember(Name = "matchStatusId")]
        public int MatchStatusId { get; set; }

        /// <summary>
        /// Gets or sets the match status text.
        /// </summary>
        [JsonProperty]
        [DataMember(Name = "matchStatusText")]
        public string MatchStatusText { get; set; }

        /// <summary>
        /// Gets or sets the match type id.
        /// </summary>
        [JsonProperty]
        [DataMember(Name = "matchTypeId")]
        public int? MatchTypeId { get; set; }

        /// <summary>
        /// Gets or sets the attendance.
        /// </summary>
        [JsonProperty]
        [DataMember(Name = "attendance")]
        public int? Attendance { get; set; }

        /// <summary>
        /// Gets or sets the round.
        /// </summary>
        [JsonProperty]
        [DataMember(Name = "round")]
        public int? Round { get; set; }

        /// <summary>
        /// Gets or sets the comment.
        /// </summary>
        [JsonProperty]
        [DataMember(Name = "comment")]
        public string Comment { get; set; }

        /// <summary>
        /// Gets or sets the stage id.
        /// </summary>
        [JsonProperty]
        [DataMember(Name = "stageId")]
        public int? StageId { get; set; }

        [JsonProperty]
        [DataMember(Name = "stage")]
        public TournamentStage Stage { get; set; }
    }
}