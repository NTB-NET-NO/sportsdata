﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RefereesObject.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The referees object.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    /// <summary>
    /// The referees object.
    /// </summary>
    public class RefereesObject
    {
        /// <summary>
        /// Gets or sets the referees.
        /// </summary>
        public Referees Referees { get; set; }
    }
}