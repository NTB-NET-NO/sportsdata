﻿using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    [JsonObject("teamData")]
    public class TeamData
    {
        /// <summary>
        /// Gets or sets the team id.
        /// </summary>
        [JsonProperty("teamid", NullValueHandling = NullValueHandling.Ignore)]
        public int TeamId { get; set; }

        [JsonProperty("selectedAlternateName", NullValueHandling = NullValueHandling.Ignore)]
        public string SelectedAlternateName { get; set; }

        [JsonProperty("city", NullValueHandling = NullValueHandling.Ignore)]
        public string City { get; set; }
    }
}
