﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    public class Function
    {
        public int PersonId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int FunctionTypeId { get; set; }

        public string FunctionTypeName { get; set; }
    }
}
