﻿using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    public class TeamDataRequest
    {
        /// <summary>
        /// Gets or sets the tournament data.
        /// </summary>
        [JsonProperty("teamData", NullValueHandling = NullValueHandling.Ignore)]
        public TeamData TeamData { get; set; }

    }
}