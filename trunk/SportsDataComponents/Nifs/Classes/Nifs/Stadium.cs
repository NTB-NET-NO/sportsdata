﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Stadium.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The stadium.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    /// <summary>
    /// The stadium.
    /// </summary>
    public class Stadium
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the uid.
        /// </summary>
        [JsonProperty("uid")]
        public string Uid { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        [JsonProperty("country", NullValueHandling = NullValueHandling.Ignore)]
        public Country Country { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        [JsonProperty("address", NullValueHandling = NullValueHandling.Ignore)]
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        [JsonProperty("place", NullValueHandling = NullValueHandling.Ignore)]
        public Place Place { get; set; }

        private DateTime _dateStart { get; set; }

        private DateTime _dateEnd { get; set; }

        [JsonProperty("dateStart", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string DateStart
        {
            get { return this._dateStart.ToString("yyyy-MM-dd"); }

            set
            {

                try
                {
                    this._dateStart = Convert.ToDateTime(value);
                }
                catch (Exception exception)
                {

                    Console.WriteLine(exception.Message);
                    Console.WriteLine(value);
                    var incoming = value;

                    if (incoming.Length == 4)
                    {
                        this._dateStart = DateTime.Parse(incoming + "-01-01");
                        return;
                    }

                    if (incoming == string.Empty)
                    {
                        this._dateStart = DateTime.Today;
                        return;
                    }

                    if (!incoming.Contains("-"))
                    {
                        if (incoming.Length < 8)
                        {
                            // Making the string shorter, because this could mean that the date is set like this
                            // 196301
                            // What if it is like this: 630120 (1963-01-20)

                        }
                        this._dateStart = DateTime.Parse(incoming);
                    }
                }
            }
        }

        [JsonProperty("dateEnd", Required = Required.Default, NullValueHandling = NullValueHandling.Ignore)]
        public string DateEnd
        {
            get { return this._dateEnd.ToString("yyyy-MM-dd"); }

            set
            {

                try
                {
                    this._dateEnd = Convert.ToDateTime(value);
                }
                catch (Exception exception)
                {

                    Console.WriteLine(exception.Message);
                    Console.WriteLine(value);
                    var incoming = value;

                    if (incoming.Length == 4)
                    {
                        this._dateEnd = DateTime.Parse(incoming + "-01-01");
                        return;
                    }

                    if (incoming == string.Empty)
                    {
                        this._dateEnd = DateTime.Today;
                        return;
                    }

                    if (!incoming.Contains("-"))
                    {
                        if (incoming.Length < 8)
                        {
                            // Making the string shorter, because this could mean that the date is set like this
                            // 196301
                            // What if it is like this: 630120 (1963-01-20)

                        }
                        this._dateEnd = DateTime.Parse(incoming);
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        [JsonProperty("names", NullValueHandling = NullValueHandling.Ignore)]
        public ObjectName[] SiteNames { get; set; }
    }
}