﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Items.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The nodes.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    /// <summary>
    /// The nodes.
    /// </summary>
    public class Items
    {
        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        public Data Data { get; set; }
        
        /// <summary>
        /// Gets or sets the children.
        /// </summary>
        public List<Items> Children { get; set; }
    }
}