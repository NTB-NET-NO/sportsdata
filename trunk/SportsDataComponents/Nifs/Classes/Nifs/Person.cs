﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Person.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The person.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    /// <summary>
    /// The person.
    /// </summary>
    public class Person
    {
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the uid.
        /// </summary>
        [JsonProperty("uid")]
        public string Uid { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [JsonProperty("name")]
        public string FullName { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        [JsonProperty("lastName")]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the nick name.
        /// </summary>
        [JsonProperty("nickName")]
        public string NickName { get; set; }

        private DateTime _birthDate { get; set; }

        /// <summary>
        /// Gets or sets the birth date.
        /// </summary>
        [JsonProperty("birthDate", NullValueHandling = NullValueHandling.Ignore)]
        public string BirthDate
        {
            get { return this._birthDate.ToString("yyyy-MM-dd"); }

            set
            {

                try
                {
                    this._birthDate = Convert.ToDateTime(value);
                }
                catch (Exception exception)
                {

                    Console.WriteLine(exception.Message);
                    Console.WriteLine(value);
                    var incoming = value;

                    if (incoming.Length == 4)
                    {
                        this._birthDate = DateTime.Parse(incoming + "-01-01");
                        return;
                    }

                    if (incoming == string.Empty)
                    {
                        this._birthDate = DateTime.Today;
                        return;
                    }

                    if (!incoming.Contains("-"))
                    {
                        if (incoming.Length < 8)
                        {
                            // Making the string shorter, because this could mean that the date is set like this
                            // 196301
                            // What if it is like this: 630120 (1963-01-20)
                            
                        }
                        this._birthDate = DateTime.Parse(incoming);
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        [JsonProperty("gender", NullValueHandling = NullValueHandling.Ignore)]
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        [JsonProperty("height", NullValueHandling = NullValueHandling.Ignore)]
        public int Height { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether starts match.
        /// </summary>
        [JsonProperty("startsMatch", NullValueHandling = NullValueHandling.Ignore)]
        public bool StartsMatch { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether starts on the bench.
        /// </summary>
        [JsonProperty("startsOnTheBench", NullValueHandling = NullValueHandling.Ignore)]
        public bool StartsOnTheBench { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether currently on the field.
        /// </summary>
        [JsonProperty("currentlyOnTheField", NullValueHandling = NullValueHandling.Ignore)]
        public bool CurrentlyOnTheField { get; set; }

        /// <summary>
        /// Gets or sets the enters field minute.
        /// </summary>
        [JsonProperty("entersFieldMinute", NullValueHandling = NullValueHandling.Ignore)]
        public int? EntersFieldMinute { get; set; }

        /// <summary>
        /// Gets or sets the leaves field minute.
        /// </summary>
        [JsonProperty("leavesFieldMinute", NullValueHandling = NullValueHandling.Ignore)]
        public int? LeavesFieldMinute { get; set; }

        /// <summary>
        /// Gets or sets the minutes played.
        /// </summary>
        [JsonProperty("minutesPlayed", NullValueHandling = NullValueHandling.Ignore)]
        public int? MinutesPlayed { get; set; }

        /// <summary>
        /// Gets or sets the shirt number.
        /// </summary>
        [JsonProperty("shirtNumber", NullValueHandling = NullValueHandling.Ignore)]
        public int? ShirtNumber { get; set; }

        /// <summary>
        /// Gets or sets the person id.
        /// </summary>
        [JsonProperty("personId", NullValueHandling = NullValueHandling.Ignore)]
        public int PersonId { get; set; }

        /// <summary>
        /// Gets or sets the match id.
        /// </summary>
        [JsonProperty("matchId", NullValueHandling = NullValueHandling.Ignore)]
        public int MatchId { get; set; }

        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        [JsonProperty("position", NullValueHandling = NullValueHandling.Ignore)]
        public Position Position { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        [JsonProperty("country", NullValueHandling = NullValueHandling.Ignore)]
        public Country Country { get; set; }
    }
}