﻿using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    [JsonObject("playerData")]
    public class PlayerData
    {
        /// <summary>
        /// Gets or sets the team id.
        /// </summary>
        [JsonProperty("playerId", NullValueHandling = NullValueHandling.Ignore)]
        public int PlayerId { get; set; }

        [JsonProperty("selectedAlternateName", NullValueHandling = NullValueHandling.Ignore)]
        public string SelectedAlternateName { get; set; }
    }
}
