﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentMetaData.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The tournament meta data.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    /// <summary>
    /// The tournament meta data.
    /// </summary>
    public class TournamentRoundMetaData
    {
        /// <summary>
        /// Gets or sets the round.
        /// </summary>
        public string Round { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        public Country Country { get; set; }
    }
}