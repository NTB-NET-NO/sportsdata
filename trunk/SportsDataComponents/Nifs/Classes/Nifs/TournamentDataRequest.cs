﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentDataRequest.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The tournament data request.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    /// <summary>
    /// The tournament data request.
    /// </summary>
    public class TournamentDataRequest
    {
        /// <summary>
        /// Gets or sets the tournament data.
        /// </summary>
        [JsonProperty("tournamentData", NullValueHandling = NullValueHandling.Ignore)]
        public TournamentData TournamentData { get; set; }

    }
}