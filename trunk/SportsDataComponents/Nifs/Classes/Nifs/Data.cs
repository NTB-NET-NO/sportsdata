// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Data.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The state.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    /// <summary>
    /// The state.
    /// </summary>
    public class Data
    {
        /// <summary>
        /// Gets or sets a value indicating whether checked.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the uid.
        /// </summary>
        public string Uid { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether ratings.
        /// </summary>
        public bool Ratings { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether assists.
        /// </summary>
        public bool Assists { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether attendances.
        /// </summary>
        public bool Attendances { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether corners.
        /// </summary>
        public bool Corners { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether goalscorers.
        /// </summary>
        public bool Goalscorers { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether half time score.
        /// </summary>
        public bool HalfTimeScore { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether minutes played.
        /// </summary>
        public bool MinutesPlayed { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether penalties.
        /// </summary>
        public bool Penalties { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether red cards.
        /// </summary>
        public bool RedCards { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether referees.
        /// </summary>
        public bool Referees { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether shots.
        /// </summary>
        public bool Shots { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether yellow cards.
        /// </summary>
        public bool YellowCards { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether indirect assists.
        /// </summary>
        public bool IndirectAssists { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether squads.
        /// </summary>
        public bool Squads { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether stadiums.
        /// </summary>
        public bool Stadiums { get; set; }
    }
}