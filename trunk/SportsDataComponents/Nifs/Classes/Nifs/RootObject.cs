﻿using System.Collections.Generic;

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    public class RootObject
    {
        public List<int> matches { get; set; }
        public List<int> paths { get; set; }
    }
}