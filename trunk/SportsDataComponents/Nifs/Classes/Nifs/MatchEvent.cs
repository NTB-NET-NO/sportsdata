﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchEvent.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The match event.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    /// <summary>
    /// The match event.
    /// </summary>
    public class MatchEvent
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the uid.
        /// </summary>
        [JsonProperty("uid")]
        public string Uid { get; set; }

        /// <summary>
        /// Gets or sets the match event type id.
        /// </summary>
        [JsonProperty("matchEventTypeId")]
        public int MatchEventTypeId { get; set; }

        /// <summary>
        /// Gets or sets the period id.
        /// </summary>
        [JsonProperty("periodId")]
        public int PeriodId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether important.
        /// </summary>
        [JsonProperty("important")]
        public bool Important { get; set; }

        /// <summary>
        /// Gets or sets the sorting.
        /// </summary>
        [JsonProperty("sorting")]
        public int Sorting { get; set; }

        /// <summary>
        /// Gets or sets the time.
        /// </summary>
        [JsonProperty("time")]
        public int? Time { get; set; }

        /// <summary>
        /// Gets or sets the over time.
        /// </summary>
        [JsonProperty("overtime")]
        public int? OverTime { get; set; }

        /// <summary>
        /// Gets or sets the match id.
        /// </summary>
        [JsonProperty("matchId")]
        public int MatchId { get; set; }

        /// <summary>
        /// Gets or sets the person.
        /// </summary>
        [JsonProperty("person")]
        public Person Person { get; set; }

        /// <summary>
        /// Gets or sets the team.
        /// </summary>
        [JsonProperty("team")]
        public MatchTeam MatchTeam { get; set; }

        /// <summary>
        /// Gets or sets the live feed id.
        /// </summary>
        [JsonProperty("liveFeedId")]
        public int? LiveFeedId { get; set; }

        /// <summary>
        /// Gets or sets the live feed id.
        /// </summary>
        [JsonProperty("score")]
        public string Score { get; set; }
    }
}