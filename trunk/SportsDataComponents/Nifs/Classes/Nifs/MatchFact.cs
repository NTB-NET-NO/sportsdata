﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchFact.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The match fact.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    /// <summary>
    /// The match fact.
    /// </summary>
    public class MatchFact
    {
        /// <summary>
        /// Gets or sets the uid.
        /// </summary>
        [JsonProperty("uid")]
        public string Uid { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the time stamp.
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the time stamp.
        /// </summary>
        [JsonProperty("timestamp")]
        public DateTime TimeStamp { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        [JsonProperty("result")]
        public Result Result { get; set; }

        /// <summary>
        /// Gets or sets the home team.
        /// </summary>
        [JsonProperty("homeTeam")]
        public MatchTeam HomeTeam { get; set; }

        /// <summary>
        /// Gets or sets the away team.
        /// </summary>
        [JsonProperty("awayTeam")]
        public MatchTeam AwayTeam { get; set; }

        /// <summary>
        /// Gets or sets the stage.
        /// </summary>
        [JsonProperty("stage")]
        public TournamentStage Stage { get; set; }

        /// <summary>
        /// Gets or sets the match events.
        /// </summary>
        [JsonProperty("matchEvents")]
        public List<MatchEvent> MatchEvents { get; set; }

        /// <summary>
        /// Gets or sets the stadium
        /// </summary>
        [JsonProperty("stadium")]
        public Stadium Stadium { get; set; }

        /// <summary>
        /// Gets or sets the stadium
        /// </summary>
        [JsonProperty("attendance")]
        public int? Attendance { get; set; }

        /// <summary>
        /// Gets or sets the referees.
        /// </summary>
        [JsonProperty("referees")]
        public Referee[] Referees { get; set; }

        /// <summary>
        /// Gets or sets the round
        /// </summary>
        [JsonProperty("round")]
        public string Round { get; set; }

        /// <summary>
        /// Gets or sets the match status id
        /// </summary>
        [JsonProperty("matchStatusId")]
        public int MatchStatusId { get; set; }

        /// <summary>
        /// Gets or sets the match type id
        /// </summary>
        [JsonProperty("matchTypeId")]
        public int? MatchTypeId { get; set; }

        /// <summary>
        /// Gets or sets the match relation.
        /// </summary>
        [JsonProperty("matchRelation", NullValueHandling = NullValueHandling.Ignore)]
        public MatchRelation MatchRelation { get; set; }
    }
}