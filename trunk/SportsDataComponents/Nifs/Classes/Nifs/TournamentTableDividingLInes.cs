﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentTableDividingLines.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The tournament table dividing l ines.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    /// <summary>
    /// The tournament table dividing l ines.
    /// </summary>
    [JsonObject("TournamentTableDividingLines")]
    public class TournamentTableDividingLines
    {
        /// <summary>
        /// Gets or sets the tournament table dividing line.
        /// </summary>
        [JsonProperty("tournamentTableDividingLine")]
        public TournamentTableDividingLine[] TournamentTableDividingLine { get; set; }
    }
}