﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchRelation.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The match relation.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    /// <summary>
    /// The match relation.
    /// </summary>
    public class MatchRelation
    {
        /// <summary>
        /// Gets or sets the winning team id.
        /// </summary>
        [JsonProperty("winningTeamId")]
        public int? WinningTeamId { get; set; }

        /// <summary>
        /// Gets or sets the victory type id.
        /// </summary>
        [JsonProperty("victoryTypeId")]
        public int? VictoryTypeId { get; set; }

        /// <summary>
        /// Gets or sets the match relation type id.
        /// </summary>
        [JsonProperty("matchRelationTypeId")]
        public int MatchRelationTypeId { get; set; }

        /// <summary>
        /// Gets or sets the aggregated home goals.
        /// </summary>
        [JsonProperty("aggregatedHomeGoals")]
        public int? AggregatedHomeGoals { get; set; }

        /// <summary>
        /// Gets or sets the aggregated away goals.
        /// </summary>
        [JsonProperty("aggregatedAwayGoals")]
        public int? AggregatedAwayGoals { get; set; }

        /// <summary>
        /// Gets or sets the other match id.
        /// </summary>
        [JsonProperty("otherMatchId")]
        public int OtherMatchId { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the uid.
        /// </summary>
        [JsonProperty("uid")]
        public string Uid { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [JsonProperty("id")]
        public int? Id { get; set; }
    }
}