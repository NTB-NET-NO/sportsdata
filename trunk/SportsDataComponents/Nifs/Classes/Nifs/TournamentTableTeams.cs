﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentTableTeams.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The tournament table teams.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    /// <summary>
    /// The tournament table teams.
    /// </summary>
    [JsonObject("teams")]
    public class TournamentTableTeams
    {
        /// <summary>
        /// Gets or sets the tournament table team.
        /// </summary>
        /// <summary>
        /// Gets or sets the uid.
        /// </summary>
        [JsonProperty("team")]
        public TournamentTableTeam[] TournamentTableTeam { get; set; }
    }
}