﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExportRequest.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The export request.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.Nifs
{
    /// <summary>
    /// The export request.
    /// </summary>
    public class ExportRequest
    {
        /// <summary>
        /// Gets or sets the tournament matches.
        /// </summary>
        [JsonProperty("matches", NullValueHandling = NullValueHandling.Ignore)]
        public string[] TournamentMatches { get; set; }

        /// <summary>
        /// Gets or sets the export paths.
        /// </summary>
        [JsonProperty("paths", NullValueHandling = NullValueHandling.Ignore)]
        public string[] ExportPaths { get; set; }
    }
}