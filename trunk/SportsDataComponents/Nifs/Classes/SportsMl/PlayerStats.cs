﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The player stats.
    /// </summary>
    [XmlRoot("player-stats")]
    [JsonObject("playerStats")]
    public class PlayerStats
    {
        /// <summary>
        /// Gets or sets the score.
        /// </summary>
        [XmlAttribute("score")]
        [JsonProperty("score", NullValueHandling = NullValueHandling.Ignore)]
        public string Score { get; set; }

        /// <summary>
        /// Gets or sets the event time entered.
        /// </summary>
        [XmlAttribute("event-time-entered")]
        [JsonProperty("eventTimeEntered")]
        public int EventTimeEntered { get; set; }

        /// <summary>
        /// Gets or sets the event time exited.
        /// </summary>
        [XmlAttribute("event-time-exited")]
        [JsonProperty("eventTimeExited")]
        public int EventTimeExited { get; set; }

        /// <summary>
        /// Gets or sets the temporal unit type.
        /// </summary>
        [XmlAttribute("temporal-unit-type")]
        [JsonProperty("temporalUnitType")]
        public string TemporalUnitType { get; set; }

        /// <summary>
        /// Gets or sets the score.
        /// </summary>
        [XmlElement("sub-score")]
        [JsonProperty("subScore", NullValueHandling = NullValueHandling.Ignore)]
        public SubScore SubScore { get; set; }

        /// <summary>
        /// Gets or sets the rank.
        /// </summary>
        [XmlElement("rank")]
        [JsonProperty("rank", NullValueHandling = NullValueHandling.Ignore)]
        public Rank Rank { get; set; }

        /// <summary>
        /// Gets or sets the award.
        /// </summary>
        [XmlElement("award")]
        [JsonProperty("award", NullValueHandling = NullValueHandling.Ignore)]
        public Award Award { get; set; }
    }
}