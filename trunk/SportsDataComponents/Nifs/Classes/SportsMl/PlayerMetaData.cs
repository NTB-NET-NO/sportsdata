﻿using System;
using System.Globalization;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The player meta data.
    /// </summary>
    [XmlRoot("player-metadata")]
    public class PlayerMetaData
    {
        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        [XmlAttribute("key")]
        [JsonProperty("key", NullValueHandling = NullValueHandling.Ignore)]
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        [XmlAttribute("gender")]
        [JsonProperty("gender", NullValueHandling = NullValueHandling.Ignore)]
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets the date of birth.
        /// </summary>
        [XmlAttribute("date-of-birth")]
        [JsonProperty("dateOfBirth", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [XmlElement("name")]
        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public Name[] PlayerNames { get; set; }

        /// <summary>
        /// Gets or sets the home location.
        /// </summary>
        [XmlElement("home-location")]
        [JsonProperty("homeLocation", NullValueHandling = NullValueHandling.Ignore)]
        public HomeLocation HomeLocation { get; set; }

        /// <summary>
        /// Gets or sets the uniform number.
        /// </summary>
        [XmlAttribute("uniform-number")]
        [JsonProperty("uniformNumber", NullValueHandling = NullValueHandling.Ignore)]
        public int UniformNumber { get; set; }

        /// <summary>
        /// Gets or sets the team id ref.
        /// </summary>
        [XmlAttribute("team-idref")]
        [JsonProperty("teamIdref", NullValueHandling = NullValueHandling.Ignore)]
        public string TeamIdRef { get; set; }

        /// <summary>
        /// Gets or sets the event position.
        /// </summary>
        [XmlAttribute("position-event")]
        [JsonProperty("positionEvent ", NullValueHandling = NullValueHandling.Ignore)]
        public string PositionEvent { get; set; }

        /// <summary>
        /// Gets or sets the uniform number.
        /// </summary>
        [XmlAttribute("lineup-slot")]
        [JsonProperty("lineupSlot", NullValueHandling = NullValueHandling.Ignore)]
        public int LineupSlot { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        [XmlAttribute("status")]
        [JsonProperty("status", NullValueHandling = NullValueHandling.Ignore)]
        public string Status { get; set; }

        /// <summary>
        /// The should serialize player.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ShouldSerializePlayer()
        {
            DateTime dateValue;
            return DateTime.TryParseExact(this.DateOfBirth.ToString("yyyy-MM-dd hh:mm:ss"), "M/d/yyyy h:mm:ss tt", new CultureInfo("en-US"), DateTimeStyles.None, out dateValue);
        }
    }
}