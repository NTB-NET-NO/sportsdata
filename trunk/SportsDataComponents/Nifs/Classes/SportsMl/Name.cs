﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The name.
    /// </summary>
    [XmlRoot("name")]
    public class Name
    {
        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        [XmlAttribute("first")]
        [JsonProperty("first", NullValueHandling = NullValueHandling.Ignore)]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        [XmlIgnore]
        [JsonProperty("last", NullValueHandling = NullValueHandling.Ignore)]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the full name.
        /// </summary>
        [XmlText]
        [JsonProperty("full", NullValueHandling = NullValueHandling.Ignore)]
        public string FullName { get; set; }

        /// <summary>
        /// Gets or sets the abbreviation.
        /// </summary>
        [XmlIgnore]
        [JsonProperty("abbreviation", NullValueHandling = NullValueHandling.Ignore)]
        public string Abbreviation { get; set; }

        /// <summary>
        /// Gets or sets the role which is a qcode.
        /// </summary>
        [XmlAttribute("role")]
        public string Role { get; set; }

        /// <summary>
        /// Gets or sets the creator - which is a qcode.
        /// </summary>
        [XmlAttribute("creator")]
        public string Creator { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        [XmlIgnore]
        public string Description { get; set; }
    }
}