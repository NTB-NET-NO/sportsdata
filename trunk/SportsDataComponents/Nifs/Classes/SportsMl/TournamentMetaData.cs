﻿using System;
using System.Globalization;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The tournament meta data.
    /// </summary>
    public class TournamentMetaData
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [XmlAttribute("id")]
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        [XmlAttribute("key")]
        [JsonProperty("key", NullValueHandling = NullValueHandling.Ignore)]
        public string TournamentKey { get; set; }

        /// <summary>
        /// Gets or sets the start date time.
        /// </summary>
        [XmlAttribute("start-date-time")]
        [JsonProperty("startDateTime", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime StartDateTime { get; set; }

        /// <summary>
        /// Gets or sets the end date time.
        /// </summary>
        [XmlAttribute("end-date-time")]
        [JsonProperty("endDateTime", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime EndDateTime { get; set; }

        /// <summary>
        /// Gets or sets the end date time.
        /// </summary>
        [XmlAttribute("status")]
        [JsonProperty("status", NullValueHandling = NullValueHandling.Ignore)]
        public string TournamentStatus { get; set; }

        /// <summary>
        /// Gets or sets the event name.
        /// </summary>
        [XmlElement("name")]
        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public Name[] TournamentNames { get; set; }

        /// <summary>
        /// Gets or sets the sports property.
        /// </summary>
        [XmlElement("sports-property")]
        [JsonProperty("sportsProperty", NullValueHandling = NullValueHandling.Ignore)]
        public SportsProperty SportsProperty { get; set; }

        /// <summary>
        /// The should serialize End Date Time.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ShouldSerializeEndDateTime()
        {
            DateTime dateValue;
            return DateTime.TryParseExact(this.EndDateTime.ToString("yyyy-MM-dd hh:mm:ss"), "M/d/yyyy h:mm:ss tt", new CultureInfo("en-US"), DateTimeStyles.None, out dateValue);
        }

        /// <summary>
        /// The should serialize Start Date Time.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ShouldSerializeStartDateTime()
        {
            DateTime dateValue;
            return DateTime.TryParseExact(this.StartDateTime.ToString("yyyy-MM-dd hh:mm:ss"), "M/d/yyyy h:mm:ss tt", new CultureInfo("en-US"), DateTimeStyles.None, out dateValue);
        }
    }
}