﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The team stats.
    /// </summary>
    [XmlRoot("team-stats")]
    public class TeamStats
    {
        /// <summary>
        /// Gets or sets the score.
        /// </summary>
        [XmlAttribute("score")]
        [JsonProperty("score", NullValueHandling = NullValueHandling.Ignore)]
        public string Score { get; set; }

        /// <summary>
        /// Gets or sets the event outcome.
        /// </summary>
        [XmlAttribute("event-outcome")]
        [JsonProperty("eventOutcome", NullValueHandling = NullValueHandling.Ignore)]
        public string EventOutcome { get; set; }

        /// <summary>
        /// Gets or sets the outcome totals.
        /// </summary>
        [XmlElement("outcome-totals")]
        [JsonProperty("outcomeTotals", NullValueHandling = NullValueHandling.Ignore)]
        public OutcomeTotals[] OutcomeTotals { get; set; }

        /// <summary>
        /// Gets or sets the sub score.
        /// </summary>
        [XmlElement("sub-score")]
        [JsonProperty("subScores", NullValueHandling = NullValueHandling.Ignore)]
        public SubScore[] SubScore { get; set; }

        /// <summary>
        /// Gets or sets the sub scores.
        /// </summary>
        [XmlIgnore]
        [JsonProperty("subScoresx", NullValueHandling = NullValueHandling.Ignore)]
        public SubScores SubScores { get; set; }

        /// <summary>
        /// Gets or sets the award.
        /// </summary>
        [XmlElement("award")]
        [JsonProperty("award", NullValueHandling = NullValueHandling.Ignore)]
        public Award[] Award { get; set; }

        /// <summary>
        /// Gets or sets the rank.
        /// </summary>
        [XmlElement("rank")]
        [JsonProperty("rank", NullValueHandling = NullValueHandling.Ignore)]
        public Rank Rank { get; set; }

        /// <summary>
        /// The should serialize Score.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ShouldSerializeScore()
        {
            return this.Score != string.Empty;
        }

        /// <summary>
        /// The should serialize Score.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ShouldSerializeSubScore()
        {
            if (this.SubScore != null)
            {
                return this.SubScore.Length > 0;
            }

            return false;
        }

        /// <summary>
        /// The should serialize player.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ShouldSerializeEventOutcome()
        {
            return this.Score != string.Empty;
        }
    }
}