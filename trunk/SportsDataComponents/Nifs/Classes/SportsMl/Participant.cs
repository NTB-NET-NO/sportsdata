﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The participant.
    /// </summary>
    public class Participant
    {
        /// <summary>
        /// Gets or sets the player id ref.
        /// </summary>
        [XmlAttribute("idref")]
        [JsonProperty("idref", NullValueHandling = NullValueHandling.Ignore)]
        public string IdRef { get; set; }

        /// <summary>
        /// Gets or sets the team id ref.
        /// </summary>
        [XmlAttribute("team-idref")]
        [JsonProperty("teamIdref", NullValueHandling = NullValueHandling.Ignore)]
        public string TeamIdRef { get; set; }

        /// <summary>
        /// Gets or sets the role.
        /// </summary>
        [XmlAttribute("role")]
        [JsonProperty("role", NullValueHandling = NullValueHandling.Ignore)]
        public string Role { get; set; }
    }
}