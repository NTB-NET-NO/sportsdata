﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The sports event.
    /// </summary>
    [XmlRoot("sports-event")]
    [JsonObject("sportsEvent")]
    public class SportsEvent
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [XmlAttribute("id")]
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the event meta data.
        /// </summary>
        [XmlElement("event-metadata")]
        [JsonProperty("eventMetadata", NullValueHandling = NullValueHandling.Ignore)]
        public EventMetaData EventMetaData { get; set; }

        /// <summary>
        /// Gets or sets the team.
        /// </summary>
        [XmlElement("team")]
        [JsonProperty("teams", NullValueHandling = NullValueHandling.Ignore)]
        public Team[] Team { get; set; }

        /// <summary>
        /// Gets or sets the player.
        /// </summary>
        [XmlElement("player")]
        [JsonProperty("players", NullValueHandling = NullValueHandling.Ignore)]
        public Player[] Player { get; set; }

        /// <summary>
        /// Gets or sets the officials.
        /// </summary>
        [XmlElement("officials")]
        [JsonProperty("officials", NullValueHandling = NullValueHandling.Ignore)]
        public Officials Officials { get; set; }

        /// <summary>
        /// Gets or sets the player.
        /// </summary>
        [XmlElement("actions")]
        [JsonProperty("actions", NullValueHandling = NullValueHandling.Ignore)]
        public EventActions EventActions { get; set; }
    }
}