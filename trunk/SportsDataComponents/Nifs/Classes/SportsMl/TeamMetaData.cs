﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The team meta data.
    /// </summary>
    [XmlRoot("team-metadata")]
    public class TeamMetaData
    {
        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        [XmlAttribute("key")]
        [JsonProperty("key", NullValueHandling = NullValueHandling.Ignore)]
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the alignment.
        /// </summary>
        [XmlAttribute("alignment")]
        [JsonProperty("alignment", NullValueHandling = NullValueHandling.Ignore)]
        public string Alignment { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [XmlElement("name")]
        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public Name[] TeamNames { get; set; }

        /// <summary>
        /// Gets or sets the home location.
        /// </summary>
        [XmlElement("home-location")]
        [JsonProperty("homeLocation", NullValueHandling = NullValueHandling.Ignore)]
        public HomeLocation HomeLocation { get; set; }

        /// <summary>
        /// Gets or sets the home location.
        /// </summary>
        [XmlElement("sports-content-codes")]
        [JsonProperty("sportsContentCodes", NullValueHandling = NullValueHandling.Ignore)]
        public SportsContentCodes SportsContentCodes { get; set; }
    }
}