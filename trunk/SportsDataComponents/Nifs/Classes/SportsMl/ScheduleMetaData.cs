﻿using System;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The schedule meta data.
    /// </summary>
    [XmlRoot("schedule-metadata")]
    [JsonObject("scheduleMetadata")]
    public class ScheduleMetaData
    {
        /// <summary>
        /// Gets or sets the start date time.
        /// </summary>
        [XmlAttribute("start-date-time")]
        [JsonProperty("startDatetime", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime StartDateTime { get; set; }

        /// <summary>
        /// Gets or sets the end date time.
        /// </summary>
        [XmlAttribute("end-date-time")]
        [JsonProperty("endDatetime", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime EndDateTime { get; set; }
    }
}