using System;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The player.
    /// </summary>
    [Serializable]
    [XmlRoot("player")]
    [JsonObject("player")]
    public class Player
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [XmlAttribute("id")]
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the player meta data.
        /// </summary>
        [XmlElement("player-metadata")]
        [JsonProperty("playerMetadata", NullValueHandling = NullValueHandling.Ignore)]
        public PlayerMetaData PlayerMetaData { get; set; }

        /// <summary>
        /// Gets or sets the player stats.
        /// </summary>
        [XmlElement("player-stats")]
        [JsonProperty("playerStats", NullValueHandling = NullValueHandling.Ignore)]
        public PlayerStats PlayerStats { get; set; }
    }
}