﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The site meta data.
    /// </summary>
    [XmlRoot("site-metadata")]
    [JsonObject("siteMetadata")]
    public class SiteMetaData
    {
        /// <summary>
        /// Gets or sets the site key.
        /// </summary>
        [XmlAttribute("key")]
        [JsonProperty("key", NullValueHandling = NullValueHandling.Ignore)]
        public string SiteKey { get; set; }

        /// <summary>
        /// Gets or sets the capacity.
        /// </summary>
        [XmlAttribute("capacity")]
        [JsonProperty("capacity", NullValueHandling = NullValueHandling.Ignore)]
        public string Capacity { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [XmlElement("name")]
        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public Name[] Name { get; set; }

        /// <summary>
        /// Gets or sets the home location.
        /// </summary>
        [XmlElement("home-location")]
        [JsonProperty("homeLocation", NullValueHandling = NullValueHandling.Ignore)]
        public HomeLocation HomeLocation { get; set; }
    }
}