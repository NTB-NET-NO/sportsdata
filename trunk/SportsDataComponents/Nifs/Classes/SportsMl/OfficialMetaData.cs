﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The official meta data.
    /// </summary>
    [XmlRoot("official-metadata")]
    [JsonObject("officialMetadata")]
    public class OfficialMetaData
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        [XmlAttribute("id")]
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the official key.
        /// </summary>
        [XmlAttribute("key")]
        [JsonProperty("key", NullValueHandling = NullValueHandling.Ignore)]
        public string OfficialKey { get; set; }

        /// <summary>
        /// Gets or sets the official source.
        /// </summary>
        [XmlAttribute("official-source")]
        [JsonProperty("officialSource", NullValueHandling = NullValueHandling.Ignore)]
        public string OfficialSource { get; set; }

        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        [XmlAttribute("position-event")]
        [JsonProperty("positionEvent", NullValueHandling = NullValueHandling.Ignore)]
        public string PositionEvent { get; set; }

        /// <summary>
        /// Gets or sets the uniform number.
        /// </summary>
        [XmlAttribute("uniform-number")]
        [JsonProperty("uniformNumber", NullValueHandling = NullValueHandling.Ignore)]
        public string UniformNumber { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [XmlElement("name")]
        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public Name Name { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [XmlElement("home-location")]
        [JsonProperty("homeLocation", NullValueHandling = NullValueHandling.Ignore)]
        public HomeLocation HomeLocation { get; set; }
    }
}