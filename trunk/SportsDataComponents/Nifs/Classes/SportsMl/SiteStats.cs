﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The site stats.
    /// </summary>
    [XmlRoot("site-stats")]
    [JsonObject("siteStats")]
    public class SiteStats
    {
        /// <summary>
        /// Gets or sets the attendance.
        /// </summary>
        [XmlAttribute("attendance")]
        [JsonProperty("attendance", NullValueHandling = NullValueHandling.Ignore)]
        public int Attendance { get; set; }

        /// <summary>
        /// Gets or sets the temperature.
        /// </summary>
        [XmlAttribute("temperature")]
        [JsonProperty("temperature", NullValueHandling = NullValueHandling.Ignore)]
        public string Temperature { get; set; }

        /// <summary>
        /// Gets or sets the weather code.
        /// </summary>
        [XmlAttribute("weather-code")]
        [JsonProperty("weatherCode", NullValueHandling = NullValueHandling.Ignore)]
        public string WeatherCode { get; set; }

        /// <summary>
        /// Gets or sets the weather wind.
        /// </summary>
        [XmlAttribute("weather-wind")]
        [JsonProperty("weatherWind", NullValueHandling = NullValueHandling.Ignore)]
        public string WeatherWind { get; set; }

        /// <summary>
        /// Gets or sets the weather label.
        /// </summary>
        [XmlAttribute("weather-label")]
        [JsonProperty("weatherLabel", NullValueHandling = NullValueHandling.Ignore)]
        public string WeatherLabel { get; set; }

        /// <summary>
        /// Gets or sets the weather prediction.
        /// </summary>
        [XmlAttribute("weather-prediction")]
        [JsonProperty("weatherPrediction", NullValueHandling = NullValueHandling.Ignore)]
        public string WeatherPrediction { get; set; }
    }
}