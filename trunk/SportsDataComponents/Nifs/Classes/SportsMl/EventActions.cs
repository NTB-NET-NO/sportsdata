﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The event actions.
    /// </summary>
    [XmlRoot("actions")]
    [JsonObject("eventActions")]
    public class EventActions
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [XmlAttribute("id")]
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the class.
        /// </summary>
        [XmlAttribute("class")]
        [JsonProperty("class", NullValueHandling = NullValueHandling.Ignore)]
        public string Class { get; set; }

        /// <summary>
        /// Gets or sets the class.
        /// </summary>
        [XmlAttribute("style")]
        [JsonProperty("style", NullValueHandling = NullValueHandling.Ignore)]
        public string Style { get; set; }

        /// <summary>
        /// Gets or sets the class.
        /// </summary>
        [XmlElement("action")]
        [JsonProperty("action", NullValueHandling = NullValueHandling.Ignore)]
        public EventAction[] EventAction { get; set; }

        /// <summary>
        /// The should serialize player.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ShouldSerializeEventAction()
        {
            return this.EventAction.Length > 0;
        }
    }
}