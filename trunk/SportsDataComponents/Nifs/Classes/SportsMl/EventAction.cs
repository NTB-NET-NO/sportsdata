﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The event actions soccer.
    /// </summary>
    [XmlRoot("action")]
    [JsonObject("action")]
    public class EventAction
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [XmlAttribute("id")]
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        [XmlAttribute("sequence-number")]
        [JsonProperty("sequenceNumber", NullValueHandling = NullValueHandling.Ignore)]
        public string SequenceNumber { get; set; }

        /// <summary>
        /// Gets or sets the score team.
        /// </summary>
        [XmlAttribute("score-team")]
        [JsonProperty("scoreTeam", NullValueHandling = NullValueHandling.Ignore)]
        public string ScoreTeam { get; set; }

        /// <summary>
        /// Gets or sets the score team.
        /// </summary>
        [XmlAttribute("team-idref")]
        [JsonProperty("teamIdref", NullValueHandling = NullValueHandling.Ignore)]
        public string TeamIdref { get; set; }

        /// <summary>
        /// Gets or sets the score team.
        /// </summary>
        [XmlAttribute("opposing-team-idref")]
        [JsonProperty("opposingTeamIdref", NullValueHandling = NullValueHandling.Ignore)]
        public string OpposingTeamIdref { get; set; }

        /// <summary>
        /// Gets or sets the score team.
        /// </summary>
        [XmlAttribute("score-team-opposing")]
        [JsonProperty("scoreTeamOpposing", NullValueHandling = NullValueHandling.Ignore)]
        public string ScoreTeamOpposing { get; set; }

        /// <summary>
        /// Gets or sets the score team.
        /// </summary>
        [XmlAttribute("score-type")]
        [JsonProperty("scoreType", NullValueHandling = NullValueHandling.Ignore)]
        public string ScoreType { get; set; }

        /// <summary>
        /// Gets or sets the score team.
        /// </summary>
        [XmlAttribute("time-elapsed")]
        [JsonProperty("timeElapsed", NullValueHandling = NullValueHandling.Ignore)]
        public string TimeElapsed { get; set; }

        /// <summary>
        /// Gets or sets the score team.
        /// </summary>
        [XmlAttribute("time-remaining")]
        [JsonProperty("timeRemaining", NullValueHandling = NullValueHandling.Ignore)]
        public string TimeRemaining { get; set; }

        /// <summary>
        /// Gets or sets the score team.
        /// </summary>
        [XmlAttribute("player-count")]
        [JsonProperty("playerCount", NullValueHandling = NullValueHandling.Ignore)]
        public string PlayerCount { get; set; }

        /// <summary>
        /// Gets or sets the player id ref.
        /// </summary>
        [XmlAttribute("player-idref")]
        [JsonProperty("playerIdref", NullValueHandling = NullValueHandling.Ignore)]
        public string PlayerIdRef { get; set; }

        /// <summary>
        /// Gets or sets the score team.
        /// </summary>
        [XmlAttribute("minutes-elapsed")]
        [JsonProperty("minutesElapsed", NullValueHandling = NullValueHandling.Ignore)]
        public string MinutesElapsed { get; set; }

        /// <summary>
        /// Gets or sets the period value.
        /// </summary>
        [XmlAttribute("period-value")]
        [JsonProperty("periodValue", NullValueHandling = NullValueHandling.Ignore)]
        public string PeriodValue { get; set; }

        /// <summary>
        /// Gets or sets the score team.
        /// </summary>
        [XmlAttribute("period-minute-elapsed")]
        [JsonProperty("periodMinuteElapsed", NullValueHandling = NullValueHandling.Ignore)]
        public string PeriodMinuteElapsed { get; set; }

        /// <summary>
        /// Gets or sets the score team.
        /// </summary>
        [XmlAttribute("period-time-elapsed")]
        [JsonProperty("periodTimeElapsed", NullValueHandling = NullValueHandling.Ignore)]
        public string PeriodTimeElapsed { get; set; }

        /// <summary>
        /// Gets or sets the score team.
        /// </summary>
        [XmlAttribute("period-time-remaining")]
        [JsonProperty("periodTimeRemaining", NullValueHandling = NullValueHandling.Ignore)]
        public string PeriodTimeRemaining { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        [XmlAttribute("type")]
        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the participant.
        /// </summary>
        [XmlElement("participant")]
        [JsonProperty("participant", NullValueHandling = NullValueHandling.Ignore)]
        public Participant Participant { get; set; }

        /// <summary>
        /// Gets or sets the participant.
        /// </summary>
        [XmlIgnore]
        [JsonProperty("participants", NullValueHandling = NullValueHandling.Ignore)]
        public Participants Participants { get; set; }
    }
}