﻿using System.Xml.Serialization;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The sports content qualifier.
    /// </summary>
    [XmlRoot("sports-content-qualifier")]
    public class SportsContentQualifier
    {
        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        [XmlAttribute("gender")]
        public string Gender { get; set; }
    }
}