﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The rank.
    /// </summary>
    [XmlRoot("official")]
    [JsonObject("official")]
    public class Officials
    {
        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        [XmlElement("official")]
        [JsonProperty("official", NullValueHandling = NullValueHandling.Ignore)]
        public Official[] Official { get; set; }

        /// <summary>
        /// The should serialize official.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ShouldSerializeOfficial()
        {
            return this.Official.Length > 0;
        }
    }
}