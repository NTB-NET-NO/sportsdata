﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The standing meta data.
    /// </summary>
    [XmlRoot("standing-metadata")]
    [JsonObject("standingMetadata")]
    public class StandingMetaData
    {
        /// <summary>
        /// Gets or sets the team coverage.
        /// </summary>
        [XmlAttribute("team-coverage")]
        [JsonProperty("teamCoverage", NullValueHandling = NullValueHandling.Ignore)]
        public string TeamCoverage { get; set; }

        /// <summary>
        /// Gets or sets the alignment scope.
        /// </summary>
        [XmlAttribute("alignment-scope")]
        [JsonProperty("alignmentScope", NullValueHandling = NullValueHandling.Ignore)]
        public string AlignmentScope { get; set; }
    }
}