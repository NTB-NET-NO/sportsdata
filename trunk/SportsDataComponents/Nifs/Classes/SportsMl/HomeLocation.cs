﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The home location.
    /// </summary>
    [XmlRoot("home-location")]
    [JsonObject("homeLocation")]
    public class HomeLocation
    {
        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        [XmlElement("name")]
        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public Name Name { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        [XmlAttribute("qcode")]
        [JsonProperty("qcode", NullValueHandling = NullValueHandling.Ignore)]
        public string Qcode { get; set; }
    }
}