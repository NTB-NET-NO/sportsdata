﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The outcome totals.
    /// </summary>
    [XmlRoot("outcome-totals")]
    [JsonObject("outcomeTotals")]
    public class OutcomeTotals
    {
        /// <summary>
        /// Gets or sets the alignment scope.
        /// </summary>
        [XmlAttribute("alignment-scope")]
        [JsonProperty("alignmentScope", NullValueHandling = NullValueHandling.Ignore)]
        public string AlignmentScope { get; set; }

        /// <summary>
        /// Gets or sets the events played.
        /// </summary>
        [XmlAttribute("events-played")]
        [JsonProperty("eventsPlayed", NullValueHandling = NullValueHandling.Ignore)]
        public int EventsPlayed { get; set; }

        /// <summary>
        /// Gets or sets the wins.
        /// </summary>
        [XmlAttribute("wins")]
        [JsonProperty("wins", NullValueHandling = NullValueHandling.Ignore)]
        public int Wins { get; set; }

        /// <summary>
        /// Gets or sets the ties.
        /// </summary>
        [XmlAttribute("ties")]
        [JsonProperty("ties", NullValueHandling = NullValueHandling.Ignore)]
        public int Ties { get; set; }

        /// <summary>
        /// Gets or sets the losses.
        /// </summary>
        [XmlAttribute("losses")]
        [JsonProperty("losses", NullValueHandling = NullValueHandling.Ignore)]
        public int Losses { get; set; }

        /// <summary>
        /// Gets or sets the standing points.
        /// </summary>
        [XmlAttribute("standing-points")]
        [JsonProperty("standingPoints", NullValueHandling = NullValueHandling.Ignore)]
        public int StandingPoints { get; set; }

        /// <summary>
        /// Gets or sets the points scored for.
        /// </summary>
        [XmlAttribute("points-scored-for")]
        [JsonProperty("pointsScoredFor", NullValueHandling = NullValueHandling.Ignore)]
        public int PointsScoredFor { get; set; }

        /// <summary>
        /// Gets or sets the points scored against.
        /// </summary>
        [XmlAttribute("points-scored-against")]
        [JsonProperty("pointsScoredAgainst", NullValueHandling = NullValueHandling.Ignore)]
        public int PointsScoredAgainst { get; set; }
    }
}