﻿using System;
using System.Globalization;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The sports meta data.
    /// </summary>
    [XmlRoot("sports-metadata")]
    [JsonObject("sportsMetadata")]
    public class SportsMetaData
    {
        /// <summary>
        /// Gets or sets the doc id.
        /// </summary>
        [XmlAttribute(AttributeName = "doc-id")]
        [JsonProperty("docId", NullValueHandling = NullValueHandling.Ignore)]
        public string DocId { get; set; }

        /// <summary>
        /// Gets or sets the publisher.
        /// </summary>
        [XmlAttribute(AttributeName = "publisher")]
        [JsonProperty("publisher", NullValueHandling = NullValueHandling.Ignore)]
        public string Publisher { get; set; }

        /// <summary>
        /// Gets or sets the slug.
        /// </summary>
        [XmlAttribute(AttributeName = "slug")]
        [JsonProperty("slug", NullValueHandling = NullValueHandling.Ignore)]
        public string Slug { get; set; }

        /// <summary>
        /// Gets or sets the start date time.
        /// </summary>
        [XmlAttribute(AttributeName = "start-date-time")]
        [JsonProperty("startDatetime", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime StartDateTime { get; set; }

        /// <summary>
        /// Gets or sets the start date time.
        /// </summary>
        [XmlAttribute(AttributeName = "end-date-time")]
        [JsonProperty("endDatetime", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime EndDateTime { get; set; }

        /// <summary>
        /// Gets or sets the sports content codes.
        /// </summary>
        [XmlElement("sports-content-codes")]
        [JsonProperty("sportsContentCodes", NullValueHandling = NullValueHandling.Ignore)]
        public SportsContentCodes SportsContentCodes { get; set; }

        /// <summary>
        /// Gets or sets the sports title.
        /// </summary>
        [XmlElement("sports-title")]
        [JsonProperty("sportsTitle", NullValueHandling = NullValueHandling.Ignore)]
        public string SportsTitle { get; set; }

        /// <summary>
        /// The should serialize Sports title.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ShouldSerializeSportsTitle()
        {
            return this.SportsTitle != string.Empty;
        }

        /// <summary>
        /// The should serialize Sports title.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ShouldSerializeDocId()
        {
            return this.DocId != string.Empty;
        }

        /// <summary>
        /// The should serialize Sports title.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ShouldSerializeSlug()
        {
            return this.Slug != string.Empty;
        }

        /// <summary>
        /// The should serialize End Date Time.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ShouldSerializeEndDateTime()
        {
            DateTime dateValue;
            return DateTime.TryParseExact(this.EndDateTime.ToString("yyyy-MM-dd hh:mm:ss"), "M/d/yyyy h:mm:ss tt", new CultureInfo("en-US"), DateTimeStyles.None, out dateValue);
        }

        /// <summary>
        /// The should serialize Start Date Time.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ShouldSerializeStartDateTime()
        {
            DateTime dateValue;
            return DateTime.TryParseExact(this.StartDateTime.ToString("yyyy-MM-dd hh:mm:ss"), "M/d/yyyy h:mm:ss tt", new CultureInfo("en-US"), DateTimeStyles.None, out dateValue);
        }
    }
}