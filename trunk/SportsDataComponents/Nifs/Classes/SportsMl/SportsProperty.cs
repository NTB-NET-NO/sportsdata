﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The sports property.
    /// </summary>
    [XmlRoot("sports-property")]
    [JsonObject("sportsProperty")]
    public class SportsProperty
    {
        /// <summary>
        /// Gets or sets the formal name.
        /// </summary>
        /// <summary>
        /// Gets or sets the team id.
        /// </summary>
        [XmlAttribute("formal-name")]
        [JsonProperty("formalName", NullValueHandling = NullValueHandling.Ignore)]
        public string FormalName { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <summary>
        /// Gets or sets the team id.
        /// </summary>
        [XmlAttribute("value")]
        [JsonProperty("value", NullValueHandling = NullValueHandling.Ignore)]
        public string Value { get; set; }
    }
}