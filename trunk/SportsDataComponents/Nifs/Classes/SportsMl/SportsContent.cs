﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The sports content.
    /// </summary>
    [XmlRoot("sports-content")]
    [JsonObject("sportsContent")]
    public class SportsContent
    {
        /// <summary>
        /// Gets or sets the sports meta data.
        /// </summary>
        [XmlElement("sports-metadata")]
        [JsonProperty("sportsMetadata", NullValueHandling = NullValueHandling.Ignore)]
        public SportsMetaData SportsMetaData { get; set; }

        /// <summary>
        /// Gets or sets the sports event.
        /// </summary>
        [XmlElement("sports-event")]
        [JsonProperty("sportsEvents", NullValueHandling = NullValueHandling.Ignore)]
        public SportsEvent[] SportsEvent { get; set; }

        /// <summary>
        /// Gets or sets the standing.
        /// </summary>
        [XmlElement("standing")]
        [JsonProperty("standing", NullValueHandling = NullValueHandling.Ignore)]
        public Standing Standing { get; set; }

        /// <summary>
        /// Gets or sets the schedule.
        /// </summary>
        [XmlElement("schedule")]
        [JsonProperty("schedule", NullValueHandling = NullValueHandling.Ignore)]
        public Schedule Schedule { get; set; }

        /// <summary>
        /// Gets or sets the schedule.
        /// </summary>
        [XmlElement("tournament")]
        [JsonProperty("tournaments", NullValueHandling = NullValueHandling.Ignore)]
        public Tournament[] Tournament { get; set; }

        /// <summary>
        /// Gets or sets the team.
        /// </summary>
        [XmlIgnore]
        [JsonProperty("teams", NullValueHandling = NullValueHandling.Ignore)]
        public Team[] Team { get; set; }

        /// <summary>
        /// Gets or sets the team.
        /// </summary>
        [XmlIgnore]
        [JsonProperty("players", NullValueHandling = NullValueHandling.Ignore)]
        public Player[] Player { get; set; }

        [XmlElement("article")]
        public Article Article { get; set; }
    }
}