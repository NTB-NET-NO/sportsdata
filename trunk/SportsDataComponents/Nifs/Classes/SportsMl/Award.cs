﻿using System.Xml.Serialization;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The award.
    /// </summary>
    [XmlRoot("award")]
    public class Award
    {
        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        [XmlAttribute("value")]
        public string Value { get; set; }

        /// <summary>
        /// Gets or sets the award type.
        /// </summary>
        [XmlAttribute("award-type")]
        public string AwardType { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [XmlAttribute("name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the total.
        /// </summary>
        [XmlAttribute("total")]
        public int Total { get; set; }
    }
}