﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The sports content code.
    /// </summary>
    [XmlRoot("sports-content-code")]
    [JsonObject("sportsContentCode")]
    public class SportsContentCode
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [XmlAttribute("code-type")]
        [JsonProperty("codeType", NullValueHandling = NullValueHandling.Ignore)]
        public string CodeType { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [XmlAttribute("code-name")]
        [JsonProperty("codeName", NullValueHandling = NullValueHandling.Ignore)]
        public string CodeName { get; set; }

        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        [XmlAttribute("code-key")]
        [JsonProperty("codeKey", NullValueHandling = NullValueHandling.Ignore)]
        public string CodeKey { get; set; }

        /// <summary>
        /// Gets or sets the code source.
        /// </summary>
        [XmlAttribute("code-source")]
        [JsonProperty("codeSource", NullValueHandling = NullValueHandling.Ignore)]
        public string CodeSource { get; set; }

        /// <summary>
        /// Gets or sets the sports content qualifier.
        /// </summary>
        [XmlElement("sports-content-qualifier")]
        [JsonProperty("sportsContentQualifier", NullValueHandling = NullValueHandling.Ignore)]
        public SportsContentQualifier SportsContentQualifier { get; set; }

        /// <summary>
        /// The should serialize CodeKey.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ShouldSerializeCodeKey()
        {
            return this.CodeKey != string.Empty;
        }

        /// <summary>
        /// The should serialize SportsContentQualifier.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ShouldSerializeSportsContentQualifier()
        {
            return this.SportsContentQualifier != null;
        }
    }
}