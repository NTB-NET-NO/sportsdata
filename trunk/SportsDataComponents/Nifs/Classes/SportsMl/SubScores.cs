﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The sub scores.
    /// </summary>
    [JsonObject("subScores")]
    public class SubScores
    {
        /// <summary>
        /// Gets or sets the uniform number.
        /// </summary>
        [XmlIgnore]
        [JsonProperty("subScore", NullValueHandling = NullValueHandling.Ignore)]
        public SubScore[] SubScore { get; set; }
    }
}