﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The tournament.
    /// </summary>
    [XmlRoot("tournament")]
    [JsonObject("tournament")]
    public class Tournament
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [XmlAttribute("id")]
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the tournament meta data.
        /// </summary>
        [XmlElement("tournament-metadata")]
        [JsonProperty("tournamentMetadata", NullValueHandling = NullValueHandling.Ignore)]
        public TournamentMetaData TournamentMetaData { get; set; }

        /// <summary>
        /// Gets or sets the sports event.
        /// </summary>
        [XmlElement("sports-event")]
        [JsonProperty("sportsEvent", NullValueHandling = NullValueHandling.Ignore)]
        public SportsEvent SportsEvent { get; set; }
    }
}