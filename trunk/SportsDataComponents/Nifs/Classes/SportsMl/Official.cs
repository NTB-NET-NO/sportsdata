﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The rank.
    /// </summary>
    [XmlRoot("official")]
    [JsonObject("official")]
    public class Official
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [XmlAttribute("id")]
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        [XmlElement("official-metadata")]
        [JsonProperty("officialMetadata", NullValueHandling = NullValueHandling.Ignore)]
        public OfficialMetaData OfficialMetaData { get; set; }

        /// <summary>
        /// Gets or sets the affiliation.
        /// </summary>
        [XmlElement("affiliation")]
        [JsonProperty("affiliation", NullValueHandling = NullValueHandling.Ignore)]
        public Affiliation Affiliation { get; set; }
    }
}