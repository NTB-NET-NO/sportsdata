﻿namespace NTB.SportsData.Components.Nifs.Classes.SportsMl.Eums
{
    /// <summary>
    /// The team stats event outcome.
    /// </summary>
    public enum TeamStatsEventOutcome
    {
        /// <summary>
        /// The win.
        /// </summary>
        Win,

        /// <summary>
        /// The loss.
        /// </summary>
        Loss,

        /// <summary>
        /// The tie.
        /// </summary>
        Tie
    }
}