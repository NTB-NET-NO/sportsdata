﻿namespace NTB.SportsData.Components.Nifs.Classes.SportsMl.Eums
{
    /// <summary>
    /// The team meta data alignment.
    /// </summary>
    public enum TeamMetaDataAlignment
    {
        /// <summary>
        /// The home.
        /// </summary>
        Home,

        /// <summary>
        /// The away.
        /// </summary>
        Away,

        /// <summary>
        /// The neutral.
        /// </summary>
        Neutral
    }
}