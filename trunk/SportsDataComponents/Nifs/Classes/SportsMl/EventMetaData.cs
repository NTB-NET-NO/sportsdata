﻿using System;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The event meta data.
    /// </summary>
    [XmlRoot("event-metadata")]
    public class EventMetaData
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [XmlAttribute("id")]
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        [XmlAttribute("key")]
        [JsonProperty("key", NullValueHandling = NullValueHandling.Ignore)]
        public string EventKey { get; set; }

        /// <summary>
        /// Gets or sets the start date time.
        /// </summary>
        [XmlAttribute("start-date-time")]
        [JsonProperty("startDateTime", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime StartDateTime { get; set; }

        /// <summary>
        /// Gets or sets the end date time.
        /// </summary>
        [XmlAttribute("end-date-time")]
        [JsonProperty("endDateTime", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime EndDateTime { get; set; }

        /// <summary>
        /// Gets or sets the end date time.
        /// </summary>
        [XmlAttribute("event-status")]
        [JsonProperty("eventStatus", NullValueHandling = NullValueHandling.Ignore)]
        public string EventStatus { get; set; }

        /// <summary>
        /// Gets or sets the event name.
        /// </summary>
        [XmlElement("name")]
        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public Name EventName { get; set; }

        /// <summary>
        /// Gets or sets the event source.
        /// </summary>
        [XmlAttribute("event-source")]
        [JsonProperty("eventSource", NullValueHandling = NullValueHandling.Ignore)]
        public string EventSource { get; set; }

        /// <summary>
        /// Gets or sets the site.
        /// </summary>
        [XmlElement("site")]
        [JsonProperty("site", NullValueHandling = NullValueHandling.Ignore)]
        public Site Site { get; set; }

        /// <summary>
        /// Gets or sets the sports content codes.
        /// </summary>
        [XmlElement("sports-content-codes")]
        [JsonProperty("sportsContentCodes", NullValueHandling = NullValueHandling.Ignore)]
        public SportsContentCodes SportsContentCodes { get; set; }
    }
}