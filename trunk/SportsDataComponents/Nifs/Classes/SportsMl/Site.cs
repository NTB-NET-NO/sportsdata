﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The site.
    /// </summary>
    [XmlRoot("site")]
    [JsonObject("site")]
    public class Site
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [XmlAttribute("id")]
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the site meta data.
        /// </summary>
        [XmlElement("site-metadata")]
        [JsonProperty("siteMetadata", NullValueHandling = NullValueHandling.Ignore)]
        public SiteMetaData SiteMetaData { get; set; }

        /// <summary>
        /// Gets or sets the site stats.
        /// </summary>
        [XmlElement("site-stats")]
        [JsonProperty("siteStats", NullValueHandling = NullValueHandling.Ignore)]
        public SiteStats SiteStats { get; set; }
    }
}