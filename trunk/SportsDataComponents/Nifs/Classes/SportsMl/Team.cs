﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The team.
    /// </summary>
    [XmlRoot("team")]
    public class Team
    {
        /// <summary>
        /// Gets or sets the team id.
        /// </summary>
        [XmlAttribute("id")]
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public string TeamId { get; set; }

        /// <summary>
        /// Gets or sets the team meta data.
        /// </summary>
        [XmlElement("team-metadata")]
        [JsonProperty("teamMetadata", NullValueHandling = NullValueHandling.Ignore)]
        public TeamMetaData TeamMetaData { get; set; }

        /// <summary>
        /// Gets or sets the team stats.
        /// </summary>
        [XmlElement("team-stats")]
        [JsonProperty("teamStats", NullValueHandling = NullValueHandling.Ignore)]
        public TeamStats TeamStats { get; set; }

        /// <summary>
        /// Gets or sets the player.
        /// </summary>
        [XmlElement("player")]
        [JsonProperty("players", NullValueHandling = NullValueHandling.Ignore)]
        public Player[] Player { get; set; }

        /// <summary>
        /// The should serialize player.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ShouldSerializePlayer()
        {
            if (this.Player != null)
            {
                return this.Player.Length > 0;
            }

            return false;
        }
    }
}