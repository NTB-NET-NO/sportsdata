﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace NTB.SportsData.Components.Nifs.Classes.SportsMl
{
    /// <summary>
    /// The sub score.
    /// </summary>
    [XmlRoot("sub-score")]
    [JsonObject("subScore")]
    public class SubScore
    {
        /// <summary>
        /// Gets or sets the sub score type.
        /// </summary>
        [XmlAttribute("sub-score-type")]
        [JsonProperty("subScoretype", NullValueHandling = NullValueHandling.Ignore)]
        public string SubScoreType { get; set; }

        /// <summary>
        /// Gets or sets the sub score name.
        /// </summary>
        [XmlAttribute("sub-score-name")]
        [JsonProperty("subScoreName", NullValueHandling = NullValueHandling.Ignore)]
        public string SubScoreName { get; set; }

        /// <summary>
        /// Gets or sets the score.
        /// </summary>
        [XmlAttribute("score")]
        [JsonProperty("score", NullValueHandling = NullValueHandling.Ignore)]
        public string Score { get; set; }

        /// <summary>
        /// Gets or sets the score.
        /// </summary>
        [XmlAttribute("rank")]
        [JsonProperty("rank", NullValueHandling = NullValueHandling.Ignore)]
        public string Rank { get; set; }

        /// <summary>
        /// Gets or sets the score.
        /// </summary>
        [XmlAttribute("period-value")]
        [JsonProperty("periodValue", NullValueHandling = NullValueHandling.Ignore)]
        public string PeriodValue { get; set; }
    }
}