﻿using System.Xml.Serialization;

namespace NTB.SportsData.Components.Nifs.Classes.Nitf
{
    [XmlRoot("nitf")]
    public class Nitf
    {
        [XmlElement("head")]
        public Head Head { get; set; }

        [XmlElement("body")]
        public Body Body { get; set; }
    }

    public class Head
    {
        [XmlElement("meta")]
        public Meta[] Meta { get; set; }
    }

    public class Meta
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("content")]
        public string Content { get; set; }
    }

    public class Body
    {
        [XmlElement("body.head")]
        public BodyHead Head { get; set; }

        [XmlElement("body.content")]
        public Content Content { get; set; }
    }

    public class BodyHead
    {
        [XmlElement("hedline")]
        public Headline Headline { get; set; }

        [XmlElement("distributor")]
        public Distributor Distributor { get; set; }

        [XmlElement("dateline")]
        public Dateline Dateline { get; set; }
    }

    public class Headline
    {
        [XmlElement("hl1")]
        public string Title { get; set; }
    }

    public class Distributor
    {
        [XmlElement("org")]
        public string Org { get; set; }
    }

    public class Dateline
    {
        [XmlElement("location")]
        public string Location { get; set; }
    }

    public class Content
    {
        [XmlElement("p")]
        public Paragraph[] Paragraph { get; set; }
    }

    public class Paragraph
    {
        [XmlText]
        public string Content { get; set; }

        [XmlAttribute("lede")]
        public bool Lead { get; set; }

        [XmlAttribute("class")]
        public string ParagraphClass { get; set; }
    }
}