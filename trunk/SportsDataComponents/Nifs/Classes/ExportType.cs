﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsData.Components.Nifs.Classes
{
    /// <summary>
    /// The export type.
    /// </summary>
    public class ExportType
    {
        /// <summary>
        /// Gets or sets the rank.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the url.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Gets or sets the server path.
        /// </summary>
        public string ServerPath { get; set; }

        /// <summary>
        /// Gets or sets the label.
        /// </summary>
        public string Label { get; set; }
    }
}
