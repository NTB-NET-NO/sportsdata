﻿using System;
using System.Configuration;
using System.IO;
using NTB.SportsData.Classes.Classes;

namespace NTB.SportsData.Components.Nifs.Helpers
{
    public class LatestFetchDateTimeHelper
    {
        public DateTime CurrentDateTime;

        public LatestFetchDateTimeHelper()
        {
            this.CurrentDateTime = DateTime.Now;
        }

        /// <summary>
        /// This method writes the last time we downloaded the data from NIF. 
        /// I am considering if there should be a default date-time from when I want to download - just in case.
        /// </summary>
        /// <param name="dataFileParams">
        /// Keeping the data around
        /// </param>
        /// <param name="interval">
        /// Value taken from the config-file
        /// </param>
        /// <returns>
        /// DateTime telling when we downloaded last. 
        /// DateTime is set to now minus one minute if the file do not exist.
        /// </returns>
        public DateTime GetLatestDownloadDateTime(DataFileParams dataFileParams, double interval)
        {
            var latestStoredDownloadDateTime = this.GetLatestStoredDownloadDateTime(dataFileParams);

            if (latestStoredDownloadDateTime == null)
            {
                // IF time is null, then we find now time and go back interval.
                latestStoredDownloadDateTime = DateTime.Now.AddMilliseconds(-interval);
            }

            latestStoredDownloadDateTime = this.GetValidDownloadTime(this.CurrentDateTime, latestStoredDownloadDateTime.Value, interval);

            // We know NIF has a cache, it used to be 5 minutes, but they have removed this on "our" methods.
            // This version has not hardcoded this, but uses a setting in the configuration file for the application.
            var nifMinuteBack = Convert.ToInt32(ConfigurationManager.AppSettings["nif_minute_back"]);
            latestStoredDownloadDateTime = latestStoredDownloadDateTime.Value.AddMinutes(-nifMinuteBack);
            return latestStoredDownloadDateTime.Value;
        }

        /// <summary>
        /// Method checking if the difference between current datetime and the stored is less than the max difference
        /// </summary>
        /// <param name="currentDateTime">current date time</param>
        /// <param name="storedDateTime">date time stored in file</param>
        /// <returns>integer telling how many hours difference - if any</returns>
        public int GetHourDifference(DateTime currentDateTime, DateTime storedDateTime)
        {
            var hoursDifference = (currentDateTime - storedDateTime).TotalHours;
            hoursDifference = Math.Round(hoursDifference);
            if (hoursDifference < 0)
            {
                hoursDifference = (storedDateTime - currentDateTime).TotalHours;
            }
            return Convert.ToInt32(hoursDifference);
        }

        public DateTime GetValidDownloadTime(DateTime currentDateTime, DateTime storedDateTime, double interval)
        {
            var validOldestHour = Convert.ToInt32(ConfigurationManager.AppSettings["MaxDownloadTimeSpan"]);

            var hoursDifference = this.GetHourDifference(currentDateTime, storedDateTime);
            // If the current dateTime - stored dateTime is larger than six hours, then we shall use the current date time
            if (hoursDifference > validOldestHour)
            {
                return currentDateTime.AddMilliseconds(-interval);
            }

            return storedDateTime;
        }

        public DateTime? GetLatestStoredDownloadDateTime(DataFileParams dataFileParams)
        {
            var filename = Path.Combine(dataFileParams.FileStorePath, dataFileParams.InstanceName.ToLower() + ".txt");
            var latestDownloadFileInfo = new FileInfo(filename);
            DateTime? lastDownloadedDateTime;
            if (!latestDownloadFileInfo.Exists)
            {
                return null;
            }
            else
            {
                var dateTimeStored = File.ReadAllText(filename);
                lastDownloadedDateTime = DateTime.Parse(dateTimeStored);
            }

            return lastDownloadedDateTime;
        }

        public void StoreLatestDownloadDateTime(DataFileParams dataFileParams, DateTime latestDownloadedDateTime)
        {
            var filename = Path.Combine(dataFileParams.FileStorePath, dataFileParams.InstanceName.ToLower() + ".txt");

            File.WriteAllText(filename, latestDownloadedDateTime.ToString("yyyy-MM-dd") + " " + latestDownloadedDateTime.ToString("HH:mm:ss"));
        }

    }
}
