﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NTB.SportsData.Classes.Classes;

namespace NTB.SportsData.Components.Nifs.Helpers
{
    public class DataCleaner
    {
        /// <summary>
        ///     Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(DataCleaner));

        /// <summary>
        /// The clean match referee.
        /// </summary>
        /// <param name="referees">
        /// The match.
        /// </param>
        public List<Referee> CleanMatchReferee(List<Referee> referees)
        {
            foreach (var referee in referees)
            {
                if (referee.RefereeName == null)
                {
                    continue;
                }

                Logger.DebugFormat("Name of referee: {0}", referee.RefereeName);
                try
                {
                    if (referee.RefereeName.Contains(","))
                    {
                        var refereePart = referee.RefereeName.Split(',');
                        referee.FirstName = refereePart[0].Trim();
                        referee.LastName = refereePart[1].Trim();
                    }
                    else
                    {
                        var refereePart = referee.RefereeName.Split(' ');
                        referee.FirstName = refereePart[0].Trim();

                        var lastnameArray = refereePart.Skip(1);

                        referee.LastName = string.Join(" ", lastnameArray);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                }
            }

            return referees;
        }

        /// <summary>
        /// The get match goal incidents.
        /// </summary>
        /// <param name="matchIncidents">
        /// The match incidents.
        /// </param>
        /// <param name="matchPlayers">
        /// The match players.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<MatchEvent> CleanMatchIncidents(List<MatchEvent> matchIncidents, List<Player> matchPlayers)
        {
            try
            {
                if (!matchIncidents.Any())
                {
                    return new List<MatchEvent>();
                }

                // goalIncidents = (from incidents in matchIncidents where incidents.IncidentType.ToLower().Contains("mål") select incidents).ToList();
                foreach (var i in matchIncidents)
                {
                    i.TeamId = (from m in matchPlayers where m.Id == i.PersonId select m.TeamId).SingleOrDefault();
                    i.MatchEventShort = i.MatchEventType;

                    if (i.MatchEventType.ToLower()
                        .Contains("mål"))
                    {
                        i.MatchEventType = "mål";
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);

                return null;
            }

            return matchIncidents;
        }
    }
}
