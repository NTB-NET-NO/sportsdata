﻿using System.Text.RegularExpressions;

namespace NTB.SportsData.Components.Nifs.Helpers
{
    public class EditorialMetaDataHelper
    {
        /// <summary>
        ///     Is All Upper checks if all characters in the string are all upper
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public bool IsAllUpper(string input)
        {
            for (var i = 0; i < input.Length; i++)
            {
                if (!char.IsUpper(input[i]))
                    return false;
            }

            return true;
        }

        public bool IsFirstCharacterUpper(string input)
        {
            if (char.IsUpper(input, 0))
            {
                return true;
            }

            return false;
        }

        public string GetGroupPlaceHolder(string s)
        {
            var regexMatch = Regex.Match(s, @"\{(group)\}", RegexOptions.IgnoreCase);
            if (regexMatch.Success)
            {
                var foundGroup = regexMatch.Groups[1].Value;
                return foundGroup;

            }

            // Not a match, we return an emtpy string
            return string.Empty;
        }

        public string UppercaseFirst(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }

            // Return char and concat substring.
            return char.ToUpper(s[0]) + s.Substring(1);
        }
    }
}
