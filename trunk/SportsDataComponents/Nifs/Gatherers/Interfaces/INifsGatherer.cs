﻿using System;
using System.Collections.Generic;
using NTB.SportsData.Classes.Classes;

namespace NTB.SportsData.Components.Nifs.Gatherers.Interfaces
{
    /// <summary>
    /// The TeamSportGatherer interface.
    /// </summary>
    /// <remarks>
    /// The way you have to query the remote data store you have to do the following
    /// Call the Nifs Endpoint (in test): https://test-ntb-api.nifs.no/matches/?updatedSince={DateTime} - 2017-02-07%20:00:00
    ///                        (int prod): https://ntb-api.nifs.no/matches/?updatedSince={DateTime} - 2017-02-07%20:00:00
    /// 
    /// Based on the call you will get a list of match objects which all has a Tournament Stage property. 
    /// Use the Tournament Stage property to group all matches from that tournament. 
    /// Get matches from today based on the Tournament Stage Ids you now have grouped
    /// Get Standing for the Tournament Stage 
    /// 
    /// Produce SportsML and send out
    /// 
    /// </remarks>
    public interface INifsGatherer
    {
        //List<ChangeInfo> GetChangesResultTeam(double interval, string userKey, string clientPassword,
        //    DateTime eventStartDate, DateTime eventEndDate);

        //Result GetMatchResultByResultId(int resultId);

        //Match GetMatchInfoByMatchId(int matchId);

        //// Result GetMatchResultByMatchId(int matchId);

        //List<Match> GetTournamentMatchesByTournamentIdAndDate(int tournamentId, DateTime? matchDate);

        //List<Referee> GetMatchRefereesByMatchId(int matchId);

        //List<Player> GetMatchPlayersByMatchId(int matchId);

        //List<MatchEvent> GetMatchIncidentsByMatchId(int matchId);

        //List<TeamResult> GetTournamentStandingByTournamentId(Tournament tournament);

        //List<int> GetUniqueTournaments(int tournamentId, List<int> tournamentIds);

        //Tournament GetTournamentById(int id);

        //List<PartialResult> GetPartialResultsForMatch(int matchId);

        //List<AgeCategory> GetTournamentAgeCategory(int tournamentId);

        //List<Match> GetTournamentMaintenanceMatchesByTournamentIdAndDate(int tournamentId, DateTime matchStartDate, DateTime matchEndDate);

        //List<ChangeInfo> GetTodaysChangesResultTeam(string userKey, string clientPassword, DateTime eventStartDate, DateTime eventEndDate);
    }
}