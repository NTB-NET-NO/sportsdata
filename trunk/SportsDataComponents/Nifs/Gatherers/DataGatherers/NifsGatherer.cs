﻿using System;
using System.Collections.Generic;
using System.Configuration;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nifs.Facade;
using NTB.SportsData.Components.Nifs.Facade.Interfaces;
using NTB.SportsData.Components.Nifs.Gatherers.Interfaces;

namespace NTB.SportsData.Components.Nifs.Gatherers.DataGatherers
{
    /// <summary>
    ///     The team sport gatherer.
    /// </summary>
    public class NifsGatherer : INifsGatherer
    {
        /// <summary>
        /// The facade.
        /// </summary>
        private readonly INifsFacade _facade;

        public NifsGatherer()
        {
            this._facade = new NifsFacade();
        }

        /// <summary>
        /// Gets or sets the match date start.
        /// </summary>
        public DateTime MatchDateStart { get; set; }

        /// <summary>
        /// Gets or sets the match date end.
        /// </summary>
        public DateTime MatchDateEnd { get; set; }

        
    }
}