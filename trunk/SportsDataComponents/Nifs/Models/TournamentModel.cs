﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using log4net;
using NTB.SportsData.Components.Nifs.Classes.Nifs;
using NTB.SportsData.Components.Nifs.Models.Interfaces;

namespace NTB.SportsData.Components.Nifs.Models
{
    public class TournamentModel : ITournamentModel
    {
        /// <summary>
        /// Filters out the stage Ids so we 
        /// </summary>
        /// <param name="matchesList"></param>
        /// <returns>
        /// List of Tournament Stages
        /// </returns>
        public List<TournamentStage> FilterTournamentStageIdsFromMatches(List<MatchFact> matchesList)
        {
            return matchesList
                .GroupBy(m => new {m.Stage.Id})
                .Select(g => g.First().Stage)
                .ToList();
        }
    }
}
