﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using log4net;
using Newtonsoft.Json;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nifs.Classes;
using NTB.SportsData.Components.Nifs.Classes.Nifs;
using NTB.SportsData.Components.Nifs.Classes.Nitf;
using NTB.SportsData.Components.Nifs.Classes.SportsMl;
using NTB.SportsData.Components.Nifs.Clients;
using NTB.SportsData.Components.Nifs.Data.DataMappers.Article;
using NTB.SportsData.Components.Nifs.Data.DataMappers.ExportTypes;
using NTB.SportsData.Components.Nifs.Data.DataMappers.Players;
using NTB.SportsData.Components.Nifs.Data.DataMappers.Teams;
using NTB.SportsData.Components.Nifs.Data.DataMappers.Tournaments;
using NTB.SportsData.Components.Nifs.Helpers;
using NTB.SportsData.Components.Nifs.Populators;
using NTB.SportsData.Components.PublicRepositories;
using Player = NTB.SportsData.Components.Nifs.Classes.SportsMl.Player;

namespace NTB.SportsData.Components.Nifs.Models
{
    public class SportsContentModel
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        ///     Gets or sets the tournament stage.
        /// </summary>
        public TournamentStage TournamentStage { get; set; }

        /// <summary>
        ///     Gets or sets the tournament division.
        /// </summary>
        public TournamentDivision TournamentDivision { get; set; }

        /// <summary>
        ///     Gets or sets the tournament.
        /// </summary>
        public TournamentRoundMetaData TournamentRoundMetaData { get; set; }

        public bool SeriesPairing { get; set; }

        /// <summary>
        ///     The create sports ml.
        /// </summary>
        /// <param name="sportsEvent">
        ///     The sports event.
        /// </param>
        /// <param name="standing">
        ///     The standing.
        /// </param>
        /// <param name="schedule">
        ///     The schedule.
        /// </param>
        /// <param name="selectedDateTime">
        ///     The selected DateTime.
        /// </param>
        /// <returns>
        ///     The <see cref="SportsContent" />.
        /// </returns>
        public SportsContent CreateSportsContent(SportsEvent[] sportsEvent, Standing standing, Schedule schedule, DateTime selectedDateTime)
        {
            var sportsContent = new SportsContent();

            try
            {
                var sportsMetaData = new SportsMetaData();
                var id = 0;
                var tournamentName = string.Empty;
                tournamentName = this.GetTournamentName(sportsEvent, tournamentName);

                if (this.TournamentStage != null)
                {
                    id = this.TournamentStage.Id;
                    tournamentName = this.TournamentStage.Tournament.Name.Replace(" ", string.Empty);
                }

                var docIdTournamentName = tournamentName;

                docIdTournamentName = Encoding.ASCII.GetString(
                    Encoding.Convert(
                        Encoding.UTF8,
                        Encoding.GetEncoding(
                            Encoding.ASCII.EncodingName,
                            new EncoderReplacementFallback(string.Empty),
                            new DecoderExceptionFallback()
                            ),
                        Encoding.UTF8.GetBytes(docIdTournamentName)
                    )
                );

                // check if document is in the database
                var documentRepository = new DocumentRepository();
                var version = documentRepository.GetDocumentVersionByTournamentId(id);

                var documentName = this.CreateDocumentBaseName(sportsEvent, id, docIdTournamentName);
                var document = new Document();
                document.Version = version;
                document.Created = DateTime.Now;
                document.TournamentId = id;
                document.SportId = 16;
                document.Filename = documentName;
                if (version == 1)
                {
                    documentRepository.InsertOne(document);
                }
                else
                {
                    document.Version = version;
                    documentRepository.Update(document);
                }

                // append version to filename
                // documentName = string.Format("{0}_V{1}", documentName, version);

                sportsMetaData.DocId = documentName;
                sportsMetaData.Slug = string.Format("fotball-{0}-data", tournamentName);
                sportsMetaData.StartDateTime = selectedDateTime;
                sportsMetaData.EndDateTime = selectedDateTime.AddHours(2);
                

                // matchFact.Name.Replace(" ", string.Empty)) };
                sportsContent.SportsMetaData = sportsMetaData;
                sportsContent.SportsMetaData.SportsContentCodes = new SportsContentCodes();

                // <sports-content-code code-type="ntbspcode:version" code-name="version" code-key="version:1" />
                var versionSportsContentCode = new SportsContentCode();
                versionSportsContentCode.CodeType = "ntbspcode:version";
                versionSportsContentCode.CodeName = version.ToString();
                versionSportsContentCode.CodeKey = "v:" + version;

                var sportsContentCodeTournament = new SportsContentCode();
                sportsContentCodeTournament.CodeType = "spct:tournament";
                if (this.TournamentStage != null)
                {
                    sportsContentCodeTournament.CodeName = this.TournamentStage.Tournament.Name;
                }

                sportsContentCodeTournament.CodeKey = "ntbtournamentkey:" + tournamentName;

                var sportsContentCodeLeague = new SportsContentCode();
                sportsContentCodeLeague.CodeType = "spct:league";
                var tournamentDataMapper = new TournamentDataMapper();
                var localTournament = new SportsData.Classes.Classes.Tournament();
                if (this.TournamentStage != null)
                {
                    sportsContentCodeLeague.CodeKey = "league:nitf-l." + this.TournamentStage.Id;

                    localTournament = tournamentDataMapper.GetTournamentByStageId(this.TournamentStage.Id);
                    if (localTournament != null)
                    {
                        sportsContentCodeLeague.CodeName = localTournament.Name;
                    }
                    else
                    {
                        sportsContentCodeLeague.CodeName = this.TournamentStage.Name;
                    }
                }
                else
                {
                    var checkedEvent = sportsEvent[0];
                    if (checkedEvent != null)
                    {
                        sportsContentCodeLeague.CodeKey = checkedEvent.EventMetaData.Id;
                        sportsContentCodeLeague.CodeName = checkedEvent.EventMetaData.EventName.FullName;
                    }
                }

                sportsContentCodeLeague.SportsContentQualifier = new SportsContentQualifier();

                // Checking if this is a tournament for men or women
                if ((this.TournamentStage != null) && (this.TournamentStage.Tournament != null && this.TournamentStage.Tournament.Gender != null))
                {
                    sportsContentCodeLeague.SportsContentQualifier.Gender = this.TournamentStage.Tournament.Gender == "men" ? "male" : "female";
                }

                var sportsContentCodeCountry = new SportsContentCode {CodeType = "ntbspct:country"};
                if (this.TournamentRoundMetaData != null)
                {
                    
                    if (this.TournamentRoundMetaData.Country.Name == "UEFA" || this.TournamentRoundMetaData.Country.Name == "FIFA")
                    {
                        sportsContentCodeCountry.CodeType = "ntbspct:federation";
                    }
                }

                if (this.TournamentRoundMetaData != null)
                {
                    sportsContentCodeCountry.CodeKey = "country:" + this.TournamentRoundMetaData.Country.Id;
                    sportsContentCodeCountry.CodeName = this.TournamentRoundMetaData.Country.Name;
                }

                var sportsContentCodeRound = new SportsContentCode();
                if (this.TournamentRoundMetaData != null)
                {
                    sportsContentCodeRound.CodeType = "ntbspct:round";
                    sportsContentCodeRound.CodeKey = "round:" + this.TournamentRoundMetaData.Round;
                    sportsContentCodeRound.CodeName = this.TournamentRoundMetaData.Round;
                }

                var sportsContentCodeRoundName = new SportsContentCode();

                // Checking if we can create this element
                if ((this.TournamentStage != null) && this.TournamentStage.GroupName != null)
                {
                    sportsContentCodeRoundName.CodeType = "ntbspct:roundname";
                    sportsContentCodeRoundName.CodeKey = "roundname:" + this.TournamentStage.GroupName.Replace(" ", "");
                    sportsContentCodeRoundName.CodeName = this.TournamentStage.GroupName;
                }

                var sportsContentCodeStage = new SportsContentCode();

                if (this.TournamentDivision != null && this.TournamentDivision.Id > 0)
                {
                    sportsContentCodeStage.CodeType = "ntbspct:stagename";
                    sportsContentCodeStage.CodeKey = "stagekey:" + this.TournamentDivision.Id;
                    sportsContentCodeStage.CodeName = this.TournamentDivision.Name;
                }

                var sportsContentSeriesPairing = new SportsContentCode();
                if (this.SeriesPairing)
                {
                    if (this.TournamentDivision != null)
                    {
                        sportsContentSeriesPairing.CodeType = "ntbspct:series-pairing";
                        sportsContentSeriesPairing.CodeKey = this.TournamentDivision.Key;
                        sportsContentSeriesPairing.CodeName = "2nd Leg";
                    }
                }

                if (!this.SeriesPairing)
                {
                    if (this.TournamentDivision != null)
                    {
                        sportsContentSeriesPairing.CodeType = "ntbspct:omkamp";
                        sportsContentSeriesPairing.CodeKey = this.TournamentDivision.Key;
                        sportsContentSeriesPairing.CodeName = "omkamp";
                    }
                }

                var sportsContentCodeDay = new SportsContentCode();
                if (sportsEvent[0] != null)
                {
                    sportsContentCodeDay.CodeType = "ntbspct:day";
                    sportsContentCodeDay.CodeKey = "day:" + sportsEvent[0].EventMetaData.StartDateTime.DayOfWeek;
                    sportsContentCodeDay.CodeName = sportsEvent[0].EventMetaData.StartDateTime.ToString("dddd");
                }

                var sportsContentCodeSport = new SportsContentCode();
                sportsContentCodeSport.CodeType = "spcode:sport";
                sportsContentCodeSport.CodeKey = "sport:15054000";
                sportsContentCodeSport.CodeName = "Fotball";

                var sportsContentCodePublisher = new SportsContentCode();
                sportsContentCodePublisher.CodeType = "spcode:publisher";
                sportsContentCodePublisher.CodeKey = "publisher:nifs.no";
                sportsContentCodePublisher.CodeName = "Norsk Internasjonal Fotball Statistikk";

                var sportsContentCodeDistributor = new SportsContentCode();
                sportsContentCodeDistributor.CodeType = "spcode:distributor";
                sportsContentCodeDistributor.CodeKey = "publisher:ntb.no";
                sportsContentCodeDistributor.CodeName = "Norsk Telegrambyrå AS";


                var eventType = tournamentDataMapper.GetTournamentEventTypeByTournamentId(localTournament.Id);
                var sportsContentLeagueCode = new SportsContentCode();
                sportsContentLeagueCode.CodeKey = "ntbtournamenttype:" + eventType.Id;
                sportsContentLeagueCode.CodeName = eventType.Name;
                sportsContentLeagueCode.CodeType = "ntbspct:subjectcode";

                var eventCountry = tournamentDataMapper.GetTournamentCountryByTournamentId(localTournament.Id);
                var sportsContentEventCountry = new SportsContentCode();
                
                if (this.TournamentRoundMetaData != null)
                {
                    sportsContentEventCountry.CodeType = "ntbspct:eventcountry";
                    sportsContentEventCountry.CodeKey = "ntbeventcountry:" + this.TournamentRoundMetaData.Country.Id;
                    sportsContentEventCountry.CodeName = this.TournamentRoundMetaData.Country.Name;

                    if (eventCountry.Id != null)
                    {
                        sportsContentEventCountry.CodeKey = "ntbeventcountry:" + eventCountry.Id;
                        sportsContentEventCountry.CodeName = eventCountry.Name;
                    }
                    
                }

                sportsContent.SportsMetaData.SportsContentCodes.SportsContentCode = new SportsContentCode[14];
                sportsContent.SportsMetaData.SportsContentCodes.SportsContentCode[0] = sportsContentCodeDistributor;
                sportsContent.SportsMetaData.SportsContentCodes.SportsContentCode[1] = sportsContentCodePublisher;
                sportsContent.SportsMetaData.SportsContentCodes.SportsContentCode[2] = sportsContentCodeSport;
                sportsContent.SportsMetaData.SportsContentCodes.SportsContentCode[3] = sportsContentCodeLeague;
                if (sportsContentCodeStage.CodeName != null)
                {
                    sportsContent.SportsMetaData.SportsContentCodes.SportsContentCode[4] = sportsContentCodeStage;
                }

                if (sportsContentCodeCountry.CodeName != null)
                {
                    sportsContent.SportsMetaData.SportsContentCodes.SportsContentCode[5] = sportsContentCodeCountry;
                }
                sportsContent.SportsMetaData.SportsContentCodes.SportsContentCode[6] = sportsContentCodeRound;

                if (sportsContentCodeRoundName.CodeName != null)
                {
                    sportsContent.SportsMetaData.SportsContentCodes.SportsContentCode[7] = sportsContentCodeRoundName;
                }

                sportsContent.SportsMetaData.SportsContentCodes.SportsContentCode[8] = sportsContentCodeDay;
                if (sportsContentSeriesPairing.CodeName != null)
                {
                    sportsContent.SportsMetaData.SportsContentCodes.SportsContentCode[9] = sportsContentSeriesPairing;
                }

                sportsContent.SportsMetaData.SportsContentCodes.SportsContentCode[10] = sportsContentCodeTournament;
                sportsContent.SportsMetaData.SportsContentCodes.SportsContentCode[11] = sportsContentLeagueCode;
                if (sportsContentEventCountry.CodeName != null)
                {
                    sportsContent.SportsMetaData.SportsContentCodes.SportsContentCode[12] = sportsContentEventCountry;
                }
                sportsContent.SportsMetaData.SportsContentCodes.SportsContentCode[13] = versionSportsContentCode;
                sportsContent.SportsMetaData.SportsTitle = this.TournamentStage.Tournament.Name;

                sportsContent.SportsEvent = new SportsEvent[sportsEvent.Length];
                sportsContent.SportsEvent = sportsEvent;

                if (standing != null)
                {
                    Logger.Debug("Adding standing to Xml");
                    sportsContent.Standing = standing;
                }

                if (schedule != null)
                {
                    Logger.Debug("Adding schedule to Xml");
                    sportsContent.Schedule = schedule;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }

            return sportsContent;
        }

        public string CreateDocumentName(int version, SportsEvent[] sportsEvent, int id, string docIdTournamentName)
        {
            // Generate document name
            var documentName = string.Format("sml_{0}_{1}_nifs_T{2}_V{3}",
                sportsEvent[0].EventMetaData.StartDateTime.ToString("yyyyMMdd")
                , docIdTournamentName, id, version);
            return documentName;
        }

        public string CreateDocumentBaseName(SportsEvent[] sportsEvent, int id, string docIdTournamentName)
        {
            // Generate document name
            var documentName = string.Format("sml_{0}_{1}_nifs_T{2}",
                sportsEvent[0].EventMetaData.StartDateTime.ToString("yyyyMMdd")
                , docIdTournamentName, id);
            return documentName;
        }

        public string GetTournamentName(SportsEvent[] sportsEvent, string tournamentName)
        {
            if (sportsEvent.Length > 0)
            {
                try
                {
                    if (sportsEvent[0] != null)
                    {
                        if (sportsEvent[0].EventMetaData != null)
                        {
                            if (sportsEvent[0].EventMetaData.EventName != null)
                            {
                                if (sportsEvent[0].EventMetaData.EventName.FullName != null)
                                {
                                    tournamentName = sportsEvent[0].EventMetaData.EventName.FullName;
                                }
                            }
                        }
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                }
            }
            return tournamentName;
        }

        public SportsContent CreateSportsContent(MatchFact matchFact)
        {
            try
            {
                // This part will be done in a new file, I just want to start doing it here
                var sportsEvent = new SportsEvent
                {
                    Id = matchFact.Uid
                };
                var eventMetaData = new EventMetaData
                {
                    EventName = new Name
                    {
                        FullName = matchFact.Name
                    },
                    StartDateTime = matchFact.TimeStamp,
                    EndDateTime = matchFact.TimeStamp.AddMinutes(120),
                    Site = this.GetSite(matchFact)
                };

                sportsEvent.EventMetaData = eventMetaData;

                // var Teams = new List<Team>();
                var teams = new Classes.SportsMl.Team[2];
                var teamPopluator = new TeamPopulator();
                var homeTeam = teamPopluator.PopulateTeam(matchFact, "home");

                var awayTeam = teamPopluator.PopulateTeam(matchFact, "away");

                SetMatchOutcome(homeTeam, awayTeam);

                this.PopulateHomeTeamPlayers(matchFact, homeTeam);

                this.PopulateAwayTeamPlayers(matchFact, awayTeam);

                teams[0] = homeTeam;
                teams[1] = awayTeam;

                sportsEvent.Officials = this.GetOfficials(matchFact);
                sportsEvent.Team = teams;


                var sportsContent = new SportsContent
                {
                    SportsEvent = new SportsEvent[1],
                    SportsMetaData = new SportsMetaData
                    {
                        DocId = string.Format("sjs-{0}_{1}", eventMetaData.StartDateTime.ToString("yyyy-mm-dd"), matchFact.Name.Replace(" ", string.Empty)),
                        StartDateTime = matchFact.TimeStamp
                        // .ToString("yyyy-MM-ddTHH:mm:ss")
                    }
                };

                sportsContent.SportsEvent[0] = sportsEvent;
                sportsContent.SportsEvent[0].EventActions = this.GetMatchActions(matchFact);

                return sportsContent;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                Logger.Error(exception.StackTrace);

                return null;
            }

        }

        /// <summary>
        ///     The get site.
        /// </summary>
        /// <param name="matchFact">
        ///     The match fact.
        /// </param>
        /// <returns>
        ///     The <see cref="Site" />.
        /// </returns>
        private Site GetSite(MatchFact matchFact)
        {
            if (matchFact.Stadium == null)
            {
                return null;
            }
            var site = new Site();
            site.Id = "site_" + matchFact.Stadium.Id;

            site.SiteMetaData = new SiteMetaData();
            // Get more data from the API
            var siteClient = new SiteClient();
            var stadium = siteClient.GetSiteMetaData(Convert.ToInt32(matchFact.Stadium.Id));

            if (stadium != null && (stadium.Place != null))
            {
                Logger.DebugFormat("Content of matchFact.Stadium.Place.Name: {0}", matchFact.Stadium.Place.Name);

                site.SiteMetaData.HomeLocation = new HomeLocation();
                site.SiteMetaData.HomeLocation.Name = new Name();
                site.SiteMetaData.HomeLocation.Name.FullName = matchFact.Stadium.Place.Name;
                site.SiteMetaData.SiteKey = @"site:" + stadium.Uid;
            }

            var siteName = new Name
            {
                Role = "nrol:full",
                FullName = matchFact.Stadium.Name
            };

            var siteAlternateNames = new Name[] { };

            if (stadium != null && stadium.SiteNames != null)
            {
                siteAlternateNames = new Name[stadium.SiteNames.Length];
            }

            if (stadium != null && (stadium.SiteNames != null && stadium.SiteNames.Length > 0))
            {

                var counter = 0;
                foreach (var name in stadium.SiteNames)
                {

                    var siteAlternateName = new Name
                    {
                        Role = "sportnrol:alternate",
                        FullName = name.Name
                    };

                    // todo: This shall be changed to use the new sport name role when that is defined!
                    switch (name.Use.Substring(0, 4).ToLower())
                    {
                        case "uefa":
                            siteAlternateName.Creator = "creator:uefa";
                            break;

                        case "fifa":
                            siteAlternateName.Creator = "creator:fifa";
                            break;

                        // not added as I am not sure if I shall have name of federation or create a more generic qcode
                        //case "spon":
                        //    siteAlternateName.Creator = "creator:sponsor";
                        //    break;

                        default:
                            siteAlternateName.Creator = null;
                            break;
                    }

                    // adding
                    siteAlternateNames[counter] = siteAlternateName;

                    counter++;
                }

            }

            if (siteAlternateNames.Length > 0)
            {
                if (stadium != null && stadium.SiteNames != null)
                {
                    var numberOfSiteNames = stadium.SiteNames.Length + 1;
                
                    site.SiteMetaData.Name = new Name[numberOfSiteNames];
                    site.SiteMetaData.Name[0] = siteName;
                    var counter = 1;
                    foreach (var siteAlternateName in siteAlternateNames)
                    {
                        site.SiteMetaData.Name[counter] = siteAlternateName;
                        counter++;
                    }
                }

            }
            else
            {
                site.SiteMetaData.Name = new Name[1];
                site.SiteMetaData.Name[0] = siteName;
            }


            site.SiteStats = new SiteStats();
            if (matchFact.Attendance != null)
            {
                site.SiteStats.Attendance = Convert.ToInt32(matchFact.Attendance.ToString());
            }

            return site;
        }

        /// <summary>
        ///     The get match actions.
        /// </summary>
        /// <param name="matchFact">
        ///     The match fact.
        /// </param>
        /// <returns>
        ///     The <see cref="EventAction" />.
        /// </returns>
        private EventActions GetMatchActions(MatchFact matchFact)
        {
            var eventActions = new EventActions();
            if (matchFact.MatchEvents == null)
            {
                return null;
            }

            eventActions.EventAction = new EventAction[matchFact.MatchEvents.Count];

            
            // Getting goal scorers
            var counter = 0;
            var hometeamScore = 0;
            var awayteamScore = 0;
            foreach (var matchEvent in matchFact.MatchEvents)
            {
                var eventAction = new EventAction();

                // If this is a penalty score, then we have to document it in a away
                if (matchEvent.MatchEventTypeId == 10)
                {
                    eventAction = new EventAction
                    {
                        MinutesElapsed = matchEvent.Time.ToString(),
                        TeamIdref = @"nifs-t." + matchEvent.MatchTeam.Id,
                        Id = @"a." + matchEvent.Id,
                        SequenceNumber = matchEvent.Sorting.ToString(),
                        Type = @"spsocaction:penalty"
                    };
                }

                // Getting the scorer
                if (matchEvent.MatchEventTypeId == 2)
                {
                    eventAction = new EventAction
                    {
                        MinutesElapsed = matchEvent.Time.ToString(),
                        TeamIdref = @"nifs-t." + matchEvent.MatchTeam.Id,
                        Id = @"a." + matchEvent.Id,
                        SequenceNumber = matchEvent.Sorting.ToString()
                    };
                    if (matchEvent.MatchTeam.Id == matchFact.HomeTeam.Id)
                    {
                        hometeamScore++;
                        eventAction.ScoreTeam = hometeamScore.ToString();
                    }

                    if (matchEvent.MatchTeam.Id == matchFact.AwayTeam.Id)
                    {
                        awayteamScore++;
                        eventAction.ScoreTeamOpposing = awayteamScore.ToString();
                    }

                    if (matchEvent.Score != null)
                    {
                        var scoreItems = matchEvent.Score.Split('-');
                        eventAction.ScoreTeam = scoreItems[0];
                        eventAction.ScoreTeamOpposing = scoreItems[1];
                        eventAction.Type = @"socscoreres:goal";
                    }

                    if (matchEvent.Person != null)
                    {
                        eventAction.Participants = new Participants
                        {
                            Participant = new Participant[1]
                        };

                        var participant = new Participant
                        {
                            IdRef = @"nifs-p." + matchEvent.Person.Id,
                            Role = @"spsocrole:scorer"
                        };
                        eventAction.Participants.Participant[0] = participant;
                        eventAction.Participant = participant;
                    }
                }

                // Own goal
                if (matchEvent.MatchEventTypeId == 8)
                {
                    eventAction = new EventAction
                    {
                        MinutesElapsed = matchEvent.Time.ToString(),
                        TeamIdref = @"nifs-t." + matchEvent.MatchTeam.Id,
                        Id = @"a." + matchEvent.Id,
                        SequenceNumber = matchEvent.Sorting.ToString()
                    };
                    if (matchEvent.MatchTeam.Id == matchFact.HomeTeam.Id)
                    {
                        hometeamScore++;
                        eventAction.ScoreTeam = hometeamScore.ToString();
                    }

                    if (matchEvent.MatchTeam.Id == matchFact.AwayTeam.Id)
                    {
                        awayteamScore++;
                        eventAction.ScoreTeamOpposing = awayteamScore.ToString();
                    }

                    if (matchEvent.Score != null)
                    {
                        var scoreItems = matchEvent.Score.Split('-');
                        eventAction.ScoreTeam = scoreItems[0];
                        eventAction.ScoreTeamOpposing = scoreItems[1];
                        eventAction.Type = @"socscoreres:own-goal";
                    }

                    if (matchEvent.Person != null)
                    {
                        eventAction.Participants = new Participants
                        {
                            Participant = new Participant[1]
                        };

                        var participant = new Participant
                        {
                            IdRef = @"nifs-p." + matchEvent.Person.Id,
                            Role = @"spsocrole:scorer"
                        };
                        eventAction.Participants.Participant[0] = participant;
                        eventAction.Participant = participant;
                    }
                }

                // Created red card
                if (matchEvent.MatchEventTypeId == 3)
                {
                    eventAction = new EventAction
                    {
                        MinutesElapsed = matchEvent.Time.ToString(),
                        TeamIdref = @"nifs-t." + matchEvent.MatchTeam.Id,
                        Id = @"a." + matchEvent.Id,
                        SequenceNumber = matchEvent.Sorting.ToString()
                    };
                    eventAction.Type = @"spsocpenaltylevel:red-card";

                    if (matchEvent.Person != null)
                    {
                        eventAction.Participants = new Participants
                        {
                            Participant = new Participant[1]
                        };

                        var participant = new Participant
                        {
                            IdRef = @"nifs-p." + matchEvent.Person.Id,
                            Role = @"spsocrole:penalty-committed-by"
                        };
                        eventAction.Participants.Participant[0] = participant;
                        eventAction.Participant = participant;
                    }
                }

                // Creating yellow card
                if (matchEvent.MatchEventTypeId == 4)
                {
                    eventAction = new EventAction
                    {
                        MinutesElapsed = matchEvent.Time.ToString(),
                        TeamIdref = @"nifs-t." + matchEvent.MatchTeam.Id,
                        Id = @"a." + matchEvent.Id,
                        SequenceNumber = matchEvent.Sorting.ToString()
                    };
                    eventAction.Type = @"spsocpenaltylevel:yellow-card";

                    if (matchEvent.Person != null)
                    {
                        eventAction.Participants = new Participants
                        {
                            Participant = new Participant[1]
                        };

                        var participant = new Participant
                        {
                            IdRef = @"nifs-p." + matchEvent.Person.Id,
                            Role = @"spsocrole:penalty-committed-by"
                        };
                        eventAction.Participants.Participant[0] = participant;
                        eventAction.Participant = participant;
                    }
                }

                // Creating second yellow card
                if (matchEvent.MatchEventTypeId == 12)
                {
                    eventAction = new EventAction
                    {
                        MinutesElapsed = matchEvent.Time.ToString(),
                        TeamIdref = @"nifs-t." + matchEvent.MatchTeam.Id,
                        Id = @"a." + matchEvent.Id,
                        SequenceNumber = matchEvent.Sorting.ToString(),
                        Type = @"spsocpenaltylevel:yellow-red-card"
                    };

                    if (matchEvent.Person != null)
                    {
                        eventAction.Participants = new Participants
                        {
                            Participant = new Participant[1]
                        };

                        var participant = new Participant
                        {
                            IdRef = @"nifs-p." + matchEvent.Person.Id,
                            Role = @"spsocrole:penalty-committed-by"
                        };
                        eventAction.Participants.Participant[0] = participant;
                        eventAction.Participant = participant;
                    }
                }

                // Creating red card
                if (matchEvent.MatchEventTypeId == 3)
                {
                    eventAction = new EventAction
                    {
                        MinutesElapsed = matchEvent.Time.ToString(),
                        TeamIdref = @"nifs-t." + matchEvent.MatchTeam.Id,
                        Id = @"a." + matchEvent.Id,
                        SequenceNumber = matchEvent.Sorting.ToString(),
                        Type = @"spsocpenaltylevel:red-card"
                    };

                    if (matchEvent.Person != null)
                    {
                        eventAction.Participants = new Participants
                        {
                            Participant = new Participant[1]
                        };

                        var participant = new Participant
                        {
                            IdRef = @"nifs-p." + matchEvent.Person.Id,
                            Role = @"spsocrole:penalty-committed-by"
                        };
                        eventAction.Participants.Participant[0] = participant;
                        eventAction.Participant = participant;
                    }
                }

                // Substitution                
                if (matchEvent.MatchEventTypeId == 23)
                {
                    eventAction = new EventAction
                    {
                        MinutesElapsed = matchEvent.Time.ToString(),
                        TeamIdref = matchEvent.MatchTeam.Id,
                        Type = @"socact:sub-off",
                        Id = @"a." + matchEvent.Id,
                        SequenceNumber = matchEvent.Sorting.ToString(),
                        Participants = new Participants
                        {
                            Participant = new Participant[1]
                        }
                    };

                    if (matchEvent.Person != null)
                    {
                        var participant = new Participant
                        {
                            IdRef = @"nifs-p." + matchEvent.Person.Id,
                            Role = @"socpartrole:sub-off"
                        };

                        eventAction.Participants.Participant[0] = participant;
                        eventAction.Participant = participant;
                    }
                }

                if (matchEvent.MatchEventTypeId == 24)
                {
                    eventAction = new EventAction
                    {
                        MinutesElapsed = matchEvent.Time.ToString(),
                        TeamIdref = matchEvent.MatchTeam.Id,
                        Type = @"socact:sub-on",
                        Id = @"a." + matchEvent.Id,
                        SequenceNumber = matchEvent.Sorting.ToString(),
                        Participants = new Participants
                        {
                            Participant = new Participant[1]
                        }
                    };

                    if (matchEvent.Person != null)
                    {
                        var participant = new Participant
                        {
                            IdRef = @"nifs-p." + matchEvent.Person.Id,
                            Role = @"socpartrole:sub-on"
                        };

                        eventAction.Participants.Participant[0] = participant;
                        eventAction.Participant = participant;
                    }
                }

                // Penalty shootout
                /*
                 * <action id="a.6710995" sequence-number="41" score-team="0" team-idref="nifs-t.3" score-team-opposing="4" minutes-elapsed="62" type="socscoreres:goal">
        <participant idref="nifs-p.19102" role="spsocrole:scorer" />
      </action>
                 */
                if (matchEvent.MatchEventTypeId == 30)
                {
                    eventAction = new EventAction
                    {
                        MinutesElapsed = matchEvent.Time.ToString(),
                        TeamIdref = "nifs-t." + matchEvent.MatchTeam.Id,
                        Type = @"socact:penalty-shootout",
                        Id = @"a." + matchEvent.Id,
                        SequenceNumber = matchEvent.Sorting.ToString(),
                        Participants = new Participants
                        {
                            Participant = new Participant[1]
                        }
                    };

                    if (matchEvent.Score != null)
                    {
                        var scoreItems = matchEvent.Score.Split('-');
                        eventAction.ScoreTeam = scoreItems[0];
                        eventAction.ScoreTeamOpposing = scoreItems[1];
                        eventAction.Type = @"socscoreres:penalty-shootout-goal";
                    }

                    if (matchEvent.Person != null)
                    {
                        var participant = new Participant
                        {
                            IdRef = @"nifs-p." + matchEvent.Person.Id,
                            Role = @"spsocrole:penalty-kicker"
                        };

                        eventAction.Participants.Participant[0] = participant;
                        eventAction.Participant = participant;
                    }
                }

                if (matchEvent.MatchEventTypeId == 31)
                {
                    eventAction = new EventAction
                    {
                        MinutesElapsed = matchEvent.Time.ToString(),
                        TeamIdref = "nifs-t." + matchEvent.MatchTeam.Id,
                        Id = @"a." + matchEvent.Id,
                        SequenceNumber = matchEvent.Sorting.ToString(),
                        Participants = new Participants
                        {
                            Participant = new Participant[1]
                        }
                    };

                    if (matchEvent.Score != null)
                    {
                        var scoreItems = matchEvent.Score.Split('-');
                        eventAction.ScoreTeam = scoreItems[0];
                        eventAction.ScoreTeamOpposing = scoreItems[1];
                        eventAction.Type = @"socscoreres:penalty-shootout-miss";
                    }

                    if (matchEvent.Person != null)
                    {
                        var participant = new Participant
                        {
                            IdRef = @"nifs-p." + matchEvent.Person.Id,
                            Role = @"spsocrole:penalty-kicker"
                        };

                        eventAction.Participants.Participant[0] = participant;
                        eventAction.Participant = participant;
                    }
                }

                eventActions.EventAction[counter] = eventAction;
                counter++;
            }

            return eventActions;
        }

        /// <summary>
        ///     The get tournament table.
        ///     @todo: Move this code to tournament client
        /// </summary>
        /// <param name="stage">
        ///     The stage.
        /// </param>
        /// <returns>
        ///     The <see cref="Classes.TournamentTable" />.
        /// </returns>
        private Classes.Nifs.TournamentTable GetTournamentTable(TournamentStage stage)
        {
            try
            {
                if (stage == null)
                {
                    return null;
                }

                var client = new TournamentClient();
                var content = client.GetTournamentTable(stage);

                var tournamentTable = JsonConvert.DeserializeObject<Classes.Nifs.TournamentTable>(content);

                return tournamentTable;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);

                return null;
            }
        }

        /// <summary>
        ///     The get officials.
        /// </summary>
        /// <param name="matchFact">
        ///     The match fact.
        /// </param>
        /// <returns>
        ///     The <see cref="Officials" />.
        /// </returns>
        private Officials GetOfficials(MatchFact matchFact)
        {
            var officials = new Officials();
            if (matchFact.Referees == null)
            {
                return null;
            }

            var numberOfReferees = 0;

            if (matchFact.Referees != null)
            {
                Logger.DebugFormat("Number of referees: {0}", matchFact.Referees.Length);
                if (matchFact.Referees != null)
                {
                    numberOfReferees = matchFact.Referees.Length;
                }
            }

            officials.Official = new Official[numberOfReferees];

            for (var i = 0; i < numberOfReferees; i++)
            {
                var official = new Official();
                Logger.DebugFormat("Referee id: {0}", matchFact.Referees[i].PersonId);

                // We need to get more information about the referee form the API

                var personClient = new PersonClient();
                var person = personClient.GetPersonMetaData(matchFact.Referees[i].PersonId);

                official.OfficialMetaData = new OfficialMetaData();
                official.OfficialMetaData.OfficialKey = string.Format("official:nifs-o.{0}", matchFact.Referees[i].PersonId);

                // Set the referee position based on referee type id
                switch (matchFact.Referees[i].RefereeTypeId)
                {
                    case 1:
                        official.OfficialMetaData.PositionEvent = "nifsrefereeposition:referee";
                        break;
                    case 2:
                        official.OfficialMetaData.PositionEvent = "nifsrefereerole:assistantreferee";
                        break;
                    case 4:
                        official.OfficialMetaData.PositionEvent = "nifsrefereerole:fourthreferee";
                        break;
                    case 5:
                        official.OfficialMetaData.PositionEvent = "nifsrefereerole:goallineassistantreferee";
                        break;
                    default:
                        official.OfficialMetaData.PositionEvent = "nifsrefereeposition:referee";
                        break;
                }

                official.OfficialMetaData.Name = new Name();
                official.OfficialMetaData.Name.Role = "nrol:full";
                if (person != null && person.FullName != null)
                {
                    official.OfficialMetaData.Name.FullName = person.FullName; // matchFact.Referees[i].Name;

                    if (person.Country != null && person.Country.Name != string.Empty)
                    {
                        official.OfficialMetaData.HomeLocation = new HomeLocation();
                        official.OfficialMetaData.HomeLocation.Name = new Name();
                        official.OfficialMetaData.HomeLocation.Name.Role = "nrol:full";
                        official.OfficialMetaData.HomeLocation.Name.FullName = person.Country.Name;
                    }
                }
                else
                {
                    official.OfficialMetaData.Name.FullName = matchFact.Referees[i].Name;
                }


                if (matchFact.Referees[i].MatchTeam != null)
                {
                    official.Affiliation = new Affiliation();
                    official.Affiliation.MembershipKey = @"clubid:" + matchFact.Referees[i].MatchTeam.Id;
                    official.Affiliation.MembershipType = @"ntbmembershiptype:club";
                    official.Affiliation.MembershipName = matchFact.Referees[i].MatchTeam.Name;
                }

                officials.Official[i] = official;
            }

            return officials;
        }

        /// <summary>
        /// The create sports schedule.
        /// </summary>
        /// <param name="stage">
        /// The stage.
        /// </param>
        /// <param name="selectedDateTime">
        /// The selected Date Time.
        /// </param>
        /// <returns>
        /// The <see cref="Schedule"/>.
        /// </returns>
        public Schedule CreateSportsSchedule(TournamentStage stage, DateTime selectedDateTime)
        {
            // @todo: Move this code to tournament client
            try
            {
                if (stage == null)
                {
                    return null;
                }

                var client = new TournamentClient();
                var tournamentMatches = client.GetSportsSchedule(stage);

                // We are to filter out Friday, Saturday, Sunday and Monday
                var filteredMatches = new List<TournamentMatch>();
                var startDateTime = Convert.ToDateTime(selectedDateTime);
                var endDateTime = Convert.ToDateTime(selectedDateTime);
                if (selectedDateTime.DayOfWeek == DayOfWeek.Friday)
                {
                    endDateTime = selectedDateTime.AddDays(+3);
                    filteredMatches = tournamentMatches.Where(x => x.MatchDateTime >= startDateTime.Date && x.MatchDateTime.Date <= endDateTime.Date).ToList();
                }

                if (selectedDateTime.DayOfWeek == DayOfWeek.Saturday)
                {
                    startDateTime = selectedDateTime.AddDays(-1);
                    endDateTime = selectedDateTime.AddDays(+2);
                    filteredMatches = tournamentMatches.Where(x => x.MatchDateTime.Date >= startDateTime.Date && x.MatchDateTime.Date <= endDateTime.Date).ToList();
                }

                if (selectedDateTime.DayOfWeek == DayOfWeek.Sunday)
                {
                    startDateTime = selectedDateTime.AddDays(-2);
                    endDateTime = selectedDateTime.AddDays(+1);
                    filteredMatches = tournamentMatches.Where(x => x.MatchDateTime.Date >= startDateTime.Date && x.MatchDateTime.Date <= endDateTime.Date).ToList();
                }

                if (selectedDateTime.DayOfWeek == DayOfWeek.Monday)
                {
                    startDateTime = selectedDateTime.AddDays(-3);
                    filteredMatches = tournamentMatches.Where(x => x.MatchDateTime.Date >= startDateTime.Date && x.MatchDateTime.Date <= endDateTime.Date).ToList();
                }

                var schedule = new Schedule();
                schedule.ScheduleMetaData = new ScheduleMetaData();
                schedule.ScheduleMetaData.StartDateTime = startDateTime;
                schedule.ScheduleMetaData.EndDateTime = endDateTime;

                if (filteredMatches.Count == 0)
                {
                    return schedule;
                }

                var sportsEvents = new List<SportsEvent>();

                var teamDataMapper = new TeamDataMapper();

                foreach (var tournamentMatch in filteredMatches)
                {
                    var sportsEvent = new SportsEvent();
                    sportsEvent.Id = @"sched_m_" + tournamentMatch.Id;

                    sportsEvent.EventMetaData = new EventMetaData();
                    sportsEvent.EventMetaData.StartDateTime = tournamentMatch.MatchDateTime;
                    sportsEvent.EventMetaData.EndDateTime = tournamentMatch.MatchDateTime.AddHours(2);

                    if (tournamentMatch.MatchStatusId == 1 || tournamentMatch.MatchStatusId == 16 || tournamentMatch.MatchStatusId == 17)
                    {
                        sportsEvent.EventMetaData.EventStatus = "speventstatus:post-event";
                    }

                    if (tournamentMatch.MatchStatusId == 3)
                    {
                        sportsEvent.EventMetaData.EventStatus = "speventstatus:postponed";
                    }

                    if (tournamentMatch.MatchStatusId == 4)
                    {
                        sportsEvent.EventMetaData.EventStatus = "speventstatus:suspended";
                    }

                    if (tournamentMatch.MatchStatusId == 2)
                    {
                        sportsEvent.EventMetaData.EventStatus = "speventstatus:pre-event";
                    }

                    if (tournamentMatch.MatchStatusId >= 8 && tournamentMatch.MatchStatusId <= 15)
                    {
                        sportsEvent.EventMetaData.EventStatus = "speventstatus:pre-event";
                    }

                    var localHomeTeamname = tournamentMatch.HomeMatchTeam.Name;
                    var localAwayTeamname = tournamentMatch.AwayMatchTeam.Name;
                    var teamName = teamDataMapper.GetNifsTeamById(Convert.ToInt32(tournamentMatch.HomeMatchTeam.Id));
                    if (teamName != null)
                    {
                        if (teamName.TeamName != null)
                        {
                            localHomeTeamname = teamName.TeamName;
                        }
                        else
                        {
                            localHomeTeamname = tournamentMatch.HomeMatchTeam.Name;
                        }
                    }

                    teamName = teamDataMapper.GetNifsTeamById(Convert.ToInt32(tournamentMatch.AwayMatchTeam.Id));
                    if (teamName != null)
                    {
                        if (teamName.TeamName != null)
                        {
                            localAwayTeamname = teamName.TeamName;
                        }
                        else
                        {
                            localAwayTeamname = tournamentMatch.AwayMatchTeam.Name;
                        }
                    }

                    sportsEvent.EventMetaData.EventName = new Name();
                    sportsEvent.EventMetaData.EventName.FullName = string.Format("{0} - {1}", localHomeTeamname, localAwayTeamname);


                    // TODO: We need to check against the database to be sure we are using the correct name
                    sportsEvent.Team = new Classes.SportsMl.Team[2];
                    var homeTeam = new Classes.SportsMl.Team();
                    homeTeam.TeamMetaData = new TeamMetaData();
                    homeTeam.TeamMetaData.Alignment = "home";
                    homeTeam.TeamMetaData.TeamNames = new Name[1];

                    var homeName = new Name();
                    homeName.Role = "nrol:full";
                    homeName.FullName = localHomeTeamname;

                    homeTeam.TeamMetaData.TeamNames[0] = homeName;
                    homeTeam.TeamStats = new TeamStats();
                    if (tournamentMatch.Result.HomeFullTimeScore == null)
                    {
                        tournamentMatch.Result.HomeFullTimeScore = 0;
                    }
                    homeTeam.TeamStats.Score = tournamentMatch.Result.HomeFullTimeScore.ToString();

                    var awayTeam = new Classes.SportsMl.Team();
                    awayTeam.TeamMetaData = new TeamMetaData();
                    awayTeam.TeamMetaData.Alignment = "away";
                    awayTeam.TeamMetaData.TeamNames = new Name[1];

                    var awayTeamName = new Name();
                    awayTeamName.Role = "nrol:full";

                    awayTeamName.FullName = localAwayTeamname;

                    awayTeam.TeamMetaData.TeamNames[0] = awayTeamName;


                    awayTeam.TeamStats = new TeamStats();
                    if (tournamentMatch.Result.AwayFullTimeScore == null)
                    {
                        tournamentMatch.Result.AwayFullTimeScore = 0;
                    }
                    awayTeam.TeamStats.Score = tournamentMatch.Result.AwayFullTimeScore.ToString();

                    sportsEvent.Team[0] = homeTeam;
                    sportsEvent.Team[1] = awayTeam;

                    sportsEvents.Add(sportsEvent);
                }

                schedule.SportsEvent = new SportsEvent[sportsEvents.Count];
                schedule.SportsEvent = sportsEvents.ToArray();

                return schedule;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                return null;
            }
        }

        

        /// <summary>
        ///     The create sports standing.
        /// </summary>
        /// <param name="stage">
        ///     The stage.
        /// </param>
        /// <returns>
        ///     The <see cref="Standing" />.
        /// </returns>
        public Standing CreateSportsStanding(TournamentStage stage)
        {
            // TODO: We need to check against the database to be sure we are using the correct name

            var standing = new Standing();
            try
            {
                Logger.Debug("Creating standing");
                var tournamentTable = this.GetTournamentTable(stage);

                if (tournamentTable == null)
                {
                    return null;
                }

                standing.StandingMetaData = new StandingMetaData();
                standing.StandingMetaData.TeamCoverage = "multi-team";

                var numberOfTeams = tournamentTable.TournamentTableTeams.Length;
                standing.Team = new Classes.SportsMl.Team[numberOfTeams];

                // Looping over stats from nifs
                var counter = 0;

                var teamDataMapper = new TeamDataMapper();
                foreach (var tournamentTableTeam in tournamentTable.TournamentTableTeams)
                {
                    Logger.DebugFormat("Working with team {0}", tournamentTableTeam.Name);
                    var team = new Classes.SportsMl.Team();
                    team.TeamMetaData = new TeamMetaData();
                    team.TeamMetaData.TeamNames = new Name[1];

                    // We are getting the name of the team to be put in the table
                    // TODO: We need to check against the database to be sure we are using the correct name
                    var teamName = teamDataMapper.GetNifsTeamById(Convert.ToInt32(tournamentTableTeam.Id));

                    var name = new Name();
                    name.Role = "nrol:full";
                    name.FullName = teamName.TeamName;
                    if (teamName.TeamName == null)
                    {
                        name.FullName = tournamentTableTeam.Name;
                    }


                    team.TeamMetaData.TeamNames[0] = name;

                    team.TeamStats = new TeamStats();
                    team.TeamStats.OutcomeTotals = new OutcomeTotals[1];
                    var outcomeTotal = new OutcomeTotals();
                    outcomeTotal.AlignmentScope = "events-all";
                    outcomeTotal.EventsPlayed = tournamentTableTeam.Played;
                    outcomeTotal.Wins = tournamentTableTeam.Won;
                    outcomeTotal.Losses = tournamentTableTeam.Lost;
                    outcomeTotal.Ties = tournamentTableTeam.Draw;
                    outcomeTotal.PointsScoredFor = tournamentTableTeam.GoalsScored;
                    outcomeTotal.PointsScoredAgainst = tournamentTableTeam.GoalsConceded;
                    outcomeTotal.StandingPoints = tournamentTableTeam.Points;

                    team.TeamStats.OutcomeTotals[0] = outcomeTotal;

                    team.TeamStats.Rank = new Rank();
                    team.TeamStats.Rank.Value = tournamentTableTeam.Place;

                    standing.Team[counter] = team;
                    counter++;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }
            return standing;
        }

        /// <summary>
        ///     The get populate home team players.
        /// </summary>
        /// <param name="homeTeam">
        ///     The home Team.
        /// </param>
        /// <summary>
        ///     The populate home Team Players.
        /// </summary>
        /// <param name="matchFact">
        ///     The match fact.
        /// </param>
        private void PopulateHomeTeamPlayers(MatchFact matchFact, Classes.SportsMl.Team homeTeam)
        {
            if (matchFact.HomeTeam.Persons.Count == 0)
            {
                homeTeam.Player = null;
            }

            // number of Players from the api
            var remoteTeamPlayers = matchFact.HomeTeam.Persons.Count;
            var homeTeamPlayers = new Player[remoteTeamPlayers];

            var playerDataMapper = new PlayerDataMapper();

            var counter = 0;
            foreach (var person in matchFact.HomeTeam.Persons)
            {
                var player = new Player();
                player.Id = @"nifs-p." + person.PersonId;

                player.PlayerMetaData = new PlayerMetaData();
                player.PlayerMetaData.Key = "player:nifs-p." + person.PersonId;
                player.PlayerMetaData.PlayerNames = new Name[1];


                player.PlayerMetaData.PlayerNames = new Name[1];
                var name = new Name();
                name.Role = "nrol:full";
                var localPlayer = playerDataMapper.GetPlayerMetaDataById(person.PersonId);
                if (localPlayer.PlayerMetaData != null)
                {
                    if (localPlayer.PlayerMetaData.PlayerNames[0].FullName != null)
                    {
                        name.FullName = localPlayer.PlayerMetaData.PlayerNames[0].FullName;
                    }

                    if (localPlayer.PlayerMetaData.PlayerNames[0].FirstName != null)
                    {
                        name.FirstName = localPlayer.PlayerMetaData.PlayerNames[0].FirstName;
                    }

                    if (localPlayer.PlayerMetaData.PlayerNames[0].LastName != null)
                    {
                        name.LastName = localPlayer.PlayerMetaData.PlayerNames[0].LastName;
                    }
                }
                else
                {
                    if (person.FullName != null)
                    {
                        name.FullName = person.FullName;
                    }

                    if (person.FirstName != null)
                    {
                        name.FirstName = person.FirstName;
                    }

                    if (person.LastName != null)
                    {
                        name.LastName = person.LastName;
                    }

                }


                player.PlayerMetaData.PlayerNames[0] = name;

                player.PlayerMetaData.PlayerNames[0].Role = "nrol:full";
                player.PlayerMetaData.UniformNumber = Convert.ToInt32(person.ShirtNumber);
                if (person.Position != null)
                {
                    //player.PlayerMetaData.PositionEvent = @"spsocposition:" + this.GetEventPositionByString(person.Position.Description);
                    player.PlayerMetaData.PositionEvent = @"spsocposition:" + this.GetEventPositionById(person.Position.Id);
                    player.PlayerMetaData.LineupSlot = Convert.ToInt32(person.Position.X);
                }

                if (person.StartsMatch)
                {
                    player.PlayerMetaData.Status = @"spplayerstatus:starter";
                }

                if (person.StartsOnTheBench)
                {
                    player.PlayerMetaData.Status = @"spplayerstatus:bench";
                }

                player.PlayerMetaData.HomeLocation = new HomeLocation();

                if (person.Country != null)
                {
                    player.PlayerMetaData.HomeLocation.Name = new Name
                    {
                        Role = "role:countryname",
                        FullName = person.Country.Name
                    };
                }

                player.PlayerStats = new PlayerStats();
                if (person.StartsMatch)
                {
                    player.PlayerStats.EventTimeEntered = 0;

                    // Gets the time when the player left
                    if (person.MinutesPlayed != null)
                    {
                        player.PlayerStats.EventTimeExited = (int)person.MinutesPlayed;
                    }

                    if (person.StartsMatch && person.EntersFieldMinute == null && person.LeavesFieldMinute == null)
                    {
                        person.EntersFieldMinute = 0;
                        person.LeavesFieldMinute = 90;

                        player.PlayerStats.EventTimeEntered = (int)person.EntersFieldMinute;
                        player.PlayerStats.EventTimeExited = (int)person.LeavesFieldMinute;
                    }

                    if (person.StartsMatch && person.EntersFieldMinute == 0 && person.LeavesFieldMinute == 0)
                    {
                        person.EntersFieldMinute = 0;
                        person.LeavesFieldMinute = 90;

                        player.PlayerStats.EventTimeEntered = (int)person.EntersFieldMinute;
                        player.PlayerStats.EventTimeExited = (int)person.LeavesFieldMinute;
                    }
                }
                else
                {
                    // Gets the time when the player enters
                    if (person.EntersFieldMinute != null)
                    {
                        player.PlayerStats.EventTimeEntered = (int)person.EntersFieldMinute;
                    }

                    // Gets the time when the player left
                    if (person.LeavesFieldMinute != null)
                    {
                        player.PlayerStats.EventTimeExited = (int)person.LeavesFieldMinute;
                    }
                }

                player.PlayerStats.TemporalUnitType = "sptemporalunit:event";

                homeTeamPlayers[counter] = player;

                counter++;
            }

            homeTeam.Player = homeTeamPlayers;
        }

        /// <summary>
        ///     The populate away Team Players.
        /// </summary>
        /// <param name="matchFact">
        ///     The match fact.
        /// </param>
        /// <param name="awayTeam">
        ///     The away Team.
        /// </param>
        private void PopulateAwayTeamPlayers(MatchFact matchFact, Classes.SportsMl.Team awayTeam)
        {
            if (matchFact.AwayTeam.Persons.Count == 0)
            {
                awayTeam.Player = null;
            }

            var playerDataMapper = new PlayerDataMapper();
            var remoteTeamPlayers = matchFact.AwayTeam.Persons.Count;
            var awayTeamPlayers = new Player[remoteTeamPlayers];
            var counter = 0;
            foreach (var person in matchFact.AwayTeam.Persons)
            {
                var player = new Player();
                player.Id = @"nifs-p." + person.PersonId;
                player.PlayerMetaData = new PlayerMetaData();
                player.PlayerMetaData.Key = "player:nifs-p." + person.PersonId;


                player.PlayerMetaData.PlayerNames = new Name[1];
                var name = new Name();
                name.Role = "nrol:full";
                var localPlayer = playerDataMapper.GetPlayerMetaDataById(person.PersonId);
                if (localPlayer.PlayerMetaData != null)
                {
                    name.FullName = localPlayer.PlayerMetaData.PlayerNames[0].FullName;
                    name.FirstName = localPlayer.PlayerMetaData.PlayerNames[0].FirstName;
                    name.LastName = localPlayer.PlayerMetaData.PlayerNames[0].LastName;
                }
                else
                {
                    if (person.FullName != null)
                    {
                        name.FullName = person.FullName;
                    }

                    if (person.FirstName != null)
                    {
                        name.FirstName = person.FirstName;
                    }

                    if (person.LastName != null)
                    {
                        name.LastName = person.LastName;
                    }
                }

                player.PlayerMetaData.PlayerNames[0] = name;

                player.PlayerMetaData.UniformNumber = Convert.ToInt32(person.ShirtNumber);
                if (person.Position != null)
                {
                    // player.PlayerMetaData.PositionEvent = @"spsocposition:" + this.GetEventPositionByString(person.Position.Description);
                    player.PlayerMetaData.PositionEvent = @"spsocposition:" + this.GetEventPositionById(person.Position.Id);
                    player.PlayerMetaData.LineupSlot = Convert.ToInt32(person.Position.X);
                }

                if (person.StartsMatch)
                {
                    player.PlayerMetaData.Status = @"spplayerstatus:starter";
                }

                if (person.StartsOnTheBench)
                {
                    player.PlayerMetaData.Status = @"spplayerstatus:bench";
                }

                player.PlayerMetaData.HomeLocation = new HomeLocation();

                // player.PlayerMetaData.HomeLocation.Country = person.Country.Name;
                if (person.Country != null)
                {
                    player.PlayerMetaData.HomeLocation.Name = new Name
                    {
                        Role = "role:countryname",
                        FullName = person.Country.Name
                    };
                }

                player.PlayerStats = new PlayerStats();
                if (person.StartsMatch)
                {
                    player.PlayerStats.EventTimeEntered = 0;

                    // Gets the time when the player left
                    if (person.MinutesPlayed != null)
                    {
                        player.PlayerStats.EventTimeExited = (int)person.MinutesPlayed;
                    }

                    if (person.StartsMatch && person.EntersFieldMinute == null && person.LeavesFieldMinute == null)
                    {
                        person.EntersFieldMinute = 0;
                        person.LeavesFieldMinute = 90;

                        player.PlayerStats.EventTimeEntered = (int)person.EntersFieldMinute;
                        player.PlayerStats.EventTimeExited = (int)person.LeavesFieldMinute;
                    }

                    if (person.StartsMatch && person.EntersFieldMinute == 0 && person.LeavesFieldMinute == 0)
                    {
                        person.EntersFieldMinute = 0;
                        person.LeavesFieldMinute = 90;

                        player.PlayerStats.EventTimeEntered = (int)person.EntersFieldMinute;
                        player.PlayerStats.EventTimeExited = (int)person.LeavesFieldMinute;
                    }

                    player.PlayerStats.TemporalUnitType = "sptemporalunit:event";
                }
                else
                {
                    // Gets the time when the player enters
                    if (person.EntersFieldMinute != null)
                    {
                        player.PlayerStats.EventTimeEntered = (int)person.EntersFieldMinute;
                    }

                    // Gets the time when the player left
                    if (person.LeavesFieldMinute != null)
                    {
                        player.PlayerStats.EventTimeExited = (int)person.LeavesFieldMinute;
                    }

                    player.PlayerStats.TemporalUnitType = "sptemporalunit:event";
                }

                awayTeamPlayers[counter] = player;
                counter++;
            }

            awayTeam.Player = awayTeamPlayers;
        }

        private int GetLineupSlotById(int id)
        {
            Logger.DebugFormat("Position id: {0}", id);
            switch (id)
            {
                case 1:
                    return 0;
                case 2:
                    return 0;
                case 3:
                    return 4;
                case 4:
                    return 1;
                case 5:
                    return 3;
                case 14:
                case 16:
                case 27:
                case 28:
                    return 0;

                case 6:
                case 7:
                case 8:
                case 9:
                case 15:
                case 17:
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 29:
                case 30:
                case 31:
                case 32:
                case 35:
                case 36:
                    return 1;

                case 10:
                case 13:
                case 33:
                case 34:
                    return 0;

                default:
                    return 0;
            }
        }

        /// <summary>
        ///     The get event position by id.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        private string GetEventPositionById(int id)
        {
            Logger.DebugFormat("Position id: {0}", id);
            switch (id)
            {
                case 1:
                    return "keeper";
                case 2:
                case 3:
                case 4:
                case 5:
                case 14:
                case 16:
                case 27:
                case 28:
                    return "defender";

                case 6:
                case 7:
                case 8:
                case 9:
                case 15:
                case 25:
                case 26:
                case 29:
                case 30:
                case 31:
                case 32:
                case 35:
                case 36:
                    return "midfielder";

                case 17:
                case 18:
                case 19:
                    return "defensive-midfielder";

                case 22:
                case 23:
                case 24:
                    return "attacking-midfielder";
                case 10:
                case 13:
                case 20:
                case 21:
                case 33:
                case 34:
                    return "forward";

                default:
                    return "midfielder";
            }
        }

        /// <summary>
        ///     The set match outcome.
        /// </summary>
        /// <param name="homeTeam">
        ///     The home Team.
        /// </param>
        /// <param name="awayTeam">
        ///     The away Team.
        /// </param>
        private static void SetMatchOutcome(Classes.SportsMl.Team homeTeam, Classes.SportsMl.Team awayTeam)
        {
            if (string.IsNullOrEmpty(homeTeam.TeamStats.Score))
            {
                return;
            }

            if (Convert.ToInt32(homeTeam.TeamStats.Score) > Convert.ToInt32(awayTeam.TeamStats.Score))
            {
                homeTeam.TeamStats.EventOutcome = "outcome:win";
            }
            else if (Convert.ToInt32(homeTeam.TeamStats.Score) < Convert.ToInt32(awayTeam.TeamStats.Score))
            {
                homeTeam.TeamStats.EventOutcome = "outcome:loss";
            }
            else if (Convert.ToInt32(homeTeam.TeamStats.Score) == Convert.ToInt32(awayTeam.TeamStats.Score))
            {
                homeTeam.TeamStats.EventOutcome = "outcome:tie";
            }

            if (Convert.ToInt32(homeTeam.TeamStats.Score) > Convert.ToInt32(awayTeam.TeamStats.Score))
            {
                awayTeam.TeamStats.EventOutcome = "outcome:loss";
            }
            else if (Convert.ToInt32(homeTeam.TeamStats.Score) < Convert.ToInt32(awayTeam.TeamStats.Score))
            {
                awayTeam.TeamStats.EventOutcome = "outcome:win";
            }
            else if (Convert.ToInt32(homeTeam.TeamStats.Score) == Convert.ToInt32(awayTeam.TeamStats.Score))
            {
                homeTeam.TeamStats.EventOutcome = "outcome:tie";
            }
        }

        /// <summary>
        ///     The get event position.
        /// </summary>
        /// <param name="position">
        ///     The position.
        /// </param>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        private string GetEventPositionByString(string position)
        {
            position = position.ToLower();
            Logger.DebugFormat("Position: {0}", position);
            switch (position)
            {
                case "keeper":
                    return "keeper";

                case "defender":
                case "right full back":
                case "left full back":
                case "center half":
                case "full back":
                case "sweeper":
                // Norwegian terms
                case "høyre midtforsvar":
                case "venstreback":
                case "venstre midtforsvar":
                case "høyreback":
                    return "defender";

                case "midfield":
                case "right midfield":
                case "central midfield":
                case "left midfield":
                case "wing":
                case "right defensive midfield":
                case "defensive midfield":
                case "left defensive midfield":
                case "left wing":
                case "right wing":
                case "right offensive midfield":
                case "offensive midfield":
                case "left offensive midfield":
                case "inside right midfield":
                case "inside left midfield":
                case "right center half":
                case "left center half":
                case "right defensive central midfield":
                case "left defensive central midfield":
                case "right offensive central midfield":
                case "left offensive central midfield":
                case "right central midfield":
                case "left central midfield":
                // Norwegian terms
                case "venstre midtbane":
                case "offensiv midtbane":
                case "defensiv midtbane":
                case "høyre midtbane":
                case "høyre sentral midtbane":
                case "høyre offensiv midtbane":
                case "venstre offensiv midtbane":
                case "venstre sentral midtbane":
                case "høyre defensiv indre løper":
                case "venstre defensiv indre løper":
                    return "midfielder";

                case "striker":
                case "forward":
                case "right striker":
                case "left striker":
                // Norwegian terms
                case "spiss":
                case "venstre spiss":
                case "høyre spiss":
                    return "forward";

                default:
                    return position.ToLower()
                        .Replace(" ", string.Empty);
            }
        }

        /// <summary>
        ///     The create sports event.
        /// </summary>
        /// <param name="matchFact">
        ///     The match fact.
        /// </param>
        /// <returns>
        ///     The <see cref="SportsEvent" />.
        /// </returns>
        public SportsEvent CreateSportsEvent(MatchFact matchFact)
        {
            try
            {
                // This part will be done in a new file, I just want to start doing it here
                var sportsEvent = new SportsEvent
                {
                    Id = matchFact.Uid
                };
                var eventMetaData = new EventMetaData
                {
                    EventName = new Name
                    {
                        FullName = matchFact.Name
                    }
                };
                eventMetaData.StartDateTime = matchFact.TimeStamp;
                eventMetaData.EndDateTime = matchFact.TimeStamp.AddMinutes(120);

                if (matchFact.MatchStatusId == 1 || matchFact.MatchStatusId == 16 || matchFact.MatchStatusId == 17)
                {
                    eventMetaData.EventStatus = "speventstatus:post-event";
                }

                if (matchFact.MatchStatusId == 3)
                {
                    eventMetaData.EventStatus = "speventstatus:postponed";
                }

                if (matchFact.MatchStatusId == 4)
                {
                    eventMetaData.EventStatus = "speventstatus:suspended";
                }

                if (matchFact.MatchStatusId == 2)
                {
                    eventMetaData.EventStatus = "speventstatus:pre-event";
                }

                if (matchFact.MatchStatusId >= 8 && matchFact.MatchStatusId <= 15)
                {
                    eventMetaData.EventStatus = "speventstatus:pre-event";
                }

                var site = this.GetSite(matchFact);
                if (site != null)
                {
                    eventMetaData.Site = site;
                }

                sportsEvent.EventMetaData = eventMetaData;

                if (this.TournamentRoundMetaData == null)
                {
                    this.TournamentRoundMetaData = new TournamentRoundMetaData();
                }

                if (matchFact.Stage != null)
                {
                    if (matchFact.Stage.Tournament != null)
                    {
                        if (matchFact.Stage.Tournament.Country != null)
                        {
                            this.TournamentRoundMetaData.Country = matchFact.Stage.Tournament.Country;
                        }
                    }
                }

                if (matchFact.Round != null)
                {
                    this.TournamentRoundMetaData.Round = matchFact.Round;
                }

                this.PopulateTournamentDivision(matchFact);

                // var Teams = new List<Team>();
                var teams = new Classes.SportsMl.Team[2];
                var teamPolulator = new TeamPopulator();
                var homeTeam = teamPolulator.PopulateTeam(matchFact, "home");
                var awayTeam = teamPolulator.PopulateTeam(matchFact, "away");

                SetMatchOutcome(homeTeam, awayTeam);

                this.PopulateHomeTeamPlayers(matchFact, homeTeam);

                this.PopulateAwayTeamPlayers(matchFact, awayTeam);

                teams[0] = homeTeam;
                teams[1] = awayTeam;

                sportsEvent.Officials = this.GetOfficials(matchFact);
                sportsEvent.Team = teams;

                sportsEvent.EventActions = this.GetMatchActions(matchFact);

                return sportsEvent;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                Logger.Error(exception.StackTrace);

                return null;
            }
        }

        /// <summary>
        ///     The populate tournament division.
        /// </summary>
        /// <param name="matchFact">
        ///     The match fact.
        /// </param>
        private void PopulateTournamentDivision(MatchFact matchFact)
        {
            var tournamentDivision = new TournamentDivision();
            tournamentDivision.Id = Convert.ToInt32(matchFact.MatchTypeId);

            if (matchFact.MatchTypeId == null)
            {
                tournamentDivision = null;
                return;
            }

            this.SeriesPairing = false;
            if (matchFact.MatchRelation != null)
            {
                if (matchFact.MatchRelation.AggregatedAwayGoals != null || matchFact.MatchRelation.AggregatedHomeGoals != null)
                {
                    tournamentDivision.Key = "match:" + matchFact.MatchRelation.OtherMatchId;
                    this.SeriesPairing = true;
                }
                else
                {
                    tournamentDivision.Key = "match:" + matchFact.MatchRelation.OtherMatchId;
                }
            }

            if (tournamentDivision.Id == 1)
            {
                tournamentDivision.Name = "Finale";
            }

            if (tournamentDivision.Id == 2)
            {
                tournamentDivision.Name = "Bronsefinale";
            }

            if (tournamentDivision.Id == 3)
            {
                tournamentDivision.Name = "Semifinale";
            }

            if (tournamentDivision.Id == 4)
            {
                tournamentDivision.Name = "Kvartfinale";
            }

            if (tournamentDivision.Id == 5)
            {
                tournamentDivision.Name = "Åttedelsfinale";
            }

            if (tournamentDivision.Id == 16)
            {
                tournamentDivision.Name = "Kvalifisering";
            }

            if (tournamentDivision.Id == 17)
            {
                tournamentDivision.Name = "play-off";
            }

            if (tournamentDivision.Id == 6)
            {
                tournamentDivision.Name = "kamp om 5. plass";
            }

            if (tournamentDivision.Id == 11)
            {
                tournamentDivision.Name = "kamp om 7. plass";
            }

            if (tournamentDivision.Id == 12)
            {
                tournamentDivision.Name = "kamp om 9. plass";
            }

            if (tournamentDivision.Id == 13)
            {
                tournamentDivision.Name = "kamp om 11. plass";
            }

            if (tournamentDivision.Id == 18)
            {
                tournamentDivision.Name = "kamp om 13. plass";
            }

            if (tournamentDivision.Id == 19)
            {
                tournamentDivision.Name = "kamp om 15. plass";
            }

            if (tournamentDivision.Id == 20)
            {
                tournamentDivision.Name = "kamp om 17. plass";
            }

            if (tournamentDivision.Id == 21)
            {
                tournamentDivision.Name = "kamp om 19. plass";
            }

            this.TournamentDivision = tournamentDivision;
        }

        public Dictionary<string, Article> CreateArticle(TournamentStage stage, TournamentRoundMetaData tournamentRoundMetaData, List<OutputFolder> exportPaths, DateTime selectedDateTime)
        {
            try
            {
                var tournamentDataMapper = new TournamentDataMapper();
                var tournament = tournamentDataMapper.GetTournamentByStageId(stage.Id);

                var tournamentFootNote = tournamentDataMapper.GetTournamentFootNoteById(tournament.Id);

                var exportTypesDataMapper = new ExportTypeDataMapper();
                var exportTypes = exportTypesDataMapper.GetExportTypes();

                var articleDataMapper = new ArticleDataMapper(tournament.Id);
                var articleDictionary = new Dictionary<string, Article>();

                var editorialMetaDataHelper = new EditorialMetaDataHelper();

                foreach (var exportPath in exportPaths)
                {
                    var currentExportPath = exportPath;
                    var exportType = exportTypes.SingleOrDefault(x => x.Label == currentExportPath.Product);

                    if (exportType == null)
                    {
                        Logger.InfoFormat("Export type is null, so we continue the loop");
                        continue;
                    }

                    var article = new Article();
                    var nitf = new Nitf();

                    var body = new Body();
                    var content = new Content();
                    content.Paragraph = new Paragraph[2];
                    var bodyhead = new BodyHead();
                    var distributor = new Distributor();
                    var headLine = new Headline();

                    var dateLine = new Dateline();

                    var head = new Head();
                    var meta = new Meta();

                    var culture = new System.Globalization.CultureInfo("nb-NO");
                    var day = culture.DateTimeFormat.GetDayName(selectedDateTime.DayOfWeek);
                    headLine.Title = articleDataMapper.GetTitleByExportId(exportType.Id);
                    headLine.Title = headLine.Title.Replace("{day}", day);

                    if (headLine.Title.ToLower().Contains("{group}"))
                    {
                        // Get the group placeholder
                        var placeholder = editorialMetaDataHelper.GetGroupPlaceHolder(headLine.Title);

                        if (stage.GroupName != null)
                        {
                            var changedTitle = false;
                            if (editorialMetaDataHelper.IsAllUpper(placeholder))
                            {
                                headLine.Title = headLine.Title.Replace("{group}", stage.GroupName.ToUpper());
                                changedTitle = true;
                            }

                            if (!changedTitle)
                            {
                                if (editorialMetaDataHelper.IsFirstCharacterUpper(placeholder))
                                {
                                    var groupName = stage.GroupName;
                                    if (groupName.Contains(" "))
                                    {
                                        var groupParts = stage.GroupName.Split(' ');
                                        groupName = groupParts[0].ToLower();
                                        groupName = editorialMetaDataHelper.UppercaseFirst(groupName);
                                        groupName = groupName + " " + groupParts[1];
                                    }
                                    else
                                    {
                                        groupName = editorialMetaDataHelper.UppercaseFirst(groupName);
                                    }
                                    headLine.Title = headLine.Title.Replace("{group}", groupName);
                                    changedTitle = true;
                                }
                                else
                                {
                                    var groupName = stage.GroupName;
                                    if (groupName.Contains(" "))
                                    {
                                        var groupParts = groupName.Split(' ');

                                        groupName = groupParts[0].ToLower() + " " + groupParts[1];
                                    }
                                    else
                                    {
                                        groupName = groupName.ToLower();
                                    }

                                    headLine.Title = headLine.Title.Replace("{group}", groupName);
                                    changedTitle = true;
                                }
                            }

                        }
                    }

                    // Adding ingress / introline (if the value exists)
                    var leadParagraph = new Paragraph();
                    leadParagraph.Lead = true;
                    leadParagraph.Content = articleDataMapper.GetLedeByExportId(exportType.Id);
                    leadParagraph.Content = leadParagraph.Content.Replace("{day}", day);

                    var footnoteParagraph = new Paragraph();

                    footnoteParagraph.Lead = false;
                    footnoteParagraph.ParagraphClass = "tableFootNote";
                    footnoteParagraph.Content = tournamentFootNote;

                    // Todo: Need to check the stage-type. 
                    // TODO: If this is something else than round - as in final stages, then not to add . round to the string.
                    // TODO: Check your XSLT on how to fix this

                    if (this.TournamentDivision != null || this.TournamentRoundMetaData.Round != string.Empty)
                    {
                        if (this.TournamentDivision == null)
                        {
                            this.TournamentDivision = new TournamentDivision
                            {
                                Name = this.TournamentRoundMetaData.Round + ". runde"
                            };
                        }

                        leadParagraph.Content = leadParagraph.Content.Replace("{round}", this.TournamentDivision.Name);
                    }

                    if (leadParagraph.Content.Contains("{group}"))
                    {
                        var placeholder = editorialMetaDataHelper.GetGroupPlaceHolder(leadParagraph.Content);
                        if (stage.GroupName != null)
                        {
                            var changedParagraph = false;
                            if (editorialMetaDataHelper.IsAllUpper(placeholder))
                            {
                                leadParagraph.Content = leadParagraph.Content.Replace("{group}", stage.GroupName.ToUpper());
                                changedParagraph = true;
                            }

                            if (!changedParagraph)
                            {
                                if (editorialMetaDataHelper.IsFirstCharacterUpper(placeholder))
                                {
                                    var groupName = stage.GroupName;
                                    if (groupName.Contains(" "))
                                    {
                                        var groupParts = groupName.Split(' ');
                                        groupName = groupParts[0].ToLower();
                                        groupName = editorialMetaDataHelper.UppercaseFirst(groupName);
                                        groupName = groupName + " " + groupParts[1];
                                    }
                                    else
                                    {
                                        groupName = editorialMetaDataHelper.UppercaseFirst(groupName);
                                    }

                                    leadParagraph.Content = leadParagraph.Content.Replace("{group}", groupName);
                                    changedParagraph = true;
                                }
                                else
                                {
                                    var groupName = stage.GroupName;
                                    if (groupName.Contains(" "))
                                    {
                                        var groupParts = groupName.Split(' ');

                                        groupName = groupParts[0].ToLower() + " " + groupParts[1];
                                    }
                                    else
                                    {
                                        groupName = groupName.ToLower();
                                    }
                                    leadParagraph.Content = leadParagraph.Content.Replace("{group}", groupName);
                                    changedParagraph = true;
                                }
                            }
                            // paragraph.Content = paragraph.Content.Replace("{group}", stage.GroupName.ToLower());
                        }
                    }

                    // just cleaning up in case
                    headLine.Title = headLine.Title.Replace("{group}", string.Empty);
                    leadParagraph.Content = leadParagraph.Content.Replace("{group}", string.Empty);

                    content.Paragraph[0] = leadParagraph;
                    content.Paragraph[1] = footnoteParagraph;

                    dateLine.Location = articleDataMapper.GetDateLineByTournamentId(tournament.Id); ;

                    distributor.Org = ConfigurationManager.AppSettings["DataOwner"];

                    meta.Name = "slugline";
                    var slugline = articleDataMapper.GetSluglineByExportId(exportType.Id);


                    if (slugline.Contains("{group}"))
                    {
                        var placeholder = editorialMetaDataHelper.GetGroupPlaceHolder(slugline);
                        if (stage.GroupName != null)
                        {
                            var changedSlugline = false;
                            if (editorialMetaDataHelper.IsAllUpper(placeholder))
                            {
                                slugline = slugline.Replace("{group}", stage.GroupName.ToUpper());
                                changedSlugline = true;
                            }

                            if (!changedSlugline)
                            {
                                if (editorialMetaDataHelper.IsFirstCharacterUpper(placeholder))
                                {
                                    var groupName = stage.GroupName;
                                    if (groupName.Contains(" "))
                                    {
                                        var groupParts = groupName.Split(' ');
                                        groupName = groupParts[0].ToLower();
                                        groupName = editorialMetaDataHelper.UppercaseFirst(groupName);
                                        groupName = groupName + " " + groupParts[1];
                                    }
                                    else
                                    {
                                        groupName = editorialMetaDataHelper.UppercaseFirst(groupName);
                                    }
                                    slugline = slugline.Replace("{group}", groupName);
                                    changedSlugline = true;
                                }
                                else
                                {
                                    var groupName = stage.GroupName;
                                    if (groupName.Contains(" "))
                                    {
                                        var groupParts = groupName.Split(' ');
                                        groupName = groupParts[0].ToLower() + " " + groupParts[1];
                                    }
                                    else
                                    {
                                        groupName = groupName.ToLower();
                                    }
                                    slugline = slugline.Replace("{group}", groupName);
                                    changedSlugline = true;
                                }
                            }
                            // paragraph.Content = paragraph.Content.Replace("{group}", stage.GroupName.ToLower());
                        }
                    }

                    meta.Content = slugline;

                    // Head
                    var metas = new Meta[2];
                    metas[0] = meta;

                    var comingMatchesMeta = new Meta();
                    comingMatchesMeta.Name = "presentComingMatches";
                    var comingMatches = tournamentDataMapper.PresentComingMatchesByTournamentId(tournament.Id);
                    if (comingMatches == 0)
                    {
                        comingMatchesMeta.Content = "false";
                    }
                    else
                    {
                        comingMatchesMeta.Content = "true";
                    }
                    metas[1] = comingMatchesMeta;
                    head.Meta = metas;


                    // Creating the head part of the body element
                    bodyhead.Dateline = dateLine;
                    bodyhead.Distributor = distributor;
                    bodyhead.Headline = headLine;

                    body.Head = bodyhead;

                    body.Content = content;
                    nitf.Head = head;
                    nitf.Body = body;
                    article.Nitf = nitf;
                    articleDictionary.Add(exportPath.Product, article);
                }

                return articleDictionary;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);

                return null;
            }
        }

        /// <summary>
        /// Combine the sports content elements
        /// </summary>
        /// <param name="sportsContent"></param>
        /// <param name="exportPaths"></param>
        /// <param name="articles"></param>
        /// <returns></returns>
        public Dictionary<string, SportsContent> CombineSportsContents(SportsContent sportsContent, string[] exportPaths, Dictionary<string, Article> articles)
        {
            var sportsContents = new Dictionary<string, SportsContent>();
            foreach (var exportPath in exportPaths)
            {
                var currentSportContent = new SportsContent();
                currentSportContent = sportsContent;
                var currentExportPath = exportPath;
                if (articles.Keys.Contains(exportPath))
                {
                    Logger.Info("Adding article elements");

                    // ns.Add("nitf", "http://iptc.org/std/NITF/2006-10-18/");

                    var currentArticle = (from a in articles where a.Key == currentExportPath select a.Value).Single();

                    currentSportContent.SportsMetaData.Slug = currentArticle.Nitf.Head.Meta[0].Content;

                    currentSportContent.Article = currentArticle;
                }
                Logger.DebugFormat("ExportPath in combine: {0}", currentExportPath);
                Logger.DebugFormat("Content of slug: {0}", currentSportContent.SportsMetaData.Slug);

                var t = new SportsContent();
                t = currentSportContent;
                sportsContents.Add(currentExportPath, t);
            }

            foreach (KeyValuePair<string, SportsContent> keyValuePair in sportsContents)
            {
                Logger.DebugFormat("Current kvp {0}, value {1}", keyValuePair.Key, keyValuePair.Value.SportsMetaData.Slug);
            }
            return sportsContents;
        }
    }
}
