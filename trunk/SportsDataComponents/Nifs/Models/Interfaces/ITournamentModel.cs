﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsData.Components.Nifs.Classes.Nifs;

namespace NTB.SportsData.Components.Nifs.Models.Interfaces
{
    public interface ITournamentModel
    {
        List<TournamentStage> FilterTournamentStageIdsFromMatches(List<MatchFact> matchesList);
    }
}
