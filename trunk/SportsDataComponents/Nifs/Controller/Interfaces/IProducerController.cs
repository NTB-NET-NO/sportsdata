﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsData.Components.Nifs.Controller.Interfaces
{
    public interface IProducerController
    {
        void Producer(SportsData.Classes.Classes.DataFileParams dataFileParams, double interval);
    }
}
