﻿using System.Collections.Generic;
using NTB.SportsData.Components.Nifs.Classes.Nifs;

namespace NTB.SportsData.Components.Nifs.Controller.Interfaces
{
    public interface IMatchController
    {
        List<MatchFact> GetTodaysMatchesByStageIds(int stageId);
    }
}
