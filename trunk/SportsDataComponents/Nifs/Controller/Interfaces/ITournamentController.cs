﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsData.Components.Nifs.Classes.Nifs;

namespace NTB.SportsData.Components.Nifs.Controller.Interfaces
{
    public interface ITournamentController
    {
        List<TournamentStage> GetTournamentStagesFromLatestChanges(SportsData.Classes.Classes.DataFileParams dataFileParams, double interval);
    }
}
