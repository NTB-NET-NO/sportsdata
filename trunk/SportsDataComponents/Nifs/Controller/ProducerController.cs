﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nifs.Classes.SportsMl;
using NTB.SportsData.Components.Nifs.Clients;
using NTB.SportsData.Components.Nifs.Controller.Interfaces;
using NTB.SportsData.Components.Nifs.Models;
using NTB.SportsData.Components.Nifs.Storages;

namespace NTB.SportsData.Components.Nifs.Controller
{
    public class ProducerController : IProducerController
    {
        /// <summary>
        /// Declaring the logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()
            .DeclaringType);

        public void Producer(DataFileParams dataFileParams, double interval)
        {
            ITournamentController tournamentController = new TournamentController();
            var tournamentStages = tournamentController.GetTournamentStagesFromLatestChanges(dataFileParams, interval);

            if (tournamentStages == null)
            {
                return;
            }
            
            // var exportPaths = dataFileParams.FileOutputFolders.Select(ep => ep.OutputPath).ToArray();
            IMatchController matchController = new MatchController();
            foreach (var tournamentStage in tournamentStages)
            {
                var matches = matchController.GetTodaysMatchesByStageIds(tournamentStage.Id);

                var sportsObject = new SportsContentModel();

                var sportsEvents = new List<SportsEvent>();

                var selectedDateTime = new DateTime();

                if (matches == null)
                {
                    continue;
                }
                // Now we need to match up matches and stage into a sportsml-object
                // And save the object as XML.
                Parallel.ForEach(
                        matches, (currentMatch, state) =>
                        {
                            try
                            {
                                var matchClient = new MatchClient();
                                var matchFact = matchClient.GetMatchFacts(currentMatch.Id);

                                // Check if there was any matches
                                if (matchFact == null)
                                {
                                    return;
                                }

                                if (sportsObject.TournamentStage == null)
                                {
                                    if (matchFact.Stage != null)
                                    {
                                        sportsObject.TournamentStage = matchFact.Stage;
                                    }
                                }

                                selectedDateTime = matchFact.TimeStamp;
                                var sportsEvent = sportsObject.CreateSportsEvent(matchFact);
                                sportsEvents.Add(sportsEvent);
                            }
                            catch (Exception exception)
                            {
                                Logger.Error(exception);
                            }
                        });

                var tournamentStanding = sportsObject.CreateSportsStanding(sportsObject.TournamentStage);

                var sportsSchedule = new Schedule();
                if (selectedDateTime.DayOfWeek == DayOfWeek.Friday || selectedDateTime.DayOfWeek == DayOfWeek.Saturday || selectedDateTime.DayOfWeek == DayOfWeek.Sunday || selectedDateTime.DayOfWeek == DayOfWeek.Monday)
                {
                    sportsSchedule = sportsObject.CreateSportsSchedule(sportsObject.TournamentStage, selectedDateTime);
                }

                var sportsContent = sportsObject.CreateSportsContent(sportsEvents.ToArray(), tournamentStanding, sportsSchedule, selectedDateTime);

                var articleContent = sportsObject.CreateArticle(sportsObject.TournamentStage, sportsObject.TournamentRoundMetaData, dataFileParams.FileOutputFolders, selectedDateTime);

                var fileStorage = new FileStorage();

                fileStorage.SaveXmlFile(sportsContent, dataFileParams.FileOutputFolders, sportsObject.TournamentStage.Id, articleContent);
                var saveJson = false;
                if (saveJson)
                {
                    fileStorage.SaveJsonFile(sportsContent, dataFileParams.FileOutputFolders, sportsObject.TournamentStage.Id, articleContent);
                }
            }
        }
    }
}