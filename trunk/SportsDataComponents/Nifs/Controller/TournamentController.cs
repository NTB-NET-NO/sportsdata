﻿using System;
using System.Collections.Generic;
using NTB.SportsData.Components.Nifs.Classes.Nifs;
using NTB.SportsData.Components.Nifs.Clients;
using NTB.SportsData.Components.Nifs.Clients.Interfaces;
using NTB.SportsData.Components.Nifs.Controller.Interfaces;
using NTB.SportsData.Components.Nifs.Helpers;
using NTB.SportsData.Components.Nifs.Models;
using NTB.SportsData.Components.Nifs.Models.Interfaces;
using log4net;

namespace NTB.SportsData.Components.Nifs.Controller
{
    public class TournamentController : ITournamentController
    {
        /// <summary>
        /// Declaring the logger
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //    /// <summary>
        /////     Static logger
        ///// </summary>
        //private static readonly ILog Logger = LogManager.GetLogger(typeof(NifsJobBuilder));
        /// <summary>
        /// Gets the latest matches based on the date and time in the latest stored date file
        /// </summary>
        /// <param name="dataFileParams">
        /// The data file parameteres 
        /// </param>
        /// <param name="interval">
        /// Det poll times interval in double
        /// </param>
        /// <returns>
        /// List of tournament stages
        /// </returns>
        public List<TournamentStage> GetTournamentStagesFromLatestChanges(SportsData.Classes.Classes.DataFileParams dataFileParams, double interval)
        {
            var helper = new LatestFetchDateTimeHelper();
            var latestFetchDateTime = helper.GetLatestDownloadDateTime(dataFileParams, interval);

            var tournamentClient = new TournamentClient();
            var listOfMatches = tournamentClient.GetUpdatedMatchesByFetchDate(latestFetchDateTime);

            

            if (listOfMatches == null || listOfMatches.Count == 0)
            {
                Log.InfoFormat("No matches updated since last check. Will update store file and return to start");
                helper.StoreLatestDownloadDateTime(dataFileParams, DateTime.Now);
                return null;
            }

            ITournamentModel model = new TournamentModel();
            var listOfTournamentStages = model.FilterTournamentStageIdsFromMatches(listOfMatches);

            helper.StoreLatestDownloadDateTime(dataFileParams, DateTime.Now);
            return listOfTournamentStages;
        }

        /// <summary>
        /// Gets the tournament stages
        /// </summary>
        /// <param name="tournamentStages"></param>
        public void ProcessTournamentStages(List<TournamentStage> tournamentStages)
        {
            var matchController = new MatchController();

            IMatchClient matchClient = new MatchClient();

            // Looping over the result
            foreach (var tournamentStage in tournamentStages)
            {
                var stageMatches = matchClient.GetTodaysMatchesByStageId(tournamentStage.Id);
            }
        }
    }
}
