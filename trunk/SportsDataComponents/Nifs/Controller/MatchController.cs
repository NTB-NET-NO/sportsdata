﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsData.Components.Nifs.Classes.Nifs;
using NTB.SportsData.Components.Nifs.Clients;
using NTB.SportsData.Components.Nifs.Clients.Interfaces;
using NTB.SportsData.Components.Nifs.Controller.Interfaces;

namespace NTB.SportsData.Components.Nifs.Controller
{
    public class MatchController : IMatchController
    {
        public List<MatchFact> GetTodaysMatchesByStageIds(int stageId)
        {
            IMatchClient client = new MatchClient();
            return client.GetTodaysMatchesByStageId(stageId);
        }
    }
}
