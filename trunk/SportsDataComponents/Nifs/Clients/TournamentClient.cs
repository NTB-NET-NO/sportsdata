﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using log4net;
using Newtonsoft.Json;
using NTB.SportsData.Components.Nifs.Classes.Nifs;
using RestSharp;


namespace NTB.SportsData.Components.Nifs.Clients
{
    public class TournamentClient
    {
        /// <summary>
        /// Declaring the logger
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public bool AcceptAllCertifications(object sender, X509Certificate certification, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
        /// <summary>
        /// The get tournaments by country id.
        /// </summary>
        /// <param name="latestFetchTime">
        /// The date and time of latest fetch time.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<MatchFact> GetUpdatedMatchesByFetchDate(DateTime latestFetchTime)
         {
            // The address to the rest api
             var apiUrl = ConfigurationManager.AppSettings["NifsEndPoint"];

            // Creating a correct url
            var urlLatestFetchTime = string.Format("{0}T{1}", latestFetchTime.ToString("yyyy-MM-dd"), latestFetchTime.TimeOfDay);  // ("YYYY-MM-DD+hh:mm:ss"));

            try
            {
                // The URL for the Rest API
                Log.InfoFormat("Fetching data from: {0}", apiUrl);

                // Creating the client
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls;
                var client = new RestClient(apiUrl);

                var request = new RestRequest("matches/", Method.GET);
                request.AddParameter("updatedSince", urlLatestFetchTime);
                request.AddHeader("Accept", "application/json");


                var response = client.Execute(request);
                if (response.ErrorException != null)
                {
                    Log.Error(response.ErrorException.Message);

                    if (response.ErrorException.InnerException != null)
                    {
                        Log.Error(response.ErrorException.InnerException.Message);
                    }

                    return null;
                }

                var content = response.Content;

                if (content == string.Empty)
                {
                    Log.Info("No new updates found.");
                    return null;
                }

                var matchFacts = JsonConvert.DeserializeObject<List<MatchFact>>(content)
                    .ToList();

                return matchFacts;
            }
            catch (Exception exception)
            {
                Log.Error(exception);
            }

            return null;
         }

        public List<TournamentMatch> GetSportsSchedule(TournamentStage stage)
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls;

                var apiUrl = ConfigurationManager.AppSettings["NifsEndPoint"];

                // Creating the client
                var client = new RestClient(apiUrl);
                var request = new RestRequest("stages/{stage}/matches/", Method.GET);
                request.AddParameter("stage", stage.Id, ParameterType.UrlSegment);
                request.AddHeader("Accept", "application/json");

                var response = client.Execute(request);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    return new List<TournamentMatch>();
                }

                var content = response.Content;

                var tournamentMatches = JsonConvert.DeserializeObject<List<TournamentMatch>>(content);
                return tournamentMatches;
            }
            catch (Exception exception)
            {
                Log.Error(exception);
                return new List<TournamentMatch>();
            }
        }

        public string GetTournamentTable(TournamentStage stage)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls;
            // stages/{$stagesId}/table/
            var apiUrl = ConfigurationManager.AppSettings["NifsEndPoint"];
            
            // Creating the client
            var client = new RestClient(apiUrl);
            var request = new RestRequest("stages/{stage}/table/", Method.GET);
            request.AddParameter("stage", stage.Id, ParameterType.UrlSegment);
            request.AddHeader("Accept", "application/json");

            var response = client.Execute(request);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                return string.Empty;
            }

            var content = response.Content;
            return content;
        }
    }
}
