﻿using System.Collections.Generic;
using NTB.SportsData.Components.Nifs.Classes.Nifs;

namespace NTB.SportsData.Components.Nifs.Clients.Interfaces
{
    public interface IMatchClient
    {
        List<MatchFact> GetTodaysMatchesByStageId(int id);
    }
}
