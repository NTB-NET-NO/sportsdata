﻿using NTB.SportsData.Components.Nifs.Classes.Nifs;

namespace NTB.SportsData.Components.Nifs.Clients
{
    using System.Configuration;
    using System.Net;

    using Newtonsoft.Json;

    using RestSharp;

    public class PersonClient
    {
        public Person GetPersonMetaData(int personId)
        {
            // formatting date so it can be used
            var apiUrl = ConfigurationManager.AppSettings["NifsEndPoint"];

            // apiUrl += string.Format("persons/{0}", personId);

            // Creating the client
            var client = new RestClient(apiUrl);
            var request = new RestRequest("persons/{personid}", Method.GET);
            request.AddParameter("personid", personId, ParameterType.UrlSegment);
            request.AddHeader("Accept", "application/json");

            var response = client.Execute(request);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                return null;
            }

            var content = response.Content;

            var settings = new JsonSerializerSettings();
            settings.DateFormatString = "YYYY-MM-DD";
            var person = JsonConvert.DeserializeObject<Person>(content);

            return person;
        }
    }
}
