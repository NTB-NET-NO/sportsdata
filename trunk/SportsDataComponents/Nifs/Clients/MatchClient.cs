﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using log4net;
using Newtonsoft.Json;
using NTB.SportsData.Components.Nifs.Classes.Nifs;
using NTB.SportsData.Components.Nifs.Clients.Interfaces;
using RestSharp;

namespace NTB.SportsData.Components.Nifs.Clients
{
    public class MatchClient : IMatchClient
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<MatchFact> GetTodaysMatchesByStageId(int id)
        {
            try
            {
                // The URL for the Rest API
                var matchDate = DateTime.Today.ToString("yyyy-MM-dd");
                // The address to the rest api
                var apiUrl = ConfigurationManager.AppSettings["NifsEndPoint"];

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls;

                // Creating the client
                // apiUrl += string.Format("stages/{stages}/matches/?date={1}", id, matchDate);
                var client = new RestClient(apiUrl);

                var request = new RestRequest("stages/{stage}/matches/", Method.GET);
                request.AddParameter("stage", id, ParameterType.UrlSegment);
                request.AddParameter("date", matchDate);
                request.AddHeader("Accept", "application/json");

                //var request = new RestRequest("health/{entity}/status");
                //request.AddParameter("entity", "s2", ParameterType.UrlSegment);

                var response = client.Execute(request);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    return null;
                }

                var content = response.Content;

                return JsonConvert.DeserializeObject<List<MatchFact>>(content);
            }
            catch (Exception exception)
            {
                Logger.Error(exception); 

                return new List<MatchFact>();
            }
        }

        public MatchFact GetMatchFacts(string currentMatchId)
        {
            try
            {
                var apiUrl = ConfigurationManager.AppSettings["NifsEndPoint"];

                // apiUrl += string.Format("matches/{0}?summary=true", currentMatchId);
                Logger.DebugFormat("Requesting from NIFS: {0}", apiUrl);


                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls;

                // Creating the client
                // apiUrl += string.Format("stages/{stages}/matches/?date={1}", id, matchDate);
                var client = new RestClient(apiUrl);

                var request = new RestRequest("matches/{matchId}/", Method.GET);
                request.AddParameter("matchId", currentMatchId, ParameterType.UrlSegment);
                request.AddParameter("summary", true);
                request.AddHeader("Accept", "application/json");

                // Creating the client

                var response = client.Execute(request);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    return null;
                }

                var content = response.Content;

                var matchFact = JsonConvert.DeserializeObject<MatchFact>(content);

                return matchFact;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);

                return new MatchFact();
            }
        }
    }
}
