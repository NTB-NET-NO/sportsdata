﻿using System;
using NTB.SportsData.Components.Nifs.Classes.Nifs;

namespace NTB.SportsData.Components.Nifs.Clients
{
    using System.Configuration;
    using System.Net;

    using Newtonsoft.Json;

    using RestSharp;
    using log4net;

    public class SiteClient
    {

        /// <summary>
        ///     The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(SiteClient));

        /// <summary>
        ///     The get site meta data.
        /// </summary>
        /// <param name="siteId">
        ///     The site id.
        /// </param>
        /// <returns>
        ///     The <see cref="Stadium" />.
        /// </returns>
        public Stadium GetSiteMetaData(int siteId)
        {
            // formatting date so it can be used
            var apiUrl = ConfigurationManager.AppSettings["NifsEndPoint"];

            // apiUrl += string.Format("stadiums/{0}", siteId);
            Logger.DebugFormat("Calling API: {0}", apiUrl);

            // Creating the client
            var client = new RestClient(apiUrl);
            var request = new RestRequest("stadiums/{id}", Method.GET);
            request.AddParameter("id", siteId, ParameterType.UrlSegment);
            request.AddHeader("Accept", "application/json");


            var response = client.Execute(request);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                return null;
            }

            var content = response.Content;
            Logger.Debug(content);
            try
            {
                var format = "yyyy";
                var jsonSerializerSettings = new JsonSerializerSettings();
                jsonSerializerSettings.MissingMemberHandling = MissingMemberHandling.Ignore;
                jsonSerializerSettings.DateFormatString = format;


                var stadium = JsonConvert.DeserializeObject<Stadium>(content, jsonSerializerSettings);
                return stadium;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                return new Stadium();
            }
        }
    }
}
