﻿using System.Configuration;
using System.Net;

using Newtonsoft.Json;
using NTB.SportsData.Components.Nifs.Classes.Nifs;
using RestSharp;


namespace NTB.SportsData.Components.Nifs.Clients
{
    public class TeamClient
    {
        /// <summary>
        ///     The get team meta data.
        /// </summary>
        /// <param name="teamId">
        ///     The team id.
        /// </param>
        /// <returns>
        ///     The <see cref="MatchTeam" />.
        /// </returns>
        public MatchTeam GetTeamMetaData(int teamId)
        {
            // formatting date so it can be used
            var apiUrl = ConfigurationManager.AppSettings["NifsEndPoint"];

            // Creating the client
            var client = new RestClient(apiUrl);
            
            var request = new RestRequest("teams/{teamid}", Method.GET);
            request.AddParameter("teamid", teamId, ParameterType.UrlSegment);
            request.AddHeader("Accept", "application/json");


            var response = client.Execute(request);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                return null;
            }

            var content = response.Content;

            var team = JsonConvert.DeserializeObject<MatchTeam>(content);

            return team;
        }
    }
}
