﻿using System;
using System.Collections.Generic;
using System.Xml;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Classes.Enums;
using Quartz;
using Quartz.Impl;

namespace NTB.SportsData.Components.Nifs.Abstracts
{
    /// <summary>
    /// The a schedule type factory.
    /// </summary>
    public abstract class AScheduleTypeFactory
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static ILog logger;

        /// <summary>
        ///     schedulerFactory to be used to create scheduled tasks
        /// </summary>
        private readonly StdSchedulerFactory schedulerFactory = new StdSchedulerFactory();

        /// <summary>
        /// Gets the logger.
        /// </summary>
        public static ILog Logger
        {
            get
            {
                if (logger != null)
                {
                    return logger;
                }

                var type = System.Reflection.MethodBase.GetCurrentMethod()
                    .DeclaringType;
                logger = LogManager.GetLogger(type);
                return logger;
            }
        }

        /// <summary>
        /// Gets the scheduler factory.
        /// </summary>
        public StdSchedulerFactory SchedulerFactory
        {
            get
            {
                if (this.schedulerFactory != null)
                {
                    return this.schedulerFactory;
                }

                return new StdSchedulerFactory();
            }
        }

        /// <summary>
        /// Gets or sets the trigger.
        /// </summary>
        public ITrigger Trigger { get; set; }

        /// <summary>
        /// Gets or sets the job detail.
        /// </summary>
        public IJobDetail JobDetail { get; set; }

        /// <summary>
        ///     Gets or sets the scheduler.
        /// </summary>
        public IScheduler Scheduler { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether schedule type.
        /// </summary>
        public ScheduleType ScheduleType { get; set; }

        /// <summary>
        /// Gets or sets the instance name.
        /// </summary>
        public string InstanceName { get; set; }

        /// <summary>
        /// Gets or sets the operation mode.
        /// </summary>
        public OperationMode OperationMode { get; set; }

        /// <summary>
        /// Gets or sets the file output folder.
        /// </summary>
        public List<OutputFolder> FileOutputFolder { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether populate database.
        /// </summary>
        public bool PopulateDatabase { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether purge.
        /// </summary>
        public bool Purge { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether create xml.
        /// </summary>
        public bool CreateXml { get; set; }

        /// <summary>
        /// Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        /// Gets or sets the federation id.
        /// </summary>
        public int FederationId { get; set; }

        /// <summary>
        /// Gets or sets the discipline id.
        /// </summary>
        public int DisciplineId { get; set; }

        /// <summary>
        /// Gets or sets the date time offset.
        /// </summary>
        public DateTime DateTimeOffset { get; set; }

        /// <summary>
        /// The get schedule day.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetScheduleDay(XmlNode node)
        {
            var scheduleDay = string.Empty;
            if (node.Attributes != null && node.Attributes["DayOfWeek"] != null)
            {
                scheduleDay = node.Attributes["DayOfWeek"].Value;
            }

            return scheduleDay;
        }

        /// <summary>
        /// The get schedule duration.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="duration">
        /// The duration.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int GetScheduleDuration(XmlNode node, int duration)
        {
            // Checking if there is a setting for Duration
            if (node.Attributes != null && node.Attributes["Duration"] != null)
            {
                duration = Convert.ToInt32(node.Attributes["Duration"].Value);
            }

            return duration;
        }

        /// <summary>
        /// The get schedule hours.
        /// </summary>
        /// <param name="scheduleTimeArray">
        /// The schedule time array.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int GetScheduleHours(string[] scheduleTimeArray)
        {
            // Doing the same thing for hours
            if (scheduleTimeArray[0] == "00")
            {
                scheduleTimeArray[0] = "0";
            }

            var hours = Convert.ToInt32(scheduleTimeArray[0]);
            return hours;
        }

        /// <summary>
        /// The get schedule id.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="SportsDataException">
        /// Throws an exception if something goes wrong
        /// </exception>
        public string GetScheduleId(XmlNode node)
        {
            if (node.Attributes == null)
            {
                throw new SportsDataException("Cannot find Schedule Id!");
            }

            var scheduleId = node.Attributes["Id"].Value;
            return scheduleId;
        }

        /// <summary>
        /// The get schedule minutes.
        /// </summary>
        /// <param name="scheduleTimeArray">
        /// The schedule time array.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int GetScheduleMinutes(string[] scheduleTimeArray)
        {
            // If minutes contains two zeros (00), we change it to one zero (0) 
            // If not, scheduler won't understand
            if (scheduleTimeArray[1] == "00")
            {
                scheduleTimeArray[1] = "0";
            }

            var minutes = Convert.ToInt32(scheduleTimeArray[1]);
            return minutes;
        }

        /// <summary>
        /// The get schedule results.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="results">
        /// The results.
        /// </param>
        /// <param name="scheduleMessage">
        /// The schedule message.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool GetScheduleResults(XmlNode node, bool results, ref bool scheduleMessage)
        {
            // Checking if there is a setting for results
            if (node.Attributes != null && node.Attributes["Results"] != null)
            {
                results = Convert.ToBoolean(node.Attributes["Results"].Value);
                scheduleMessage = !results;
            }

            return results;
        }

        /// <summary>
        /// The get schedule week.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetScheduleWeek(XmlNode node)
        {
            var scheduleWeek = string.Empty;
            if (node.Attributes != null && node.Attributes["Week"] != null)
            {
                scheduleWeek = node.Attributes["Week"].Value;
            }

            return scheduleWeek;
        }

        /// <summary>
        /// The get day of week.
        /// </summary>
        /// <param name="scheduleDay">
        /// The schedule day.
        /// </param>
        /// <param name="hours">
        /// The hours.
        /// </param>
        /// <param name="minutes">
        /// The minutes.
        /// </param>
        /// <returns>
        /// The <see cref="DateTimeOffset"/>.
        /// </returns>
        public DateTimeOffset GetDayOfWeek(string scheduleDay, int hours, int minutes)
        {
            // If we use the DateTimeOffset it means that we have to find out if the day has passed or not.
            // So we have to find out which day it is today
            var today = DateTime.Today.DayOfWeek;
            var numDayOfWeek = 0;
            switch (today.ToString())
            {
                case "Monday":
                    numDayOfWeek = 1;
                    break;
                case "Tuesday":
                    numDayOfWeek = 2;
                    break;

                case "Wednesday":
                    numDayOfWeek = 3;
                    break;

                case "Thursday":
                    numDayOfWeek = 4;
                    break;

                case "Friday":
                    numDayOfWeek = 5;
                    break;

                case "Saturday":
                    numDayOfWeek = 6;
                    break;

                case "Sunday":
                    numDayOfWeek = 0;
                    break;
            }

            // Checking if numDayOfWeek is smaller than ScheduleDay
            DateTimeOffset dto = DateTime.Today;

            // Get the scheduled day
            var scheduledDay = Convert.ToInt32(scheduleDay);

            int days;
            if (numDayOfWeek <= scheduledDay)
            {
                // It is, so we need to find out when the next scheduling should happen
                days = scheduledDay - numDayOfWeek; // this can be zero

                dto = DateTime.Today.AddDays(days)
                    .AddHours(hours)
                    .AddMinutes(minutes);

                // <Schedule Id="1" Time="15:12" DayOfWeek="1" Week="2"/>
            }
            else if (numDayOfWeek > scheduledDay)
            {
                const int weekdays = 7;
                var daysLeft = weekdays - numDayOfWeek;
                days = daysLeft + scheduledDay;

                dto = DateTime.Today.AddDays(days)
                    .AddHours(hours)
                    .AddMinutes(minutes)
                    .ToUniversalTime();
            }

            return dto;
        }

        /// <summary>
        /// The setup schedule type.
        /// </summary>
        /// <param name="node">
        /// The node list.
        /// </param>
        /// <param name="results">
        /// The results.
        /// </param>
        /// <param name="scheduleMessage">
        /// The schedule message.
        /// </param>
        /// <param name="duration">
        /// The duration.
        /// </param>
        /// <param name="scheduler">
        /// The scheduler.
        /// </param>
        /// <returns>
        /// The <see cref="DateTimeOffset"/>.
        /// </returns>
        public abstract DateTimeOffset SetupScheduleType(XmlNode node, bool results, bool scheduleMessage, int duration, IScheduler scheduler);

        /// <summary>
        /// The populate job detail.
        /// </summary>
        /// <param name="results">
        /// The results.
        /// </param>
        /// <param name="scheduleMessage">
        /// The schedule message.
        /// </param>
        /// <param name="duration">
        /// The duration.
        /// </param>
        /// <param name="jobDetail">
        /// The job detail.
        /// </param>
        public abstract void PopulateJobDetail(bool results, bool scheduleMessage, int duration, IJobDetail jobDetail);

        /// <summary>
        /// The set job detail.
        /// </summary>
        /// <param name="scheduleId">
        /// The schedule id.
        /// </param>
        /// <returns>
        /// The <see cref="IJobDetail"/>.
        /// </returns>
        /// <exception cref="ArgumentOutOfRangeException">
        /// throws an exception on error
        /// </exception>
        public abstract IJobDetail SetJobDetail(string scheduleId);
    }
}