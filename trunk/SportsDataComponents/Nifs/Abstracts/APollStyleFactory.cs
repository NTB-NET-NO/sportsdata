﻿using System.Collections.Generic;
using System.Xml;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Classes.Enums;
using Quartz;
using Quartz.Impl;

namespace NTB.SportsData.Components.Nifs.Abstracts
{
    /// <summary>
    /// The PollStyleFactory interface.
    /// </summary>
    public abstract class APollStyleFactory
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static ILog logger;

        /// <summary>
        ///     schedulerFactory to be used to create scheduled tasks
        /// </summary>
        private readonly StdSchedulerFactory _schedulerFactory = new StdSchedulerFactory();

        /// <summary>
        /// Gets the logger.
        /// </summary>
        public static ILog Logger
        {
            get
            {
                if (logger != null)
                {
                    return logger;
                }

                var type = System.Reflection.MethodBase.GetCurrentMethod()
                    .DeclaringType;
                logger = LogManager.GetLogger(type);
                return logger;
            }
        }

        /// <summary>
        /// Gets the scheduler factory.
        /// </summary>
        public StdSchedulerFactory SchedulerFactory
        {
            get
            {
                if (this._schedulerFactory != null)
                {
                    return this._schedulerFactory;
                }

                return new StdSchedulerFactory();
            }
        }

        /// <summary>
        ///     Gets or sets the scheduler.
        /// </summary>
        public IScheduler Scheduler { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether schedule type.
        /// </summary>
        public ScheduleType ScheduleType { get; set; }

        /// <summary>
        /// Gets or sets the instance name.
        /// </summary>
        public string InstanceName { get; set; }

        /// <summary>
        /// Gets or sets the operation mode.
        /// </summary>
        public OperationMode OperationMode { get; set; }

        /// <summary>
        /// Gets or sets the file output folder.
        /// </summary>
        public List<OutputFolder> FileOutputFolder { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether populate database.
        /// </summary>
        public bool PopulateDatabase { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether purge.
        /// </summary>
        public bool Purge { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether create xml.
        /// </summary>
        public bool CreateXml { get; set; }

        /// <summary>
        /// Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        /// Gets or sets the federation id.
        /// </summary>
        public int FederationId { get; set; }

        /// <summary>
        /// Gets or sets the discipline id.
        /// </summary>
        public int DisciplineId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether push results.
        /// </summary>
        public bool PushResults { get; set; }

        /// <summary>
        /// Gets or sets the interval.
        /// </summary>
        public int Interval { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether push schedule.
        /// </summary>
        public bool PushSchedule { get; set; }

        /// <summary>
        /// Gets or sets the data file params.
        /// </summary>
        public DataFileParams DataFileParams { get; set; }

        public int FederationKey { get; set; }

        /// <summary>
        /// The setup poll style.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        public abstract void ConfigurePollStyle(XmlNode configNode);

        /// <summary>
        /// The start poll style.
        /// </summary>
        public abstract void StartPollStyle();

        /// <summary>
        /// The stop poll style.
        /// </summary>
        public abstract void StopPollStyle();
    }
}