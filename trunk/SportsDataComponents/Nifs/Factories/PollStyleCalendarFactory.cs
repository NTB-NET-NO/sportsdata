﻿using System;
using System.Collections.Generic;
using System.Xml;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Classes.Enums;
using NTB.SportsData.Components.Nifs.Abstracts;
using NTB.SportsData.Components.Nifs.DataFileCreators;
using Quartz;
using Quartz.Impl.Matchers;

namespace NTB.SportsData.Components.Nifs.Factories
{
    /// <summary>
    /// The poll style calendar factory.
    /// </summary>
    public class PollStyleCalendarFactory : APollStyleFactory
    {
        /// <summary>
        /// The setup poll style.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Throws an exception if something goes wrong
        /// </exception>
        /// <exception cref="Exception">
        /// Throws an exception if something goes wrong
        /// </exception>
        public override void ConfigurePollStyle(XmlNode configNode)
        {
            try
            {
                Logger.Info("Setting up schedulers");
                Logger.Info("--------------------------------------------------------------");

                // Getting the number of jobs to create
                var xPathExpression = string.Format("../NIFTeamComponent[@Name='{0}']/Schedules/Schedule", this.InstanceName);
                var nodeList = configNode.SelectNodes(xPathExpression);

                var jobs = 0;
                if (nodeList != null)
                {
                    jobs = nodeList.Count;
                }

                Logger.Debug("Number of jobs: " + jobs);

                // Creating a boolean variable to store if we are to get results out or not
                var results = false;

                // Creating an integer value to be used to tell the duration of the gathering of data
                var duration = 0;

                // Creating a boolean variable to store if this is a schedule-message og not
                var scheduleMessage = false;

                Logger.Debug("Schedule of type: " + this.ScheduleType);

                // Getting the scheduler
                this.Scheduler = this.SchedulerFactory.GetScheduler();

                AScheduleTypeFactory scheduleTypeFactory = null;

                switch (this.ScheduleType)
                {
                    case ScheduleType.Daily:

                        // nodeList, dtOffset, results, scheduleMessage, duration, scheduler);
                        scheduleTypeFactory = new DailyScheduleTypeFactory
                        {
                            CreateXml = this.CreateXml,
                            InstanceName = this.InstanceName,
                            SportId = this.SportId,
                            DisciplineId = this.DisciplineId,
                            FederationId = this.FederationId,
                            FederationKey = this.FederationKey,
                            FileOutputFolder = this.FileOutputFolder,
                            OperationMode = this.OperationMode,
                            PopulateDatabase = this.PopulateDatabase,
                            Purge = this.Purge,
                            ScheduleType = this.ScheduleType
                        };
                        break;

                    case ScheduleType.Weekly:
                        // scheduleTypeConfigure.SetupScheduleTypeWeekly(nodeList, results, scheduleMessage, duration, niFweeklyTrigger, nifWeeklyJobDetails, scheduler);
                        scheduleTypeFactory = new WeeklyScheduleTypeFactory
                        {
                            CreateXml = this.CreateXml,
                            InstanceName = this.InstanceName,
                            SportId = this.SportId,
                            DisciplineId = this.DisciplineId,
                            FederationId = this.FederationId,
                            FederationKey = this.FederationKey,
                            FileOutputFolder = this.FileOutputFolder,
                            OperationMode = this.OperationMode,
                            PopulateDatabase = this.PopulateDatabase,
                            Purge = this.Purge,
                            ScheduleType = this.ScheduleType
                        };

                        break;

                    case ScheduleType.Continous:
                        break;
                    case ScheduleType.Monthly:
                        break;
                    case ScheduleType.Yearly:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                // Setup up Schedule type
                if (scheduleTypeFactory != null)
                {
                    foreach (XmlNode node in nodeList)
                    {
                        scheduleTypeFactory.SetupScheduleType(node, results, scheduleMessage, duration, this.Scheduler);
                    }
                }

                // We might move the code regarding jobdetails here... 

                // Configuring the datafile-object so that we know that the databases are up and running

                // We don't have to check for datafile stuff when we only do purge...
                if (this.OperationMode == OperationMode.Purge)
                {
                    return;
                }

                this.DataFileParams = new DataFileParams
                {
                    FederationId = this.FederationId,
                    CreateXml = this.CreateXml,
                    SportId = this.SportId,
                    DisciplineId = this.DisciplineId
                };

                var datafile = new TournamentDataFileCreator
                {
                    TeamDataFileParams = this.DataFileParams
                };

                if (datafile.Configure() == false)
                {
                    // throw new SportsDataException("Problems initiating database tables");
                }

                // Catching exceptions that might occur
            }
            catch (SchedulerConfigException sce)
            {
                Logger.Debug("A SchedulerConfigException has occured: ", sce);
            }
            catch (SchedulerException se)
            {
                Logger.Debug("A schedulerException has occured: ", se);
            }
            catch (Exception e)
            {
                Logger.Debug("An exception has occured", e);
            }
        }

        public override void StartPollStyle()
        {
            try
            {
                Logger.Info("Setting up jobListener and scheduler");

                // starting the scheduler
                var nifScheduler = this.SchedulerFactory.GetScheduler();

                // Creating the job listener
                // new NIFSportsDataJobListener();
                var jobGroupNames = nifScheduler.GetJobGroupNames();

                foreach (var jobGroupName in jobGroupNames)
                {
                    Logger.DebugFormat("JobGroupName: {0}", jobGroupName);

                    if (jobGroupName != "GroupNIF")
                    {
                        continue;
                    }

                    Logger.DebugFormat("Key: {0}", nifScheduler.GetJobKeys(GroupMatcher<JobKey>.GroupContains(jobGroupName)));

                    var groupMatcher = GroupMatcher<JobKey>.GroupContains(jobGroupName);
                    var jobKeys = nifScheduler.GetJobKeys(groupMatcher);

                    foreach (var jobKey in jobKeys)
                    {
                        Logger.DebugFormat("Name: {0}", jobKey.Name);
                        Logger.DebugFormat("Group: {0}", jobKey.Group);
                    }
                }

                var triggerGroupNames = nifScheduler.GetTriggerGroupNames();

                foreach (var triggerGroupName in triggerGroupNames)
                {
                    Logger.Debug("TriggerGroupName: " + triggerGroupName);
                }

                var listOfJobs = nifScheduler.GetJobGroupNames();
                foreach (var job in listOfJobs)
                {
                    Logger.Debug("Job: " + job);
                }

                Logger.Debug("This Component name: " + this.InstanceName);

                // Starting scheduler
                nifScheduler.Start();
            }
            catch (SchedulerException exception)
            {
                Logger.Debug(exception);
                throw new SportsDataException("An error has occured when starting Scheduler.", exception);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                throw new SportsDataException("An error has occured when starting Scheduler.", exception);
            }
        }

        /// <summary>
        /// The stop pol l style.
        /// </summary>
        public override void StopPollStyle()
        {
            try
            {
                Logger.Debug("Deleting / stopping jobs!");

                // ISchedulerFactory sf = new StdSchedulerFactory();

                var sched = this.SchedulerFactory.GetScheduler();

                // Shutting down scheduler
                var jobGroupNames = sched.GetJobGroupNames();
                foreach (var jobGroupName in jobGroupNames)
                {
                    Logger.InfoFormat("Closing with group {0}", jobGroupName);
                    var keys = new List<JobKey>(sched.GetJobKeys(GroupMatcher<JobKey>.GroupContains(jobGroupName)));


                    // Looping through each key as we'd like to know when things go right and not!
                    keys.ForEach(
                        key =>
                        {
                            var detail = sched.GetJobDetail(key);
                            sched.DeleteJob(key);

                            Logger.InfoFormat("Shutting down: {0} ({1}) Group: {2}",
                                detail.Description, key.Name, key.Group);
                        });

                    sched.Shutdown();
                }
            }
            catch (SchedulerException schedulerException)
            {
                Logger.Fatal(schedulerException);
            }
            catch (Exception exception)
            {
                Logger.ErrorFormat("Problems closing component {0}!", this.InstanceName);
                Logger.Error(exception);
            }
        }
    }
}