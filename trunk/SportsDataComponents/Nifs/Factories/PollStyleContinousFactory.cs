﻿using System;
using System.Xml;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nifs.Abstracts;

namespace NTB.SportsData.Components.Nifs.Factories
{
    /// <summary>
    ///     The poll style continous factory.
    /// </summary>
    public class PollStyleContinousFactory : APollStyleFactory
    {
        /// <summary>
        /// The start poll style.
        /// </summary>
        public override void StartPollStyle()
        {
            // Not sure if we have to do anything
            Logger.Info("Starting Continous PollStyle component");
        }

        /// <summary>
        /// The stop pol l style.
        /// </summary>
        public override void StopPollStyle()
        {
            // Not sure if we have to do anything
            Logger.Info("Stopping Continous PollStyle component");
        }

        /// <summary>
        /// The setup poll style.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        public override void ConfigurePollStyle(XmlNode configNode)
        {
            if (configNode.Attributes != null)
            {
                this.Interval = Convert.ToInt32(configNode.Attributes["Interval"].Value);
            }

            var folderNodeList = configNode.SelectNodes("../NIFTeamComponent[@Name='" + this.InstanceName + "']/Folders/Folder");

            var folderJobs = 0;
            if (folderNodeList != null)
            {
                folderJobs = folderNodeList.Count;
            }

            // <Folder Type="XMLOutputFolder" Mode="FILE" Results="True" PushSchedule="False">C:\Utvikling\norm\xml\NTBSportsData</Folder>
            // string OutputFolder = "";
            Logger.Debug("Number of Folders: " + folderJobs);
            if (folderNodeList != null)
            {
                foreach (XmlNode foldernode in folderNodeList)
                {
                    if (foldernode.Attributes != null && foldernode.Attributes["Result"] != null)
                    {
                        this.PushResults = Convert.ToBoolean(foldernode.Attributes["Result"].Value);
                    }

                    if (foldernode.Attributes != null && foldernode.Attributes["PushSchedule"] != null)
                    {
                        this.PushSchedule = Convert.ToBoolean(foldernode.Attributes["PushSchedule"].Value);
                    }

                    var product = string.Empty;
                    if (foldernode.Attributes != null && foldernode.Attributes["Product"] != null)
                    {
                        product = foldernode.Attributes["Product"].Value;
                    }

                    var outputFolder = new OutputFolder();
                    outputFolder.Result = this.PushResults;
                    outputFolder.Product = product;
                    outputFolder.OutputPath = foldernode.InnerText;

                    this.FileOutputFolder.Add(outputFolder);
                }
            }

            if (this.Interval == 0)
            {
                this.Interval = 5000;
            }

            this.Interval = this.Interval*60*1000; // Adding so that it is one minute

            // this.pollTimer.Interval = this.interval; // short startup - interval * 1000;
            Logger.DebugFormat("Poll interval: {0} seconds", this.Interval);
        }
    }
}