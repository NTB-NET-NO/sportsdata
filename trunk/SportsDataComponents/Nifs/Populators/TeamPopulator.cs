﻿using System;
using System.Linq;
using log4net;
using NTB.SportsData.Components.Nifs.Classes.Nifs;
using NTB.SportsData.Components.Nifs.Classes.SportsMl;
using NTB.SportsData.Components.Nifs.Clients;
using NTB.SportsData.Components.Nifs.Data.DataMappers.Teams;

namespace NTB.SportsData.Components.Nifs.Populators
{
    public class TeamPopulator
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(TeamPopulator));

        /// <summary>
        ///     The populate home Team.
        /// </summary>
        /// <param name="matchFact">
        ///     The match fact.
        /// </param>
        /// <param name="alignment">
        ///     The alignment property
        /// </param>
        /// <returns>
        ///     The <see cref="Team" />.
        /// </returns>
        public Team PopulateTeam(MatchFact matchFact, string alignment)
        {
            var team = new Team();
            var teamMetaData = new TeamMetaData[1];
            var matchTeamMetaData = new TeamMetaData();
            var teamDataMapper = new TeamDataMapper();
            
            matchTeamMetaData.Alignment = alignment;

            var teamClient = new TeamClient();
            if (alignment == "home")
            {
                team.TeamId = "nifs-t." + matchFact.HomeTeam.Id;
                matchTeamMetaData.Key = "Team:nifs-t." + matchFact.HomeTeam.Id;
                var matchTeam = teamClient.GetTeamMetaData(Convert.ToInt32(matchFact.HomeTeam.Id));
                if (matchTeam != null && matchTeam.Country != null)
                {
                    matchTeamMetaData.HomeLocation = new HomeLocation();
                    matchTeamMetaData.HomeLocation.Name = new Name();
                    matchTeamMetaData.HomeLocation.Name.FullName = matchTeam.Country.Name;
                    matchTeamMetaData.HomeLocation.Name.Role = "nrol:full";
                }
            }
            else
            {
                team.TeamId = "nifs-t." + matchFact.AwayTeam.Id;
                matchTeamMetaData.Key = "Team:nifs-t." + matchFact.AwayTeam.Id;
                var matchTeam = teamClient.GetTeamMetaData(Convert.ToInt32(matchFact.AwayTeam.Id));
                if (matchTeam != null && matchTeam.Country != null)
                {
                    matchTeamMetaData.HomeLocation = new HomeLocation();
                    matchTeamMetaData.HomeLocation.Name = new Name();
                    matchTeamMetaData.HomeLocation.Name.FullName = matchTeam.Country.Name;
                    matchTeamMetaData.HomeLocation.Name.Role = "nrol:full";
                }
            }
            var teamNames = new Name[1];

            var teamId = 0;
            if (alignment == "home")
            {
                teamId = Convert.ToInt32(matchFact.HomeTeam.Id);
            }
            else if (alignment == "away")
            {
                teamId = Convert.ToInt32(matchFact.AwayTeam.Id);
            }

            var matchTeamName = teamDataMapper.GetNifsTeamById(teamId);

            if (matchTeamName == null || matchTeamName.TeamName == null)
            {
                if (alignment == "home")
                {
                    matchTeamName = new Classes.Team
                                        {
                                            TeamName = matchFact.HomeTeam.Name
                                        };
                    
                }
                else
                {
                    matchTeamName = new Classes.Team
                                        {
                                            TeamName = matchFact.AwayTeam.Name
                                        };
                }
            }

            var teamName = new Name
                               {
                                   Role = "nrol:full",
                                   FullName = matchTeamName.TeamName 
                               };
            teamNames[0] = teamName;

            matchTeamMetaData.TeamNames = new Name[1];
            matchTeamMetaData.TeamNames[0] = teamName;

            teamMetaData[0] = matchTeamMetaData;

            var stats = new TeamStats();

            // stats.SubScores = new SubScores();
            stats.SubScore = new SubScore[6];
            if (alignment == "home")
            {
                if (matchFact.Result.HomeScore45 == null)
                {
                    matchFact.Result.HomeScore45 = 0;
                }

                var halfTimeScore = new SubScore
                {
                    PeriodValue = "1",
                    SubScoreName = "pause",
                    SubScoreType = "subtype:halftime",
                    Score = matchFact.Result.HomeScore45.ToString()
                };

                if (matchFact.Result.HomeScore90 == null)
                {
                    matchFact.Result.HomeScore90 = 0;
                }

                stats.SubScore[0] = halfTimeScore;

                var fullTimeScore = new SubScore
                {
                    PeriodValue = "2",
                    SubScoreName = "fulltid",
                    SubScoreType = "subtype:fulltime",
                    Score = matchFact.Result.HomeScore90.ToString()
                };

                stats.SubScore[1] = fullTimeScore;

                if (matchFact.Result.HomeScore105 != null || matchFact.Result.HomeScore120 != null)
                {
                    if (matchFact.Result.HomeScore105 == null)
                    {
                        matchFact.Result.HomeScore105 = 0;
                    }

                    var extraTime1 = new SubScore
                    {
                        PeriodValue = "3",
                        SubScoreName = "pause ekstraomgang",
                        SubScoreType = "subtype:halfextratime",
                        Score = matchFact.Result.HomeScore105.ToString()
                    };

                    stats.SubScore[2] = extraTime1;
                }

                if (matchFact.Result.HomeScore120 != null)
                {
                    var extraTime2 = new SubScore
                    {
                        PeriodValue = "4",
                        SubScoreName = "fulltid ekstraomgang",
                        SubScoreType = "subtype:fullextratime",
                        Score = matchFact.Result.HomeScore120.ToString()
                    };

                    stats.SubScore[3] = extraTime2;
                }

                if (matchFact.Result.HomeScorePenalties != null)
                {
                    var penaltyShootout = new SubScore
                    {
                        PeriodValue = "5",
                        SubScoreName = "straffekonkurranse",
                        SubScoreType = "subtype:penalty-shootout",
                        Score = matchFact.Result.HomeScorePenalties.ToString()
                    };

                    stats.SubScore[4] = penaltyShootout;
                }

                if (matchFact.MatchRelation != null)
                {
                    if (matchFact.MatchRelation.AggregatedHomeGoals != null)
                    {
                        var aggregatedMatches = new SubScore
                        {
                            PeriodValue = "6",
                            SubScoreName = "sammenlagt",
                            SubScoreType = "subtype:aggregate-goals-home",
                            Score = matchFact.MatchRelation.AggregatedHomeGoals.ToString()
                        };
                        stats.SubScore[5] = aggregatedMatches;
                    }
                }
            }

            if (alignment == "away")
            {
                if (matchFact.Result.AwayScore45 == null)
                {
                    matchFact.Result.AwayScore45 = 0;
                }
                var halfTimeScore = new SubScore
                {
                    PeriodValue = "1",
                    SubScoreName = "pause",
                    SubScoreType = "subtype:halftime",
                    Score = matchFact.Result.AwayScore45.ToString()
                };

                stats.SubScore[0] = halfTimeScore;

                if (matchFact.Result.AwayScore90 == null)
                {
                    matchFact.Result.AwayScore90 = 0;
                }

                var fullTimeScore = new SubScore
                {
                    PeriodValue = "2",
                    SubScoreName = "fulltid",
                    SubScoreType = "subtype:fulltime",
                    Score = matchFact.Result.AwayScore90.ToString()
                };

                stats.SubScore[1] = fullTimeScore;

                if (matchFact.Result.AwayScore105 != null || matchFact.Result.AwayScore120 != null)
                {
                    if (matchFact.Result.AwayScore105 == null)
                    {
                        matchFact.Result.AwayScore105 = 0;
                    }

                    var extraTime1 = new SubScore
                    {
                        PeriodValue = "3",
                        SubScoreName = "pause ekstraomgang",
                        SubScoreType = "subtype:halfextratime",
                        Score = matchFact.Result.AwayScore105.ToString()
                    };

                    stats.SubScore[2] = extraTime1;
                }

                if (matchFact.Result.HomeScore120 != null)
                {
                    var extraTime2 = new SubScore
                    {
                        PeriodValue = "4",
                        SubScoreName = "fulltid ekstraomgang",
                        SubScoreType = "subtype:fullextratime",
                        Score = matchFact.Result.AwayScore120.ToString()
                    };

                    stats.SubScore[3] = extraTime2;
                }

                if (matchFact.Result.HomeScorePenalties != null)
                {
                    var penaltyShootout = new SubScore
                    {
                        PeriodValue = "5",
                        SubScoreName = "straffekonkurranse",
                        SubScoreType = "subtype:penalty-shootout",
                        Score = matchFact.Result.AwayScorePenalties.ToString()
                    };

                    stats.SubScore[4] = penaltyShootout;
                }

                if (matchFact.MatchRelation != null)
                {
                    if (matchFact.MatchRelation.AggregatedHomeGoals != null)
                    {
                        var aggregatedMatches = new SubScore
                        {
                            PeriodValue = "6",
                            SubScoreName = "sammenlagt",
                            SubScoreType = "subtype:aggregate-goals-away",
                            Score = matchFact.MatchRelation.AggregatedAwayGoals.ToString()
                        };
                        stats.SubScore[5] = aggregatedMatches;
                    }
                }
            }
            

            Logger.DebugFormat("We have {0} items in the array", stats.SubScore.Length);

            stats.SubScore = stats.SubScore.Where(x => x != null)
                .ToArray();

            Logger.DebugFormat("We have {0} items in the array", stats.SubScore.Length);

            stats.Score = stats.SubScore.Last()
                .Score;

            foreach (var subScore in stats.SubScore)
            {
                Logger.DebugFormat("Output of score {0} - {1}", subScore.Score, subScore.SubScoreName);
            }
            team.TeamStats = stats;

            team.TeamMetaData = teamMetaData[0];
            return team;
        }
    }
}