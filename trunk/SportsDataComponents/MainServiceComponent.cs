﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Timers;
using System.Xml;
using log4net;
using NTB.SportsData.Classes.Enums;
using NTB.SportsData.Components.Components;
using NTB.SportsData.Components.Nif.Components;
using NTB.SportsData.Components.Nifs.Components;
using NTB.SportsData.Components.Profixio;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;
using NTB.SportsData.Utilities;

namespace NTB.SportsData.Components
{
    /// <summary>
    /// The main service component.
    /// </summary>
    public partial class MainServiceComponent : Component
    {
        /// <summary>
        /// The _logger.
        /// </summary>
        private static readonly ILog Logger;

        private static bool _startUp = false;

        /// <summary>
        /// Initializes static members of the <see cref="MainServiceComponent"/> class.
        /// </summary>
        static MainServiceComponent()
        {
            // Set up _logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository()
                .Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }

            Logger = LogManager.GetLogger(typeof(MainServiceComponent));

            if (Thread.CurrentThread.Name == null)
            {
                Thread.CurrentThread.Name = "MainServiceComponent";
            }

            _startUp = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainServiceComponent"/> class. 
        /// Initializes a new instance of the class
        /// </summary>
        public MainServiceComponent()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainServiceComponent"/> class.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        public MainServiceComponent(IContainer container)
        {
            container.Add(this);

            this.InitializeComponent();
        }

        /// <summary>
        /// The maintenance timer_ elapsed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void maintenanceTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            MDC.Set("JOBNAME", "MainWorkerJob");

            Logger.Debug("In maintenanceTimer_Elapsed");

            /*
             * Not sure what this shall do yet, 
             * but I would believe that getting some data would be an idea...
             */
            try
            {
                Logger.Debug("Stopping maintenanceTimer");
                this.maintenanceTimer.Stop();

                // We are checking if the jobs are doing what they are supposed to do
                foreach (var keyValuePair in this.JobInstances.Where(kvp => kvp.Value.Enabled))
                {
                    var state = keyValuePair.Value.GetComponentState(_startUp);
                    

                    Logger.InfoFormat("State of component {0}: {1}", keyValuePair.Value.InstanceName, state);

                    if (state != ComponentState.Running)
                    {
                        // Setting the startup variable to true
                        _startUp = true;

                        // Mailing restarting
                        Mail.SendMail(string.Format("The component {0} is not running, but is enabled. We will try and restart", keyValuePair.Value.InstanceName));
                        Logger.InfoFormat("The component {0} is not running, but is enabled", keyValuePair.Value.InstanceName);

                        Logger.DebugFormat("We are stopping the component {0}!", keyValuePair.Value.InstanceName);
                        keyValuePair.Value.Stop();

                        Logger.Debug("We are waiting 5 seconds!");
                        Thread.Sleep(5000);

                        // Reconfiguring the component
                        Logger.DebugFormat("We are re-configuring the component {0}!", keyValuePair.Value.InstanceName);

                        // Starting the component
                        Logger.DebugFormat("We are starting the component {0}!", keyValuePair.Value.InstanceName);
                        keyValuePair.Value.Start();

                        Mail.SendMail(string.Format("The component {0} has been restarted.", keyValuePair.Value.InstanceName));
                    }
                }

                // Watch the config file
                if (!this.configFileWatcher.EnableRaisingEvents)
                {
                    this.configFileWatcher.EnableRaisingEvents = this.WatchConfigSet;
                }

                Logger.Debug("Setting Startup to false");
                _startUp = false;

                Logger.Debug("Starting Maintenance-timer");
                this.maintenanceTimer.Start();
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("MainServiceComponent::maintenanceTimer_Elapsed() failed to renew PushSubscription: " + ex.Message, ex);
                Mail.SendException(ex);
            }
        }

        /// <summary>
        /// Reconfigures everything from an updated config set
        /// </summary>
        /// <param name="sender">
        /// The source of the event.
        /// </param>
        /// <param name="e">
        /// File change params
        /// </param>
        /// <remarks>
        /// <para>
        /// When the configuration is updated on disk, this function triggers a complete job reload.
        /// </para>
        /// <para>
        /// Note that app.config is also reloaded, but refreshed config settings only apply to <c>AppSettings&gt;</c> here, not the other sections of the config file.
        /// </para>
        /// </remarks>
        private void configFileWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            ThreadContext.Properties["JOBNAME"] = "MainServiceWorker";

            // Outer-try-final to prevent dupe events
            try
            {
                // Stopping file watcher
                this.configFileWatcher.EnableRaisingEvents = false;
                Logger.Info("MainServiceComponent::configFileWatcher_Changed() hit. Will reconfigure.");

                ThreadContext.Stacks["NDC"].Pop();
                ThreadContext.Stacks["NDC"].Push("RECONFIGURE");

                // Pause everything
                this.Pause();

                // Give it a little break
                Thread.Sleep(5000);

                // Reconfigure
                ConfigurationManager.RefreshSection("applicationSettings");
                ConfigurationManager.RefreshSection("appSection");
                this.Configure();

                this.Start();
            }
            catch (Exception ex)
            {
                Logger.Fatal("NTBSportsData reconfiguration failed - TERMINATING!", ex);
                Mail.SendException(ex);
                throw;
            }
            finally
            {
                ThreadContext.Stacks["NDC"].Pop();
                this.configFileWatcher.EnableRaisingEvents = this.WatchConfigSet;
            }
        }

        #region Instance variables and control data

        /// <summary>
        /// Name of the file that contains the job configuration set
        /// </summary>
        /// <remarks>The name of the job config XML file is stored here.</remarks>
        protected string ConfigSet = string.Empty;

        /// <summary>
        /// Enable watching the config set for changes and reconfigure at runtime
        /// </summary>
        /// <remarks>config setting that defines if we are wathcing the config set for changes/auto reconfigure or not.</remarks>
        protected bool WatchConfigSet;

        /// <summary>
        /// List of currently Configured jobs
        /// </summary>
        /// <remarks>Internal Dictionary to keep track of running worker jobs.</remarks>
        protected Dictionary<string, IBaseSportsDataInterfaces> JobInstances = new Dictionary<string, IBaseSportsDataInterfaces>();

        /// <summary>
        /// Notification handler service host
        /// </summary>
        /// <remarks>
        /// This si the actuall service host that controls the notification object <see>
        ///                                                                            <cref>ewsNotify</cref>
        ///                                                                        </see>
        /// </remarks>
        protected ServiceHost ServiceHost;

        #endregion

        #region Control functions

        /// <summary>
        /// The configure.
        /// </summary>
        public void Configure()
        {
            try
            {
                ThreadContext.Stacks["NDC"].Pop();
                // NDC.Push("CONFIG");
                ThreadContext.Stacks["NDC"].Push("CONFIG");

                // Checking if a directory exists or not
                var di = new DirectoryInfo(ConfigurationManager.AppSettings["OutPath"]);

                if (!di.Exists)
                {
                    di.Create();
                }

                // Read params 
                this.ConfigSet = ConfigurationManager.AppSettings["ConfigurationSet"];
                this.WatchConfigSet = Convert.ToBoolean(ConfigurationManager.AppSettings["WatchConfiguration"]);

                // Logging
                Logger.InfoFormat("{0} : {1}", "ConfigurationSet", this.ConfigSet);
                Logger.InfoFormat("{0} : {1}", "WatchConfiguration", this.WatchConfigSet);

                // Load configuration set
                var config = new XmlDocument();
                config.Load(this.ConfigSet);

                // Setting up the watcher
                this.configFileWatcher.Path = Path.GetDirectoryName(this.ConfigSet);
                this.configFileWatcher.Filter = Path.GetFileName(this.ConfigSet);

                // Clearing all jobInstances before we populate them again. 
                this.JobInstances.Clear();

                // Creating NFF Components
                var nodes = config.SelectNodes("/ComponentConfiguration/NFFComponents/NFFComponent[@Enabled='True' or @Enabled='true']");
                if (nodes != null)
                {
                    Logger.InfoFormat("NFFComponent job instances found: {0}", nodes.Count);

                    var checkDatabase = false;
                    foreach (XmlNode nd in nodes)
                    {
                        IBaseSportsDataInterfaces nffComponent = new NffGathererComponent();
                        if (checkDatabase)
                        {
                            nffComponent.DatabasePopulated = true;
                        }
                        else
                        {
                            checkDatabase = nffComponent.DatabasePopulated;
                        }

                        nffComponent.Configure(nd);

                        Logger.DebugFormat("Adding {0}", nffComponent.InstanceName);
                        this.JobInstances.Add(nffComponent.InstanceName, nffComponent);
                    }
                }

                // ... then create NIF Team components
                nodes = config.SelectNodes("/ComponentConfiguration/NIFTeamComponents/NIFTeamComponent[@Enabled='True' or @Enabled='true']");
                if (nodes != null)
                {
                    Logger.InfoFormat("NIFComponent job instances found: {0}", nodes.Count);

                    var checkDatabase = false;
                    foreach (XmlNode nd in nodes)
                    {
                        IBaseSportsDataInterfaces teamComponent = new NifTeamGathererComponent();
                        if (checkDatabase)
                        {
                            teamComponent.DatabasePopulated = true;
                        }
                        else
                        {
                            checkDatabase = teamComponent.DatabasePopulated;
                        }

                        teamComponent.Configure(nd);

                        Logger.DebugFormat("Adding {0}", teamComponent.InstanceName);
                        this.JobInstances.Add(teamComponent.InstanceName, teamComponent);
                    }
                }

                // ... then create NIF Single Sport components
                nodes = config.SelectNodes("/ComponentConfiguration/NIFSingleSportComponents/NIFSingleSportComponent[@Enabled='True' or @Enabled='true']");
                if (nodes != null)
                {
                    Logger.InfoFormat("NIFSingleSportComponents job instances found: {0}", nodes.Count);

                    var checkDatabase = false;
                    foreach (XmlNode nd in nodes)
                    {
                        IBaseSportsDataInterfaces singleSportComponent = new NifSingleSportGathererComponent();
                        if (checkDatabase)
                        {
                            singleSportComponent.DatabasePopulated = true;
                        }
                        else
                        {
                            checkDatabase = singleSportComponent.DatabasePopulated;
                        }

                        singleSportComponent.Configure(nd);

                        Logger.DebugFormat("Adding {0}", singleSportComponent.InstanceName);
                        this.JobInstances.Add(singleSportComponent.InstanceName, singleSportComponent);
                    }
                }

                // ... then create NIF Single Sport components
                nodes = config.SelectNodes("/ComponentConfiguration/NifsComponents/NifsComponent");
                if (nodes != null)
                {
                    Logger.InfoFormat("NifsComponent job instances found: {0}", nodes.Count);

                    var checkDatabase = false;
                    foreach (XmlNode nd in nodes)
                    {
                        IBaseSportsDataInterfaces nifsGathererComponent = new NifsGathererComponent();
                        if (checkDatabase)
                        {
                            nifsGathererComponent.DatabasePopulated = true;
                        }
                        else
                        {
                            checkDatabase = nifsGathererComponent.DatabasePopulated;
                        }

                        nifsGathererComponent.Configure(nd);

                        Logger.DebugFormat("Adding {0}", nifsGathererComponent.InstanceName);
                        this.JobInstances.Add(nifsGathererComponent.InstanceName, nifsGathererComponent);
                    }
                }

                // ... then the Profixio (Norway Cup and maybe some other competitions in the future) Components
                nodes = config.SelectNodes("/ComponentConfiguration/ProfixioComponents/ProfixioComponent[@Enabled='True' or @Enabled='true']");
                if (nodes != null)
                {
                    Logger.InfoFormat("Profixio Component job instances found: {0}", nodes.Count);

                    var checkDatabase = false;
                    foreach (XmlNode nd in nodes)
                    {
                        IBaseSportsDataInterfaces profixioComponent = new ProfixioGathererComponent();
                        if (checkDatabase)
                        {
                            profixioComponent.DatabasePopulated = true;
                        }
                        else
                        {
                            checkDatabase = profixioComponent.DatabasePopulated;
                        }

                        profixioComponent.Configure(nd);

                        Logger.DebugFormat("Adding {0}", profixioComponent.InstanceName);
                        this.JobInstances.Add(profixioComponent.InstanceName, profixioComponent);
                    }
                }

                // ... then create Purge components
                nodes = config.SelectNodes("/ComponentConfiguration/PurgeComponent[@Enabled='True' or @Enabled='true']");
                if (nodes != null)
                {
                    Logger.InfoFormat("PurgeComponent job instances found: {0}", nodes.Count);

                    foreach (XmlNode nd in nodes)
                    {
                        IBaseSportsDataInterfaces purgeComponent = new PurgeComponent();
                        purgeComponent.Configure(nd);

                        Logger.Debug("Adding " + purgeComponent.InstanceName);
                        this.JobInstances.Add(purgeComponent.InstanceName, purgeComponent);
                    }
                }

                // ... then create distribution components
                nodes = config.SelectNodes("/ComponentConfiguration/DistributionComponent[@Enabled='True' or @Enabled='true']");
                if (nodes == null)
                {
                    return;
                }

                Logger.InfoFormat("DistributionComponent job instances found: {0}", nodes.Count);

                foreach (XmlNode nd in nodes)
                {
                    IBaseSportsDataInterfaces purgeComponent = new PurgeComponent();
                    purgeComponent.Configure(nd);

                    Logger.Debug("Adding " + purgeComponent.InstanceName);
                    this.JobInstances.Add(purgeComponent.InstanceName, purgeComponent);
                }

                // ... then create Maintenance components
                nodes = config.SelectNodes("/ComponentConfiguration/MaintenanceComponents/MaintenanceComponent[@Enabled='True' or @Enabled='true']");
                if (nodes != null)
                {
                    Logger.InfoFormat("MaintenanceComponent job instances found: {0}", nodes.Count);

                    foreach (XmlNode nd in nodes)
                    {
                        IBaseSportsDataInterfaces maintenanceComponent = new MaintenanceComponent();
                        maintenanceComponent.Configure(nd);

                        Logger.Debug("Adding " + maintenanceComponent.InstanceName);
                        this.JobInstances.Add(maintenanceComponent.InstanceName, maintenanceComponent);
                    }
                }
            }
            catch (Exception exception)
            {
                if (exception.InnerException != null)
                {
                    Logger.Error(exception.InnerException.Message);
                    Logger.Error(exception.InnerException.StackTrace);
                }

                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
                Mail.SendException(exception);
            }
            finally
            {
                
                Logger.Info("=================================================================");
                Logger.Info("Done configuring jobs");
                Logger.Info("=================================================================");

                // ThreadContext.Stacks["NDC"].Pop();
                NDC.Pop();
            }
        }

        /// <summary>
        /// Starts the main service instance.
        /// </summary>
        /// <remarks>
        /// The function will set up polling and events to Start all processing jobs.
        /// </remarks>
        public void Start()
        {
            ThreadContext.Stacks["NDC"].Push("Start");

            try
            {
                // Start instances
                var numberOfJobs = this.JobInstances.Where(x => x.Value.Enabled);
                Logger.DebugFormat("Number of jobs to start: {0}", numberOfJobs.Count());

                // Looping jobs
                foreach (var kvp in this.JobInstances.Where(kvp => kvp.Value.Enabled))
                {
                    Logger.Info("Starting " + kvp.Value.InstanceName);
                    kvp.Value.Start();
                }

                // Starting maintenance
                Logger.Info("Starting maintenance timer");
                this.maintenanceTimer.Start();

                // Watch the config file
                this.configFileWatcher.EnableRaisingEvents = this.WatchConfigSet;
            }
            catch (FaultException fex)
            {
                Logger.Fatal("An error has occured. Could not Start service", fex);
                if (fex.InnerException == null)
                {
                    return;
                }

                Logger.Fatal(fex.InnerException.Message);
                Logger.Fatal(fex.InnerException.StackTrace);
                Mail.SendException(fex);
            }
            catch (Exception ex)
            {
                Logger.Fatal("An error has occured. Could not Start service", ex);
                Logger.Fatal(ex.StackTrace);
                Mail.SendException(ex);
            }
            finally
            {
                ThreadContext.Stacks["NDC"].Pop();
            }
        }

        /// <summary>
        /// The pause.
        /// </summary>
        public void Pause()
        {
            this.maintenanceTimer.Stop();
            this.configFileWatcher.EnableRaisingEvents = false;

            // Stop instances
            foreach (var kvp in this.JobInstances.Where(kvp => kvp.Value.Enabled))
            {
                Logger.InfoFormat("Stopping {0}", kvp.Value.InstanceName);
                kvp.Value.Stop();
            }
        }

        /// <summary>
        /// The stop.
        /// </summary>
        public void Stop()
        {
            this.Pause();

            // Kill notification service handler
            if (this.ServiceHost == null)
            {
                return;
            }

            this.ServiceHost.Close();
            this.ServiceHost = null;
        }

        #endregion
    }
}