﻿using System;
using System.Collections.Generic;
using System.Linq;

using NTB.SportsData.Components.ProfixioTournamentService;
using ServiceStack.Text;
using Tournament = NTB.SportsData.Classes.Classes.Tournament;
using log4net;

namespace NTB.SportsData.Components.Profixio.Service.DataMappers
{
    public class TournamentDataMapper : Interfaces.ITournamentDataMapper
    {
        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ProfixioGathererComponent));

        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        public TournamentDataMapper()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        public MatchTable[] GetTournamentStanding(string applicationKey, string tournamentId, string groupId)
        {
            var client = new ForTournamentPortClient();

            try
            {
                var tables = client.getTable(applicationKey, tournamentId, groupId, null, null, null);
                return tables;
            }
            catch (Exception exception)
            {
                Logger.Info("An error has occured, check error log for details");

                ErrorLogger.Error("An error has occured while trying to get data from the data provider:");
                ErrorLogger.Error(exception.Message);
                ErrorLogger.Error(exception.StackTrace);

                if (exception.InnerException != null)
                {
                    ErrorLogger.Error(exception.InnerException.Message);
                    ErrorLogger.Error(exception.InnerException.StackTrace);
                }
                // No access to tournament at requested level
            }

            return null;
            
            
        }

        public Tournament GetTournamentByMatchId(int matchId)
        {
            throw new System.NotImplementedException();
        }

        public List<Tournament> GetTournamentsBySportId(int sportId)
        {
            throw new System.NotImplementedException();
        }

        public List<Tournament> GetTournamentsBySeasonId(int seasonId)
        {
            throw new System.NotImplementedException();
        }

        public List<Tournament> GetCustomerTournaments(int sportId, int seasonId)
        {
            throw new System.NotImplementedException();
        }
    }
}
