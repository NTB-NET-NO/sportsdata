﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsData.Components.Profixio.Service.Interfaces;
using NTB.SportsData.Components.ProfixioTournamentService;
using log4net;

namespace NTB.SportsData.Components.Profixio.Service.DataMappers
{

    public class MatchDataMapper : IMatchDataMapper
    {
        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(MatchDataMapper));

        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        public MatchDataMapper()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        public List<TournamentMatch> GetTournamentMatches(string applicationKey, string tournamentId)
        {
            var client = new ForTournamentPortClient();

            /**
             * In Profixio terms we have to "translate" parameters to the following:
             * Applicationkey = orgId
             * tournamentId = SeasonId
             * ClassId = tournamentid
             */
            try
            {
                return
                    client.getMatches(applicationKey, tournamentId, string.Empty, string.Empty, null, null, null, null)
                        .ToList();
            }
            catch (Exception exception)
            {
                Logger.Info("An error has occured while trying to get data from the data provider. Check error log for details");
                ErrorLogger.Error("An error has occured while trying to get data from the data provider:");
                ErrorLogger.Error(exception.Message);
                ErrorLogger.Error(exception.StackTrace);

                if (exception.InnerException != null)
                {
                    ErrorLogger.Error(exception.InnerException.Message);
                    ErrorLogger.Error(exception.InnerException.StackTrace);
                }

                return new List<TournamentMatch>();
            }
            
        }
    }
}
