﻿using System.Collections.Generic;
using NTB.SportsData.Components.ProfixioTournamentService;

namespace NTB.SportsData.Components.Profixio.Service.Interfaces
{
    public interface ITournamentDataMapper
    {
        MatchTable[] GetTournamentStanding(string applicationKey, string tournamentId, string groupId);
    }

}
