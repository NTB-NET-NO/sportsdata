﻿using System.Collections.Generic;
using NTB.SportsData.Components.ProfixioTournamentService;

namespace NTB.SportsData.Components.Profixio.Service.Interfaces
{
    public interface IMatchDataMapper
    {
        List<TournamentMatch> GetTournamentMatches(string applicationKey, string tournamentId);
    }
}
