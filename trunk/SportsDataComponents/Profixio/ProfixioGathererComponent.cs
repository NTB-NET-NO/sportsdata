﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Threading;
using System.Xml;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Classes.Enums;

// Adding Log4net support
using log4net;

// Adding support for Quarts.Net
using Quartz;
using Quartz.Impl;
using Quartz.Impl.Matchers;

namespace NTB.SportsData.Components.Profixio
{
    using NTB.SportsData.Components.RemoteRepositories.Interfaces;

    public partial class ProfixioGathererComponent : Component, IBaseSportsDataInterfaces
    {
        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ProfixioGathererComponent));

        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");
        public ProfixioGathererComponent()
        {
            InitializeComponent();
        }

        public ProfixioGathererComponent(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        /// <summary>
        /// The Configured job name for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="InstanceName"/></remarks>
        protected string Name;

        /// <summary>
        /// Flag to indicate sucessful configure. Instance will not start polling nor handle messages if not properly Configured
        /// </summary>
        /// <remarks>Holds the Configured status of the instance. <c>True</c> means successfully Configured</remarks>
        protected bool Configured = false;

        /// <summary>
        /// Error-Retry flag
        /// </summary>
        /// <remarks>The flag is being set if a network or EWS error is encountered. Current operations are being aborted for later retry.</remarks>
        protected bool ErrorRetry = false;

        /// <summary>
        /// Send Email-notifications when a new messages is processed
        /// </summary>
        /// <remarks>Set to an email address. Multiple adresses are supported, separate with <c>;</c></remarks>
        protected string EmailNotification;

        /// <summary>
        /// Subject for email notifications
        /// </summary>
        /// <remarks>The subject is built during processing and used when sending the email when processing completes</remarks>
        protected string EmailSubject;

        /// <summary>
        /// Body for email notifications
        /// </summary>
        /// <remarks>The body is built during processing and used when sending the email when processing completes</remarks>
        protected string EmailBody;

        /// <summary>
        /// The schedule type.
        /// </summary>
        protected ScheduleType ScheduleType;

        /// <summary>
        ///     The Content Type
        /// </summary>
        protected string ContentType;

        #region Polling settings

        /// <summary>
        /// schedulerFactory to be used to create scheduled tasks
        /// </summary>
        protected StdSchedulerFactory SchedulerFactory = new StdSchedulerFactory();

        /// <summary>
        /// The scheduler.
        /// </summary>
        protected IScheduler Scheduler;

        /// <summary>
        /// The Configured interval for this instance, used for Continous polling
        /// </summary>
        /// <remarks>
        ///   <para>Indicates the interval time in seconds for <c>Continous</c>polling. 60 means that the job runs every minute.</para>
        ///   <para>Default value: <c>60</c></para>
        /// </remarks>
        protected int Interval = 60;


        #endregion

        #region File folders

        /// <summary>
        /// Output file folder
        /// </summary>
        /// <remarks>
        /// File folder where files shall be plased after being created. 
        /// </remarks>
        protected string FileOutputFolder;

        /// <summary>
        /// Error file folder
        /// </summary>
        /// <remarks>
        ///   <para>File folder where failing files are stored.</para>
        ///   <para>If this is not set, failing files are deleted instead of saved.</para>
        /// </remarks>
        protected string FileErrorFolder;

        /// <summary>
        /// File folder where completed files are stored
        /// </summary>
        /// <remarks>
        ///   <para>File folder where completed files are archived. Subdirectories for years, months and dates are created.</para>
        ///   <para>If this is not set, imported files are deleted instead of archived.</para>
        /// </remarks>
        protected string FileDoneFolder;

        #endregion

        /// <summary>
        /// The season id.
        /// </summary>
        protected int SeasonId = 0;

        protected int TournamentId = 0;

        /// <summary>
        /// The _data file creator.
        /// </summary>
        private readonly ProfixioDataFileCreator _dataFileCreator = new ProfixioDataFileCreator();

        /// <summary>
        /// Busy status event
        /// </summary>
        private readonly AutoResetEvent _busyEvent = new AutoResetEvent(true);

        /// <summary>
        /// Exit control event
        /// </summary>
        private readonly AutoResetEvent _stopEvent = new AutoResetEvent(false);

        /// <summary>
        /// The result field that we use to store if this is a result file or not.
        /// </summary>
        private bool _result;

        #region Common Components variables (no need to check)

        /// <summary>
        /// Gets the name of the instance.
        /// </summary>
        /// <value>The name of the instance.</value>
        /// <remarks>The Configured instance job name.</remarks>
        public string InstanceName
        {
            get { return Name; }
        }

        /// <summary>
        /// Gets the operation mode. 
        /// </summary>
        /// <value>The operation mode.</value>
        /// <remarks><c>Gatherer</c> is the only valid mode, its hard coded for this component.</remarks>
        public OperationMode OperationMode
        {
            get { return OperationMode.Gatherer; }
            set
            {
                operationMode = OperationMode;
            }
        }

        /// <summary>
        /// Gets the poll style.
        /// </summary>
        /// <value>The poll style.</value>
        /// <remarks>Contionous, Scheduled and FileSystemWatch are valid for <c>Gatherer</c> objects.</remarks>
        public PollStyle PollStyle
        {
            get { return pollStyle; }
        }

        #endregion

        /// <summary>
        /// Gets a value indicating whether the Enabled status of the instance.
        /// </summary>
        /// <value>The Enabled status.</value>
        /// <remarks><c>True</c> if the job instance is Enabled.</remarks>
        public bool Enabled
        {
            get { return enabled; }
        }

        /// <summary>
        /// Gets or sets the component state.
        /// </summary>
        public ComponentState ComponentState { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether database populated.
        /// </summary>
        public bool DatabasePopulated { get; set; }

        /// <summary>
        /// Gets or sets the maintenance mode.
        /// </summary>
        public MaintenanceMode MaintenanceMode { get; set; }

        /// <summary>
        /// Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        /// Gets or sets the federation id.
        /// </summary>
        public int FederationId { get; set; }

        /// <summary>
        /// The Populate Database field - tells if the job shall populate the database or not
        /// </summary>
        protected bool PopulateDatabase;

        /// <summary>
        /// The Create XML field - tells if we are to create an XML file or not
        /// </summary>
        protected bool CreateXml;

        /// <summary>
        /// Gets or sets the fixture type
        /// </summary>
        public string FixtureType { get; set; }

        public void Configure(XmlNode configNode)
        {
            if (configNode.Attributes == null)
            {
                throw new NotImplementedException();
            }

            // Basic configuration sanity check
            if (configNode.Name != "ProfixioComponent" || configNode.Attributes.GetNamedItem("Name") == null)
            {
                throw new ArgumentException("The XML configuration node passed is invalid", "configNode");
            }

            if (Thread.CurrentThread.Name == null)
            {
                Thread.CurrentThread.Name = configNode.Attributes.GetNamedItem("Name").ToString();
            }

            // Getting the OperationMode which tells if this what type of OperationMode this is
            try
            {
                Name = configNode.Attributes["Name"].Value;
                _Enabled = Convert.ToBoolean(configNode.Attributes["Enabled"].Value);
                operationMode = (OperationMode)Enum.Parse(typeof(OperationMode), configNode.Attributes["OperationMode"].Value, true);
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing OperationMode values in XML configuration node", ex);
            }

            if (configNode.Attributes != null && configNode.Attributes["MaintenanceMode"] != null)
            {
                try
                {
                    maintenanceMode =
                        (MaintenanceMode)
                        Enum.Parse(typeof(MaintenanceMode), configNode.Attributes["MaintenanceMode"].Value, true);
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException(
                        "Invalid or missing OperationMode values in XML configuration node", ex);
                }
            }

            // Getting the PollStyle which tells if this what type of PollStyle this is
            try
            {
                pollStyle = (PollStyle)Enum.Parse(typeof(PollStyle), configNode.Attributes["PollStyle"].Value, true);
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing PollStyle values in XML configuration node", ex);
            }

            // Getting the ScheduleType which tells if this what type of scheduling we are doing
            try
            {
                if (configNode.Attributes != null && configNode.Attributes["ScheduleType"] != null)
                {
                    ScheduleType =
                        (ScheduleType)
                        Enum.Parse(typeof(ScheduleType), configNode.Attributes["ScheduleType"].Value, true);
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing ScheduleType values in XML configuration node", ex);
            }

            try
            {
                if (configNode.Attributes != null && configNode.Attributes["ContentType"] != null)
                {
                    ContentType = configNode.Attributes["ContentType"].ToString();
                }
            }
            catch (Exception exception)
            {
                Logger.Info("Not able to get Content Type of message.");
                Logger.Info("No critical error");

                if (exception.InnerException != null)
                {
                    ErrorLogger.Error(exception.InnerException.Message);
                    ErrorLogger.Error(exception.InnerException.StackTrace);
                }
            }

            try
            {
                if (configNode.Attributes != null && configNode.Attributes["FixtureType"] != null)
                {
                    FixtureType = configNode.Attributes["FixtureType"].ToString();
                }
            }
            catch (Exception exception)
            {
                Logger.Info("Not able to get Content Type of message.");
                Logger.Info("No critical error");

                ErrorLogger.Error(exception.Message);
                ErrorLogger.Error(exception.StackTrace);

                if (exception.InnerException != null)
                {
                    ErrorLogger.Error(exception.InnerException.Message);
                    ErrorLogger.Error(exception.InnerException.StackTrace);
                }
            }

            // This boolean variable is used to tell us that the database shall be populated or not
            PopulateDatabase = false;

            const bool purge = false;

            CreateXml = false;

            // This string is used to set the name of the job
            // Getting the name of this Component instance
            try
            {
                if (configNode.Attributes != null)
                {
                    Name = configNode.Attributes["Name"].Value;
                    enabled = Convert.ToBoolean(configNode.Attributes["Enabled"].Value);
                    OperationMode = (OperationMode)Enum.Parse(typeof(OperationMode), configNode.Attributes["OperationMode"].Value, true);
                    pollStyle = (PollStyle)Enum.Parse(typeof(PollStyle), configNode.Attributes["PollStyle"].Value, true);
                }

                ThreadContext.Stacks["NDC"].Push(InstanceName);

                // This value is used to tell that we shall insert into database... I just wonder if we shall do it
                // on the schedule-level... Well, we try here first
                // Done: Consider using schedule-level to decide if we are to insert into database or not

                // Check if we are to insert the data into the database
                if (configNode.Attributes != null && configNode.Attributes["InsertDatabase"] != null)
                {
                    PopulateDatabase = Convert.ToBoolean(configNode.Attributes["InsertDatabase"].Value);
                }

                // Check if we are to create an XML-File
                if (configNode.Attributes != null && configNode.Attributes["XML"] != null)
                {
                    CreateXml = Convert.ToBoolean(configNode.Attributes["XML"].Value);
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to configure this job instance!", ex);
            }

            SetTournamentValues(configNode);
            SetFolderValues(configNode);
            
            DataFileParams dataFileParams;
            if (_Enabled)
            {
                CheckFolderExists();

                SetupPollStyle(configNode);
            }

            //Finish configuration
            ThreadContext.Stacks["NDC"].Pop();
            Configured = true;
        }

        private void SetupPollStyle(XmlNode configNode)
        {
            try
            {
                //Switch on pollstyle
                switch (pollStyle)
                {
                    #region PollStyle.Continous
                    case PollStyle.Continous:
                        SetupContinousPollStyle(configNode);

                        break;
                    #endregion

                    #region PollStyle.PushSubscribe

                    // Code for Pubnub push technology
                    // CONTINUE HERE TOMORROW! (Is this done, what am I to do here tomorrow) (2012-03-09)
                    case PollStyle.PushSubscribe:

                        break;
                    #endregion

                    #region PollStyle.CalendarPoll
                    case PollStyle.CalendarPoll:
                        // Getting the times for this job

                        try
                        {
                            IJobDetail jobDetail = null;
                            // = JobBuilder.Create<TournamentMatches>().WithIdentity("Job1", "Group1").Build();

                            ITrigger weeklyTrigger = null;


                            Logger.Info("Setting up schedulers");
                            Logger.Info("--------------------------------------------------------------");

                            // Getting the number of jobs to create
                            XmlNodeList nodeList =
                                configNode.SelectNodes("../ProfixioComponent[@Name='" + InstanceName + "']/Schedules/Schedule");

                            int jobs = 0;
                            if (nodeList != null)
                            {
                                jobs = nodeList.Count;
                            }
                            Logger.Debug("Number of jobs: " + jobs);

                            // Creating a string to store the ScheduleID
                            string scheduleId;

                            // Creating a boolean variable to store if we are to get results out or not
                            bool results = false;

                            // Creating an integer value to be used to tell the duration of the gathering of data
                            int duration = 0;

                            // Creating a boolean variable to store if this is a schedule-message og not
                            bool scheduleMessage = false;

                            // Creating integer for offset
                            // This variable is used to tell that we are to get tomorrows matches or yesterdays matches
                            // Used to create the 'Todays matches' for newspapers
                            // So a positive number is most commonly used

                            DateTime dtOffset = DateTime.Today;


                            Logger.Debug("Schedule of type: " + ScheduleType);

                            // Getting the scheduler
                            var scheduler1 = SchedulerFactory.GetScheduler();

                            switch (ScheduleType)
                            {

                                #region ScheduleType.Daily
                                case ScheduleType.Daily:
                                    if (nodeList != null)
                                        foreach (XmlNode node in nodeList)
                                        {
                                            scheduleId = node.Attributes["Id"].Value;

                                            // Creating the offset time 
                                            int offset = 0;
                                            if (node.Attributes["Offset"] != null)
                                            {
                                                offset = Convert.ToInt32(node.Attributes["Offset"].Value);
                                            }

                                            // If the value is larger or less than 0
                                            if (offset > 0 || offset < 0)
                                            {
                                                if (offset > 0)
                                                {
                                                    dtOffset = DateTime.Today.AddDays(offset);
                                                }
                                                else if (offset < 0)
                                                {
                                                    TimeSpan tsOffset = TimeSpan.FromDays(offset);
                                                    dtOffset = DateTime.Today.Subtract(tsOffset);
                                                }
                                            }
                                            // Splitting the time into two values so that we can create a cron job
                                            string[] scheduleTimeArray =
                                                node.Attributes["Time"].Value.Split(new[] { ':' });

                                            // If minutes contains two zeros (00), we change it to one zero (0) 
                                            // If not, scheduler won't understand
                                            if (scheduleTimeArray[1] == "00")
                                            {
                                                scheduleTimeArray[1] = "0";
                                            }

                                            // Doing the same thing for hours
                                            if (scheduleTimeArray[0] == "00")
                                            {
                                                scheduleTimeArray[0] = "0";
                                            }

                                            // Checking if there is a setting for results
                                            if (node.Attributes["Result"] != null)
                                            {
                                                results = Convert.ToBoolean(node.Attributes["Result"].Value);
                                                scheduleMessage = !results;
                                            }

                                            // Checking if there is a setting for Duration
                                            if (node.Attributes["Duration"] != null)
                                            {
                                                duration = Convert.ToInt32(node.Attributes["Duration"].Value);
                                            }

                                            /*
                                         * Creating cron expressions and more. 
                                         * This is the cron syntax:
                                         *  Seconds
                                         *  Minutes
                                         *  Hours
                                         *  Day-of-Month
                                         *  Month
                                         *  Day-of-Week
                                         *  Year (optional field)
                                         */



                                            // Creating the daily cronexpression
                                            string stringCronExpression = "0 " +
                                                                          scheduleTimeArray[1] + " " +
                                                                          scheduleTimeArray[0] + " " +
                                                                          "? " +
                                                                          "* " +
                                                                          "* ";


                                            // Setting up the CronTrigger
                                            Logger.Debug("Setting up the CronTrigger with following pattern: " + stringCronExpression);

                                            // Setting up the Daily CronTrigger
                                            // new CronExpression(stringCronExpression);


                                            // dailyCronTrigger = new CronTrigger(InstanceName + ScheduleID, InstanceName);
                                            // dailyCronTrigger.CronExpression = cronExpression;


                                            // Creating the daily Cron Trigger
                                            ITrigger dailyCronTrigger = TriggerBuilder.Create()
                                                                                         .WithIdentity(InstanceName + "_" + scheduleId, "groupProfixio")
                                                                                         .WithDescription(InstanceName)
                                                                                         .WithCronSchedule(stringCronExpression)
                                                                                         .Build();






                                            Logger.Debug("dailyCronTrigger: " + dailyCronTrigger.Description);

                                            // Creating the jobDetail 
                                            IJobDetail dailyJobDetail = JobBuilder.Create<ProfixioJobBuilder>()
                                                                                     .WithIdentity("job_" + InstanceName + "_" + scheduleId, "groupProfixio")
                                                                                     .WithDescription(InstanceName)
                                                                                     .Build();

                                            if (FileOutputFolder != null)
                                            {
                                                dailyJobDetail.JobDataMap["OutputFolder"] = FileOutputFolder;
                                            }
                                            dailyJobDetail.JobDataMap["InsertDatabase"] = PopulateDatabase;
                                            dailyJobDetail.JobDataMap["ScheduleType"] = ScheduleType.ToString();
                                            dailyJobDetail.JobDataMap["Results"] = results;
                                            dailyJobDetail.JobDataMap["ScheduleMessage"] = scheduleMessage;
                                            dailyJobDetail.JobDataMap["Duration"] = duration;
                                            dailyJobDetail.JobDataMap["CreateXML"] = CreateXml;
                                            dailyJobDetail.JobDataMap["DateOffset"] = dtOffset;
                                            dailyJobDetail.JobDataMap["SportId"] = SportId;
                                            dailyJobDetail.JobDataMap["TournamentId"] = TournamentId;
                                            dailyJobDetail.JobDataMap["OperationMode"] = OperationMode.ToString();

                                            if (dailyJobDetail == null) continue;

                                            // dailyJobDetail.AddJobListener(JobListenerName);

                                            Logger.Debug("Setting up and starting dailyJobDetail job " + dailyJobDetail.Description +
                                                         " using trigger : " + dailyCronTrigger.Description);
                                            // scheduler.ScheduleJob(dailyJobDetail, dailyCronTrigger[a]);
                                            scheduler1.ScheduleJob(dailyJobDetail, dailyCronTrigger);
                                        }
                                    break;
                                #endregion

                                #region ScheduleType.Weekly
                                case ScheduleType.Weekly:
                                    if (nodeList != null)
                                        foreach (XmlNode node in nodeList)
                                        {

                                            scheduleId = GetScheduleId(node);

                                            // Splitting the time into two values so that we can create a cron job
                                            if (node.Attributes != null)
                                            {
                                                string[] scheduleTimeArray =
                                                    node.Attributes["Time"].Value.Split(new[] { ':' });

                                                var minutes = GetScheduleMinutes(scheduleTimeArray);

                                                var hours = GetScheduleHours(scheduleTimeArray);


                                                results = GetScheduleResults(node, results, ref scheduleMessage);

                                                duration = GetScheduleDuration(node, duration);

                                                var scheduleWeek = GetScheduleWeek(node);


                                                var scheduleDay = GetScheduleDay(node);


                                                var dto = GetDayOfWeek(scheduleDay, hours, minutes);

                                                // Creating a simple Scheduler
                                                CalendarIntervalScheduleBuilder calendarIntervalSchedule =
                                                    CalendarIntervalScheduleBuilder.Create();


                                                // Creating the weekly Trigger using the stuff that we've calculated
                                                weeklyTrigger = TriggerBuilder.Create()
                                                    .WithDescription(InstanceName)
                                                    .WithIdentity(InstanceName + "_" + scheduleId, "groupProfixio")
                                                    .StartAt(dto)
                                                    .WithSchedule(calendarIntervalSchedule.WithIntervalInWeeks(Convert.ToInt32(scheduleWeek)))
                                                    .Build();
                                            }

                                            // This part might be moved
                                            switch (OperationMode)
                                            {
                                                case OperationMode.Gatherer:
                                                    jobDetail = JobBuilder.Create<ProfixioJobBuilder>()
                                                                                   .WithIdentity("job_" + InstanceName + "_" + scheduleId, "groupProfixio")
                                                                                   .WithDescription(InstanceName)
                                                                                   .Build();
                                                    break;
                                                
                                                case OperationMode.Distributor:
                                                    jobDetail = JobBuilder.Create<ProfixioJobBuilder>()
                                                                                   .WithIdentity("job_" + InstanceName + "_" + scheduleId, "groupProfixio")
                                                                                   .WithDescription(InstanceName)
                                                                                   .Build();
                                                    break;
                                                case OperationMode.Hold:
                                                    break;
                                            }

                                            if (FileOutputFolder != null || FileOutputFolder != "")
                                            {
                                                if (jobDetail != null)
                                                {
                                                    jobDetail.JobDataMap["OutputFolder"] = FileOutputFolder;
                                                }
                                            }

                                            if (jobDetail != null)
                                            {
                                                jobDetail.JobDataMap["InsertDatabase"] = PopulateDatabase;
                                                jobDetail.JobDataMap["ScheduleType"] = ScheduleType.ToString();
                                                jobDetail.JobDataMap["Results"] = results;
                                                jobDetail.JobDataMap["ScheduleMessage"] = scheduleMessage;
                                                jobDetail.JobDataMap["Duration"] = duration;
                                                jobDetail.JobDataMap["CreateXML"] = CreateXml;
                                                jobDetail.JobDataMap["SportId"] = SportId;
                                                jobDetail.JobDataMap["TournamentId"] = TournamentId;
                                                jobDetail.JobDataMap["OperationMode"] = OperationMode.ToString();
                                            }
                                        }

                                    if (jobDetail != null)
                                    {
                                        if (weeklyTrigger != null)
                                        {
                                            Logger.Debug("Setting up and starting weeklyJobDetail job " + jobDetail.Description +
                                                         " using trigger : " + weeklyTrigger.Description);
                                            scheduler1.ScheduleJob(jobDetail, weeklyTrigger);
                                        }
                                    }

                                    break;

                                #endregion

                                //case ScheduleType.Monthly:
                                //    // doing Monthly stuff here
                                //    throw new NotSupportedException("Monthly scheduletype is not implemented!");
                                //    break;

                                //case ScheduleType.Yearly:
                                //    // doing yearly Stuff here
                                //    throw new NotSupportedException("Yearly scheduletype is not implemented!");
                                //    break;

                            }

                            // We might move the code regarding jobdetails here... 

                            // Configuring the datafile-object so that we know that the databases are up and running

                            // We don't have to check for datafile stuff when we only do purge...
                            if (OperationMode != OperationMode.Purge)
                            {

                                var dataFileParams = new DataFileParams
                                {
                                    FederationId = FederationId,
                                    CreateXml = CreateXml,
                                    SportId = SportId,
                                    TournamentId = TournamentId
                                };

                                var datafile = new ProfixioDataFileCreator
                                {
                                    DataParams = dataFileParams
                                };

                                if (datafile.Configure() == false)
                                {
                                    throw new Exception("Problems initiating database tables");
                                }
                            }

                            // Catching exceptions that might occur
                        }
                        catch (SchedulerConfigException sce)
                        {
                            Logger.Debug("A SchedulerConfigException has occured: ", sce);
                        }
                        catch (SchedulerException se)
                        {
                            Logger.Debug("A schedulerException has occured: ", se);
                        }
                        catch (Exception e)
                        {
                            Logger.Debug("An exception has occured", e);
                        }

                        break;
                    #endregion

                    #region PollStyle.Scheduled
                    case PollStyle.Scheduled:
                        /* 
                        schedule = configNode.Attributes["Schedule"].Value;
                        TimeSpan s = Utilities.GetScheduleInterval(schedule);
                        logger.DebugFormat("Schedule: {0} Calculated time to next run: {1}", schedule, s);
                        pollTimer.Interval = s.TotalMilliseconds;
                        */
                        throw new NotSupportedException("Invalid polling style for this job type");

                    #endregion

                    #region PollStyle.FileSystemWatch
                    case PollStyle.FileSystemWatch:
                        //Check for config overrides
                        /*
                        if (configNode.Attributes.GetNamedItem("BufferSize") != null)
                            bufferSize = Convert.ToInt32(configNode.Attributes["BufferSize"].Value);
                        logger.DebugFormat("FileSystemWatcher buffer size: {0}", bufferSize);

                        //Set values
                        filesWatcher.Path = fileInputFolder;
                        filesWatcher.Filter = fileFilter;
                        filesWatcher.IncludeSubdirectories = includeSubdirs;
                        filesWatcher.InternalBufferSize = bufferSize;

                        //Do not start the event watcher here, wait until after the startup cleaning job
                        pollTimer.Interval = 5000; // short startup;
                        */
                        throw new NotSupportedException("Invalid polling style for this job type");

                    #endregion



                    default:
                        //Unsupported pollstyle for this object
                        throw new NotSupportedException("Invalid polling style for this job type (" + pollStyle + ")");
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing PollStyle-specific values in XML configuration node: " + ex.Message, ex);
            }
            
        }

        private void CheckFolderExists()
        {
            // Checking if file folders exists
            try
            {
                if (FileOutputFolder != null)
                {
                    if (!Directory.Exists(FileOutputFolder))
                    {
                        Directory.CreateDirectory(FileOutputFolder);
                    }
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid, unknown or missing file folder: " + FileOutputFolder, ex);
            }
        }

        private void SetFolderValues(XmlNode configNode)
        {
            try
            {
                // Find the file folder to work with
                // DONE: This could be a nodelist though - need to check if we get more than one result
                XmlNodeList folderNodes = configNode.SelectNodes("Folders/Folder[@Type='XMLOutputFolder']");
                if (folderNodes != null)
                    foreach (XmlNode folderNode in folderNodes)
                    {
                        // XmlNode folderNode = configNode.SelectSingleNode("Folders/Folder[@Type='XMLOutputFolder']");
                        // configNode.SelectSingleNode("Folder").Attributes("XMLOutputFolder");
                        if (folderNode != null)
                        {
                            FileOutputFolder = folderNode.InnerText;

                        }

                        Logger.InfoFormat("Gatherer job - Polling: {0} / File output folder: {1} / Enabled: {2}",
                                          Enum.GetName(typeof(PollStyle), pollStyle), FileOutputFolder, _Enabled);
                    }
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get output-folder", ex);
            }
        }
        private void SetTournamentValues(XmlNode configNode)
        {
            // Get TournamentId
            var tournamentNode = configNode.SelectSingleNode("Tournament");

            if (tournamentNode != null)
            {
                if (tournamentNode.Attributes != null && tournamentNode.Attributes["Id"] != null)
                {
                    TournamentId = Convert.ToInt32(tournamentNode.Attributes["Id"].Value);
                }

                if (tournamentNode.Attributes != null && tournamentNode.Attributes["sportId"] != null)
                {
                    SportId = Convert.ToInt32(tournamentNode.Attributes["sportId"].Value);
                }
            }
        }

        private static DateTimeOffset GetDayOfWeek(string scheduleDay, int hours, int minutes)
        {
            // If we use the DateTimeOffset it means that we have to find out if the day has passed or not.
            // So we have to find out which day it is today
            DayOfWeek today = DateTime.Today.DayOfWeek;
            int numDayOfWeek = 0;
            switch (today.ToString())
            {
                case "Monday":
                    numDayOfWeek = 1;
                    break;
                case "Tuesday":
                    numDayOfWeek = 2;
                    break;

                case "Wednesday":
                    numDayOfWeek = 3;
                    break;

                case "Thursday":
                    numDayOfWeek = 4;
                    break;

                case "Friday":
                    numDayOfWeek = 5;
                    break;

                case "Saturday":
                    numDayOfWeek = 6;
                    break;

                case "Sunday":
                    numDayOfWeek = 0;
                    break;
            }

            // Checking if numDayOfWeek is smaller than ScheduleDay
            DateTimeOffset dto = DateTime.Today;

            // Get the scheduled day
            int scheduledDay = Convert.ToInt32(scheduleDay);

            int days;
            if (numDayOfWeek <= scheduledDay)
            {
                // It is, so we need to find out when the next scheduling should happen
                days = scheduledDay - numDayOfWeek; // this can be zero

                dto = DateTime.Today.AddDays(days).AddHours(hours).AddMinutes(minutes);

                // <Schedule Id="1" Time="15:12" DayOfWeek="1" Week="2"/>
            }
            else if (numDayOfWeek > scheduledDay)
            {
                const int weekdays = 7;
                int daysLeft = weekdays - numDayOfWeek;
                days = daysLeft + scheduledDay;

                dto = DateTime.Today.AddDays(days).AddHours(hours).AddMinutes(minutes).ToUniversalTime();
            }
            return dto;
        }

        private static string GetScheduleDay(XmlNode node)
        {
            string scheduleDay = "";
            if (node.Attributes != null && node.Attributes["DayOfWeek"] != null)
            {
                scheduleDay = node.Attributes["DayOfWeek"].Value;
            }
            return scheduleDay;
        }

        private static string GetScheduleWeek(XmlNode node)
        {
            string scheduleWeek = "";
            if (node.Attributes != null && node.Attributes["Week"] != null)
            {
                scheduleWeek =
                    node.Attributes["Week"].Value;
            }
            return scheduleWeek;
        }

        private static int GetScheduleDuration(XmlNode node, int duration)
        {
            // Checking if there is a setting for Duration
            if (node.Attributes != null && node.Attributes["Duration"] != null)
            {
                duration = Convert.ToInt32(node.Attributes["Duration"].Value);
            }
            return duration;
        }

        private static bool GetScheduleResults(XmlNode node, bool results, ref bool scheduleMessage)
        {
            // Checking if there is a setting for results
            if (node.Attributes != null && node.Attributes["Results"] != null)
            {
                results = Convert.ToBoolean(node.Attributes["Results"].Value);
                scheduleMessage = !results;
            }
            return results;
        }

        private static int GetScheduleHours(string[] scheduleTimeArray)
        {
            // Doing the same thing for hours
            if (scheduleTimeArray[0] == "00")
            {
                scheduleTimeArray[0] = "0";
            }
            int hours = Convert.ToInt32(scheduleTimeArray[0]);
            return hours;
        }

        private static int GetScheduleMinutes(string[] scheduleTimeArray)
        {
            // If minutes contains two zeros (00), we change it to one zero (0) 
            // If not, scheduler won't understand
            if (scheduleTimeArray[1] == "00")
            {
                scheduleTimeArray[1] = "0";
            }
            int minutes = Convert.ToInt32(scheduleTimeArray[1]);
            return minutes;
        }

        private static string GetScheduleId(XmlNode node)
        {
            if (node.Attributes == null)
            {
                throw new SportsDataException("Cannot find Schedule Id!");
            }
            string scheduleId = node.Attributes["Id"].Value;
            return scheduleId;
        }

        private void SetupContinousPollStyle(XmlNode configNode)
        {
            if (configNode.Attributes != null)
                Interval = Convert.ToInt32(configNode.Attributes["Interval"].Value);
            XmlNodeList folderNodeList =
                configNode.SelectNodes("../NIFTeamComponent[@Name='" + InstanceName + "']/Folders/Folder");


            int folderJobs = 0;
            if (folderNodeList != null)
            {
                folderJobs = folderNodeList.Count;
            }

            // <Folder Type="XMLOutputFolder" Mode="FILE" Results="True" PushSchedule="False">C:\Utvikling\norm\xml\NTBSportsData</Folder>
            // string OutputFolder = "";
            Logger.Debug("Number of Folders: " + folderJobs);
            if (folderNodeList != null)
            {
                foreach (XmlNode foldernode in folderNodeList)
                {
                    if (foldernode.Attributes != null && foldernode.Attributes["Result"] != null)
                    {
                        PushResults = Convert.ToBoolean(foldernode.Attributes["Result"].Value);
                    }

                    if (foldernode.Attributes != null && foldernode.Attributes["PushSchedule"] != null)
                    {
                        PushSchedule = Convert.ToBoolean(foldernode.Attributes["PushSchedule"].Value);
                    }

                    FileOutputFolder = foldernode.InnerText;
                }
            }

            if (Interval == 0)
            {
                Interval = 5000;
            }
            Interval = Interval * 60 * 1000; // Adding so that it is one minute
            pollTimer.Interval = Interval; // short startup - interval * 1000;
            Logger.DebugFormat("Poll interval: {0} seconds", Interval);
        }

        public void Start()
        {
            if (!Configured)
            {
                throw new SportsDataException("ProfixioGathererComponent is not properly Configured. NIFWSComponent::Start() Aborted!");
            }

            if (!_Enabled)
            {
                throw new SportsDataException("ProfixioGathererComponent is not Enabled. NIFWSComponent::Start() Aborted!");
            }

            if (pollStyle == PollStyle.Continous)
            {
                // Not sure if we have to do anything
                Logger.Info("Starting Continous PollStyle component");
                pollTimer.Enabled = true;
            }


            if (pollStyle == PollStyle.CalendarPoll)
            {
                try
                {
                    Logger.Info("Setting up jobListener and scheduler");

                    // starting the scheduler
                    IScheduler nifScheduler = SchedulerFactory.GetScheduler();

                    // Creating the job listener
                    // new NIFSportsDataJobListener();

                    IList<string> jobGroupNames = nifScheduler.GetJobGroupNames();

                    foreach (string jobGroupName in jobGroupNames)
                    {
                        Logger.Debug("JobGroupName: " + jobGroupName);

                        if (jobGroupName != "GroupNIF") continue;
                        Logger.Debug("Key: " +
                                     nifScheduler.GetJobKeys(GroupMatcher<JobKey>.GroupContains(jobGroupName)));

                        GroupMatcher<JobKey> groupMatcher = GroupMatcher<JobKey>.GroupContains(jobGroupName);
                        Quartz.Collection.ISet<JobKey> jobKeys = nifScheduler.GetJobKeys(groupMatcher);

                        foreach (JobKey jobKey in jobKeys)
                        {
                            Logger.Debug("Name: " + jobKey.Name);
                            Logger.Debug("Group: " + jobKey.Group);
                        }
                    }

                    IList<string> triggerGroupNames = nifScheduler.GetTriggerGroupNames();

                    foreach (string triggerGroupName in triggerGroupNames)
                    {
                        Logger.Debug("TriggerGroupName: " + triggerGroupName);
                    }

                    IList<string> listOfJobs = nifScheduler.GetJobGroupNames();
                    foreach (string job in listOfJobs)
                    {
                        Logger.Debug("Job: " + job);
                    }

                    Logger.Debug("This Component name: " + Name);


                    // Starting scheduler
                    nifScheduler.Start();
                }
                catch (SchedulerException exception)
                {
                    Logger.Debug(exception);
                    throw new Exception("An error has occured when starting Scheduler.", exception);
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    throw new Exception("An error has occured when starting Scheduler.", exception);
                }
            }

            _stopEvent.Reset();
            _busyEvent.Set();

            // Starting poll-timer
            // pollTimer.Interval = 2000;
            pollTimer.Enabled = true;
            pollTimer.Start();
        }

        public void Stop()
        {
            if (!Configured)
            {
                throw new SportsDataException("ProfixioGathererComponent is not properly Configured. NIFWSComponent::Start() Aborted!");
            }

            if (!_Enabled)
            {
                throw new SportsDataException("ProfixioGathererComponent is not Enabled. NIFWSComponent::Start() Aborted!");
            }

            #region Stopping Scheduler
            // Check status - Handle busy polltimer loops
            if (pollStyle == PollStyle.CalendarPoll)
            {
                try
                {
                    Logger.Debug("Deleting / stopping jobs!");
                    ISchedulerFactory sf = new StdSchedulerFactory();
                    IScheduler sched = sf.GetScheduler();
                    // Shutting down scheduler

                    sched.GetJobGroupNames();

                    List<JobKey> keys = new
                        List<JobKey>(sched.GetJobKeys(GroupMatcher<JobKey>.GroupEquals(InstanceName)));
                    // Looping through each key as we'd like to know when things go right and not!
                    keys.ForEach(key =>
                    {
                        IJobDetail detail = sched.GetJobDetail(key);
                        sched.DeleteJob(key);

                        Logger.Debug("Shutting down: " + detail.Description + "(" + key.Name + ")" + " Group: " + key.Group);
                    });

                    sched.Shutdown();

                }
                catch (SchedulerException schedulerException)
                {
                    Logger.Fatal(schedulerException);
                }

                catch (Exception exception)
                {
                    Logger.Error("Problems closing NIF WebClients!", exception);

                }
            }
            #endregion
        }

        /// <summary>
        /// The push results.
        /// </summary>
        protected bool PushResults = false;

        /// <summary>
        /// The push schedule.
        /// </summary>
        protected bool PushSchedule = false;

        /// <summary>
        /// The get component state.
        /// </summary>
        /// <returns>
        /// The <see cref="ComponentState"/>.
        /// </returns>
        public ComponentState GetComponentState(bool componentState)
        {
            return ComponentState.Running;
        }
    }
}
