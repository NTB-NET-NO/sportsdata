﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Glue;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Mappers;
using NTB.SportsData.Components.ProfixioTournamentService;

namespace NTB.SportsData.Components.Profixio.Facade.DataMappers
{
    public class MatchTableRowMapper : BaseMapper<MatchTableRow, TournamentTable>
    {
        protected override void SetUpMapper(Mapping<MatchTableRow, TournamentTable> mapper)
        {
            mapper.Relate(x => x.MatchClassId, y => y.MatchClassId, StringToInt32Converter());
            mapper.Relate(x => x.MatchGroupId, y => y.TournamentId, StringToInt32Converter());
            mapper.Relate(x => x.a, y => y.TeamName);
            mapper.Relate(x => x.id, y => y.TeamId, NullableIntToIntConverter());
            mapper.Relate(x => x.b, y => y.NoMatches, StringToInt32Converter());
            mapper.Relate(x => x.c, y => y.NoWins, StringToInt32Converter());
            mapper.Relate(x => x.d, y => y.NoDraws, StringToInt32Converter());
            mapper.Relate(x => x.e, y => y.NoLosses, StringToInt32Converter());
            mapper.Relate(x => x.f, y => y.DiffString);
            mapper.Relate(x => x.g, y => y.Points, StringToInt32Converter());
        }

                    /*
                     * Header.a: TeamName
Header.b: Matches
Header.c: Win
Header.d: Draw
Header.e: Lost
Header.f: Goal +/-
Header.g: Points
Header.h: 
Header.i: 
Header.j: 
Header.k: 
Header.l: 
        */
    }
}
