﻿using System;
using Glue;
using Glue.Converters;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Mappers;
using NTB.SportsData.Components.ProfixioTournamentService;
using ServiceStack.Text;

namespace NTB.SportsData.Components.Profixio.Facade.DataMappers
{
    public class MatchMapper : BaseMapper<TournamentMatch, Match>
    {
        // Just an empty constructor...
        
        protected override void SetUpMapper(Mapping<TournamentMatch, Match> mapper)
        {
            mapper.Relate(x => x.awayteamid, y => y.AwayTeamId);
            mapper.Relate(x => x.awayteamname, y => y.AwayTeamName);
            mapper.Relate(x => x.awayclubid, y => y.AwayClubId);
            mapper.Relate(x => x.awayclubname, y => y.AwayClubName);
            mapper.Relate(x => x.classid, y => y.ClassId);
            mapper.Relate(x => x.homeclubid, y => y.HomeClubId);
            mapper.Relate(x => x.homeclubname, y => y.HomeClubName);
            mapper.Relate(x => x.hometeamid, y => y.HomeTeamId);
            mapper.Relate(x => x.hometeamname, y => y.HomeTeamName);
            mapper.Relate(x => x.endgamelevel, y => y.EndGameLevel, NullableIntToIntConverter());
            mapper.Relate(x => x.id, y => y.Id);
            mapper.Relate(x => x.matchcomment, y => y.MatchComment);
            mapper.Relate(x => x.matchdate, y => y.MatchDate, StringToDateTimeConverter());
            mapper.Relate(x => x.matchname, y => y.MatchName);
            mapper.Relate(x => x.matchno, y => y.MatchNo);
            mapper.Relate(x => x.reason, y => y.Reason);
            mapper.Relate(x => x.sortorder, y => y.SortOrder);
            mapper.Relate(x => x.winner, y => y.Winner);
            mapper.Relate(x => x.UpdateTimeStamp, y => y.UpdateTimeStampString);
            mapper.Relate(x => x.MatchGroupId, y=> y.MatchGroupId);
            mapper.Relate(x => x.awaygoal, y => y.AwayTeamGoals, StringToInt32Converter());
            mapper.Relate(x => x.homegoal, y => y.HomeTeamGoals, StringToInt32Converter());
            mapper.Relate(x => x.classid, y => y.ClassId, NullableIntToIntConverter());
        }
    }
}
