﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Mappers;
using NTB.SportsData.Components.NIFConnectService;
using NTB.SportsData.Components.Profixio.Facade.DataMappers;
using NTB.SportsData.Components.Profixio.Service.DataMappers;
using NTB.SportsData.Components.Profixio.Service.Interfaces;
using NTB.SportsData.Components.ProfixioTournamentService;
using TournamentTable = NTB.SportsData.Classes.Classes.TournamentTable;

namespace NTB.SportsData.Components.Profixio.Facade
{
    public class ProfixioFacade
    {
        private readonly IMatchDataMapper _matchDataMapper;
        private readonly ITournamentDataMapper _tournamentDataMapper;

        public ProfixioFacade()
        {
            _matchDataMapper = new MatchDataMapper();
            _tournamentDataMapper = new TournamentDataMapper();
        }

        public List<TournamentTable> GetTournamentStanding(string applicationId, string tournamentId, int groupId)
        {
            // Get the table from the 
            var results = _tournamentDataMapper.GetTournamentStanding(applicationId, tournamentId, groupId.ToString());

            if (results == null)
            {
                return new List<TournamentTable>();
            }

            // return result.Select(row => mapper.Map(row, new Match())).ToList();

            var mapper = new MatchTableRowMapper();

            List<TournamentTable> tableRows = new List<TournamentTable>();
            foreach (var result in results)
            {
                tableRows.AddRange(result.Rows.Select(row => mapper.Map(row, new TournamentTable())).ToList());
            }
            return tableRows;
        }

        public List<NTB.SportsData.Classes.Classes.Match> GetRemoteMatchesByTournamentId(string applicationKey, string tournamentId)
        {
            var result = _matchDataMapper.GetTournamentMatches(applicationKey, tournamentId);

            if (result == null)
            {
                return new List<NTB.SportsData.Classes.Classes.Match>();
            }
            
            var mapper = new DataMappers.MatchMapper();


            return result.Select(row => mapper.Map(row, new NTB.SportsData.Classes.Classes.Match())).ToList();
        }
    }
}
