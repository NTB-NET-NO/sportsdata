﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProfixioDataFileCreator.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The profixio data file creator.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using NTB.SportsData.Components.PublicRepositories;

namespace NTB.SportsData.Components.Profixio
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;

    using log4net;

    using NTB.SportsData.Classes.Classes;
    using NTB.SportsData.Classes.Enums;
    using NTB.SportsData.Components.Profixio.Facade;
    using NTB.SportsData.Components.ProfixioTournamentService;
    using Tournament = NTB.SportsData.Classes.Classes.Tournament;

    /// <summary>
    /// The profixio data file creator.
    /// </summary>
    public class ProfixioDataFileCreator
    {
        /// <summary>
        /// Static logger
        /// </summary>
        public static readonly ILog Logger = LogManager.GetLogger(typeof(ProfixioDataFileCreator));

        /// <summary>
        /// The _application key.
        /// </summary>
        private string _applicationKey = ConfigurationManager.AppSettings["ProfixioApplicationKey"];

        /// <summary>
        /// The client.
        /// </summary>
        private ForTournamentPortClient client = new ForTournamentPortClient();

        /// <summary>
        /// Initializes a new instance of the <see cref="ProfixioDataFileCreator"/> class.
        /// </summary>
        public ProfixioDataFileCreator()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();

            if (!LogManager.GetRepository()
                     .Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        /// <summary>
        /// Using the Data File Params class
        /// </summary>
        public DataFileParams DataParams { get; set; }

        /// <summary>
        /// Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        /// The configure.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Configure()
        {
            return true;
        }

        /// <summary>
        /// The create results.
        /// </summary>
        /// <param name="applicationKey">
        /// The application key.
        /// </param>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        public void CreateResults(string applicationKey, int tournamentId)
        {
            var customerTournaments = this.GetCustomerTournaments();

            this.CreateMatchResults(customerTournaments, tournamentId.ToString());
        }

        /// <summary>
        /// The get list of customers.
        /// </summary>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Customer> GetListOfCustomers(Tournament tournament)
        {
            var repository = new CustomerRepository();

            var customers = repository.GetCustomerByTournamentAndSportId(tournament.SportId, tournament.Id);

            if (customers != null)
            {
                return customers;
            }

            return new List<Customer>();
        }

        /// <summary>
        /// The get tournament standing.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <param name="groupId">
        /// The group id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        private List<TournamentTable> GetTournamentStanding(int tournamentId, int groupId)
        {
            var facade = new ProfixioFacade();
            var applicationKey = ConfigurationManager.AppSettings["ProfixioApplicationKey"];
            var result = facade.GetTournamentStanding(applicationKey, tournamentId.ToString(), groupId);

            return result;
        }

        /// <summary>
        /// The get organization from sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see cref="Federation"/>.
        /// </returns>
        private Federation GetOrganizationFromSportId(int sportId)
        {
            var orgRepository = new FederationRepository();
            return orgRepository.GetFederationBySportId(sportId);
        }

        /// <summary>
        /// A private function returning true of false if any of the results are larger than zero
        /// </summary>
        /// <param name="matches">
        /// List of matches to check
        /// </param>
        /// <returns>
        /// true if any of the matches has result, false otherwise
        /// </returns>
        private bool HasResults(List<Match> matches)
        {
            return matches.Any(match => match.HomeTeamGoals > 0 || match.AwayTeamGoals > 0);
        }

        /// <summary>
        /// The create match results.
        /// </summary>
        /// <param name="tournaments">
        /// The tournaments.
        /// </param>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        private void CreateMatchResults(List<Tournament> tournaments, string tournamentId)
        {
            /**
             * In Profixio terms we have to "translate" parameters to the following:
             * Applicationkey = OrgId
             * TournamentId = SeasonId
             * ClassId = TournamentNumber
             */
            var remotematches = this.GetTournamentMatches(this._applicationKey, tournamentId);

            var endGames = false;
            var computedEndGame = 0;
            foreach (var tournament in tournaments)
            {
                var matches = new List<Match>();
                var levelAEndGameMatches = new List<Match>();
                var levelBEndGameMatches = new List<Match>();

                var tournament1 = tournament;

                if (tournament1.Number == computedEndGame && endGames)
                {
                    continue;
                }

                if (this.DataParams.Results)
                {
                    var todaysMatches = from m in remotematches where (m.MatchDate != null) && m.MatchDate.Value.Date.ToShortDateString() == DateTime.Today.Date.ToShortDateString() select m;

                    matches.AddRange(
                        from m in remotematches
                        where m.MatchGroupId == tournament1.Id && (m.MatchDate != null) && m.MatchDate.Value.Date.ToShortDateString() == DateTime.Today.Date.ToShortDateString() && Convert.ToInt32(
                            m.MatchDate.Value.ToShortTimeString()
                                                                                                                                                                                 .Replace(":", string.Empty)) <= Convert.ToInt32(
                                                                                                                                                                                     DateTime.Now.ToShortTimeString()
                                                                                                                                                                                                                     .Replace(":", string.Empty)) && m.Winner.Trim() != string.Empty select m);

                    if (matches.Count == 0)
                    {
                        matches.AddRange(
                            from m in remotematches
                            where m.ClassId == tournament1.Number && (m.MatchDate != null) && m.MatchDate.Value.ToShortDateString() == DateTime.Today.Date.ToShortDateString() && Convert.ToInt32(
                                m.MatchDate.Value.ToShortTimeString()
                                                                                                                                                                               .Replace(":", string.Empty)) <= Convert.ToInt32(
                                                                                                                                                                                   DateTime.Now.ToShortTimeString()
                                                                                                                                                                                                                   .Replace(":", string.Empty)) && m.Winner.Trim() != string.Empty select m);

                        if (matches.Count > 0)
                        {
                            // Get A-endgamematches
                            levelAEndGameMatches = (from m in matches where m.EndGameLevel == 1 select m).ToList();

                            if (levelAEndGameMatches.Count > 0)
                            {
                                endGames = true;
                            }

                            // Get A-endgamematches
                            levelBEndGameMatches = (from m in matches where m.EndGameLevel == 2 select m).ToList();

                            if (levelBEndGameMatches.Count > 0)
                            {
                                endGames = true;
                            }
                        }
                    }
                }
                else
                {
                    matches.AddRange(from m in remotematches where m.MatchGroupId == tournament1.Id && (m.MatchDate != null) && m.MatchDate.Value.Date.ToShortDateString() == DateTime.Today.Date.ToShortDateString() select m);

                    if (matches.Count == 0)
                    {
                        matches.AddRange(from m in remotematches where m.ClassId == tournament1.Number && (m.MatchDate != null) && m.MatchDate.Value.ToShortDateString() == DateTime.Today.Date.ToShortDateString() select m);

                        if (matches.Count > 0)
                        {
                            // Get A-endgamematches
                            levelAEndGameMatches = (from m in matches where m.EndGameLevel == 1 select m).ToList();

                            if (levelAEndGameMatches.Count > 0)
                            {
                                endGames = true;
                            }

                            // Get A-endgamematches
                            levelBEndGameMatches = (from m in matches where m.EndGameLevel == 2 select m).ToList();

                            if (levelBEndGameMatches.Count > 0)
                            {
                                endGames = true;
                            }
                        }
                    }
                }

                if (matches.Count == 0 && (levelAEndGameMatches.Count == 0 || levelBEndGameMatches.Count == 0))
                {
                    Logger.Info("Queries returned no matches");
                    continue;
                }

                /**
                 * TODO:
                 * Need to find out how we deal with A/B-end game levels
                 * With current setup we will be running the same matches but with different 
                 * tournament-Ids to clients, meaning they will get duplicates
                 * 
                 * To solve some of this, I've added endGame boolean so we can continue the loop in case the 
                 * tournament number is equal the current stored tournament number.
                 * 
                 * The challenge is below and how to deal with all the other stuff...
                 */
                var tournamentRound = "0";

                if (endGames)
                {
                    // Checking if levelA-matches are present
                    if (levelAEndGameMatches.Count > 0)
                    {
                        tournamentRound = "A-sluttspill";
                        tournament1.Name += ", " + tournamentRound;
                        this.CreateXmlDocument(tournament1, levelAEndGameMatches, tournamentRound, true);
                        computedEndGame = tournament1.Number;
                    }

                    // No else, or it will never come here
                    if (levelBEndGameMatches.Count > 0)
                    {
                        tournamentRound = "B-sluttspill";
                        tournament1.Name += ", " + tournamentRound;
                        this.CreateXmlDocument(tournament1, levelBEndGameMatches, tournamentRound, true);
                        computedEndGame = tournament1.Number;
                    }
                }
                else
                {
                    this.CreateXmlDocument(tournament1, matches, tournamentRound, false);
                }
            }

            Logger.Info("Done getting matches from provider");

            // List<Match> matches = (from tournament in tournaments from match in remoteMatches where match.ClassId == tournament.Number select match).ToList();
        }

        /// <summary>
        /// The create xml document.
        /// </summary>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <param name="matches">
        /// The matches.
        /// </param>
        /// <param name="tournamentRound">
        /// The tournament round.
        /// </param>
        /// <param name="isPlayoff">
        /// The is playoff.
        /// </param>
        private void CreateXmlDocument(Tournament tournament, List<Match> matches, string tournamentRound, bool isPlayoff)
        {
            var xmlDataCreator = new SportsDataXmlDocumentCreator();

            var ageCategory = this.GetAgeCategory(tournament.Id);
            Logger.Info("Creating AgeCategory-node for Tournament " + tournament.Id);
            xmlDataCreator.CreateAgeCategoryNode(ageCategory, tournament);
            xmlDataCreator.CreateMatchNode(matches);

            Logger.Info("Creating Header-node for Tournament " + tournament.Id);
            xmlDataCreator.CreateHeaderNode();

            Logger.Info("Creating Tournament-node for Tournament " + tournament.Id);
            if (matches[0].MatchDate != null)
            {
                xmlDataCreator.CreateTournamentNode(tournament, tournamentRound, matches[0].MatchDate.Value);
            }

            var organization = this.GetOrganizationFromSportId(tournament.SportId);

            Logger.Info("Creating Organization-node for Tournament " + tournament.Id);
            xmlDataCreator.CreateOrganizationNode(organization);

            Logger.Info("Creating Sport-node for Tournament " + tournament.Id);
            xmlDataCreator.CreateSportNode(organization);

            var customers = this.GetListOfCustomers(tournament);

            Logger.Info("Creating Customer-node for Tournament " + tournament.Id);
            xmlDataCreator.CreateCustomerNode(customers);

            var standing = new List<TournamentTable>();
            if (isPlayoff == false)
            {
                standing = this.GetTournamentStanding(tournament.SeasonId, tournament.Id);
            }

            if (standing.Count > 0)
            {
                xmlDataCreator.CreateTournamentStandingNode(standing);
                xmlDataCreator.HasStanding = true;
            }

            // Check if we have results
            if (this.HasResults(matches))
            {
                xmlDataCreator.Result = true;
            }

            // Creating the document type node
            var filename = xmlDataCreator.CreateFileNameBase(organization, tournament, this.DataParams.DateStart);

            // now let's check if we have this document in the database
            var repository = new DocumentRepository();
            var version = repository.GetDocumentVersionByTournamentId(tournament.Id);

            var fullFilename = string.Empty;

            var documentType = DocumentType.Result;
            if (this.DataParams.Results)
            {
                if (xmlDataCreator.HasStanding && xmlDataCreator.HasMatchFacts)
                {
                    fullFilename = filename + "_Fakta_V" + version + ".xml";
                    documentType = DocumentType.MatchFact;
                }
                else if (xmlDataCreator.HasStanding && !xmlDataCreator.HasMatchFacts)
                {
                    fullFilename = filename + "_ResTab_V" + version + ".xml";
                    documentType = DocumentType.Standing;
                }
                else if (!xmlDataCreator.HasStanding && !xmlDataCreator.HasMatchFacts)
                {
                    fullFilename = filename + "_Res_V" + version + ".xml";
                    documentType = DocumentType.Result;
                }
            }
            else if (this.DataParams.Results == false)
            {
                fullFilename = filename + "_Sched_V" + version + ".xml";
                documentType = DocumentType.Schedule;
            }

            var document = new Document
                               {
                                   Created = DateTime.Today, 
                                   Filename = filename, 
                                   SportId = this.DataParams.SportId, 
                                   TournamentId = tournament.Id, 
                                   Version = version, 
                                   FullName = fullFilename, 
                                   DocumentType = documentType
                               };

            if (version == 1)
            {
                repository.InsertOne(document);
            }
            else
            {
                repository.Update(document);
            }

            xmlDataCreator.CreateFileNode(document);

            xmlDataCreator.CreateDocumentTypeNode();

            // Creating the document node
            var doc = xmlDataCreator.CreateDocument();

            foreach (var outputFolder in this.DataParams.FileOutputFolder)
            {
                var path = outputFolder;

                if (this.DataParams.FileOutputFolder == null)
                {
                    if (this.DataParams.SportId == 4)
                    {
                        path = (from o in this.DataParams.FileOutputFolders where o.DisciplineId == this.DataParams.DisciplineId select o.OutputPath).Single();
                    }
                    else
                    {
                        path = (from o in this.DataParams.FileOutputFolders where o.SportId == this.DataParams.SportId select o.OutputPath).Single();
                    }
                }

                xmlDataCreator.WriteDocument(doc, path);
            }
        }

        /// <summary>
        /// The get age category.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see cref="AgeCategory"/>.
        /// </returns>
        private AgeCategory GetAgeCategory(int tournamentId)
        {
            var ageCategoryRepository = new AgeCategoryRepository();
            var ageCategory = ageCategoryRepository.GetAgeCategoryByTournamentId(tournamentId, this.DataParams.SportId);
            return ageCategory ?? ageCategoryRepository.GetAgeCategoryDefinitionByTournamentId(tournamentId, this.DataParams.SportId);
        }

        /// <summary>
        /// The get tournament matches.
        /// </summary>
        /// <param name="applicationKey">
        /// The application key.
        /// </param>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Match> GetTournamentMatches(string applicationKey, string tournamentId)
        {
            var facade = new ProfixioFacade();
            var matches = facade.GetRemoteMatchesByTournamentId(applicationKey, tournamentId);

            return matches;
        }

        /// <summary>
        /// The get customer tournaments.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Tournament> GetCustomerTournaments()
        {
            Logger.Info("Getting tournaments from database");

            try
            {
                var tournamentRepository = new TournamentRepository();

                return tournamentRepository.GetCustomerTournaments(this.DataParams.SportId, this.DataParams.SeasonId);
            }
            catch (Exception exception)
            {
                Logger.Error("An error occured while getting tournaments!");
                Logger.Error(exception);

                return new List<Tournament>();
            }
        }

        /**
         * This is what is needed to receive results form Norway Cup
         * 1) Customers: We need to know which customers shall have results
         * 2) Tournaments: We also need to know which tournaments the customer have subscribed to
         * 3) 
         * 
         * 
         */
    }
}