﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProfixioJobBuilder.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The profixio job builder.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.Profixio
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;

    using log4net;

    using NTB.SportsData.Classes.Classes;
    using NTB.SportsData.Classes.Enums;

    using Quartz;

    /// <summary>
    /// The profixio job builder.
    /// </summary>
    internal class ProfixioJobBuilder : IJob
    {
        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ProfixioJobBuilder));

        /// <summary>
        /// Initializes static members of the <see cref="ProfixioJobBuilder"/> class.
        /// </summary>
        static ProfixioJobBuilder()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository()
                     .Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        /// <summary>
        /// The execute.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public void Execute(IJobExecutionContext context)
        {
            Logger.Info("Executing JobExecutionContext: " + context.JobDetail.Description);
            var outputFolder = new List<string>();

            var createXml = false;

            var insertDatabase = false;

            var results = false;

            var dateOffset = DateTime.Now;

            var duration = 0;

            var sportId = 0;

            var federationId = 0;

            var tournamentId = 0;

            var seasonId = 0;

            var operationMode = OperationMode.Gatherer;

            // Mapping information sent from Component
            var dataMap = context.JobDetail.JobDataMap;

            // Populate variables to be used to 
            try
            {
                outputFolder.Add(dataMap.GetString("OutputFolder"));
                Logger.Debug("OutputFolder: " + outputFolder);

                insertDatabase = dataMap.GetBoolean("InsertDatabase");
                Logger.Debug(insertDatabase ? "InsertDatabase: true" : "InsertDatabase: false");

                var scheduleType = dataMap.GetString("ScheduleType");
                Logger.Debug("ScheduleType: " + scheduleType);

                results = dataMap.GetBoolean("Results");
                Logger.Debug(results ? "Results: true" : "Results: false");

                var scheduleMessage = dataMap.GetBoolean("ScheduleMessage");
                if (results && scheduleMessage == false)
                {
                    scheduleMessage = true;
                }

                Logger.Debug(scheduleMessage ? "ScheduleMessage: true" : "ScheduleMessage: false");

                duration = dataMap.GetInt("Duration");
                Logger.Debug("Duration: " + duration);

                createXml = dataMap.GetBoolean("CreateXML");
                Logger.Debug(createXml ? "CreateXML: true" : "CreateXML: false");

                sportId = dataMap.GetInt("SportId");

                federationId = dataMap.GetInt("FederationId");

                tournamentId = dataMap.GetInt("TournamentId");

                seasonId = dataMap.GetInt("TournamentId");

                if (dataMap.Keys.Contains("DateOffset"))
                {
                    dateOffset = dataMap.GetDateTime("DateOffset");
                    Logger.Debug("DateOffset: " + dateOffset.ToShortDateString());
                }

                operationMode = (OperationMode)Enum.Parse(typeof(OperationMode), dataMap.GetString("OperationMode"));
            }
            catch (Exception exception)
            {
                Logger.Debug(exception);
                Logger.Debug(exception.StackTrace);
            }

            // Creating the DataFileCreator object
            var dataFileParams = new DataFileParams
                                     {
                                         FileOutputFolder = outputFolder, 
                                         SportId = sportId, 
                                         FederationId = federationId, 
                                         TournamentId = tournamentId, 
                                         SeasonId = seasonId
                                     };

            if (insertDatabase)
            {
                Logger.Debug("We shall insert data in the database");

                // creator.UpdateDatabase = true;
            }

            // This parameter tells us if we are to add results-tags to the XML
            dataFileParams.Results = false;
            dataFileParams.PushResults = false;
            if (results)
            {
                Logger.Debug("We shall also render result");
                dataFileParams.Results = true;
                dataFileParams.PushResults = false;
            }

            Logger.Debug("Duration: " + duration);
            if (duration > 0)
            {
                Logger.Debug("We shall create a longer period of matches");
                dataFileParams.Duration = duration;

                // Creating the date start
                dataFileParams.DateStart = DateTime.Today;

                if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                {
                    dataFileParams.DateStart = DateTime.Parse("2011-05-16");
                }

                dataFileParams.DateEnd = dataFileParams.DateStart.AddDays(duration);
            }
            else if (duration < 0)
            {
                var datestart = DateTime.Now.AddDays(duration);

                dataFileParams.DateStart = datestart;

                dataFileParams.DateEnd = DateTime.Today;
            }
            else
            {
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                {
                    dataFileParams.DateStart = DateTime.Parse(ConfigurationManager.AppSettings["profixiotestingdate"]);

                    dataFileParams.DateEnd = DateTime.Parse(ConfigurationManager.AppSettings["profixiotestingdate"]);
                }
                else
                {
                    dataFileParams.DateStart = DateTime.Today;
                    dataFileParams.DateEnd = DateTime.Today;

                    if (dateOffset != DateTime.Today)
                    {
                        dataFileParams.DateStart = dateOffset;
                        dataFileParams.DateEnd = dateOffset;
                    }

                    Logger.Debug("Date Start: " + dataFileParams.DateStart.ToShortDateString());
                    Logger.Debug("Date End: " + dataFileParams.DateEnd.ToShortDateString());
                }
            }

            Logger.Debug("We are about to create XML File");
            dataFileParams.CreateXml = createXml;

            var creator = new ProfixioDataFileCreator()
                              {
                                  DataParams = dataFileParams
                              };

            // Configuring the creator object
            if (!creator.Configure())
            {
                return;
            }

            var applicationKey = ConfigurationManager.AppSettings["ProfixioApplicationKey"];

            if (operationMode == OperationMode.Gatherer)
            {
                creator.CreateResults(applicationKey, tournamentId);
            }
            else
            {
                var dataFileCreator = new ProfixioDataFileCreator()
                                          {
                                              DataParams = dataFileParams
                                          };

                Logger.Debug("Running CreateResults method");
                dataFileCreator.CreateResults(applicationKey, tournamentId);
            }
        }
    }
}