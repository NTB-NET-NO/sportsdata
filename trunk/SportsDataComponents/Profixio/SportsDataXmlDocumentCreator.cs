// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NffXmlDocument.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The nff xml document.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using log4net;
using NTB.SportsData.Classes.Classes;

namespace NTB.SportsData.Components.Profixio
{
    /// <summary>
    ///     The nff xml document.
    /// </summary>
    public class SportsDataXmlDocumentCreator
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SportsDataXmlDocumentCreator));

        /// <summary>
        ///     The _nff datafile creator.
        /// </summary>
        /// <summary>
        ///     Initializes a new instance of the <see cref="SportsDataXmlDocumentCreator" /> class.
        /// </summary>
        public SportsDataXmlDocumentCreator()
        {
            this.Standing = false;
            this.MatchFacts = false;
            this.RenderResult = false;
        }

        /// <summary>
        ///     Gets or sets a value indicating whether standing.
        /// </summary>
        public bool Standing { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether match facts.
        /// </summary>
        public bool MatchFacts { get; set; }

        public bool RenderResult { get; set; }

        private XmlNode StadiumNode { get; set; }

        private XmlNode AgeCategoryNode { get; set; }

        private XmlNode HeaderNode { get; set; }

        private XmlNode CustomerNode { get; set; }

        private XmlNode OrganizationNode { get; set; }

        private XmlNode SportNode { get; set; }

        private XmlNode FileNode { get; set; }

        private XmlNode MatchNode { get; set; }

        private XmlNode PlayerNode { get; set; }

        private Dictionary<int, XmlDocument> HomeTeamNode { get; set; }

        private Dictionary<int, XmlDocument> AwayTeamNode { get; set; }

        private XmlNode IncidentNode { get; set; }

        private XmlNode TournamentNode { get; set; }

        private XmlNode RefereeNode { get; set; }

        private XmlNode TournamentTableNode { get; set; }

        private XmlNode DocumentTypeNode { get; set; }

        public bool Result { get; set; }

        public bool HasMatchFacts { get; set; }

        public bool HasStanding { get; set; }

        public void CreateTournamentNode(Tournament tournament, string tournamentRound, DateTime matchDate)
        {
            // xmlRoot.SetAttribute("Type", _nffDatafileCreator.RenderResult ? "resultat" : "terminliste");
            var doc = new XmlDocument();
            // Create Tournament-MetaInfo node
            var xmlElement = doc.CreateElement("Tournament");
            var xmlTournamentMetaInfo = doc.CreateElement("Tournament-MetaInfo");
            xmlTournamentMetaInfo.SetAttribute("Id", tournament.Id.ToString());
            xmlTournamentMetaInfo.SetAttribute("Name", tournament.Name.Trim());
            xmlTournamentMetaInfo.SetAttribute("Number", tournament.Number.ToString());
            xmlTournamentMetaInfo.SetAttribute("Division", tournament.Division.ToString());

            var documentId = "T" + tournament.Number + "R" + tournamentRound + "_"
                             + matchDate.Year
                             + matchDate.Month.ToString()
                                 .PadLeft(2, '0')
                             + matchDate.Day.ToString()
                                 .PadLeft(2, '0');

            xmlElement.SetAttribute("doc-id", documentId);
            xmlElement.SetAttribute("Type", this.RenderResult ? "resultat" : "terminliste");
            xmlElement.AppendChild(xmlTournamentMetaInfo);

            doc.AppendChild(xmlElement);

            this.TournamentNode = doc;
        }

        public void CreateHeaderNode()
        {
            var doc = new XmlDocument();

            var xmlElement = doc.CreateElement("Header");

            doc.AppendChild(xmlElement);

            this.HeaderNode = doc;
        }

        public void CreateAgeCategoryNode(AgeCategory ageCategory, Tournament tournament)
        {
            var doc = new XmlDocument();
            var xmlAgeCategory = doc.CreateElement("AgeCategory");

            if (ageCategory != null)
            {
                if (!tournament.NationalTeamTournament)
                {
                    switch (ageCategory.Id)
                    {
                        case 18:
                        case 21:
                            xmlAgeCategory.SetAttribute("min-age", ageCategory.FromAge.ToString());
                            xmlAgeCategory.SetAttribute("max-age", ageCategory.ToAge.ToString());
                            xmlAgeCategory.SetAttribute("name", ageCategory.Name);
                            xmlAgeCategory.SetAttribute("id", ageCategory.Id.ToString());
                            xmlAgeCategory.InnerText = "senior";
                            break;

                        default:
                            xmlAgeCategory.SetAttribute("min-age", ageCategory.FromAge.ToString());
                            xmlAgeCategory.SetAttribute("max-age", ageCategory.ToAge.ToString());
                            xmlAgeCategory.SetAttribute("name", ageCategory.Name);
                            xmlAgeCategory.SetAttribute("id", ageCategory.Id.ToString());
                            xmlAgeCategory.InnerText = "aldersbestemt";
                            break;
                    }
                }
                else
                {
                    xmlAgeCategory.SetAttribute("min-age", ageCategory.FromAge.ToString());
                    xmlAgeCategory.SetAttribute("max-age", ageCategory.ToAge.ToString());
                    xmlAgeCategory.SetAttribute("name", ageCategory.Name);
                    xmlAgeCategory.SetAttribute("id", ageCategory.Id.ToString());
                    xmlAgeCategory.InnerText = ageCategory.Name;
                }
            }

            doc.AppendChild(xmlAgeCategory);
            this.AgeCategoryNode = doc;
        }

        public void CreateCustomerNode(List<Customer> customers)
        {
            var doc = new XmlDocument();
            var xmlCustomers = doc.CreateElement("Customers");

            // Now we shall add customers
            if (customers == null)
            {
                return;
            }

            foreach (var customer in customers)
            {
                var xmlElement = doc.CreateElement("Customer");
                var xmlCustomerName = doc.CreateElement("CustomerName");
                xmlCustomerName.SetAttribute("Id", customer.Id.ToString());
                xmlCustomerName.SetAttribute("RemoteCustomerId", customer.RemoteCustomerId.ToString());
                var xmlText = doc.CreateTextNode(customer.Name);
                xmlCustomerName.AppendChild(xmlText);
                xmlElement.AppendChild(xmlCustomerName);
                xmlCustomers.AppendChild(xmlElement);
            }

            doc.AppendChild(xmlCustomers);

            this.CustomerNode = doc;
        }

        public void CreateOrganizationNode(Federation organization)
        {
            var doc = new XmlDocument();
            var xmlElement = doc.CreateElement("Organisation");
            xmlElement.SetAttribute("Name", organization.NameShort);
            xmlElement.InnerText = organization.Name;

            doc.AppendChild(xmlElement);

            this.OrganizationNode = doc;
        }

        internal void CreateSportNode(Federation organization)
        {
            var doc = new XmlDocument();
            var xmlElement = doc.CreateElement("Sport");
            xmlElement.InnerText = organization.Sport;

            doc.AppendChild(xmlElement);

            this.SportNode = doc;
        }

        public string CreateFileNameBase(Federation organization, Tournament tournament, DateTime dateTime)
        {
            var fileDate = dateTime.Year + dateTime.Month.ToString()
                .PadLeft(2, '0') + dateTime.Day.ToString()
                    .PadLeft(2, '0');

            return "NTB_SportsData_" + fileDate + "_" + organization.NameShort + "_T" + tournament.Id;
        }

        private string CreateDateTimeString(DateTime dateTime)
        {
            var monthNumber = dateTime.Month.ToString();
            if (monthNumber.Length == 1)
            {
                monthNumber = "0" + dateTime.Month;
            }

            var dayNumber = dateTime.Day.ToString();
            if (dayNumber.Length == 1)
            {
                dayNumber = "0" + dateTime.Day;
            }

            return dateTime.Year + "-" + monthNumber + "-" + dayNumber + "T" +
                   dateTime.ToShortTimeString() + ":00";
        }

        internal void CreateDocumentTypeNode()
        {
            var doc = new XmlDocument();
            var xmlElement = doc.CreateElement("DocumentType");

            if (!this.HasMatchFacts && !this.HasStanding)
            {
                xmlElement.SetAttribute("type", "resultatmelding");
            }
            else if (this.HasMatchFacts)
            {
                xmlElement.SetAttribute("type", "kampfakta");
            }
            else
            {
                xmlElement.SetAttribute("type", "sportsresults");
            }


            doc.AppendChild(xmlElement);

            this.DocumentTypeNode = doc;
        }


        internal void CreateFileNode(Document document)
        {
            var doc = new XmlDocument();
            var xmlElement = doc.CreateElement("File");
            var xmlElementName = doc.CreateElement("Name");
            xmlElementName.SetAttribute("FullName", document.FullName);
            xmlElementName.SetAttribute("Type", "xml");
            xmlElementName.InnerText = document.Filename;

            var xmlCreatedDateTime = doc.CreateElement("Created");
            xmlCreatedDateTime.InnerText = this.CreateDateTimeString(document.Created);

            var xmlVersion = doc.CreateElement("Version");
            xmlVersion.InnerText = document.Version.ToString();

            xmlElement.AppendChild(xmlElementName);
            xmlElement.AppendChild(xmlCreatedDateTime);
            xmlElement.AppendChild(xmlVersion);
            doc.AppendChild(xmlElement);

            this.FileNode = doc;
        }

        internal void CreateMatchNode(List<Match> matches)
        {
            var doc = new XmlDocument();
            var xmlElement = doc.CreateElement("Matches");
            foreach (var match in matches)
            {
                // Creating the start time
                var timeString = string.Empty;
                if (match.MatchDate != null)
                {
                    timeString = match.MatchDate.Value.ToShortTimeString()
                        .Replace(":", string.Empty);
                }

                match.MatchStartTime = Convert.ToInt32(timeString);

                // match.MatchStartTime = Convert.ToInt32(match.MatchDate.ToShortDateString().Substring(0,4).Replace(":", string.Empty));

                var xmlMatchElement = doc.CreateElement("Match");
                xmlMatchElement.SetAttribute("Id", match.Id.ToString());
                if (match.MatchDate != null)
                {
                    xmlMatchElement.SetAttribute("startdate", match.MatchDate.Value.ToShortDateString());
                }
                xmlMatchElement.SetAttribute("starttime", match.MatchStartTime.ToString()
                    .PadLeft(4, '0'));

                var xmlMatchResultFinal = doc.CreateElement("MatchResult");
                xmlMatchResultFinal.SetAttribute("type", "Sluttresultat");
                xmlMatchResultFinal.SetAttribute("hometeam", match.HomeTeamGoals.ToString());
                xmlMatchResultFinal.SetAttribute("awayteam", match.AwayTeamGoals.ToString());
                xmlMatchElement.AppendChild(xmlMatchResultFinal);

                var xmlMatchResultPause = doc.CreateElement("MatchResult");
                xmlMatchResultPause.SetAttribute("type", "Pauseresultat");
                xmlMatchResultPause.SetAttribute("hometeam", match.PartialResult);
                xmlMatchElement.AppendChild(xmlMatchResultPause);

                xmlElement.AppendChild(xmlMatchElement);

                this.CreateHomeTeamNode(match);
                this.CreateAwayTeamNode(match);
            }

            doc.AppendChild(xmlElement);

            this.MatchNode = doc;
        }

        internal void CreatePlayerNode(List<Player> list)
        {
            var doc = new XmlDocument();

            var xmlElement = doc.CreateElement("Players");
            foreach (var player in list)
            {
                var xmlPlayer = doc.CreateElement("Player");
                xmlPlayer.SetAttribute("id", player.Id.ToString());
                xmlPlayer.SetAttribute("position", player.Position);

                var xmlPlayerName = doc.CreateElement("PlayerName");
                xmlPlayerName.SetAttribute("FirstName", player.FirstName);
                xmlPlayerName.SetAttribute("LastName", player.LastName);
                xmlPlayerName.InnerText = player.FirstName + " " + player.LastName;
                xmlPlayer.AppendChild(xmlPlayerName);
                xmlElement.AppendChild(xmlPlayer);
            }

            doc.AppendChild(xmlElement);
            this.PlayerNode = doc;
        }

        internal void CreateHomeTeamNode(Match match)
        {
            var doc = new XmlDocument();

            var xmlElement = doc.CreateElement("HomeTeam");
            xmlElement.SetAttribute("Id", match.HomeTeamId.ToString());

            var xmlName = doc.CreateElement("Name");
            xmlName.InnerText = match.HomeTeamName.Trim();

            var xmlGoals = doc.CreateElement("Totalgoals");
            xmlGoals.InnerText = match.HomeTeamGoals.ToString();

            xmlElement.AppendChild(xmlName);
            xmlElement.AppendChild(xmlGoals);

            doc.AppendChild(xmlElement);

            if (this.HomeTeamNode == null)
            {
                Logger.Debug("Creating a new HomeTeamNode for match " + match.Id);
                this.HomeTeamNode = new Dictionary<int, XmlDocument>();
            }

            if (this.HomeTeamNode.Keys.Contains(match.Id))
            {
                Logger.Debug("Match " + match.Id + "is already in the dictionary");
                return;
            }
            Logger.Debug("Adding home team " + match.HomeTeamName + " to HomeTeamNode-Dictionary " + match.Id);
            this.HomeTeamNode.Add(match.Id, doc);
        }

        internal void CreateStadiumNode(VenueExtended venueExtended)
        {
            var doc = new XmlDocument();
            var xmlElement = doc.CreateElement("Stadium");
            if (venueExtended != null && venueExtended.Id > 0)
            {
                xmlElement.InnerText = venueExtended.Name.Trim();
            }

            doc.AppendChild(xmlElement);

            this.StadiumNode = doc;
        }

        internal void CreateAwayTeamNode(Match match)
        {
            var doc = new XmlDocument();

            var xmlElement = doc.CreateElement("AwayTeam");
            xmlElement.SetAttribute("Id", match.AwayTeamId.ToString());

            var xmlName = doc.CreateElement("Name");
            xmlName.InnerText = match.AwayTeamName.Trim();

            var xmlGoals = doc.CreateElement("Totalgoals");
            xmlGoals.InnerText = match.AwayTeamGoals.ToString();

            xmlElement.AppendChild(xmlName);
            xmlElement.AppendChild(xmlGoals);

            doc.AppendChild(xmlElement);

            if (this.AwayTeamNode == null)
            {
                Logger.Debug("Creating a new AwayTeamNode for match " + match.Id);
                this.AwayTeamNode = new Dictionary<int, XmlDocument>();
            }

            if (this.AwayTeamNode.Keys.Contains(match.Id))
            {
                Logger.Debug("Match " + match.Id + "is already in the dictionary");
                return;
            }
            Logger.Debug("Adding home team " + match.AwayTeamName + " to HomeTeamNode-Dictionary " + match.Id);
            this.AwayTeamNode.Add(match.Id, doc);
        }

        internal void CreateIncidentNode(List<MatchIncident> list)
        {
            var doc = new XmlDocument();

            var xmlElement = doc.CreateElement("Events");

            foreach (var incident in list)
            {
                var xmlEvent = doc.CreateElement("Event");
                xmlEvent.SetAttribute("id", incident.IncidentTypeId.ToString());
                xmlEvent.SetAttribute("teamid", incident.TeamId.ToString());
                xmlEvent.SetAttribute("value", incident.IncidentType);
                xmlEvent.SetAttribute("minute", incident.Time.ToString());

                var xmlPlayer = doc.CreateElement("Player");
                xmlPlayer.SetAttribute("id", incident.Player.Id.ToString());
                var xmlPlayerName = doc.CreateElement("Player");
                xmlPlayerName.SetAttribute("FirstName", incident.Player.FirstName);
                xmlPlayerName.SetAttribute("LastName", incident.Player.LastName);
                var fullname = incident.Player.FirstName + " " + incident.Player.LastName;
                xmlPlayerName.SetAttribute("full", fullname);
                xmlPlayerName.InnerText = fullname;
            }

            doc.AppendChild(xmlElement);
            this.IncidentNode = doc;
        }

        internal void CreateRefereeNode(List<Referee> referees)
        {
            var doc = new XmlDocument();

            var xmlElement = doc.CreateElement("Referees");

            foreach (var referee in referees)
            {
                var xmlReferee = doc.CreateElement("Referee");
                xmlReferee.SetAttribute("Type", referee.RefereeType);
                xmlReferee.InnerText = referee.RefereeName;

                xmlElement.AppendChild(xmlReferee);
            }

            doc.AppendChild(xmlElement);
            this.RefereeNode = doc;
        }


        internal XmlDocument CreateDocument()
        {
            var doc = new XmlDocument();
            var xmlElement = doc.CreateElement("SportsData");
            XmlNode xmlTournament = null;
            XmlNode xmlHeader = null;


            if (this.TournamentNode.SelectSingleNode("Tournament") != null)
            {
                xmlTournament = doc.ImportNode(this.TournamentNode.SelectSingleNode("Tournament"), true);

                xmlElement.AppendChild(xmlTournament);
            }

            if (this.HeaderNode.SelectSingleNode("Header") != null)
            {
                xmlHeader = doc.ImportNode(this.HeaderNode.SelectSingleNode("Header"), true);
                if (xmlTournament != null) xmlTournament.AppendChild(xmlHeader);
            }

            if (this.OrganizationNode.SelectSingleNode("Organisation") != null)
            {
                var xmlOrganization = doc.ImportNode(this.OrganizationNode.SelectSingleNode("Organisation"), true);

                if (xmlHeader != null) xmlHeader.AppendChild(xmlOrganization);
            }
            if (this.AgeCategoryNode.SelectSingleNode("AgeCategory") != null)
            {
                var xmlAgeCategoryNode = doc.ImportNode(this.AgeCategoryNode.SelectSingleNode("AgeCategory"), true);
                if (xmlHeader != null) xmlHeader.AppendChild(xmlAgeCategoryNode);
            }

            if (this.CustomerNode.SelectSingleNode("Customers") != null)
            {
                var xmlCustomerNode = doc.ImportNode(this.CustomerNode.SelectSingleNode("Customers"), true);
                if (xmlHeader != null) xmlHeader.AppendChild(xmlCustomerNode);
            }

            if (this.SportNode.SelectSingleNode("Sport") != null)
            {
                var xmlSportNode = doc.ImportNode(this.SportNode.SelectSingleNode("Sport"), true);
                if (xmlHeader != null) xmlHeader.AppendChild(xmlSportNode);
            }

            var matchDoc = new XmlDocument();

            if (this.MatchNode.SelectSingleNode("Matches") != null)
            {
                var xmlMatchNode = matchDoc.ImportNode(this.MatchNode.SelectSingleNode("Matches"), true);
                matchDoc.AppendChild(xmlMatchNode);
            }

            var xmlMatchesElement = doc.CreateElement("Matches");
            var matchNodeList = matchDoc.SelectNodes("/Matches/Match");

            if (matchNodeList != null)
            {
                foreach (XmlNode mNode in matchNodeList)
                {
                    var matchId = Convert.ToInt32(mNode.SelectSingleNode("@Id")
                        .Value);
                    var foo = mNode.Attributes;
                    var xmlMatch = doc.CreateElement("Match");
                    xmlMatch.SetAttribute("Id", matchId.ToString());
                    xmlMatch.SetAttribute("startdate", mNode.SelectSingleNode("@startdate")
                        .Value);
                    xmlMatch.SetAttribute("starttime", mNode.SelectSingleNode("@starttime")
                        .Value);

                    if (this.StadiumNode != null && this.StadiumNode.SelectSingleNode("Stadium") != null)
                    {
                        var xmlStadiumNode = doc.ImportNode(this.StadiumNode.SelectSingleNode("Stadium"), true);
                        xmlMatch.AppendChild(xmlStadiumNode);
                    }

                    var test = from h in this.HomeTeamNode
                        where h.Key == matchId
                        select h.Value;
                    if (!test.Any())
                    {
                        continue;
                    }


                    XmlNode homeNode = (from h in this.HomeTeamNode
                        where h.Key == matchId
                        select h.Value).Single();


                    if (homeNode.SelectSingleNode("HomeTeam") != null)
                    {
                        var xmlHomeTeamNode = doc.ImportNode(homeNode.SelectSingleNode("HomeTeam"), true);
                        xmlMatch.AppendChild(xmlHomeTeamNode);
                    }

                    XmlNode awayNode = (from a in this.AwayTeamNode
                        where a.Key == matchId
                        select a.Value).Single();

                    if (awayNode.SelectSingleNode("AwayTeam") != null)
                    {
                        var xmlAwayTeamNode = doc.ImportNode(awayNode.SelectSingleNode("AwayTeam"), true);
                        xmlMatch.AppendChild(xmlAwayTeamNode);
                    }

                    if (this.RefereeNode != null && this.RefereeNode.SelectSingleNode("Referees") != null)
                    {
                        var xmlRefereeNode = doc.ImportNode(this.RefereeNode.SelectSingleNode("Referees"), true);
                        xmlMatch.AppendChild(xmlRefereeNode);
                    }

                    if (this.PlayerNode != null)
                    {
                        if (this.PlayerNode.SelectSingleNode("Players") != null)
                        {
                            var xmlPlayerNode = doc.ImportNode(this.PlayerNode.SelectSingleNode("Players"), true);
                            xmlMatch.AppendChild(xmlPlayerNode);
                        }
                    }

                    if (this.IncidentNode != null)
                    {
                        if (this.IncidentNode.SelectSingleNode("Events") != null)
                        {
                            var xmlIncidentsNode = doc.ImportNode(this.IncidentNode.SelectSingleNode("Events"), true);
                            xmlMatch.AppendChild(xmlIncidentsNode);
                        }
                    }

                    xmlMatchesElement.AppendChild(xmlMatch);
                }
            }


            doc.AppendChild(xmlElement);


            // XmlNode matchNode = doc.ImportNode(matchDoc.SelectSingleNode("Matches"), true);
            var tnode = doc.SelectSingleNode("/SportsData/Tournament");
            if (tnode != null)
            {
                tnode.AppendChild(xmlMatchesElement);
            }

            var headerNode = doc.SelectSingleNode("/SportsData/Tournament/Header");
            if (headerNode != null)
            {
                if (this.DocumentTypeNode.SelectSingleNode("DocumentType") != null)
                {
                    var documentTypeNode = doc.ImportNode(this.DocumentTypeNode.SelectSingleNode("DocumentType"), true);
                    headerNode.AppendChild(documentTypeNode);
                }

                if (this.FileNode.SelectSingleNode("File") != null)
                {
                    var fileNode = doc.ImportNode(this.FileNode.SelectSingleNode("File"), true);
                    headerNode.AppendChild(fileNode);
                }
            }

            // node.OwnerDocument.ImportNode(matchNode, true);

            if (this.TournamentTableNode != null)
            {
                var xmlTournamentStandingsNode = doc.ImportNode(this.TournamentTableNode.SelectSingleNode("Standings"),
                    true);
                if (xmlTournament != null) xmlTournament.AppendChild(xmlTournamentStandingsNode);
            }

            return doc;
        }

        internal void WriteDocument(XmlDocument doc, string path)
        {
            var node = doc.SelectSingleNode("/SportsData/Tournament/Header/File/Name/@FullName");
            var filename = "NTB_SportsData_" + DateTime.Today.Year + DateTime.Today.Month + DateTime.Today.Day;
            if (node != null)
            {
                filename = node.Value;
            }

            var fullname = path + @"\" + filename;
            // Now we shall get path

            doc.Save(fullname);
        }


        internal void CreateTournamentStandingNode(List<TournamentTable> tournamentTables)
        {
            var doc = new XmlDocument();
            var xmlElement = doc.CreateElement("Standings");
            var counter = 0;
            foreach (var tournamentTable in tournamentTables)
            {
                counter++;
                var xmlTeam = doc.CreateElement("Team");
                xmlTeam.SetAttribute("Id", tournamentTable.TeamId.ToString());
                xmlTeam.SetAttribute("TablePosition", counter.ToString());

                var xmlTeamName = doc.CreateElement("Name");
                xmlTeamName.InnerText = tournamentTable.TeamName.Trim();
                xmlTeam.AppendChild(xmlTeamName);

                var xmlPoints = doc.CreateElement("Points");
                xmlPoints.SetAttribute("Total", tournamentTable.Points.ToString());

                var xmlHomePoints = doc.CreateElement("HomePoints");
                xmlHomePoints.InnerText = tournamentTable.HomePoints.ToString();

                var xmlAwayPoints = doc.CreateElement("AwayPoints");
                xmlAwayPoints.InnerText = tournamentTable.AwayPoints.ToString();

                xmlPoints.AppendChild(xmlHomePoints);
                xmlPoints.AppendChild(xmlAwayPoints);
                xmlTeam.AppendChild(xmlPoints);

                var xmlGoals = doc.CreateElement("Goals");

                var goalsplit = tournamentTable.DiffString.Split('-');

                xmlGoals.SetAttribute("TotalScored", goalsplit[0].Trim());
                xmlGoals.SetAttribute("TotalAgainst", goalsplit[1].Trim());

                var xmlHomeGoals = doc.CreateElement("Home");
                xmlHomeGoals.SetAttribute("Scored", tournamentTable.NoHomeGoals.ToString());
                xmlHomeGoals.SetAttribute("Against", tournamentTable.NoHomeAgainst.ToString());

                var xmlAwayGoals = doc.CreateElement("Away");
                xmlAwayGoals.SetAttribute("Scored", tournamentTable.NoAwayGoals.ToString());
                xmlAwayGoals.SetAttribute("Against", tournamentTable.NoAwayAgainst.ToString());

                xmlGoals.AppendChild(xmlHomeGoals);
                xmlGoals.AppendChild(xmlAwayPoints);
                xmlTeam.AppendChild(xmlGoals);

                var xmlMatches = doc.CreateElement("Matches");
                xmlMatches.SetAttribute("Total", tournamentTable.NoMatches.ToString());
                xmlMatches.SetAttribute("Won", tournamentTable.NoWins.ToString());
                xmlMatches.SetAttribute("Draw", tournamentTable.NoDraws.ToString());
                xmlMatches.SetAttribute("Lost", tournamentTable.NoLosses.ToString());

                var xmlHomeMatches = doc.CreateElement("HomeMatches");
                xmlHomeMatches.SetAttribute("Won", tournamentTable.NoHomeWins.ToString());
                xmlHomeMatches.SetAttribute("Draw", tournamentTable.NoHomeDraws.ToString());
                xmlHomeMatches.SetAttribute("Lost", tournamentTable.NoHomeLosses.ToString());
                xmlHomeMatches.SetAttribute("Total", tournamentTable.NoHomeMatches.ToString());

                var xmlAwayMatches = doc.CreateElement("AwayMatches");
                xmlAwayMatches.SetAttribute("Won", tournamentTable.NoAwayWins.ToString());
                xmlAwayMatches.SetAttribute("Draw", tournamentTable.NoAwayDraws.ToString());
                xmlAwayMatches.SetAttribute("Lost", tournamentTable.NoAwayLosses.ToString());
                xmlAwayMatches.SetAttribute("Total", tournamentTable.NoAwayMatches.ToString());

                xmlMatches.AppendChild(xmlHomeMatches);
                xmlMatches.AppendChild(xmlAwayMatches);
                xmlTeam.AppendChild(xmlMatches);

                xmlElement.AppendChild(xmlTeam);
            }

            doc.AppendChild(xmlElement);

            this.TournamentTableNode = doc;
        }
    }
}