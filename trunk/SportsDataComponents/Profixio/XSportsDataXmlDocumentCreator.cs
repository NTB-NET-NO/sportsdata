﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using NTB.SportsData.Classes.Classes;

namespace NTB.SportsData.Components.Profixio
{
    public class XSportsDataXmlDocumentCreator
    {
        private XmlElement _xmlMatches;
        private XmlElement _xmlTournament;

        public void CreateTournamentXmlStructure(Tournament tournament)
        {
            var xmlDoc = new XmlDocument();
            _xmlTournament = xmlDoc.CreateElement("tournament");

            xmlDoc.AppendChild(_xmlTournament);
        }

        public void CreateMatchXmlStructure(List<Match> matches)
        {
            var xmlDoc = new XmlDocument();
            _xmlMatches = xmlDoc.CreateElement("matches");
            foreach (var match in matches)
            {
                XmlElement xmlMatch = xmlDoc.CreateElement("match");
                xmlMatch.SetAttribute("id", match.Id.ToString());

                _xmlMatches.AppendChild(xmlMatch);
            }


            xmlDoc.AppendChild(_xmlMatches);
        }
    }
}
