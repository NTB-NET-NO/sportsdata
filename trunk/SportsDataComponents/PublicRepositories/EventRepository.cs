﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventRepository.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;

namespace NTB.SportsData.Components.PublicRepositories
{
    /// <summary>
    /// The event repository.
    /// </summary>
    internal class EventRepository : IRepository<SportEvent>, ISportEventDataMapper
    {
        #region Static Fields

        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(EventRepository));

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// Not emplemented
        /// </exception>
        public void Delete(SportEvent domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="SportEvent"/>.
        /// </returns>
        public SportEvent Get(int id)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                var command = new SqlCommand { Connection = sqlConnection, CommandText = "Service_CheckEvents" };

                command.Parameters.Add(new SqlParameter("@EventId", id));

                // Telling the system that this is a StoredProcedure
                command.CommandType = CommandType.StoredProcedure;

                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                // Creating the SQL-reader.
                SqlDataReader sqlDataReader = command.ExecuteReader();

                if (!sqlDataReader.HasRows)
                {
                    return null;
                }

                var sportEvent = new SportEvent();
                while (sqlDataReader.Read())
                {
                    sportEvent.ActivityId = Convert.ToInt32(sqlDataReader["ActivityId"]);
                    sportEvent.EventId = Convert.ToInt32(sqlDataReader["EventId"]);
                    sportEvent.EventOrganizerId = Convert.ToInt32(sqlDataReader["OrganizerId"]);
                    sportEvent.EventName = sqlDataReader["EventName"].ToString();
                    sportEvent.EventLocation = sqlDataReader["Location"].ToString();
                    sportEvent.EventDateStart = Convert.ToDateTime(sqlDataReader["DateStart"]);
                    sportEvent.EventDateEnd = Convert.ToDateTime(sqlDataReader["DateEnd"]);
                }

                return new SportEvent();
            }
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public IQueryable<SportEvent> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get events by org id and sport id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<SportEvent> GetEventsByOrgIdAndSportId(int orgId, int sportId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand
                                         {
                                             Connection = sqlConnection, 
                                             CommandText = "Service_GetEventsExtra"
                                         };

                    DateTime startDate = DateTime.Today.Date;
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                    {
                        startDate = Convert.ToDateTime(ConfigurationManager.AppSettings["niftestingdate"]);
                    }

                    string sqlFormattedDate = startDate.ToString("yyyy-MM-dd");

                    sqlCommand.Parameters.Add(new SqlParameter("@Today", sqlFormattedDate));

                    sqlCommand.Parameters.Add(new SqlParameter("@FederationId", orgId));

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                    // Telling the system that this is a StoredProcedure
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                        sqlConnection.Open();
                    }

                    // Creating the SQL-reader.
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    var sportEvents = new List<SportEvent>();
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            var sportEvent = new SportEvent
                                                 {
                                                     EventName = sqlDataReader["Name"].ToString(), 
                                                     EventId =
                                                         Convert.ToInt32(sqlDataReader["EventId"].ToString()), 
                                                     EventDateStart =
                                                         Convert.ToDateTime(
                                                             sqlDataReader["DateStart"].ToString()), 
                                                     EventDateEnd =
                                                         Convert.ToDateTime(
                                                             sqlDataReader["DateEnd"].ToString()), 
                                                     EventLocation = sqlDataReader["Location"].ToString(), 
                                                     EventTimeStart =
                                                         sqlDataReader["TimeStart"] == DBNull.Value
                                                             ? 0
                                                             : Convert.ToInt32(
                                                                 sqlDataReader["TimeStart"].ToString()), 
                                                     EventTimeEnd =
                                                         sqlDataReader["TimeEnd"] == DBNull.Value
                                                             ? 0
                                                             : Convert.ToInt32(
                                                                 sqlDataReader["TimeEnd"].ToString()), 
                                                     ActivityId =
                                                         Convert.ToInt32(
                                                             sqlDataReader["ActivityId"].ToString()), 
                                                     EventOrganizerId =
                                                         Convert.ToInt32(
                                                             sqlDataReader["OrganizerId"].ToString())
                                                 };

                            // Field2 = rdr.GetSqlInt32(Field2_Ordinal).ToNullableInt32()
                            sportEvents.Add(sportEvent);
                        }
                    }

                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }

                    return sportEvents;
                }
                catch (SqlException sqlexception)
                {
                    Logger.Error(sqlexception);
                    return new List<SportEvent>();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return new List<SportEvent>();
                }
            }
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        public void InsertAll(List<SportEvent> domainobject)
        {
            foreach (SportEvent sportEvent in domainobject)
            {
                this.InsertOne(sportEvent);
            }
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int InsertOne(SportEvent domainobject)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                Logger.Debug("We are Inserting!");

                // We shall update the event
                var sqlCommand = new SqlCommand { Connection = sqlConnection, CommandText = "Service_InsertEvent" };
                sqlCommand.Parameters.Add(new SqlParameter("@EventId", domainobject.EventId));
                sqlCommand.Parameters.Add(new SqlParameter("@ActivityId", domainobject.ActivityId));
                sqlCommand.Parameters.Add(
                    new SqlParameter(
                        "@OrganizerId", 
                        domainobject.EventOrganizerId =
                        domainobject.EventOrganizerId ?? domainobject.AdministrativeOrgId));
                sqlCommand.Parameters.Add(new SqlParameter("@EventName", domainobject.EventName));
                sqlCommand.Parameters.Add(
                    new SqlParameter(
                        "@Location", 
                        domainobject.EventLocation = domainobject.EventLocation ?? string.Empty));
                sqlCommand.Parameters.Add(new SqlParameter("@DateStart", domainobject.EventDateStart));
                sqlCommand.Parameters.Add(new SqlParameter("@DateEnd", domainobject.EventDateEnd));
                if (domainobject.EventTimeStart != null)
                {
                    sqlCommand.Parameters.Add(new SqlParameter("@TimeStart", domainobject.EventTimeStart));
                }

                if (domainobject.EventTimeEnd != null)
                {
                    sqlCommand.Parameters.Add(new SqlParameter("@TimeEnd", domainobject.EventTimeEnd));
                }

                // Telling the system that this is a StoredProcedure
                sqlCommand.CommandType = CommandType.StoredProcedure;

                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                sqlCommand.ExecuteNonQuery();

                if (sqlConnection.State != ConnectionState.Closed)
                {
                    sqlConnection.Close();
                }

                return 0;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        public void Update(SportEvent domainobject)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                Logger.Debug("We are updating!");

                // We shall update the event
                var sqlCommand = new SqlCommand { Connection = sqlConnection, CommandText = "Service_UpdateEvent" };
                sqlCommand.Parameters.Add(new SqlParameter("@EventId", domainobject.EventId));
                sqlCommand.Parameters.Add(new SqlParameter("@ActivityId", domainobject.ActivityId));
                sqlCommand.Parameters.Add(new SqlParameter("@OrganizerId", domainobject.EventOrganizerId));
                sqlCommand.Parameters.Add(new SqlParameter("@EventName", domainobject.EventName));
                sqlCommand.Parameters.Add(new SqlParameter("@Location", domainobject.EventLocation));
                sqlCommand.Parameters.Add(new SqlParameter("@DateStart", domainobject.EventDateStart));
                sqlCommand.Parameters.Add(new SqlParameter("@DateEnd", domainobject.EventDateEnd));
                sqlCommand.Parameters.Add(new SqlParameter("@TimeStart", domainobject.EventTimeStart));
                sqlCommand.Parameters.Add(new SqlParameter("@TimeEnd", domainobject.EventTimeEnd));

                // Telling the system that this is a StoredProcedure
                sqlCommand.CommandType = CommandType.StoredProcedure;

                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                sqlCommand.ExecuteNonQuery();

                if (sqlConnection.State != ConnectionState.Closed)
                {
                    sqlConnection.Close();
                }
            }
        }

        /// <summary>
        /// The update event set download.
        /// </summary>
        /// <param name="eventId">
        /// The event id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool UpdateEventSetDownload(int eventId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                        sqlConnection.Open();
                    }

                    var command = new SqlCommand { Connection = sqlConnection, CommandText = "Service_SetDownload" };

                    command.Parameters.Add(new SqlParameter("@EventId", eventId));

                    // Telling the system that this is a StoredProcedure
                    command.CommandType = CommandType.StoredProcedure;

                    if (command.ExecuteNonQuery() == 1)
                    {
                        if (sqlConnection.State != ConnectionState.Closed)
                        {
                            sqlConnection.Close();
                        }

                        return true;
                    }

                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }

                    return false;
                }
                catch (SqlException exception)
                {
                    Logger.Error(exception);
                    return false;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return false;
                }
            }
        }

        #endregion
    }
}