﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;

namespace NTB.SportsData.Components.PublicRepositories
{
    public class AgeCategoryRepository : IAgeCategoryDataMapper
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(AgeCategoryRepository));

        public AgeCategory GetAgeCategoryByIdAndSportId(int ageCategoryId, int sportId)
        {
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("[Service_GetAgeCategoryByCategoryAndSportIdId]", sqlConnection);

                    sqlCommand.Parameters.Add(
                        new SqlParameter("@AgeCategoryId", ageCategoryId));

                    sqlCommand.Parameters.Add(
                        new SqlParameter("@SportId", sportId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();


                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }
                    else
                    {
                        var ageCategory = new AgeCategory();

                        // @todo: Consider if this shall read values from the database or not... 
                        // @todo: In hindsight the code below do seem dumb...
                        while (sqlDataReader.Read())
                        {
                            if (sqlDataReader["AgeCategoryDefinition"].ToString()
                                .ToLower() == "senior")
                            {
                                ageCategory.Id = 18;
                                ageCategory.Name = sqlDataReader["AgeCategoryDefinition"].ToString();
                                ageCategory.SportId = sportId;
                                ageCategory.FromAge = 40;
                                ageCategory.ToAge = 45;
                            }
                            else
                            {
                                ageCategory.Id = 1;
                                ageCategory.Name = sqlDataReader["AgeCategoryDefinition"].ToString();
                                ageCategory.SportId = sportId;
                                ageCategory.FromAge = 14;
                                ageCategory.ToAge = 15;
                            }
                        }

                        return ageCategory;
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error("An error occured while getting age category from database. ", exception);
                    return null;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void InsertAgeCategory(AgeCategory ageCategory, int sportId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("[Service_InsertAgeCategory]", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryId", ageCategory.Id));
                    sqlCommand.Parameters.Add(new SqlParameter("@MinAge", ageCategory.FromAge));
                    sqlCommand.Parameters.Add(new SqlParameter("@MaxAge", ageCategory.ToAge));
                    sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryName", ageCategory.Name));
                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));
                    sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryDefinitionId", ageCategory.AgeCategoryDefinitionId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error("An error occured while getting age category from database. ", exception);
                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void UpdateTournamentAgeCategory(AgeCategory ageCategory, int tournamentId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("[Service_UpdateTournamentAgeCategory]", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryId", ageCategory.Id));
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", tournamentId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error("An error occured while getting age category from database. ", exception);
                    throw;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public AgeCategory GetAgeCategoryByTournamentId(int tournamentId, int sportId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("Service_GetAgeCategoryByTournamentId", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", tournamentId));

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }
                    else
                    {
                        var ageCategory = new AgeCategory();
                        while (sqlDataReader.Read())
                        {
                            ageCategory.Id = Convert.ToInt32(sqlDataReader["AgeCategoryID"]);
                            ageCategory.Name = sqlDataReader["AgeCategoryName"].ToString();
                            ageCategory.SportId = sportId;
                            ageCategory.FromAge = Convert.ToInt32(sqlDataReader["MinAge"]);
                            ageCategory.ToAge = Convert.ToInt32(sqlDataReader["MaxAge"]);
                            ageCategory.AgeCategoryDefinitionId = Convert.ToInt32(sqlDataReader["AgeCategoryDefinitionId"]);
                            if (sqlDataReader["AgeCategoryDefinition"] != DBNull.Value)
                            {
                                ageCategory.AgeCategoryDefinition = sqlDataReader["AgeCategoryDefinition"].ToString();
                            }
                        }

                        return ageCategory;
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error("An error occured while getting age category from database. ", exception);
                    return null;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public AgeCategory GetAgeCategoryDefinitionByTournamentId(int tournamentId, int sportId)
        {
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("Service_GetAgeCategoryDefinitionByTournamentId",
                        sqlConnection);

                    sqlCommand.Parameters.Add(
                        new SqlParameter("@TournamentId", tournamentId));

                    sqlCommand.Parameters.Add(
                        new SqlParameter("@SportId", sportId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();


                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }
                    else
                    {
                        var ageCategory = new AgeCategory();
                        while (sqlDataReader.Read())
                        {
                            if (sqlDataReader["AgeCategoryDefinition"].ToString()
                                .ToLower() == "senior")
                            {
                                ageCategory.Id = 18;
                                ageCategory.Name = sqlDataReader["AgeCategoryDefinition"].ToString();
                                ageCategory.SportId = sportId;
                                ageCategory.FromAge = 40;
                                ageCategory.ToAge = 45;
                            }
                            else
                            {
                                ageCategory.Id = 1;
                                ageCategory.Name = sqlDataReader["AgeCategoryDefinition"].ToString();
                                ageCategory.SportId = sportId;
                                ageCategory.FromAge = 14;
                                ageCategory.ToAge = 15;
                            }
                        }

                        return ageCategory;
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error("An error occured while getting age category from database. ", exception);
                    return null;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        ///     The check age categories.
        /// </summary>
        /// <param name="ageCategories">
        ///     The age categories.
        /// </param>
        /// <param name="sportId">
        ///     The sport Id.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        public bool CheckAgeCategories(AgeCategory[] ageCategories, int sportId)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                        sqlConnection.Open();
                    }

                    Logger.Info("Starting check of AgeCategories table and inserting necessary categories into database");

                    foreach (var ageCategory in ageCategories)
                    {
                        Logger.Info("Checking databasetable for: " + ageCategory.Name);
                        var sqlCommand = new SqlCommand("Service_GetAgeCategoryByCategoryId", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                        sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryId", ageCategory.Id));

                        sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                        var sqlDataReader = sqlCommand.ExecuteReader();

                        if (!sqlDataReader.HasRows)
                        {
                            var connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                            var insertConnection = new SqlConnection(connectionString);

                            // Open the connection to the database
                            insertConnection.Open();

                            // We must insert
                            var ageCategoryInsertCommand = new SqlCommand("Service_InsertAgeCategory", insertConnection)
                            {
                                CommandType = CommandType.StoredProcedure
                            };

                            // Setting CommandType to StoredProcedure
                            ageCategoryInsertCommand.Parameters.Add(new SqlParameter("@AgeCategoryId", ageCategory.Id));

                            ageCategoryInsertCommand.Parameters.Add(new SqlParameter("@MinAge", ageCategory.FromAge));

                            ageCategoryInsertCommand.Parameters.Add(new SqlParameter("@MaxAge", ageCategory.ToAge));

                            ageCategoryInsertCommand.Parameters.Add(new SqlParameter("@AgeCategoryName",
                                ageCategory.Name));

                            ageCategoryInsertCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                            // We must populate the database
                            ageCategoryInsertCommand.ExecuteNonQuery();

                            insertConnection.Close();
                        }

                        sqlDataReader.Close();
                    }

                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }

                    return true;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return false;
            }
        }
    }
}