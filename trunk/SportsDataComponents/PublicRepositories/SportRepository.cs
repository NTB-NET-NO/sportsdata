﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SportRepository.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The sport repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;

namespace NTB.SportsData.Components.PublicRepositories
{
    /// <summary>
    /// The sport repository.
    /// </summary>
    public class SportRepository : IRepository<Sport>, ISportDataMapper
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SportRepository));

        /// <summary>
        /// The error logger.
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public int InsertOne(Sport domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void InsertAll(List<Sport> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Update(Sport domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Delete(Sport domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<Sport> GetAll()
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                var sports = new List<Sport>();
                var sqlCommand = new SqlCommand("AspNet_Identity_GetApplicationId", sqlConnection) { CommandType = CommandType.StoredProcedure };

                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                // Creating the SQL-reader.
                var sqlDataReader = sqlCommand.ExecuteReader();

                if (!sqlDataReader.HasRows)
                {
                    return sports.AsQueryable();
                }

                while (sqlDataReader.Read())
                {
                    var sport = new Sport { Id = Convert.ToInt32(sqlDataReader["SportId"]), Name = sqlDataReader["SportId"].ToString() };

                    sports.Add(sport);
                }

                return sports.AsQueryable();
            }
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Sport"/>.
        /// </returns>
        public Sport Get(int id)
        {
            try
            {
                var sports = this.GetAll();

                return sports.SingleOrDefault(x => x.Id == id);
            }
            catch (Exception exception)
            {
                ErrorLogger.Error(exception.Message);

                return new Sport();
            }
        }

        /// <summary>
        /// The get sport by org id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see cref="Sport"/>.
        /// </returns>
        public Sport GetSportByOrgId(int orgId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                var command = new SqlCommand("Service_GetSportByOrgId", sqlConnection) { CommandType = CommandType.StoredProcedure };

                command.Parameters.Add(new SqlParameter("@OrgId", orgId));

                // Telling the system that this is a StoredProcedure
                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                // Creating the SQL-reader.
                var sqlDataReader = command.ExecuteReader();

                if (!sqlDataReader.HasRows)
                {
                    return null;
                }

                var sport = new Sport();
                while (sqlDataReader.Read())
                {
                    sport.Id = Convert.ToInt32(sqlDataReader["SportId"]);
                    sport.Name = sqlDataReader["Sport"].ToString();
                }

                return sport;
            }
        }
    }
}