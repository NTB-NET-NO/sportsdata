﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerRepository.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The customer repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;

namespace NTB.SportsData.Components.PublicRepositories
{
    /// <summary>
    /// The customer repository.
    /// </summary>
    internal class CustomerRepository : ICustomerDataMapper
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(CustomerRepository));

        /// <summary>
        /// The error logger.
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        /// <summary>
        /// The get customer by match and sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Customer> GetCustomerByMatchAndSportId(int matchId, int sportId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("Service_GetJobIdAndTournamentIdByMatchId", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@MatchId", matchId));

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    // xTODO: Continue here
                    /**
                     * What we are to do: 
                     * First we get the TournamentId and JobId. We then use this to find
                     * the customers that shall have these data.
                     * We are to return a list of customernames
                     * 
                     */
                    var customers = new List<Customer>();
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            var customer = new Customer();
                            if (sqlDataReader["CustomerName"] != DBNull.Value)
                            {
                                customer.Name = sqlDataReader["CustomerName"].ToString();
                            }

                            if (sqlDataReader["CustomerId"] != DBNull.Value)
                            {
                                customer.Id = Convert.ToInt32(sqlDataReader["CustomerId"].ToString());
                            }

                            if (sqlDataReader["CustomerId"] != DBNull.Value)
                            {
                                customer.RemoteCustomerId = Convert.ToInt32(sqlDataReader["TTCustomerId"].ToString());
                            }

                            customers.Add(customer);
                        }

                        return customers;
                    }

                    // logger.Debug("No customers needs these data");
                    // TODO: Change this to return null-list
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["livetesting"]))
                    {
                        var customer = new Customer { Name = "Norsk Telegrambyrå" };
                        customers.Add(customer);
                        return customers;
                    }

                    // TODO: Change this to return the Customer-list
                    return customers;
                }
                catch (Exception exception)
                {
                    Logger.Error("An error occured while getting list of customers from database. ", exception);
                    return null;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The get customer by tournament and sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Customer> GetCustomerByTournamentAndSportId(int sportId, int tournamentId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("[Service_GetCustomersFromTournamentIdAndSportId]", sqlConnection);

                    Logger.DebugFormat("Calling {0} with parameters TournamentId: {1}, SportId: {2}", "Service_GetCustomersFromTournamentIdAndSportId", tournamentId, sportId);
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", tournamentId));

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    var customers = new List<Customer>();
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            var customer = new Customer();
                            customer.Name = sqlDataReader["CustomerName"].ToString();
                            customer.Id = sqlDataReader["CustomerId"] != DBNull.Value ? Convert.ToInt32(sqlDataReader["CustomerId"]) : 0;

                            customer.RemoteCustomerId = sqlDataReader["RemoteCustomerId"] != DBNull.Value ? Convert.ToInt32(sqlDataReader["RemoteCustomerId"]) : 0;

                            customers.Add(customer);
                        }
                    }

                    Logger.Info("Number of customers " + customers.Count);
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["livetesting"]))
                    {
                        var customer = new Customer { Name = "NTB", Id = 0, RemoteCustomerId = 44967 };
                        customers.Add(customer);
                        return customers;
                    }

                    // If we are testing we will end up here.    
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                    {
                        var customer = new Customer { Name = "NTB", Id = 0, RemoteCustomerId = 44967 };
                        customers.Add(customer);
                        return customers;
                    }

                    // TODO: Change this to return the Customer-list
                    return customers;
                }
                catch (Exception exception)
                {
                    Logger.Error("An error occured while getting list of customers from database. ", exception);
                    return null;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The get customer by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Customer> GetCustomerBySportId(int sportId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("[Service_GetCustomersBySportId]", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    var customers = new List<Customer>();
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            var customer = new Customer { Id = Convert.ToInt32(sqlDataReader["CustomerId"]), Name = sqlDataReader["CustomerName"].ToString(), RemoteCustomerId = Convert.ToInt32(sqlDataReader["TTCustomerId"]) };

                            customers.Add(customer);
                        }

                        return customers;
                    }

                    // logger.Debug("No customers needs these data");
                    // TODO: Change this to return null-list
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["livetesting"]))
                    {
                        var customer = new Customer { Name = "Norsk Telegrambyrå", Id = 1234, RemoteCustomerId = 1234 };
                        customers.Add(customer);
                        return customers;
                    }

                    // TODO: Change this to return the Customer-list
                    return customers;
                }
                catch (Exception exception)
                {
                    Logger.Error("An error occured while getting list of customers from database. ", exception);
                    return null;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The get customer regions by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <param name="customerId">
        /// The customer id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<CustomerRegion> GetCustomerRegionsBySportId(int sportId, int customerId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("[Service_GetCustomerMunicipalitesBySportId]", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", customerId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    var regions = new List<CustomerRegion>();
                    if (!sqlDataReader.HasRows)
                    {
                        return regions;
                    }

                    while (sqlDataReader.Read())
                    {
                        var region = new CustomerRegion { Id = Convert.ToInt32(sqlDataReader["RegionId"]), Name = sqlDataReader["RegionName"].ToString(), };

                        regions.Add(region);
                    }

                    return regions;
                }
                catch (Exception exception)
                {
                    Logger.Error("An error occured while getting list of regions for the customer from database. ", exception);
                    return null;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Throws an exception because it is not been implemented
        /// </exception>
        public int InsertOne(Customer domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// Throws an exception because it is not been implemented
        /// </exception>
        public void InsertAll(List<Customer> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// Throws an exception because it is not been implemented
        /// </exception>
        public void Update(Customer domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// Throws an exception because it is not been implemented
        /// </exception>
        public void Delete(Customer domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Throws an exception because it is not been implemented
        /// </exception>
        public IQueryable<Customer> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Customer"/>.
        /// </returns>
        public Customer GetCustomerById(int id)
        {
            var customer = new Customer();
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("Service_GetCustomerById", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", id));

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return customer;
                    }

                    while (sqlDataReader.Read())
                    {
                        customer.Id = Convert.ToInt32(sqlDataReader["CustomerId"]);
                        customer.Name = sqlDataReader["CustomerName"].ToString();
                        customer.RemoteCustomerId = Convert.ToInt32(sqlDataReader["RemoteCustomerId"]);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error("An error occured while getting list of customers from database. Check error log for log");
                    ErrorLogger.Error(exception.Message);
                    ErrorLogger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        ErrorLogger.Error(exception.InnerException.Message);
                        ErrorLogger.Error(exception.InnerException.StackTrace);
                    }

                    return customer;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }

            return customer;
        }
    }
}