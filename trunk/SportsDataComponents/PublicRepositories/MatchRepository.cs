﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using log4net;
using log4net.Config;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;

namespace NTB.SportsData.Components.PublicRepositories
{
    /// <summary>
    /// The match repository.
    /// </summary>
    public class MatchRepository : IRepository<Match>, IMatchDataMapper
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Initializes static members of the <see cref="MatchRepository" /> class.
        /// </summary>
        static MatchRepository()
        {
            // Set up logger
            XmlConfigurator.Configure();
            if (!LogManager.GetRepository()
                .Configured)
            {
                BasicConfigurator.Configure();
            }
        }

        #endregion

        #region Static Fields

        /// <summary>
        ///     Creating the static Error Logger
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        /// <summary>
        ///     Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(MatchRepository));

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public void Delete(Match domainobject)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    Logger.Debug("Running SQL-Procedure [SportsData_DeleteMatchByMatchId]");
                    var sqlCommand = new SqlCommand("[SportsData_DeleteMatchByMatchId]", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@MatchId", domainobject.Id));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();

                    Logger.InfoFormat("Match {0} deleted successfully", domainobject.Id);
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Match"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Not Implemented
        /// </exception>
        public Match Get(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public IQueryable<Match> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get match by match id and sport id.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see cref="Match"/>.
        /// </returns>
        public Match GetMatchByMatchIdAndSportId(int matchId, int sportId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    Logger.Debug("Running SQL-Procedure [Service_GetMatchByMatchId]");
                    var sqlCommand = new SqlCommand("[Service_GetMatchByMatchId]", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@MatchId", matchId));

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        Logger.InfoFormat("No matches found with ID {0} and sport {1}", matchId, sportId);
                        return null;
                    }

                    var match = new Match {Downloaded = false};

                    while (sqlDataReader.Read())
                    {
                        match.Id = Convert.ToInt32(sqlDataReader["MatchId"]);
                        match.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                        match.HomeTeamName = sqlDataReader["HomeTeam"].ToString();
                        match.AwayTeamName = sqlDataReader["AwayTeam"].ToString();
                        match.MatchDate = Convert.ToDateTime(sqlDataReader["MatchDate"]);
                        match.MatchStartTime = Convert.ToInt32(sqlDataReader["MatchStartTime"]);
                        match.TournamentId = Convert.ToInt32(sqlDataReader["TournamentId"]);

                        if (sqlDataReader["Downloaded"] != DBNull.Value)
                        {
                            match.Downloaded = Convert.ToBoolean(sqlDataReader["Downloaded"]);
                        }
                    }

                    return match;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return null;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The get matches by date interval.
        /// </summary>
        /// <param name="dateStart">
        /// The date start.
        /// </param>
        /// <param name="dateEnd">
        /// The date end.
        /// </param>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Match> GetMatchesByDateInterval(DateTime dateStart, DateTime dateEnd, int tournamentId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                Logger.Debug("Running SQL-Procedure [Service_GetMatchByDateInterval]");
                var sqlCommand = new SqlCommand("Service_GetMatchesByDateInterval", sqlConnection);

                sqlCommand.Parameters.Add(new SqlParameter("@DateStart", dateStart));

                sqlCommand.Parameters.Add(new SqlParameter("@DateEnd", dateEnd));

                sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", tournamentId));

                sqlCommand.CommandType = CommandType.StoredProcedure;
                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                var sqlDataReader = sqlCommand.ExecuteReader();

                if (!sqlDataReader.HasRows)
                {
                    return null;
                }

                var matches = new List<Match>();
                while (sqlDataReader.Read())
                {
                    Logger.Debug("MatchId: " + sqlDataReader["MatchId"]);
                    Logger.Debug("SportId: " + sqlDataReader["SportId"]);
                    Logger.Debug("HomeTeam: " + sqlDataReader["HomeTeam"]);
                    Logger.Debug("AwayTeam: " + sqlDataReader["AwayTeam"]);
                    Logger.Debug("MatchDate: " + sqlDataReader["MatchDate"]);
                    Logger.Debug("MatchStartTime: " + sqlDataReader["MatchStartTime"]);
                    Logger.Debug("TournamentId: " + sqlDataReader["TournamentId"]);
                    Logger.Debug("Downloaded: " + sqlDataReader["Downloaded"]);

                    var match = new Match
                    {
                        Id = Convert.ToInt32(sqlDataReader["MatchId"]),
                        SportId = Convert.ToInt32(sqlDataReader["SportId"]),
                        HomeTeamName = sqlDataReader["HomeTeam"].ToString(),
                        AwayTeamName = sqlDataReader["AwayTeam"].ToString(),
                        MatchDate = Convert.ToDateTime(sqlDataReader["MatchDate"]),
                        MatchStartTime = Convert.ToInt32(sqlDataReader["MatchStartTime"]),
                        TournamentId = Convert.ToInt32(sqlDataReader["TournamentId"]),
                    };

                    if (sqlDataReader["Downloaded"] is DBNull)
                    {
                        match.Downloaded = false;
                    }
                    else if (Convert.ToInt32(sqlDataReader["Downloaded"]) == 0)
                    {
                        match.Downloaded = false;
                    }
                    else
                    {
                        match.Downloaded = true;
                    }

                    matches.Add(match);
                }

                return matches;
            }
        }

        /// <summary>
        /// The get matches by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Match> GetMatchesBySportId(int sportId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    Logger.Debug("Running SQL-Procedure [Service_GetMatches]");
                    var sqlCommand = new SqlCommand("[SportsData_GetMatches]", sqlConnection)
                    {
                        CommandType =
                            CommandType
                                .StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    var matches = new List<Match>();
                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }
                    else
                    {
                        while (sqlDataReader.Read())
                        {
                            var match = new Match
                            {
                                Id = Convert.ToInt32(sqlDataReader["MatchId"]),
                                SportId = sportId,
                                HomeTeamName = sqlDataReader["HomeTeam"].ToString(),
                                AwayTeamName = sqlDataReader["AwayTeam"].ToString(),
                                MatchDate = Convert.ToDateTime(sqlDataReader["MatchDate"]),
                                TournamentId = Convert.ToInt32(sqlDataReader["TournamentId"])
                            };
                            matches.Add(match);
                        }

                        return matches;
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error("Something happened while getting number of matches for sport", exception);
                    return null;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The get numberof matches.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int GetNumberofMatches(int sportId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                Logger.DebugFormat("ConnectionString: {0}", ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
                try
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        Logger.DebugFormat("State of sql connection: {0}", sqlConnection.State);
                        sqlConnection.Open();
                    }

                    Logger.Debug("Running SQL-Procedure [Service_GetNumberOfMatches]");
                    var sqlCommand = new SqlCommand("Service_GetNumberOfMatches", sqlConnection)
                    {
                        CommandType =
                            CommandType
                                .StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    var numberOfMatches = 0;
                    if (!sqlDataReader.HasRows)
                    {
                        return numberOfMatches;
                    }
                    else
                    {
                        while (sqlDataReader.Read())
                        {
                            if (sqlDataReader["NumberOfMatches"] != DBNull.Value)
                            {
                                numberOfMatches = Convert.ToInt32(sqlDataReader["NumberOfMatches"].ToString());
                            }
                        }

                        return numberOfMatches;
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error("Something happened while getting number of matches for sport", exception);
                    return 0;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The get scheduled match by match id and sport id.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see cref="Match"/>.
        /// </returns>
        public Match GetScheduledMatchByMatchIdAndSportId(int matchId, int sportId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    Logger.Debug("Running SQL-Procedure [Service_GetScheduleMatchByMatchId]");
                    var sqlCommand = new SqlCommand("[Service_GetScheduledMatchByMatchId]", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@MatchId", matchId));

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    var match = new Match {Downloaded = false};

                    while (sqlDataReader.Read())
                    {
                        match.Id = Convert.ToInt32(sqlDataReader["MatchId"]);
                        match.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                        match.HomeTeamName = sqlDataReader["HomeTeam"].ToString();
                        match.AwayTeamName = sqlDataReader["AwayTeam"].ToString();
                        match.MatchDate = Convert.ToDateTime(sqlDataReader["MatchDate"]);
                        match.MatchStartTime = Convert.ToInt32(sqlDataReader["MatchStartTime"]);
                        match.TournamentId = Convert.ToInt32(sqlDataReader["TournamentId"]);

                        if (sqlDataReader["Downloaded"] != DBNull.Value)
                        {
                            match.Downloaded = Convert.ToBoolean(sqlDataReader["Downloaded"]);
                        }
                    }

                    return match;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return null;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        public void InsertAll(List<Match> domainobject)
        {
            foreach (var match in domainobject)
            {
                // We need to check if this item is in the database
                var existingMatch = this.GetMatchByMatchIdAndSportId(match.Id, match.SportId);
                if (existingMatch == null)
                {
                    this.InsertOne(match);
                }
            }
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int InsertOne(Match domainobject)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    Logger.Debug("Running SQL-Procedure [Service_InsertMatches]");
                    var sqlCommand = new SqlCommand("[Service_InsertMatches]", sqlConnection)
                    {
                        CommandType =
                            CommandType
                                .StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@MatchId", domainobject.Id));

                    sqlCommand.Parameters.Add(new SqlParameter("@HomeTeam", domainobject.HomeTeamName));

                    sqlCommand.Parameters.Add(new SqlParameter("@AwayTeam", domainobject.AwayTeamName));

                    sqlCommand.Parameters.Add(new SqlParameter("@Date", domainobject.MatchDate));

                    sqlCommand.Parameters.Add(new SqlParameter("@Time", domainobject.MatchStartTime));

                    sqlCommand.Parameters.Add(new SqlParameter("@Downloaded", 0));

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", domainobject.TournamentId));

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentRoundNumber", domainobject.RoundId));

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentRoundName", domainobject.RoundName));

                    sqlCommand.Parameters.Add(new SqlParameter("@HomeTeamId", domainobject.HomeTeamId));

                    sqlCommand.Parameters.Add(new SqlParameter("@AwayTeamId", domainobject.AwayTeamId));

                    sqlCommand.Parameters.Add(new SqlParameter("@VenueId", domainobject.VenueUnitId));

                    sqlCommand.Parameters.Add(new SqlParameter("@VenueName", domainobject.VenueName));

                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();

                    Logger.InfoFormat(
                        "Match {0}: {1} - {2} inserted successfully.",
                        domainobject.Id,
                        domainobject.HomeTeamName,
                        domainobject.AwayTeamName);
                }
                catch (Exception exception)
                {
                    Logger.Error("An error occurred while inserting match. Check error log");
                    ErrorLogger.Error(exception.Message);
                    ErrorLogger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        ErrorLogger.Error(exception.InnerException.Message);
                        ErrorLogger.Error(exception.InnerException.StackTrace);
                    }

                    return 0;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return 1;
            }
        }

        /// <summary>
        /// The set match downloaded.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        public void SetMatchDownloaded(int matchId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    Logger.Debug("Running SQL-Procedure [Service_SetDownloadMatch]");
                    var sqlCommand = new SqlCommand("[Service_SetDownloadMatch]", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@MatchId", matchId));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error("Something happened while setting match as downloaded", exception);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The set schedule match downloaded.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        public void SetScheduleMatchDownloaded(int matchId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    Logger.Debug("Running SQL-Procedure [Service_SetDownloadMatchSchedule]");
                    var sqlCommand = new SqlCommand("[Service_SetDownloadMatchSchedule]", sqlConnection)
                    {
                        CommandType
                            =
                            CommandType
                                .StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@MatchId", matchId));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error("Something happened while setting match as downloaded", exception);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Delete matches by day interval
        /// </summary>
        /// <param name="days">number of days to save</param>
        public void DeleteMatchesByDayInterval(int days)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    Logger.Debug("Running SQL-Procedure [SportsData_DeleteMatchesByDays]");
                    var sqlCommand = new SqlCommand("[SportsData_DeleteMatchesByDays]", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Days", days));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Delete matches by day interval
        /// </summary>
        /// <param name="days">number of days to save</param>
        /// <param name="sportId">The Sport we are about to delete from</param>
        public void DeleteMatchesByDayIntervalAndSportId(int days, int sportId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    Logger.Debug("Running SQL-Procedure [SportsData_DeleteMatchesByDaysAndSportId]");
                    var sqlCommand = new SqlCommand("[SportsData_DeleteMatchesByDaysAndSportId]", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@Days", days));

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        public void Update(Match domainobject)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    Logger.Debug("Running SQL-Procedure [Service_UpdateMatches]");
                    var sqlCommand = new SqlCommand("[Service_UpdateMatches]", sqlConnection)
                    {
                        CommandType =
                            CommandType
                                .StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@MatchId", domainobject.Id));
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", domainobject.TournamentId));
                    sqlCommand.Parameters.Add(new SqlParameter("@HomeTeam", domainobject.HomeTeamName));
                    sqlCommand.Parameters.Add(new SqlParameter("@AwayTeam", domainobject.AwayTeamName));
                    sqlCommand.Parameters.Add(new SqlParameter("@Date", domainobject.MatchDate));
                    sqlCommand.Parameters.Add(new SqlParameter("@Time", domainobject.MatchStartTime));
                    sqlCommand.Parameters.Add(new SqlParameter("@Downloaded", domainobject.Downloaded));

                    sqlCommand.ExecuteNonQuery();

                    Logger.InfoFormat(
                        "Match {0}: {1} - {2} updated successfully.",
                        domainobject.Id,
                        domainobject.HomeTeamName,
                        domainobject.AwayTeamName);
                }
                catch (Exception exception)
                {
                    Logger.Error("Something happened while setting match as downloaded", exception);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        #endregion
    }
}