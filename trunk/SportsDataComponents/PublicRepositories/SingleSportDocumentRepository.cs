﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SingleSportDocumentRepository.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The single sport document repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;

namespace NTB.SportsData.Components.PublicRepositories
{
    /// <summary>
    /// The single sport document repository.
    /// </summary>
    internal class SingleSportDocumentRepository : IRepository<SingleSportDocument>, ISingleSportDocumentDataMapper
    {
        /// <summary>
        /// The logger.
        /// </summary>
        public static readonly ILog Logger = LogManager.GetLogger(typeof(DocumentRepository));

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int InsertOne(SingleSportDocument domainobject)
        {
            // We must now add this to the database
            // sqlConnection = new SqlConnection(connectionString);
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("Service_InsertSingleSportDocument", sqlConnection) { CommandType = CommandType.StoredProcedure };

                    sqlCommand.Parameters.Add(new SqlParameter("@EventId", domainobject.EventId));

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));

                    // Setting the version to 1
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentVersion", domainobject.Version));

                    // Setting the DocumentName
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentName", domainobject.Filename));

                    // Setting the DocumentName
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentCreated", DateTime.Now));

                    // Open the connection
                    sqlConnection.Open();

                    // Run the query
                    sqlCommand.ExecuteNonQuery();

                    return 1;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return 0;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void InsertAll(List<SingleSportDocument> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        public void Update(SingleSportDocument domainobject)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("Service_UpdateSingleSportDocument", sqlConnection) { CommandType = CommandType.StoredProcedure };

                    // Setting the DocumentName
                    Logger.Debug("DocumentVersion: " + domainobject.Version);
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentVersion", domainobject.Version));

                    // Setting the TournamentId
                    Logger.Debug("TournamentId: " + domainobject.EventId);
                    sqlCommand.Parameters.Add(new SqlParameter("@EventId", domainobject.EventId));

                    // Setting the DocumentName
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentCreated", DateTime.Now));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // Run the query
                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Delete(SingleSportDocument domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public IQueryable<SingleSportDocument> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="SingleSportDocument"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public SingleSportDocument Get(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get document version by event id.
        /// </summary>
        /// <param name="eventId">
        /// The event id.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int GetDocumentVersionByEventId(int eventId)
        {
            var documentVersion = 1;
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("Service_GetDocumentByEventId", sqlConnection) { CommandType = CommandType.StoredProcedure };

                    Logger.Debug("GetDocumentByEventId: " + eventId);
                    sqlCommand.Parameters.Add(new SqlParameter("@EventId", eventId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            documentVersion = Convert.ToInt32(sqlDataReader["DocumentVersion"]) + 1;
                        }
                    }

                    return documentVersion;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return 0;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }
    }
}