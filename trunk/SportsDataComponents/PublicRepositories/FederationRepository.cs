﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OrganizationRepository.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;

namespace NTB.SportsData.Components.PublicRepositories
{
    /// <summary>
    ///     The organization repository.
    /// </summary>
    internal class FederationRepository : IRepository<Federation>, IOrganizationDataMapper
    {
        #region Static Fields

        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(FederationRepository));

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public void Delete(Federation domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Federation"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public Federation Get(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     The get all.
        /// </summary>
        /// <returns>
        ///     The <see cref="IQueryable" />.
        /// </returns>
        public IQueryable<Federation> GetAll()
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("[Service_GetSports]", sqlConnection)
                                         {
                                             CommandType = CommandType.StoredProcedure
                                         };

                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    var organizations = new List<Federation>();
                    while (sqlDataReader.Read())
                    {
                        var organization = new Federation
                                               {
                                                   Id = Convert.ToInt32(sqlDataReader["OrgId"]),
                                                   Name = sqlDataReader["OrgName"].ToString(),
                                                   SportId = Convert.ToInt32(sqlDataReader["SportId"]),
                                                   Sport = sqlDataReader["Sport"].ToString()
                                               };

                        organizations.Add(organization);
                    }

                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }

                    return organizations.AsQueryable();
                }
                catch (Exception exception)
                {
                    Logger.Error("Error while getting Organizations", exception);
                    Logger.Error(exception.StackTrace);
                    return null;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The get federation discipline by id.
        /// </summary>
        /// <param name="disciplineId">
        /// The discipline id.
        /// </param>
        /// <returns>
        /// The <see cref="Discipline"/>.
        /// </returns>
        public Discipline GetFederationDisciplineById(int disciplineId)
        {
            if (disciplineId == 0)
            {
                throw new Exception("Discipline Id is null. Cannot get federation!");
            }

            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("[Service_GetDisciplineById]", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@DisciplineId", disciplineId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    var discipline = new Discipline();
                    while (sqlDataReader.Read())
                    {
                        discipline.Id = Convert.ToInt32(sqlDataReader["DisciplineId"]);
                        discipline.Name = sqlDataReader["Discipline"].ToString();
                        discipline.OrgId = Convert.ToInt32(sqlDataReader["OrgId"]);
                    }

                    return discipline;
                }
                catch (Exception exception)
                {
                    Logger.Error("Error while getting Discipline by Id", exception);
                    Logger.Error(exception.StackTrace);
                    return new Discipline();
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The get organization by org id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see cref="Federation"/>.
        /// </returns>
        public Federation GetFederationById(int orgId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("[Service_GetOrganizationByOrgId]", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@OrgId", orgId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    var federation = new Federation();
                    while (sqlDataReader.Read())
                    {
                        federation.Id = Convert.ToInt32(sqlDataReader["OrgId"]);
                        federation.Name = sqlDataReader["OrgName"].ToString();
                        federation.NameShort = sqlDataReader["OrgNameShort"].ToString();
                        federation.Sport = sqlDataReader["Sport"].ToString();
                        federation.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                        federation.TeamSport = Convert.ToInt32(sqlDataReader["TeamSport"]);
                        federation.SingleSport = Convert.ToInt32(sqlDataReader["SingleSport"]);
                    }


                    return federation;
                }
                catch (Exception exception)
                {
                    Logger.Error("Error while getting Organization by Org Id", exception);
                    Logger.Error(exception.StackTrace);
                    return new Federation();
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The get organization by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see cref="Federation"/>.
        /// </returns>
        public Federation GetFederationBySportId(int sportId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("[Service_GetOrganizationBySportId]", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    var organization = new Federation();
                    while (sqlDataReader.Read())
                    {
                        organization.Id = Convert.ToInt32(sqlDataReader["OrgId"]);
                        organization.Name = sqlDataReader["OrgName"].ToString();
                        organization.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                        organization.NameShort = sqlDataReader["OrgNameShort"].ToString();
                        organization.SingleSport = Convert.ToInt32(sqlDataReader["SingleSport"]);
                        organization.TeamSport = Convert.ToInt32(sqlDataReader["TeamSport"]);
                        organization.Sport = sqlDataReader["Sport"].ToString();
                    }

                    return organization;
                }
                catch (Exception exception)
                {
                    Logger.Error("Error while getting Organization by Sport Id", exception);
                    Logger.Error(exception.StackTrace);
                    return new Federation();
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public void InsertAll(List<Federation> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public int InsertOne(Federation domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public void Update(Federation domainobject)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}