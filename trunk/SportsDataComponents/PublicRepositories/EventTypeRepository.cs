﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using log4net;
using NTB.SportsData.Classes.Classes;

namespace NTB.SportsData.Components.PublicRepositories
{
    public class EventTypeRepository
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(EventTypeRepository));

        public EventType GetEventTypeById(int id)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_GetEventTypeByEventTypeId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@EventTypeId", id));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    var eventType = new EventType();
                    if (!sqlDataReader.HasRows)
                    {
                        // throw new Exception("Query returned no rows!");
                        Logger.Info("Query returned no rows");
                        return null;
                    }

                    while (sqlDataReader.Read())
                    {
                        // TournamentId, DistrictId, SeasonId, SportId, TournamentName, Push, AgeCategoryId, GenderId, TournamentTypeId, TournamentNumber
                        eventType.EventTypeId = Convert.ToInt32(sqlDataReader["EventTypeId"]);
                        eventType.EventTypeName = sqlDataReader["EventTypeName"].ToString();
                    }

                    return eventType;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }

                    return null;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }
        /// <summary>
        /// Get the event types from the database
        /// </summary>
        /// <returns>List of event types</returns>
        public List<EventType> GetEventTypes()
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_GetEventTypes", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    var eventTypes = new List<EventType>();
                    if (!sqlDataReader.HasRows)
                    {
                        // throw new Exception("Query returned no rows!");
                        Logger.Info("Query returned no rows");
                        
                    }

                    while (sqlDataReader.Read())
                    {
                        var eventType = new EventType();

                        // TournamentId, DistrictId, SeasonId, SportId, TournamentName, Push, AgeCategoryId, GenderId, TournamentTypeId, TournamentNumber
                        eventType.EventTypeId = Convert.ToInt32(sqlDataReader["EventTypeId"]);
                        eventType.EventTypeName = sqlDataReader["EventTypeName"].ToString();

                        Logger.DebugFormat("Adding eventtype {0} with id {1}", eventType.EventTypeName, eventType.EventTypeId);
                        eventTypes.Add(eventType);
                    }

                    return eventTypes;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }

                    return null;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public EventType GetEventTypeByTournamentId(int id)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("SportsData_GetEventTypeByTournamentId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", id));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    var eventType = new EventType();
                    if (!sqlDataReader.HasRows)
                    {
                        // throw new Exception("Query returned no rows!");
                        Logger.Info("Query returned no rows");
                        return null;
                    }

                    while (sqlDataReader.Read())
                    {
                        // TournamentId, DistrictId, SeasonId, SportId, TournamentName, Push, AgeCategoryId, GenderId, TournamentTypeId, TournamentNumber
                        if (sqlDataReader["EventTypeId"] != DBNull.Value)
                        {
                            eventType.EventTypeId = Convert.ToInt32(sqlDataReader["EventTypeId"]);
                        }

                        if (sqlDataReader["EventTypeName"] != DBNull.Value)
                        {
                            eventType.EventTypeName = sqlDataReader["EventTypeName"].ToString();
                        }
                    }

                    return eventType;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }

                    return null;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }
    }
}
