﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RegionRepository.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The region repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using log4net;
using NTB.SportsData.Classes.Classes;

namespace NTB.SportsData.Components.PublicRepositories
{
    /// <summary>
    /// The region repository.
    /// </summary>
    public class RegionRepository
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(RegionRepository));

        /// <summary>
        /// The get all regions.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<CustomerRegion> GetAllRegions()
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    var regions = new List<CustomerRegion>();
                    var sqlCommand = new SqlCommand("SportsData_GetAllRegions", sqlConnection) { CommandType = CommandType.StoredProcedure };

                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    // Creating the SQL-reader.
                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return new List<CustomerRegion>();
                    }

                    while (sqlDataReader.Read())
                    {
                        var region = new CustomerRegion { Id = Convert.ToInt32(sqlDataReader["RegionId"]), Name = sqlDataReader["RegionName"].ToString() };

                        regions.Add(region);
                    }

                    return regions;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);

                    return new List<CustomerRegion>();
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }
    }
}