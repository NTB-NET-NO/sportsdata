﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;

namespace NTB.SportsData.Components.PublicRepositories
{
    /// <summary>
    /// The tournament repository.
    /// </summary>
    internal class TournamentRepository : IRepository<Tournament>, ITournamentDataMapper
    {
        /// <summary>
        ///     Creating the static Error Logger
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        /// <summary>
        ///     Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(TournamentRepository));

        #region Public Methods and Operators

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public void Delete(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Tournament"/>.
        /// </returns>
        public Tournament Get(int id)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand(@"Service_GetTournamentByTournamentId", sqlConnection) {CommandType = CommandType.StoredProcedure};

                sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", id));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                var sqlDataReader = sqlCommand.ExecuteReader();

                var tournament = new Tournament();
                if (!sqlDataReader.HasRows)
                {
                    return tournament;
                }

                while (sqlDataReader.Read())
                {
                    tournament.Id = Convert.ToInt32(sqlDataReader["TournamentId"]);
                    tournament.Name = sqlDataReader["TournamentName"].ToString();
                    tournament.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                    tournament.SeasonId = Convert.ToInt32(sqlDataReader["SeasonId"]);
                    tournament.GenderId = Convert.ToInt32(sqlDataReader["GenderId"]);
                    tournament.AgeCategoryId = Convert.ToInt32(sqlDataReader["AgeCategoryId"]);
                    tournament.TypeId = Convert.ToInt32(sqlDataReader["TournamentTypeId"]);
                    tournament.DistrictId = Convert.ToInt32(sqlDataReader["DistrictId"]);
                    if (sqlDataReader["DisciplineId"] != DBNull.Value)
                    {
                        tournament.DisciplineId = Convert.ToInt32(sqlDataReader["DisciplineId"]);
                    }

                    if (sqlDataReader["PublishResult"] != DBNull.Value)
                    {
                        var publishTableInt = Convert.ToInt32(sqlDataReader["PublishResult"]);
                        if (publishTableInt == 0)
                        {
                            tournament.PublishResult = false;
                        }
                        else
                        {
                            tournament.PublishResult = true;
                        }
                    }

                    if (sqlDataReader["PublishTournamentTable"] != DBNull.Value)
                    {
                        var publishTableInt = Convert.ToInt32(sqlDataReader["PublishTournamentTable"]);
                        if (publishTableInt == 0)
                        {
                            tournament.PublishTournamentTable = false;
                        }
                        else
                        {
                            tournament.PublishTournamentTable = true;
                        }
                    }

                    if (sqlDataReader["AgeCategoryDefinitionId"] != DBNull.Value)
                    {
                        tournament.AgeCategoryDefinitionId = Convert.ToInt32(sqlDataReader["AgeCategoryDefinitionId"]);
                    }

                    if (sqlDataReader["TournamentNumber"] != DBNull.Value)
                    {
                        tournament.Number = Convert.ToInt32(sqlDataReader["TournamentNumber"].ToString());
                    }
                }

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return tournament;
            }
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public IQueryable<Tournament> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get customer tournaments.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetCustomerTournaments(int sportId, int seasonId)
        {
            var tournaments = new List<Tournament>();
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                var sqlCommand = new SqlCommand(@"[Service_GetCustomerTournaments]", sqlConnection) {CommandType = CommandType.StoredProcedure};

                sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", seasonId));

                sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                var sqlDataReader = sqlCommand.ExecuteReader();

                if (!sqlDataReader.HasRows)
                {
                    return null;
                }

                while (sqlDataReader.Read())
                {
                    var tournament = new Tournament
                    {
                        Id = Convert.ToInt32(sqlDataReader["TournamentId"]),
                        Name = sqlDataReader["TournamentName"].ToString(),
                        Number = Convert.ToInt32(sqlDataReader["TournamentNumber"]),
                        TypeId = Convert.ToInt32(sqlDataReader["TournamentTypeId"]),
                        SportId = Convert.ToInt32(sqlDataReader["SportId"]),
                        SeasonId = Convert.ToInt32(sqlDataReader["SeasonId"]),
                        DistrictId = Convert.ToInt32(sqlDataReader["DistrictId"]),
                        AgeCategoryId = Convert.ToInt32(sqlDataReader["AgeCategoryId"]),
                        GenderId = Convert.ToInt32(sqlDataReader["GenderId"])
                    };

                    if (sqlDataReader["AgeCategoryDefinitionId"] != DBNull.Value)
                    {
                        tournament.AgeCategoryDefinitionId = Convert.ToInt32(sqlDataReader["AgeCategoryDefinitionId"]);
                    }

                    

                    tournaments.Add(tournament);
                }

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return tournaments;
            }
        }

        /// <summary>
        /// The get tournament by match id.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <returns>
        /// The <see cref="Tournament"/>.
        /// </returns>
        public Tournament GetTournamentByMatchId(int matchId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand(@"Service_GetTournamentIdByMatchId", sqlConnection) {CommandType = CommandType.StoredProcedure};

                sqlCommand.Parameters.Add(new SqlParameter("@MatchId", matchId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                var sqlDataReader = sqlCommand.ExecuteReader();

                var tournament = new Tournament();
                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        tournament.Id = Convert.ToInt32(sqlDataReader["TournamentId"]);
                        tournament.Name = sqlDataReader["TournamentName"].ToString();
                        tournament.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                        tournament.SeasonId = Convert.ToInt32(sqlDataReader["SeasonId"]);
                        tournament.GenderId = Convert.ToInt32(sqlDataReader["GenderId"]);
                        tournament.AgeCategoryId = Convert.ToInt32(sqlDataReader["AgeCategoryId"]);
                        tournament.TypeId = Convert.ToInt32(sqlDataReader["TournamentTypeId"]);
                        tournament.DistrictId = Convert.ToInt32(sqlDataReader["DistrictId"]);
                        if (sqlDataReader["DisciplineId"] != DBNull.Value)
                        {
                            tournament.DisciplineId = Convert.ToInt32(sqlDataReader["DisciplineId"]);
                        }

                        if (sqlDataReader["PublishResult"] != DBNull.Value)
                        {
                            var publishTableInt = Convert.ToInt32(sqlDataReader["PublishResult"]);
                            if (publishTableInt == 0)
                            {
                                tournament.PublishResult = false;
                            }
                            else
                            {
                                tournament.PublishResult = true;
                            }
                        }

                        if (sqlDataReader["PublishTournamentTable"] != DBNull.Value)
                        {
                            var publishTableInt = Convert.ToInt32(sqlDataReader["PublishTournamentTable"]);
                            if (publishTableInt == 0)
                            {
                                tournament.PublishTournamentTable = false;
                            }
                            else
                            {
                                tournament.PublishTournamentTable = true;
                            }
                        }

                        if (sqlDataReader["AgeCategoryDefinitionId"] != DBNull.Value)
                        {
                            tournament.AgeCategoryDefinitionId = Convert.ToInt32(sqlDataReader["AgeCategoryDefinitionId"]);
                        }

                        if (sqlDataReader["TournamentNumber"] != DBNull.Value)
                        {
                            tournament.Number = Convert.ToInt32(sqlDataReader["TournamentNumber"].ToString());
                        }
                    }
                }


                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return tournament;
            }
        }

        /// <summary>
        /// The get tournaments by season id.
        /// </summary>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetTournamentsBySeasonId(int seasonId)
        {
            var tournaments = new List<Tournament>();
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand(@"[Service_GetTournamentsBySeasonId]", sqlConnection) {CommandType = CommandType.StoredProcedure};

                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", seasonId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    while (sqlDataReader.Read())
                    {
                        try
                        {
                            var tournament = new Tournament
                            {
                                Id = Convert.ToInt32(sqlDataReader["TournamentId"]),
                                Name = sqlDataReader["TournamentName"].ToString(),
                                TypeId = Convert.ToInt32(sqlDataReader["TournamentTypeId"]),
                                SportId = Convert.ToInt32(sqlDataReader["SportId"]),
                                SeasonId = Convert.ToInt32(sqlDataReader["SeasonId"]),
                                DistrictId = Convert.ToInt32(sqlDataReader["DistrictId"]),
                                AgeCategoryId = Convert.ToInt32(sqlDataReader["AgeCategoryId"]),
                                GenderId = Convert.ToInt32(sqlDataReader["GenderId"])
                            };

                            if (sqlDataReader["TournamentNumber"] != DBNull.Value)
                            {
                                var tournamentNumber = sqlDataReader["TournamentNumber"].ToString();
                                if (tournamentNumber != string.Empty)
                                {
                                    tournament.Number = Convert.ToInt32(sqlDataReader["TournamentNumber"]);
                                }
                            }

                            tournaments.Add(tournament);
                        }
                        catch (Exception exception)
                        {
                            Logger.Info("An error occuredd while getting Tournaments by Season. Check error log for more details.");
                            ErrorLogger.Error(exception);
                        }
                    }
                }
                catch (Exception exception)
                {
                    Logger.Info("An error occuredd while getting Tournaments by Season. Check error log for more details.");
                    ErrorLogger.Error(exception);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return tournaments;
            }
        }

        /// <summary>
        /// The get tournaments by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetTournamentsBySportId(int sportId)
        {
            var tournaments = new List<Tournament>();
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("[Service_GetTournamentsBySportId]", sqlConnection) {CommandType = CommandType.StoredProcedure};

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    while (sqlDataReader.Read())
                    {
                        try
                        {
                            var tournament = new Tournament
                            {
                                Id = Convert.ToInt32(sqlDataReader["TournamentId"]),
                                Name = sqlDataReader["TournamentName"].ToString(),
                                TypeId = Convert.ToInt32(sqlDataReader["TournamentTypeId"]),
                                SportId = Convert.ToInt32(sqlDataReader["SportId"]),
                                SeasonId = Convert.ToInt32(sqlDataReader["SeasonId"]),
                                DistrictId = Convert.ToInt32(sqlDataReader["DistrictId"]),
                                AgeCategoryId = Convert.ToInt32(sqlDataReader["AgeCategoryId"]),
                                GenderId = Convert.ToInt32(sqlDataReader["GenderId"])
                            };
                            if (sqlDataReader["TournamentNumber"] != DBNull.Value)
                            {
                                if (sqlDataReader["TournamentNumber"].ToString() == string.Empty)
                                {
                                    tournament.Number = Convert.ToInt32(sqlDataReader["TournamentNumber"]);
                                }
                            }

                            tournaments.Add(tournament);
                        }
                        catch (Exception exception)
                        {
                            Logger.DebugFormat("Tournament Name {0} and number {1} and id {2}",
                                sqlDataReader["TournamentName"],
                                sqlDataReader["TournamentNumber"],
                                sqlDataReader["TournamentId"]);

                            ErrorLogger.Error(exception);
                        }
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return tournaments;
            }
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        public void InsertAll(List<Tournament> domainobject)
        {
            foreach (var tournament in domainobject)
            {
                this.InsertOne(tournament);
            }
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int InsertOne(Tournament domainobject)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                var sqlCommand = new SqlCommand(@"[Service_InsertTournament]", sqlConnection) {CommandType = CommandType.StoredProcedure};

                sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", domainobject.Id));
                sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", domainobject.DistrictId));
                sqlCommand.Parameters.Add(new SqlParameter("@DisciplineId", domainobject.DisciplineId));
                sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", domainobject.SeasonId));
                sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));
                sqlCommand.Parameters.Add(new SqlParameter("@TournamentName", domainobject.Name));
                sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryId", domainobject.AgeCategoryId));
                sqlCommand.Parameters.Add(new SqlParameter("@GenderId", domainobject.GenderId));
                sqlCommand.Parameters.Add(new SqlParameter("@TournamentNumber", domainobject.Number));
                sqlCommand.Parameters.Add(new SqlParameter("@TournamentTypeId", domainobject.TournamentTypeId));
                sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryDefinitionId", domainobject.AgeCategoryDefinitionId));
                sqlCommand.Parameters.Add(new SqlParameter("@Push", false));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                sqlCommand.ExecuteNonQuery();

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return 1;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        public void Update(Tournament domainobject)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand(@"[Service_UpdateTournament]", sqlConnection) {CommandType = CommandType.StoredProcedure};

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", domainobject.Id));
                    sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", domainobject.DistrictId));
                    sqlCommand.Parameters.Add(new SqlParameter("@DisciplineId", domainobject.DisciplineId));
                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", domainobject.SeasonId));
                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentName", domainobject.Name));
                    sqlCommand.Parameters.Add(new SqlParameter("@Push", false));
                    sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryId", domainobject.AgeCategoryId));
                    sqlCommand.Parameters.Add(new SqlParameter("@GenderId", domainobject.GenderId));
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentTypeId", domainobject.TypeId));
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentNumber", domainobject.Number));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Updates the tournament type
        /// </summary>
        /// <param name="domainobject">the tournament type object</param>
        public void UpdateTournamentType(Tournament domainobject)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand(@"[Service_UpdateTournamentType]", sqlConnection) {CommandType = CommandType.StoredProcedure};

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentTypeId", domainobject.TypeId));
                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", domainobject.Id));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void UpdateTournamentPublishStatus(Tournament tournament)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand(@"[Service_UpdateTournamentPublishStatus]", sqlConnection) {CommandType = CommandType.StoredProcedure};

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", tournament.Id));
                    sqlCommand.Parameters.Add(new SqlParameter("@PublishTournamentTable", tournament.PublishTournamentTable));
                    sqlCommand.Parameters.Add(new SqlParameter("@PublishResult", tournament.PublishResult));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void UpdateTournamentDisciplineId(Tournament tournament)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand(@"[Service_UpdateTournamentDiscipline]", sqlConnection) {CommandType = CommandType.StoredProcedure};

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", tournament.Id));
                    sqlCommand.Parameters.Add(new SqlParameter("@DisciplineId", tournament.DisciplineId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        #endregion
    }
}