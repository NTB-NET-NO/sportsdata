﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SeasonRepository.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The season repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;

namespace NTB.SportsData.Components.PublicRepositories
{
    /// <summary>
    /// The season repository.
    /// </summary>
    public class SeasonRepository : ISeasonDataMapper
    {
        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SeasonRepository));

        /// <summary>
        /// Initializes static members of the <see cref="SeasonRepository"/> class. 
        /// Initializes static members of the <see cref="MatchRepository"/> class.
        /// </summary>
        static SeasonRepository()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        public Season GetSeasonById(int seasonId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    // string SQLQuery = "SELECT SeasonID FROM Seasons";
                    var sqlCommand = new SqlCommand("[Service_GetAllSeasons]", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }
                    var season = new Season();
                    while (sqlDataReader.Read())
                    {
                        
                        if (sqlDataReader["SeasonID"] != DBNull.Value)
                        {
                            season.Id = Convert.ToInt32(sqlDataReader["SeasonID"].ToString());
                        }

                        if (sqlDataReader["SeasonName"] != DBNull.Value)
                        {
                            season.Name = sqlDataReader["SeasonName"].ToString();
                        }

                        if (sqlDataReader["SeasonStartDate"] != DBNull.Value)
                        {
                            season.StartDate = Convert.ToDateTime(sqlDataReader["SeasonStartDate"].ToString());
                        }

                        if (sqlDataReader["SeasonEndDate"] != DBNull.Value)
                        {
                            season.EndDate = Convert.ToDateTime(sqlDataReader["SeasonEndDate"].ToString());
                        }

                        if (sqlDataReader["SeasonActive"] != DBNull.Value)
                        {
                            season.Active = Convert.ToBoolean(sqlDataReader["SeasonActive"].ToString());
                        }

                        if (sqlDataReader["DisciplineId"] != DBNull.Value)
                        {
                            season.DisciplineId = Convert.ToInt32(sqlDataReader["DisciplineId"].ToString());
                        }

                        if (sqlDataReader["SportId"] != DBNull.Value)
                        {
                            season.SportId = Convert.ToInt32(sqlDataReader["SportId"].ToString());
                        }
                    }

                    return season;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);

                    return null;
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int InsertOne(Season domainobject)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand("[Service_InsertSeasons]", sqlConnection) { CommandType = CommandType.StoredProcedure };

                    var startDate = DateTime.Today.Date;
                    if (domainobject.StartDate.Year > 1900)
                    {
                        startDate = domainobject.StartDate;
                    }

                    var endDate = DateTime.Today.Date;
                    if (domainobject.StartDate.Year > 1900)
                    {
                        endDate = domainobject.EndDate;
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("SeasonId", domainobject.Id));
                    sqlCommand.Parameters.Add(new SqlParameter("SeasonName", domainobject.Name));
                    sqlCommand.Parameters.Add(new SqlParameter("SeasonActive", domainobject.Active));
                    sqlCommand.Parameters.Add(new SqlParameter("SeasonStartDate", startDate));
                    sqlCommand.Parameters.Add(new SqlParameter("SeasonEndDate", endDate));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.ExecuteNonQuery();

                    return 1;
                }
                catch (Exception exception)
                {
                    Logger.Error("An error has occured while inserting season: " + exception);
                    return 0;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        public void InsertAll(List<Season> domainobject)
        {
            foreach (var season in domainobject)
            {
                this.InsertOne(season);
            }
        }

        /// <summary>
        /// The get season by dates.
        /// </summary>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <param name="endDate">
        /// The end date.
        /// </param>
        /// <returns>
        /// The <see cref="Season"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// This method will throw an exception because it is not implemented yet
        /// </exception>
        public Season GetSeasonByDates(DateTime startDate, DateTime endDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get seasons by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Season> GetSeasonsBySportId(int sportId)
        {
            var seasons = new List<Season>();
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                var sqlCommand = new SqlCommand(@"Service_GetSeasonsBySportId", sqlConnection) { CommandType = CommandType.StoredProcedure };

                sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));
                sqlCommand.Parameters.Add(new SqlParameter("@DisciplineId", null));

                sqlConnection.Open();

                var sqlDataReader = sqlCommand.ExecuteReader();

                if (!sqlDataReader.HasRows)
                {
                    return seasons;
                }

                while (sqlDataReader.Read())
                {
                    var season = new Season
                                     {
                                         Id = Convert.ToInt32(sqlDataReader["SeasonId"]), 
                                         SportId = Convert.ToInt32(sqlDataReader["SportId"]), 
                                         Name = sqlDataReader["SeasonName"].ToString(), 
                                         Active = true, 
                                         StartDate = Convert.ToDateTime(sqlDataReader["SeasonStartDate"].ToString()), 
                                         EndDate = Convert.ToDateTime(sqlDataReader["SeasonEndDate"].ToString()), 
                                         DisciplineId = sqlDataReader["DisciplineId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DisciplineId"])
                                     };

                    var seasonactive = sqlDataReader["SeasonActive"].ToString().ToLower();

                    season.Active = true;
                    if (seasonactive == "false" || seasonactive == "0")
                    {
                        season.Active = false;
                    }

                    seasons.Add(season);
                }

                sqlDataReader.Close();

                sqlConnection.Close();
            }

            return seasons;
        }

        /// <summary>
        /// The get season by sport id and discipline id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <param name="disciplineId">
        /// The discipline id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Season> GetSeasonBySportIdAndDisciplineId(int sportId, int disciplineId)
        {
            var seasons = new List<Season>();

            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    var sqlCommand = new SqlCommand(@"Service_GetSeasonsBySportId", sqlConnection) { CommandType = CommandType.StoredProcedure };

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                    sqlCommand.Parameters.Add(new SqlParameter("@DisciplineId", disciplineId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    while (sqlDataReader.Read())
                    {
                        var season = new Season
                                         {
                                             Id = Convert.ToInt32(sqlDataReader["SeasonId"]), 
                                             SportId = Convert.ToInt32(sqlDataReader["SportId"]), 
                                             Name = sqlDataReader["SeasonName"].ToString(), 
                                             Active = true, 
                                             StartDate = Convert.ToDateTime(sqlDataReader["SeasonStartDate"].ToString()), 
                                             EndDate = Convert.ToDateTime(sqlDataReader["SeasonEndDate"].ToString()), 
                                             DisciplineId = sqlDataReader["DisciplineId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DisciplineId"])
                                         };
                        var seasonactive = sqlDataReader["SeasonActive"].ToString().ToLower();
                        season.Active = true;
                        if (seasonactive == "false" || seasonactive == "0")
                        {
                            season.Active = false;
                        }

                        seasons.Add(season);
                    }

                    sqlDataReader.Close();

                    return seasons;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return null;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public List<Season> GetAllSeasons()
        {
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    // string SQLQuery = "SELECT SeasonID FROM Seasons";
                    var sqlCommand = new SqlCommand("[Service_GetAllSeasons]", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    var seasons = new List<Season>();
                    while (sqlDataReader.Read())
                    {
                        var season = new Season();
                        if (sqlDataReader["SeasonID"] != DBNull.Value)
                        {
                            season.Id = Convert.ToInt32(sqlDataReader["SeasonID"].ToString());
                        }

                        if (sqlDataReader["SeasonName"] != DBNull.Value)
                        {
                            season.Name = sqlDataReader["SeasonName"].ToString();
                        }

                        if (sqlDataReader["SeasonStartDate"] != DBNull.Value)
                        {
                            season.StartDate = Convert.ToDateTime(sqlDataReader["SeasonStartDate"].ToString());
                        }

                        if (sqlDataReader["SeasonEndDate"] != DBNull.Value)
                        {
                            season.EndDate = Convert.ToDateTime(sqlDataReader["SeasonEndDate"].ToString());
                        }

                        if (sqlDataReader["SeasonActive"] != DBNull.Value)
                        {
                            season.Active = Convert.ToBoolean(sqlDataReader["SeasonActive"].ToString());
                        }

                        if (sqlDataReader["DisciplineId"] != DBNull.Value)
                        {
                            season.DisciplineId = Convert.ToInt32(sqlDataReader["DisciplineId"].ToString());
                        }

                        if (sqlDataReader["SportId"] != DBNull.Value)
                        {
                            season.SportId = Convert.ToInt32(sqlDataReader["SportId"].ToString());
                        }

                        seasons.Add(season);
                    }

                    return seasons;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);

                    return null;
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void InsertSeason(Season season)
        {
            using (
                var sqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
            {
                try
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    // string SQLQuery = "SELECT SeasonID FROM Seasons";
                    var sqlCommand = new SqlCommand("[Service_InsertSeasons]", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", season.Id));
                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonName", season.Name));
                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonActive", season.Active));
                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonStartDate", season.StartDate));
                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonEndDate", season.EndDate));
                    sqlCommand.Parameters.Add(new SqlParameter("@DisciplineId", season.DisciplineId));
                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", season.SportId));

                    sqlCommand.ExecuteNonQuery();

                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void InsertAllSeasons(List<Season> seasons)
        {
            foreach (var season in seasons)
            {
                this.InsertSeason(season);
            }
        }
    }
}