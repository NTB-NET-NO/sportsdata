﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;

namespace NTB.SportsData.Components.PublicRepositories
{
    internal class DocumentRepository : IRepository<Document>, IDocumentDataMapper
    {
        public static readonly ILog Logger = LogManager.GetLogger(typeof (DocumentRepository));

        public int InsertOne(Document domainobject)
        {
            // We must now add this to the database
            // sqlConnection = new SqlConnection(connectionString);
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("Service_InsertDocument", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", domainobject.TournamentId));

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));

                    // DONE: Change this to really get it from the database!

                    // Setting the version to 1
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentVersion", domainobject.Version));

                    // Setting the DocumentName
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentName", domainobject.Filename));

                    // Setting the DocumentName
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentCreated", DateTime.Now));

                    // Open the connection
                    sqlConnection.Open();

                    // Run the query
                    sqlCommand.ExecuteNonQuery();

                    return 1;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return 0;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void InsertAll(List<Document> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Document domainobject)
        {
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("Service_UpdateDocument", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };


                    // Setting the DocumentName
                    Logger.Debug("DocumentVersion: " + domainobject.Version);
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentVersion", domainobject.Version));

                    // Setting the TournamentId
                    Logger.Debug("TournamentId: " + domainobject.TournamentId);
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", domainobject.TournamentId));

                    // Setting the DocumentName
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentCreated", DateTime.Now));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }



                    // Run the query
                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }

                }
            }
        }

        public void Delete(Document domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Document> GetAll()
        {
            throw new NotImplementedException();
        }

        public Document Get(int id)
        {
            throw new NotImplementedException();
        }

        public int GetDocumentVersionByTournamentId(int tournamentId)
        {
            int documentVersion = 1;
            using (var sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("Service_GetDocumentByTournamentId", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                    Logger.Debug("GetDocumentByTournamentId: " + tournamentId);
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", tournamentId));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            documentVersion = Convert.ToInt32(sqlDataReader["DocumentVersion"]) + 1;
                        }
                    }

                    return documentVersion;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return 0;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }
    }
}
