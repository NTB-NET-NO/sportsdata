﻿using System;
using System.Collections.Generic;
using Glue;
using Glue.Converters;
using NTB.SportsData.Classes.Enums;

namespace NTB.SportsData.Components.Mappers
{
    public abstract class BaseMapper<TLeftType, TRightType>
    {
        private readonly Mapping<TLeftType, TRightType> _map;

        protected BaseMapper()
            : this(null, null)
        {

        }
        protected BaseMapper(Func<TRightType, TLeftType> creatorTowardsLeft, Func<TLeftType, TRightType> creatorTowardsRight)
        {
            this._map = new Mapping<TLeftType, TRightType>(creatorTowardsLeft, creatorTowardsRight);
            this.SetUpMapper(this._map);
        }

        protected abstract void SetUpMapper(Mapping<TLeftType, TRightType> mapper);

        public virtual TRightType Map(TLeftType from, TRightType to)
        {
            return this._map.Map(from, to);
        }

        public virtual TLeftType Map(TRightType from, TLeftType to)
        {
            return this._map.Map(from, to);
        }

        public Mapping<TLeftType, TRightType> GetMapper()
        {
            return this._map;
        }

        public void StringToIntConverter()
        {
            this._map.AddConverter(Converting.BetweenIntAndString());
        }

        public IConverter NullableIntToIntConverter()
        {
            return new QuickConverter<int?, int>(this.ConvertNullableIntToInt32, this.ConvertInt32ToNull);
        }

        private ChangeType ConvertRemoteChangeType(NIFConnectService.ChangeType changeType)
        {
            if (changeType == NIFConnectService.ChangeType.Created)
            {
                return ChangeType.Created;
            }

            if (changeType == NIFConnectService.ChangeType.Deleted)
            {
                return ChangeType.Deleted;
            }

            if (changeType == NIFConnectService.ChangeType.Modified)
            {
                return ChangeType.Modified;
            }

            return ChangeType.Unknown;
        }

        private NIFConnectService.ChangeType ConvertLocalChangeType(ChangeType changeType)
        {
            if (changeType == ChangeType.Created)
            {
                return NIFConnectService.ChangeType.Created;
            }

            if (changeType == ChangeType.Deleted)
            {
                return NIFConnectService.ChangeType.Deleted;
            }

            if (changeType == ChangeType.Modified)
            {
                return NIFConnectService.ChangeType.Modified;
            }

            return NIFConnectService.ChangeType.Unknown;
        }

        public IConverter RemoteChangeTypeConverter()
        {
            return new QuickConverter<NIFConnectService.ChangeType, ChangeType>(this.ConvertRemoteChangeType,
                this.ConvertLocalChangeType);
        }

        public IConverter RemoteEntityTypeConverter()
        {
            return new QuickConverter<NIFConnectService.EntityType, EntityType>(this.ConvertRemoteEntityType, this.ConvertLocalEntityType);
        }
        
        private NIFConnectService.EntityType ConvertLocalEntityType(EntityType entityType)
        {
            if (entityType == EntityType.Tournament)
            {
                return NIFConnectService.EntityType.Tournament;
            }

            if (entityType == EntityType.Course)
            {
                return NIFConnectService.EntityType.Course;
            }

            if (entityType == EntityType.Event)
            {
                return NIFConnectService.EntityType.Event;
            }

            if (entityType == EntityType.Function)
            {
                return NIFConnectService.EntityType.Function;
            }

            if (entityType == EntityType.License)
            {
                return NIFConnectService.EntityType.License;
            }

            if (entityType == EntityType.Match)
            {
                return NIFConnectService.EntityType.Match;
            }

            if (entityType == EntityType.MatchIncident)
            {
                return NIFConnectService.EntityType.MatchIncident;
            }

            if (entityType == EntityType.Organization)
            {
                return NIFConnectService.EntityType.Organization;
            }

            if (entityType == EntityType.Person)
            {
                return NIFConnectService.EntityType.Person;
            }

            if (entityType == EntityType.Qualification)
            {
                return NIFConnectService.EntityType.Qualification;
            }

            if (entityType == EntityType.Result)
            {
                return NIFConnectService.EntityType.Result;
            }

            if (entityType == EntityType.Team)
            {
                return NIFConnectService.EntityType.Team;
            }

            if (entityType == EntityType.Venue)
            {
                return NIFConnectService.EntityType.Venue;
            }

            return NIFConnectService.EntityType.Unknown;
        }

        private EntityType ConvertRemoteEntityType(NIFConnectService.EntityType entityType)
        {
            
            if (entityType == NIFConnectService.EntityType.Tournament)
            {
                return EntityType.Tournament;
            }

            if (entityType == NIFConnectService.EntityType.Course)
            {
                return EntityType.Course;
            }

            if (entityType == NIFConnectService.EntityType.Event)
            {
                return EntityType.Event;
            }

            if (entityType == NIFConnectService.EntityType.Function)
            {
                return EntityType.Function;
            }

            if (entityType == NIFConnectService.EntityType.License)
            {
                return EntityType.License;
            }

            if (entityType == NIFConnectService.EntityType.Match)
            {
                return EntityType.Match;
            }

            if (entityType == NIFConnectService.EntityType.MatchIncident)
            {
                return EntityType.MatchIncident;
            }

            if (entityType == NIFConnectService.EntityType.Organization)
            {
                return EntityType.Organization;
            }

            if (entityType == NIFConnectService.EntityType.Person)
            {
                return EntityType.Person;
            }

            if (entityType == NIFConnectService.EntityType.Qualification)
            {
                return EntityType.Qualification;
            }

            if (entityType == NIFConnectService.EntityType.Result)
            {
                return EntityType.Result;
            }

            if (entityType == NIFConnectService.EntityType.Team)
            {
                return EntityType.Team;
            }

            if (entityType == NIFConnectService.EntityType.Venue)
            {
                return EntityType.Venue;
            }

            return EntityType.Unknown;
        }

        public IConverter NullableBoolToBoolConverter()
        {
            return new QuickConverter<bool?, bool>(this.ConvertNullableBoolToBool, this.ConvertBoolToNullableBool);
        }

        private bool? ConvertBoolToNullableBool(bool b)
        {
            if (b == false)
            {
                return null;
            }

            return b;
        }

        private bool ConvertNullableBoolToBool(bool? b)
        {
            if (b == null)
            {
                return false;
            }

            if (b == false)
            {
                return false;
            }
            return true;
        }


        public IConverter ConvertNullableIntToStringConverter()
        {
            return new QuickConverter<int?, string>(this.ConvertNullableIntoToString, this.ConvertStringToNullableInt);
        }
        private int? ConvertStringToNullableInt(string s)
        {
            if (s == string.Empty)
            {
                return null;
            }

            return Convert.ToInt32(s);
        }

        private string ConvertNullableIntoToString(int? i)
        {
            if (i == null)
            {
                return string.Empty;
            }

            return i.ToString();
        }

        private int? ConvertInt32ToNull(int i)
        {
            if (i == 0)
            {
                return null;
            }

            return i;
        }

        private int ConvertNullableIntToInt32(int? i)
        {
            return i != null ? Convert.ToInt32(i) : 0;
        }

        public IConverter StringToDateTimeConverter()
        {
            return new QuickConverter<String, DateTime>(this.ConvertStringToDateTime, this.ConvertDateToString);
        }

        private string ConvertDateToString(DateTime dateTime)
        {
            return dateTime.ToLongDateString();
        }

        private DateTime ConvertStringToDateTime(string s)
        {
            return Convert.ToDateTime(s);
        }

        public IConverter StringToInt32Converter()
        {
            var converter = new QuickConverter<String, Int32>(this.ConvertStringToInt32, this.ConvertInt32ToString);

            return converter;
        }

        private string ConvertInt32ToString(int i)
        {
            return i.ToString();
        }

        private int ConvertStringToInt32(string s)
        {
            return s == string.Empty ? 0 : Convert.ToInt32(s);
        }

        public void YesNoConverter()
        {
            var converter = new QuickConverter<string, bool>(fromString => fromString != "N",
                                                             fromBool => fromBool ? "Y" : "N");

            // or ?
            this._map.AddConverter(converter);
            // return converter;
        }

        public IConverter BoolToStringConverter()
        {
            var converter = new QuickConverter<bool, string>(this.ConvertFromBoolToString, this.ConvertFromStringToBool);

            return converter;
        }

        private string ConvertFromBoolToString(bool value)
        {
            if (value)
            {
                return "true";
            }

            return "false";
        }

        private bool ConvertFromStringToBool(string value)
        {
            if (value == "true")
            {
                return true;
            }

            return false;
        }

        public IConverter StringToTimeIntConverter()
        {
            var converter = new QuickConverter<string, Int32>(this.ConvertStringToTimeInt, this.ConvertFromTimeIntToString);

            return converter;
        }

        private string ConvertFromTimeIntToString(int i)
        {
            // A Time String is like this: 10:00:00
            var timeString = i.ToString();

            return timeString;
        }

        private int ConvertStringToTimeInt(string s)
        {
            var dt = Convert.ToDateTime(s);
            var stringTime = dt.ToShortTimeString().Replace(":", string.Empty);
            return Convert.ToInt32(stringTime);

        }
    }
}
