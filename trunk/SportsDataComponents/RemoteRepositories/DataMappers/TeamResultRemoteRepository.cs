﻿using System;
using System.Configuration;
using log4net;
using NTB.SportsData.Components.Nif.Facade;
using NTB.SportsData.Components.NIFConnectService;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;

namespace NTB.SportsData.Components.RemoteRepositories.DataMappers
{
    /// <summary>
    ///     The team result remote repository.
    /// </summary>
    public class TeamResultRemoteRepository : ITeamResultDataMapper
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SingleSportFacade));

        /// <summary>
        /// The get match result by result id.
        /// </summary>
        /// <param name="resultId">
        /// The result id.
        /// </param>
        /// <returns>
        /// The <see cref="Result1"/>.
        /// </returns>
        public Result1 GetMatchResultByResultId(int resultId)
        {
            var client = new TournamentPublicServiceClient();
            try
            {
                if (client.ClientCredentials == null)
                {
                    throw new SportsDataException("Event Client not ready. Client Credentials are null");
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                var request = new ResultIdRequest4(resultId);
                var response = client.MatchResultGet(request);

                client.Close();
                return !response.Success ? null : response.Result;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                client.Abort();
                throw;
            }
        }
    }
}