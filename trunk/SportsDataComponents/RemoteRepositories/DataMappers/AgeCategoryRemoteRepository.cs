﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using log4net;
using NTB.SportsData.Components.NIFConnectService;

namespace NTB.SportsData.Components.RemoteRepositories.DataMappers
{
    public class AgeCategoryRemoteRepository
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(AgeCategoryRemoteRepository));

        /// <summary>
        ///     Creating the static Error Logger
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");


        public List<TournamentClass> GetClassCodeByCodeId(int classId)
        {
            var client = new TournamentServiceClient();
            try
            {
                if (client.ClientCredentials == null)
                {
                    return new List<TournamentClass>();
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                var request = new TournamentClassIdsRequest
                {
                    TournamentClassIds = new[]
                    {
                        classId
                    }
                };


                var response = client.GetClassByClassIds(request);

                client.Close();
                return !response.Success ? null : response.TournamentClassList.ToList();
            }
            catch (Exception exception)
            {
                ErrorLogger.Error(exception);

                Logger.Info("We are closing the connection because of a connection error");

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }

                return null;
            }
        }
    }
}
