﻿using System;
using System.Configuration;
using System.ServiceModel;
using log4net;
using NTB.SportsData.Components.NIFConnectService;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;

namespace NTB.SportsData.Components.RemoteRepositories.DataMappers
{
    /// <summary>
    /// The person remote repository.
    /// </summary>
    public class PersonRemoteRepository : IPersonRemoteDataMapper
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ClassExerciseRemoteRepository));

        /// <summary>
        /// The get person by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Person"/>.
        /// </returns>
        public Person GetPersonById(int id)
        {
            var client = new PersonServiceClient();
            try
            {
                if (client.ClientCredentials == null)
                {
                    throw new SportsDataException("Person Client not ready. Client Credentials are null");
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                var request = new PersonIdRequest4() {Id = id};

                var response = client.GetPersonById(request);

                if (!response.Success)
                {
                    return new Person();
                }

                return response.Person;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }

                return new Person();
            }
        }
    }
}