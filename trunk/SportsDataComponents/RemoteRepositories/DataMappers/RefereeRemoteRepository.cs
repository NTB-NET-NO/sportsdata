﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using log4net;
using NTB.SportsData.Components.Nif.Mappers;
using NTB.SportsData.Components.NIFConnectService;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;

namespace NTB.SportsData.Components.RemoteRepositories.DataMappers
{
    /// <summary>
    /// The referee remote repository.
    /// </summary>
    public class RefereeRemoteRepository : IRefereeRemoteDataMapper
    {
        
        #region Static Fields

        /// <summary>
        /// The error logger.
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(RefereeRemoteRepository));

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get referee by match id.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Classes.Classes.Referee> GetRefereeByMatchId(int matchId)
        {
            var client = new ResultTeamServiceClient();
            try
            {
                if (client.ClientCredentials == null)
                {
                    return null;
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                var request = new MatchIdRequest {MatchId = matchId};

                var response = client.GetMatchReferees(request);

                if (!response.Success)
                {
                    return null;
                }

                var mapper = new RefereeMapper();

                // Referee[] result = response.Referees;
                var result = response.RefereesPublic.ToList();

                client.Close();

                return result.Select(row => mapper.Map(row, new Classes.Classes.Referee()))
                    .ToList();
            }
            catch (Exception exception)
            {
                Logger.Error("An error has occured. Check the error log for errors");
                ErrorLogger.Error(exception);

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }

                return null;
            }
        }

        #endregion
    }
}