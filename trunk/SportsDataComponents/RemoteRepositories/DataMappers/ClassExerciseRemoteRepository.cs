﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClassExerciseRemoteRepository.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The class exercise remote repository.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.ServiceModel;

namespace NTB.SportsData.Components.RemoteRepositories.DataMappers
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;

    using log4net;
    
    using NIFConnectService;
    using Interfaces;

    /// <summary>
    /// The class exercise remote repository.
    /// </summary>
    public class ClassExerciseRemoteRepository : IClassExerciseRemoteDataMapper
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ClassExerciseRemoteRepository));

        /// <summary>
        ///     Creating the static Error Logger
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        /// <summary>
        /// The get event class exercises.
        /// </summary>
        /// <param name="eventId">
        /// The event id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<ClassExercise> GetEventClassExercises(int eventId)
        {
            var client = new ClassExerciseServiceClient();

            try
            {
                if (client.ClientCredentials == null)
                {
                    Logger.Error("Class Exercise Client not ready. Client Credentials are null");
                    throw new SportsDataException("Class Exercise Client not ready. Client Credentials are null");
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                var request = new EventIdRequest4() {EventId = eventId};

                var response = client.GetClassExercisesByEventId(request);

                client.Close();

                if (!response.Success)
                {
                    return new List<ClassExercise>();
                }

                return response.ClassExercises.ToList();
            }
            catch (Exception exception)
            {
                ErrorLogger.Error(exception);

                Logger.Info("We are closing the connection because of a connection error");

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }

                return null;
            }
        }
    }
}