﻿using System;
using System.Configuration;
using System.ServiceModel;
using log4net;
using log4net.Config;
using NTB.SportsData.Components.NIFConnectService;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;

namespace NTB.SportsData.Components.RemoteRepositories.DataMappers
{
    /// <summary>
    /// The org remote repository.
    /// </summary>
    public class OrgRemoteRepository : IOrganizationRemoteDataMapper
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(OrgRemoteRepository));
        
        /// <summary>
        /// The get organization by org id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see cref="Federation"/>.
        /// </returns>
        public Org GetOrganizationByOrgId(int orgId)
        {
            var client = new OrgServiceClient();
            try
            {
                if (client.ClientCredentials == null)
                {
                    return null;
                }

                // Get service username
                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];

                // Get service password
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                var request = new OrgIdRequest()
                {
                    OrgId = orgId
                };

                var response = client.GetOrg(request);

                client.Close();
                
                if (!response.Success)
                {
                    return null;
                }

                return response.Org;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }

                return null;
            }
        }

        /// <summary>
        /// The get organization by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see cref="Federation"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public Org GetOrganizationBySportId(int sportId)
        {
            throw new NotImplementedException();
        }
    }
}