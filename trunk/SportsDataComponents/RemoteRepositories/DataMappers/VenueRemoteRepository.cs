﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using log4net;
using NTB.SportsData.Components.Nif.Mappers;
using NTB.SportsData.Components.NIFConnectService;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;
using ActivityArea = NTB.SportsData.Classes.Classes.ActivityArea;
using VenueExtended = NTB.SportsData.Classes.Classes.VenueExtended;
using VenueUnit = NTB.SportsData.Classes.Classes.VenueUnit;

namespace NTB.SportsData.Components.RemoteRepositories.DataMappers
{
    /// <summary>
    /// The venue remote repository.
    /// </summary>
    public class VenueRemoteRepository : IRepository<VenueExtended>, IVenueRemoteDataMapper
    {
        /// <summary>
        /// The error logger.
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(VenueRemoteRepository));

        /// <summary>
        /// The get venue activity area by id.
        /// </summary>
        /// <param name="activityAreaId">
        /// The activity area id.
        /// </param>
        /// <returns>
        /// The <see cref="ActivityArea"/>.
        /// </returns>
        public ActivityArea GetVenueActivityAreaById(int activityAreaId)
        {
            var client = new VenueServiceClient();
            try
            {
                if (client.ClientCredentials == null)
                {
                    return null;
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];
            
                var mapper = new ActivityAreaMapper();

                var request = new ActivityAreaIdRequest() {ActivityAreaId = activityAreaId};
                var response = client.GetActivityArea(request);

                client.Close();
                if (!response.Success)
                {
                    return null;
                }

                var result = response.ActivityArea;

                // return response.ActivityArea;
                return mapper.Map(result, new ActivityArea());
            }
            catch (Exception exception)
            {
                Logger.Error(exception);

                Logger.Info("We are closing the connection because of a connection error");

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }

                return null;
            }
        }

        /// <summary>
        /// The get venue unit by id.
        /// </summary>
        /// <param name="venueUnitId">
        /// The venue unit id.
        /// </param>
        /// <returns>
        /// The <see cref="VenueUnit"/>.
        /// </returns>
        public VenueUnit GetVenueUnitById(int venueUnitId)
        {
            var client = new VenueServiceClient();
            try
            {
                if (client.ClientCredentials == null)
                {
                    return null;
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                var mapper = new VenueUnitMapper();

                var request = new VenueUnitIdRequest() {VenueUnitId = venueUnitId};

                var response = client.GetVenueUnit(request);

                client.Close();

                if (!response.Success)
                {
                    return null;
                }

                var result = response.VenueUnit;

                // return response.ActivityArea;
                return mapper.Map(result, new VenueUnit());
            }
            catch (Exception exception)
            {
                Logger.Error(exception);

                Logger.Info("We are closing the connection because of a connection error");

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }

                return null;
            }
        }

        /// <summary>
        /// The get venue by id.
        /// </summary>
        /// <param name="venueId">
        /// The venue id.
        /// </param>
        /// <returns>
        /// The <see cref="Venue"/>.
        /// </returns>
        public Classes.Classes.Venue GetVenueById(int venueId)
        {
            var client = new VenueServiceClient();
            try
            {
                if (client.ClientCredentials == null)
                {
                    return null;
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                var mapper = new VenueMapper();

                var request = new VenueIdRequest() {VenueId = venueId};

                var response = client.GetVenue(request);

                client.Close();
                if (!response.Success)
                {
                    return null;
                }

                var result = response.Venue;

                return mapper.Map(result, new Classes.Classes.Venue());
            }
            catch (Exception exception)
            {
                Logger.Error(exception);

                Logger.Info("We are closing the connection because of a connection error");

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }

                return null;
            }
        }
        
        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// not implemented
        /// </exception>
        public void Delete(VenueExtended domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Venue"/>.
        /// </returns>
        public VenueExtended Get(int id)
        {
            var client = new VenueServiceClient();
            try
            {
                if (client.ClientCredentials == null)
                {
                    return null;
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                if (id == 0)
                {
                    return new VenueExtended();
                }

                var searchParams = new VenueSearchParams {VenueId = id};

                var request = new VenueSearchRequest {SearchParams = searchParams};

                var response = client.SearchVenue(request);

                if (!response.Success)
                {
                    return null;
                }

                var mapper = new VenueExtendedMapper();

                var result = response.Venues;

                return !result.Any()
                    ? null
                    : result.Select(row => mapper.Map(row, new VenueExtended()))
                        .Single();
            }
            catch (Exception exception)
            {
                Logger.Info("An error occured while trying to get matches from remote server. Check error log for details");
                ErrorLogger.Error(exception.Message);
                ErrorLogger.Error(exception.StackTrace);

                if (exception.InnerException != null)
                {
                    ErrorLogger.Error("The Inner exception information");
                    ErrorLogger.Error(exception.InnerException.Message);
                    ErrorLogger.Error(exception.InnerException.StackTrace);
                }

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }

                return null;
            }
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// not implemented
        /// </exception>
        public IQueryable<VenueExtended> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// not implemented
        /// </exception>
        public void InsertAll(List<VenueExtended> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// not implemented
        /// </exception>
        public int InsertOne(VenueExtended domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// not implemented
        /// </exception>
        public void Update(VenueExtended domainobject)
        {
            throw new NotImplementedException();
        }

    }
}