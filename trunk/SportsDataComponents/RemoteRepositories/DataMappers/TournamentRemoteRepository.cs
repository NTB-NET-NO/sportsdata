﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Text;
using log4net;
using NTB.SportsData.Components.NIFConnectService;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;

namespace NTB.SportsData.Components.RemoteRepositories.DataMappers
{
    /// <summary>
    /// The tournament remote repository.
    /// </summary>
    public class TournamentRemoteRepository : IRepository<Tournament>, ITournamentRemoteDataMapper
    {
        /// <summary>
        /// The error logger.
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(TournamentRemoteRepository));

        public List<TournamentClasses> GetTournamentClassesByTournamentid(int tournamentId)
        {
            var client = new TournamentServiceClient();
            try
            {
                if (client.ClientCredentials == null)
                {
                    throw new SportsDataException("Could not connect to the NIF API!");
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];


                var request = new TournamentIdRequest2
                {
                    TournamentId = tournamentId
                };
                var response = client.GetTournamentClassesByTournamentId(request);
                if (!response.Success)
                {
                    return null;
                }
                return response.TournamentClassesList.ToList();
            }
            catch (Exception exception)
            {
                Logger.Info("An error occured. Check the error log");
                ErrorLogger.Error(exception);
                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }

                return null;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public void Delete(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Tournament"/>.
        /// </returns>
        public Tournament Get(int id)
        {
            var client = new TournamentServiceClient();
            try
            {
                if (client.ClientCredentials == null)
                {
                    throw new SportsDataException("Could not connect to the NIF API!");
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                var request = new TournamentIdRequest2()
                {
                    TournamentId = id
                };
                var response = client.GetTournament(request);

                client.Close();
                if (!response.Success)
                {
                    return null;
                }

                return response.Tournament;
            }
            catch (Exception exception)
            {
                Logger.Info("An error occured. Check the error log");
                ErrorLogger.Error(exception);
                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }

                return null;
            }
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public IQueryable<Tournament> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get season tournaments.
        /// </summary>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetSeasonTournaments(int seasonId)
        {
            var client = new SeasonServiceClient();

            try
            {
                if (client.ClientCredentials == null)
                {
                    throw new SportsDataException("Could not connect to the NIF API!");
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                var request = new SeasonIdRequest {SeasonId = seasonId};

                var response = client.GetSeasonTournaments(request);

                client.Close();

                if (!response.Success)
                {
                    return null;
                }

                return response.Tournaments.ToList();
            }
            catch (Exception exception)
            {
                Logger.Info("An error has occurred. Check the error logger");
                ErrorLogger.Error(exception);

                client.Abort();
                return null;
            }
        }

        /// <summary>
        /// The get tournament table.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<TeamResult1> GetPublicTournamentTable(int tournamentId)
        {
            var client = new TournamentPublicServiceClient();
            try
            {
                if (client.ClientCredentials == null)
                {
                    throw new SportsDataException("Could not connect to the NIF API!");
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                // Set the TournamentId Request object with the tournament Id
                var request = new TournamentTableRequest2 { TournamentId = tournamentId };

                // Call the api to get the Tournament Table
                var response = client.ResultTableGet(request);

                client.Close();

                // If we receive an error, we shall print out the log and return an empty list
                if (!response.Success)
                {
                    ErrorLogger.Error(response.ErrorMessage);
                    return new List<TeamResult1>();
                }

                // We fetch the table from the response
                var result = response.Results;

                // ... and return the result as a List (we can do this in one operation, but this is more readable)
                return result.ToList();
            }
            catch (Exception exception)
            {
                var st = new StackTrace();
                var sf = st.GetFrame(0);

                var currentMethodName = sf.GetMethod();
                var sb = new StringBuilder();
                var mailMessage = string.Format("An error has occured in the method {0}", currentMethodName);
                sb.Append(mailMessage + Environment.NewLine);
                sb.Append(Environment.NewLine);
                sb.Append("Exception: " + Environment.NewLine);
                sb.Append(exception.Message + Environment.NewLine);
                sb.Append(Environment.NewLine);
                sb.Append("StackTrace: " + Environment.NewLine);
                sb.Append(exception.StackTrace + Environment.NewLine);

                Utilities.Mail.SendMail(sb.ToString());

                Logger.Info(
                    "An error occured while trying to get Tournament Table from remote server. Check error log for details");
                ErrorLogger.Error(exception.Message);
                ErrorLogger.Error(exception.StackTrace);

                if (exception.InnerException != null)
                {
                    ErrorLogger.Error("The Inner exception information");
                    ErrorLogger.Error(exception.InnerException.Message);
                    ErrorLogger.Error(exception.InnerException.StackTrace);
                }

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }

                return null;
            }
        }

        /// <summary>
        /// The get tournament table.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<TeamResult> GetTournamentTable(int tournamentId)
        {
            var client = new TournamentServiceClient();
            try
            {
                if (client.ClientCredentials == null)
                {
                    throw new SportsDataException("Could not connect to the NIF API!");
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                // Set the TournamentId Request object with the tournament Id
                var request = new TournamentTableRequest {TournamentId = tournamentId};

                // Call the api to get the Tournament Table
                var response = client.GetTournamentResults(request);

                client.Close();
                // If we receive an error, we shall print out the log and return an empty list
                if (!response.Success)
                {
                    ErrorLogger.Error(response.ErrorMessage);
                    return new List<TeamResult>();
                }

                // We fetch the table from the response
                var result = response.Results;

                // ... and return the result as a List (we can do this in one operation, but this is more readable)
                return result.ToList();
            }
            catch (Exception exception)
            {
                var st = new StackTrace();
                var sf = st.GetFrame(0);

                var currentMethodName = sf.GetMethod();
                var sb = new StringBuilder();

                sb.AppendFormat("An error has occured in the method {0}{1}{2}", currentMethodName, Environment.NewLine,Environment.NewLine);
                sb.AppendFormat("Tournament: {0}{1}{2}", tournamentId, Environment.NewLine, Environment.NewLine);
                sb.AppendFormat("Exception: {0}{1}{2}", Environment.NewLine, exception.Message, Environment.NewLine);
                sb.AppendFormat("StackTrace: {0}{1}{2}", Environment.NewLine, exception.StackTrace, Environment.NewLine);
                

                Utilities.Mail.SendMail(sb.ToString());

                Logger.Info(
                    "An error occured while trying to get Tournament Table from remote server. Check error log for details");
                ErrorLogger.Error(exception.Message);
                ErrorLogger.Error(exception.StackTrace);

                if (exception.InnerException != null)
                {
                    ErrorLogger.Error("The Inner exception information");
                    ErrorLogger.Error(exception.InnerException.Message);
                    ErrorLogger.Error(exception.InnerException.StackTrace);
                }

                client.Close();
                return null;
            }
        }

        public Tournament1 GetTournamentByMatchId(int matchId)
        {
            var client = new TournamentPublicServiceClient();
            try
            {
                if (client.ClientCredentials == null)
                {
                    throw new SportsDataException("Could not connect to the NIF API!");
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                var matchIdRequest = new MatchIdRequest8(matchId);
                var matchResponse = client.MatchGet(matchIdRequest);

                // Set the TournamentId Request object with the tournament Id
                var tournamentId = 0;
                if (matchResponse.Success)
                {
                    tournamentId = matchResponse.Match.TournamentId;
                }
                
                var request = new TournamentIdRequest10(tournamentId);

                // Call the api to get the Tournament Table
                var response = client.TournamentGet(request);

                client.Close();
                // If we receive an error, we shall print out the log and return an empty list
                if (!response.Success)
                {
                    ErrorLogger.Error(response.ErrorMessage);
                    return new Tournament1();
                }

                // We fetch the table from the response
                var result = response.Tournament;

                // ... and return the result as a List (we can do this in one operation, but this is more readable)
                return result;
            }
            catch (Exception exception)
            {
                Logger.Info(
                    "An error occured while trying to get Tournament Table from remote server. Check error log for details");
                ErrorLogger.Error(exception.Message);
                ErrorLogger.Error(exception.StackTrace);

                if (exception.InnerException != null)
                {
                    ErrorLogger.Error("The Inner exception information");
                    ErrorLogger.Error(exception.InnerException.Message);
                    ErrorLogger.Error(exception.InnerException.StackTrace);
                }

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }

                return null;
            }
        }

        /// <summary>
        /// The get tournament byid.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see cref="Tournament"/>.
        /// </returns>
        public Tournament1 GetTournamentById(int tournamentId)
        {
            var client = new TournamentPublicServiceClient();
            try
            {
                if (client.ClientCredentials == null)
                {
                    throw new SportsDataException("Could not connect to the NIF API!");
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                // Set the TournamentId Request object with the tournament Id
                var request = new TournamentIdRequest10() {TournamentId = tournamentId};

                // Call the api to get the Tournament Table
                var response = client.TournamentGet(request);

                client.Close();
                // If we receive an error, we shall print out the log and return an empty list
                if (!response.Success)
                {
                    ErrorLogger.Error(response.ErrorMessage);
                    return new Tournament1();
                }

                // We fetch the table from the response
                var result = response.Tournament;

                // ... and return the result as a List (we can do this in one operation, but this is more readable)
                return result;
            }
            catch (Exception exception)
            {
                Logger.Info(
                    "An error occured while trying to get Tournament Table from remote server. Check error log for details");
                ErrorLogger.Error(exception.Message);
                ErrorLogger.Error(exception.StackTrace);

                if (exception.InnerException != null)
                {
                    ErrorLogger.Error("The Inner exception information");
                    ErrorLogger.Error(exception.InnerException.Message);
                    ErrorLogger.Error(exception.InnerException.StackTrace);
                }

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }

                return null;
            }
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public void InsertAll(List<Tournament> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public int InsertOne(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public void Update(Tournament domainobject)
        {
            throw new NotImplementedException();
        }
    }
}