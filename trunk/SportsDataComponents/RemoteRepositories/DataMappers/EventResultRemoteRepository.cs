﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.NIFConnectService;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;
using EventClass = NTB.SportsData.Classes.Classes.EventClass;
using Result = NTB.SportsData.Components.NIFConnectService.Result;

namespace NTB.SportsData.Components.RemoteRepositories.DataMappers
{
    /// <summary>
    ///     The event result remote repository.
    /// </summary>
    public class EventResultRemoteRepository : IEventResultRemoteDataMapper
    {
        /// <summary>
        ///     Creating the static Error Logger
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        /// <summary>
        ///     Creating the static Logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(EventResultRemoteRepository));


        public Result GetResultByResultId(int resultId)
        {
            var client = new ResultServiceClient();
            try
            {
                if (client.ClientCredentials == null)
                {
                    throw new SportsDataException("Result Client not ready. Client Credentials are null");
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                var request = new ResultIdRequest() {ResultId = resultId};
                var response = client.GetResult(request);

                if (!response.Success)
                {
                    return new Result();
                }

                var eventResponse = response.Result;

                client.Close();
                return eventResponse;
            }
            catch (Exception exception)
            {

                ErrorLogger.Error(exception);

                Logger.Info("We are closing the connection because of a connection error");

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }

                return new Result();
            }
        }

        /// <summary>
        /// The get event result by event id.
        /// </summary>
        /// <param name="eventId">
        /// The event id.
        /// </param>
        /// <param name="classExerciseId">
        /// The class Exercise Id.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>Dictionary</cref>
        ///     </see>
        ///     .
        /// </returns>
        /// <exception cref="Exception">
        /// Throws error message from API
        /// </exception>
        public Dictionary<EventClass, List<Athlete>> GetEventResultByEventId(int eventId, int classExerciseId)
        {
            var client = new ResultServiceClient();

            try
            {
                if (client.ClientCredentials == null)
                {
                    throw new SportsDataException("Result Client not ready. Client Credentials are null");
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                var orgClient = new OrgServiceClient();
                if (orgClient.ClientCredentials == null)
                {
                    throw new SportsDataException("Could not connect to Organization client!");
                }

                orgClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                orgClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                var request = new ResultByEventIdRequest {EventId = eventId, ClassExerciseId = classExerciseId};

                var response = client.GetResultsByEventIdOrClassExerciseId(request);

                client.Close();

                if (!response.Success)
                {
                    throw new SportsDataException(response.ErrorMessage);
                }

                var results = response.Results;
                var filteredResult = from myResult in results group myResult by myResult.ClassExerciseId into newGroup orderby newGroup.Key select newGroup;

                // response.Results.SetValu GroupBy(x => x.ClassExerciseId);
                var listOfClasses = new Dictionary<int, string>();

                var listOfResults = new Dictionary<EventClass, List<Athlete>>();
                foreach (var groups in filteredResult)
                {
                    var name = groups.First()
                        .ClassExerciseName;
                    var id = groups.First()
                        .ClassExerciseId;
                    listOfClasses.Add(id, name);

                    var eventClass = new EventClass {ClassId = id, ClassName = name};

                    var athletes = new List<Athlete>();
                    foreach (var result in groups.Where(x => x.Rank != null))
                    {
                        var orgRequest = new OrgIdRequest {OrgId = result.ClubId};

                        var orgResponse = orgClient.GetOrgAdvanced(orgRequest);

                        orgClient.Close();
                        var club = new Club();
                        if (orgResponse.Success)
                        {
                            var org = orgResponse.Club;

                            club.Address1 = org.AddressField1;
                            club.Address2 = org.AddressField2;
                            club.City = org.AddressField3;
                            club.Postal = org.AddressField4;
                            club.Name = org.OrgName;
                            club.ClubId = org.OrgID;
                            club.RegionName = org.RegionName;
                        }

                        var athlete = new Athlete
                        {
                            Rank = result.Rank,
                            FirstName = result.FirstName,
                            LastName = result.LastName,
                            ClubId = result.ClubId,
                            ClubName = result.Club,
                            CompetitorId = result.CompetitorId,
                            PersonId = result.PersonId,
                            SportEventId = result.EventId,
                            Nationality = result.Nationality,
                            StartNo = result.StartNo,
                            Team = result.Team,
                            TeamId = result.TeamId,
                            Time = result.Time,
                            TimeBehind = result.TimeBehind,
                            Club = club
                        };

                        athletes.Add(athlete);
                    }

                    listOfResults.Add(eventClass, athletes);
                }

                return listOfResults;
            }
            catch (Exception exception)
            {
                ErrorLogger.Error(exception);

                Logger.Info("We are closing the connection because of a connection error");

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }

                return null;
            }
        }
    }
}