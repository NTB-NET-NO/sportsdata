﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchRemoteRepository.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Components.RemoteRepositories.DataMappers
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.ServiceModel;

    using log4net;

    using NIFConnectService;
    using Interfaces;

    /// <summary>
    ///     The match remote repository.
    /// </summary>
    public class MatchRemoteRepository : IMatchRemoteDataMapper
    {
        /// <summary>
        ///     Creating the static Error Logger
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        /// <summary>
        ///     Creating the static Logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(MatchRemoteRepository));

        /// <summary>
        /// The get all.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<TournamentMatchExtended> GetAll(int tournamentId)
        {
            var client = new TournamentServiceClient();
            try
            {
                if (client.ClientCredentials == null)
                {
                    throw new SportsDataException("Could not connect to the NIF API");
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                var request = new TournamentMatchRequest
                {
                    TournamentId = tournamentId
                };
                var response = client.GetTournamentMatches(request);

                client.Close();
                return !response.Success ? null : response.TournamentMatch.ToList();
            }
            catch (Exception exception)
            {
                Logger.Error(exception);

                Logger.Info("We are closing the connection because of a connection error");

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }
            }

            return null;
        }

        /// <summary>
        /// The get match information.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <returns>
        /// The <see cref="MatchSummary"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// throws an exception because we haven't implemented this procedure.
        /// </exception>
        public MatchSummary GetMatchInformation(int matchId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get matches by date interval.
        /// </summary>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <param name="endDate">
        /// The end date.
        /// </param>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<MatchSearchResult> GetMatchesByDateInterval(DateTime startDate, DateTime endDate, int tournamentId)
        {
            var client = new MatchServiceClient();
            try
            {
                if (client.ClientCredentials == null)
                {
                    throw new SportsDataException("Could not connect to the NIF API"); 
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                Logger.DebugFormat("Setting parameters for request. FromDate: {0}, ToDate: {1}, TournamentId: {2}", startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"), tournamentId);

                var request = new MatchSearchRequest
                                  {
                                      Criteria = new MatchSearchCriteria
                                                     {
                                                         MatchDateFrom = startDate, 
                                                         MatchDateTo = endDate, 
                                                         TournamentId = tournamentId
                                                     }
                                  };

                var response = client.MatchSearch(request);

                Logger.Debug("Returning response from query");

                client.Close();
                return !response.Success ? null : response.Matches.ToList();
            }
            catch (Exception exception)
            {
                Logger.Info("An error occured while trying to get matches from remote server. Check error log for details");
                ErrorLogger.Error(exception.Message);
                ErrorLogger.Error(exception.StackTrace);

                if (exception.InnerException != null)
                {
                    ErrorLogger.Error("The Inner exception information");
                    ErrorLogger.Error(exception.InnerException.Message);
                    ErrorLogger.Error(exception.InnerException.StackTrace);
                }

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }

                return null;
            }
        }

        /// <summary>
        /// The get partial results.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<PartialResult> GetPartialResults(int matchId)
        {
            var client = new ResultTeamServiceClient();
            
            try
            {
                if (client.ClientCredentials == null)
                {
                    throw new SportsDataException("Could not connect to the NIF API");
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                var request = new MatchIdRequest(matchId);

                var response = client.GetPartialResultsForMatch(request);

                client.Close();
                return !response.Success ? null : response.PartialResults.ToList();
            }
            catch (Exception exception)
            {
                ErrorLogger.Error(exception);

                Logger.Info("We are closing the connection because of a connection error");

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }
                return null;
            }
        }

        public List<MatchSearchResult1> SearchMatchesByDateAndTournamentId(int tournamentId, DateTime startDate, DateTime endDate)
        {
            var client = new TournamentPublicServiceClient();
            try
            {
                if (client.ClientCredentials == null)
                {
                    throw new SportsDataException("Could not connect to the NIF API");
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                Logger.DebugFormat("Setting parameters for request. FromDate: {0}, ToDate: {1}, TournamentId: {2}", startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"), tournamentId);

                var request = new MatchSearchRequest2
                {
                    Criteria = new MatchSearchCriteria1
                    {
                        MatchDateFrom = startDate,
                        MatchDateTo = endDate,
                        TournamentNameOrIds = new [] {tournamentId.ToString()}
                    }
                };

                var response = client.MatchSearch(request);

                Logger.Debug("Returning response from query");

                client.Close();
                return !response.Success ? null : response.Matches.ToList();
            }
            catch (Exception exception)
            {
                ErrorLogger.Error(exception);
                
                Logger.Info("We are closing the connection because of a connection error");

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }

                return new List<MatchSearchResult1>();
            }
        }

        /// <summary>
        /// The get matches by tournament id.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Match1> GetMatchesByTournamentId(int tournamentId)
        {
            var client = new MatchServiceClient();
            
            try
            {
                if (client.ClientCredentials == null)
                {
                    throw new SportsDataException("Could not connect to the NIF API");
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                var request = new MatchSummeriesForTournamentRequest(false, tournamentId);
                var result = client.GetMatchesForTournament(request);

                client.Close();

                if (!result.Success)
                {
                    return null;
                }

                // Filter the result perhaps?
                var matches = result.Matches;
                
                return matches.ToList();
            }
            catch (Exception exception)
            {
                ErrorLogger.Error(exception);

                Logger.Info("We are closing the connection because of a connection error");

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }

                return null;
            }
        }

        /// <summary>
        /// The get match summaries by tournament id.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<MatchSummary> GetMatchSummariesByTournamentId(int tournamentId)
        {
            var client = new MatchServiceClient();
            try
            {
                if (client.ClientCredentials == null)
                {
                    throw new SportsDataException("Could not connect to the NIF API"); 
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                var request = new MatchSummeriesForTournamentRequest(false, tournamentId);
                var result = client.GetMatchSummariesForTournament(request);

                client.Close();
                if (!result.Success)
                {
                    return null;
                }

                // Filter the result perhaps?
                var matchSummaries = result.MatchSummaries;

                return matchSummaries.ToList();
            }
            catch (Exception exception)
            {
                ErrorLogger.Error(exception);

                Logger.Info("We are closing the connection because of a connection error");

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }

                return null;
            }
        }

        /// <summary>
        /// The get match info by match id.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <returns>
        /// The <see cref="MatchInfo"/>.
        /// </returns>
        public Match GetMatchInfoByMatchId(int matchId)
        {
            var client = new TournamentPublicServiceClient();
            try
            {
                if (client.ClientCredentials == null)
                {
                    throw new SportsDataException("Could not connect to the NIF API");
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                var request = new MatchIdRequest8
                                  {
                                      MatchId = matchId
                                  };

                var result = client.MatchGet(request);

                client.Close();
                if (!result.Success)
                {
                    Logger.DebugFormat("Message from NIF API: {0}, error code: {1}", result.ErrorMessage, result.ErrorCode);
                    return new Match();
                }

                return result.Match;
            }
            catch (Exception exception)
            {
                Logger.ErrorFormat("An error has occured: {0}. See error log for more information", exception.Message);

                ErrorLogger.Error(exception);

                Logger.Info("We are closing the connection because of a connection error");

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }


                return new Match();
            }
        }

        public Result1 GetMatchResultByMatchId(int matchId)
        {
            var client = new ResultTeamServiceClient();
            try
            {
                if (client.ClientCredentials == null)
                {
                    throw new SportsDataException("Could not connect to the NIF API");
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                var request = new MatchIdRequest(matchId);
                var result = client.GetMatchResult(request);

                client.Close();
                if (!result.Success)
                {
                    Logger.DebugFormat("Message from NIF API: {0}, error code: {1}", result.ErrorMessage, result.ErrorCode);
                    return new Result1();
                }

                return result.Result;
            }
            catch (Exception exception)
            {
                Logger.ErrorFormat("An error has occured: {0}. See error log for more information", exception.Message);
                ErrorLogger.Error(exception.Message);

                ErrorLogger.Error(exception.StackTrace);

                Logger.Info("We are closing the connection because of a connection error");

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }

                return new Result1();
            }
        }


        public MatchResultPartialResultsResponse GetMatchInfoByResultId(int resultId)
        {
            var client = new ResultTeamServiceClient();
            try
            {
                if (client.ClientCredentials == null)
                {
                    throw new SportsDataException("Could not connect to the NIF API");
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                var request = new ResultIdRequest2()
                {
                    ResultId = resultId
                };

                var result = client.MatchGetByResultId(request);

                
                client.Close();
                if (!result.Success)
                {
                    Logger.DebugFormat("Message from NIF API: {0}, error code: {1}", result.ErrorMessage, result.ErrorCode);
                    return new MatchResultPartialResultsResponse();
                }

                return result;
            }
            catch (Exception exception)
            {
                Logger.ErrorFormat("An error has occured: {0}. See error log for more information", exception.Message);

                ErrorLogger.Error(exception);

                Logger.Info("We are closing the connection because of a connection error");

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }


                return new MatchResultPartialResultsResponse();
            }
        }
    }
}