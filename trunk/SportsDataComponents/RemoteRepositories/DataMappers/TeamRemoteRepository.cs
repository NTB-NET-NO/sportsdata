﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using log4net;
using NTB.SportsData.Components.NIFConnectService;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;

namespace NTB.SportsData.Components.RemoteRepositories.DataMappers
{
    /// <summary>
    /// The team remote repository.
    /// </summary>
    public class TeamRemoteRepository : ITeamRemoteDataMapper
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(TeamRemoteRepository));

        /// <summary>
        /// The get team players by match id.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<SquadIndividual> GetTeamPlayersByMatchId(int matchId)
        {
            var client = new TeamServiceClient();
            try
            {
                if (client.ClientCredentials == null)
                {
                    throw new SportsDataException("Event Client not ready. Client Credentials are null");
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                var request = new MatchIdRequest4() {MatchId = matchId};
                var response = client.GetSquadIndividualsForMatch(request);

                client.Close();

                if (!response.Success)
                {
                    Logger.ErrorFormat("An error has occured. ErrorCode: {0}, ErrorMessage: {1}", response.ErrorCode, response.ErrorMessage);
                    return new List<SquadIndividual>();
                }

                return response.SquadIndividuals.ToList();
            }
            catch (Exception exception)
            {
                Logger.Error(exception);

                Logger.Info("We are closing the connection because of a connection error");

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }

                return new List<SquadIndividual>();
            }
        }
    }
}