﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using log4net;
using NTB.SportsData.Components.NIFConnectService;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;

namespace NTB.SportsData.Components.RemoteRepositories.DataMappers
{
    /// <summary>
    ///     The match incident remote repository.
    /// </summary>
    public class MatchIncidentRemoteRepository : IMatchIncidentDataMapper
    {
        /// <summary>
        ///     The error logger.
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(MatchIncidentRemoteRepository));

        /// <summary>
        ///     The match service client.
        /// </summary>
        // private ResultTeamServiceClient resultTeamClient = new ResultTeamServiceClient();

        /// <summary>
        ///     The get match incidents.
        /// </summary>
        /// <param name="matchId">
        ///     The match id.
        /// </param>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<MatchIncident> GetMatchIncidentsWithSummariesSetToFalse(int matchId)
        {
            var client = new MatchServiceClient();
            try
            {
                if (client.ClientCredentials == null)
                {
                    return new List<MatchIncident>();
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                var request = new MatchIncidentSummariesRequest { MatchId = matchId, Summaries = false };

                var response = client.GetMatchIncidents(request);

                client.Close();
                
                if (!response.Success)
                {
                    Logger.Error("Error while trying to get Match Incidents for match (" + matchId + ") " + response.ErrorMessage);
                }

                return response.MatchIncidents.ToList();

            }
            catch (Exception exception)
            {
                Logger.Info("An error occured while trying to get matches from remote server. Check error log for details");
                ErrorLogger.Error(exception.Message);
                ErrorLogger.Error(exception.StackTrace);

                if (exception.InnerException != null)
                {
                    ErrorLogger.Error("The Inner exception information");
                    ErrorLogger.Error(exception.InnerException.Message);
                    ErrorLogger.Error(exception.InnerException.StackTrace);
                }

                Logger.Info("Closing Client");

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }

                return null;
            }
        }

        /// <summary>
        ///     The get match incidents.
        /// </summary>
        /// <param name="matchId">
        ///     The match id.
        /// </param>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<MatchIncident> GetMatchIncidentsWithSummariesSetToTrue(int matchId)
        {
            var client = new MatchServiceClient();

            try
            {
                if (client.ClientCredentials == null)
                {
                    return new List<MatchIncident>();
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];
                var request = new MatchIncidentSummariesRequest {MatchId = matchId, Summaries = true};

                var response = client.GetMatchIncidents(request);

                if (!response.Success)
                {
                    Logger.Error("Error while trying to get Match Incidents for match (" + matchId + ") " + response.ErrorMessage);
                }

                var matchEvents = response.MatchIncidents.ToList();

                return matchEvents;
            }
            catch (Exception exception)
            {
                Logger.Info("An error occured while trying to get matches from remote server. Check error log for details");
                ErrorLogger.Error(exception.Message);
                ErrorLogger.Error(exception.StackTrace);

                if (exception.InnerException != null)
                {
                    ErrorLogger.Error("The Inner exception information");
                    ErrorLogger.Error(exception.InnerException.Message);
                    ErrorLogger.Error(exception.InnerException.StackTrace);
                }

                Logger.Info("Closing Client");

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }
                    
                return null;
            }
        }
    }
}