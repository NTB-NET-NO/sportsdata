﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SynchRemoteRepository.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System.ServiceModel;

namespace NTB.SportsData.Components.RemoteRepositories.DataMappers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using log4net;
    
    using NIFConnectService;
    using Interfaces;

    /// <summary>
    ///     The synch remote repository.
    /// </summary>
    public class SynchRemoteRepository : ISynchRemoteRepository
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SynchRemoteRepository));

        /// <summary>
        ///     Gets or sets the federation user key.
        /// </summary>
        public string FederationUserKey { get; set; }

        /// <summary>
        ///     Gets or sets the client password.
        /// </summary>
        public string ClientPassword { get; set; }

        /// <summary>
        ///     Gets or sets the event start date.
        /// </summary>
        public DateTime EventStartDate { get; set; }

        /// <summary>
        ///     Gets or sets the event end date.
        /// </summary>
        public DateTime EventEndDate { get; set; }

        /// <summary>
        ///     The get sport events updates.
        /// </summary>
        /// <returns>
        ///     The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<ChangeInfo> GetSportEventsUpdates()
        {
            var client = new SynchronizationServiceClient(); 

            try
            {
                if (client.ClientCredentials == null)
                {
                    throw new SportsDataException("Could not connect to the NIF API");
                }

                client.ClientCredentials.UserName.UserName = this.FederationUserKey;
                client.ClientCredentials.UserName.Password = this.ClientPassword;

                /*
                 * The idea is to first check if we have some results the last hour. If we do, we shall download and distribute since the start of the day
                 * If we don't... well then we don't
                 * 
                 */

                // We are only checking today
                var request = new SynchronizationSimpleRequest
                                  {
                                      FromDate = this.EventStartDate, 
                                      ToDate = this.EventEndDate
                                  };
                var response = client.GetChangesResult(request);

                if (response.Success == false)
                {
                    return new List<ChangeInfo>();
                }

                Logger.DebugFormat("Connected with username {0}", this.FederationUserKey);
                Logger.DebugFormat("Collected from {0} to {1}", this.EventStartDate, this.EventEndDate);

                client.Close();

                return response.Synchronization.Changes.ToList();
            }
            catch (Exception exception)
            {
                Logger.Error(exception);

                Logger.Info("We are closing the connection because of a connection error");

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }
            }

            return new List<ChangeInfo>();
        }

        /// <summary>
        ///     The get sport events by result update.
        /// </summary>
        /// <returns>
        ///     The <see cref="ChangeInfo" />.
        /// </returns>
        public List<ChangeInfo> GetSportsResultsForOrganisation()
        {
            var client = new SynchronizationServiceClient();
            try
            {
                
                if (client.ClientCredentials == null)
                {
                    throw new SportsDataException("Could not connect to the NIF API!");
                }

                client.ClientCredentials.UserName.UserName = this.FederationUserKey;
                client.ClientCredentials.UserName.Password = this.ClientPassword;

                // We are only checking the last hour
                var request = new SynchronizationSimpleRequest
                                  {
                                      FromDate = this.EventStartDate, 
                                      ToDate = this.EventEndDate
                                  };

                Logger.DebugFormat("Connected with username {0}", this.FederationUserKey);
                Logger.DebugFormat("Collected from {0} to {1}", this.EventStartDate, this.EventEndDate);

                var response = client.GetChangesResult(request);

                client.Close();
                if (!response.Success)
                {
                    return new List<ChangeInfo>();
                }

                var changeInfo = response.Synchronization.Changes;

                
                return !changeInfo.Any() ? new List<ChangeInfo>() : changeInfo.ToList();
            }
            catch (Exception exception)
            {
                Logger.Error(exception);

                Logger.Info("We are closing the connection because of a connection error");

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }

                return new List<ChangeInfo>();
                
            }
        }

        /// <summary>
        /// The get changes result team.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<ChangeInfo> GetChangesResultTeam()
        {
            var client = new SynchronizationServiceClient();
            try
            {
                
                if (client.ClientCredentials == null)
                {
                    throw new SportsDataException("Could not connect to the NIF API!");
                }
                client.ClientCredentials.UserName.UserName = this.FederationUserKey;
                client.ClientCredentials.UserName.Password = this.ClientPassword;

                // We are only checking the last hour
                var request = new SynchronizationSimpleRequest
                {
                    FromDate = this.EventStartDate,
                    ToDate = this.EventEndDate
                };

                var response = client.GetChangesResultTeam(request);

                Logger.DebugFormat("Connected with username {0}", this.FederationUserKey);
                Logger.DebugFormat("Collected from {0} to {1}", this.EventStartDate, this.EventEndDate);

                client.Close();

                if (!response.Success)
                {
                    return new List<ChangeInfo>();
                }

                var changeInfo = response.Synchronization.Changes;

                return !changeInfo.Any() ? new List<ChangeInfo>() : changeInfo.ToList();
            }
            catch (Exception exception)
            {
                
                Logger.Error(exception);

                Logger.Info("We are closing the connection because of a connection error");

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }

                return new List<ChangeInfo>();
            }
        }        
    }
}