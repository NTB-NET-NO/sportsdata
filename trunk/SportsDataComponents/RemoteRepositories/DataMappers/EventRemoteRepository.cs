﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using log4net;
using NTB.SportsData.Components.NIFConnectService;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;

namespace NTB.SportsData.Components.RemoteRepositories.DataMappers
{
    /// <summary>
    /// The event remote repository.
    /// </summary>
    public class EventRemoteRepository : IEventRemoteDataMapper
    {

        /// <summary>
        ///     Creating the static Error Logger
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        /// <summary>
        ///     Creating the static Logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(EventResultRemoteRepository));
        
        #region Public Methods and Operators

        /// <summary>
        /// The get events by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public List<Event> GetEventsBySportId(int sportId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get org events.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <param name="endDate">
        /// The end date.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        /// <exception cref="Exception">
        /// Throws exception containing error message from the API
        /// </exception>
        public List<EventSearchAdvancedResult> GetOrgEvents(int orgId, DateTime startDate, DateTime endDate)
        {
            var client = new EventServiceClient();
            try
            {
                if (client.ClientCredentials == null)
                {
                    throw new SportsDataException("Event Client not ready. Client Credentials are null");
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                var criteria = new EventSearchAdvancedSearchCriteria
                {
                    AdmOrgId = orgId,
                    StartDate = startDate,
                    EndDate = endDate
                };

                var request = new EventSearchAdvancedRequest(criteria);

                var response = client.SearchEventsAdvanced(request);

                client.Close();

                if (!response.Success)
                {
                    throw new SportsDataException(response.ErrorMessage);
                }

                return response.Events.ToList();
            }
            catch (Exception exception)
            {
                ErrorLogger.Error(exception);

                Logger.Info("We are closing the connection because of a connection error");

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }

                return null;
            }
        }
        
        /// <summary>
        /// The get event by id.
        /// </summary>
        /// <param name="eventId">
        /// The event id.
        /// </param>
        /// <returns>
        /// The <see cref="Event"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Throws exception containing error message from API on error
        /// </exception>
        public Event GetEventById(int eventId)
        {
            var client = new EventServiceClient();

            try
            {
                if (client.ClientCredentials == null)
                {
                    throw new SportsDataException("Event Client not ready. Client Credentials are null");
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                var request = new EventRequest(eventId, null);
                var response = client.GetSingleEvent(request);

                client.Close();

                if (!response.Success)
                {
                    throw new SportsDataException(response.ErrorMessage);
                }

                return response.Event;
            }
            catch (Exception exception)
            {
                ErrorLogger.Error(exception);

                Logger.Info("We are closing the connection because of a connection error");

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }

                return null;
            }
        }

        #endregion
    }
}