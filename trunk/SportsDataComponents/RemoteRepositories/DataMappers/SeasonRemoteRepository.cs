﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using log4net;
using NTB.SportsData.Components.NIFConnectService;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;

namespace NTB.SportsData.Components.RemoteRepositories.DataMappers
{
    /// <summary>
    /// The season remote repository.
    /// </summary>
    public class SeasonRemoteRepository : ISeasonRemoteDataMapper
    {
        /// <summary>
        /// The error logger.
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SeasonRemoteRepository));

        /// <summary>
        /// The get seasons by org id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Season> GetSeasonsByOrgId(int orgId)
        {
            var client = new SeasonServiceClient();
            try
            {
                if (client.ClientCredentials == null)
                {
                    return null;
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                var request = new SeasonRequest(orgId, DateTime.Today);
                var response = client.GetFederationSeasons(request);

                client.Close();
                return !response.Success ? null : response.Season.ToList();
            }
            catch (Exception exception)
            {
                Logger.Info(
                    "An error occured while trying to get Seasons from remote server. Check error log for details");
                ErrorLogger.Error(exception.Message);
                ErrorLogger.Error(exception.StackTrace);

                if (exception.InnerException != null)
                {
                    ErrorLogger.Error("The Inner exception information");
                    ErrorLogger.Error(exception.InnerException.Message);
                    ErrorLogger.Error(exception.InnerException.StackTrace);
                }

                client.Abort();

                return null;
            }
        }
    }
}