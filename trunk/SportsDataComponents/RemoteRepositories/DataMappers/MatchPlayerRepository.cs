﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nif.Mappers;
using NTB.SportsData.Components.NIFConnectService;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;

namespace NTB.SportsData.Components.RemoteRepositories.DataMappers
{
    /// <summary>
    /// The match player repository.
    /// </summary>
    public class MatchPlayerRepository : IMatchTeamPlayerDataMapper
    {
        /// <summary>
        ///     Creating the static Error Logger
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        /// <summary>
        ///     Creating the static Logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(MatchRemoteRepository));

        /// <summary>
        /// The get match line ups.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        /// <exception cref="Exception">
        /// Throw exception if error from API occures
        /// </exception>
        public List<Player> GetMatchLineUps(int matchId, int orgId)
        {
            var client = new MatchServiceClient();
            
            try
            {
                if (client.ClientCredentials == null)
                {
                    throw new SportsDataException("Could not connect to NIF API");
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                var request = new MatchIdAndOrgIdRequest {MatchId = matchId, OrgId = orgId};

                var response = client.GetMatchLineUps(request);

                client.Close();
                if (!response.Success)
                {
                    throw new SportsDataException(string.Format("Error while getting Match Line Ups. Error Message is: {0}", response.ErrorMessage));
                }

                var mapper = new MatchLineUpMapper();
                mapper.YesNoConverter();

                return response.MatchLineUps.Select(row => mapper.Map(row, new Player()))
                    .ToList();
            }
            catch (Exception exception)
            {
                ErrorLogger.Error(exception);

                Logger.Info("We are closing the connection because of a connection error");

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }

                return null;
            }
        }

        /// <summary>
        /// The get match team players.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        /// <exception cref="Exception">
        /// Throws exception on error
        /// </exception>
        public List<Player> GetMatchTeamPlayers(int matchId)
        {
            var client = new TeamServiceClient();
            try
            {
                if (client.ClientCredentials == null)
                {
                    throw new SportsDataException("Could not connect to NIF API");
                }

                client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];

                var request = new MatchIdRequest4 {MatchId = matchId};

                var response = client.GetSquadIndividualsForMatch(request);

                client.Close();

                if (!response.Success)
                {
                    throw new SportsDataException(string.Format("Could not get data from NIF. Message was: {0} (Code: {1} )", response.ErrorMessage, response.ErrorCode));
                }

                var result = response.SquadIndividuals.ToList();

                var mapper = new SquadIndividualMapper();

                if (!result.Any())
                {
                    return null;
                }

                var players = result.Select(row => mapper.Map(row, new Player()))
                    .ToList();

                return players;
            }
            catch (Exception exception)
            {
                ErrorLogger.Error(exception);

                Logger.Info("We are closing the connection because of a connection error");

                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }

                return null;
            }
        }
    }
}