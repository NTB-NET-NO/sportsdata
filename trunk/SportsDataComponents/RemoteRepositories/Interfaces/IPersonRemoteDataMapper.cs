﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Interface1.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The PersonRemoteDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Components.RemoteRepositories.Interfaces
{
    using NTB.SportsData.Components.NIFConnectService;

    /// <summary>
    ///     The PersonRemoteDataMapper interface.
    /// </summary>
    public interface IPersonRemoteDataMapper
    {
        /// <summary>
        /// The get person by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// Returns
        ///     a <see cref="Person"/> object.
        /// </returns>
        Person GetPersonById(int id);
    }
}