// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMatchEventDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The MatchEventDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.RemoteRepositories.Interfaces
{
    using System.Collections.Generic;

    using NTB.SportsData.Classes.Classes;

    /// <summary>
    /// The MatchEventDataMapper interface.
    /// </summary>
    internal interface IMatchEventDataMapper
    {
        /// <summary>
        /// The get match events.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<MatchEvent> GetMatchEvents(int matchId);
    }
}