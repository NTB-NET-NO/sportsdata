﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRefereeRemoteDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The RefereeRemoteDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Components.RemoteRepositories.Interfaces
{
    using System.Collections.Generic;

    using NTB.SportsData.Classes.Classes;

    /// <summary>
    ///     The RefereeRemoteDataMapper interface.
    /// </summary>
    internal interface IRefereeRemoteDataMapper
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get referee by match id.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Referee> GetRefereeByMatchId(int matchId);

        #endregion
    }
}