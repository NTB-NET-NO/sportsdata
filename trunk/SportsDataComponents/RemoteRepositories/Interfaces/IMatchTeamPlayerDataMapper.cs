﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMatchTeamPlayerDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The MatchTeamPlayerDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.RemoteRepositories.Interfaces
{
    using System.Collections.Generic;

    using NTB.SportsData.Classes.Classes;

    /// <summary>
    /// The MatchTeamPlayerDataMapper interface.
    /// </summary>
    internal interface IMatchTeamPlayerDataMapper
    {
        /// <summary>
        /// The get match team players.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Player> GetMatchTeamPlayers(int matchId);

        /// <summary>
        /// The get match line ups.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Player> GetMatchLineUps(int matchId, int orgId);
    }
}