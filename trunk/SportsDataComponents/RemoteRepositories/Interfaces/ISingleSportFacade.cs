﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISingleSportFacade.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The SingleSportFacade interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Components.RemoteRepositories.Interfaces
{
    using System;
    using System.Collections.Generic;

    using NTB.SportsData.Classes.Classes;

    /// <summary>
    ///     The SingleSportFacade interface.
    /// </summary>
    public interface ISingleSportFacade
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get event info.
        /// </summary>
        /// <param name="eventId">
        /// The event Id.
        /// </param>
        /// <returns>
        /// The <see cref="SportEvent"/>.
        /// </returns>
        SportEvent GetEventInfo(int eventId);

        /// <summary>
        /// The get event results.
        /// </summary>
        /// <param name="eventId">
        /// The event id.
        /// </param>
        /// <param name="federationId">
        /// The federation id.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>Dictionary</cref>
        ///     </see>
        ///     .
        /// </returns>
        Dictionary<EventClass, List<Athlete>> GetEventResults(int eventId, int federationId);

        /// <summary>
        /// The get org events.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <param name="endDate">
        /// The end date.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<SportEvent> GetOrgEvents(int orgId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// The get sync sport events.
        /// </summary>
        /// <param name="userKey">
        /// The user key.
        /// </param>
        /// <param name="clientPassword">
        /// The client password.
        /// </param>
        /// <param name="eventStartDate">
        /// The event start date.
        /// </param>
        /// <param name="eventEndDate">
        /// The event end date.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<SportEvent> GetSyncSportEvents(string userKey, string clientPassword, DateTime eventStartDate, DateTime eventEndDate);

        /// <summary>
        /// The get sync sport event.
        /// </summary>
        /// <param name="userKey">
        /// The user key.
        /// </param>
        /// <param name="clientPassword">
        /// The client password.
        /// </param>
        /// <param name="eventStartDate">
        /// The event start date.
        /// </param>
        /// <param name="eventEndDate">
        /// The event end date.
        /// </param>
        /// <returns>
        /// The <see cref="SportEvent"/>.
        /// </returns>
        SportEvent GetSyncSportEvent(string userKey, string clientPassword, DateTime eventStartDate, DateTime eventEndDate);

        /// <summary>
        /// The get sync sport result.
        /// </summary>
        /// <param name="userKey">
        /// The user key.
        /// </param>
        /// <param name="clientPassword">
        /// The client password.
        /// </param>
        /// <param name="eventStartDate">
        /// The event start date.
        /// </param>
        /// <param name="eventEndDate">
        /// The event end date.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<SportResult> GetSyncSportResult(string userKey, string clientPassword, DateTime eventStartDate, DateTime eventEndDate);

        /// <summary>
        /// The check sync sport result.
        /// </summary>
        /// <param name="userKey">
        /// The user key.
        /// </param>
        /// <param name="clientPassword">
        /// The client password.
        /// </param>
        /// <param name="eventStartDate">
        /// The event start date.
        /// </param>
        /// <param name="eventEndDate">
        /// The event end date.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool CheckSyncSportResult(string userKey, string clientPassword, DateTime eventStartDate, DateTime eventEndDate);

        #endregion
    }
}