﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITournamentDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The TournamentDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Components.RemoteRepositories.Interfaces
{
    using System.Collections.Generic;

    using NTB.SportsData.Classes.Classes;

    /// <summary>
    ///     The TournamentDataMapper interface.
    /// </summary>
    internal interface ITournamentDataMapper
    {
        /// <summary>
        /// The get tournament by match id.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <returns>
        /// The <see cref="Tournament"/>.
        /// </returns>
        Tournament GetTournamentByMatchId(int matchId);

        /// <summary>
        /// The get tournaments by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Tournament> GetTournamentsBySportId(int sportId);

        /// <summary>
        /// The get tournaments by season id.
        /// </summary>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Tournament> GetTournamentsBySeasonId(int seasonId);

        /// <summary>
        /// The get customer tournaments.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Tournament> GetCustomerTournaments(int sportId, int seasonId);

        /// <summary>
        /// Updates the tournament type
        /// </summary>
        /// <param name="tournament">
        /// the tournament type object
        /// </param>
        void UpdateTournamentType(Tournament tournament);

        void UpdateTournamentDisciplineId(Tournament tournament);
    }
}