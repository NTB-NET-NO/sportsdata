﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IClassExerciseRemoteDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The ClassExerciseRemoteDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.RemoteRepositories.Interfaces
{
    using System.Collections.Generic;

    using NTB.SportsData.Components.NIFConnectService;

    /// <summary>
    /// The ClassExerciseRemoteDataMapper interface.
    /// </summary>
    public interface IClassExerciseRemoteDataMapper
    {
        /// <summary>
        /// The get event class exercises.
        /// </summary>
        /// <param name="eventId">
        /// The event id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<ClassExercise> GetEventClassExercises(int eventId);
    }
}