﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISportDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The SportDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.RemoteRepositories.Interfaces
{
    using NTB.SportsData.Classes.Classes;

    /// <summary>
    /// The SportDataMapper interface.
    /// </summary>
    internal interface ISportDataMapper
    {
        /// <summary>
        /// The get sport by org id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see cref="Sport"/>.
        /// </returns>
        Sport GetSportByOrgId(int orgId);
    }
}