﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDataFileCreator.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The DataFileCreator interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.RemoteRepositories.Interfaces
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The DataFileCreator interface.
    /// </summary>
    /// <typeparam name="T">
    /// </typeparam>
    internal interface IDataFileCreator<T>
    {
        /// <summary>
        /// The configure.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool Configure();

        /// <summary>
        /// The find.
        /// </summary>
        /// <param name="remoteEvents">
        /// The remote events.
        /// </param>
        /// <param name="events">
        /// The events.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<T> Find(List<T> remoteEvents, List<T> events);

        /// <summary>
        /// The get remote by org id.
        /// </summary>
        /// <param name="dateStart">
        /// The date start.
        /// </param>
        /// <param name="dateEnd">
        /// The date end.
        /// </param>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<T> GetRemoteByOrgId(DateTime dateStart, DateTime dateEnd, int orgId);

        /// <summary>
        /// The get local by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<T> GetLocalBySportId(int sportId);

        /// <summary>
        /// The get.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<T> Get();

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        void InsertOne(T domainobject);

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        void InsertAll(List<T> domainobject);

        /// <summary>
        /// The create result.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        void CreateResult(List<T> domainobject);
    }
}