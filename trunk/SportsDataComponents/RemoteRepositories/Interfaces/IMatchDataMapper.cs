﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMatchDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The MatchDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Components.RemoteRepositories.Interfaces
{
    using System;
    using System.Collections.Generic;

    using Classes.Classes;

    /// <summary>
    ///     The MatchDataMapper interface.
    /// </summary>
    internal interface IMatchDataMapper
    {
        /// <summary>
        /// The get matches by date interval.
        /// </summary>
        /// <param name="dateStart">
        /// The date start.
        /// </param>
        /// <param name="dateEnd">
        /// The date end.
        /// </param>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<Match> GetMatchesByDateInterval(DateTime dateStart, DateTime dateEnd, int tournamentId);

        /// <summary>
        /// The get scheduled match by match id and sport id.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see cref="Match"/>.
        /// </returns>
        Match GetScheduledMatchByMatchIdAndSportId(int matchId, int sportId);

        /// <summary>
        /// The set match downloaded.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        void SetMatchDownloaded(int matchId);

        /// <summary>
        /// The set schedule match downloaded.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        void SetScheduleMatchDownloaded(int matchId);

        /// <summary>
        /// Deletes a match older than the specified parameter
        /// </summary>
        /// <param name="days">
        /// Defines how many days to save (last 7, 30 etc)
        /// </param>
        void DeleteMatchesByDayInterval(int days);

        /// <summary>
        /// Deletes a match older than the specified parameter
        /// </summary>
        /// <param name="days">
        /// Defines how many days to save (last 7, 30 etc)
        /// </param>
        /// <param name="sportId">
        /// Defines which sport we are to delete from
        /// </param>
        void DeleteMatchesByDayIntervalAndSportId(int days, int sportId);
    }
}