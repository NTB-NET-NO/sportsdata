﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISeasonRemoteDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The SeasonRemoteDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.RemoteRepositories.Interfaces
{
    using System.Collections.Generic;

    using NTB.SportsData.Components.NIFConnectService;

    /// <summary>
    /// The SeasonRemoteDataMapper interface.
    /// </summary>
    internal interface ISeasonRemoteDataMapper
    {
        /// <summary>
        /// The get seasons by org id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Season> GetSeasonsByOrgId(int orgId);
    }
}