﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISportEventDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The SportEventDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.RemoteRepositories.Interfaces
{
    /// <summary>
    /// The SportEventDataMapper interface.
    /// </summary>
    internal interface ISportEventDataMapper
    {
        /// <summary>
        /// The update event set download.
        /// </summary>
        /// <param name="eventId">
        /// The event id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool UpdateEventSetDownload(int eventId);
    }
}