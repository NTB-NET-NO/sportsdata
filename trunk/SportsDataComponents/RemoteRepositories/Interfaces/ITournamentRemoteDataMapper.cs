﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITournamentRemoteDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The TournamentRemoteDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Components.RemoteRepositories.Interfaces
{
    using System.Collections.Generic;

    using NTB.SportsData.Components.NIFConnectService;

    /// <summary>
    ///     The TournamentRemoteDataMapper interface.
    /// </summary>
    public interface ITournamentRemoteDataMapper
    {
        /// <summary>
        /// The get season tournaments.
        /// </summary>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Tournament> GetSeasonTournaments(int seasonId);

        /// <summary>
        /// The get tournament table.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<TeamResult> GetTournamentTable(int tournamentId);

        /// <summary>
        /// The get tournament byid.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see cref="Tournament"/>.
        /// </returns>
        Tournament1 GetTournamentById(int tournamentId);

        Tournament1 GetTournamentByMatchId(int matchId);

        List<TournamentClasses> GetTournamentClassesByTournamentid(int tournamentId);

        List<TeamResult1> GetPublicTournamentTable(int tournamentId);

    }
}