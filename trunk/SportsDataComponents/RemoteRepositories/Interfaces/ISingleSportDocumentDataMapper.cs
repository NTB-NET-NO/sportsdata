﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISingleSportDocumentDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The SingleSportDocumentDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.RemoteRepositories.Interfaces
{
    /// <summary>
    /// The SingleSportDocumentDataMapper interface.
    /// </summary>
    internal interface ISingleSportDocumentDataMapper
    {
        /// <summary>
        /// The get document version by event id.
        /// </summary>
        /// <param name="eventId">
        /// The event id.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        int GetDocumentVersionByEventId(int eventId);
    }
}