﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IVenueRemoteDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The VenueRemoteDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.RemoteRepositories.Interfaces
{
    using NTB.SportsData.Classes.Classes;

    /// <summary>
    /// The VenueRemoteDataMapper interface.
    /// </summary>
    public interface IVenueRemoteDataMapper
    {
        /// <summary>
        /// The get venue activity area by id.
        /// </summary>
        /// <param name="activityAreaId">
        /// The activity area id.
        /// </param>
        /// <returns>
        /// The <see cref="ActivityArea"/>.
        /// </returns>
        ActivityArea GetVenueActivityAreaById(int activityAreaId);

        /// <summary>
        /// The get venue unit by id.
        /// </summary>
        /// <param name="venueUnitId">
        /// The venue unit id.
        /// </param>
        /// <returns>
        /// The <see cref="VenueUnit"/>.
        /// </returns>
        VenueUnit GetVenueUnitById(int venueUnitId);

        /// <summary>
        /// The get venue by id.
        /// </summary>
        /// <param name="venueId">
        /// The venue id.
        /// </param>
        /// <returns>
        /// The <see cref="Venue"/>.
        /// </returns>
        Venue GetVenueById(int venueId);
    }
}