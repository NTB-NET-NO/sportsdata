﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IEventRemoteDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The EventRemoteDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Components.RemoteRepositories.Interfaces
{
    using System;
    using System.Collections.Generic;

    using NTB.SportsData.Components.NIFConnectService;

    /// <summary>
    ///     The EventRemoteDataMapper interface.
    /// </summary>
    internal interface IEventRemoteDataMapper
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get event by id.
        /// </summary>
        /// <param name="eventId">
        /// The event id.
        /// </param>
        /// <returns>
        /// The <see cref="Event"/>.
        /// </returns>
        Event GetEventById(int eventId);

        /// <summary>
        /// The get org events.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <param name="endDate">
        /// The end date.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<EventSearchAdvancedResult> GetOrgEvents(int orgId, DateTime startDate, DateTime endDate);

        #endregion
    }
}