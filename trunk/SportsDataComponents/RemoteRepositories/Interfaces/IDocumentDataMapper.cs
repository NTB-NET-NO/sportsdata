﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDocumentDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The DocumentDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.RemoteRepositories.Interfaces
{
    /// <summary>
    /// The DocumentDataMapper interface.
    /// </summary>
    internal interface IDocumentDataMapper
    {
        /// <summary>
        /// The get document version by tournament id.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        int GetDocumentVersionByTournamentId(int tournamentId);
    }
}