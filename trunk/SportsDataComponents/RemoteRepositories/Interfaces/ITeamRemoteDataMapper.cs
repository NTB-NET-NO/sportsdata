﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITeamRemoteDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The TeamRemoteDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.RemoteRepositories.Interfaces
{
    using System.Collections.Generic;

    using NTB.SportsData.Components.NIFConnectService;

    /// <summary>
    /// The TeamRemoteDataMapper interface.
    /// </summary>
    public interface ITeamRemoteDataMapper
    {
        /// <summary>
        /// The get team players by match id.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<SquadIndividual> GetTeamPlayersByMatchId(int matchId);
    }
}