﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IEventResultRemoteDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The EventResultRemoteDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.RemoteRepositories.Interfaces
{
    using System.Collections.Generic;

    using NTB.SportsData.Classes.Classes;

    using Result = NTB.SportsData.Components.NIFConnectService.Result;

    /// <summary>
    /// The EventResultRemoteDataMapper interface.
    /// </summary>
    public interface IEventResultRemoteDataMapper
    {
        /// <summary>
        /// The get event result by event id.
        /// </summary>
        /// <param name="eventId">
        /// The event id.
        /// </param>
        /// <param name="federationId">
        /// The federation id.
        /// </param>
        /// <returns>
        /// The <see cref="Dictionary"/>.
        /// </returns>
        Dictionary<EventClass, List<Athlete>> GetEventResultByEventId(int eventId, int federationId);

        /// <summary>
        /// The get result by result id.
        /// </summary>
        /// <param name="resultId">
        /// The result id.
        /// </param>
        /// <returns>
        /// The <see cref="Result"/>.
        /// </returns>
        Result GetResultByResultId(int resultId);
    }
}