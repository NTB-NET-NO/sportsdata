// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDatabase.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The Database interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.RemoteRepositories.Interfaces
{
    /// <summary>
    /// The Database interface.
    /// </summary>
    public interface IDatabase
    {
        /*
         * Description of what this interface shall do. It shall be an interface that can be used
         * both for the NFF data and for NIF data
         */

        /*
         * So we need: 
         * Set 
         * Purge - if we are to delete all matches from the database (and tournaments)
         *         This is used when we run the weekly job that shall populate the database with matches for the coming 7 days.
         * 
         * Insert - used to insert data into the database
         * 
         * update - used to update the data in the database
         */

        /// <summary>
        ///     Variable to be used to tell if we are to purge data in the database
        /// </summary>
        bool Purge { get; set; }

        /// <summary>
        /// The update data.
        /// </summary>
        void UpdateData();

        /// <summary>
        /// The insert data.
        /// </summary>
        void InsertData();

        /// <summary>
        /// The delete data.
        /// </summary>
        void DeleteData();
    }
}