﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISynchRemoteRepository.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The SynchRemoteRepository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Components.RemoteRepositories.Interfaces
{
    using System;
    using System.Collections.Generic;

    using NTB.SportsData.Components.NIFConnectService;

    /// <summary>
    ///     The SynchRemoteRepository interface.
    /// </summary>
    public interface ISynchRemoteRepository
    {
        /// <summary>
        ///     Gets or sets the federation user key.
        /// </summary>
        string FederationUserKey { get; set; }

        /// <summary>
        ///     Gets or sets the client password.
        /// </summary>
        string ClientPassword { get; set; }

        /// <summary>
        ///     Gets or sets the event start date.
        /// </summary>
        DateTime EventStartDate { get; set; }

        /// <summary>
        ///     Gets or sets the event end date.
        /// </summary>
        DateTime EventEndDate { get; set; }

        /// <summary>
        ///     The get sport events updates.
        /// </summary>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<ChangeInfo> GetSportEventsUpdates();

        /// <summary>
        ///     The get sport events by result update.
        /// </summary>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<ChangeInfo> GetSportsResultsForOrganisation();

        /// <summary>
        ///     The get changes result team.
        /// </summary>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<ChangeInfo> GetChangesResultTeam();
    }
}