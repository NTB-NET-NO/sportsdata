﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAgeCategoryDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The AgeCategoryDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.RemoteRepositories.Interfaces
{
    using NTB.SportsData.Classes.Classes;

    /// <summary>
    /// The AgeCategoryDataMapper interface.
    /// </summary>
    public interface IAgeCategoryDataMapper
    {
        /// <summary>
        /// The get age category by tournament id.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see cref="AgeCategory"/>.
        /// </returns>
        AgeCategory GetAgeCategoryByTournamentId(int tournamentId, int sportId);

        AgeCategory GetAgeCategoryByIdAndSportId(int ageCategoryId, int sportId);

        void InsertAgeCategory(AgeCategory ageCategory, int sportId);

        void UpdateTournamentAgeCategory(AgeCategory ageCategory, int tournamentId);
    }
}