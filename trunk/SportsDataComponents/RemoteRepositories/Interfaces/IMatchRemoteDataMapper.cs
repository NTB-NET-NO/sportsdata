﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMatchRemoteDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The MatchRemoteDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Components.RemoteRepositories.Interfaces
{
    using System;
    using System.Collections.Generic;

    using NIFConnectService;

    /// <summary>
    ///     The MatchRemoteDataMapper interface.
    /// </summary>
    public interface IMatchRemoteDataMapper
    {
        List<MatchSearchResult> GetMatchesByDateInterval(DateTime startDate, DateTime endDate, int tournamentId);

        List<TournamentMatchExtended> GetAll(int tournamentId);

        MatchSummary GetMatchInformation(int matchId);

        List<PartialResult> GetPartialResults(int matchId);

        Result1 GetMatchResultByMatchId(int matchId);

        List<Match1> GetMatchesByTournamentId(int tournamentId);

        List<MatchSummary> GetMatchSummariesByTournamentId(int tournamentId);

        Match GetMatchInfoByMatchId(int matchId);

        List<MatchSearchResult1> SearchMatchesByDateAndTournamentId(int tournamentId, DateTime startDate, DateTime endDate);

        MatchResultPartialResultsResponse GetMatchInfoByResultId(int resultId);
    }
}