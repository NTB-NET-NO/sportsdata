﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISeasonDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The SeasonDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.RemoteRepositories.Interfaces
{
    using System;
    using System.Collections.Generic;

    using Classes.Classes;

    /// <summary>
    /// The SeasonDataMapper interface.
    /// </summary>
    internal interface ISeasonDataMapper
    {
        /// <summary>
        /// The get season by dates.
        /// </summary>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <param name="endDate">
        /// The end date.
        /// </param>
        /// <returns>
        /// The <see cref="Season"/>.
        /// </returns>
        Season GetSeasonByDates(DateTime startDate, DateTime endDate);

        /// <summary>
        /// The get seasons by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Season> GetSeasonsBySportId(int sportId);

        /// <summary>
        /// The get season by sport id and discipline id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <param name="disciplineId">
        /// The discipline id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Season> GetSeasonBySportIdAndDisciplineId(int sportId, int disciplineId);

        List<Season> GetAllSeasons();

        Season GetSeasonById(int seasonId);

        int InsertOne(Season domainobject);

        void InsertAll(List<Season> domainobject);

        void InsertSeason(Season season);

        void InsertAllSeasons(List<Season> seasons);
    }
}