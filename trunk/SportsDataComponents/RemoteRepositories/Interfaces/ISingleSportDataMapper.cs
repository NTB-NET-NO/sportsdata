﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISingleSportDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The SingleSportDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.RemoteRepositories.Interfaces
{
    /// <summary>
    /// The SingleSportDataMapper interface.
    /// </summary>
    internal interface ISingleSportDataMapper
    {
    }
}