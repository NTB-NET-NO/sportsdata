﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMatchIncidentDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The MatchIncidentDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using NTB.SportsData.Components.NIFConnectService;

namespace NTB.SportsData.Components.RemoteRepositories.Interfaces
{
    using System.Collections.Generic;


    /// <summary>
    /// The MatchIncidentDataMapper interface.
    /// </summary>
    internal interface IMatchIncidentDataMapper
    {
        /// <summary>
        /// The get match incidents.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<MatchIncident> GetMatchIncidentsWithSummariesSetToTrue(int matchId);

        List<MatchIncident> GetMatchIncidentsWithSummariesSetToFalse(int matchId);
    }
}