﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITeamResultDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The TeamResultDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Components.RemoteRepositories.Interfaces
{
    using NTB.SportsData.Components.NIFConnectService;

    /// <summary>
    ///     The TeamResultDataMapper interface.
    /// </summary>
    public interface ITeamResultDataMapper
    {
        /// <summary>
        /// The get match result by result id.
        /// </summary>
        /// <param name="resultId">
        /// The result id.
        /// </param>
        /// <returns>
        /// The <see cref="Result1"/>.
        /// </returns>
        Result1 GetMatchResultByResultId(int resultId);
    }
}