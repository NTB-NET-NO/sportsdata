﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICustomerDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The CustomerDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.RemoteRepositories.Interfaces
{
    using System.Collections.Generic;

    using Classes.Classes;

    /// <summary>
    /// The CustomerDataMapper interface.
    /// </summary>
    public interface ICustomerDataMapper
    {
        /// <summary>
        /// The get customer by match and sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<Customer> GetCustomerByMatchAndSportId(int sportId, int matchId);

        /// <summary>
        /// The get customer by tournament and sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<Customer> GetCustomerByTournamentAndSportId(int sportId, int tournamentId);

        /// <summary>
        /// The get customer by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<Customer> GetCustomerBySportId(int sportId);

        /// <summary>
        /// The get customer regions by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <param name="customerId">
        /// The customer id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<CustomerRegion> GetCustomerRegionsBySportId(int sportId, int customerId);

        Customer GetCustomerById(int id);
    }
}