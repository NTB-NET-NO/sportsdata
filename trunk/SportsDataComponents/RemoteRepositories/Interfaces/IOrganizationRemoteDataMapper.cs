﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IOrganizationDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The OrganizationDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Components.RemoteRepositories.Interfaces
{
    using NTB.SportsData.Components.NIFConnectService;

    /// <summary>
    ///     The OrganizationDataMapper interface.
    /// </summary>
    internal interface IOrganizationRemoteDataMapper
    {
        /// <summary>
        /// The get organization by org id.
        /// </summary>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The <see cref="Org"/>.
        /// </returns>
        Org GetOrganizationByOrgId(int orgId);

        /// <summary>
        /// The get organization by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see cref="Org"/>.
        /// </returns>
        Org GetOrganizationBySportId(int sportId);
    }
}