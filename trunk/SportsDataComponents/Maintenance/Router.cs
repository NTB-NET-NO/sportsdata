// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Router.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using log4net;
using log4net.Config;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Classes.Enums;
using NTB.SportsData.Components.Nff.DataFileCreators;
using NTB.SportsData.Components.Nff.Models;
using NTB.SportsData.Components.Nff.Repositories.Interfaces;
using NTB.SportsData.Components.Nif.Creators;
using NTB.SportsData.Components.Nif.DataFileController;
using NTB.SportsData.Components.PublicRepositories;
using MatchRepository = NTB.SportsData.Components.Nff.Repositories.MatchRepository;

namespace NTB.SportsData.Components.Maintenance
{
    // Adding support for log4net
    // Adding namespace for utilities

    /// <summary>
    ///     This class is used to do the maintenance that is needed from time to time
    /// </summary>
    public class Router
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Router" /> class.
        /// </summary>
        public Router()
        {
            if (!LogManager.GetRepository()
                .Configured)
            {
                BasicConfigurator.Configure();
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the output folder.
        /// </summary>
        public List<OutputFolder> OutputFolders { get; set; }

        #endregion

        #region Static Fields

        /// <summary>
        ///     The logger.
        /// </summary>
        public static readonly ILog Logger = LogManager.GetLogger(typeof(Router));

        /// <summary>
        ///     The error logger.
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The check all missing matches.
        /// </summary>
        public void CheckAllMissingMatches()
        {
            IMatchRepository repository = new MatchRepository();

            var missingMatches = repository.GetAllMissingMatches();

            // If we have matches, then we shall do something with them - like get the data for it
            if (missingMatches.Any())
            {
                foreach (var matchId in missingMatches)
                {
                    Logger.Info("Checking if we have data now for Match Id: " + matchId);
                    var dataFileCreator = new MaintenanceDataFileCreator();
                    dataFileCreator.CreateDataFileMaintenance(matchId);
                }
            }
        }

        /// <summary>
        ///     Method that wraps checking database, and then getting the actual information and then creating the Xml-file
        /// </summary>
        public void CheckMissingMatches()
        {
            // var sportsDataDatabase = new NffSportsDataDatabase();
            var datafileCreator = new MaintenanceDataFileCreator();
            IMatchRepository matchRepository = new MatchRepository();

            var missingMatches = matchRepository.GetListOfMatchesWithoutResults();

            // If we have matches, then we shall do something with them - like get the data for it
            if (missingMatches.Any())
            {
                foreach (var matchId in missingMatches)
                {
                    Logger.Info("Checking if we have data now for Match Id: " + matchId);

                    datafileCreator.CreateDataFileMaintenance(matchId);
                }
            }
        }


        public void foooooo(string file)
        {
            Logger.Info(@"===============================================================================");
            Logger.Info(@"                         Start maintaining customer                             ");
            Logger.Info(@"===============================================================================");
            try
            {
                var customerId = 0;
                var remoteCustomerNumber = 0;
                var startDateTime = new DateTime();
                var endDateTime = new DateTime();

                var xmlDocument = new XmlDocument();
                xmlDocument.Load(file);

                var xmlNode = xmlDocument.SelectSingleNode("/SportsData/Customer");
                if (xmlNode != null && xmlNode.Attributes != null)
                {
                    customerId = XmlConvert.ToInt32(xmlNode.Attributes["id"].Value);
                    remoteCustomerNumber = XmlConvert.ToInt32(xmlNode.Attributes["foreigncustomerid"].Value);
                }

                xmlNode = xmlDocument.SelectSingleNode("/SportsData/Customer/Job");
                if (xmlNode != null && xmlNode.Attributes != null)
                {
                    startDateTime = Convert.ToDateTime(xmlNode.Attributes["startdate"].Value);
                    endDateTime = Convert.ToDateTime(xmlNode.Attributes["enddate"].Value);
                }

                var xmlSoccerNodeList =
                    xmlDocument.SelectNodes("/SportsData/Customer/Tournaments/Tournament[@sportId=16]");
                var xmlSportNodeList =
                    xmlDocument.SelectNodes("/SportsData/Customer/Tournaments/Tournament[@sportId!=16]");

                var soccerTask = Task.Factory.StartNew(() => this.ProcessSoccerNodeList(xmlSoccerNodeList, customerId, remoteCustomerNumber, startDateTime, endDateTime));
                var sportTask = Task.Factory.StartNew(() => this.ProcessSportNodeList(xmlSportNodeList, customerId, remoteCustomerNumber, startDateTime, endDateTime));
                Task.WaitAll(soccerTask, sportTask);


                Logger.Info(@"===============================================================================");
                Logger.Info(@"                         Done maintaining customer                             ");
                Logger.Info(@"===============================================================================");
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
        }
        /// <summary>
        ///     The send tournaments to all clients.
        /// </summary>
        /// <param name="file">
        ///     The file.
        /// </param>
        public void SendTournamentsToAllClients(string file)
        {
            Logger.Info(@"===============================================================================");
            Logger.Info(@"                         Start maintaining customer                             ");
            Logger.Info(@"===============================================================================");

            try
            {
                var startDateTime = new DateTime();
                var endDateTime = new DateTime();

                var xmlDocument = new XmlDocument();
                xmlDocument.Load(file);

                var xmlNode = xmlDocument.SelectSingleNode("/SportsData/SportsData-Meta");
                if (xmlNode != null && xmlNode.Attributes != null)
                {
                    startDateTime = Convert.ToDateTime(xmlNode.Attributes["startdate"].Value);
                }

                if (xmlNode != null && xmlNode.Attributes != null)
                {
                    endDateTime = Convert.ToDateTime(xmlNode.Attributes["enddate"].Value);
                }

                var xmlNodeList = xmlDocument.SelectNodes("/SportsData/Tournaments/Tournament");
                if (xmlNodeList == null)
                {
                    return;
                }

                // First we create the SoccerNodeList
                var xmlNodeListSoccer = xmlDocument.SelectNodes("/SportsData/Tournaments/Tournament[@sportId=16]");

                var xmlNodeListSport = xmlDocument.SelectNodes("/SportsData/Tournaments/Tournament[@sportId!=16]");

                if (xmlNodeListSport != null)
                {
                    foreach (XmlNode xmlNodeSport in xmlNodeListSport)
                    {
                        try
                        {
                            if (xmlNodeSport.Attributes == null)
                            {
                                continue;
                            }

                            var tournamentId = Convert.ToInt32(xmlNodeSport.Attributes["id"].Value);
                            var sportId = Convert.ToInt32(xmlNodeSport.Attributes["sportId"].Value);
                            var disciplineId = Convert.ToInt32(xmlNodeSport.Attributes["disciplineId"].Value);
                            if (sportId == 0)
                            {
                                continue;
                            }
                            // We need to get the federation
                            var orgRepository = new FederationRepository();
                            var f = orgRepository.GetFederationBySportId(sportId);
                            var federationId = f.Id;

                            var outputFolders =
                                (from o in this.OutputFolders where o.SportId == sportId select o).ToList();

                            var creator = new MaintenanceResultsCreator()
                            {
                                DataParams =
                                    new DataFileParams
                                    {
                                        CreateXml = true,
                                        DateStart = startDateTime,
                                        DateEnd = endDateTime,
                                        DateOffset = false,
                                        DisciplineId = disciplineId,
                                        FederationId = federationId,
                                        Duration = 0,
                                        OperationMode = OperationMode.Maintenance,
                                        PollStyle = PollStyle.Continous,
                                        PushResults = true,
                                        Results = true,
                                        SportId = sportId,
                                        FileOutputFolders = outputFolders,
                                        Maintenance = true
                                    }
                            };

                            // Creating the xpath expression
                            var xpathExpression = string.Format("/SportsData/Tournaments/Tournament[@id='{0}' and @sportId='{1}']/Customers/Customer", tournamentId, sportId);

                            // Running the XPath Expression to select nodes
                            var xmlNodeListCustomers = xmlDocument.SelectNodes(xpathExpression);

                            // Checking if the list of customers is full or not
                            if (xmlNodeListCustomers == null)
                            {
                                Logger.Info("No customers found for this sport");
                                continue;
                            }

                            if (xmlNodeListCustomers.Count == 0)
                            {
                                continue;
                            }

                            // Creating CustomerList
                            var customerRepository = new CustomerRepository();
                            var customers =
                                (from XmlNode customerNode in xmlNodeListCustomers
                                    where customerNode.Attributes != null
                                    select new Customer
                                    {
                                        Id = Convert.ToInt32(customerNode.Attributes["id"].Value),
                                        RemoteCustomerId =
                                            Convert.ToInt32(customerNode.Attributes["foreigncustomerid"].Value),
                                        Name =
                                            customerRepository.GetCustomerById(
                                                Convert.ToInt32(customerNode.Attributes["id"].Value))
                                                .Name
                                    }).ToList
                                    ();

                            Logger.InfoFormat("Checking for Creating Datafile for tournament: {0}", tournamentId);

                            var tournament = new Tournament {Id = tournamentId, SportId = sportId};

                            creator.Create(new List<Tournament> { tournament });
                        }
                        catch (Exception exception)
                        {
                            Logger.Error("An error has occured. Please check the error log!");
                            ErrorLogger.Error(exception.Message);
                            ErrorLogger.Error(exception.StackTrace);

                            if (exception.InnerException != null)
                            {
                                ErrorLogger.Error(exception.InnerException.Message);
                                ErrorLogger.Error(exception.InnerException.StackTrace);
                            }
                        }
                    }
                }

                if (xmlNodeListSoccer == null)
                {
                    return;
                }

                foreach (XmlNode xmlNodeSoccer in xmlNodeListSoccer)
                {
                    const int sportId = 16;

                    var outputFolder = (from o in this.OutputFolders where o.SportId == sportId select o).Single();

                    var tournamentId = 0;
                    if (xmlNodeSoccer.Attributes != null)
                    {
                        tournamentId = Convert.ToInt32(xmlNodeSoccer.Attributes["id"].Value);
                    }

                    // Below this we have customers subscribing to soccer results
                    var xpathString =
                        string.Format(
                            "/SportsData/Tournaments/Tournament[@id='{0}' and @sportId='{1}']/Customers/Customer",
                            tournamentId, sportId);
                    var xmlNodeListCustomers = xmlDocument.SelectNodes(xpathString);

                    if (xmlNodeListCustomers == null)
                    {
                        continue;
                    }

                    // Creating CustomerList
                    var customerRepository = new CustomerRepository();
                    var customers =
                        (from XmlNode customerNode in xmlNodeListCustomers
                            where customerNode.Attributes != null
                            select new Customer
                            {
                                Id = Convert.ToInt32(customerNode.Attributes["id"].Value),
                                RemoteCustomerId = Convert.ToInt32(customerNode.Attributes["foreigncustomerid"].Value),
                                Name =
                                    customerRepository.GetCustomerById(
                                        Convert.ToInt32(customerNode.Attributes["id"].Value))
                                        .Name
                            }).ToList();

                    Logger.Info("Checking for Creating Datafile for tournament: " + tournamentId);
                    var dataFileCreator = new TournamentDataFileCreator();
                    dataFileCreator.Model = new DataFileModel
                    {SportId = sportId, OutputFolder = outputFolder.OutputPath};
                    dataFileCreator.CreateTournamentDataFile(customers, tournamentId, startDateTime, endDateTime);
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
        }

        public void ProcessSportNodeList(XmlNodeList xmlSportNodeList, int customerId, int remoteCustomerNumber, DateTime startDateTime, DateTime endDateTime)
        {
            // Looping all other sports
            if (xmlSportNodeList == null)
            {
                return;
            }

            // We need to get the list of sports
            var orgRepository = new FederationRepository();
            var organizations = orgRepository.GetAll();

            foreach (XmlNode xmlSportNode in xmlSportNodeList)
            {
                var sportId = 0;
                var disciplineId = 0;
                if (xmlSportNode.Attributes != null)
                {
                    sportId = Convert.ToInt32(xmlSportNode.Attributes.GetNamedItem("sportId")
                        .Value);

                    // we are only using this for bandy / floorball
                    if (sportId != 16)
                    {
                        if (xmlSportNode.Attributes.GetNamedItem("disciplineId") != null)
                        {
                            disciplineId = Convert.ToInt32(xmlSportNode.Attributes.GetNamedItem("disciplineId")
                                .Value);
                        }
                    }
                }

                // Checking if we have a valid sportId
                Logger.Debug("SportId: " + sportId);
                try
                {
                    // This is just a "quick fix"
                    if (sportId == 0)
                    {
                        // @todo: Maybe we should send an e-mail to someone that the information is wrong
                        continue;
                    }

                    if ((from o in organizations where o.SportId == sportId select o.Id) == null)
                    {
                        return;
                    }

                    var federationId = (from o in organizations where o.SportId == sportId select o.Id).Single();

                    var endDateCheck = DateTime.Parse(endDateTime.ToShortDateString());
                    var startDateCheck = DateTime.Parse(startDateTime.ToShortDateString());

                    var creator = new MaintenanceResultsCreator
                    {
                        DataParams =
                            new DataFileParams
                            {
                                CreateXml = true,
                                DateStart = startDateCheck,
                                DateEnd = endDateCheck,
                                DateOffset = false,
                                DisciplineId = disciplineId,
                                Duration = 0,
                                FederationId = federationId,
                                OperationMode = OperationMode.Maintenance,
                                PollStyle = PollStyle.Continous,
                                PushResults = true,
                                Results = true,
                                SportId = sportId,
                                FileOutputFolders = this.OutputFolders
                            }
                    };

                    var tournament = new Tournament
                    {
                        Id = Convert.ToInt32(xmlSportNode.Attributes.GetNamedItem("id").Value),
                        SportId = Convert.ToInt32(xmlSportNode.Attributes.GetNamedItem("sportId")
                            .Value)
                    };

                    var tournaments = creator.Create(new List<Tournament> { tournament });

                    var customers = new List<Customer>();
                    customers.Add(this.GetMaintenanceCustomer(customerId));

                    creator.CreateCustomerDataFile(customers, remoteCustomerNumber, tournaments);
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the customer from the database based on the customer id
        /// </summary>
        /// <param name="customerId">The customer Id</param>
        /// <returns>A customer object</returns>
        public Customer GetMaintenanceCustomer(int customerId)
        {
            var customerRepository = new CustomerRepository();
            var customer = customerRepository.GetCustomerById(customerId);
            return customer;
        }

        public void ProcessSoccerNodeList(XmlNodeList xmlSoccerNodeList, int customerId, int remoteCustomerNumber, DateTime startDateTime, DateTime endDateTime)
        {
            if (xmlSoccerNodeList != null)
            {
                const int sportId = 16;
                var outputFolders = (from o in this.OutputFolders where o.SportId == sportId select o.OutputPath).ToList();

                var dataFileCreator = new CustomerDataFileCreator();
                dataFileCreator.Model = new DataFileModel
                {
                    SportId = sportId,
                    FileOutputFolder = outputFolders,
                    RenderResult = true
                };

                var customers = new List<Customer>()
                {
                    new Customer()
                    {
                        Id = customerId,
                        RemoteCustomerId = remoteCustomerNumber
                    }
                };

                dataFileCreator.CreateCustomerDataFile(customers, xmlSoccerNodeList, startDateTime, endDateTime);
            }
        }


        /// <summary>
        ///     The send tournaments to client.
        /// </summary>
        /// <param name="file">
        ///     The file.
        /// </param>
        public void SendTournamentsToClient(string file)
        {
            Logger.Info(@"===============================================================================");
            Logger.Info(@"                         Start maintaining customer                             ");
            Logger.Info(@"===============================================================================");
            try
            {
                var customerId = 0;
                var remoteCustomerNumber = 0;
                var startDateTime = new DateTime();
                var endDateTime = new DateTime();

                var xmlDocument = new XmlDocument();
                xmlDocument.Load(file);

                var xmlNode = xmlDocument.SelectSingleNode("/SportsData/Customer");
                if (xmlNode != null && xmlNode.Attributes != null)
                {
                    customerId = XmlConvert.ToInt32(xmlNode.Attributes["id"].Value);
                    remoteCustomerNumber = XmlConvert.ToInt32(xmlNode.Attributes["foreigncustomerid"].Value);
                }

                xmlNode = xmlDocument.SelectSingleNode("/SportsData/Customer/Job");
                if (xmlNode != null && xmlNode.Attributes != null)
                {
                    startDateTime = Convert.ToDateTime(xmlNode.Attributes["startdate"].Value);
                    endDateTime = Convert.ToDateTime(xmlNode.Attributes["enddate"].Value);
                }

                var xmlSoccerNodeList =
                    xmlDocument.SelectNodes("/SportsData/Customer/Tournaments/Tournament[@sportId=16]");
                var xmlSportNodeList =
                    xmlDocument.SelectNodes("/SportsData/Customer/Tournaments/Tournament[@sportId!=16]");

                var soccerTask = Task.Factory.StartNew(() => this.ProcessSoccerNodeList(xmlSoccerNodeList, customerId, remoteCustomerNumber, startDateTime, endDateTime));
                var sportTask = Task.Factory.StartNew(() => this.ProcessSportNodeList(xmlSportNodeList, customerId, remoteCustomerNumber, startDateTime, endDateTime));
                Task.WaitAll(soccerTask, sportTask);
                

                Logger.Info(@"===============================================================================");
                Logger.Info(@"                         Done maintaining customer                             ");
                Logger.Info(@"===============================================================================");
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
        }

        /// <summary>
        ///     The tournament router.
        /// </summary>
        /// <param name="file">
        ///     The file.
        /// </param>
        public void TournamentRouter(string file)
        {
            var xmlDocument = new XmlDocument();
            xmlDocument.Load(file);

            var convertionType = string.Empty;

            var xmlNode = xmlDocument.SelectSingleNode("/SportsData/SportsData-Meta");
            if (xmlNode != null && xmlNode.Attributes != null)
            {
                convertionType = xmlNode.Attributes["type"].Value;
            }

            if (convertionType.ToLower() == "customer")
            {
                this.SendTournamentsToClient(file);
            }
            else
            {
                this.SendTournamentsToAllClients(file);
            }
        }

        #endregion
    }
}