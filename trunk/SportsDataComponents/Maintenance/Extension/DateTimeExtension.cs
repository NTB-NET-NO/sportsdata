﻿using System;
using System.Collections.Generic;

namespace NTB.SportsData.Components.Maintenance.Extension
{
    public class DateTimeExtension
    {
        public IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
        {
            for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
                yield return day;
        }
    }
}