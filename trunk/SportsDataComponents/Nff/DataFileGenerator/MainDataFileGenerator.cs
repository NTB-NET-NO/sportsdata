﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nff.Facade;
using NTB.SportsData.Components.Nff.Models;
using NTB.SportsData.Components.Nff.Repositories.Interfaces;
using NTB.SportsData.Components.PublicRepositories;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;

namespace NTB.SportsData.Components.Nff.DataFileGenerator
{
    public class MainDataFileGenerator
    {
        private ISoccerFacade facade;

        public MainDataFileGenerator()
        {
            this.facade = new SoccerFacade();
        }
        /// <summary>
        ///     Static logger
        /// </summary>
        public static readonly ILog Logger = LogManager.GetLogger(typeof(MainDataFileGenerator));

        public XmlDocumentModel Model { get; set; }

        /// <summary>
        ///     This method is used to control the flow of creating the XML-file
        ///     We are first calling the createXMLStructure (which creates the XML-file sort of)
        ///     Then we are actually saving the file to disk in createXMLFile.
        /// </summary>
        /// <param name="tournament">
        ///     Holds the Tournament object with all data and methods
        /// </param>
        /// <param name="matches">
        ///     Holds the Match object
        /// </param>
        /// <param name="renderResults">
        ///     Holds the bool value of the render Result. If false we shall not render results
        /// </param>
        /// <param name="sportId">
        ///     The sport Id.
        /// </param>
        /// <param name="ageCategoryTournament">
        ///     The age Category Tournament.
        /// </param>
        public void CreateDataFile(Tournament tournament, List<Match> matches, bool renderResults, int sportId,
            AgeCategory ageCategoryTournament)
        {
            try
            {
                // Getting customers
                var customerMatches = matches;

                ICustomerDataMapper dataMapper = new CustomerRepository();

                var listOfCustomers = new List<Customer>();
                foreach (var match in customerMatches)
                {
                    if (match.TournamentId == 0)
                    {
                        match.TournamentId = tournament.Id;
                    }

                    // Now I can check against the database, because I now know that the match exists... 
                    if (match.TournamentId != 0)
                    {
                        Logger.InfoFormat("Getting customers by match.TournamentId: {0}", match.TournamentId);
                        Logger.DebugFormat("Sport Id is: {0}", sportId);
                        listOfCustomers = dataMapper.GetCustomerByTournamentAndSportId(sportId, match.TournamentId);
                    }
                    else
                    {
                        listOfCustomers = dataMapper.GetCustomerByMatchAndSportId(match.Id, sportId);
                    }

                }

                if (!listOfCustomers.Any())
                {
                    return;
                }

                this.Model = new XmlDocumentModel
                {
                    Customers = listOfCustomers
                };
                // this.Model.

                Logger.Debug("Numbers of customers in : List of customers: " + listOfCustomers.Count());

                Logger.Debug(@"Tournament: " + tournament.Name + @"(" + tournament.Id + @")");

                List<TournamentTable> standings = null;

                // If we are to update the results, we do so here. 
                if (this.Model.UpdateDatabase)
                {
                    Logger.Debug("Updating/Inserting into database");
                    IMatchRepository matchRepository = new Repositories.MatchRepository();

                    foreach (var match in matches)
                    {
                        var foundMatch = matchRepository.GetMatchByMatchAndSportId(match.Id, this.Model.SportId);

                        if (foundMatch == null)
                        {
                            matchRepository.InsertMatch(match, this.Model.SportId);
                        }
                        else
                        {
                            matchRepository.Updatematch(match, this.Model.SportId);
                        }
                    }
                }

                if (listOfCustomers.Any())
                {
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                    {
                        this.Model.CreateXml = true;
                    }

                    // If we are to create the XML-file, we do so here.
                    if (this.Model.CreateXml)
                    {
                        Logger.Debug("Creating XML Files!");

                        var soccerXmlDocument = new SoccerXmlDocument(this.Model);

                        // Checking if this tournament has a table or not
                        if (tournament.PublishTournamentTable)
                        {
                            standings = this.facade.GetTournamentTableTeam(tournament.Id);
                        }

                        // Creating the XMLDocument
                        var doc = soccerXmlDocument.CreateXmlStructure(matches, tournament, ageCategoryTournament, standings, false, listOfCustomers);

                        // Creating filename
                        var filenames = soccerXmlDocument.CreateFileName(tournament.DistrictId, tournament.Id, null, sportId);

                        // Finally creating the XML-file
                        if (doc != null)
                        {
                            soccerXmlDocument.WriteXmlFile(
                                doc, tournament.Name, tournament.Id, filenames, tournament.DistrictId);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                Logger.Error(exception.StackTrace);
            }}
    }
}
