﻿using System;

using NTB.SportsData.Components.Nff.Repositories;
using NTB.SportsData.Components.Nff.Repositories.Interfaces;

using log4net;
using NTB.SportsData.Components.Nff.Facade;

namespace NTB.SportsData.Components.Nff.Configurators
{
    public class MunicipalitiesConfigurator
    {
        /// <summary>
        ///     Static logger
        /// </summary>
        public static readonly ILog Logger = LogManager.GetLogger(typeof(MunicipalitiesConfigurator));

        /// <summary>
        ///     The configure municipalities.
        /// </summary>
        /// <param name="district">
        ///     The district.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        public bool ConfigureMunicipalities(Classes.Classes.District district)
        {
            Logger.DebugFormat("Checking if {0} (id: {1}) and Municipality has some table rows!", district.Name, district.Id);

            var returnValue = false;
            try
            {
                // If district is bigger than 1 (not Norges Fotballforbund) we can check for Municipalities...
                if (district.Id > 1)
                {
                    var soccerFacade = new SoccerFacade();
                    var municipalities = soccerFacade.GetMunicipalitiesByDistrictId(district.Id);
                    IMunicipalityRepository municipalityRepository = new MunicipalityRepository();
                    foreach (var municipality in municipalities)
                    {
                        var response = municipalityRepository.GetMunicipalityById(municipality.Id);
                        if (response != null)
                        {
                            returnValue = true;
                            continue;
                        }
                        municipalityRepository.InsertMunicipality(municipality);
                    }

                }

                return returnValue;
            }
            catch (Exception exception)
            {
                Logger.Debug(exception);
            }

            return true;
        }
    }
}
