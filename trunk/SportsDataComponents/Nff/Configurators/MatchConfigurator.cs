﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nff.Facade;
using NTB.SportsData.Components.PublicRepositories;

namespace NTB.SportsData.Components.Nff.Configurators
{
    public class MatchConfigurator
    {
        /// <summary>
        ///     Static logger
        /// </summary>
        public static readonly ILog Logger = LogManager.GetLogger(typeof(MatchConfigurator));

        /// <summary>
        ///     The configure matches.
        /// </summary>
        /// <param name="district">
        ///     The district.
        /// </param>
        /// <param name="sportId">
        /// The sport id
        /// </param>
        public void ConfigureMatches(District district, int sportId)
        {
            try
            {
                // Creating the NffSportsDataDatabase object
                ISoccerFacade soccerFacade = new SoccerFacade();

                var matchRepository = new MatchRepository();

                var numberOfMatches = matchRepository.GetNumberofMatches(sportId);

                if (numberOfMatches != 0)
                {
                    return;
                }

                // This is populated so that we can have the push subscription working
                
                // var seasonTournamentMapper = new SeasonTournamentMapper();
                var remoteMatches = soccerFacade.GetMatchesByDistrict(district.Id);
                foreach (var match in remoteMatches)
                {
                    if (match.MatchDate != null)
                    {
                        match.MatchStartTime = match.MatchDate.Value.Hour + match.MatchDate.Value.Minute;
                    }
                }
                var localMatches = matchRepository.GetMatchesBySportId(sportId);
                var matches = this.CheckRemoteMatchesAgainstLocalMatches(remoteMatches, localMatches);
                matchRepository.InsertAll(matches); //.SetMatches = matches;
            }
            catch (Exception e)
            {
                Logger.Debug(e);
            }
        }

        /// <summary>
        /// The check remote matches against local matches.
        /// </summary>
        /// <param name="remoteMatches">
        /// The remote matches.
        /// </param>
        /// <param name="localMatches">
        /// The local matches.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Match> CheckRemoteMatchesAgainstLocalMatches(List<Match> remoteMatches, List<Match> localMatches)
        {
            var filteredMatches =
                remoteMatches.Select(match1 => (from m in localMatches where m.Id == match1.Id select m).Single())
                    .Where(match => match != null)
                    .ToList();

            return filteredMatches;
        }
    }
}
