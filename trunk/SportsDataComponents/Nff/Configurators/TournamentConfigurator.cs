﻿using System;
using System.Collections.Generic;
using System.Configuration;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nff.Facade;
using NTB.SportsData.Components.Nff.Importers;

namespace NTB.SportsData.Components.Nff.Configurators
{
    public class TournamentConfigurator
    {
        /// <summary>
        ///     Static logger
        /// </summary>
        public static readonly ILog Logger = LogManager.GetLogger(typeof(MunicipalitiesConfigurator));

        /// <summary>
        ///     The configure tournaments.
        /// </summary>
        /// <param name="district">
        ///     The district.
        /// </param>
        /// <param name="seasons">
        ///     The list of seasons.
        /// </param>
        public void ConfigureTournaments(District district, List<Season> seasons)
        {
            try
            {
                Logger.Info("Checking tournaments for district " + district.Name);

                var soccerFacade = new SoccerFacade();

                var tournaments = new List<Tournament>();
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                {
                    foreach (var season in seasons)
                    {
                        tournaments.AddRange(soccerFacade.GetTournamentsByDistrictAndSeasonId(district.Id, season.Id));
                    }
                }
                else
                {
                    foreach (var season in seasons)
                    {
                        try
                        {
                            Logger.DebugFormat("Getting tournaments with following parameters: district.DistrictId {0}, season.Id {1}",district.Id, season.Id);

                            tournaments.AddRange(soccerFacade.GetTournamentsByDistrictAndSeasonId(district.Id, season.Id));
                        }
                        catch (Exception e)
                        {
                            Logger.Info(e.Message);
                            Logger.Debug(e.StackTrace);
                        }
                    }
                }

                var importer = new TournamentImporter();
                importer.ImportTournaments(tournaments);
            }
            catch (Exception e)
            {
                Logger.Info(e.Message);
                Logger.Info(e.StackTrace);
            }
        }
    }
}
