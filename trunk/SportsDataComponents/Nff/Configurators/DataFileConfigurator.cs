﻿using System;
using log4net;
using NTB.SportsData.Components.Nff.DataFileCreators.Helpers;
using NTB.SportsData.Components.Nff.Importers;
using NTB.SportsData.Components.Nff.Repositories;

namespace NTB.SportsData.Components.Nff.Configurators
{
    public class DataFileConfigurator
    {
        /// <summary>
        ///     Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(DataFileConfigurator));

        public int SportId { get; set; }
        /// <summary>
        ///     This method is used to configure the datafile object. It shall check if we have rows in Tournament and Districts
        ///     Databases
        /// </summary>
        /// <returns>
        ///     Returns true if we were able to configure the component or dll
        /// </returns>
        public bool Configure()
        {
            try
            {
                Logger.Debug("Configuring DatafileCreator");

                var systemRepository = new SystemRepository();

                // First we check when this tournament was last checked - if we don't have to configure, then we return
                if (systemRepository.CheckLastTableCheck() == false)
                {
                    return true;
                }

                var ageCategoryImporter = new AgeCategoryImporter();
                ageCategoryImporter.SportId = this.SportId;
                ageCategoryImporter.ImportAgeCategories();

                var seasonPopulatorHelper = new SeasonPopulatorHelper();
                if (!seasonPopulatorHelper.PopulateSeasons()) return false;

                var districtPopulatorHelper = new DistrictPopulatorHelper();
                return districtPopulatorHelper.PopulateDistricts();
            }
            catch (Exception e)
            {
                Logger.ErrorFormat("An exception has occured: {0}", e);
                return false;
            }
        }
    }
}
