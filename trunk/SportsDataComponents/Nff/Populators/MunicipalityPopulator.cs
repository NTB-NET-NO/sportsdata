﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.Text;
using NTB.SportsData.Components.NFFMetaService;

namespace NTB.SportsData.Components.Nff.Populators
{
    public class MunicipalityPopulator
    {
        /// <summary>
        ///     The organization service client.
        /// </summary>
        private MetaServiceClient _serviceClient; // = new MetaServiceClient();

        public MunicipalityPopulator()
        {
            this.CheckCheckServiceState();
        }

        public List<Municipality> GetMunicipalitiesByDistrictId(int districtId)
        {
            return this._serviceClient.GetMunicipalitiesbyDistrict(districtId).ToList();
        }

        private void CheckCheckServiceState()
        {
            if (this._serviceClient == null)
            {
                this._serviceClient = new MetaServiceClient();

                if (this._serviceClient.ClientCredentials != null)
                {
                    this._serviceClient.ClientCredentials.UserName.UserName =
                        ConfigurationManager.AppSettings["NFFServiceUsername"];
                    this._serviceClient.ClientCredentials.UserName.Password =
                        ConfigurationManager.AppSettings["NFFServicePassword"];

                    return;
                }
            }

            if (this._serviceClient.InnerChannel.State == CommunicationState.Faulted)
            {
                this._serviceClient.Close();
                this._serviceClient = null;

                this._serviceClient = new MetaServiceClient();

            }

            if (this._serviceClient.ClientCredentials != null)
            {
                this._serviceClient.ClientCredentials.UserName.UserName =
                    ConfigurationManager.AppSettings["NFFServiceUsername"];
                this._serviceClient.ClientCredentials.UserName.Password =
                    ConfigurationManager.AppSettings["NFFServicePassword"];
            }
        }
    }
}
