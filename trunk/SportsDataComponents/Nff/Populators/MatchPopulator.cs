﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using log4net;
using NTB.SportsData.Components.NFFTournamentService;

namespace NTB.SportsData.Components.Nff.Populators
{
    public class MatchPopulator
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(MatchPopulator));

        /// <summary>
        ///     The organization service client.
        /// </summary>
        private TournamentServiceClient _serviceClient;

        public MatchPopulator()
        {
            this.CheckCheckServiceState();
        }

        // List<Match>(this.Tournament);

        public List<Match> GetMatchesByDistrict(int districtId, bool includeSquad = false, bool includeReferees = false, bool includeResults = false, bool includeEvents = false)
        {
            try
            {
                var matchDate = DateTime.Today;

                if (ConfigurationManager.AppSettings["testing"].ToLower() == "true")
                {
                    matchDate = DateTime.Parse(ConfigurationManager.AppSettings["nfftestingdate"]);
                }
                return
                    this._serviceClient.GetMatchesByDistrict(districtId, matchDate, matchDate, includeSquad, includeReferees, includeResults,
                        includeEvents, null)
                        .ToList();
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                return new List<Match>();
            }
        }

        public List<Match> GetMatchesByTournament(int tournamentId, bool includeSquad, bool includeReferees, bool includeResults, bool includeEvents, DateTime? tournamentDate = null)
        {
            try
            {
                return
                    this._serviceClient.GetMatchesByTournament(tournamentId, includeSquad, includeReferees, includeResults, includeEvents, tournamentDate)
                        .ToList();
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                return new List<Match>();
            }
        }

        public List<Match> GetMatchesByDateInterval(DateTime dateStart, DateTime dateEnd, int districtId,
            bool includeSquad = false, bool includeReferees = false, bool includeResults = false, bool includeEvents = false)
        {
            try
            {
                return
                    this._serviceClient.GetMatchesByDateInterval(dateStart, dateEnd, districtId, includeSquad, includeReferees, includeResults, includeEvents)
                        .ToList();
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                return new List<Match>();
            }
        }

        public Match GetMatchById(int matchId, bool includeSquad = false, bool includeReferees = false, bool includeResults = false, bool includeEvents = false)
        {
            try
            {
                return this._serviceClient.GetMatch(matchId, includeSquad, includeReferees, includeResults, includeEvents);
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                return new Match();
            }
        }

        private void CheckCheckServiceState()
        {
            if (this._serviceClient == null)
            {
                this._serviceClient = new TournamentServiceClient();

                if (this._serviceClient.ClientCredentials != null)
                {
                    this._serviceClient.ClientCredentials.UserName.UserName =
                        ConfigurationManager.AppSettings["NFFServiceUsername"];
                    this._serviceClient.ClientCredentials.UserName.Password =
                        ConfigurationManager.AppSettings["NFFServicePassword"];
                }

                return;
            }

            if (this._serviceClient.InnerChannel.State == CommunicationState.Faulted)
            {
                this._serviceClient.Close();
                this._serviceClient = null;

                this._serviceClient = new TournamentServiceClient();

            }
            if (this._serviceClient.State == CommunicationState.Opened)
            {
                this._serviceClient.Close();
                this._serviceClient = null;

                this._serviceClient = new TournamentServiceClient();
            }

            if (this._serviceClient.ClientCredentials != null)
            {
                this._serviceClient.ClientCredentials.UserName.UserName =
                    ConfigurationManager.AppSettings["NFFServiceUsername"];
                this._serviceClient.ClientCredentials.UserName.Password =
                    ConfigurationManager.AppSettings["NFFServicePassword"];
            }
        }
    }
}
