﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using log4net;
using NTB.SportsData.Components.NFFMetaService;

namespace NTB.SportsData.Components.Nff.Populators
{
    public class SeasonPopulator
    {
        /// <summary>
        ///     The organization service client.
        /// </summary>
        private MetaServiceClient _serviceClient; // = new MetaServiceClient();

        /// <summary>
        ///     Static logger
        /// </summary>
        public static readonly ILog Logger = LogManager.GetLogger(typeof(SeasonPopulator));

        public SeasonPopulator()
        {
            this.CheckCheckServiceState();
        }

        public List<Season> GetSeasons()
        {
            var listSeasons = new List<Season>();

            // Get the differnet stuff that I need
            Logger.Debug("Getting seasons!");
            var seasons = this._serviceClient.GetSeasons();
            
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]) == false)
            {
                // Only get ongoing AND those that are less than two years from now...
                var yearFilterStart = Convert.ToInt32(DateTime.Today.AddYears(-2).Year);
                var filteredSeason = (from s in seasons where s.SeasonStartDate != null && (s.Ongoing && s.SeasonStartDate.Value.Year > yearFilterStart) select s).ToList();
                listSeasons.AddRange(filteredSeason);

            }
            else
            {
                var startDate = DateTime.Parse(ConfigurationManager.AppSettings["nfftestingdate"]);
                var endDate = DateTime.Parse(ConfigurationManager.AppSettings["nfftestingdate"]);

                var filteredSeason = (from s in seasons 
                                      where s.SeasonStartDate != null && s.SeasonEndDate != null && 
                                      (s.SeasonStartDate.Value.Date <= startDate.Date && s.SeasonEndDate.Value.Date >= endDate.Date) 
                                      && s.Ongoing
                                      select s).ToList();

                listSeasons.AddRange(filteredSeason);
            }

            return listSeasons;
        }

        private void CheckCheckServiceState()
        {
            if (this._serviceClient == null)
            {
                this._serviceClient = new MetaServiceClient();

                if (this._serviceClient.ClientCredentials != null)
                {
                    this._serviceClient.ClientCredentials.UserName.UserName =
                        ConfigurationManager.AppSettings["NFFServiceUsername"];
                    this._serviceClient.ClientCredentials.UserName.Password =
                        ConfigurationManager.AppSettings["NFFServicePassword"];

                    return;
                }
            }

            if (this._serviceClient.InnerChannel.State == CommunicationState.Faulted)
            {
                this._serviceClient.Close();
                this._serviceClient = null;

                this._serviceClient = new MetaServiceClient();

            }

            if (this._serviceClient.ClientCredentials != null)
            {
                this._serviceClient.ClientCredentials.UserName.UserName =
                    ConfigurationManager.AppSettings["NFFServiceUsername"];
                this._serviceClient.ClientCredentials.UserName.Password =
                    ConfigurationManager.AppSettings["NFFServicePassword"];
            }
        }
    }
}
