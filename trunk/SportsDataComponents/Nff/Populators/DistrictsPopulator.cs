﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using NTB.SportsData.Components.NFFOrganizationService;

namespace NTB.SportsData.Components.Nff.Populators
{
    public class DistrictsPopulator
    {
        /// <summary>
        ///     The organization service client.
        /// </summary>
        private OrganizationServiceClient _serviceClient; //  = new OrganizationServiceClient();

        public DistrictsPopulator()
        {

            this.CheckCheckServiceState();
        }

        public List<District> GetDistricts()
        {
            return this._serviceClient.GetDistricts().ToList();
        }

        private void CheckCheckServiceState()
        {
            if (this._serviceClient == null)
            {
                this._serviceClient = new OrganizationServiceClient();

                if (this._serviceClient.ClientCredentials != null)
                {
                    this._serviceClient.ClientCredentials.UserName.UserName =
                        ConfigurationManager.AppSettings["NFFServiceUsername"];
                    this._serviceClient.ClientCredentials.UserName.Password =
                        ConfigurationManager.AppSettings["NFFServicePassword"];

                    return;
                }
            }

            if (this._serviceClient.InnerChannel.State == CommunicationState.Faulted)
            {
                this._serviceClient.Close();
                this._serviceClient = null;

                this._serviceClient = new OrganizationServiceClient();
                
            }

            if (this._serviceClient.ClientCredentials != null)
            {
                this._serviceClient.ClientCredentials.UserName.UserName =
                    ConfigurationManager.AppSettings["NFFServiceUsername"];
                this._serviceClient.ClientCredentials.UserName.Password =
                    ConfigurationManager.AppSettings["NFFServicePassword"];
            }
        }
    }
}
