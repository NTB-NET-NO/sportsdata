﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.Text;
using NTB.SportsData.Components.NFFMetaService;


namespace NTB.SportsData.Components.Nff.Populators
{
    public class AgeCategoryTournamentPopulator
    {
        /// <summary>
        ///     The meta service client.
        /// </summary>
        private MetaServiceClient _serviceClient; 

        public AgeCategoryTournamentPopulator()
        {
            this.CheckCheckServiceState();
        }

        /// <summary>
        ///     The get age category tournament.
        /// </summary>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<AgeCategoryTournament> GetAgeCategoryTournament()
        {
            var tournamentAgeCategory = this._serviceClient.GetAgeCategoriesTournament();
            var filteredAgeCategories = (from act in tournamentAgeCategory where act.MinAge >= 12 select act).ToList();

            return filteredAgeCategories;
        }

        private void CheckCheckServiceState()
        {
            if (this._serviceClient == null)
            {
                this._serviceClient = new MetaServiceClient();

                if (this._serviceClient.ClientCredentials != null)
                {
                    this._serviceClient.ClientCredentials.UserName.UserName =
                        ConfigurationManager.AppSettings["NFFServiceUsername"];
                    this._serviceClient.ClientCredentials.UserName.Password =
                        ConfigurationManager.AppSettings["NFFServicePassword"];

                    return;
                }
            }

            if (this._serviceClient.InnerChannel.State == CommunicationState.Faulted)
            {
                this._serviceClient.Close();
                this._serviceClient = null;

                this._serviceClient = new MetaServiceClient();

            }

            if (this._serviceClient.ClientCredentials != null)
            {
                this._serviceClient.ClientCredentials.UserName.UserName =
                    ConfigurationManager.AppSettings["NFFServiceUsername"];
                this._serviceClient.ClientCredentials.UserName.Password =
                    ConfigurationManager.AppSettings["NFFServicePassword"];
            }
        }
    }
}
