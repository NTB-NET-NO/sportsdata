﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using NTB.SportsData.Components.NFFTournamentService;

namespace NTB.SportsData.Components.Nff.Populators
{
    public class TournamentPopulator
    {
        // TournamentServiceClient.GetTournamentsByDistrict(district.Id, season.Id, null);

        /// <summary>
        ///     The organization service client.
        /// </summary>
        private TournamentServiceClient _serviceClient; // = new TournamentServiceClient();

        public TournamentPopulator()
        {
            this.CheckCheckServiceState();
        }

        public List<Tournament> GetTournamentsByDistrictAndSeasonId(int districtId, int seasonId)
        {
            return this._serviceClient.GetTournamentsByDistrict(districtId, seasonId, null).ToList();
        }

        public List<TournamentTableTeam> GetTournamentTableTeam(int tournamentId, DateTime? selectedDateTime = null)
        {
            return this._serviceClient.GetTournamentStanding(tournamentId, null).ToList();
        }

        public Tournament GetTournamentById(int tournamentId)
        {
            return this._serviceClient.GetTournament(tournamentId);
        }

        private void CheckCheckServiceState()
        {
            if (this._serviceClient == null)
            {
                this._serviceClient = new TournamentServiceClient();

                if (this._serviceClient.ClientCredentials != null)
                {
                    this._serviceClient.ClientCredentials.UserName.UserName =
                        ConfigurationManager.AppSettings["NFFServiceUsername"];
                    this._serviceClient.ClientCredentials.UserName.Password =
                        ConfigurationManager.AppSettings["NFFServicePassword"];

                    return;
                }
            }

            if (this._serviceClient.InnerChannel.State == CommunicationState.Faulted)
            {
                this._serviceClient.Close();
                this._serviceClient = null;

                this._serviceClient = new TournamentServiceClient();

            }

            if (this._serviceClient.ClientCredentials != null)
            {
                this._serviceClient.ClientCredentials.UserName.UserName =
                    ConfigurationManager.AppSettings["NFFServiceUsername"];
                this._serviceClient.ClientCredentials.UserName.Password =
                    ConfigurationManager.AppSettings["NFFServicePassword"];
            }

            foreach (var operation in this._serviceClient.ChannelFactory.Endpoint.Contract.Operations)
            {
                var behavior = operation.Behaviors.Find<DataContractSerializerOperationBehavior>();
                if (behavior != null)
                {
                    behavior.MaxItemsInObjectGraph = 2147483647;
                }
            }

        }

    }
}
