﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Classes.Enums;

namespace NTB.SportsData.Components.Nff.Models
{
    public class XmlDocumentModel
    {
        /// <summary>
        ///     Gets or sets the federation key.
        /// </summary>
        public int FederationKey { get; set; }

        /// <summary>
        ///     Gets or sets the alert receivers.
        /// </summary>
        public List<AlertReceiver> AlertReceivers { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether create xml.
        /// </summary>
        public bool CreateXml { get; set; }

        /// <summary>
        ///     Gets or sets the date end.
        /// </summary>
        public DateTime DateEnd { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether date offset.
        /// </summary>
        public bool DateOffset { get; set; }

        /// <summary>
        ///     Gets or sets the date start.
        /// </summary>
        public DateTime DateStart { get; set; }

        /// <summary>
        ///     Gets or sets the discipline id.
        /// </summary>
        public int DisciplineId { get; set; }

        /// <summary>
        ///     Gets or sets the duration.
        /// </summary>
        public int Duration { get; set; }

        /// <summary>
        ///     Gets or sets the federation id.
        /// </summary>
        public int FederationId { get; set; }

        /// <summary>
        ///     Gets or sets the file output folder.
        /// </summary>
        public List<string> FileOutputFolder { get; set; }

        /// <summary>
        ///     Gets or sets the file output folders.
        /// </summary>
        public List<OutputFolder> FileOutputFolders { get; set; }

        /// <summary>
        ///     Gets or sets the operation mode.
        /// </summary>
        public OperationMode OperationMode { get; set; }

        /// <summary>
        ///     Gets or sets the poll style.
        /// </summary>
        public PollStyle PollStyle { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether populate database.
        /// </summary>
        public bool PopulateDatabase { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether purge.
        /// </summary>
        public bool Purge { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether push results.
        /// </summary>
        public bool PushResults { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether push schedule.
        /// </summary>
        public bool PushSchedule { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether results.
        /// </summary>
        public bool Results { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether schedule message.
        /// </summary>
        public bool ScheduleMessage { get; set; }

        /// <summary>
        ///     Gets or sets the schedule type.
        /// </summary>
        public string ScheduleType { get; set; }

        /// <summary>
        ///     Gets or sets the season id.
        /// </summary>
        public int SeasonId { get; set; }

        /// <summary>
        ///     Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        ///     Gets or sets the tournament id.
        /// </summary>
        public int TournamentId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether maintenance.
        /// </summary>
        public bool Maintenance { get; set; }

        /// <summary>
        /// Gets or sets the interval 
        /// </summary>
        public int Interval { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether standing.
        /// </summary>
        public bool Standing { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether match facts.
        /// </summary>
        public bool MatchFacts { get; set; }

        public List<Customer> Customers { get; set; }

        public bool UpdateDatabase { get; set; }

        public string OutputFolder { get; set; }

        public bool RenderResult { get; set; }

        public string JobName { get; set; }
    }
}
