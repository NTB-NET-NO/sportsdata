﻿using System;

namespace NTB.SportsData.Components.Nff.Models
{
    public class SportsDataDocument
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Version { get; set; }

        public int SportId { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public int TournamentId { get; set; }
    }
}