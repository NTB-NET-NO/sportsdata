// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NFFJobBuilder.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The nff job builder.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using NTB.SportsData.Components.Maintenance;
using NTB.SportsData.Components.Nff.DataFileCreators;
using NTB.SportsData.Components.Nff.Models;

namespace NTB.SportsData.Components.Nff
{
    using System;
    using System.Configuration;

    // Adding support for log4net
    using log4net;

    // Adding support for Quartz Scheduler
    using Quartz;

    /// <summary>
    /// The nff job builder.
    /// </summary>
    internal class NffJobBuilder : IJob
    {
        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(NffJobBuilder));

        /// <summary>
        /// Initializes static members of the <see cref="NffJobBuilder"/> class.
        /// </summary>
        static NffJobBuilder()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        // This class shall be used to get the matches from Norwegian Soccer Federation

        /// <summary>
        /// The execute.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public void Execute(IJobExecutionContext context)
        {
            Logger.Info("Executing JobExecutionContext" + context.JobDetail.Description);
            string outputFolder = string.Empty;

            string jobName = string.Empty;
            
            string operationMode = string.Empty;

            string contentType = string.Empty;

            string fixtureType = string.Empty;

            bool insertDatabase = false;
            
            bool results = false;
            
            DateTime dateOffset = DateTime.Now;
            
            int duration = 0;

            bool totalSchedule = false;

            int sportId = 0;

            // Mapping information sent from Component
            JobDataMap dataMap = context.JobDetail.JobDataMap;

            bool createXml = true;

            // Populate variables to be used to 
            try
            {
                // Cleaning up the code a bit. having all variable implementations here
                outputFolder = dataMap.GetString("OutputFolder");

                jobName = dataMap.GetString("JobName");
                
                operationMode = dataMap.GetString("OperationMode");
                
                insertDatabase = dataMap.GetBoolean("InsertDatabase");
                
                string scheduleType = dataMap.GetString("ScheduleType");
                
                results = dataMap.GetBoolean("Results");
                
                bool scheduleMessage = dataMap.GetBoolean("ScheduleMessage");
                
                duration = dataMap.GetInt("Duration");
                
                createXml = dataMap.GetBoolean("CreateXML");

                if (dataMap.Keys.Contains("DateOffset"))
                {
                    dateOffset = dataMap.GetDateTime("DateOffset");
                }

                if (dataMap.Keys.Contains("TotalSchedule"))
                {
                    totalSchedule = dataMap.GetBoolean("TotalSchedule");
                }

                if (dataMap.Keys.Contains("SportId"))
                {
                    sportId = dataMap.GetInt("SportId");
                }

                if (dataMap.Keys.Contains("ContentType"))
                {
                    contentType = dataMap.GetString("ContentType");
                }

                if (dataMap.Keys.Contains("FixtureType"))
                {
                    fixtureType = dataMap.GetString("FixtureType");
                }

                // And all logging here
                Logger.Debug("jobName: " + jobName);
                Logger.Debug("OutputFolder: " + outputFolder);
                Logger.Debug("OperationMode: " + operationMode);
                Logger.Debug(insertDatabase ? "InsertDatabase: true" : "InsertDatabase: false");
                Logger.Debug("ScheduleType: " + scheduleType);
                Logger.Debug(results ? "Results: true" : "Results: false");
                Logger.Debug(scheduleMessage ? "ScheduleMessage: true" : "ScheduleMessage: false");
                Logger.Debug("Duration: " + duration);
                Logger.Debug(createXml ? "CreateXML: true" : "CreateXML: false");
                Logger.Debug("DateOffset: " + dateOffset.ToShortDateString());
                Logger.Debug("ContentType: " + contentType);
                Logger.Debug("FixtureType: " + fixtureType);
            }
            catch (Exception exception)
            {
                Logger.Debug(exception);
                Logger.Debug(exception);
            }

            if (operationMode.ToLower() == "maintenance")
            {
                var maintentance = new Router();
                maintentance.CheckAllMissingMatches();
            }
            else
            {
                // Logging to make sure that we have it all
                Logger.Debug("DateOffset in TournamentMatches: " + dateOffset);

                // Creating the DataFileCreator object
                var datafilemodel = new DataFileModel{
                            OutputFolder = outputFolder, 
                            JobName = jobName,
                            SportId = sportId
                        };

                Logger.Debug("Outputfolder: " + outputFolder);

                if (insertDatabase)
                {
                    Logger.Debug("We shall insert data in the database");
                    datafilemodel.UpdateDatabase = true;
                }

                // This parameter tells us if we are to add results-tags to the XML
                if (results)
                {
                    Logger.Debug("We shall also render result");
                    datafilemodel.RenderResult = true;
                }

                Logger.Debug("Duration: " + duration);

                datafilemodel.DateStart = DateTime.Today;
                datafilemodel.DateEnd = DateTime.Today;

                if (dateOffset != DateTime.Today)
                {
                    datafilemodel.DateStart = dateOffset;
                    datafilemodel.DateEnd = dateOffset;
                }


                if (duration > 0)
                {
                    Logger.Debug("We shall create a longer period of matches");
                    datafilemodel.Duration = duration;

                    // Creating the date start
                    datafilemodel.DateStart = DateTime.Today;

                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                    {
                        datafilemodel.DateStart = DateTime.Parse(ConfigurationManager.AppSettings["nfftestingdate"]);
                    }

                    datafilemodel.DateEnd = datafilemodel.DateStart.AddDays(7);
                }
                else
                {
                    datafilemodel.DateStart = DateTime.Parse(ConfigurationManager.AppSettings["nfftestingdate"]);
                    datafilemodel.DateEnd = DateTime.Parse(ConfigurationManager.AppSettings["nfftestingdate"]);
                }

                Logger.DebugFormat("Date Start: {0}", datafilemodel.DateStart);
                Logger.DebugFormat("Date End: {0}", datafilemodel.DateEnd);
                   

                Logger.Debug("We are about to create XML File");
                datafilemodel.CreateXml = createXml;

                // Telling the job if this shall create a total schedule or not
                datafilemodel.TotalSchedule = totalSchedule;

                // Start getting the data
                if (totalSchedule == false)
                {
                    var creator = new DistrictDataFileCreator();
                    creator.Model = datafilemodel;
                    creator.CreateDataFileByDistrict();
                }
                else
                {
                    var creator = new TotalScheduleDataFileCreator();
                    creator.Model = datafilemodel;
                    creator.CreateTotalSchedule();

                }
            }
        }
    }
}