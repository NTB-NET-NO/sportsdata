﻿using NTB.SportsData.Components.Nff.Models;

namespace NTB.SportsData.Components.Nff.DataFileCreators.Abstracts
{
    public abstract class ADataFileCreator
    {
        /// <summary>
        /// The Data File Model which contains most (more than most) attributes for this class
        /// </summary>
        public DataFileModel Model { get; set; }
    }
}
