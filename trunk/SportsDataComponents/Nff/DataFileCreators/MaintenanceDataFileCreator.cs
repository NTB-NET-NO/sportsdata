﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nff.DataFileCreators.Abstracts;
using NTB.SportsData.Components.Nff.Facade;
using NTB.SportsData.Components.Nff.Models;
using NTB.SportsData.Components.Nff.Repositories;
using NTB.SportsData.Components.Nff.Repositories.Interfaces;
using NTB.SportsData.Utilities;

namespace NTB.SportsData.Components.Nff.DataFileCreators
{
    public class MaintenanceDataFileCreator : ADataFileCreator
    {
        private readonly ISoccerFacade _facade;

        public MaintenanceDataFileCreator()
        {
            this._facade = new SoccerFacade();
        }

        /// <summary>
        ///     Static logger
        /// </summary>
        public static readonly ILog Logger = LogManager.GetLogger(typeof(MaintenanceDataFileCreator));

        /// <summary>
        ///     The create data file maintenance.
        /// </summary>
        /// <param name="matchId">
        ///     The match id.
        /// </param>
        public void CreateDataFileMaintenance(int matchId)
        {
            Logger.Debug("We are in CreateDataFileMaintenance!");

            // Creating the SportsDatabase object
            var db = new NffSportsDataDatabase();
            Logger.Debug("We have gotten database object");

            // This could mean that we don't have the data in our database, then we should check with the service
            var tournament = new Tournament();
            var tournamentId = 0;
            DateTime? matchDate = null;

            Match match = null;

            IMatchRepository matchRepository = new MatchRepository();
            try
            {
                // Now we that have an ID, we shall get todays matches
                Logger.Debug("Getting the Matches Object and putting it into the Match list using TournamentId");
                match = this._facade.GetMatchById(matchId, false, false, false, false);
                matchDate = match.MatchDate;

                // Check if match is in the database

                var foundMatch = matchRepository.GetMatchByMatchAndSportId(match.Id, this.Model.SportId);

                // the match is is not in the database, we must add it..
                if (foundMatch == null)
                {
                    matchRepository.InsertMatch(match, this.Model.SportId);
                }

                Logger.Debug("This is match match.TournamentAgeCategoryId: " + match.TournamentAgeCategoryId);

                Logger.Debug("Getting Match object and using this to get the tournamentid: " + match.TournamentId);

                tournamentId = match.TournamentId;
                Logger.Debug("Now getting the Tournament Object based on the Tournament ID: " + tournamentId);

                // Getting the Tournament Object based on the TournamentId
                tournament = this._facade.GetTournamentById(tournamentId);

                if (tournament == null)
                {
                    return;
                }
                // tournament = this.TournamentServiceClient.GetTournament(tournamentId);

                Logger.Debug("Tournament:" + tournament.Name + ".");
                Logger.Debug("Done getting Tournament Object");
            }
            catch (Exception exception)
            {
                if (exception.Message == "No matches found.")
                {
                    matchRepository.UpdateMatchNotFound(match, tournament);
                }

                Logger.Error(exception.Message);

                Logger.Error(exception.StackTrace);

                // Consider this one and remove if it gives to many e-mails
                Mail.SendException(exception);
            }

            var customers = new List<Customer>();

            // Checking if the tournament is a national team tournament
            if (tournament.NationalTeamTournament == false)
            {
                // Now I can check against the database, because I now know that the match exists... 
                customers = db.GetListOfCustomersByMatchId(Convert.ToInt32(matchId), this.Model.SportId);
            }
            else
            {
                var customer = new Customer { Name = "NTB" };

                customers.Add(customer);
            }

            Logger.Debug("Number of Customers: " + customers.Count);

            if (customers.Any())
            {
                Logger.Debug("Number of customers: " + customers.Count);
                foreach (var customer in customers)
                {
                    Logger.Debug("Customer: " + customer.Name);
                }

                // Seems kinda odd to ask for this when we already have it in the match object
                if (match != null)
                {
                    tournamentId = match.TournamentId;
                }

                Logger.Debug("Getting TournamentId: " + tournamentId);

                try
                {
                    Logger.Debug("publish result: " + tournament.PublishResult);
                    if (tournament.PublishResult)
                    {
                        Logger.Debug("publish result is true");

                        // Finding out if we are to get referee, squad and match events for this tournament
                        var agecatid = tournament.TournamentAgeCategory;

                        var tournamentages = this._facade.GetAgeCategoryTournament();

                        //var tournamentages =
                        //    new List<AgeCategoryTournament>(this.MetaServiceClient.GetAgeCategoriesTournament());

                        var filteredAgeCategories =
                            tournamentages.Find(t => t.Id == agecatid && t.ToAge >= 12);

                        // Setting some variables to false
                        var getSquad = false;
                        var getReferee = false;
                        var getEvents = false;

                        // We only want squad, referee and events from mathces that are in the AgeCategory Toppfotball
                        Logger.Debug("Checking if we shall have more data");

                        if (filteredAgeCategories != null)
                        {
                            // Do I need to do this as I am filtering below?
                            if (filteredAgeCategories.ToAge >= 12)
                            {
                                // Checking if this is a toppfotball classification
                                if (filteredAgeCategories.Name == "Toppfotball")
                                {
                                    Logger.Debug("Toppfotball!");
                                    getSquad = true;
                                    getReferee = true;
                                    getEvents = true;
                                }

                                Logger.Debug("Getting all matches in this tournament for this day!");
                                try
                                {
                                    // Getting matches that are played today (but then getting more than todays matches)
                                    // using the date that came with the Push notification
                                    DateTime? tournamentDate = DateTime.Today;
                                    if ((matchDate != null) && (matchDate != tournamentDate))
                                    {
                                        tournamentDate = matchDate;
                                    }

                                    var matches = this._facade.GetMatchesByTournament(tournamentId, getSquad, getReferee, true,
                                        getEvents, tournamentDate);

                                    // I guess I need to have some Linq-filter here
                                    {
                                        var filteredMatches = matches.FindAll(m => m.MatchDate == DateTime.Today);

                                        // If we have hits, we shall 
                                        if (filteredMatches.Any())
                                        {
                                            Logger.Debug("Number of hits: " + filteredMatches.Count);

                                            // And populating it with filteredMatches instead
                                            matches = filteredMatches;
                                        }
                                    }

                                    // Creating the Standings object that we will be using when creating XML
                                    List<TournamentTable> standings = null;

                                    // Checking if this tournament is one that 
                                    if (tournament.PublishTournamentTable)
                                    {
                                        Logger.Debug("Getting the Standings");

                                        try
                                        {
                                            standings = this._facade.GetTournamentTableTeam(tournamentId, DateTime.Today);
                                        }
                                        catch (Exception exception)
                                        {
                                            Logger.Debug(exception.Message);
                                            if (exception.Message == "No tournament standings found.")
                                            {
                                                Logger.Info("No standings found for tournament " + tournament.Name);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Logger.InfoFormat("The standings from tournament {0} cannot publish results!",
                                            tournament.Name);
                                    }

                                    Logger.Debug("Creating XML-structure!");

                                    // Creating filename
                                    var districtId = tournament.DistrictId;

                                    // Creating the XMLDocument
                                    var xmldocumentModel = new XmlDocumentModel
                                    {
                                        SportId = this.Model.SportId,
                                        CreateXml = this.Model.CreateXml,
                                        Customers = customers,
                                        OutputFolder = this.Model.OutputFolder

                                    };
                                    
                                    var xmlDocumentCreator = new SoccerXmlDocument(xmldocumentModel);
                                    
                                    var doc = xmlDocumentCreator.CreateXmlStructure(matches, tournament,
                                        filteredAgeCategories, standings, false, customers);

                                    // Creating the filename
                                    var fileNames = xmlDocumentCreator.CreateFileName(tournament.DistrictId,
                                        tournament.Id, null, this.Model.SportId);

                                    if (doc != null)
                                    {
                                        // Now creating the XML-file
                                        Logger.Debug("Creating XML-File");
                                        xmlDocumentCreator.WriteXmlFile(doc, tournament.Name, tournament.Id, fileNames,
                                            districtId);
                                    }
                                }
                                catch (Exception exception)
                                {
                                    if (exception.Message == "No matches found.")
                                    {
                                        matchRepository.UpdateMatchNotFound(match, tournament);
                                    }
                                    else
                                    {
                                        Logger.Error(exception.Message);
                                        Logger.Error(exception.StackTrace);

                                        // Consider this one and remove if it sends to many mails
                                        Mail.SendException(exception);
                                    }
                                }
                            } // End of MaxAge-check
                        } // End of object-check
                    }
                    else
                    {
                        Logger.InfoFormat("The results from tournament {0} cannot publish results!", tournament.Name);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Info("We have received an exception while doing match or tournament lookup");
                    Logger.Info("====================================================================");
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
            }
            else
            {
                Logger.Debug("No customers is receiving this result");
            }
        }
    }
}
