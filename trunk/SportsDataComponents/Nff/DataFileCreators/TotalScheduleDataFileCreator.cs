﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nff.DataFileCreators.Abstracts;
using NTB.SportsData.Components.Nff.DataFileGenerator;
using NTB.SportsData.Components.Nff.Facade;
using NTB.SportsData.Components.Nff.Models;

namespace NTB.SportsData.Components.Nff.DataFileCreators
{
    public class TotalScheduleDataFileCreator : ADataFileCreator
    {
        private readonly ISoccerFacade _facade;

        public TotalScheduleDataFileCreator()
        {
            this._facade = new SoccerFacade();
        }
        /// <summary>
        ///     Static logger
        /// </summary>
        public static readonly ILog Logger = LogManager.GetLogger(typeof(TotalScheduleDataFileCreator));

        /// <summary>
        ///     The create total schedule.
        /// </summary>
        public void CreateTotalSchedule()
        {
            

            // Checking the service state
            try
            {
                var soccerFacade = new SoccerFacade();
                var districts = soccerFacade.GetDistricts();
                var seasons = soccerFacade.GetSeasons();

                var ageCategoryTournaments = soccerFacade.GetAgeCategoryTournament();

                List<TournamentTable> tournamentTable;

                foreach (var district in districts)
                {
                    foreach (var season in seasons)
                    {
                        foreach (var ageCategoryTournament in ageCategoryTournaments)
                        {
                            var tournaments = soccerFacade.GetTournamentsByDistrictAndSeasonId(district.Id, season.Id);

                            var filteredTournaments =
                                tournaments.FindAll(t => t.TournamentAgeCategory == ageCategoryTournament.Id);

                            foreach (var tournament in filteredTournaments)
                            {
                                var matches = soccerFacade.GetMatchesByTournament(tournament.Id, false, false,
                                    this.Model.RenderResult, false, null);

                                var filteredAgeCategory =
                                    (from act in ageCategoryTournaments
                                     where act.Id == tournament.TournamentAgeCategory
                                     select act).Single();

                                if (this.Model.RenderResult)
                                {
                                    try
                                    {
                                        /*
                                        * Legger du på timestamp får du endringer gjort etter datoen du sender med. Sender du med NULL blir ikke timestamp tatt hensyn til.
                                        */
                                        if (tournament.PublishTournamentTable)
                                        {
                                            tournamentTable = soccerFacade.GetTournamentTableTeam(tournament.Id);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Error(ex);
                                    }
                                }
                                // Creating the XMLDocument
                                var xmldocumentModel = new XmlDocumentModel
                                {
                                    SportId = this.Model.SportId,
                                    CreateXml = this.Model.CreateXml,
                                };
                                var dataFileGenerator = new MainDataFileGenerator();
                                
                                dataFileGenerator.CreateDataFile(tournament, matches, this.Model.RenderResult, this.Model.SportId, filteredAgeCategory);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error("An exception has occured!", e);

                Logger.Info("Cannot get Districts. Connection to server might be down");
            }
        }
    }
}
