// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NffPushDatafileCreator.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the NffPushDatafileCreator type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nff.Facade;
using NTB.SportsData.Components.Nff.Models;
using NTB.SportsData.Components.Nff.Repositories.Interfaces;
using NTB.SportsData.Components.PublicRepositories;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;
using NTB.SportsData.Utilities;
using MatchRepository = NTB.SportsData.Components.Nff.Repositories.MatchRepository;

namespace NTB.SportsData.Components.Nff.DataFileCreators
{
    /// <summary>
    /// The nff push datafile creator.
    /// </summary>
    public class PushDatafileCreator
    {
        /// <summary>
        ///     Static logger
        /// </summary>
        public static readonly ILog Logger = LogManager.GetLogger(typeof(PushDatafileCreator));

        /// <summary>
        /// The match id.
        /// </summary>
        public int MatchId = 0;

        private readonly ISoccerFacade _facade;

        public DataFileModel Model { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="PushDatafileCreator"/> class.
        /// </summary>
        public PushDatafileCreator()
        {
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }

            this._facade = new SoccerFacade();
        }

        /// <summary>
        /// Creating datafile based on the information in the push message
        /// </summary>
        /// <param name="sportId">
        /// The sport Id.
        /// </param>
        public void CreateDataFileFromPush(int sportId)
        {
            Logger.Debug("We are in CreateDataFileFromPush!");

            Logger.Debug("We are creating a NFF SportsDataDatabase Object!");

            // NffSportsDataDatabase db = new NffSportsDataDatabase();
            IMatchRepository matchRepository = new MatchRepository();
            Logger.Debug("We have gotten database object");

            // Waiting for some seconds (set in config-file) before we continue. This to be sure that we are getting the tournament table
            var intWait = Convert.ToInt32(ConfigurationManager.AppSettings["pubnub_wait_seconds"]);

            if (intWait > 0)
            {
                Logger.InfoFormat("We are waiting {0} seconds before proceeding", intWait);
                Thread.Sleep(intWait * 1000);
            }

            try
            {
                // Now we have an ID, we shall get todays matches
                Logger.InfoFormat("Getting the Matches Object and putting it into the Match list using MatchId: {0}", this.Model.MatchId);
                var match = this._facade.GetMatchById(this.Model.MatchId, false, false, false, false);
                
                var matchDate = match.MatchDate;
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["livetesting"]))
                {
                    // matchDate = DateTime.Today.AddDays(-1);
                }

                // Check if match is in the database
                
                var listOfMatchesWithoutResults = matchRepository.GetListOfMatchesWithoutResults();

                // this.MatchId, sportId

                Logger.Debug(
                    "This is match match.TournamentAgeCategoryId: " + match.TournamentAgeCategoryId);

                Logger.Info("Using Matchobject to get the tournamentid: " + match.TournamentId);

                var tournamentId = match.TournamentId;
                
                Logger.Info(
                    "Getting the Tournament Object based on the Tournament ID: " + tournamentId);

                // Getting the Tournament Object based on the TournamentId
                var tournament = this._facade.GetTournamentById(tournamentId);
                if (tournament == null)
                {
                    return;
                }

                if (match.TournamentId == 0)
                {
                    match.TournamentId = tournament.Id;
                }

                // is match in database
                var localMatch = matchRepository.GetMatchByMatchAndSportId(match.Id, sportId);
                if (localMatch.MatchName == null)
                {
                    matchRepository.InsertMatch(match, sportId);
                    
                    // Also updating the sportsdata-input_matches table (if the match is there)
                }

                Logger.DebugFormat("Tournament: {0}", tournament.Name);
                Logger.Debug("Done getting Tournament Object");

                if (!listOfMatchesWithoutResults.Contains(match.Id))
                {
                    matchRepository.InsertMatchNotFound(match, tournament);
                }

                var customers = new List<Customer>();

                // Checking if the tournament is a national team tournament
                if (tournament.NationalTeamTournament == false)
                {
                    ICustomerDataMapper customerDataMapper = new CustomerRepository(); 

                    // Now I can check against the database, because I now know that the match exists... 
                    if (match.TournamentId != 0)
                    {
                        Logger.InfoFormat("Getting customers by match.TournamentId: {0}", match.TournamentId);
                        Logger.DebugFormat("Sport Id is: {0}", sportId);
                        customers = customerDataMapper.GetCustomerByTournamentAndSportId(sportId, match.TournamentId);
                    }
                    else
                    {
                        customers = customerDataMapper.GetCustomerByMatchAndSportId(match.Id, sportId);
                    }    
                }
                else
                {
                    var customer = new Customer { Name = "NTB" };

                    customers.Add(customer);
                }

                Logger.Info("Number of Customers: " + customers.Count());

                if (customers.Any())
                {
                    Logger.Debug("Number of customers: " + customers.Count());
                    foreach (var customer in customers)
                    {
                        Logger.Debug("Customer: " + customer.Name);
                    }

                    try
                    {
                        // Inverted the if so that we get fewer levels
                        Logger.Debug("publish result: " + tournament.PublishResult);
                        if (!tournament.PublishResult)
                        {
                            Logger.InfoFormat("The results from tournament {0} cannot publish results!", tournament.Name);
                            return;
                        }

                        Logger.Debug("publish result is true");

                        // Finding out if we are to get referee, squad and match events for this tournament
                        int? agecatid = tournament.TournamentAgeCategory;

                        var tournamentages = this._facade.GetAgeCategoryTournament();
                            
                        var filteredAgeCategories =
                            tournamentages.Find(t => t.Id == agecatid && t.ToAge >= 12);

                        // Setting some variables to false
                        var getSquad = false;
                        var getReferee = false;
                        var getEvents = false;

                        // We only want squad, referee and events from mathces that are in the AgeCategory Toppfotball
                        Logger.Debug("Checking if we shall have more data");

                        if (filteredAgeCategories == null)
                        {
                            Logger.Info("Filter AgeCategory returned no data");
                            return;
                        }

                        // Do I need to do this as I am filtering below?
                        if (filteredAgeCategories.ToAge < 12)
                        {
                            Logger.Info("AgeCategory.MaxAge less than 12. Cannot create data");
                            return;
                        }

                        // Checking if this is a toppfotball classification
                        if (filteredAgeCategories.Name == "Toppfotball")
                        {
                            Logger.Debug("Toppfotball!");
                            getSquad = true;
                            getReferee = true;
                            getEvents = true;
                        }

                        Logger.Debug("Getting all matches in this tournament for this day!");

                        // We need to change this so that it only generates a file with one date in it - and that date must be from the date in the push
                        // Getting matches that are played today (but then getting more than todays matches)
                        // using the date that came with the Push notification
                        DateTime? tournamentDate = DateTime.Today;
                        if (matchDate != tournamentDate)
                        {
                            if (matchDate != null)
                            {
                                var matchDateString = string.Format("{0}-{1}-{2}", matchDate.Value.Year, matchDate.Value.Month, matchDate.Value.Day);

                                tournamentDate = Convert.ToDateTime(matchDateString);
                            }
                        }

                        Logger.Debug(
                            "Calling GetMatchesByTournament with following parameters (Except tournamentDate):");
                        Logger.DebugFormat("tournamentId; {0}, getSquard: {1}, getReferee: {2}, getEvents: {3}, tournamentDate: {4}",
                            tournamentId, getSquad, getReferee, getEvents, tournamentDate);
                        // matchDate = DateTime.Today.Date;

                        List<Match> matches; //  = new List<Match>();

                        try
                        {
                            Logger.InfoFormat(
                                "Getting matches from the tournament with id {0} before try",
                                tournamentId);
                            matches =
                                new List<Match>(this._facade.GetMatchesByTournament(tournamentId, getSquad, getReferee, true, getEvents, tournamentDate));
                            matches = MatchFilters.GetTodaysMatches(matches, matchDate);
                            
                        }
                        catch (Exception exception)
                        {
                            Logger.Info("Exception caught while getting information from server");
                            Logger.Error(exception.Message);
                            Logger.Info("Trying to get the data without referees");
                            matches =
                                new List<Match>(this._facade.GetMatchesByTournament(
                                        tournamentId, false, false, true, false, tournamentDate));
                            matches = MatchFilters.GetTodaysMatches(matches, matchDate);
                        }

                        // If we don't have any matches, well, then we return back home....
                        if (matches.Count == 0)
                        {
                            Logger.Info("Date filter returned no matches...");

                            return;
                        }

                        // Creating the Standings object that we will be using when creating XML
                        List<TournamentTable> standings = null;

                        
                        var soccerXmlDocument = new SoccerXmlDocument(new XmlDocumentModel());
                        soccerXmlDocument.Model.RenderResult = true;
                        soccerXmlDocument.Model.OutputFolder = this.Model.OutputFolder;
                        soccerXmlDocument.Model.FileOutputFolder = this.Model.FileOutputFolder;
                        soccerXmlDocument.Model.CreateXml = this.Model.CreateXml;
                        soccerXmlDocument.Model.Customers = this.Model.Customers;
                        soccerXmlDocument.Model.AlertReceivers = this.Model.AlertReceivers;
                        soccerXmlDocument.Model.DateEnd = this.Model.DateEnd;
                        soccerXmlDocument.Model.DateStart = this.Model.DateStart;
                        soccerXmlDocument.Model.DisciplineId = this.Model.DisciplineId;
                        soccerXmlDocument.Model.SportId = this.Model.SportId;
                        soccerXmlDocument.Model.FederationId = this.Model.FederationId;
                        
                        // Checking if this tournament is one that 
                        if (tournament.PublishTournamentTable)
                        {
                            
                            soccerXmlDocument.Model.Standing = true;
                            Logger.Info("Getting the Standings");

                            try
                            {
                                standings =
                                    new List<TournamentTable>(
                                        this._facade.GetTournamentTableTeam(tournamentId));
                            }
                            catch (Exception exception)
                            {
                                Logger.Debug(exception.Message);
                                if (exception.Message == "No tournament standings found.")
                                {
                                    Logger.InfoFormat("No standings found for tournament {0}", tournament.Name);
                                }
                            }
                        }
                        else
                        {
                            Logger.InfoFormat(
                                "The standings from tournament {0} cannot publish results!", tournament.Name);
                        }

                        Logger.Info("Creating XML-structure!");

                        // Creating filename
                        var districtId = tournament.DistrictId;

                        // Creating the XMLDocument
                        var doc = soccerXmlDocument.CreateXmlStructure(
                            matches, tournament, filteredAgeCategories, standings, false, customers);

                        var fileNames = soccerXmlDocument.CreateFileName(tournament.DistrictId, tournament.Id, null, sportId);

                        if (doc != null)
                        {
                            // Now creating the XML-file
                            Logger.Info("Creating XML-File");
                            
                            soccerXmlDocument.WriteXmlFile(
                                doc,
                                tournament.Name,
                                tournament.Id,
                                fileNames,
                                districtId);
                        }
                    }
                    catch (Exception exception)
                    {
                        if (exception.Message == "No matches found.")
                        {
                            
                            matchRepository.InsertMatchNotFound(match, tournament);
                        }
                        else
                        {
                            Logger.Error(exception.Message);
                            Logger.Error(exception.StackTrace);

                            // Consider this one and remove if it sends to many mails
                            Mail.SendException(exception);
                        }
                    }

                    // End of MaxAge-check
                }
                else
                {
                    Logger.Debug("No customers is receiving this result");
                }

                // Now we update the sportsdatainput-database
                if (match.HomeTeamGoals != null && match.AwayTeamGoals != null)
                {
                    matchRepository.UpdateSportsDataInputMatches(match);
                }

            }
            catch (Exception exception)
            {
                Logger.Info("We have received an exception while doing match or tournament lookup");
                Logger.Info("====================================================================");
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
            finally
            {
                // "Disposing" the DB Object
                Logger.Info("Disposing database object in finally");
                
            }
        }

        /// <summary>
        ///     The Home Team Goals
        /// </summary>
        public int HomeTeamGoals { get; set; }

        /// <summary>
        ///     The Away Team Goals
        /// </summary>
        public int AwayTeamGoals { get; set; }
    }
}