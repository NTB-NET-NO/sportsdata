﻿using NTB.SportsData.Components.Nff.Configurators;
using NTB.SportsData.Components.Nff.Facade;
using NTB.SportsData.Components.Nff.Importers;

namespace NTB.SportsData.Components.Nff.DataFileCreators.Helpers
{
    public class DistrictPopulatorHelper
    {
        public int SportId { get; set; }
        public bool PopulateDistricts()
        {
            // Getting the districts from NFF
            ISoccerFacade facade = new SoccerFacade();
            var districts = facade.GetDistricts();
            var importer = new DistrictImporter();
            importer.ImportDistricts(districts);

            var municipalityConfigurator = new MunicipalitiesConfigurator();
            var tournamentConfigurator = new TournamentConfigurator();
            var matchConfigurator = new MatchConfigurator();

            var seasons = facade.GetSeasons();
            foreach (var district in districts)
            {
                var municipalityConfigured = municipalityConfigurator.ConfigureMunicipalities(district);
                if (!municipalityConfigured)
                {
                    return false;
                }

                tournamentConfigurator.ConfigureTournaments(district, seasons);

                // We are getting matches from the NFF database that happens today. 
                matchConfigurator.ConfigureMatches(district, this.SportId);
            }
            return true;
        }
    }
}
