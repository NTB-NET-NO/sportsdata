﻿using NTB.SportsData.Components.Nff.Facade;
using NTB.SportsData.Components.Nff.Importers;

namespace NTB.SportsData.Components.Nff.DataFileCreators.Helpers
{
    public class SeasonPopulatorHelper
    {
        public bool PopulateSeasons()
        {
            ISoccerFacade facade = new SoccerFacade();

            var seasons = facade.GetSeasons();
            var importer = new SeasonImporter();
            return importer.ImportSeasons(seasons);
        }
    }
}
