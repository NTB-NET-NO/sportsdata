﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nff.DataFileCreators.Abstracts;
using NTB.SportsData.Components.Nff.DataFileGenerator;
using NTB.SportsData.Components.Nff.Facade;
using NTB.SportsData.Components.Nff.Models;
using NTB.SportsData.Components.Nff.Repositories;
using NTB.SportsData.Components.Nff.Repositories.Interfaces;

namespace NTB.SportsData.Components.Nff.DataFileCreators
{
    public class DistrictDataFileCreator : ADataFileCreator
    {
        private ISoccerFacade _facade;

        public DistrictDataFileCreator()
        {
            this._facade = new SoccerFacade();
        }
        /// <summary>
        ///     Static logger
        /// </summary>
        public static readonly ILog Logger = LogManager.GetLogger(typeof(DistrictDataFileCreator));

        public void CreateDataFileByDistrict()
        {
            Logger.Debug("================================");
            Logger.Debug("== In CreateDataFileByDistric ==");
            Logger.Debug("================================");

            // Checking the service state
            ISoccerFacade facade = new SoccerFacade();
            var districts = facade.GetDistricts();

            var seasons = facade.GetSeasons();

            try
            {
                // During testing we are to use last season (season 2010)
                Logger.Debug("Getting Age Categories!");


                var filteredAgeCategories = facade.GetAgeCategoryTournament();

                Logger.Debug("filteredAgeCategories.Length: " + filteredAgeCategories.Count());

                
                foreach (var district in districts)
                {
                    // Getting todays matches
                    List<Match> matches = null;

                    Logger.DebugFormat("DistictID: {0}, Name: {1}", district.Id, district.Name);

                    // Getting tournaments based on districtID and SeasonID
                    var includeSquad = false;

                    var includeReferees = false;

                    var includeResults = false;

                    var includeEvents = false;

                    if (this.Model.RenderResult)
                    {
                        includeResults = this.Model.RenderResult;
                        includeSquad = true;
                        includeEvents = true;
                        includeReferees = true;
                    }

                    if (this.Model.Duration == 0)
                    {
                        /*
                         * If we add timestamp you will get changes after the date you are adding. If you use NULL the timestamp is not being taken care of.
                         * 
                         */
                        Logger.Debug("Duration is zero. Getting matches by DateInterval!");
                        try
                        {
                            // We might get that there aren't any matches, so we don't do much if there is such an incident.
                            if (this.Model.DateStart == null)
                            {
                                this.Model.DateStart = DateTime.Today;
                                this.Model.DateEnd = this.Model.DateStart;
                            }

                            if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                            {
                                this.Model.DateStart = DateTime.Parse(ConfigurationManager.AppSettings["nfftestingdate"]);
                                this.Model.DateEnd = DateTime.Parse(ConfigurationManager.AppSettings["nfftestingdate"]);
                            }
                            matches = this._facade.GetMatchesByDistrict(district.Id);
                            // matches = this.TournamentServiceClient.GetMatchesByDistrict(this.DistrictId, this.DateStart, this.DateEnd, includeSquad, includeReferees, includeResults, includeEvents, null).ToList();

                            // What are we really doing here?
                            IMatchRepository matchRepository = new MatchRepository();
                            foreach (var match in matches)
                            {
                                var foundMatch = matchRepository.GetMatchByMatchAndSportId(match.Id, this.Model.SportId);
                            }
                        }
                        catch (Exception exception)
                        {
                            if (exception.Message.ToLower() != "no matches found.")
                            {
                                Logger.Error(exception.Message);
                                Logger.Error(exception.StackTrace);
                            }
                        }
                    }

                    if (this.Model.Duration > 0)
                    {
                        Logger.Debug("Duration is bigger than zero. Getting matches by DateInterval!");
                        try
                        {
                            if (this.Model.DateStart == null)
                            {
                                this.Model.DateStart = DateTime.Today.AddDays(-1);
                                this.Model.DateEnd = DateTime.Today;
                            }

                            // We might get no-matches-found error
                            matches =
                                this._facade.GetMatchesByDateInterval(this.Model.DateStart, this.Model.DateEnd, district.Id,
                                    includeSquad, includeReferees, includeResults, includeEvents)
                                    .ToList();
                        }
                        catch (Exception exception)
                        {
                            if (exception.Message.ToLower() != "no matches found.")
                            {
                                Logger.Error(exception.Message);
                                Logger.Error(exception.StackTrace);
                            }
                        }
                    }

                    if (matches == null || matches.Count == 0)
                    {
                        Logger.Debug("No matches for district " + district.Name);
                        continue;
                    }

                    try
                    {
                        /*
                         * If we add timestamp you will get changes after the date you are adding. If you use NULL the timestamp is not being taken care of.
                         * 
                         */
                        foreach (var season in seasons)
                        {
                            try
                            {
                                Logger.InfoFormat("Getting TournamentsbyDistrict: {0}, Season: {1}", district.Name,
                                    season.Id);

                                if (this._facade == null)
                                {
                                    this._facade = new SoccerFacade();
                                }
                                var tournaments = this._facade.GetTournamentsByDistrictAndSeasonId(district.Id, season.Id);

                                // We are looping through the Age Categories
                                foreach (var ageCategoryTournament in filteredAgeCategories)
                                {
                                    var filteredTournaments =
                                        tournaments.FindAll(t => t.TournamentAgeCategory == ageCategoryTournament.Id);

                                    Logger.DebugFormat("ageCategoryTournament: {0}", ageCategoryTournament.Name);

                                    if (!filteredTournaments.Any())
                                    {
                                        continue;
                                    }

                                    foreach (var tournament in filteredTournaments)
                                    {
                                        var currentTournament = tournament;

                                        var filteredMatches = (from m in matches
                                                               where
                                                               (m.MatchDate != null) &&
                                                                   m.TournamentId == currentTournament.Id &&
                                                                   m.MatchDate.Value.Date >= this.Model.DateStart.Date &&
                                                                   m.MatchDate.Value.Date <= this.Model.DateEnd.Date
                                                               select m).ToList();

                                        var filteredAgeCategory =
                                            (from act in filteredAgeCategories
                                             where act.Id == currentTournament.TournamentAgeCategory
                                             select act).Single();
                                        
                                        // If we get matches, we shall create the datafile.
                                        if (filteredMatches.Any())
                                        {
                                            Logger.Debug("Total matches found: " + filteredMatches.Count());
                                            // Creating the XMLDocument
                                            var xmldocumentModel = new XmlDocumentModel
                                            {
                                                SportId = this.Model.SportId,
                                                CreateXml = this.Model.CreateXml,
                                                TournamentId = currentTournament.Id,
                                            };
                                            var outputFolders = new List<OutputFolder>();
                                            var outputFolder = new OutputFolder {OutputPath = this.Model.OutputFolder};
                                            outputFolders.Add(outputFolder);
                                            xmldocumentModel.FileOutputFolders = outputFolders;
                                            // var xmlDocumentCreator = new SoccerXmlDocument(xmldocumentModel);
                                            var mainDataFileGenerator = new MainDataFileGenerator();
                                            mainDataFileGenerator.Model = xmldocumentModel;
                                            mainDataFileGenerator.CreateDataFile(tournament, filteredMatches,
                                                this.Model.RenderResult, this.Model.SportId, filteredAgeCategory);
                                        }
                                    }
                                }
                            }
                            catch (Exception exception)
                            {
                                Logger.Error(exception.Message);
                                if (exception.Message.ToLower() != "no tournaments found.")
                                {
                                    Logger.Error(exception.StackTrace);
                                }
                            }
                        }
                    }
                    catch (Exception exception)
                    {
                        Logger.Error(exception);
                        Logger.Error(exception.StackTrace);
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                Logger.Error(exception.StackTrace);
            }
        }
        
    }
}
