﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nff.DataFileCreators.Abstracts;
using NTB.SportsData.Components.Nff.Facade;
using NTB.SportsData.Components.Nff.Models;

namespace NTB.SportsData.Components.Nff.DataFileCreators
{
    public class TournamentDataFileCreator : ADataFileCreator
    {
        private readonly ISoccerFacade _facade;

        public TournamentDataFileCreator()
        {
            this._facade = new SoccerFacade();
        }

        /// <summary>
        ///     Static logger
        /// </summary>
        public static readonly ILog Logger = LogManager.GetLogger(typeof(TournamentDataFileCreator));

        public Tournament GetTournament(int tournamentId)
        {
            // Getting the Tournament Object based on the TournamentId
            var tournament = this._facade.GetTournamentById(tournamentId);
            if (tournament == null)
            {
                return null;
            }
            
            Logger.DebugFormat("Tournament: {0}", tournament.Name);
            
            return tournament;
        }

        public List<Match> GetMatchesByTournament(int tournamentId)
        {
            // Now that we have some information about the tournament, we can do more
            return this._facade.GetMatchesByTournament(tournamentId, true, true, true, true, null);
        }

        public List<Match> FilterMatches(List<Match> matches, DateTime currentDate)
        {
            return (from m in matches
                    where
                        (m.MatchDate != null) && m.MatchDate.Value.ToShortDateString() == currentDate.ToShortDateString()
                    select m).ToList();
        }

        /// <summary>
        ///     The create tournament data file.
        /// </summary>
        /// <param name="customers">
        ///     The customers.
        /// </param>
        /// <param name="tournamentId">
        ///     The tournament id.
        /// </param>
        /// <param name="startDateTime">
        ///     The start date time.
        /// </param>
        /// <param name="endDateTime">
        ///     The end Date Time.
        /// </param>
        public void CreateTournamentDataFile(List<Customer> customers, int tournamentId, DateTime startDateTime,
            DateTime endDateTime)
        {
            Logger.Debug("We are in CreateTournamentDataFile!");

            this.Model.RenderResult = true;

            try
            {
                var tournament = this.GetTournament(tournamentId);

                if (tournament == null)
                {
                    return;
                }
                // Now that we have some information about the tournament, we can do more
                var matches = this.GetMatchesByTournament(tournament.Id);

                for (var currentDate = DateTime.Parse(startDateTime.ToShortDateString());
                    endDateTime.CompareTo(currentDate) >= 0;
                    currentDate = currentDate.AddDays(1.0))
                {
                    var filteredMatches = this.FilterMatches(matches, currentDate);

                    if (filteredMatches.Count == 0)
                    {
                        Logger.InfoFormat("No matches found in tournament {0} on date {1}", tournamentId, currentDate);
                        continue;
                    }

                    // Creating the Standings object that we will be using when creating XML
                    List<TournamentTable> standings = null;

                    // Checking if this tournament is one that 
                    if (tournament.PublishTournamentTable)
                    {
                        Logger.Debug("Getting the Standings");

                        try
                        {
                            standings = this._facade.GetTournamentTableTeam(tournamentId, null);
                        }
                        catch (Exception exception)
                        {
                            Logger.Debug(exception.Message);
                            if (exception.Message == "No tournament standings found.")
                            {
                                Logger.InfoFormat("No standings found for tournament {0}", tournament.Name);
                            }
                        }
                    }
                    else
                    {
                        Logger.InfoFormat("The standings from tournament {0} cannot publish results!", tournament.Name);
                    }

                    Logger.Debug("Creating XML-structure!");

                    var ageCategoryTournament = this._facade.GetAgeCategoryTournament().Single(ageCategory => ageCategory.Id == tournament.TournamentAgeCategory);

                    // Creating the XMLDocument
                    // Creating the XMLDocument
                    var xmldocumentModel = new XmlDocumentModel
                    {
                        SportId = this.Model.SportId,
                        CreateXml = this.Model.CreateXml,
                        Customers = customers,

                    };
                    var xmlDocumentCreator = new SoccerXmlDocument(xmldocumentModel);
                    var doc = xmlDocumentCreator.CreateXmlStructure(filteredMatches, tournament, ageCategoryTournament,
                        standings, false, customers);

                    // Creating the filename
                    var fileNames = xmlDocumentCreator.CreateFileName(tournament.DistrictId, tournament.Id,
                        currentDate, this.Model.SportId);

                    if (doc == null)
                    {
                        continue;
                    }

                    // Now creating the XML-file
                    Logger.Debug("Creating XML-File");
                    xmlDocumentCreator.WriteXmlFile(doc, tournament.Name, tournament.Id, fileNames,
                        tournament.DistrictId);
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
        }
    }
}
