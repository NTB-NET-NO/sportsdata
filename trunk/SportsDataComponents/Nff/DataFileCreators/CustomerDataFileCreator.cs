﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nff.DataFileCreators.Abstracts;
using NTB.SportsData.Components.Nff.Facade;
using NTB.SportsData.Components.Nff.Models;
using NTB.SportsData.Components.PublicRepositories;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;

namespace NTB.SportsData.Components.Nff.DataFileCreators
{
    public class CustomerDataFileCreator : ADataFileCreator
    {
        private readonly ISoccerFacade _facade;

        public CustomerDataFileCreator()
        {
            this._facade = new SoccerFacade();
        }
        /// <summary>
        ///     Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(NffDatafileCreator));

        
        /// <summary>
        ///     The create customer data file.
        /// </summary>
        /// <param name="customerId">
        ///     The customer id.
        /// </param>
        /// <param name="remoteCustomerNumber">
        ///     The remote customer number.
        /// </param>
        /// <param name="xmlNodeList">
        ///     The xml node list.
        /// </param>
        /// <param name="startDateTime">
        ///     The start date time.
        /// </param>
        /// <param name="endDateTime">
        ///     The end date time.
        /// </param>
        public void CreateCustomerDataFile(List<Customer> customers, XmlNodeList xmlNodeList, DateTime startDateTime, DateTime endDateTime)
        {
            Logger.Debug("We are in CreateCustomerDataFile!");

            // Creating the customer Object
            // Creating the SportsDatabase object
            ICustomerDataMapper customerRepoistory = new CustomerRepository();
            Logger.Debug("We have gotten database object");

            foreach (var customer in customers)
            {
                var currentCustomer = customerRepoistory.GetCustomerById(customer.Id);
                customer.Id = currentCustomer.Id;
                customer.RemoteCustomerId = currentCustomer.RemoteCustomerId;
                customer.Name = currentCustomer.Name;
            }
            
            this.Model.RenderResult = true;

            ISoccerFacade facade = new SoccerFacade();
            try
            {
                var ageCategoryTournament = facade.GetAgeCategoryTournament();

                foreach (XmlNode tournamentNode in xmlNodeList)
                {
                    // Checking that we don't have an empty attribute
                    if (tournamentNode.Attributes == null)
                    {
                        continue;
                    }

                    var tournamentId = Convert.ToInt32(tournamentNode.Attributes["id"].Value);

                    // Getting the Tournament Object based on the TournamentId
                    try
                    {
                        // var tournament = this.TournamentServiceClient.GetTournament(tournamentId);
                        var tournament = facade.GetTournamentById(tournamentId);
                        if (tournament == null)
                        {
                            continue;
                        }

                        Logger.DebugFormat("Tournament: {0}.", tournament.Name);
                        
                        Logger.Debug("Done getting Tournament Object");

                        if (tournament.AgeCategory == null)
                        {
                            Logger.Debug("Tournament has no age category. We'll continue the loop");
                            continue;
                        }
                        var filteredAgeCategory =
                            (from act in ageCategoryTournament
                             where act.Id == tournament.TournamentAgeCategory
                             select act).Single();

                        Logger.Info("Checking for Creating Datafile for tournament: " + tournamentId);

                        // Now that we have some information about the tournament, we can do more
                        try
                        {
                            var matches = facade.GetMatchesByTournament(tournamentId, true, true, true, true);

                            for (var currentDate = DateTime.Parse(startDateTime.ToShortDateString());
                                endDateTime.CompareTo(currentDate) >= 0;
                                currentDate = currentDate.AddDays(1.0))
                            {
                                var filteredMatches =
                                    (from m in matches
                                     where (m.MatchDate != null) && m.MatchDate.Value.ToShortDateString() == currentDate.ToShortDateString()
                                     select m).ToList();

                                if (filteredMatches.Count == 0)
                                {
                                    Logger.InfoFormat("No matches found in tournament {0} on date: {1}", tournamentId, currentDate);
                                    continue;
                                }

                                // Creating the Standings object that we will be using when creating XML
                                List<TournamentTable> standings = null;

                                // Checking if this tournament is one that 
                                if (tournament.PublishTournamentTable)
                                {
                                    Logger.Debug("Getting the Standings");

                                    try
                                    {
                                        standings = facade.GetTournamentTableTeam(tournamentId, currentDate);
                                    }
                                    catch (Exception exception)
                                    {
                                        Logger.Debug(exception.Message);
                                        if (exception.Message == "No tournament standings found.")
                                        {
                                            Logger.InfoFormat("No standings found for tournament {0}", tournament.Name);
                                        }
                                    }
                                }
                                else
                                {
                                    Logger.InfoFormat("The standings from tournament {0} cannot publish results!", tournament.Name);
                                }

                                Logger.Debug("Creating XML-structure!");

                                // Creating the XMLDocument
                                var xmldocumentModel = new XmlDocumentModel
                                {
                                    SportId = this.Model.SportId,
                                    CreateXml = this.Model.CreateXml,
                                    Customers = customers,
                                    FileOutputFolder = this.Model.FileOutputFolder,
                                    RenderResult = this.Model.RenderResult

                                };
                                var xmlDocumentCreator = new SoccerXmlDocument(xmldocumentModel);
                                var doc = xmlDocumentCreator.CreateXmlStructure(filteredMatches, tournament,
                                    filteredAgeCategory, standings, false, customers);

                                // Creating the filename
                                var fileNames = xmlDocumentCreator.CreateFileName(tournament.DistrictId,
                                    tournament.Id, currentDate, this.Model.SportId);

                                if (doc != null)
                                {
                                    // Now creating the XML-file
                                    Logger.Debug("Creating XML-File");
                                    xmlDocumentCreator.WriteXmlFile(doc, tournament.Name,
                                        tournament.Id, fileNames, tournament.DistrictId);
                                }
                            }
                        }
                        catch (Exception exception)
                        {
                            Logger.Error(exception.Message);
                            Logger.Error(exception.StackTrace);

                            if (exception.InnerException != null)
                            {
                                Logger.Error(exception.InnerException.Message);
                                Logger.Error(exception.InnerException.StackTrace);
                            }
                        }
                    }
                    catch (Exception
                        exception)
                    {
                        Logger.Error(exception.Message);
                        Logger.Error(exception.StackTrace);
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
        }

        /// <summary>
        ///     The create customer data file.
        /// </summary>
        /// <param name="customerId">
        ///     The customer id.
        /// </param>
        /// <param name="remoteCustomerId">
        ///     The remote Customer Id.
        /// </param>
        /// <param name="tournamentId">
        ///     The tournament id.
        /// </param>
        /// <param name="selectedStartDate">
        ///     The start date.
        /// </param>
        /// <param name="selectedEndDate">
        ///     The end date.
        /// </param>
        public void CreateCustomerDataFile(int customerId, int remoteCustomerId, int tournamentId,
            DateTime selectedStartDate, DateTime selectedEndDate)
        {
            Logger.Debug("We are in CreateCustomerDataFile!");

            // Creating the customer Object
            var customers = new List<Customer>();

            Logger.Debug("We have gotten database object");
            ICustomerDataMapper repository = new CustomerRepository();
            var customer = repository.GetCustomerById(customerId);

            customers.Add(customer);

            this.Model.RenderResult = true;

            ISoccerFacade facade = new SoccerFacade();
            try
            {
                // Getting the Tournament Object based on the TournamentId
                var tournament = facade.GetTournamentById(tournamentId);
                if (tournament == null)
                {
                    return;
                }
                // this.TournamentServiceClient.GetTournament(tournamentId);
                var ageCategoryTournament = facade.GetAgeCategoryTournament();

                var filteredAgeCategory =
                    (from act in ageCategoryTournament
                     where act.Id == tournament.TournamentAgeCategory
                     select act).Single();

                Logger.DebugFormat("Tournament: {0}", tournament.Name);
                Logger.Debug("Done getting Tournament Object");

                // Now that we have some information about the tournament, we can do more
                //var matches = this.TournamentServiceClient.GetMatchesByTournament(tournamentId, true, true, true, true, null).ToList();
                var matches = facade.GetMatchesByTournament(tournamentId, true, true, true, true, null);

                Logger.InfoFormat("No matches found in tournament {0} on date: {1}", tournamentId, selectedStartDate);
                var filteredMatches = (from m in matches where m.MatchDate == selectedStartDate select m).ToList();

                if (filteredMatches.Count == 0)
                {
                    return;
                }

                // Creating the Standings object that we will be using when creating XML
                List<TournamentTable> standings = null;

                // Checking if this tournament is one that 
                if (tournament.PublishTournamentTable)
                {
                    Logger.Debug("Getting the Standings");

                    try
                    {
                        //standings = new List<TournamentTableTeam>(this.TournamentServiceClient.GetTournamentStanding(tournamentId, selectedStartDate));
                        standings = facade.GetTournamentTableTeam(tournamentId, selectedStartDate);
                    }
                    catch (Exception exception)
                    {
                        Logger.Debug(exception.Message);
                        if (exception.Message == "No tournament standings found.")
                        {
                            Logger.InfoFormat("No standings found for tournament {0}", tournament.Name);
                        }
                    }
                }
                else
                {
                    Logger.InfoFormat("The standings from tournament {0} cannot publish results!", tournament.Name);
                }

                Logger.Debug("Creating XML-structure!");

                // Creating the XMLDocument
                var xmldocumentModel = new XmlDocumentModel
                {
                    SportId = this.Model.SportId,
                    CreateXml = this.Model.CreateXml,
                    Customers = customers,

                };
                var xmlDocumentCreator = new SoccerXmlDocument(xmldocumentModel);
                var doc = xmlDocumentCreator.CreateXmlStructure(filteredMatches, tournament, filteredAgeCategory,
                    standings, false, customers);

                // Creating the filename
                var fileNames = xmlDocumentCreator.CreateFileName(tournament.DistrictId, tournament.Id,
                    selectedStartDate, this.Model.SportId);

                if (doc != null)
                {
                    // Now creating the XML-file
                    Logger.Debug("Creating XML-File");
                    xmlDocumentCreator.WriteXmlFile(doc, tournament.Name, tournament.Id, fileNames,
                        tournament.DistrictId);
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
        }


    }
}
