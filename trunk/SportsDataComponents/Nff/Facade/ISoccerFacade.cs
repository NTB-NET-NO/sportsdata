﻿using System;
using System.Collections.Generic;
using NTB.SportsData.Classes.Classes;

namespace NTB.SportsData.Components.Nff.Facade
{
    public interface ISoccerFacade
    {
        List<AgeCategory> GetAgeCategoryTournament();

        List<District> GetDistricts();

        List<Municipality> GetMunicipalitiesByDistrictId(int districtId);

        List<Season> GetSeasons();

        List<Match> GetMatchesByDistrict(int districtId);

        List<Match> GetMatchesByTournament(int tournamentId, bool includeSquad, bool includeReferees, bool includeResults, bool includeEvents, DateTime? tournamentDate = null);

        List<TournamentTable> GetTournamentTableTeam(int tournamentId, DateTime? selectedDateTime = null);

        Tournament GetTournamentById(int tournamentId);

        Match GetMatchById(int matchId, bool includeSquad, bool includeReferees, bool includeResults, bool includeEvents);

        List<int> GetListOfMatchesWithoutResults();

        List<Tournament> GetTournamentsByDistrictAndSeasonId(int districtId, int seasonId);

        List<Match> GetMatchesByDateInterval(DateTime dateStart, DateTime dateEnd, int districtId, bool includeSquad,
            bool includeReferees, bool includeResults, bool includeEvents);
    }
}
