﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nff.Mappers;
using NTB.SportsData.Components.Nff.Populators;
using NTB.SportsData.Components.NFFMetaService;
using Municipality = NTB.SportsData.Classes.Classes.Municipality;
using Season = NTB.SportsData.Classes.Classes.Season;

namespace NTB.SportsData.Components.Nff.Facade
{
    public class SoccerFacade : ISoccerFacade
    {

        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SoccerFacade));

        public List<AgeCategory> GetAgeCategoryTournament()
        {
            var populator = new AgeCategoryTournamentPopulator();
            var remoteAgeCategoryTournaments = populator.GetAgeCategoryTournament();

            var mapper = new AgeCategoryMapper();

            return remoteAgeCategoryTournaments.Select(row => mapper.Map(row, new AgeCategory())).ToList();
        }

        public List<District> GetDistricts()
        {
            var populator = new DistrictsPopulator();
            var remoteDistricts = populator.GetDistricts();

            var mapper = new DistrictMapper();
            return remoteDistricts.Select(row => mapper.Map(row, new District()))
                .ToList();
        }

        public List<Municipality> GetMunicipalitiesByDistrictId(int districtId)
        {
            var populator = new MunicipalityPopulator();
            var remoteMunicipalities = populator.GetMunicipalitiesByDistrictId(districtId);

            var mapper = new MunicipalityMapper();
            return remoteMunicipalities.Select(row => mapper.Map(row, new Municipality()))
                .ToList();
        }

        public List<Tournament> GetTournamentsByDistrictAndSeasonId(int districtId, int seasonId)
        {
            var populator = new TournamentPopulator();
            var response = populator.GetTournamentsByDistrictAndSeasonId(districtId, seasonId);
            var mapper = new TournamentMapper();
            return response.Select(row => mapper.Map(row, new Tournament()))
                .ToList();
        }

        public List<Season> GetSeasons()
        {
            var populator = new SeasonPopulator();
            var response = populator.GetSeasons();
            var mapper = new SeasonMapper();

            return response.Select(row => mapper.Map(row, new Season()))
                .ToList();
        }

        public List<Match> GetMatchesByDistrict(int districtId)
        {
            var populator = new MatchPopulator();
            var response = populator.GetMatchesByDistrict(districtId);
            var mapper = new MatchMapper();
            var matchResultMapper = new MatchResultMapper();
            var refereeMapper = new RefereeMapper();
            var playerMapper = new PlayerMapper();
            var matchIncidentMapper = new MatchIncidentMapper();
            var returnMatches = response.Select(row => mapper.Map(row, new Match()))
                .ToList();

            foreach (var returnMatch in returnMatches)
            {
                var currentMatch = returnMatch;
                foreach (var match in response.Where(x => x.MatchId == currentMatch.Id))
                {
                    currentMatch.MatchResultList = match.MatchResultList.Select(row => matchResultMapper.Map(row, new MatchResult()))
                        .ToList();
                    currentMatch.Referees = match.Referees.Select(row => refereeMapper.Map(row, new Referee()))
                        .ToList();
                    currentMatch.AwayTeamPlayers = match.AwayTeamPlayers.Select(row => playerMapper.Map(row, new Player()))
                        .ToList();
                    currentMatch.HomeTeamPlayers = match.HomeTeamPlayers.Select(row => playerMapper.Map(row, new Player()))
                        .ToList();
                    currentMatch.MatchIncidents = match.MatchEventList.Select(row => matchIncidentMapper.Map(row, new MatchIncident())).ToList();
                }
            }

            return returnMatches;
        }

        public List<Match> GetMatchesByTournament(int tournamentId, bool includeSquad, bool includeReferees, bool includeResults, bool includeEvents, DateTime? tournamentDate = null)
        {
            var populator = new MatchPopulator();
            var response = populator.GetMatchesByTournament(tournamentId, includeSquad, includeReferees, includeResults, includeEvents, tournamentDate);
            var mapper = new MatchMapper();

            var matchResultMapper = new MatchResultMapper();
            var refereeMapper = new RefereeMapper();
            var playerMapper = new PlayerMapper();
            var matchIncidentMapper = new MatchIncidentMapper();

            var returnMatches = response.Select(row => mapper.Map(row, new Match()))
                .ToList();


            foreach (var returnMatch in returnMatches)
            {
                var currentMatch = returnMatch;
                foreach (var match in response.Where(x => x.MatchId == currentMatch.Id))
                {
                    currentMatch.MatchResultList = match.MatchResultList.Select(row => matchResultMapper.Map(row, new MatchResult()))
                        .ToList();
                    currentMatch.Referees = match.Referees.Select(row => refereeMapper.Map(row, new Referee()))
                        .ToList();
                    currentMatch.AwayTeamPlayers = match.AwayTeamPlayers.Select(row => playerMapper.Map(row, new Player()))
                        .ToList();
                    currentMatch.HomeTeamPlayers = match.HomeTeamPlayers.Select(row => playerMapper.Map(row, new Player()))
                        .ToList();
                    currentMatch.MatchIncidents = match.MatchEventList.Select(row => matchIncidentMapper.Map(row, new MatchIncident())).ToList();
                }
            }

            return returnMatches;
        }

        public List<TournamentTable> GetTournamentTableTeam(int tournamentId, DateTime? selectedDateTime = null)
        {
            var populator = new TournamentPopulator();
            var response = populator.GetTournamentTableTeam(tournamentId, selectedDateTime);
            var mapper = new TournamentTableMapper();
            return response.Select(row => mapper.Map(row, new TournamentTable())).ToList();
        }

        public List<Match> GetMatchesByDateInterval(DateTime dateStart, DateTime dateEnd, int districtId,
            bool includeSquad, bool includeReferees, bool includeResults, bool includeEvents)
        {
            var populator = new MatchPopulator();
            var response = populator.GetMatchesByDateInterval(dateStart, dateEnd, districtId,
            includeSquad, includeReferees, includeResults, includeEvents);
            var mapper = new MatchMapper();

            var matchResultMapper = new MatchResultMapper();
            var refereeMapper = new RefereeMapper();
            var playerMapper = new PlayerMapper();
            var matchIncidentMapper = new MatchIncidentMapper();

            var returnMatches = response.Select(row => mapper.Map(row, new Match()))
                .ToList();


            foreach (var returnMatch in returnMatches)
            {
                var currentMatch = returnMatch;
                foreach (var match in response.Where(x => x.MatchId == currentMatch.Id))
                {
                    currentMatch.MatchResultList = match.MatchResultList.Select(row => matchResultMapper.Map(row, new MatchResult()))
                        .ToList();
                    currentMatch.Referees = match.Referees.Select(row => refereeMapper.Map(row, new Referee()))
                        .ToList();
                    currentMatch.AwayTeamPlayers = match.AwayTeamPlayers.Select(row => playerMapper.Map(row, new Player()))
                        .ToList();
                    currentMatch.HomeTeamPlayers = match.HomeTeamPlayers.Select(row => playerMapper.Map(row, new Player()))
                        .ToList();
                    currentMatch.MatchIncidents = match.MatchEventList.Select(row => matchIncidentMapper.Map(row, new MatchIncident())).ToList();
                }
            }

            return returnMatches;

        }

        public Match GetMatchById(int matchId, bool includeSquad, bool includeReferees, bool includeResults, bool includeEvents)
        {
            var populator = new MatchPopulator();
            var response = populator.GetMatchById(matchId, includeSquad, includeReferees, includeResults, includeEvents);
            var mapper = new MatchMapper();
            return mapper.Map(response, new Match());
        }

        public Tournament GetTournamentById(int tournamentId)
        {
            var populator = new TournamentPopulator();
            var response = populator.GetTournamentById(tournamentId);
            var mapper = new TournamentMapper();

            // If we are not to publish, then we won't publish
            if (response.PublishResult == false)
            {
                Logger.InfoFormat("Not allowed to publish result for {0} ({1}", response.TournamentName, response.TournamentName);
                return null;
            }


            var ageCategoryPopulator = new AgeCategoryTournamentPopulator();
            var ageCategories = ageCategoryPopulator.GetAgeCategoryTournament();
            var foundAgeCategories = ageCategories.Where(x => x.AgeCategoryId == response.TournamentAgeCategory);

            var returnValue = mapper.Map(response, new Tournament());
            var ageCategoryTournaments = foundAgeCategories as AgeCategoryTournament[] ?? foundAgeCategories.ToArray();
            var ageCategoryMapper = new AgeCategoryMapper();

            var mappedCategory = ageCategoryTournaments.Select(row => ageCategoryMapper.Map(row, new AgeCategory())).ToList();
            if (mappedCategory.Count() == 1)
            {
                returnValue.AgeCategory = mappedCategory.Single();
            }

            return returnValue;
        }

        public List<int> GetListOfMatchesWithoutResults()
        {
            return new List<int>();
        }
    }
}
