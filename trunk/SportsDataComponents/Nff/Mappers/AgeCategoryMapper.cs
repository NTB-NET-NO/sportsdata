﻿using Glue;

using NTB.SportsData.Components.Mappers;
using NTB.SportsData.Components.NFFMetaService;

namespace NTB.SportsData.Components.Nff.Mappers
{
    public class AgeCategoryMapper : BaseMapper<AgeCategoryTournament, Classes.Classes.AgeCategory>
    {
        protected override void SetUpMapper(Mapping<AgeCategoryTournament, Classes.Classes.AgeCategory> mapper)
        {
            mapper.Relate(x => x.AgeCategoryId, y => y.Id);
            mapper.Relate(x => x.AgeCategoryName, y => y.Name);
            mapper.Relate(x => x.MaxAge, y => y.ToAge, this.NullableIntToIntConverter());
            mapper.Relate(x => x.MinAge, y => y.FromAge, this.NullableIntToIntConverter());
            mapper.Relate(x => x.ShortName, y => y.ShortName);
        }
    }
}
