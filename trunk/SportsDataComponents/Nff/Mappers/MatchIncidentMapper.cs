﻿using Glue;
using NTB.SportsData.Components.Mappers;
using NTB.SportsData.Components.NFFTournamentService;

namespace NTB.SportsData.Components.Nff.Mappers
{
    public class MatchIncidentMapper : BaseMapper<MatchEvent, Classes.Classes.MatchIncident>
    {
        protected override void SetUpMapper(Mapping<MatchEvent, Classes.Classes.MatchIncident> mapper)
        {
            mapper.Relate(x => x.MatchEventId, y => y.MatchIncidentId);
            mapper.Relate(x => x.MatchId, y => y.MatchId);
            mapper.Relate(x => x.MatchEventType, y => y.IncidentType);
            mapper.Relate(x => x.MatchEventTypeId, y => y.IncidentTypeId);
            mapper.Relate(x => x.Minute, y => y.Time, this.NullableIntToIntConverter());
            mapper.Relate(x => x.PlayerId, y => y.PlayerId, this.NullableIntToIntConverter());
            mapper.Relate(x => x.TeamId, y => y.TeamId, this.NullableIntToIntConverter());
            mapper.Relate(x => x.PersonId, y => y.PersonId, this.NullableIntToIntConverter());
            mapper.Relate(x => x.ConnectedToEvent, y => y.ConnectedToEvent);
        }
    }
}
