﻿using Glue;
using NTB.SportsData.Components.Mappers;
using NTB.SportsData.Components.NFFTournamentService;

namespace NTB.SportsData.Components.Nff.Mappers
{
    public class TournamentTableMapper : BaseMapper<TournamentTableTeam, Classes.Classes.TournamentTable>
    {
        protected override void SetUpMapper(Mapping<TournamentTableTeam, Classes.Classes.TournamentTable> mapper)
        {
            mapper.Relate(x => x.TournamentId, y=>y.TournamentId);
            mapper.Relate(x => x.TournamentTeamId, y => y.TournamentTeamId);
            mapper.Relate(x => x.TeamId, y => y.TeamId);
            mapper.Relate(x => x.TeamName, y => y.TeamName);
            mapper.Relate(x => x.TeamNameInTournament, y => y.TeamNameInTournament);
            mapper.Relate(x => x.TablePosition, y => y.TablePosition);
            mapper.Relate(x => x.AwayGoalDifference, y => y.AwayGoalDifference);
            mapper.Relate(x => x.AwayGoalsAgainst, y => y.NoAwayAgainst, this.NullableIntToIntConverter());
            mapper.Relate(x => x.AwayGoalsScored, y => y.NoAwayGoals, this.NullableIntToIntConverter());
            mapper.Relate(x => x.AwayMatchesDraw, y => y.NoAwayDraws, this.NullableIntToIntConverter());
            mapper.Relate(x => x.AwayMatchesLost, y => y.NoAwayLosses, this.NullableIntToIntConverter());
            mapper.Relate(x => x.AwayMatchesWon, y => y.NoAwayWins, this.NullableIntToIntConverter());
            mapper.Relate(x => x.AwayMatchesPlayed, y => y.NoAwayMatches, this.NullableIntToIntConverter());
            mapper.Relate(x => x.AwayPoints, y => y.NoAwayPoints, this.NullableIntToIntConverter());
            mapper.Relate(x => x.HomeGoalDifference, y => y.HomeGoalDifference);
            mapper.Relate(x => x.HomeGoalsAgainst, y => y.NoHomeAgainst, this.NullableIntToIntConverter());
            mapper.Relate(x => x.HomeGoalsScored, y => y.NoHomeGoals, this.NullableIntToIntConverter());
            mapper.Relate(x => x.HomeMatchesDraw, y => y.NoHomeDraws, this.NullableIntToIntConverter());
            mapper.Relate(x => x.HomeMatchesLost, y => y.NoHomeLosses, this.NullableIntToIntConverter());
            mapper.Relate(x => x.HomeMatchesWon, y => y.NoHomeWins, this.NullableIntToIntConverter());
            mapper.Relate(x => x.HomeMatchesPlayed, y => y.NoHomeMatches, this.NullableIntToIntConverter());
            mapper.Relate(x => x.HomePoints, y => y.NoHomePoints, this.NullableIntToIntConverter());
            mapper.Relate(x => x.TotalGoalsAgainst, y => y.TotalGoalsAgainst);
            mapper.Relate(x => x.LastChangedDate, y => y.LastChangedDate);
            mapper.Relate(x => x.TotalGoalDifference, y => y.TotalGoalDifference);
            mapper.Relate(x => x.TotalGoalsScored, y => y.NoGoals);
            mapper.Relate(x => x.TotalMatchesDraw, y => y.NoDraws);
            mapper.Relate(x => x.TotalMatchesLost, y => y.NoLosses);
            mapper.Relate(x => x.TotalMatchesWon, y => y.NoWins);
            mapper.Relate(x => x.TotalPoints, y => y.Points);
        }
    }
}
