﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.Nff.Mappers
{
    using Glue;

    using NTB.SportsData.Components.Mappers;
    using NTB.SportsData.Components.NFFTournamentService;

    /// <summary>
    /// The player mapper.
    /// </summary>
    internal class PlayerMapper : BaseMapper<Player, Classes.Classes.Player>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<Player, Classes.Classes.Player> mapper)
        {
            mapper.Relate(x => x.Debutant, y => y.Debutant, this.BoolToStringConverter());
            mapper.Relate(x => x.FirstName, y => y.FirstName);
            mapper.Relate(x => x.PlayedMatch, y => y.Played, this.BoolToStringConverter());
            mapper.Relate(x => x.PlayerId, y => y.Id, this.NullableIntToIntConverter());
            mapper.Relate(x => x.PlayerShirtNumber, y => y.PlayerShirtNumber, this.ConvertNullableIntToStringConverter());
            mapper.Relate(x => x.Position, y => y.Position);
            mapper.Relate(x => x.PositionId, y => y.PositionId, this.NullableIntToIntConverter());
            mapper.Relate(x => x.Substitute, y => y.Substitute, this.BoolToStringConverter());
            mapper.Relate(x => x.SubstitutedWithPlayerId, y => y.SubstitutedWithPlayerId, this.NullableIntToIntConverter());
            mapper.Relate(x => x.SurName, y => y.LastName);
            mapper.Relate(x => x.TeamCaptain, y => y.TeamCaptain, this.BoolToStringConverter());
        }
    }
}