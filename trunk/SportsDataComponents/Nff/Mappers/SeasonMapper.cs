﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SeasonMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.Nff.Mappers
{
    using Glue;
    using SportsData.Components.Mappers;
    using NFFMetaService;

    /// <summary>
    /// The season mapper.
    /// </summary>
    public class SeasonMapper : BaseMapper<Season, Classes.Classes.Season>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<Season, Classes.Classes.Season> mapper)
        {
            mapper.Relate(x => x.Ongoing, y => y.Active);
            mapper.Relate(x => x.SeasonId, y => y.Id);
            mapper.Relate(x => x.SeasonName, y => y.Name);
            mapper.Relate(x => x.SeasonStartDate, y => y.StartDate);
            mapper.Relate(x => x.SeasonEndDate, y => y.EndDate);
        }
    }
}