﻿using Glue;
using NTB.SportsData.Components.Mappers;
using NTB.SportsData.Components.NFFTournamentService;

namespace NTB.SportsData.Components.Nff.Mappers
{
    public class TournamentMapper : BaseMapper<Tournament, Classes.Classes.Tournament>
    {
        protected override void SetUpMapper(Mapping<Tournament, Classes.Classes.Tournament> mapper)
        {
            mapper.Relate(x => x.AgeCategoryId, y => y.AgeCategoryId);
            mapper.Relate(x => x.DistrictId, y => y.DistrictId);
            mapper.Relate(x => x.Division, y => y.Division);
            mapper.Relate(x => x.GenderId, y => y.GenderId);
            mapper.Relate(x => x.Group, y => y.Group);
            mapper.Relate(x => x.HalfTimeBreakInMin, y => y.HalfTimeBreakInMin);
            mapper.Relate(x => x.LastChangedDate, y => y.LastChangedDate);
            mapper.Relate(x => x.MatchPeriodDurationInMin, y => y.MatchPeriodDurationInMin);
            mapper.Relate(x => x.NationalTeamTournament, y => y.NationalTeamTournament);
            mapper.Relate(x => x.NumberOfMatchPeriods, y => y.NumberOfMatchPeriods);
            mapper.Relate(x => x.NumberOfPromotedTeams, y => y.NumberOfPromotedTeams);
            mapper.Relate(x => x.NumberOfRelegatedTeams, y => y.NumberOfRelegatedTeams);
            mapper.Relate(x => x.NumberOfTeams, y => y.NumberOfTeams);
            mapper.Relate(x => x.NumberOfTeamsQualifiedForPromotion, y => y.NumberOfTeamsQualifiedForPromotion);
            mapper.Relate(x => x.NumberOfTeamsQualifiedForRelegation, y => y.NumberOfTeamsQualifiedForRelegation);
            mapper.Relate(x => x.PublishResult, y => y.PublishResult);
            mapper.Relate(x => x.PublishTournamentTable, y => y.PublishTournamentTable);
            mapper.Relate(x => x.PushChanges, y => y.PushChanges);
            mapper.Relate(x => x.ResultReportingMaxNumberOfGoals, y => y.ResultReportingMaxNumberOfGoals);
            mapper.Relate(x => x.ResultReportingAllowed, y => y.ResultReportingAllowed);

            mapper.Relate(x => x.ResultReportingDaysOpenForRegistrationBeforeMatchStart, y => y.ResultReportingDaysOpenForRegistrationBeforeMatchStart);
            mapper.Relate(x => x.SeasonId, y => y.SeasonId);
            mapper.Relate(x => x.StartDateFirstTournamentRound, y => y.StartDateFirstTournamentRound);
            mapper.Relate(x => x.TournamentId, y => y.Id);
            mapper.Relate(x => x.TournamentName, y => y.Name);
            mapper.Relate(x => x.TournamentStatusId, y => y.TournamentStatusId);
            mapper.Relate(x => x.TournamentTypeId, y => y.TournamentTypeId);
            mapper.Relate(x => x.TournamentAgeCategory, y => y.TournamentAgeCategory);
            mapper.Relate(x => x.TournamentTableComment, y => y.TournamentTableComment);
            mapper.Relate(x => x.TournamentNumber, y => y.TournamentNumber);
        }
    }
}

