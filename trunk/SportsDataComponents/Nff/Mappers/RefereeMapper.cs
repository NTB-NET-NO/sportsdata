﻿using Glue;
using NTB.SportsData.Components.Mappers;
using NTB.SportsData.Components.NFFTournamentService;

namespace NTB.SportsData.Components.Nff.Mappers
{
    public class RefereeMapper : BaseMapper<Referee, Classes.Classes.Referee>
    {
        protected override void SetUpMapper(Mapping<Referee, Classes.Classes.Referee> mapper)
        {
            mapper.Relate(x => x.PersonId, y => y.Id, this.NullableIntToIntConverter());
            mapper.Relate(x => x.RefereeClubId, y => y.ClubId, this.NullableIntToIntConverter());
            mapper.Relate(x => x.RefereeClub, y => y.ClubName);
            mapper.Relate(x => x.FirstName, y => y.FirstName);
            mapper.Relate(x => x.SurName, y => y.LastName);
            mapper.Relate(x => x.RefereeType, y => y.RefereeType);
            mapper.Relate(x => x.RefereeTypeId, y => y.RefereeTypeId, this.NullableIntToIntConverter());
            mapper.Relate(x => x.RefereeName, y => y.RefereeName);
        }
    }
}
