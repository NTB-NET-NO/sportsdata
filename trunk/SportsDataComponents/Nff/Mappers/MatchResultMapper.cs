﻿using Glue;
using NTB.SportsData.Components.Mappers;
using NTB.SportsData.Components.NFFTournamentService;

namespace NTB.SportsData.Components.Nff.Mappers
{
    public class MatchResultMapper : BaseMapper<MatchResult, Classes.Classes.MatchResult>
    {
        protected override void SetUpMapper(Mapping<MatchResult, Classes.Classes.MatchResult> mapper)
        {
            mapper.Relate(x => x.AwayTeamGoals, y => y.AwayTeamGoals);
            mapper.Relate(x => x.Comment, y => y.Comment);
            mapper.Relate(x => x.HomeTeamGoals, y => y.HomeTeamGoals);
            mapper.Relate(x => x.LastChangeDate, y => y.LastChangeDate);
            mapper.Relate(x => x.MatchId, y => y.MatchId);
            mapper.Relate(x => x.MatchResultSetInFiks, y => y.MatchResultSetInFiks);
            mapper.Relate(x => x.ResultId, y => y.ResultId);
            mapper.Relate(x => x.ResultTypeId, y => y.ResultTypeId);
            mapper.Relate(x => x.ResultTypeName, y => y.ResultTypeName);
        }
    }
}
