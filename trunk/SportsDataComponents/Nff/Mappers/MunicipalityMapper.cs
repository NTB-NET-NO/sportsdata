﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MunicipalityMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------



namespace NTB.SportsData.Components.Nff.Mappers
{
    using Glue;

    using SportsData.Components.Mappers;
    using NFFMetaService;

    /// <summary>
    /// The season mapper.
    /// </summary>
    public class MunicipalityMapper : BaseMapper<Municipality, Classes.Classes.Municipality>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<Municipality, Classes.Classes.Municipality> mapper)
        {
            mapper.Relate(x => x.Id, y => y.Id);
            mapper.Relate(x => x.Name, y => y.Name);

        }
    }
}