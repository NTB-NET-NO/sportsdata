﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Glue;

    using NTB.SportsData.Components.Mappers;
    using NTB.SportsData.Components.NFFMetaService;
using NTB.SportsData.Components.NFFOrganizationService;

namespace NTB.SportsData.Components.Nff.Mappers
{
    /// <summary>
    /// The season mapper.
    /// </summary>
    public class DistrictMapper : BaseMapper<District, Classes.Classes.District>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<District, Classes.Classes.District> mapper)
        {
            mapper.Relate(x => x.DistrictId, y => y.Id);
            mapper.Relate(x => x.DistrictName, y => y.Name);
            mapper.Relate(x => x.Address, y => y.Address);
            mapper.Relate(x => x.City, y => y.City);
            mapper.Relate(x => x.CoAddress, y => y.CoAddress);
            mapper.Relate(x => x.Email, y => y.Email);
            mapper.Relate(x => x.Fax, y => y.Fax);
            mapper.Relate(x => x.Homepage, y => y.Homepage);
            mapper.Relate(x => x.Phone, y => y.Phone);
            mapper.Relate(x => x.PostalCode, y => y.PostalCode);
        }
    }
}
