﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using NTB.SportsData.Components.Mappers;

namespace NTB.SportsData.Components.Nff.Mappers
{
    using Glue;
    using NFFTournamentService;

    /// <summary>
    /// The match mapper.
    /// </summary>
    public class MatchMapper : BaseMapper<Match, Classes.Classes.Match>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<Match, Classes.Classes.Match> mapper)
        {
            mapper.Relate(x => x.MatchId, y => y.Id);
            mapper.Relate(x => x.MatchStartDate, y => y.MatchDate);
            mapper.Relate(x => x.MatchNumber, y => y.MatchNo);
            mapper.Relate(x => x.AwayTeamId, y => y.AwayTeamId);
            mapper.Relate(x => x.AwayTeamName, y => y.AwayTeamName);
            mapper.Relate(x => x.AwayTeamGoals, y => y.AwayTeamGoals);
            mapper.Relate(x => x.HomeTeamId, y => y.HomeTeamId);
            mapper.Relate(x => x.HomeTeamName, y => y.HomeTeamName);
            mapper.Relate(x => x.HomeTeamGoals, y => y.HomeTeamGoals);
            mapper.Relate(x => x.TournamentId, y => y.TournamentId);
            mapper.Relate(x => x.TournamentAgeCategoryId, y => y.TournamentAgeCategoryId);
            // mapper.Relate(x => x.TournamentRoundNumber, y => y.RoundId);
            mapper.Relate(x => x.StadiumId, y => y.VenueUnitId);
            mapper.Relate(x => x.StadiumName, y => y.VenueName);
            mapper.Relate(x => x.Spectators, y => y.Spectators, this.NullableIntToIntConverter());
            mapper.Relate(x => x.TournamentRoundNumber, y => y.TournamentRoundNumber);
            // mapper.Relate(x => x.MatchResultList, y => y.MatchResultList);
            // mapper.Relate(x => x.Referees, y => y.Referees);
        }
    }
}