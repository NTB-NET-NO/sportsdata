﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchEventMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.Nff.Mappers
{
    using Glue;

    using NTB.SportsData.Components.Mappers;
    using NTB.SportsData.Components.NFFTournamentService;

    /// <summary>
    /// The match event mapper.
    /// </summary>
    internal class MatchEventMapper : BaseMapper<MatchEvent, Classes.Classes.MatchEvent>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<MatchEvent, Classes.Classes.MatchEvent> mapper)
        {
            mapper.Relate(x => x.AwayGoals, y => y.AwayGoals);
            mapper.Relate(x => x.Comment, y => y.Comment);
            mapper.Relate(x => x.ConnectedToEvent, y => y.ConnectedToEvent);
            mapper.Relate(x => x.HomeGoals, y => y.HomeGoals);
            mapper.Relate(x => x.MatchEventId, y => y.MatchEventId);
            mapper.Relate(x => x.MatchEventType, y => y.MatchEventType);
            mapper.Relate(x => x.MatchEventTypeId, y => y.MatchEventTypeId);
            mapper.Relate(x => x.MatchId, y => y.MatchId);
            mapper.Relate(x => x.Minute, y => y.Minute);
            mapper.Relate(x => x.PlayerId, y => y.PlayerId);
            mapper.Relate(x => x.PlayerName, y => y.PlayerName);
            mapper.Relate(x => x.SecondYellowCard, y => y.SecondYellowCard);
            mapper.Relate(x => x.TeamId, y => y.TeamId);
            mapper.Relate(x => x.TeamName, y => y.TeamName);
        }
    }
}