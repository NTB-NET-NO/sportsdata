// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NffSportsDataDatabase.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using NTB.SportsData.Components.Nff.Mappers;
using NTB.SportsData.Components.PublicRepositories;

namespace NTB.SportsData.Components.Nff
{
    // Adding log4net support
    // NFF service references
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;

    using log4net;
    using log4net.Config;

    using NTB.SportsData.Classes.Classes;
    using NTB.SportsData.Components.NFFMetaService;
    using NTB.SportsData.Components.NFFOrganizationService;
    using NTB.SportsData.Components.NFFTournamentService;
    using NTB.SportsData.Utilities;

    using Match = NTB.SportsData.Components.NFFTournamentService.Match;
    using Season = NTB.SportsData.Components.NFFMetaService.Season;
    using Tournament = NTB.SportsData.Components.NFFTournamentService.Tournament;

    /// <summary>
    ///     The nff sports data database.
    /// </summary>
    public class NffSportsDataDatabase
    {
        /// <summary>
        ///     The final.
        /// </summary>
        private const bool Final = false;

        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(NffSportsDataDatabase));

        /// <summary>
        ///     The _matches.
        /// </summary>
        private List<Match> soccerMatches = new List<Match>();

        /// <summary>
        ///     The tournament service client.
        /// </summary>
        public TournamentServiceClient TournamentServiceClient = new TournamentServiceClient();

        /// <summary>
        ///     Initializes a new instance of the <see cref="NffSportsDataDatabase" /> class.
        /// </summary>
        public NffSportsDataDatabase()
        {
            try
            {
                // Set up logger
                XmlConfigurator.Configure();

                if (!LogManager.GetRepository()
                         .Configured)
                {
                    BasicConfigurator.Configure();
                }

                Logger.Debug("Creating connection to TourmanetServiceClient");
                if (this.TournamentServiceClient.ClientCredentials != null)
                {
                    this.TournamentServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServiceUsername"];
                    this.TournamentServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicePassword"];
                }

                Logger.Debug("Done creating connection to TourmanetServiceClient");
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
        }

        /// <summary>
        ///     Gets a value indicating whether final result.
        /// </summary>
        public bool FinalResult
        {
            get
            {
                return Final;
            }
        }

        /// <summary>
        ///     This method initiates the object. We are here checking if District-table and Tournament-Table are populated
        /// </summary>
        public void Init()
        {
        }

        /// <summary>
        ///     The get customer by id.
        /// </summary>
        /// <param name="customerId">
        ///     The customer id.
        /// </param>
        /// <returns>
        ///     The <see cref="Customer" />.
        /// </returns>
        public Customer GetCustomerById(int customerId)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        sqlConnection.Open();
                    }

                    var sqlCommand = new SqlCommand("Service_GetCustomerById", sqlConnection)
                                         {
                                             CommandType = CommandType.StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", customerId));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    var customer = new Customer();
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            if (sqlDataReader["CustomerId"] != DBNull.Value)
                            {
                                customer.Id = Convert.ToInt32(sqlDataReader["CustomerId"].ToString());
                            }

                            if (sqlDataReader["CustomerName"] != DBNull.Value)
                            {
                                customer.Name = sqlDataReader["CustomerName"].ToString();
                            }

                            if (sqlDataReader["RemoteCustomerId"] != DBNull.Value)
                            {
                                customer.RemoteCustomerId = Convert.ToInt32(sqlDataReader["RemoteCustomerId"].ToString());
                            }
                        }
                    }

                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }

                    return customer;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return null;
            }
        }

        /// <summary>
        ///     The update tournament.
        /// </summary>
        /// <param name="tournamentId">
        ///     The tournament id.
        /// </param>
        /// <param name="sportId">
        ///     The sport Id.
        /// </param>
        public void UpdateTournament(int tournamentId, int sportId)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        sqlConnection.Open();
                    }

                    Logger.Debug("Getting more tournament information based on tournamentid: " + tournamentId);
                    var tournament = this.TournamentServiceClient.GetTournament(tournamentId);

                    var sqlCommand = new SqlCommand("Service_UpdateTournament", sqlConnection)
                                         {
                                             CommandType = CommandType.StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", tournament.TournamentId));

                    sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", tournament.DistrictId));

                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", tournament.SeasonId));

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId)); // hardcoded to soccer

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentName", tournament.TournamentName));

                    sqlCommand.Parameters.Add(new SqlParameter("@Push", tournament.PushChanges));

                    sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryId", tournament.AgeCategoryId));

                    sqlCommand.Parameters.Add(new SqlParameter("@GenderId", tournament.GenderId));

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentTypeId", tournament.TournamentTypeId));

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentNumber", tournament.TournamentNumber));

                    sqlCommand.ExecuteNonQuery();

                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
        }

        /// <summary>
        ///     The get tournament by match id.
        /// </summary>
        /// <param name="matchId">
        ///     The match id.
        /// </param>
        /// <returns>
        ///     The <see cref="int" />.
        /// </returns>
        public int GetTournamentByMatchId(int matchId)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        sqlConnection.Open();
                    }

                    var tournamentId = 0;
                    var sqlCommand = new SqlCommand("Service_GetTournamentIdByMatchId", sqlConnection)
                                         {
                                             CommandType = CommandType.StoredProcedure
                                         };

                    sqlCommand.Parameters.Add(new SqlParameter("@MatchId", matchId));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            if (sqlDataReader["TournamentId"] != DBNull.Value)
                            {
                                tournamentId = Convert.ToInt32(sqlDataReader["TournamentId"].ToString());
                            }
                        }
                    }

                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }

                    return tournamentId;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
                return 0;
            }
        }

        /// <summary>
        ///     The check seasons.
        /// </summary>
        /// <param name="seasons">
        ///     The seasons.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        public bool CheckSeasons(List<Season> seasons)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        sqlConnection.Open();
                    }

                    // string SQLQuery = "SELECT SeasonID FROM Seasons";
                    var sqlCommand = new SqlCommand("Service_GetSeasons", sqlConnection)
                                         {
                                             CommandType = CommandType.StoredProcedure
                                         };

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (sqlDataReader.HasRows)
                    {
                        sqlDataReader.Close();
                        sqlCommand.Dispose();

                        foreach (var season in seasons)
                        {
                            sqlCommand = new SqlCommand("Service_GetSeasons", sqlConnection)
                                             {
                                                 CommandType = CommandType.StoredProcedure
                                             };

                            sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", season.SeasonId));

                            var dataReader = sqlCommand.ExecuteReader();

                            if (!dataReader.HasRows)
                            {
                                Logger.Debug("Inserting: SeasonId: " + season.SeasonId + ", Seasonname: " + season.SeasonName + ", SeasonActive: " + season.Ongoing + " SeasonStartDate: " + season.SeasonStartDate + ", SeasonEndDate" + season.SeasonEndDate);

                                if (season.SeasonId > 0)
                                {
                                    try
                                    {
                                        var connectionString = ConfigurationManager.AppSettings["ConnectionString"];
                                        var insertConnection = new SqlConnection(connectionString);

                                        // Open the connection to the database
                                        insertConnection.Open();

                                        var commandInsert = new SqlCommand("Service_InsertSeasons", insertConnection)
                                                                {
                                                                    CommandType = CommandType.StoredProcedure
                                                                };

                                        commandInsert.Parameters.Add(new SqlParameter("@SeasonName", season.SeasonName));

                                        commandInsert.Parameters.Add(new SqlParameter("@SeasonId", season.SeasonId));

                                        commandInsert.Parameters.Add(new SqlParameter("@SeasonActive", season.Ongoing));

                                        commandInsert.Parameters.Add(new SqlParameter("@SeasonStartDate", season.SeasonStartDate));

                                        commandInsert.Parameters.Add(new SqlParameter("@SeasonEndDate", season.SeasonEndDate));

                                        commandInsert.Parameters.Add(new SqlParameter("@SeasonDistributed", DBNull.Value));

                                        // We must populate the database
                                        commandInsert.ExecuteNonQuery();

                                        insertConnection.Close();
                                    }
                                    catch (SqlException exception)
                                    {
                                        Logger.Error(exception);
                                    }
                                    catch (Exception exception)
                                    {
                                        Logger.Error(exception);
                                    }
                                }
                            }

                            sqlCommand.Dispose();
                            dataReader.Close();
                        }
                    }
                    else
                    {
                        var connectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        var insertConnection = new SqlConnection(connectionString);

                        // Open the connection to the database
                        insertConnection.Open();

                        foreach (var season in seasons)
                        {
                            var commandInsert = new SqlCommand("Service_InsertSeasons", insertConnection);

                            commandInsert.Parameters.Add(new SqlParameter("@SeasonId", season.SeasonId));

                            commandInsert.Parameters.Add(new SqlParameter("@SeasonName", season.SeasonName));

                            commandInsert.Parameters.Add(new SqlParameter("@SeasonActive", season.Ongoing));

                            commandInsert.Parameters.Add(new SqlParameter("@SeasonStartDate", season.SeasonStartDate));

                            commandInsert.Parameters.Add(new SqlParameter("@SeasonEndDate", season.SeasonEndDate));

                            commandInsert.Parameters.Add(new SqlParameter("@SeasonDistributed", 0));

                            // We must populate the database
                            commandInsert.ExecuteNonQuery();
                        }

                        insertConnection.Close();
                    }

                    // if it has rows, we shall loop through and check if there are any new districts 
                    // @todo: Check if district exists or not
                    sqlDataReader.Close();
                    sqlCommand.Dispose();

                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }

                    return true;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
                return false;
            }
        }

        

        

        /// <summary>
        ///     The check tournament by id.
        /// </summary>
        /// <param name="tournamentId">
        ///     The tournament id.
        /// </param>
        /// <param name="sportId">
        ///     The sport Id.
        /// </param>
        public void CheckTournamentById(int tournamentId, int sportId)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        sqlConnection.Open();
                    }

                    Logger.Info("Checking if Tournament Database Table is populated with correct values");

                    var commander = new SqlCommand("Service_GetTournamentByTournamentId", sqlConnection)
                                        {
                                            CommandType = CommandType.StoredProcedure
                                        };

                    commander.Parameters.Add(new SqlParameter("@TournamentId", tournamentId));

                    var reader = commander.ExecuteReader();

                    if (!reader.HasRows)
                    {
                        Logger.Info("The query returned no rows. we are adding Tournament Information!");
                        var tournament = this.TournamentServiceClient.GetTournament(tournamentId);
                        this.AddTournamentData(tournament, sportId);
                    }
                    else
                    {
                        Logger.Info("The query returned a row. We are updating tournament information!");
                        var tournament = this.TournamentServiceClient.GetTournament(tournamentId);
                        this.UpdateTournamentData(tournament, sportId);
                    }

                    reader.Close();
                    commander.Dispose();

                    sqlConnection.Close();
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
        }

        /// <summary>
        ///     The check tournament table.
        /// </summary>
        /// <param name="tournaments">
        ///     The tournaments.
        /// </param>
        /// <param name="sportId">
        ///     The sport Id.
        /// </param>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        public bool CheckTournamentTable(Tournament[] tournaments, int sportId)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    Logger.Info("Checking if Tournament Database Table is populated with correct values");
                    foreach (var tournament in tournaments)
                    {
                        Logger.Info("Checking Tournaments database table for Tournament: " + tournament.TournamentName);

                        var sqlCommand = new SqlCommand("Service_GetTournamentByTournamentId", sqlConnection)
                                             {
                                                 CommandType = CommandType.StoredProcedure
                                             };
                        sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", tournament.TournamentId));

                        var reader = sqlCommand.ExecuteReader();

                        if (!reader.HasRows)
                        {
                            Logger.Info("The query returned no rows. We shall add tournament!");

                            this.AddTournamentData(tournament, sportId);
                        }
                        else
                        {
                            Logger.Info("The query returned a row. We shall update tournament!");
                            this.UpdateTournamentData(tournament, sportId);
                        }

                        reader.Close();
                        sqlCommand.Dispose();
                    }

                    sqlConnection.Close();
                }

                return true;
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return false;
            }
        }

        /// <summary>
        ///     The check clubs.
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        public bool CheckClubs()
        {
            return false;
        }

        /// <summary>
        ///     The add tournament data.
        /// </summary>
        /// <param name="tournament">
        ///     The tournament.
        /// </param>
        /// <param name="sportId">
        ///     The sport Id.
        /// </param>
        public void AddTournamentData(Tournament tournament, int sportId)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
                {
                    // Open the connection to the database
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        sqlConnection.Open();
                    }

                    try
                    {
                        // Creating the Sql-Command using the Stored Procedure: Service_InsertTournament
                        var sqlCommand = new SqlCommand("Service_InsertTournament", sqlConnection)
                                             {
                                                 CommandType = CommandType.StoredProcedure
                                             };

                        // Telling the system that the Query is a StoredProcedure
                        var pushChanges = 0; // false
                        if (tournament.PushChanges)
                        {
                            pushChanges = 1;
                        }

                        sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", tournament.TournamentId));

                        sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", tournament.DistrictId));

                        sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", tournament.SeasonId));

                        sqlCommand.Parameters.Add(new SqlParameter("@TournamentName", tournament.TournamentName));

                        sqlCommand.Parameters.Add(new SqlParameter("@Push", pushChanges));

                        sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryId", tournament.TournamentAgeCategory));

                        sqlCommand.Parameters.Add(new SqlParameter("@GenderId", tournament.GenderId));

                        sqlCommand.Parameters.Add(new SqlParameter("@TournamentTypeId", tournament.TournamentTypeId));

                        sqlCommand.Parameters.Add(new SqlParameter("@TournamentNumber", tournament.TournamentNumber));

                        // Done: Need to set this in the Config job
                        sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                        // This is for football/soccer and is hard coded, not good...

                        // We must populate the database
                        sqlCommand.ExecuteNonQuery();

                        sqlConnection.Close();
                    }
                    catch (Exception e)
                    {
                        Logger.Error(e.Message);
                        Logger.Error(e.StackTrace);
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
        }

        /// <summary>
        ///     The update tournament data.
        /// </summary>
        /// <param name="tournament">
        ///     The tournament.
        /// </param>
        /// <param name="sportId">
        ///     The sport Id.
        /// </param>
        public void UpdateTournamentData(Tournament tournament, int sportId)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
                {
                    // Open the connection to the database
                    sqlConnection.Open();

                    try
                    {
                        // Creating the Sql-Command using the Stored Procedure: Service_InsertTournament
                        var sqlCommand = new SqlCommand("Service_UpdateTournament", sqlConnection)
                                             {
                                                 CommandType = CommandType.StoredProcedure
                                             };

                        // Telling the system that the Query is a StoredProcedure
                        var pushChanges = 0; // false
                        if (tournament.PushChanges)
                        {
                            pushChanges = 1;
                        }

                        sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", tournament.TournamentId));

                        sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", tournament.DistrictId));

                        sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", tournament.SeasonId));

                        sqlCommand.Parameters.Add(new SqlParameter("@TournamentName", tournament.TournamentName));

                        sqlCommand.Parameters.Add(new SqlParameter("@Push", pushChanges));

                        sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryId", tournament.TournamentAgeCategory));

                        sqlCommand.Parameters.Add(new SqlParameter("@GenderId", tournament.GenderId));

                        sqlCommand.Parameters.Add(new SqlParameter("@TournamentTypeId", tournament.TournamentTypeId));

                        sqlCommand.Parameters.Add(new SqlParameter("@TournamentNumber", tournament.TournamentNumber));

                        sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                        // This is for football/soccer and is hard coded, not good...

                        // We must populate the database
                        sqlCommand.ExecuteNonQuery();

                        sqlConnection.Close();
                    }
                    catch (Exception e)
                    {
                        Logger.Error(e.Message);
                        Logger.Error(e.StackTrace);
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
        }

        #region Daily

        /// <summary>
        ///     The delete documents.
        /// </summary>
        /// <returns>
        ///     The <see cref="int" />.
        /// </returns>
        public int DeleteDocuments()
        {
            try
            {
                using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        sqlConnection.Open();
                    }
                    var sqlCommand = new SqlCommand("DELETE FROM Document", sqlConnection)
                                         {
                                             CommandType = CommandType.StoredProcedure
                                         };

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    return sqlDataReader.RecordsAffected;
                }
            }
            catch (SqlException sqlexception)
            {
                Logger.Error(sqlexception);
                return 0;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                return 0;
            }
        }

        #endregion

        /// <summary>
        ///     The get one result file.
        /// </summary>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        public string GetOneResultFile()
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                    Logger.Debug("Start connecting!");

                    sqlConnection.Open();

                    Logger.Debug("End connecting!");

                    // Now performing the Query
                    var sqlCommand = new SqlCommand
                                         {
                                             Connection = sqlConnection,
                                             CommandText = "SELECT TOP(1) DocumentData FROM Document"
                                         };

                    var sqlDataReader = sqlCommand.ExecuteReader();
                    var documentData = string.Empty;
                    if (sqlDataReader.HasRows)
                    {
                        // sqlreader["HomeTeam"].ToString();
                        try
                        {
                            while (sqlDataReader.Read())
                            {
                                documentData = sqlDataReader["DocumentData"].ToString();
                            }
                        }
                        catch (SqlException sqlexception)
                        {
                            Logger.Error(sqlexception);
                        }
                        catch (Exception exception)
                        {
                            Logger.Error(exception);
                        }
                    }

                    sqlConnection.Close();

                    return documentData;
                }

                return string.Empty;
            }
        }

        /// <summary>
        ///     The get list of customers by tournament number.
        /// </summary>
        /// <param name="tournamentNo">
        ///     The tournament no.
        /// </param>
        /// <param name="sportId">
        ///     The sport id.
        /// </param>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Customer> GetListOfCustomersByTournamentNumber(int tournamentNo, int sportId)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        sqlConnection.Open();
                    }

                    var sqlCommand = new SqlCommand("Service_GetCustomersFromTournamentNumberAndSportId", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@MatchId", tournamentNo));

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    /**
                     * What we are to do: 
                     * First we get the TournamentId and JobId. We then use this to find
                     * the customers that shall have these data.
                     * We are to return a list of customernames
                     * 
                     */
                    var getListOfCustomers = new List<Customer>();
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            var customer = new Customer();
                            if (sqlDataReader["CustomerName"] != DBNull.Value)
                            {
                                customer.Name = sqlDataReader["CustomerName"].ToString();
                            }

                            if (sqlDataReader["CustomerId"] != DBNull.Value)
                            {
                                customer.Id = Convert.ToInt32(sqlDataReader["CustomerId"].ToString());
                            }

                            if (sqlDataReader["RemoteCustomerId"] != DBNull.Value)
                            {
                                customer.RemoteCustomerId = Convert.ToInt32(sqlDataReader["RemoteCustomerId"].ToString());
                            }

                            getListOfCustomers.Add(customer);
                        }

                        // Filter the outcome of the database
                        return getListOfCustomers.Distinct()
                            .ToList();
                    }

                    // logger.Debug("No customers needs these data");
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["livetesting"]))
                    {
                        var customer = new Customer
                                           {
                                               Name = "NTB",
                                               Id = 0,
                                               RemoteCustomerId = 44967
                                           };
                        getListOfCustomers.Add(customer);
                        return getListOfCustomers;
                    }

                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                    {
                        var customer = new Customer
                                           {
                                               Name = "NTB",
                                               Id = 0,
                                               RemoteCustomerId = 44967
                                           };
                        getListOfCustomers.Add(customer);
                        return getListOfCustomers;
                    }

                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }

                    // Returns the Customer-list
                    return getListOfCustomers;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                Mail.SendException(exception);

                return null;
            }
        }

        /// <summary>
        ///     The get list of customers by tournament id.
        /// </summary>
        /// <param name="tournamentId">
        ///     The tournament id.
        /// </param>
        /// <param name="sportId">
        ///     The sport id.
        /// </param>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Customer> GetListOfCustomersByTournamentId(int tournamentId, int sportId)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        sqlConnection.Open();
                    }

                    var sqlCommand = new SqlCommand("Service_GetCustomersFromTournamentIdAndSportId", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", tournamentId));

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    /**
                     * What we are to do: 
                     * First we get the TournamentId and JobId. We then use this to find
                     * the customers that shall have these data.
                     * We are to return a list of customernames
                     * 
                     */
                    var getListOfCustomers = new List<Customer>();
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            var customer = new Customer();
                            if (sqlDataReader["CustomerName"] != DBNull.Value)
                            {
                                customer.Name = sqlDataReader["CustomerName"].ToString();
                            }

                            if (sqlDataReader["CustomerId"] != DBNull.Value)
                            {
                                customer.Id = Convert.ToInt32(sqlDataReader["CustomerId"].ToString());
                            }

                            if (sqlDataReader["RemoteCustomerId"] != DBNull.Value)
                            {
                                customer.RemoteCustomerId = Convert.ToInt32(sqlDataReader["RemoteCustomerId"].ToString());
                            }

                            getListOfCustomers.Add(customer);
                        }

                        // Filter the outcome of the database
                        return getListOfCustomers.Distinct()
                            .ToList();
                    }

                    // logger.Debug("No customers needs these data");
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["livetesting"]))
                    {
                        var customer = new Customer
                                           {
                                               Name = "NTB",
                                               Id = 0,
                                               RemoteCustomerId = 44967
                                           };
                        getListOfCustomers.Add(customer);
                        return getListOfCustomers;
                    }

                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                    {
                        var customer = new Customer
                                           {
                                               Name = "NTB",
                                               Id = 0,
                                               RemoteCustomerId = 44967
                                           };
                        getListOfCustomers.Add(customer);
                        return getListOfCustomers;
                    }

                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }

                    // Returns the Customer-list
                    return getListOfCustomers;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                Mail.SendException(exception);

                return null;
            }
        }

        /// <summary>
        ///     This method shall be renamed to something else. It shall also return a list of customers
        /// </summary>
        /// <param name="matchId">
        ///     The Id of the match we are to get a list of customers
        /// </param>
        /// <param name="sportId">
        ///     The Id of the sport (in this case I believe it is 1 (soccer)
        /// </param>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Customer> GetListOfCustomersByMatchId(int matchId, int sportId)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        sqlConnection.Open();
                    }

                    var sqlCommand = new SqlCommand("Service_GetCustomersFromMatchIdAndSportId", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@MatchId", matchId));

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    /**
                     * What we are to do: 
                     * First we get the TournamentId and JobId. We then use this to find
                     * the customers that shall have these data.
                     * We are to return a list of customernames
                     * 
                     */
                    var getListOfCustomers = new List<Customer>();
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            var customer = new Customer();
                            if (sqlDataReader["CustomerName"] != DBNull.Value)
                            {
                                customer.Name = sqlDataReader["CustomerName"].ToString();
                            }

                            if (sqlDataReader["CustomerId"] != DBNull.Value)
                            {
                                customer.Id = Convert.ToInt32(sqlDataReader["CustomerId"].ToString());
                            }

                            if (sqlDataReader["RemoteCustomerId"] != DBNull.Value)
                            {
                                customer.RemoteCustomerId = Convert.ToInt32(sqlDataReader["RemoteCustomerId"].ToString());
                            }

                            getListOfCustomers.Add(customer);
                        }

                        // Filter the outcome of the database
                        return getListOfCustomers.Distinct()
                            .ToList();
                    }

                    // logger.Debug("No customers needs these data");
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["livetesting"]))
                    {
                        var customer = new Customer
                                           {
                                               Name = "NTB",
                                               Id = 0,
                                               RemoteCustomerId = 44967
                                           };
                        getListOfCustomers.Add(customer);
                        return getListOfCustomers;
                    }

                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                    {
                        var customer = new Customer
                                           {
                                               Name = "NTB",
                                               Id = 0,
                                               RemoteCustomerId = 44967
                                           };
                        getListOfCustomers.Add(customer);
                        return getListOfCustomers;
                    }

                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }

                    // Returns the Customer-list
                    return getListOfCustomers;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                Mail.SendException(exception);

                return null;
            }
        }

        /// <summary>
        ///     Method to find out if the matches table is empty or not.
        ///     Returns the number of matches we have stored in the database
        /// </summary>
        /// <returns>Integer value</returns>
        public int CheckMatchesTable()
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    var sqlCommand = new SqlCommand("Service_GetNumberOfMatches", sqlConnection)
                                         {
                                             CommandType = CommandType.StoredProcedure
                                         };

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    var numberOfMatches = 0;
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            if (sqlDataReader["NumberOfMatches"] != DBNull.Value)
                            {
                                numberOfMatches = Convert.ToInt32(sqlDataReader["NumberOfMatches"].ToString());
                            }
                        }

                        return numberOfMatches;
                    }

                    return 0;
                }
                catch (SqlException sqlexception)
                {
                    Logger.Error(sqlexception);
                    Logger.Error(sqlexception);
                    return 0;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    Logger.Error(exception);
                    return 0;
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// @Todo: We need to add this somewhere
        /// </summary>
        public void foo() {

            //this.soccerMatches.Add(this.TournamentServiceClient.GetMatch(matchId, true, true, true, true));

            //var matchRepository = new MatchRepository();
            //var matchMapper = new MatchMapper();

            //var localMatches = this.soccerMatches.Select(row => matchMapper.Map(row, new Classes.Classes.Match()))
            //    .ToList();
            //matchRepository.InsertAll(localMatches);

        }

        /// <summary>
        ///     This method checks the database for the match. If we find it, we just return..
        ///     If the method cannot find the match in the database, we add the match to the list of matches we are to check
        /// </summary>
        /// <param name="match">
        ///     Integer with the Id of the Match we are to check
        /// </param>
        /// <param name="sportId">
        ///     The sport Id.
        /// </param>
        public void CheckMatchByMatch(Match match, int sportId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("Service_GetMatchByMatchId", sqlConnection)
                                     {
                                         CommandType = CommandType.StoredProcedure
                                     };

                sqlCommand.Parameters.Add(new SqlParameter("@MatchId", match.MatchId));

                sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                // opening the first connection
                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                    sqlConnection.Open();
                }

                // Creating the SQL-reader.
                var sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    return;
                }
            }

            this.soccerMatches.Add(match);

            var matchRepository = new MatchRepository();
            var matchMapper = new MatchMapper();


            var localMatches = this.soccerMatches.Select(row => matchMapper.Map(row, new Classes.Classes.Match()))
                .ToList();
            matchRepository.InsertAll(localMatches);
        }

        /// <summary>
        ///     The check matches.
        /// </summary>
        /// <param name="sportId">
        ///     The sport Id.
        /// </param>
        public void CheckMatches(int sportId)
        {
            /*
             * We shall first check if there is a match with the MatchID in the database
             * * If it is, we shall check if the data we get is the same as the ones we got
             * * * If that is the case, we don't do anything
             * * * If not, we shall update the data
             * 
             * * If it isn't, we shall add the data we get
             */
            var dateTimeMatchDate = new DateTime();
            var dateTimeMatchTime = 0;

            foreach (var match in this.soccerMatches)
            {
                try
                {
                    using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
                    {
                        var sqlCommand = new SqlCommand("Service_GetMatchByMatchId", sqlConnection)
                                             {
                                                 CommandType = CommandType.StoredProcedure
                                             };

                        sqlCommand.Parameters.Add(new SqlParameter("@MatchId", match.MatchId));

                        sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                        // opening the first connection
                        if (sqlConnection.State != ConnectionState.Open)
                        {
                            sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                            sqlConnection.Open();
                        }

                        // Creating the SQL-reader.
                        var sqlDataReader = sqlCommand.ExecuteReader();

                        // Emptying the HomeTeam and AwayTeam Strings

                        // Setting MatchId to zero.
                        var matchId = 0;

                        // ... and update to false
                        var update = false;

                        // If we don't get rows we use the information in the match object and insert into the match database table
                        string homeTeam;
                        string awayTeam;
                        if (!sqlDataReader.HasRows)
                        {
                            homeTeam = match.HomeTeamName;
                            awayTeam = match.AwayTeamName;

                            if (match.MatchStartDate.HasValue)
                            {
                                dateTimeMatchDate = match.MatchStartDate.Value;
                                var timeString = match.MatchStartDate.Value.ToShortTimeString();
                                if (timeString.Contains("."))
                                {
                                    timeString = timeString.Replace(".", string.Empty);
                                }

                                if (timeString.Contains(":"))
                                {
                                    timeString = timeString.Replace(":", string.Empty);
                                }
                                dateTimeMatchTime = Convert.ToInt32(timeString);
                            }

                            // Getting the MatchId
                            if (Convert.ToInt32(match.MatchId) > 0)
                            {
                                matchId = match.MatchId;
                            }

                            try
                            {
                                var insertConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

                                // Opening the second connection
                                if (insertConnection.State != ConnectionState.Open)
                                {
                                    insertConnection.Open();
                                }

                                var myCommand = new SqlCommand("Service_InsertMatches", insertConnection);

                                myCommand.Parameters.Add(new SqlParameter("@MatchId", matchId));

                                myCommand.Parameters.Add(new SqlParameter("@HomeTeam", homeTeam));

                                myCommand.Parameters.Add(new SqlParameter("@HomeTeamId", match.HomeTeamId));

                                myCommand.Parameters.Add(new SqlParameter("@AwayTeam", awayTeam));

                                myCommand.Parameters.Add(new SqlParameter("@AwayTeamId", match.AwayTeamId));

                                myCommand.Parameters.Add(new SqlParameter("@Date", dateTimeMatchDate));

                                myCommand.Parameters.Add(new SqlParameter("@Time", dateTimeMatchTime));

                                myCommand.Parameters.Add(new SqlParameter("@Downloaded", 1));

                                myCommand.Parameters.Add(new SqlParameter("@TournamentId", match.TournamentId));

                                myCommand.Parameters.Add(new SqlParameter("@TournamentRoundNumber", match.TournamentRoundNumber));

                                myCommand.Parameters.Add(new SqlParameter("@VenueId", match.StadiumId));

                                myCommand.Parameters.Add(new SqlParameter("@VenueName", match.StadiumName));

                                myCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                                myCommand.CommandType = CommandType.StoredProcedure;

                                myCommand.ExecuteNonQuery();

                                // Closing the connection
                                insertConnection.Close();

                                insertConnection.Dispose();
                            }
                            catch (SqlException sqlexception)
                            {
                                Logger.Error(sqlexception);
                            }
                            catch (Exception exception)
                            {
                                Logger.Error(exception);
                            }
                            finally
                            {
                                sqlDataReader.Close();

                                if (sqlConnection.State != ConnectionState.Closed)
                                {
                                    sqlConnection.Close();
                                }
                            }
                        }
                        else
                        {
                            // This should only be one so... 
                            while (sqlDataReader.Read())
                            {
                                matchId = match.MatchId;
                                if (sqlDataReader["HomeTeam"].ToString() != match.HomeTeamName)
                                {
                                    homeTeam = match.HomeTeamName;
                                    update = true;
                                }
                                else
                                {
                                    homeTeam = sqlDataReader["HomeTeam"].ToString();
                                }

                                if (sqlDataReader["AwayTeam"].ToString() != match.AwayTeamName)
                                {
                                    awayTeam = match.AwayTeamName;
                                    update = true;
                                }
                                else
                                {
                                    awayTeam = sqlDataReader["AwayTeam"].ToString();
                                }

                                if (match.MatchStartDate.HasValue)
                                {
                                    if (Convert.ToDateTime(sqlDataReader["MatchDate"]) != match.MatchStartDate.Value)
                                    {
                                        dateTimeMatchDate = match.MatchStartDate.Value;
                                        update = true;
                                    }
                                    else
                                    {
                                        dateTimeMatchDate = Convert.ToDateTime(sqlDataReader["MatchDate"]);
                                    }

                                    if (Convert.ToInt32(
                                        match.MatchStartDate.Value.ToShortTimeString()
                                            .Replace(":", "")) != 0)
                                    {
                                        dateTimeMatchTime = Convert.ToInt32(
                                            match.MatchStartDate.Value.ToShortTimeString()
                                                .Replace(":", ""));
                                        update = true;
                                    }
                                    else
                                    {
                                        dateTimeMatchTime = Convert.ToInt32(sqlDataReader["MatchStartTime"]);
                                    }
                                }
                                else
                                {
                                    // Getting the date and time values from the database (if we have it)
                                    dateTimeMatchDate = Convert.ToDateTime(sqlDataReader["MatchDate"]);
                                    dateTimeMatchTime = Convert.ToInt32(sqlDataReader["MatchStartTime"]);
                                }

                                var matchResult = match.MatchResultList;
                                var boolDownload = false;
                                var intDownload = 0;
                                foreach (var result in matchResult)
                                {
                                    if (result.ResultTypeName == "Sluttresultat")
                                    {
                                        boolDownload = true;
                                        update = true;
                                    }
                                }

                                // We shall update, but only if there is something to update
                                if (update)
                                {
                                    try
                                    {
                                        // We are checking if we are to set download to 1 or zero(null)
                                        if (boolDownload)
                                        {
                                            intDownload = 1;
                                        }

                                        var updateConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

                                        // Opening yet another connection
                                        if (updateConnection.State != ConnectionState.Open)
                                        {
                                            updateConnection.Open();
                                        }

                                        var myCommand = new SqlCommand("Service_UpdateMatches", updateConnection);

                                        myCommand.Parameters.Add(new SqlParameter("@MatchId", matchId));

                                        myCommand.Parameters.Add(new SqlParameter("@HomeTeam", homeTeam));

                                        myCommand.Parameters.Add(new SqlParameter("@AwayTeam", awayTeam));

                                        myCommand.Parameters.Add(new SqlParameter("@Date", dateTimeMatchDate));

                                        myCommand.Parameters.Add(new SqlParameter("@Time", dateTimeMatchTime));

                                        myCommand.Parameters.Add(new SqlParameter("@Downloaded", intDownload));

                                        myCommand.Parameters.Add(new SqlParameter("@TournamentId", match.TournamentId));

                                        // Setting the command type to Stored Procedure so that we use the stored procedure.
                                        myCommand.CommandType = CommandType.StoredProcedure;

                                        myCommand.ExecuteNonQuery();

                                        update = false;

                                        // Closing the connection
                                        if (updateConnection.State != ConnectionState.Closed)
                                        {
                                            updateConnection.Close();
                                        }

                                        updateConnection.Dispose();
                                    }
                                    catch (SqlException sqlexception)
                                    {
                                        Logger.Error(sqlexception);
                                    }
                                    catch (Exception exception)
                                    {
                                        Logger.Error(exception);
                                    }
                                }
                            }

                            sqlDataReader.Close();

                            // Closing connection
                            if (sqlConnection.State != ConnectionState.Closed)
                            {
                                sqlConnection.Close();
                            }

                            // Disposing connection
                            sqlConnection.Dispose();
                        }
                    }
                }
                catch (SqlException sqlexception)
                {
                    Logger.Error(sqlexception);
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                }
            }
        }

        /// <summary>
        ///     The get match by id.
        /// </summary>
        /// <param name="matchId">
        ///     The match id.
        /// </param>
        public void GetMatchById(int matchId)
        {
            try
            {
                var match = this.TournamentServiceClient.GetMatch(matchId, true, true, true, true);

                this.SetSoccerMatches.Add(match);
            }
            catch (Exception exception)
            {
                Logger.Info("MatchId: " + matchId);
                Logger.Info(exception.Message);
            }
        }

        

        #region getters and setters

        /// <summary>
        ///     Gets or sets the set matches.
        /// </summary>
        public List<Match> SetSoccerMatches
        {
            get
            {
                return this.soccerMatches;
            }

            set
            {
                this.soccerMatches = value;
            }
        }

        /// <summary>
        ///     Gets or sets the set tournaments.
        /// </summary>
        public List<Local_Tournament> SetTournaments { get; set; }

        #endregion getters and setters

        #region Weekly

        // public int deleteData()
        // {
        // try
        // {
        // // TODO: Fix Document-table so that we have a Downloaded flag there to
        // string SQLQuery = "DELETE FROM Matches WHERE Downloaded=1; DELETE FROM Document";

        // SqlCommand commander = Command(SQLQuery);

        // SqlDataReader sqlreader = commander.ExecuteReader();

        // return sqlreader.RecordsAffected;

        // }
        // catch (SqlException sqlexception)
        // {
        // logger.Error(sqlexception);
        // return 0;
        // }
        // catch (Exception exception)
        // {
        // logger.Error(exception);
        // return 0;
        // }
        // finally
        // {

        // }
        // }

        #endregion
    }
}