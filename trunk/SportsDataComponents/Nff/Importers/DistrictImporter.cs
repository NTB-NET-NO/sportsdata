﻿using System.Collections.Generic;
using NTB.SportsData.Components.Nff.Repositories;
using NTB.SportsData.Components.Nff.Repositories.Interfaces;

namespace NTB.SportsData.Components.Nff.Importers
{
    public class DistrictImporter
    {
        public void ImportDistricts(List<Classes.Classes.District> districts)
        {
            // Check the districts-table. If we get a false-return, we are stopping configuration
            IDistrictRepository districtRepository = new DistrictRepository();
            foreach (var district in districts)
            {
                var foundDistrict = districtRepository.GetDistrictById(district.Id);

                if (foundDistrict != null)
                {
                    continue;
                }

                district.SportId = 16;
                districtRepository.InsertDistrict(district);
            }

        }
    }
}
