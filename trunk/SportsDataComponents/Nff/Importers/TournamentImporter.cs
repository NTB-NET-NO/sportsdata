﻿using System.Collections.Generic;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nff.Repositories;
using NTB.SportsData.Components.Nff.Repositories.Interfaces;

namespace NTB.SportsData.Components.Nff.Importers
{
    public class TournamentImporter
    {
        public void ImportTournaments(List<Tournament> tournaments)
        {
            // Check the districts-table. If we get a false-return, we are stopping configuration
            ITournamentRepository repository = new TournamentRepository();
            foreach (var tournament in tournaments)
            {
                var foundTournament = repository.GetTournamentByTournamentId(tournament.Id);

                if (foundTournament != null)
                {
                    continue;
                }

                tournament.SportId = 16;
                repository.InsertTournament(tournament);
            }

        }

    }
}
