﻿using System;
using log4net;
using NTB.SportsData.Components.Nff.Facade;
using NTB.SportsData.Components.PublicRepositories;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;

namespace NTB.SportsData.Components.Nff.Importers
{
    public class AgeCategoryImporter
    {
        /// <summary>
        ///     Static logger
        /// </summary>
        public static readonly ILog Logger = LogManager.GetLogger(typeof(AgeCategoryImporter));

        public int SportId { get; set; }

        public void ImportAgeCategories()
        {
            Logger.Debug("Populating/checking AgeCategoryTable");
            ISoccerFacade facade = new SoccerFacade();
            IAgeCategoryDataMapper ageCategoryRepository = new AgeCategoryRepository();
            var ageCategories = facade.GetAgeCategoryTournament();
            // this.MetaServiceClient.GetAgeCategoriesTournament();

            foreach (var agecategory in ageCategories)
            {
                var foundAgeCategory = ageCategoryRepository.GetAgeCategoryByIdAndSportId(agecategory.Id, this.SportId);
                if (foundAgeCategory != null)
                {
                    continue;
                }

                // If we cannot find the age category, we will insert it.
                try
                {
                    agecategory.AgeCategoryDefinitionId = 1;
                    if (agecategory.Id == 18 || agecategory.Id == 21)
                    {
                        agecategory.AgeCategoryDefinitionId = 2;
                    } 
                    ageCategoryRepository.InsertAgeCategory(agecategory, this.SportId);
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                }
            }
        }
    }
}
