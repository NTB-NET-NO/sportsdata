﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.PublicRepositories;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;

namespace NTB.SportsData.Components.Nff.Importers
{
    public class SeasonImporter
    {
        /// <summary>
        ///     Static logger
        /// </summary>
        public static readonly ILog Logger = LogManager.GetLogger(typeof(SeasonImporter));

        public int SportId { get; set; }

        public bool ImportSeasons(List<Season> seasons)
        {
            var filteredSeasons = new List<Season>();
            var returnValue = false;

            ISeasonDataMapper seasonRepository = new SeasonRepository();
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
            {
                var testDate = DateTime.Parse(ConfigurationManager.AppSettings["nfftestingdate"])
                    .Date;
                var testSeason = seasons.Single(x => x.StartDate >= testDate && x.EndDate <= testDate);
                filteredSeasons.Add(testSeason);
            }
            else
            {
                foreach (var season in seasons.Where(x => x.Active.Equals(true)))
                {
                    filteredSeasons.Add(season);
                }
            }

            foreach (var season in filteredSeasons)
            {
                var result = seasonRepository.GetSeasonById(season.Id);

                if (result == null)
                {
                    // We must insert since the season was not found
                    var currentSeason = season;
                    seasonRepository.InsertOne(currentSeason);
                }

                returnValue = true;
            }

            return returnValue;
        }
    }
}
