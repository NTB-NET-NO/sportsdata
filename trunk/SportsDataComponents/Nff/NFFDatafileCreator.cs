// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NFFDatafileCreator.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the NffDatafileCreator type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using log4net;
using log4net.Config;
using NTB.SportsData.Components.Nff.Models;

namespace NTB.SportsData.Components.Nff
{
    // Adding support for log4net

    // NFF service references

    /// <summary>
    ///     The nff datafile creator.
    /// </summary>
    public class NffDatafileCreator
    {
        /// <summary>
        ///     Static logger
        /// </summary>
        public static readonly ILog Logger = LogManager.GetLogger(typeof(NffDatafileCreator));

        public DataFileModel Model { get; set; }
        
        /// <summary>
        ///     Initializes a new instance of the <see cref="NffDatafileCreator" /> class.
        /// </summary>
        public NffDatafileCreator()
        {
            {
                // Set up logger
                XmlConfigurator.Configure();

                if (!LogManager.GetRepository().Configured)
                {
                    BasicConfigurator.Configure();
                }
            }
        }
    }
}