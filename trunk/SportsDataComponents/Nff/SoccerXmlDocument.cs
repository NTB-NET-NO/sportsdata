// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NffXmlDocument.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The nff xml document.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Classes.Enums;
using NTB.SportsData.Components.Nff.Facade;
using log4net;
using NTB.SportsData.Components.Nff.Models;
using NTB.SportsData.Components.Nff.Repositories;
using NTB.SportsData.Components.Nff.Repositories.Interfaces;

namespace NTB.SportsData.Components.Nff
{
    /// <summary>
    ///     The nff xml document.
    /// </summary>
    public class SoccerXmlDocument
    {
        /// <summary>
        ///     Static logger
        /// </summary>
        public static readonly ILog Logger = LogManager.GetLogger(typeof(SoccerXmlDocument));

        public XmlDocumentModel Model { get; set; }
        /// <summary>
        ///     Initializes a new instance of the <see cref="SoccerXmlDocument" /> class.
        /// </summary>
        /// <param name="model">
        ///     The Xml Document Model.
        /// </param>
        public SoccerXmlDocument(XmlDocumentModel model)
        {
            this.Model = model;
            this.Model.Standing = false;
            this.Model.MatchFacts = false;
            this.Model.FileOutputFolder = model.FileOutputFolder;
            this.Model.RenderResult = model.RenderResult;
        }


        public string CreateMessageType(Filename filename)
        {
            // Adding support for document type
            var fileMessageType = "sportsresults";

            if (filename == null)
            {
                return fileMessageType;
            }

            switch (filename.MessageType)
            {
                case MessageType.MatchFacts:
                    fileMessageType = "matchfacts";
                    break;
                case MessageType.SportMessage:
                    fileMessageType = "sportsmessage";
                    break;
                case MessageType.SportResult:
                    fileMessageType = "sportsresults";
                    break;
                case MessageType.SportSchedule:
                    fileMessageType = "sportsschedule";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return fileMessageType;
        }

        public void CreateFileNode(XmlDocument xmlDoc, XmlNode node, string fileMessageType, Filename filename, string documentName, int documentVersion)
        {
            // removing possible file element strukture first
            var fileNode = xmlDoc.SelectSingleNode("/SportsData/Tournament/Header/File");
            if (fileNode != null)
            {
                fileNode.RemoveAll();
            }

            var xmlFile = xmlDoc.CreateElement("File");

            var xmlFileInfo = xmlDoc.CreateElement("Name");
            xmlFileInfo.SetAttribute("FullName", filename.FullName);

            xmlFileInfo.SetAttribute("Type", "xml");
            var xmlText = xmlDoc.CreateTextNode(documentName);
            xmlFileInfo.AppendChild(xmlText);
            xmlFile.AppendChild(xmlFileInfo);

            var xmlVersion = xmlDoc.CreateElement("Version");
            xmlText = xmlDoc.CreateTextNode(documentVersion.ToString());
            xmlVersion.AppendChild(xmlText);
            xmlFile.AppendChild(xmlVersion);

            var xmlDocumentType = xmlDoc.CreateElement("DocumentType");
            xmlDocumentType.SetAttribute("type", fileMessageType);

            xmlFileInfo = xmlDoc.CreateElement("Created");
            var dt = DateTime.Now;
            var timestamp = dt.ToString("yyyy-MM-ddTHH:mm:ss");
            xmlText = xmlDoc.CreateTextNode(timestamp);
            xmlFileInfo.AppendChild(xmlText);
            xmlFile.AppendChild(xmlFileInfo);
            if (node != null)
            {
                node.AppendChild(xmlDocumentType);
                node.AppendChild(xmlFile);
            }
        }

        /// <summary>
        ///     The create xml file.
        /// </summary>
        /// <param name="doc">
        ///     The doc.
        /// </param>
        /// <param name="tournamentName">
        ///     The tournament name.
        /// </param>
        /// <param name="tournamentId">
        ///     The tournament id.
        /// </param>
        /// <param name="filenames">
        ///     The filenames.
        /// </param>
        /// <param name="districtId">
        ///     The district id.
        /// </param>
        public void WriteXmlFile(XmlDocument doc, string tournamentName, int tournamentId, List<Filename> filenames, int districtId = 0)
        {
            // Getting DistrictName
            foreach (var filename in filenames)
            {
                try
                {
                    var currentDoc = new XmlDocument();
                    currentDoc.LoadXml(doc.OuterXml);
                    // Adding the file part to the XML-file
                    // Here we need some File-information
                    var documentName = string.Empty;
                    var documentVersion = 1;

                    // Checking if district-tournament file has been created before
                    var node = currentDoc.SelectSingleNode("/SportsData/Tournament/Header");

                    // Adding support for document type
                    var fileMessageType = this.CreateMessageType(filename);
                    
                    // Adding document version and document name
                    if (filename != null)
                    {
                        documentVersion = filename.Version;
                        documentName = filename.Name;
                    }

                    // Creating the document file node
                    this.CreateFileNode(currentDoc, node, fileMessageType, filename, documentName, documentVersion);
                    if (this.Model.FileOutputFolder == null)
                    {
                        if (this.Model.OutputFolder != string.Empty)
                        {
                            this.Model.FileOutputFolder = new List<string>()
                            {
                                this.Model.OutputFolder
                            };
                        }
                        else
                        {
                            throw new SportsDataException("Could not save the file as the output-folder is not defined!!!");
                        }
                    }

                    foreach (var outputFolder in this.Model.FileOutputFolder)
                    {
                        if (!Directory.Exists(outputFolder))
                        {
                            Logger.InfoFormat("I am creating outputfolder: {0}",outputFolder);
                            Directory.CreateDirectory(outputFolder);
                        }
                        else
                        {
                            Directory.SetCurrentDirectory(outputFolder);
                        }

                        // Creating the complete Filename
                        if (filename == null)
                        {
                            continue;
                        }
                        Logger.DebugFormat("OutputPath in filename: {0}", filename.Path);
                        filename.Path = outputFolder;
                        Logger.DebugFormat("OutputPath in filename after outputFolder: {0}", filename.Path);
                        var pathAndFilename = Path.Combine(filename.Path, filename.FullName);
                        Logger.DebugFormat("Testing to save: {0}", pathAndFilename);

                        // string completeFilename = @"c:\" + DocumentName;
                        var writerSettings = new XmlWriterSettings
                        {
                            Indent = true,
                            OmitXmlDeclaration = false,
                            Encoding = Encoding.UTF8
                        };

                        // DONE: Change Using to While so that we can loop in case the file is in use...
                        for (var i = 0; i <= 3; i++)
                        {
                            using (var writer = XmlWriter.Create(pathAndFilename, writerSettings))
                            {
                                try
                                {
                                    Logger.InfoFormat("Saving: {0}", pathAndFilename);
                                    currentDoc.Save(writer);
                                    break;
                                }
                                catch (IOException ioex)
                                {
                                    Logger.Error(ioex);
                                }
                            }
                        }
                    }
                }
                catch (XmlException xex)
                {
                    Logger.Error("Problems writing file to: " + filename.FullName, xex);
                }
                catch (IOException ioex)
                {
                    Logger.Error(ioex);
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                }
            }
        }

        /// <summary>
        ///     The create file name.
        /// </summary>
        /// <param name="districtId">
        ///     The district id.
        /// </param>
        /// <param name="tournamentId">
        ///     The tournament id.
        /// </param>
        /// <param name="dateTime">
        ///     The Date and Time for generating a filename
        /// </param>
        /// <param name="sportId">
        ///     The sport Id.
        /// </param>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Filename> CreateFileName(int districtId, int tournamentId, DateTime? dateTime, int sportId)
        {
            var documentVersion = 1;

            var filenames = new List<Filename>();
            var fileName = new Filename();

            // We need to check if the file has a table or not
            var postfix = "Res";
            fileName.MessageType = MessageType.SportMessage;

            if (this.Model.Standing)
            {
                postfix += "Tab"; // Making the postfix ResTab
                fileName.MessageType = MessageType.SportResult;
            }

            if (!this.Model.RenderResult)
            {
                postfix = "Sched";
                fileName.MessageType = MessageType.SportSchedule;
            }

            // We have not found a document produced for this tournament
            // We generate the filename, and then just the stem
            var todayFile = DateTime.Today;
            if (dateTime != null)
            {
                todayFile = Convert.ToDateTime(dateTime);
            }

            // We are not adding XML to the filename, this will be added when we are creating the rest
            var filename = "NTB_SportsData_" + todayFile.Year + todayFile.Month.ToString()
                .PadLeft(2, '0') + todayFile.Day.ToString()
                    .PadLeft(2, '0') + "_NFF_D" + districtId + "_T" + tournamentId + "_" + postfix;

            IDocumentRepository documentRepository = new DocumentRepository();

            var currentDocument = documentRepository.GetDocumentByTournamentId(tournamentId);
            

            if (currentDocument == null)
            {
                var sportsDataDocument = new SportsDataDocument();
                sportsDataDocument.TournamentId = tournamentId;
                sportsDataDocument.SportId = sportId;
                sportsDataDocument.Version = documentVersion;
                sportsDataDocument.Name = filename;
                sportsDataDocument.CreatedDateTime = DateTime.Now;

                documentRepository.InsertDocument(sportsDataDocument);
            }
            else
            {
                documentVersion = currentDocument.Version += 1;
                currentDocument.Version = documentVersion;
                currentDocument.CreatedDateTime = DateTime.Now;
            
                documentRepository.UpdateDocument(currentDocument);
            }
            
            fileName.Name = filename;
            fileName.Version = documentVersion;
            fileName.Created = DateTime.Now;

            if (filename == string.Empty)
            {
                filename = fileName.Version.ToString();
                filename += fileName.Created.ToShortDateString();
            }

            filename += "_V" + documentVersion;

            // Writing to file
            if (this.Model.OutputFolder == @"\" || string.IsNullOrEmpty(this.Model.OutputFolder))
            {
                // Model.OutputFolder = ConfigurationManager.AppSettings["OutPath"];
                filename += "_" + this.Model.JobName;
            }

            fileName.FullName = filename + ".xml";
            fileName.Path = this.Model.OutputFolder;
            Logger.Debug("Filename: " + fileName.FullName);

            filenames.Add(fileName);

            if (this.Model.MatchFacts)
            {
                var filenameFacts = "NTB_SportsData_" + todayFile.Year + todayFile.Month.ToString()
                    .PadLeft(2, '0') + todayFile.Day.ToString()
                        .PadLeft(2, '0') + "_NFF_D" + districtId + "_T" + tournamentId + "_Fakta";

                filenameFacts += "_V" + documentVersion;
                var matchFactsFilename = new Filename
                {
                    Name = filenameFacts, Version = documentVersion, Created = DateTime.Now, FullName = filenameFacts + ".xml", Path = this.Model.OutputFolder, MessageType = MessageType.MatchFacts
                };
                filenames.Add(matchFactsFilename);
            }

            return filenames;
        }

        /// <summary>
        ///     Method to create the XMLDocument that we are to convert to SportsML later
        /// </summary>
        /// <param name="matches">
        ///     List of matches
        /// </param>
        /// <param name="tournament">
        ///     Tourmanent object
        /// </param>
        /// <param name="ageCategoryTournament">
        ///     Age category tournament
        /// </param>
        /// <param name="listOfStandings">
        ///     List of standings
        /// </param>
        /// <param name="finalResult">
        ///     If this is final results or not
        /// </param>
        /// <param name="listOfCustomers">
        ///     list of customers
        /// </param>
        /// <returns>
        ///     Returns XML Document structure
        /// </returns>
        public XmlDocument CreateXmlStructure(List<Match> matches, 
            Tournament tournament, 
            AgeCategory ageCategoryTournament, 
            List<TournamentTable> listOfStandings = null, 
            bool finalResult = false, 
            List<Customer> listOfCustomers = null)
        {
            // List<Filename> fileNames = null,
            // Line is used for something that I cannot remember right now

            // Creating xml-document
            Logger.Info("I am now creating the XML Structure for " + tournament.Name);

            var doc = new XmlDocument(); // Create the XML Declaration, and append it to XML document

            this.Model.Standing = false;
            this.Model.MatchFacts = false;

            Logger.Debug("Done creating the XMLDocument object!");
            try
            {
                var dec = doc.CreateXmlDeclaration("1.0", null, null);
                Logger.Debug("Done creating the XmlDeclaration!");

                doc.AppendChild(dec); // Create the root element
                Logger.Debug("Done Create the root element!");

                // To indicate some type of status
                /*
                 * 0 - not complete
                 * 1 - complete 
                 * ?
                 * 
                 * Only to be used when generating results
                 */
                var intMatches = matches.Count();
                Logger.Info("Number of matches: " + intMatches);

                var xmlMainRoot = doc.CreateElement("SportsData");
                var xmlRoot = doc.CreateElement("Tournament");

                xmlRoot.SetAttribute("Type", this.Model.RenderResult ? "resultat" : "terminliste");

                var xmlDistrict = this.CreateDistrictElement(tournament, doc);

                
                var matchQuery = (from filteredMatch in matches where filteredMatch.TournamentId == tournament.Id select filteredMatch).Take(1)
                    .ToList();
                var matchResult = new Match();

                var xmlTournamentMetaInfo = this.CreateTournamentMetaInfo(tournament, doc, matchQuery, matchResult, xmlRoot, xmlDistrict);

                xmlRoot.AppendChild(xmlTournamentMetaInfo);
                Logger.Debug("Done create more information on Tournament and more");

                // Header information
                var xmlHeader = doc.CreateElement("Header");

                this.CreateOrganisationElement(doc, xmlHeader);

                var xmlAgeCategory = this.CreateAgeCategoryElement(tournament, ageCategoryTournament, doc);

                xmlHeader.AppendChild(xmlAgeCategory);

                var xmlCustomers = this.CreateCustomersElements(listOfCustomers, doc);

                xmlHeader.AppendChild(xmlCustomers);

                // Adding information about the sport
                var xmlSport = doc.CreateElement("Sport");
                var xmlText = doc.CreateTextNode("Fotball");
                xmlSport.AppendChild(xmlText);
                xmlHeader.AppendChild(xmlSport);

                Logger.Info("Done creating Organization information for the XML-file");

                xmlRoot.AppendChild(xmlHeader);

                Logger.Info("Creating the Matches/Match elements in the XML-file");
                var xmlMatches = doc.CreateElement("Matches");

                if (this.Model.RenderResult)
                {
                    // Adding a check so that we are not adding matches that does not have a result yet to the XML
                    var filteredMatches = (from m in matches where m.HomeTeamGoals != null select m).ToList();

                    if (filteredMatches.Count == 0)
                    {
                        return null;
                    }

                    foreach (var match in filteredMatches)
                    {
                        this.CreateMatchStructure(match, doc, xmlMatches, true);
                    }
                }
                else
                {
                    // When creating a schedule things are different. Then we might want to have todays matches and yesterdays matches and so on...
                    var scheduledMatches = matches.Where(match => match.HomeTeamGoals == null)
                        .ToList();

                    if (scheduledMatches.Any())
                    {
                        // foreach (Match match in matches.Where(match => match.HomeTeamGoals == null))
                        foreach (var match in scheduledMatches)
                        {
                            this.CreateMatchStructure(match, doc, xmlMatches, false);
                        }
                    }
                    else
                    {
                        return null;
                    }
                }

                XmlElement xmlStandings = null;
                if (this.Model.RenderResult)
                {
                    Logger.Debug("Creating the XML-element for the standings");

                    xmlStandings = doc.CreateElement("Standings");
                }

                this.CreateStandingElements(listOfStandings, doc, xmlStandings);

                xmlRoot.AppendChild(xmlMatches);

                if (listOfStandings != null)
                {
                    if (xmlStandings != null) xmlRoot.AppendChild(xmlStandings);
                }

                xmlMainRoot.AppendChild(xmlRoot);
                doc.AppendChild(xmlMainRoot);
                Logger.Debug("Done Creating the XML-structure");

                return doc;
            }
            catch (XmlException xmlexception)
            {
                Logger.Debug(xmlexception.Message);
                Logger.Debug(xmlexception);
            }
            catch (Exception exception)
            {
                Logger.Debug(exception.Message);
                Logger.Debug(exception);
            }

            return doc;
        }

        public void CreateStandingElements(List<TournamentTable> listOfStandings, XmlDocument doc, XmlElement xmlStandings)
        {
            XmlText xmlText;
            if (listOfStandings != null)
            {
                this.Model.Standing = true;

                // Looping the TournanemtTableTeam
                foreach (var standing in listOfStandings)
                {
                    var xmlStandingTeam = doc.CreateElement("Team");
                    xmlStandingTeam.SetAttribute("Id", standing.TeamId.ToString());
                    xmlStandingTeam.SetAttribute("TablePosition", standing.TablePosition.ToString());
                    var xmlTeamName = doc.CreateElement("Name");
                    xmlText = doc.CreateTextNode(standing.TeamNameInTournament.Trim());
                    xmlTeamName.AppendChild(xmlText);
                    xmlStandingTeam.AppendChild(xmlTeamName);

                    var xmlTablePosition = doc.CreateElement("Position");
                    xmlText = doc.CreateTextNode(standing.TablePosition.ToString());
                    xmlTablePosition.AppendChild(xmlText);

                    var xmlPointsTotal = doc.CreateElement("Points");
                    xmlPointsTotal.SetAttribute("Total", standing.Points.ToString());

                    var xmlGoalDifferenceTotal = doc.CreateElement("GoalDifferences");
                    xmlGoalDifferenceTotal.SetAttribute("Total", standing.TotalGoalDifference.ToString());

                    var xmlGoalsTotal = doc.CreateElement("Goals");

                    var scoresTotal = (standing.NoHomeGoals + standing.NoAwayGoals).ToString();

                    var againstTotal = (standing.NoHomeAgainst + standing.NoAwayAgainst).ToString();

                    xmlGoalsTotal.SetAttribute("TotalScored", scoresTotal);
                    xmlGoalsTotal.SetAttribute("TotalAgainst", againstTotal);

                    var matchesTotal = standing.NoAwayMatches + standing.NoHomeMatches;
                    var xmlMatchesTotal = doc.CreateElement("Matches");
                    xmlMatchesTotal.SetAttribute("Total", matchesTotal.ToString());

                    // ---- Home Matches
                    var xmlHomeMatches = doc.CreateElement("HomeMatches");
                    xmlHomeMatches.SetAttribute("Won", standing.NoHomeWins.ToString());
                    xmlHomeMatches.SetAttribute("Draw", standing.NoHomeDraws.ToString());
                    xmlHomeMatches.SetAttribute("Lost", standing.NoHomeLosses.ToString());
                    xmlMatchesTotal.AppendChild(xmlHomeMatches);

                    var xmlHomeGoalsScored = doc.CreateElement("Home");
                    xmlHomeGoalsScored.SetAttribute("Scored", standing.NoHomeGoals.ToString());
                    xmlHomeGoalsScored.SetAttribute("Against", standing.NoHomeAgainst.ToString());
                    xmlGoalsTotal.AppendChild(xmlHomeGoalsScored);

                    var xmlPointsHome = doc.CreateElement("HomePoints");
                    xmlText = doc.CreateTextNode(standing.HomePoints.ToString());
                    xmlPointsHome.AppendChild(xmlText);
                    xmlPointsTotal.AppendChild(xmlPointsHome);

                    var xmlGoalDifferenceHome = doc.CreateElement("GoalDifferenceHome");
                    xmlText = doc.CreateTextNode(standing.HomeGoalDifference.ToString());
                    xmlGoalDifferenceHome.AppendChild(xmlText);
                    xmlGoalDifferenceTotal.AppendChild(xmlGoalDifferenceHome);

                    // ---- Away Matches 
                    var xmlAwayMatches = doc.CreateElement("AwayMatches");
                    xmlAwayMatches.SetAttribute("Won", standing.NoAwayWins.ToString());
                    xmlAwayMatches.SetAttribute("Draw", standing.NoAwayDraws.ToString());
                    xmlAwayMatches.SetAttribute("Lost", standing.NoAwayLosses.ToString());
                    xmlMatchesTotal.AppendChild(xmlAwayMatches);

                    var xmlAwayGoalsScored = doc.CreateElement("Away");
                    xmlAwayGoalsScored.SetAttribute("Scored", standing.NoAwayGoals.ToString());
                    xmlAwayGoalsScored.SetAttribute("Against", standing.NoAwayAgainst.ToString());
                    xmlGoalsTotal.AppendChild(xmlAwayGoalsScored);

                    var xmlPointsAway = doc.CreateElement("AwayPoints");
                    xmlText = doc.CreateTextNode(standing.AwayPoints.ToString());
                    xmlPointsAway.AppendChild(xmlText);
                    xmlPointsTotal.AppendChild(xmlPointsAway);

                    var xmlAwayMatchesPlayed = doc.CreateElement("AwayMatchesPlayed");
                    xmlText = doc.CreateTextNode(standing.NoAwayMatches.ToString());
                    xmlAwayMatchesPlayed.AppendChild(xmlText);

                    var xmlGoalDifferenceAway = doc.CreateElement("GoalDifferenceAway");
                    xmlText = doc.CreateTextNode(standing.AwayGoalDifference.ToString());
                    xmlGoalDifferenceAway.AppendChild(xmlText);
                    xmlGoalDifferenceTotal.AppendChild(xmlGoalDifferenceAway);

                    xmlStandingTeam.AppendChild(xmlPointsTotal);
                    xmlStandingTeam.AppendChild(xmlGoalsTotal);
                    xmlStandingTeam.AppendChild(xmlMatchesTotal);

                    xmlStandings.AppendChild(xmlStandingTeam);
                }
            }

            Logger.Debug("Done creating the XML-element for the standings");
        }

        public XmlElement CreateCustomersElements(List<Customer> listOfCustomers, XmlDocument doc)
        {
            var xmlCustomers = doc.CreateElement("Customers");

            // Now we shall add customers
            if (listOfCustomers != null)
            {
                foreach (var customer in listOfCustomers)
                {
                    var xmlCustomer = doc.CreateElement("Customer");
                    var xmlCustomerName = doc.CreateElement("CustomerName");
                    xmlCustomerName.SetAttribute("Id", customer.Id.ToString());
                    xmlCustomerName.SetAttribute("RemoteCustomerId", customer.RemoteCustomerId.ToString());
                    var xmlText = doc.CreateTextNode(customer.Name);
                    xmlCustomerName.AppendChild(xmlText);
                    xmlCustomer.AppendChild(xmlCustomerName);
                    xmlCustomers.AppendChild(xmlCustomer);
                }
            }
            return xmlCustomers;
        }

        public XmlElement CreateAgeCategoryElement(Tournament tournament, AgeCategory ageCategoryTournament, XmlDocument doc)
        {
            var xmlAgeCategory = doc.CreateElement("AgeCategory");

            if (ageCategoryTournament == null)
            {
                return xmlAgeCategory;
            }

            if (!tournament.NationalTeamTournament)
            {
                switch (ageCategoryTournament.Id)
                {
                    case 18:
                    case 21:
                        xmlAgeCategory.SetAttribute("min-age", ageCategoryTournament.FromAge.ToString());
                        xmlAgeCategory.SetAttribute("max-age", ageCategoryTournament.ToAge.ToString());
                        xmlAgeCategory.SetAttribute("name", ageCategoryTournament.Name);
                        xmlAgeCategory.SetAttribute("id", ageCategoryTournament.Id.ToString());
                        xmlAgeCategory.InnerText = "senior";
                        break;

                    default:
                        xmlAgeCategory.SetAttribute("min-age", ageCategoryTournament.FromAge.ToString());
                        xmlAgeCategory.SetAttribute("max-age", ageCategoryTournament.ToAge.ToString());
                        xmlAgeCategory.SetAttribute("name", ageCategoryTournament.Name);
                        xmlAgeCategory.SetAttribute("id", ageCategoryTournament.Id.ToString());
                        xmlAgeCategory.InnerText = "aldersbestemt";
                        break;
                }
            }
            else
            {
                xmlAgeCategory.SetAttribute("min-age", ageCategoryTournament.FromAge.ToString());
                xmlAgeCategory.SetAttribute("max-age", ageCategoryTournament.ToAge.ToString());
                xmlAgeCategory.SetAttribute("name", ageCategoryTournament.Name);
                xmlAgeCategory.SetAttribute("id", ageCategoryTournament.Id.ToString());
                xmlAgeCategory.InnerText = ageCategoryTournament.Name;
            }
            return xmlAgeCategory;
        }

        public void CreateOrganisationElement(XmlDocument doc, XmlElement xmlHeader)
        {
            // Add Information about Organisation
            Logger.Info("Creating Organization information for the XML-file");
            var xmlOrganisation = doc.CreateElement("Organisation");
            xmlOrganisation.SetAttribute("Name", "NFF");
            var xmlText = doc.CreateTextNode("Norges Fotballforbund");
            xmlOrganisation.AppendChild(xmlText);
            xmlHeader.AppendChild(xmlOrganisation);
        }

        public XmlElement CreateTournamentMetaInfo(Tournament tournament, XmlDocument doc, List<Match> matchQuery, Match matchResult,
            XmlElement xmlRoot, XmlElement xmlDistrict)
        {
            // Create Tournament-MetaInfo node
            var xmlTournamentMetaInfo = doc.CreateElement("Tournament-MetaInfo");
            xmlTournamentMetaInfo.SetAttribute("Id", tournament.Id.ToString());
            xmlTournamentMetaInfo.SetAttribute("Name", tournament.Name);
            xmlTournamentMetaInfo.SetAttribute("Number", tournament.TournamentNumber);
            xmlTournamentMetaInfo.SetAttribute("Division", tournament.Division.ToString());

            foreach (var filteredMatch in matchQuery)
            {
                matchResult.MatchDate = filteredMatch.MatchDate;
                matchResult.TournamentRoundNumber = filteredMatch.TournamentRoundNumber;

                if (matchResult.MatchDate != null)
                {
                    xmlTournamentMetaInfo.SetAttribute("MatchDate", matchResult.MatchDate.Value.ToString(CultureInfo.InvariantCulture));
                }
                xmlTournamentMetaInfo.SetAttribute("RoundNumber", matchResult.TournamentRoundNumber.ToString());

                var docid = string.Format("T{0}R{1}_{2}{3}{4}", tournament.TournamentNumber, matchResult.TournamentRoundNumber,
                    matchResult.MatchDate.Value.Year, matchResult.MatchDate.Value.Month.ToString()
                                .PadLeft(2, '0'), matchResult.MatchDate.Value.Day.ToString()
                                    .PadLeft(2, '0'));
                    
                xmlRoot.SetAttribute("doc-id", docid);
            }

            xmlTournamentMetaInfo.AppendChild(xmlDistrict);
            return xmlTournamentMetaInfo;
        }

        public XmlElement CreateDistrictElement(Tournament tournament, XmlDocument doc)
        {
// Set the district-information
            var soccerFacade = new SoccerFacade();
            var districts = soccerFacade.GetDistricts();
            var district = (from d in districts where d.Id == tournament.DistrictId select d).Single();


            Logger.Info("About to create more information on Tournament and more");
            var xmlDistrict = doc.CreateElement("District");
            xmlDistrict.SetAttribute("Id", district.Id.ToString());

            xmlDistrict.SetAttribute("Name", district.Name);
            var xmlText = doc.CreateTextNode(district.Name);
            xmlDistrict.AppendChild(xmlText);
            return xmlDistrict;
        }

        /// <summary>
        ///     The create match structure.
        /// </summary>
        /// <param name="match">
        ///     The match.
        /// </param>
        /// <param name="doc">
        ///     The doc.
        /// </param>
        /// <param name="xmlMatches">
        ///     The xml matches.
        /// </param>
        /// <param name="renderResult">
        ///     The render result.
        /// </param>
        private void CreateMatchStructure(Match match, XmlDocument doc, XmlElement xmlMatches, bool renderResult)
        {
            Logger.DebugFormat("Creating the Match elements with ID {0} in the XML-file", match.Id);

            // Creating the match element with attribute
            var xmlMatch = doc.CreateElement("Match");
            xmlMatch.SetAttribute("Id", match.Id.ToString());
            var matchDate = Convert.ToDateTime(match.MatchDate);
            xmlMatch.SetAttribute("startdate", matchDate.ToShortDateString());
            xmlMatch.SetAttribute("starttime", matchDate.ToShortTimeString());

            if (match.HomeTeamName == string.Empty)
            {
                // ReSharper disable All
                var foo = "just here so that I can stop it!";
                // ReSharper restore All
            }

            // DONE: Need to check if match is to be created or not

            // DONE: We need to store the information about the district/Zone somewhere.

            // DONE: Create the <hometeam><name>structure</name><goals></goals></hometeam>
            var xmlHomeTeam = doc.CreateElement("HomeTeam");
            xmlHomeTeam.SetAttribute("Id", match.HomeTeamId.ToString());
            XmlNode xmlHomeTeamName = doc.CreateElement("Name");
            var xmlText = doc.CreateTextNode(match.HomeTeamName.Trim());
            xmlHomeTeamName.AppendChild(xmlText);
            xmlHomeTeam.AppendChild(xmlHomeTeamName);

            var xmlAwayTeam = doc.CreateElement("AwayTeam");
            xmlAwayTeam.SetAttribute("Id", match.AwayTeamId.ToString());
            XmlNode xmlAwayTeamName = doc.CreateElement("Name");

            // create <name> tag 
            xmlText = doc.CreateTextNode(match.AwayTeamName.Trim());
            xmlAwayTeamName.AppendChild(xmlText);
            xmlAwayTeam.AppendChild(xmlAwayTeamName);

            if (renderResult)
            {
                var matchResults = match.MatchResultList;

                if (matchResults != null)
                {
                    foreach (var result in matchResults)
                    {
                        var xmlMatchResult = doc.CreateElement("MatchResult");
                        xmlMatchResult.SetAttribute("type", result.ResultTypeName);
                        if (result.HomeTeamGoals != null)
                        {
                            xmlMatchResult.SetAttribute("hometeam", result.HomeTeamGoals.Value.ToString());
                        }

                        if (result.AwayTeamGoals != null)
                        {
                            xmlMatchResult.SetAttribute("awayteam", result.AwayTeamGoals.Value.ToString());
                        }

                        xmlMatch.AppendChild(xmlMatchResult);
                    }
                }

                XmlElement xmlGoals;
                if (match.AwayTeamGoals != null)
                {
                    var awayTeamGoals = match.AwayTeamGoals.ToString();
                    xmlGoals = doc.CreateElement("Totalgoals");
                    xmlText = doc.CreateTextNode(awayTeamGoals);
                    xmlGoals.AppendChild(xmlText);

                    xmlAwayTeam.AppendChild(xmlGoals);
                }

                if (match.HomeTeamGoals != null)
                {
                    var homeTeamGoals = match.HomeTeamGoals.ToString();

                    xmlText = doc.CreateTextNode(homeTeamGoals);
                    xmlGoals = doc.CreateElement("Totalgoals");
                    xmlGoals.AppendChild(xmlText);

                    xmlHomeTeam.AppendChild(xmlGoals);
                }

                var xmlMatchDate = doc.CreateElement("MatchDate");
                var xmlMatchTime = doc.CreateElement("MatchTime");

                if (match.MatchDate.Value.Year < 1970)
                {
                    var justDate = match.MatchDate;
                    var justTime = match.MatchDate;

                    // xmlMatchDate.SetAttribute("Date", match.MatchStartDate.Value.ToString());
                    xmlMatchDate.SetAttribute("Date", justDate.Value.ToShortDateString());

                    // xmlMatchTime.SetAttribute("Time", match.MatchStartDate.Value.ToString());
                    xmlMatchTime.SetAttribute("Time", justTime.Value.ToLongTimeString());

                    // Add readable value in text-node
                    xmlText = doc.CreateTextNode(match.MatchDate.Value.ToLongTimeString());
                    xmlMatchTime.AppendChild(xmlText);

                    // Add readable value in text-node
                    xmlText = doc.CreateTextNode(match.MatchDate.Value.ToLongDateString());
                    xmlMatchDate.AppendChild(xmlText);
                }
                else
                {
                    xmlText = doc.CreateTextNode("No time");
                    xmlMatchTime.AppendChild(xmlText);

                    xmlText = doc.CreateTextNode("No Date");
                    xmlMatchDate.AppendChild(xmlText);
                }

                xmlMatch.AppendChild(xmlMatchDate);
                xmlMatch.AppendChild(xmlMatchTime);

                var xmlPlayers = doc.CreateElement("Players");
                XmlElement xmlPlayer;
                XmlElement xmlPlayerName;
                if (match.HomeTeamPlayers.Any())
                {
                    this.Model.MatchFacts = true;
                    xmlPlayers.SetAttribute("Clubid", match.HomeTeamId.ToString());

                    foreach (var player in match.HomeTeamPlayers)
                    {
                        xmlPlayer = doc.CreateElement("Player");
                        xmlPlayerName = doc.CreateElement("PlayerName");
                        var firstName = string.Empty;
                        var lastName = string.Empty;

                        xmlPlayer.SetAttribute("id", player.Id.ToString());

                        if (player.Position != null)
                        {
                            xmlPlayer.SetAttribute("position", player.Position);
                        }

                        if (player.FirstName != null)
                        {
                            xmlPlayerName.SetAttribute("FirstName", player.FirstName);
                            firstName = player.FirstName;
                        }

                        if (player.LastName != null)
                        {
                            xmlPlayerName.SetAttribute("LastName", player.LastName);
                            lastName = player.LastName;
                        }

                        xmlText = doc.CreateTextNode(firstName + " " + lastName);

                        xmlPlayerName.AppendChild(xmlText);
                        xmlPlayer.AppendChild(xmlPlayerName);

                        xmlPlayers.AppendChild(xmlPlayer);
                    }
                }

                xmlMatch.AppendChild(xmlPlayers);

                if (match.AwayTeamPlayers.Any())
                {
                    xmlPlayers = doc.CreateElement("Players");
                    xmlPlayers.SetAttribute("Clubid", match.AwayTeamId.ToString());

                    foreach (var player in match.AwayTeamPlayers)
                    {
                        if (player.FirstName != null)
                        {
                            xmlPlayer = doc.CreateElement("Player");
                            xmlPlayer.SetAttribute("id", player.Id.ToString());

                            if (player.Position != null)
                            {
                                xmlPlayer.SetAttribute("position", player.Position);
                            }

                            xmlPlayerName = doc.CreateElement("PlayerName");
                            xmlPlayerName.SetAttribute("FirstName", player.FirstName);
                            xmlPlayerName.SetAttribute("LastName", player.LastName);

                            xmlText = doc.CreateTextNode(player.FirstName + " " + player.LastName);

                            xmlPlayerName.AppendChild(xmlText);
                            xmlPlayer.AppendChild(xmlPlayerName);

                            xmlPlayers.AppendChild(xmlPlayer);
                        }
                    }

                    xmlMatch.AppendChild(xmlPlayers);
                }

                // Can we get events also
                if (match.MatchEventList != null)
                {
                    // Has been moved outside foreach so that the structure is like this:
                    // events
                    // event
                    // eventtype
                    // player
                    // and so on...
                    // Fix so that player has same structure as player in match/team structure...
                    var xmlEvents = doc.CreateElement("Events");
                    foreach (var events in match.MatchEventList)
                    {
                        var xmlEvent = doc.CreateElement("Event");
                        xmlEvent.SetAttribute("id", events.MatchEventId.ToString());
                        xmlEvent.SetAttribute("TeamId", events.TeamId.ToString());

                        var xmlEventType = doc.CreateElement("EventType");
                        switch (events.MatchEventType)
                        {
                            case "Bytte ut":
                                xmlEventType.SetAttribute("name", "bytte");
                                xmlEventType.SetAttribute("value", "ut");
                                xmlEventType.SetAttribute("guid", events.ConnectedToEvent);
                                xmlEventType.SetAttribute("minute", events.Minute.ToString());
                                break;

                            case "Bytte inn":
                                xmlEventType.SetAttribute("name", "bytte");
                                xmlEventType.SetAttribute("value", "inn");
                                xmlEventType.SetAttribute("guid", events.ConnectedToEvent);
                                xmlEventType.SetAttribute("minute", events.Minute.ToString());
                                break;

                            case "Straffem�l":
                                xmlEventType.SetAttribute("name", "goal");
                                xmlEventType.SetAttribute("value", "Straffem�l");
                                xmlEventType.SetAttribute("minute", events.Minute.ToString());
                                xmlEventType.SetAttribute("homegoal", events.HomeGoals.HasValue ? events.HomeGoals.ToString() : "0");
                                xmlEventType.SetAttribute("awaygoal", events.AwayGoals.HasValue ? events.AwayGoals.ToString() : "0");
                                break;

                            case "Spillem�l":
                                xmlEventType.SetAttribute("name", "goal");
                                xmlEventType.SetAttribute("value", "Spillem�l");
                                xmlEventType.SetAttribute("minute", events.Minute.ToString());
                                xmlEventType.SetAttribute("homegoal", events.HomeGoals.HasValue ? events.HomeGoals.ToString() : "0");
                                xmlEventType.SetAttribute("awaygoal", events.AwayGoals.HasValue ? events.AwayGoals.ToString() : "0");
                                break;

                            case "Advarsel":
                                xmlEventType.SetAttribute("name", "warning");
                                xmlEventType.SetAttribute("value", events.SecondYellowCard ? "Utvisning" : "Advarsel");
                                xmlEventType.SetAttribute("minute", events.Minute.ToString());
                                break;
                        }

                        var xmlEventPlayer = doc.CreateElement("Player");
                        xmlEventPlayer.SetAttribute("id", events.PlayerId.ToString());

                        var xmlEventPlayerName = doc.CreateElement("Name");
                        if (events.PlayerName != null)
                        {
                            xmlEventPlayerName.SetAttribute("full", events.PlayerName);
                            xmlText = doc.CreateTextNode(events.PlayerName);
                        }
                        else
                        {
                            xmlText = doc.CreateTextNode(string.Empty);
                        }

                        xmlEventPlayerName.AppendChild(xmlText);
                        xmlEventPlayer.AppendChild(xmlEventPlayerName);
                        xmlEventType.AppendChild(xmlEventPlayer);
                        xmlEvent.AppendChild(xmlEventType);
                        xmlEvents.AppendChild(xmlEvent);

                        // Adding the events to the Match element
                        xmlMatch.AppendChild(xmlEvents);

                        // Logging the different values.
                        Logger.Debug("Event.AwayGoals: " + events.AwayGoals);
                        Logger.Debug("Event.Comment: " + events.Comment);
                        Logger.Debug("Event.ConnectedToEvent: " + events.ConnectedToEvent);
                        Logger.Debug("Event.HomeGoals: " + events.HomeGoals);
                        Logger.Debug("Event.MatchEventId: " + events.MatchEventId);

                        Logger.Debug("Event.MatchEventType: " + events.MatchEventType);
                        Logger.Debug("Event.MatchEventTypeId: " + events.MatchEventTypeId);

                        Logger.Debug("Event.Minute: " + events.Minute);
                        Logger.Debug("Event.PersonInfoHidden: " + events.PersonInfoHidden);
                        Logger.Debug("Event.PlayerId: " + events.PlayerId);
                        Logger.Debug("Event.PlayerName: " + events.PlayerName);

                        Logger.Debug("Event.SecondYellowCard: " + events.SecondYellowCard);
                        Logger.Debug("Event.TeamId: " + events.TeamId);
                        Logger.Debug("Event.TeamName: " + events.TeamName);
                    }
                }

                Logger.Debug("ID " + match.Id + " Matches: " + match.HomeTeamName + " - " + match.AwayTeamName);
            }

            // sb.AppendLine(Line);
            var xmlStadium = doc.CreateElement("Stadium");

            if (match.VenueName != null)
            {
                xmlStadium.SetAttribute("attendance", match.Spectators.ToString());

                // sb.AppendLine("Stadion: " + match.StadiumName);
                xmlText = doc.CreateTextNode(match.VenueName);
                xmlStadium.AppendChild(xmlText);
            }

            var xmlReferees = doc.CreateElement("Referees");

            if (match.Referees.Count > 0)
            {
                // sb.AppendLine("Dommer: " + match.Referees.);
                var referees = match.Referees.ToList();

                foreach (var referee in referees)
                {
                    if (referee.FirstName != null)
                    {
                        if (referee.FirstName.Trim() != string.Empty)
                        {
                            var xmlReferee = doc.CreateElement("Referee");
                            xmlReferee.SetAttribute("FirstName", referee.FirstName);
                            xmlReferee.SetAttribute("LastName", referee.LastName);
                            xmlReferee.SetAttribute("Type", referee.RefereeType);
                            xmlReferee.SetAttribute("Club", referee.ClubName);
                            xmlText = doc.CreateTextNode(referee.FirstName + " " + referee.LastName);
                            xmlReferee.AppendChild(xmlText);

                            xmlReferees.AppendChild(xmlReferee);
                        }
                    }
                }
            }

            xmlMatch.AppendChild(xmlReferees);

            xmlMatch.AppendChild(xmlHomeTeam);
            xmlMatch.AppendChild(xmlAwayTeam);
            xmlMatch.AppendChild(xmlStadium);
            xmlMatches.AppendChild(xmlMatch);
        }
    }
}