﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nff.Repositories.Interfaces;

namespace NTB.SportsData.Components.Nff.Repositories
{
    public class TournamentRepository : ITournamentRepository
    {
        /// <summary>
        ///     Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(TournamentRepository));


        public Tournament GetTournamentByTournamentId(int tournamentId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    var sqlCommand = new SqlCommand("Service_GetTournamentByTournamentId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", tournamentId));

                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    var tournament = new Tournament();
                    while (sqlDataReader.Read())
                    {
                        if (sqlDataReader["TournamentId"] != DBNull.Value)
                        {
                            tournament.Id = Convert.ToInt32(sqlDataReader["TournamentId"]);
                        }

                        if (sqlDataReader["DistrictId"] != DBNull.Value)
                        {
                            tournament.DistrictId = Convert.ToInt32(sqlDataReader["DistrictId"]);
                        }

                        if (sqlDataReader["SeasonId"] != DBNull.Value)
                        {
                            tournament.SeasonId = Convert.ToInt32(sqlDataReader["SeasonId"]);
                        }

                        if (sqlDataReader["SportId"] != DBNull.Value)
                        {
                            tournament.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                        }

                        if (sqlDataReader["TournamentName"] != DBNull.Value)
                        {
                            tournament.Name = sqlDataReader["TournamentName"].ToString();
                        }

                        if (sqlDataReader["Push"] != DBNull.Value)
                        {
                            tournament.Push = Convert.ToBoolean(sqlDataReader["Push"]);
                        }

                        if (sqlDataReader["AgeCategoryId"] != DBNull.Value)
                        {
                            tournament.AgeCategoryId = Convert.ToInt32(sqlDataReader["AgeCategoryId"]);
                        }

                        if (sqlDataReader["GenderId"] != DBNull.Value)
                        {
                            tournament.GenderId = Convert.ToInt32(sqlDataReader["GenderId"]);
                        }

                        if (sqlDataReader["TournamentTypeId"] != DBNull.Value)
                        {
                            tournament.TournamentTypeId = Convert.ToInt32(sqlDataReader["TournamentTypeId"]);
                        }

                        if (sqlDataReader["TournamentNumber"] != DBNull.Value)
                        {
                            tournament.TournamentNumber = sqlDataReader["TournamentNumber"].ToString();
                        }

                        if (sqlDataReader["AgeCategoryDefinitionId"] != DBNull.Value)
                        {
                            tournament.AgeCategoryDefinitionId =
                                Convert.ToInt32(sqlDataReader["AgeCategoryDefinitionId"]);
                        }
                    }

                    return tournament;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return null;
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public List<Tournament> GetAllTournaments()
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    var sqlCommand = new SqlCommand("Service_GetTournamentByTournamentId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    
                    var sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    var tournaments = new List<Tournament>();
                    while (sqlDataReader.Read())
                    {
                        var tournament = new Tournament();
                        if (sqlDataReader["TournamentId"] != DBNull.Value)
                        {
                            tournament.Id = Convert.ToInt32(sqlDataReader["TournamentId"]);
                        }

                        if (sqlDataReader["DistrictId"] != DBNull.Value)
                        {
                            tournament.DistrictId = Convert.ToInt32(sqlDataReader["DistrictId"]);
                        }

                        if (sqlDataReader["SeasonId"] != DBNull.Value)
                        {
                            tournament.SeasonId = Convert.ToInt32(sqlDataReader["SeasonId"]);
                        }

                        if (sqlDataReader["SportId"] != DBNull.Value)
                        {
                            tournament.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                        }

                        if (sqlDataReader["TournamentName"] != DBNull.Value)
                        {
                            tournament.Name = sqlDataReader["TournamentName"].ToString();
                        }

                        if (sqlDataReader["Push"] != DBNull.Value)
                        {
                            tournament.Push = Convert.ToBoolean(sqlDataReader["Push"]);
                        }

                        if (sqlDataReader["AgeCategoryId"] != DBNull.Value)
                        {
                            tournament.AgeCategoryId = Convert.ToInt32(sqlDataReader["AgeCategoryId"]);
                        }

                        if (sqlDataReader["GenderId"] != DBNull.Value)
                        {
                            tournament.GenderId = Convert.ToInt32(sqlDataReader["GenderId"]);
                        }

                        if (sqlDataReader["TournamentTypeId"] != DBNull.Value)
                        {
                            tournament.TournamentTypeId = Convert.ToInt32(sqlDataReader["TournamentTypeId"]);
                        }

                        if (sqlDataReader["TournamentNumber"] != DBNull.Value)
                        {
                            tournament.TournamentNumber = sqlDataReader["TournamentNumber"].ToString();
                        }

                        if (sqlDataReader["AgeCategoryDefinitionId"] != DBNull.Value)
                        {
                            tournament.AgeCategoryDefinitionId =
                                Convert.ToInt32(sqlDataReader["AgeCategoryDefinitionId"]);
                        }

                        tournaments.Add(tournament);
                    }

                    return tournaments;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return null;
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void InsertTournament(Tournament tournament)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    var sqlCommand = new SqlCommand("Service_GetTournamentByTournamentId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", tournament.Id));
                    sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", tournament.DistrictId));
                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", tournament.SeasonId));
                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", tournament.SportId));
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentName", tournament.Name));
                    sqlCommand.Parameters.Add(new SqlParameter("@Push", tournament.Push));
                    sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryId", tournament.AgeCategoryId));
                    sqlCommand.Parameters.Add(new SqlParameter("@GenderId", tournament.GenderId));
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentTypeId", tournament.TournamentTypeId));
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentNumber", tournament.TournamentNumber));

                    sqlCommand.ExecuteNonQuery();

                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }
    }
}
