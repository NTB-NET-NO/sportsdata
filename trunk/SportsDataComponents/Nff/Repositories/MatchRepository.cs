﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nff.Repositories.Interfaces;

namespace NTB.SportsData.Components.Nff.Repositories
{
    public class MatchRepository : IMatchRepository
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(MunicipalityRepository));

        /// <summary>
        ///     The check missing matches.
        /// </summary>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        // public List<int> CheckMissingMatches()
        public List<int> GetListOfMatchesWithoutResults()
        {
            var missingMatches = new List<int>();
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                var sqlCommand = new SqlCommand("Service_GetMissingMatches", sqlConnection);

                sqlCommand.Parameters.Clear();

                sqlCommand.Parameters.Add(new SqlParameter("@MatchDate", DateTime.Today));

                sqlCommand.CommandType = CommandType.StoredProcedure;

                var reader = sqlCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        missingMatches.Add(Convert.ToInt32(reader["Matchid"]));
                    }
                }

                return missingMatches;
            }
        }


        public Match GetMatchByMatchAndSportId(int matchId, int sportId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("Service_GetMatchByMatchId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@MatchId", matchId));

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                    // opening the first connection
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    // Creating the SQL-reader.
                    var sqlDataReader = sqlCommand.ExecuteReader();

                    var match = new Match();
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            if (sqlDataReader["MatchId"] != DBNull.Value)
                            {
                                match.Id = Convert.ToInt32(sqlDataReader["MatchId"]);
                            }

                            if (sqlDataReader["HomeTeam"] != DBNull.Value)
                            {
                                match.HomeTeamName = sqlDataReader["HomeTeam"].ToString();
                            }

                            if (sqlDataReader["AwayTeam"] != DBNull.Value)
                            {
                                match.AwayTeamName = sqlDataReader["AwayTeam"].ToString();
                            }

                            if (sqlDataReader["MatchDate"] != DBNull.Value)
                            {
                                match.MatchDate = Convert.ToDateTime(sqlDataReader["MatchDate"]);
                            }

                            if (sqlDataReader["MatchStartTime"] != DBNull.Value)
                            {
                                match.MatchStartTime = Convert.ToInt32(sqlDataReader["MatchStartTime"]);
                            }

                            if (sqlDataReader["MatchEndTime"] != DBNull.Value)
                            {
                                match.MatchEndTime = Convert.ToInt32(sqlDataReader["MatchEndTime"]);
                            }

                            if (sqlDataReader["SportId"] != DBNull.Value)
                            {
                                match.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                            }

                            if (sqlDataReader["TournamentId"] != DBNull.Value)
                            {
                                match.TournamentId = Convert.ToInt32(sqlDataReader["TournamentId"]);
                            }

                            if (sqlDataReader["Downloaded"] != DBNull.Value)
                            {
                                match.Downloaded = Convert.ToBoolean(sqlDataReader["Downloaded"]);
                            }
                        }
                    }
                    return match;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return null;
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        ///     The insert matches.
        /// </summary>
        /// <param name="match">
        ///     The match to be inserted
        /// </param>
        /// <param name="sportId">
        ///     The sport Id.
        /// </param>
        /// <returns>
        ///     The <see cref="int" />.
        /// </returns>
        public void InsertMatch (Match match, int sportId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    var tournamentId = match.TournamentId;
                    const int downloaded = 0;

                    var matchDate = match.MatchDate;
                    var matchTime = 0;
                    if (matchDate != null)
                    {
                        var matchTimeString = matchDate.Value.ToShortTimeString();
                        if (matchTimeString.Contains(":"))
                        {
                            matchTime = Convert.ToInt32(matchTimeString.Replace(":", ""));
                        } else if (matchTimeString.Contains("."))
                        {
                            matchTime = Convert.ToInt32(matchTimeString.Replace(".", ""));
                        }
                        else
                        {
                            matchTime = Convert.ToInt32(matchTimeString);
                        }
                    }
                    
                    var sqlCommand = new SqlCommand("Service_InsertMatches", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Clear();

                    sqlCommand.Parameters.Add(new SqlParameter("@MatchId", match.Id));

                    sqlCommand.Parameters.Add(new SqlParameter("@HomeTeam", match.HomeTeamName));

                    sqlCommand.Parameters.Add(new SqlParameter("@HomeTeamId", match.HomeTeamId));

                    sqlCommand.Parameters.Add(new SqlParameter("@AwayTeam", match.AwayTeamName));

                    sqlCommand.Parameters.Add(new SqlParameter("@AwayTeamId", match.AwayTeamId));

                    sqlCommand.Parameters.Add(new SqlParameter("@Date", matchDate));

                    sqlCommand.Parameters.Add(new SqlParameter("@Time", matchTime));

                    sqlCommand.Parameters.Add(new SqlParameter("@Downloaded", downloaded));

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", tournamentId));

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                    sqlCommand.Parameters.Add(new SqlParameter("@VenueId", match.VenueUnitId));

                    sqlCommand.Parameters.Add(new SqlParameter("@VenueName", match.VenueName));

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentRoundNumber", match.TournamentRoundNumber));

                    sqlCommand.ExecuteNonQuery();

                    // DONE: Move this closing to outside the loop
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }

                }
                catch (SqlException sqlexception)
                {
                    Logger.Error(sqlexception);
                    throw;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    throw;
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }
                }
            }
        }

        public void Updatematch(Match match, int sportId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    var tournamentId = match.TournamentId;
                    const int downloaded = 0;

                    var matchDate = match.MatchDate;
                    var matchTime = 0;
                    if (matchDate != null)
                    {
                        matchTime = Convert.ToInt32(matchDate.Value.ToShortTimeString().Replace(":", ""));
                    }
                    
                    var sqlCommand = new SqlCommand("Service_UpdateMatches", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Clear();

                    sqlCommand.Parameters.Add(new SqlParameter("@MatchId", match.Id));

                    sqlCommand.Parameters.Add(new SqlParameter("@HomeTeam", match.HomeTeamName));

                    sqlCommand.Parameters.Add(new SqlParameter("@AwayTeam", match.AwayTeamName));

                    sqlCommand.Parameters.Add(new SqlParameter("@Date", matchDate));

                    sqlCommand.Parameters.Add(new SqlParameter("@Time", matchTime));

                    sqlCommand.Parameters.Add(new SqlParameter("@Downloaded", downloaded));

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", tournamentId));

                    sqlCommand.ExecuteNonQuery();

                    // DONE: Move this closing to outside the loop
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }
                catch (SqlException sqlexception)
                {
                    Logger.Error(sqlexception);
                    throw;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    throw;
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }
                }
            }
        }

        /// <summary>
        /// Inserts matches by calling insertMatch method
        /// </summary>
        /// <param name="matches">
        /// The list of matches to be added to the database
        /// </param>
        /// <param name="sportId">
        /// The sport id
        /// </param>
        public void InsertMatches(List<Match> matches, int sportId)
        {
            foreach (var match in matches)
            {
                this.InsertMatch(match, sportId);
            }
        }

        /// <summary>
        ///     Update match not found is used when we have checked the match, but still cannot get information from NFF
        /// </summary>
        /// <param name="match">
        ///     Match object
        /// </param>
        /// <param name="tournament">
        ///     Tournament object
        /// </param>
        public void UpdateMatchNotFound(Match match, Tournament tournament)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    var sqlCommand = new SqlCommand("Service_UpdateMissingMatch", sqlConnection);

                    // DONE: When connection is moved, add this code
                    sqlCommand.Parameters.Clear();

                    sqlCommand.Parameters.Add(new SqlParameter("@MatchId", match.Id));

                    sqlCommand.Parameters.Add(new SqlParameter("@LastChecked", DateTime.Today));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.ExecuteNonQuery();

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        /// <summary>
        ///     The insert match not found.
        /// </summary>
        /// <param name="match">
        ///     The match.
        /// </param>
        /// <param name="tournament">
        ///     The tournament.
        /// </param>
        public void InsertMatchNotFound(Match match, Tournament tournament)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            
            {
                try
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    var sqlCommand = new SqlCommand("Service_InsertMissingMatch", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    // DONE: When connection is moved, add this code
                    sqlCommand.Parameters.Clear();

                    sqlCommand.Parameters.Add(new SqlParameter("@MatchId", match.Id));

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", match.TournamentId));

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentNo", tournament.TournamentNumber));

                    sqlCommand.Parameters.Add(new SqlParameter("@MatchDate", match.MatchDate));

                    sqlCommand.Parameters.Add(new SqlParameter("@LastChecked", DateTime.Today));

                    sqlCommand.ExecuteNonQuery();

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    // DONE: Move this closing to outside the loop
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }
            }
            
        }

        /// <summary>
        ///     The check all missing matches.
        /// </summary>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<int> GetAllMissingMatches()
        {
            
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    var sqlCommand = new SqlCommand("Service_GetMissingMatches", sqlConnection);

                    sqlCommand.Parameters.Clear();

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();

                    if (!reader.HasRows)
                    {
                        return null;
                    }

                    var missingMatches = new List<int>();
                    while (reader.Read())
                    {
                        missingMatches.Add(Convert.ToInt32(reader["Matchid"]));
                    }

                    return missingMatches;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return null;
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void UpdateSportsDataInputMatches(Match match)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    var sqlCommand = new SqlCommand("Service_UpdateSportsDataInputMatches", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    // DONE: When connection is moved, add this code
                    sqlCommand.Parameters.Clear();

                    sqlCommand.Parameters.Add(new SqlParameter("@MatchId", match.Id));

                    sqlCommand.Parameters.Add(new SqlParameter("@HomeTeamGoals", match.HomeTeamGoals));

                    sqlCommand.Parameters.Add(new SqlParameter("@AwayTeamGoals", match.AwayTeamGoals));

                    sqlCommand.ExecuteNonQuery();

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    // DONE: Move this closing to outside the loop
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }
    }
}
