﻿using System.Collections.Generic;
using NTB.SportsData.Classes.Classes;

namespace NTB.SportsData.Components.Nff.Repositories.Interfaces
{
    public interface IDistrictRepository
    {
        List<District> GetAllDistricts();

        District GetDistrictById(int districtId);

        void InsertDistrict(District district);
    }
}
