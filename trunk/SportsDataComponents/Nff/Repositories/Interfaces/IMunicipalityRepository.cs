﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsData.Classes.Classes;


namespace NTB.SportsData.Components.Nff.Repositories.Interfaces
{
    public interface IMunicipalityRepository
    {
        Municipality GetMunicipalityById(int municipalityId);

        List<Municipality> GetAllMunicipalities();

        List<Municipality> GetAllMunicipalitiesBySportId(int sportId);

        void InsertMunicipality(Municipality municipality);
    }
}
