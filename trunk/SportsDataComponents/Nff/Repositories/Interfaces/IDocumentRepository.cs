﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsData.Components.Nff.Models;

namespace NTB.SportsData.Components.Nff.Repositories.Interfaces
{
    public interface IDocumentRepository
    {
        SportsDataDocument GetDocumentByTournamentId(int tournamentId);

        SportsDataDocument GetDocumentById(int documentId);

        void UpdateDocument(SportsDataDocument dataDocument);

        void InsertDocument(SportsDataDocument dataDocument);

        void DeleteDocument(SportsDataDocument dataDocument);
    }
}
