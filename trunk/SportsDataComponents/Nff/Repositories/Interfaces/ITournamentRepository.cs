﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsData.Classes.Classes;

namespace NTB.SportsData.Components.Nff.Repositories.Interfaces
{
    public interface ITournamentRepository
    {
        Tournament GetTournamentByTournamentId(int tournamentId);

        List<Tournament> GetAllTournaments();

        void InsertTournament(Tournament tournament);
    }
}
