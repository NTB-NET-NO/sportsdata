﻿using System.Collections.Generic;
using NTB.SportsData.Classes.Classes;

namespace NTB.SportsData.Components.Nff.Repositories.Interfaces
{
    public interface IMatchRepository
    {
        Match GetMatchByMatchAndSportId(int matchId, int sportId);

        void InsertMatch(Match match, int sportId);

        void Updatematch(Match match, int sportId);

        void InsertMatches(List<Match> matches, int sportId);

        void InsertMatchNotFound(Match match, Tournament tournament);

        void UpdateMatchNotFound(Match match, Tournament tournament);

        List<int> GetListOfMatchesWithoutResults();

        List<int> GetAllMissingMatches();

        void UpdateSportsDataInputMatches(Match match);
    }
}
