﻿namespace NTB.SportsData.Components.Nff.Repositories.Interfaces
{
    interface ISystemRepository
    {
        bool CheckLastTableCheck();
    }
}
