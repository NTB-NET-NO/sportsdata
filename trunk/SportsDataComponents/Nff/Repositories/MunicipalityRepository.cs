﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nff.Repositories.Interfaces;

using log4net;

namespace NTB.SportsData.Components.Nff.Repositories
{
    public class MunicipalityRepository : IMunicipalityRepository
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(MunicipalityRepository));

        public Municipality GetMunicipalityById(int municipalityId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    // We are checking if the current ID and the district ID is in the database
                    Logger.Info("Checking Municipality database table");

                    // Using Stored Procedure: Service_GetMunicipalities
                    var sqlCommand = new SqlCommand("[Service_GetMunicipalityById]", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@MunicipalityId", municipalityId));

                    var sqlDataReader = sqlCommand.ExecuteReader();
                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    var municipality = new Municipality();

                    while (sqlDataReader.Read())
                    {
                        if (sqlDataReader["MunicipalityId"] != DBNull.Value)
                        {
                            municipality.Id = Convert.ToInt32(sqlDataReader["MunicipalityId"]);
                        }

                        if (sqlDataReader["MunicipalityName"] != DBNull.Value)
                        {
                            municipality.Name = sqlDataReader["MunicipalityName"].ToString();
                        }

                        if (sqlDataReader["DistrictId"] != DBNull.Value)
                        {
                            municipality.DistrictId = Convert.ToInt32(sqlDataReader["DistrictId"]);
                        }

                        if (sqlDataReader["SportId"] != DBNull.Value)
                        {
                            municipality.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                        }
                    }

                    return municipality;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return null;
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public List<Municipality> GetAllMunicipalities()
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    // We are checking if the current ID and the district ID is in the database
                    Logger.Info("Checking Municipality database table");

                    // Using Stored Procedure: Service_GetMunicipalities
                    var sqlCommand = new SqlCommand("[Service_GetMunicipalityById]", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    var sqlDataReader = sqlCommand.ExecuteReader();
                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    var municipalities = new List<Municipality>();

                    while (sqlDataReader.Read())
                    {
                        var municipality = new Municipality();
                        if (sqlDataReader["MunicipalityId"] != DBNull.Value)
                        {
                            municipality.Id = Convert.ToInt32(sqlDataReader["MunicipalityId"]);
                        }

                        if (sqlDataReader["MunicipalityName"] != DBNull.Value)
                        {
                            municipality.Name = sqlDataReader["MunicipalityName"].ToString();
                        }

                        if (sqlDataReader["DistrictId"] != DBNull.Value)
                        {
                            municipality.DistrictId = Convert.ToInt32(sqlDataReader["DistrictId"]);
                        }

                        if (sqlDataReader["SportId"] != DBNull.Value)
                        {
                            municipality.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                        }

                        municipalities .Add(municipality);
                    }

                    return municipalities;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return null;
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public List<Municipality> GetAllMunicipalitiesBySportId(int sportId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    // We are checking if the current ID and the district ID is in the database
                    Logger.Info("Checking Municipality database table");

                    // Using Stored Procedure: Service_GetMunicipalities
                    var sqlCommand = new SqlCommand("[Service_GetMunicipalitiesBySportId]", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@MunicipalityId", sportId));

                    var sqlDataReader = sqlCommand.ExecuteReader();
                    if (!sqlDataReader.HasRows)
                    {
                        return null;
                    }

                    var municipalities = new List<Municipality>();

                    while (sqlDataReader.Read())
                    {
                        var municipality = new Municipality();
                        if (sqlDataReader["MunicipalityId"] != DBNull.Value)
                        {
                            municipality.Id = Convert.ToInt32(sqlDataReader["MunicipalityId"]);
                        }

                        if (sqlDataReader["MunicipalityName"] != DBNull.Value)
                        {
                            municipality.Name = sqlDataReader["MunicipalityName"].ToString();
                        }

                        if (sqlDataReader["DistrictId"] != DBNull.Value)
                        {
                            municipality.DistrictId = Convert.ToInt32(sqlDataReader["DistrictId"]);
                        }

                        if (sqlDataReader["SportId"] != DBNull.Value)
                        {
                            municipality.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                        }

                        municipalities.Add(municipality);
                    }

                    return municipalities;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return null;
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void InsertMunicipality(Municipality municipality)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    // We are checking if the current ID and the district ID is in the database
                    Logger.Info("Checking Municipality database table");

                    // Using Stored Procedure: Service_GetMunicipalities
                    var sqlCommand = new SqlCommand("[Service_GetMunicipalitiesBySportId]", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@MunicipalityId", municipality.Id));
                    sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", municipality.DistrictId));
                    sqlCommand.Parameters.Add(new SqlParameter("@MunicipalityName", municipality.Name));
                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", municipality.SportId));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }
    }
}
