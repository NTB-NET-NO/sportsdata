﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using log4net;
using NTB.SportsData.Components.Nff.Models;
using NTB.SportsData.Components.Nff.Repositories.Interfaces;

namespace NTB.SportsData.Components.Nff.Repositories
{
    public class DocumentRepository : IDocumentRepository
    {
        /// <summary>
        ///     Static logger
        /// </summary>
        public static readonly ILog Logger = LogManager.GetLogger(typeof(DocumentRepository));

        public SportsDataDocument GetDocumentByTournamentId(int tournamentId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    Logger.Info("Get Documents based by tournament id from database");

                    var sqlCommand = new SqlCommand("Service_GetDocumentByTournamentId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", tournamentId));

                    var reader = sqlCommand.ExecuteReader();

                    if (!reader.HasRows)
                    {
                        return null;
                    }

                    var dataDocument = new SportsDataDocument();
                    while (reader.Read())
                    {
                        
                        if (reader["DocumentId"] != DBNull.Value)
                        {
                            dataDocument.Id = Convert.ToInt32(reader["DocumentId"]);
                        }

                        if (reader["DocumentName"] != DBNull.Value)
                        {
                            dataDocument.Name = reader["DocumentName"].ToString();
                        }

                        if (reader["DocumentVersion"] != DBNull.Value)
                        {
                            dataDocument.Version = Convert.ToInt32(reader["DocumentVersion"]);
                        }

                        if (reader["DocumentCreated"] != DBNull.Value)
                        {
                            dataDocument.CreatedDateTime = Convert.ToDateTime(reader["DocumentCreated"]);
                        }

                        if (reader["TournamentId"] != DBNull.Value)
                        {
                            dataDocument.TournamentId = Convert.ToInt32(reader["TournamentId"]);
                        }

                        if (reader["SportId"] != DBNull.Value)
                        {
                            dataDocument.SportId = Convert.ToInt32(reader["SportId"]);
                        }
                    }

                    return dataDocument;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return null;
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public SportsDataDocument GetDocumentById(int documentId)
        {
            throw new NotImplementedException();
        }

        public void UpdateDocument(SportsDataDocument dataDocument)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    Logger.Info("Update Documents in database");

                    var sqlCommand = new SqlCommand("Service_UpdateDocument", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentVersion", dataDocument.Version));
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentCreated", dataDocument.CreatedDateTime));
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", dataDocument.TournamentId));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    throw;
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void InsertDocument(SportsDataDocument dataDocument)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    Logger.Info("Insert Documents into database");

                    var sqlCommand = new SqlCommand("Service_InsertDocument", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", dataDocument.TournamentId));

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", dataDocument.SportId));

                    // Setting the version to 1
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentVersion", dataDocument.Version));

                    // Setting the DocumentName
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentName", dataDocument.Name));

                    // Setting the DocumentName
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentCreated", dataDocument.CreatedDateTime));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    throw;
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void DeleteDocument(SportsDataDocument dataDocument)
        {
            throw new NotImplementedException();
        }
    }
}
