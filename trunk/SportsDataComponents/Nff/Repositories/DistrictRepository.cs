﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nff.Repositories.Interfaces;

namespace NTB.SportsData.Components.Nff.Repositories
{
    public class DistrictRepository : IDistrictRepository
    {
        /// <summary>
        ///     Static logger
        /// </summary>
        public static readonly ILog Logger = LogManager.GetLogger(typeof(DistrictRepository));

        public List<District> GetAllDistricts()
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    Logger.Info("Get all Districts from database");

                    var sqlCommand = new SqlCommand("Service_GetDistricts", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    var reader = sqlCommand.ExecuteReader();

                    if (!reader.HasRows)
                    {
                        return null;
                    }

                    var districts = new List<District>();
                    while (reader.Read())
                    {
                        var district = new District();
                        if (reader["DistrictId"] != DBNull.Value)
                        {
                            district.Id = Convert.ToInt32(reader["DistrictId"]);
                        }

                        if (reader["DistrictName"] != DBNull.Value)
                        {
                            district.Name = reader["DistrictName"].ToString();
                        }
                    }

                    return districts;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return null;
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public District GetDistrictById(int districtId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    Logger.Info("Checking Districts database table");

                    var sqlCommand = new SqlCommand("[Service_GetDistrictById]", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", districtId));

                    var reader = sqlCommand.ExecuteReader();

                    if (!reader.HasRows)
                    {
                        return null;
                    }

                    var district = new District();
                    while (reader.Read())
                    {
                        if (reader["DistrictId"] != DBNull.Value)
                        {
                            district.Id = Convert.ToInt32(reader["DistrictId"]);
                        }

                        if (reader["DistrictName"] != DBNull.Value)
                        {
                            district.Name = reader["DistrictName"].ToString();
                        }

                        if (reader["SportId"] != DBNull.Value)
                        {
                            district.SportId = Convert.ToInt32(reader["SportId"]);
                        }
                    }

                    return district;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return null;
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void InsertDistrict(District district)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    Logger.Info("Inserts district into database");

                    var sqlCommand = new SqlCommand("[Service_InsertDistrict]", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", district.Id));
                    sqlCommand.Parameters.Add(new SqlParameter("@DistrictName", district.Name));
                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", district.SportId));

                    sqlCommand.ExecuteNonQuery();

                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                }
                finally
                {
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }
    }
}
