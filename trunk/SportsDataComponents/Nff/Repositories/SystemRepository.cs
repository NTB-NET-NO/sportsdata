﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using NTB.SportsData.Components.Nff.Repositories.Interfaces;

namespace NTB.SportsData.Components.Nff.Repositories
{
    public class SystemRepository : ISystemRepository
    {
        /// <summary>
        ///     The check last table check.
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" /> method returns true if we have to configure the service, otherwise false.
        /// </returns>
        public bool CheckLastTableCheck()
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                var sqlCommand = new SqlCommand("Service_GetLastTableCheck", sqlConnection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                sqlConnection.Open();

                var sqlDataReader = sqlCommand.ExecuteReader();

                // If we have no rows, then we insert todays datetime
                if (!sqlDataReader.HasRows)
                {
                    // Closing the datareader
                    sqlDataReader.Close();

                    // Service_InsertLastTableCheck
                    sqlCommand = new SqlCommand("[Service_InsertLastTableCheck]", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.ExecuteNonQuery();
                    return true;
                }

                var lastConfigDateTime = new DateTime();
                while (sqlDataReader.Read())
                {
                    if (sqlDataReader["LastTableCheck"] != DBNull.Value)
                    {
                        lastConfigDateTime = Convert.ToDateTime(sqlDataReader["LastTableCheck"]);
                    }
                }

                var now = DateTime.Now;
                var difference = now - lastConfigDateTime;

                if (difference.Hours < 30)
                {
                    return false;
                }

                // We have to update the database again
                sqlCommand = new SqlCommand
                {
                    Connection = sqlConnection,
                    CommandText = "Service_GetLastTableCheck",
                    CommandType = CommandType.StoredProcedure
                };

                sqlCommand.ExecuteNonQuery();
                return true;
            }
        }
    }
}