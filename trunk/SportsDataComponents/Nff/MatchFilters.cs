﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchFilters.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The match filters.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.Nff
{
    using System;
    using System.Collections.Generic;
    
    using log4net;

    /// <summary>
    /// The match filters.
    /// </summary>
    public class MatchFilters
    {
        /// <summary>
        /// Static logger
        /// </summary>
        public static readonly ILog Logger = LogManager.GetLogger(typeof(MatchFilters));

        /// <summary>
        /// The get todays matches.
        /// </summary>
        /// <param name="matches">
        /// The matches.
        /// </param>
        /// <param name="matchDate">
        /// The match date.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public static List<Classes.Classes.Match> GetTodaysMatches(
            List<Classes.Classes.Match> matches, 
            DateTime? matchDate)
        {
            if (matchDate == null)
            {
                matchDate = DateTime.Today;
            }

            var m = new List<Classes.Classes.Match>();
            foreach (var match in matches)
            {
                // We check if the match start date has a value. If it does not, we shall inform about this and the continue the loop

                if (match.MatchDate == null)
                {
                    Logger.DebugFormat(
                        "MatchStartDate in match {0} with matchId {1} has no Date Value",
                        match.HomeTeamName + " - " + match.AwayTeamName,
                        match.Id);
                    continue;
                }

                Logger.InfoFormat("Checking date in match {0} - {1} against matchDate object {2}", match.Id, match.MatchDate.Value.ToShortDateString(), matchDate.Value.ToShortDateString());
                Logger.DebugFormat("MatchId {0}: Home Team Goals: {1}", match.Id, match.HomeTeamGoals);

                // We know that the match has a date value and so we shall check this value against what we have elsewhere
                if (match.MatchDate.Value.ToShortDateString() == matchDate.Value.ToShortDateString())
                {
                    if (match.HomeTeamGoals != null)
                    {
                        Logger.InfoFormat("Adding match into list which we are to return");
                        m.Add(match);
                    }
                    else
                    {
                        Logger.Info("===== No match was added to list of todays matches. =====");
                    }
                }
                
            }
                        
            Logger.InfoFormat("Returning total of {0} matches in list", m.Count);
            return m;
        }
    }
}