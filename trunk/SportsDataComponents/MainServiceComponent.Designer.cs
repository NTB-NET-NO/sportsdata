﻿namespace NTB.SportsData.Components
{
    partial class MainServiceComponent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            configFileWatcher = new System.IO.FileSystemWatcher();
            maintenanceTimer = new System.Timers.Timer();
            ((System.ComponentModel.ISupportInitialize)(configFileWatcher)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(maintenanceTimer)).BeginInit();
            // 
            // configFileWatcher
            // 
            configFileWatcher.EnableRaisingEvents = true;
            configFileWatcher.Changed += new System.IO.FileSystemEventHandler(configFileWatcher_Changed);
            // 
            // maintenanceTimer
            // 
            maintenanceTimer.Enabled = true;
            maintenanceTimer.Interval = 60000D;
            maintenanceTimer.Elapsed += new System.Timers.ElapsedEventHandler(maintenanceTimer_Elapsed);
            ((System.ComponentModel.ISupportInitialize)(configFileWatcher)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(maintenanceTimer)).EndInit();

        }

        #endregion

        private System.IO.FileSystemWatcher configFileWatcher;
        private System.Timers.Timer maintenanceTimer;
    }
}
