﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsData.Components
{
    public class ComponentConfiguration
    {
        public bool Configured { get; set; }
        public bool CreateXML { get; set; }
        public bool InsertDatabase { get; set; }
        public bool Enabled { get; set; }
        public bool GetResults { get; set; }
    }
}
