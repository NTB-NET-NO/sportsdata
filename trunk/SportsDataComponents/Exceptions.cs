﻿using System;

namespace NTB.SportsData.Components
{
    
    public class SoccerGathererComponentException : Exception
    {
             /// <summary>
        /// Initializes a new instance of the <see cref="SoccerGathererComponentException"/> class.
        /// </summary>
        /// <remarks>Specifies an error message only.</remarks>
        /// <param name="message">The error message</param>
        public SoccerGathererComponentException (string message)
            : base(message)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SoccerGathererComponentException"/> class.
        /// </summary>
        /// <remarks>
        /// Allows for specification of both an error message and an inner exception
        /// </remarks>
        /// <param name="message">The error message.</param>
        /// <param name="exception">Inner exception to wrap.</param>
        public SoccerGathererComponentException(string message, Exception exception)
            : base(message, exception)
        { }
    
    }

    public class SportsComponentException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SoccerGathererComponentException"/> class.
        /// </summary>
        /// <remarks>Specifies an error message only.</remarks>
        /// <param name="message">The error message</param>
        public SportsComponentException(string message)
            : base(message)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SportsDataException"/> class.
        /// </summary>
        /// <remarks>
        /// Allows for specification of both an error message and an inner exception
        /// </remarks>
        /// <param name="message">The error message.</param>
        /// <param name="exception">Inner exception to wrap.</param>
        public SportsComponentException(string message, Exception exception)
            : base(message, exception)
        { }

    }

    public class SportsDataException : Exception
    {
         /// <summary>
        /// Initializes a new instance of the <see cref="SportsDataException"/> class.
        /// </summary>
        /// <remarks>Specifies an error message only.</remarks>
        /// <param name="message">The error message</param>
        public SportsDataException (string message)
            : base(message)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SportsDataException"/> class.
        /// </summary>
        /// <remarks>
        /// Allows for specification of both an error message and an inner exception
        /// </remarks>
        /// <param name="message">The error message.</param>
        /// <param name="exception">Inner exception to wrap.</param>
        public SportsDataException(string message, Exception exception)
            : base(message, exception)
        { }
    }

    public class SoccerMetaServiceException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SoccerMetaServiceException"/> class.
        /// </summary>
        /// <remarks>Specifies an error message only.</remarks>
        /// <param name="message">The error message</param>
        public SoccerMetaServiceException (string message)
            : base(message)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SoccerMetaServiceException"/> class.
        /// </summary>
        /// <remarks>
        /// Allows for specification of both an error message and an inner exception
        /// </remarks>
        /// <param name="message">The error message.</param>
        /// <param name="exception">Inner exception to wrap.</param>
        public SoccerMetaServiceException(string message, Exception exception)
            : base(message, exception)
        { }
    }

    public class SoccerOrganizationServiceException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SoccerOrganizationServiceException"/> class.
        /// </summary>
        /// <remarks>Specifies an error message only.</remarks>
        /// <param name="message">The error message</param>
        public SoccerOrganizationServiceException(string message)
            : base(message)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SoccerOrganizationServiceException"/> class.
        /// </summary>
        /// <remarks>
        /// Allows for specification of both an error message and an inner exception
        /// </remarks>
        /// <param name="message">The error message.</param>
        /// <param name="exception">Inner exception to wrap.</param>
        public SoccerOrganizationServiceException(string message, Exception exception)
            : base(message, exception)
        { }
    }


    public class SoccerTournamentServiceException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SoccerTournamentServiceException"/> class.
        /// </summary>
        /// <remarks>Specifies an error message only.</remarks>
        /// <param name="message">The error message</param>
        public SoccerTournamentServiceException(string message)
            : base(message)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SoccerTournamentServiceException"/> class.
        /// </summary>
        /// <remarks>
        /// Allows for specification of both an error message and an inner exception
        /// </remarks>
        /// <param name="message">The error message.</param>
        /// <param name="exception">Inner exception to wrap.</param>
        public SoccerTournamentServiceException(string message, Exception exception)
            : base(message, exception)
        { }
    }
}
