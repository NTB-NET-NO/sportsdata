﻿using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using NTB.SportsData.Components.NFFTournamentService;

// Adding support for Quartz Scheduler
using Quartz;

// Adding support for log4net
using log4net;

namespace NTB.SportsData.Components
{
    class UpdateSportsData: IJob
    {
        /// <summary>
        /// Static logger
        /// </summary>
        static readonly ILog Logger = LogManager.GetLogger(typeof(UpdateSportsData));

        private readonly TournamentServiceClient _tournamentServiceClient = new TournamentServiceClient();
        
        // Database variables
        private SqlDataReader _sqlreader;

        private SqlConnection _mySqlConnection;

        static UpdateSportsData()
        {
            //Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
                log4net.Config.BasicConfigurator.Configure();
        }

        public UpdateSportsData()
        {
            if (_tournamentServiceClient.ClientCredentials != null)
            {
                _tournamentServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServiceUsername"];
                _tournamentServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicePassword"];
            }
        }

        public void Execute(IJobExecutionContext context)
        {
            Logger.Info("Executing JobExecutionContext" + context.JobDetail.Description);

            using (_mySqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                if (_mySqlConnection.State != ConnectionState.Open)
                {
                    _mySqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                    Logger.Debug("Start connecting!");

                    _mySqlConnection.Open();

                    Logger.Debug("End connecting!");
                }

                // Now we can go into the database and find Customer Jobs and their municipalities
                SqlCommand sqlCommand = new SqlCommand("Service_GetCustomersMunicipalities", _mySqlConnection)
                                            {
                                                CommandType = CommandType.StoredProcedure
                                            };

                _sqlreader = sqlCommand.ExecuteReader();

                if (_sqlreader.HasRows)
                {
                    // now we shall get the tournaments from this municipality (which really is a mess to get)
                }



            }

        }
    }
}
