﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PurgeSportsData.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The purge sports data.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using NTB.SportsData.Components.PublicRepositories;

namespace NTB.SportsData.Components
{
    using System;

    // Adding support for log4net
    using log4net;

    using NTB.SportsData.Classes.Enums;

    // Adding support for Quartz Scheduler
    using Quartz;

    /// <summary>
    /// The purge sports data.
    /// </summary>
    public class PurgeSportsData : IJob
    {
        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(PurgeSportsData));

        /// <summary>
        /// Initializes static members of the <see cref="PurgeSportsData"/> class.
        /// </summary>
        static PurgeSportsData()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        /// <summary>
        /// The execute.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public void Execute(IJobExecutionContext context)
        {
            Logger.InfoFormat("Executing JobExecutionContext: {0}", context.JobDetail.Description);

            // Mapping information sent from Component
            var dataMap = context.JobDetail.JobDataMap;

            var scheduleType = (ScheduleType)Enum.Parse(typeof(ScheduleType), dataMap.GetString("ScheduleType"), true);

            var days = dataMap.GetIntValue("Duration");

            var sportId = dataMap.GetIntValue("SportId");
            
            switch (scheduleType)
            {
                case ScheduleType.Daily:
                    if (days == 0)
                    {
                        days = 1;
                    }

                    break;

                case ScheduleType.Weekly:
                    if (days == 0)
                    {
                        days = 7;
                    }

                    break;
                case ScheduleType.Continous:
                    break;
                case ScheduleType.Monthly:
                    break;
                case ScheduleType.Yearly:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            // calling the delete matches by Interval method
            var dataMapper = new MatchRepository();

            // Deleting all or some...
            if (sportId == 0)
            {
                // All are deleted
                dataMapper.DeleteMatchesByDayInterval(days);
            }
            else
            {
                // Some are deleted
                dataMapper.DeleteMatchesByDayIntervalAndSportId(days, sportId);
            }
        }
    }
}