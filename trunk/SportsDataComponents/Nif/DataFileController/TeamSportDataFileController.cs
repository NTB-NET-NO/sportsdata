﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NifTeamSportDataFileController.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using NTB.SportsData.Components.PublicRepositories;

namespace NTB.SportsData.Components.Nif.DataFileController
{
    using System;
    using System.Collections.Generic;
    
    using log4net;
    
    using Classes.Classes;
    
    /// <summary>
    ///     The nif team sport data file controller.
    /// </summary>
    public class TeamSportDataFileController
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(TeamSportDataFileController));

        /// <summary>
        ///     Gets or sets the data params.
        /// </summary>
        public DataFileParams DataParams { get; set; }

        /// <summary>
        ///     The configure.
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        /// <exception cref="Exception">
        ///     Throwns an exception if data params is not set
        /// </exception>
        public bool Configure()
        {
            if (this.DataParams == null)
            {
                throw new SportsDataException("DataParams is not set!");
            }

            return true;
        }

        

        //public void Foo2(Tournament tournament, AgeCategory ageCategory)
        //{
        //    var teamsportXmlDocumentCreator = new TeamSportXmlDocumentCreator();
        //    Logger.Info("Creating AgeCategory-node for Tournament " + tournament.Id);
        //    if (ageCategory != null)
        //    {
        //        if (ageCategory.Name.ToLower() == "aldersbestemt")
        //        {
        //            teamsportXmlDocumentCreator.HasMatchFacts = false;
        //            teamsportXmlDocumentCreator.HasStanding = true;
        //        }
        //    }

        //    teamsportXmlDocumentCreator.CreateAgeCategoryNode(ageCategory, tournament);
        //    teamsportXmlDocumentCreator.CreateMatchNode(this.RemoteMatches);

        //    Logger.InfoFormat("Creating Header-node for Tournament {0}", tournament.Id);
        //    teamsportXmlDocumentCreator.CreateHeaderNode();

        //    Logger.InfoFormat("Creating Tournament-node for Tournament {0}", tournament.Id);
        //    teamsportXmlDocumentCreator.CreateTournamentNode(tournament, this.TournamentRound, this.RemoteMatches[0].MatchDate.Value, document);

        //    Logger.InfoFormat("Creating Organization-node for Tournament {0}", tournament.Id);
        //    teamsportXmlDocumentCreator.CreateOrganizationNode(federation);

        //    Logger.InfoFormat("Creating Sport-node for Tournament {0}", tournament.Id);
        //    teamsportXmlDocumentCreator.CreateSportNode(federation);

        //    var customers = this.GetListOfCustomers(tournament);
        //    Logger.InfoFormat("Number of customers to receive this file {0}", customers.Count);

        //    Logger.InfoFormat("Creating Customer-node for Tournament {0}", tournament.Id);
        //    teamsportXmlDocumentCreator.CreateCustomerNode(customers);

        //    /*
        //     * Document moved from here
        //     */
        //    teamsportXmlDocumentCreator.CreateFileNode(document);

        //    teamsportXmlDocumentCreator.CreateDocumentTypeNode();


        //    teamsportXmlDocumentCreator.CreatePartialResults(partialresults, match.Id);


        //    this.CreateXmlNodes(match, venue);

        //    this.TournamentRound = match.RoundId.ToString(CultureInfo.InvariantCulture);

        //    // Creating the document node
        //    var doc = teamsportXmlDocumentCreator.CreateDocument();

        //    foreach (var outputFolder in this.DataParams.FileOutputFolder)
        //    {
        //        var path = outputFolder;

        //        if (this.DataParams.FileOutputFolder == null)
        //        {
        //            if (this.DataParams.SportId == 4)
        //            {
        //                path = (from o in this.DataParams.FileOutputFolders where o.DisciplineId == this.DataParams.DisciplineId select o.OutputPath).Single();
        //            }
        //            else
        //            {
        //                path = (from o in this.DataParams.FileOutputFolders where o.SportId == this.DataParams.SportId select o.OutputPath).Single();
        //            }
        //        }

        //        Logger.InfoFormat("Write document {0} to path {1}", filename, path);
        //        teamsportXmlDocumentCreator.WriteDocument(doc, path);
        //    }

        //    // We must get the Id of the matches, then update the database
        //    // SetMatchDownloaded
        //    var downloadedMatches = doc.SelectNodes("/SportsData/Tournament/Matches/Match");
        //    if (downloadedMatches == null || downloadedMatches.Count <= 0)
        //    {
        //        continue;
        //    }

        //    foreach (var matchIdNode in from XmlNode node in downloadedMatches select node.SelectSingleNode("@Id"))
        //    {
        //        if (matchIdNode == null)
        //        {
        //            Logger.Error("Match Id not collected!!!");
        //            throw new SportsDataException("No Match Id Collected while setting Download to 1");
        //        }

        //        var matchId = Convert.ToInt32(matchIdNode.Value);

        //        // TODO: Fix so the match downloaded is stored in the database
        //        if (this.DataParams.Results)
        //        {
        //            matchRepository.SetMatchDownloaded(matchId);
        //        }
        //        else
        //        {
        //            if (this.DataParams.OperationMode == OperationMode.Distributor)
        //            {
        //                matchRepository.SetScheduleMatchDownloaded(matchId);
        //            }
        //        }
        //    }
        //}

        /// <summary>
        /// The get list of customers.
        /// </summary>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Customer> GetListOfCustomers(Tournament tournament)
        {
            var repository = new CustomerRepository();

            var customers = repository.GetCustomerByTournamentAndSportId(tournament.SportId, tournament.Id);

            if (customers != null)
            {
                return customers;
            }

            return new List<Customer>();
        }

        ///// <summary>
        ///// The create xml nodes.
        ///// </summary>
        ///// <param name="match">
        ///// The match.
        ///// </param>
        ///// <param name="venue">
        ///// The venue.
        ///// </param>
        //public void CreateXmlNodes(Match match, Venue venue)
        //{
        //    var teamsportXmlDocumentCreator = new TeamSportXmlDocumentCreator();
        //    if (match.MatchPlayers != null && match.MatchPlayers.Count > 0)
        //    {
        //        Logger.Info("Creating playernode for match " + match.Id);
        //        teamsportXmlDocumentCreator.CreatePlayerNode(match.MatchPlayers, match.Id);
        //    }

        //    if (match.MatchIncidents != null && match.MatchIncidents.Count > 0)
        //    {
        //        Logger.Info("Creating MatchIncidents-node for match " + match.Id);
        //        teamsportXmlDocumentCreator.CreateIncidentNode(match.MatchIncidents, match.Id);
        //        teamsportXmlDocumentCreator.HasMatchFacts = true;
        //    }

        //    if (match.Referees != null && match.Referees.Count > 0)
        //    {
        //        Logger.InfoFormat("Creating referee-node(s) for match {0}", match.Id);
        //        teamsportXmlDocumentCreator.CreateRefereeNode(match.Referees, match.Id);
        //    }

        //    Logger.InfoFormat("Creating Stadium-node for match {0}", match.Id);
        //    teamsportXmlDocumentCreator.CreateStadiumNode(venue, match.Spectators, match.Id);

        //    Logger.Info("Creating HomeTeam-node for match " + match.Id);
        //    teamsportXmlDocumentCreator.CreateHomeTeamNode(match);

        //    Logger.Debug("Creating AwayTeam-node for match " + match.Id);
        //    teamsportXmlDocumentCreator.CreateAwayTeamNode(match);
        //}
    }
}