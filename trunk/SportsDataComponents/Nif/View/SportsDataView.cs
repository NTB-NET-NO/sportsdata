﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace NTB.SportsData.Components.Nif.View
{
    public class SportsDataView
    {

        /// <summary>
        /// The write document.
        /// </summary>
        /// <param name="doc">
        /// The doc.
        /// </param>
        /// <param name="path">
        /// The path.
        /// </param>
        public void WriteXmlDocument(XmlDocument doc, string path)
        {
            var node = doc.SelectSingleNode("/SportsData/Tournament/Header/File/Name/@FullName");
            var filename = "NTB_SportsData_" + DateTime.Today.Year + DateTime.Today.Month + DateTime.Today.Day;
            if (node != null)
            {
                filename = node.Value;
            }

            var fullname = path + @"\" + filename;

            // Now we shall get path
            doc.Save(fullname);
        }

        /// <summary>
        /// The write document.
        /// </summary>
        /// <param name="doc">
        /// The doc.
        /// </param>
        /// <param name="path">
        /// The path.
        /// </param>
        public void WriteJsonDocument(XmlDocument doc, string path)
        {
            var node = doc.SelectSingleNode("/SportsData/Tournament/Header/File/Name/@FullName");
            var filename = "NTB_SportsData_" + DateTime.Today.Year + DateTime.Today.Month + DateTime.Today.Day;
            if (node != null)
            {
                filename = node.Value;
            }

            var fullname = path + @"\" + filename;

            // Now we shall get path
            doc.Save(fullname);
        }
    }
}
