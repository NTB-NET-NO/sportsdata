﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Classes.Classes.SportsDataOutput;
using NTB.SportsData.Classes.Classes.SportsDataOutput.Standing;
using MatchEvent = NTB.SportsData.Classes.Classes.SportsDataOutput.MatchEvent;
using Team = NTB.SportsData.Classes.Classes.SportsDataOutput.Team;

namespace NTB.SportsData.Components.Nif.Models
{
    public class TeamSportsDataViewModel
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(TeamSportsDataViewModel));

        public List<MatchModel> Matches { get; set; }

        public Classes.Classes.Tournament Tournament { get; set; }

        public Document Document { get; set; }

        public List<Classes.Classes.Customer> Customers { get; set; }

        public Classes.Classes.Sport Sport { get; set; }

        public Federation Federation {get; set; }

        public Classes.Classes.EventType EventType { get; set; }

        public Classes.Classes.AgeCategory AgeCategory { get; set; }

        public List<Classes.Classes.TeamResult> TournamentStandingList { get; set; }

        public Classes.Classes.SportsDataOutput.SportsData SportsData { get; set; }

        public Classes.Classes.SportsDataOutput.SportsData PopulateSportsData()
        {
            var sportsdata = new Classes.Classes.SportsDataOutput.SportsData();
            sportsdata.Tournament = this.PopulateTournament();

            return sportsdata;
        }

        public Classes.Classes.SportsDataOutput.Tournament PopulateTournament()
        {
            var currentTournament = new Classes.Classes.SportsDataOutput.Tournament();
            currentTournament.DocId = this.CreateDocumentId();
            currentTournament.Type = this.CreateTournamentType();
            currentTournament.TournamentMetaInfo = this.PopulateTournamentMetaInfo();

            currentTournament.Header = this.PopulateHeader(currentTournament);

            currentTournament.Matches = this.PopulateTournamentMatches();

            currentTournament.TournamentTable = this.PopulateTournamentStanding();

            return currentTournament;
        }

        public string CreateTournamentType()
        {
            if (this.DocumentType == Classes.Enums.DocumentType.MatchFact)
            {
                return "kampfakta";
            }

            if (this.DocumentType == Classes.Enums.DocumentType.Result)
            {
                return "resultat";
            }

            if (this.DocumentType == Classes.Enums.DocumentType.Standing)
            {
                return "restab";
            }

            if (this.DocumentType == Classes.Enums.DocumentType.Schedule)
            {
                return "schedule";
            }

            return "resultat";
        }

        public List<Classes.Classes.SportsDataOutput.Standing.TeamResult> PopulateTournamentStanding()
        {
            var teamList = new List<Classes.Classes.SportsDataOutput.Standing.TeamResult>();
            if (this.TournamentStandingList == null)
            {
                return teamList;
            }

            foreach (var table in this.TournamentStandingList)
            {
                var team = new Classes.Classes.SportsDataOutput.Standing.TeamResult();
                team.Name = new Name();
                team.Name.Content = table.OrgName;
                team.Id = table.OrgId;
                team.TablePosition = table.Position;

                team.Points = new Points();
                team.Points.Total = table.TotalPoints;
                team.Points.HomePoints = new SubPoints();
                team.Points.HomePoints.Content = table.PointsHome;
                team.Points.AwayPoints = new SubPoints();
                team.Points.AwayPoints.Content = table.PointsAway;
                team.Goal = new Goal();
                team.Goal.TotalAgainst = table.GoalsConceeded;
                team.Goal.TotalScored = table.GoalsScored;
                
                team.Goal.AwayGoal = new SubGoal();
                team.Goal.AwayGoal.Against = table.GoalsConcededAway;
                team.Goal.AwayGoal.Scored = table.GoalsScoredAway;

                team.Goal.HomeGoal = new SubGoal();
                team.Goal.HomeGoal.Against = table.GoalsConcededHome;
                team.Goal.HomeGoal.Scored = table.GoalsScoredHome;

                team.Match = new Classes.Classes.SportsDataOutput.Standing.Match();
                team.Match.Total = table.Matches;
                team.Match.AwayMatch = new SubMatch();
                team.Match.AwayMatch.Draw = table.DrawsAway;
                team.Match.AwayMatch.Lost = table.LossesAway;
                team.Match.AwayMatch.Won = table.VictoriesAway;

                team.Match.HomeMatch = new SubMatch();
                team.Match.HomeMatch.Draw = table.DrawsHome;
                team.Match.HomeMatch.Lost = table.LossesHome;
                team.Match.HomeMatch.Won = table.VictoriesHome;

                teamList.Add(team);
            }
            return teamList;

        }

        public Header PopulateHeader(Classes.Classes.SportsDataOutput.Tournament tournament)
        {
            var header = new Header();
            header.Organisation = this.PopulateOrganisation();
            header.AgeCategory = this.PopulateAgeCategory();
            header.Customers = this.PopulateCustomers();
            header.Sport = this.PopulateSport();
            header.File = this.PopulateFile(tournament);
            header.DocumentType = this.PopulateDocumentType();
            header.EventType = this.PopulateEventType();
            return header;
        }

        private Classes.Classes.SportsDataOutput.EventType PopulateEventType()
        {
            if (this.EventType != null)
            {
                var eventType = new Classes.Classes.SportsDataOutput.EventType();
                eventType.Id = this.EventType.EventTypeId;
                eventType.Name = this.EventType.EventTypeName;

                return eventType;
            }

            return new Classes.Classes.SportsDataOutput.EventType();
            
        }
        private DocumentType PopulateDocumentType()
        {
            var documentType = new DocumentType();
            if (this.Document.DocumentTypes.Contains(this.DocumentType))
            {
                if (this.DocumentType == Classes.Enums.DocumentType.MatchFact)
                {
                    documentType.Type = "matchfacts";
                }

                if (this.DocumentType == Classes.Enums.DocumentType.Result)
                {
                    documentType.Type = "sportsresults";
                }

                if (this.DocumentType == Classes.Enums.DocumentType.Standing)
                {
                    documentType.Type = "sportsresults";
                }

                if (this.DocumentType == Classes.Enums.DocumentType.Schedule)
                {
                    documentType.Type = "sportschedule";
                }
            }

            //if (this.Tournament.PublishResult)
            //{
            //    // todo: Correct this code so it is creating either a schedule or a sportsresult file
            //    // todo: Consider setting these settings in either config-file or in exe-config
                
            //    documentType.Type = "sportsresults";
            //    return documentType;
            //}

            //documentType.Type = "sportsresults";
            return documentType;
        }

        public File PopulateFile(Classes.Classes.SportsDataOutput.Tournament tournament)
        {
            var file = new File();
            file.FileCreated = new FileCreated();
            file.FileCreated.Content = this.CreateDateTimeString(this.Document.Created);
            
            file.FileName = new FileName();
            file.FileName.FullName = string.Format("{0}_V{1}_{2}.{3}", this.Document.Filename, this.Document.Version, tournament.Type, "xml");

            file.FileName.Type = "xml";
            file.FileName.Content = this.Document.Filename;

            file.FileVersion = new FileVersion();
            file.FileVersion.Content = this.Document.Version;

            return file;
        }

        /// <summary>
        /// The create date time string.
        /// </summary>
        /// <param name="dateTime">
        /// The date time.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string CreateDateTimeString(DateTime dateTime)
        {
            var value = dateTime.ToString("yyyy-MM-dd") + "T" + dateTime.ToString("HH:mm:sszzz");
            // Logger.DebugFormat("Generated string: {0}", value.Replace(".", ":"));
            return value.Replace(".", ":");

        }

        public List<Classes.Classes.SportsDataOutput.Customer> PopulateCustomers()
        {
            var customers = new List<Classes.Classes.SportsDataOutput.Customer>();
            foreach (var customer in this.Customers)
            {
                var currentCustomer = new Classes.Classes.SportsDataOutput.Customer();
                currentCustomer.CustomerName = new CustomerName();
                currentCustomer.CustomerName.Id = customer.Id;
                currentCustomer.CustomerName.RemoteCustomerId = customer.RemoteCustomerId;
                currentCustomer.CustomerName.Content = customer.Name;

                customers.Add(currentCustomer);
            }

            return customers;
        }

        public Classes.Classes.SportsDataOutput.AgeCategory PopulateAgeCategory()
        {
            try
            {
                var ageCategory = new Classes.Classes.SportsDataOutput.AgeCategory();
                if (this.AgeCategory != null)
                {
                    ageCategory.Name = this.AgeCategory.Name;
                    ageCategory.Content = this.AgeCategory.AgeCategoryDefinition;
                    ageCategory.MinimumAge = this.AgeCategory.FromAge;
                    ageCategory.MaximumAge = this.AgeCategory.ToAge;
                    ageCategory.Id = this.AgeCategory.Id;
                }
                
                return ageCategory;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                return new Classes.Classes.SportsDataOutput.AgeCategory();
            }
        }

        public Organisation PopulateOrganisation()
        {
            try
            {
                var organisation = new Organisation();
                organisation.ShortName = this.Federation.NameShort;
                organisation.Content = this.Federation.Name;
                organisation.Id = this.Federation.Id;

                return organisation;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                return new Organisation();
            }
        }

        public Classes.Classes.SportsDataOutput.Sport PopulateSport()
        {
            var sport = new Classes.Classes.SportsDataOutput.Sport();
            sport.Id = this.Sport.Id;
            sport.Content = this.Sport.Name;

            return sport;
        }

        public DateTime CreateDocumentDateTimeFromMatchDate(Classes.Classes.Match match)
        {
            try
            {
                if (match.MatchDate == null)
                {
                    return DateTime.Today;
                }
                var matchDate = Convert.ToDateTime(match.MatchDate);

                if (match.MatchStartTime <= 0 || match.MatchStartTime > 2359 )
                {
                    return matchDate;
                }
                
                var matchTime = match.MatchStartTime.ToString();
                
                var hour = Convert.ToInt32(matchTime.Substring(0, 2));
                var minutes = 0;
                if (matchTime.Length > 4)
                {
                    minutes = Convert.ToInt32(matchTime.Substring(2, 2));
                }
                
                var matchTimeSpan = new TimeSpan(hour, minutes, 0);

                matchDate = matchDate.Add(matchTimeSpan);

                return matchDate;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);

                return Convert.ToDateTime(match.MatchDate);
            }
        }

        public TournamentMetaInfo PopulateTournamentMetaInfo()
        {
            var tournamentMetaInfo = new TournamentMetaInfo();
            tournamentMetaInfo.Id = this.Tournament.Id.ToString();
            tournamentMetaInfo.Name = this.Tournament.Name;
            tournamentMetaInfo.Division = this.Tournament.Division.ToString();
            tournamentMetaInfo.Number = this.Tournament.Number.ToString();
            
            var firstMatch = this.Matches[0].Match;

            tournamentMetaInfo.MatchDate = this.CreateDocumentDateTimeFromMatchDate(firstMatch);
            
            tournamentMetaInfo.RoundNumber = this.Matches[0].Match.RoundId.ToString();
            tournamentMetaInfo.RoundName = this.Matches[0].Match.RoundName;

            return tournamentMetaInfo;
        }

        public string CreateDocumentId()
        {
            var documentId = this.Document.Filename;

            if (this.Document.DocumentType == Classes.Enums.DocumentType.MatchFact)
            {
                documentId += "_Fakta";
            }
            else if (this.Document.DocumentType == Classes.Enums.DocumentType.Standing)
            {
                documentId += "_ResTab";
            }
            else if (this.Document.DocumentType == Classes.Enums.DocumentType.Result)
            {
                documentId += "_Res";
            }

            return documentId;
        }

        public List<TournamentMatch> PopulateTournamentMatches()
        {
            var tournamentMatches = new List<TournamentMatch>();
            foreach (var match in this.Matches)
            {
                var tournamentMatch = new TournamentMatch();
                tournamentMatch.Id = match.Match.Id;
                if (match.Match.MatchDate != null)
                {
                    tournamentMatch.StartDate = match.Match.MatchDate.Value.ToShortDateString();
                }

                tournamentMatch.StartTime = match.Match.MatchStartTime.ToString();

                tournamentMatch.HomeTeam = this.PopulateTeam("home", match);
                tournamentMatch.AwayTeam = this.PopulateTeam("away", match);
                tournamentMatch.Referees = this.PopulateReferees(match);
                tournamentMatch.Stadium = this.PopulateStadium(match);
                tournamentMatch.MatchEvents = this.PopulateMatchEvents(match);

                tournamentMatches.Add(tournamentMatch);
            }

            return tournamentMatches;
        }

        public Stadium PopulateStadium(MatchModel match)
        {
            var stadium = new Stadium();
            if (match.Venue == null)
            {
                return stadium;
            }

            stadium.Name = match.Venue.VenueName;
            stadium.Spectators = match.Match.Spectators;
            stadium.Id = match.Venue.VenueId;
            stadium.Latitude = match.Venue.Latitude;
            stadium.Longitude = match.Venue.Longitude;
            stadium.LocalCouncilName = match.Venue.LocalCouncilName;

            return stadium;
            
        }

        public List<MatchEvent> PopulateMatchEvents(MatchModel match)
        {
            var matchEvents = new List<MatchEvent>();
            
            if (match.Match.MatchEventList == null)
            {
                return matchEvents;
            }

            foreach (var matchIncident in match.Match.MatchEventList)
            {
                var matchEvent = new MatchEvent();
                matchEvent.Id = matchIncident.MatchEventId;
                matchEvent.TypeId = matchIncident.MatchEventTypeId;
                matchEvent.Value = matchIncident.MatchEventType;
                matchEvent.TeamId = matchIncident.TeamId;
                matchEvent.Minute = matchIncident.Minute.ToString();
                
                matchEvent.Player = new Classes.Classes.SportsDataOutput.Player();
                matchEvent.Player.Id = matchIncident.PersonId;
                matchEvent.Player.FirstName = matchIncident.FirstName;
                matchEvent.Player.LastName = matchIncident.LastName;

                matchEvents.Add(matchEvent);
            }

            return matchEvents;
        }

        public List<Classes.Classes.SportsDataOutput.Referee> PopulateReferees(MatchModel match)
        {
            var referees = new List<Classes.Classes.SportsDataOutput.Referee>();
            if (match.Match.Referees == null)
            {
                return referees;
            }
            foreach (var matchReferee in match.Match.Referees)
            {
                var referee = new Classes.Classes.SportsDataOutput.Referee();
                referee.Id = matchReferee.Id;
                referee.Type = matchReferee.RefereeType;
                referee.TypeId = matchReferee.RefereeTypeId;
                referee.ClubId = matchReferee.ClubId;
                referee.Club = matchReferee.ClubName;
                referee.FirstName = matchReferee.FirstName;
                referee.LastName = matchReferee.LastName;
                referee.FormattedName = matchReferee.RefereeName;
                referees.Add(referee);
            }
            
            return referees;
        }

        public Team PopulateTeam(string alignment, MatchModel match)
        {
            var team = new Team();
            team.Alignment = alignment;
            team.TeamName = new TeamName();
            if (alignment == "home")
            {
                team.Id = match.Match.HomeTeamId;
                team.TeamName.Name = match.Match.HomeTeamName;
                team.TeamName.ShortName = match.Match.HomeTeamShortName;
                if (match.Match.HomeTeamGoals != null)
                {
                    team.TotalGoals = Convert.ToInt32(match.Match.HomeTeamGoals.Value);
                }

            }
            else if (alignment == "away")
            {
                team.Id = match.Match.AwayTeamId;
                team.TeamName.Name = match.Match.AwayTeamName;
                team.TeamName.ShortName = match.Match.AwayTeamShortName;

                if (match.Match.AwayTeamGoals != null)
                {
                    team.TotalGoals = Convert.ToInt32(match.Match.AwayTeamGoals.Value);
                }
            }

            
            team.Players = this.PopulatePlayers(match, alignment);
            team.PartialResults = this.PopulatePartialResult(match, alignment);
            
            return team;
        }

        public List<Classes.Classes.SportsDataOutput.Player> PopulatePlayers(MatchModel match, string alignment)
        {
            if (alignment == "home")
            {
                match.Match.HomeTeamPlayers = match.Players.Where(x => x.TeamId == match.Match.HomeTeamId)
                    .ToList();
                return this.PopulateHomeTeamPlayers(match);
            }

            match.Match.AwayTeamPlayers = match.Players.Where(x => x.TeamId == match.Match.AwayTeamId)
                    .ToList();
            return this.PopulateAwayTeamPlayers(match);

        }

        public List<Classes.Classes.SportsDataOutput.Player> PopulateHomeTeamPlayers(MatchModel match)
        {
            var players = new List<Classes.Classes.SportsDataOutput.Player>();
            
            if (match.Match.HomeTeamPlayers == null)
            {
                return players;
            }
            foreach (var teamPlayer in match.Match.HomeTeamPlayers)
            {
                var player = new Classes.Classes.SportsDataOutput.Player();
                player.Id = teamPlayer.Id;
                player.Position = teamPlayer.Position;
                player.Captain = teamPlayer.TeamCaptain;
                player.ShirtNumber = teamPlayer.PlayerShirtNumber;
                player.FirstName = teamPlayer.FirstName;
                player.LastName = teamPlayer.LastName;
                player.FormattedName = teamPlayer.FirstName + " " + teamPlayer.LastName;

                players.Add(player);
            }

            return players;
        }

        public List<Classes.Classes.SportsDataOutput.Player> PopulateAwayTeamPlayers(MatchModel match)
        {
            var players = new List<Classes.Classes.SportsDataOutput.Player>();
            if (match.Match.AwayTeamPlayers == null)
            {
                return players;
            }

            foreach (var teamPlayer in match.Match.AwayTeamPlayers)
            {
                var player = new Classes.Classes.SportsDataOutput.Player();
                player.Id = teamPlayer.Id;
                player.Position = teamPlayer.Position;
                player.Captain = teamPlayer.TeamCaptain;
                player.ShirtNumber = teamPlayer.PlayerShirtNumber;
                player.FirstName = teamPlayer.FirstName;
                player.LastName = teamPlayer.LastName;
                player.FormattedName = teamPlayer.FirstName + " " + teamPlayer.LastName;

                players.Add(player);
            }

            return players;
        }

        public List<Classes.Classes.SportsDataOutput.PartialResult> PopulatePartialResult(MatchModel match, string alignment)
        {
            var partialResults = new List<Classes.Classes.SportsDataOutput.PartialResult>();
            if (match.PartialResults == null)
            {
                return partialResults;
            }

            foreach (var result in match.PartialResults)
            {
                var partialResult = new Classes.Classes.SportsDataOutput.PartialResult();
                partialResult.Id = result.PartialResultId;
                partialResult.TypeId = result.PartialResultTypeId;
                // partialResult.Type = result.;
                if (alignment == "home")
                {
                    partialResult.HomeTeamGoals = Convert.ToInt32(result.HomeGoals);
                    
                }
                if (alignment == "away")
                {
                    partialResult.AwayTeamGoals = Convert.ToInt32(result.AwayGoals);
                }
                
                partialResults.Add(partialResult);
            }

            return partialResults;
        }

        public Classes.Enums.DocumentType DocumentType { get; set; }
    }

}
