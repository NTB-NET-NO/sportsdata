﻿using System;
using System.Collections.Generic;
using System.Configuration;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Classes.Enums;
using NTB.SportsData.Components.Nif.Populators;
using NTB.SportsData.Components.PublicRepositories;

namespace NTB.SportsData.Components.Nif.Models
{
    public class DocumentModel
    {
        public List<Customer> Customers { get; set; }

        public Document Document { get; set; }

        public bool HasMatchFatcts { get; set; }

        public DataFileParams DataFileParams { get; set; }
        /// <summary>
        /// The get list of customers.
        /// </summary>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Customer> GetListOfCustomers(Tournament tournament)
        {
            var repository = new CustomerRepository();

            var customers = repository.GetCustomerByTournamentAndSportId(tournament.SportId, tournament.Id);

            if (customers != null)
            {
                return customers;
            }

            customers = new List<Customer>();
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
            {
                var customer = new Customer()
                {
                    Id = 1,
                    Name = "NTB",
                    RemoteCustomerId = 1
                };

                customers.Add(customer);
            }
            return customers;
        }

        
        public int GetDocumentVersionByTournamentId(int id)
        {
            var repository = new DocumentRepository();
            return repository.GetDocumentVersionByTournamentId(id);
        }

        /// <summary>
        /// Create the document object
        /// </summary>
        /// <param name="tournament">The tournament object</param>
        /// <param name="dateTime">the date time</param>
        /// <param name="version">the version</param>
        public void CreateDocument(Tournament tournament, DateTime dateTime, int version)
        {
            // Create the organization object, add discipline to it and we are good to go!
            var populator = new FederationPopulator();

            var federation = this.SetFederation(populator);

            var filenamePopulator = this.PopulateFileNamePopulator(tournament);

            // now let's check if we have this document in the database
            
            var documentTypes = this.SetDocumentTypes(filenamePopulator);

            var filename = filenamePopulator.CreateFileNameBase(federation, tournament, dateTime);
            var document = new Document
            {
                Created = DateTime.Now,
                Filename = filename,
                SportId = this.DataFileParams.SportId,
                TournamentId = tournament.Id,
                Version = version,
                FullName = string.Empty,
                DocumentTypes = documentTypes
            };

            this.Document = document;
        }

        /// <summary>
        /// Storing the document information in the database
        /// </summary>
        /// <param name="version">The version</param>
        public void StoreDocumentInDatabase(int version)
        {
            var repository = new DocumentRepository();
            if (version == 1)
            {
                repository.InsertOne(this.Document);
            }
            else
            {
                repository.Update(this.Document);
            }
        }

        public FileNamePopulator PopulateFileNamePopulator(Tournament tournament)
        {
            var filenamePopulator = new FileNamePopulator();
            if (tournament.PublishTournamentTable)
            {
                filenamePopulator.HasStanding = true;
            }

            if (this.HasMatchFatcts)
            {
                filenamePopulator.HasMatchFacts = true;
            }
            return filenamePopulator;
        }

        public Federation SetFederation(FederationPopulator populator)
        {
            var federation = populator.GetFederationFromSportId(this.DataFileParams.SportId);
            var discipline = populator.GetFederationDisciplineById(this.DataFileParams.DisciplineId);
            if (discipline == null)
            {
                throw new SportsDataException("Discipline is null!");
            }

            federation.DisciplineId = discipline.Id;
            federation.DisciplineName = discipline.Name;
            return federation;
        }

        /// <summary>
        /// Sets the document types
        /// </summary>
        /// <param name="filenamePopulator">The file name populator</param>
        public List<DocumentType> SetDocumentTypes(FileNamePopulator filenamePopulator)
        {
            var documentType = DocumentType.Result;
            var documentTypes = new List<DocumentType>();

            if (this.DataFileParams.Results)
            {
                if (filenamePopulator.HasStanding)
                {
                    // fullFilename = filename + "_Fakta_V" + version + ".xml";
                    documentType = DocumentType.Standing;
                    documentTypes.Add(documentType);
                }

                if (filenamePopulator.HasMatchFacts)
                {
                    // fullFilename = filename + "_Fakta_V" + version + ".xml";
                    documentType = DocumentType.MatchFact;
                    documentTypes.Add(documentType);
                }

                if (!filenamePopulator.HasStanding)
                {
                    documentType = DocumentType.Result;
                    documentTypes.Add(documentType);
                }
            }


            else if (filenamePopulator.RenderResult == false)
            {
                // fullFilename = filename + "_Sched_V" + version + ".xml";
                documentType = DocumentType.Schedule;
                documentTypes.Add(documentType);
            }
            else
            {
                if (documentTypes.Count == 0)
                {
                    documentTypes.Add(documentType);
                }
            }

            return documentTypes;
            
        }
    }
}
