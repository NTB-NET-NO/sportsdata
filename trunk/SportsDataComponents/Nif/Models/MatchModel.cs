﻿using System.Collections.Generic;
using NTB.SportsData.Classes.Classes;

namespace NTB.SportsData.Components.Nif.Models
{
    public class MatchModel
    {
        public Match Match { get; set; }

        public List<Player> Players { get; set; }

        public List<MatchEvent> MatchIncidents { get; set; }

        public List<Referee> MatchReferees { get; set; }

        public Venue Venue { get; set; }

        public List<PartialResult> PartialResults { get; set; }

        // todo: Consider if we shall add some methods here...

    }
}
