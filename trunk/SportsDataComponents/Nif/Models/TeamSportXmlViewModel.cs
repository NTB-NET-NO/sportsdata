using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Classes.Enums;
using NTB.SportsData.Utilities;
using MatchResult = NTB.SportsData.Classes.Classes.SportsDataOutput.MatchResult;

namespace NTB.SportsData.Components.Nif.Models
{
    public class TeamSportXmlViewModel
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(TeamSportXmlViewModel));

        public bool HasMatchFacts { get; set; }

        public bool HasStanding { get; set; }

        public bool RenderResult { get; set; }
        
        public XmlDocument CreateViewModel(DocumentModel model)
        {
            var doc = this.CreateDocument();
            return doc;
        }

        /// <summary>
        ///     Gets or sets the age category node.
        /// </summary>
        public XmlNode AgeCategoryNode { get; set; }

        /// <summary>
        ///     Gets or sets the away team node.
        /// </summary>
        public Dictionary<int, XmlDocument> AwayTeamNode { get; set; }

        /// <summary>
        ///     Gets or sets the customer node.
        /// </summary>
        public XmlNode CustomerNode { get; set; }

        /// <summary>
        ///     Gets or sets the document type node.
        /// </summary>
        public XmlNode DocumentTypeNode { get; set; }

        /// <summary>
        ///     Gets or sets the file node.
        /// </summary>
        public XmlNode FileNode { get; set; }

        /// <summary>
        ///     Gets or sets the header node.
        /// </summary>
        public XmlNode HeaderNode { get; set; }

        /// <summary>
        ///     Gets or sets the home team node.
        /// </summary>
        public Dictionary<int, XmlDocument> HomeTeamNode { get; set; }

        /// <summary>
        ///     Gets or sets the incident node.
        /// </summary>
        public Dictionary<int, XmlDocument> IncidentNode { get; set; }

        /// <summary>
        ///     gets or sets the player node
        /// </summary>
        public Dictionary<int, XmlDocument> PlayerNode { get; set; }

        /// <summary>
        ///     Gets or sets the match node.
        /// </summary>
        public XmlNode MatchNode { get; set; }

        /// <summary>
        ///     Gets or sets the organization node.
        /// </summary>
        public XmlNode OrganizationNode { get; set; }

        /// <summary>
        ///     Gets or sets the referee node.
        /// </summary>
        public Dictionary<int, XmlDocument> RefereeNode { get; set; }

        /// <summary>
        ///     Gets or sets the sport node.
        /// </summary>
        public XmlNode SportNode { get; set; }

        /// <summary>
        ///     Gets or sets the stadium node.
        /// </summary>
        public Dictionary<int, XmlDocument> StadiumNode { get; set; }

        /// <summary>
        ///     Gets or sets the partial result node.
        /// </summary>
        public Dictionary<int, XmlDocument> PartialResultNode { get; set; }

        /// <summary>
        ///     Gets or sets the tournament node.
        /// </summary>
        public XmlNode TournamentNode { get; set; }

        /// <summary>
        ///     Gets or sets the tournament table node.
        /// </summary>
        public XmlNode TournamentTableNode { get; set; }

        public void PopulatePartialResult()
        {
            
        }
        /// <summary>
        /// The set xml partial result node.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <param name="doc">
        /// The doc.
        /// </param>
        /// <param name="xmlMatch">
        /// The xml match.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool SetXmlPartialResultNode(int matchId, XmlDocument doc, XmlElement xmlMatch)
        {
            if (this.PartialResultNode == null)
            {
                return true;
            }

            Logger.Debug("Placing stadium element");
            try
            {
                var selectedNode = from s in this.PartialResultNode where s.Key == matchId select s.Value;

                var xmlPartialResultNode = new XmlDocument();

                var xmlDocuments = selectedNode as XmlDocument[] ?? selectedNode.ToArray();

                if (xmlDocuments.Any())
                {
                    xmlPartialResultNode = xmlDocuments.Single();
                }

                if (xmlPartialResultNode.SelectSingleNode("PartialResults") != null)
                {
                    var importNode = doc.ImportNode(xmlPartialResultNode.SelectSingleNode("PartialResults"), true);
                    xmlMatch.AppendChild(importNode);
                }

                return true;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);

                return false;
            }
        }

        /// <summary>
        /// The set xml stadium node.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <param name="doc">
        /// The doc.
        /// </param>
        /// <param name="xmlMatch">
        /// The xml match.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool SetXmlStadiumNode(int matchId, XmlDocument doc, XmlElement xmlMatch)
        {
            if (this.StadiumNode == null)
            {
                return true;
            }

            Logger.Debug("Placing stadium element");
            try
            {
                var selectedNode = from s in this.StadiumNode where s.Key == matchId select s.Value;

                var xmlStadiumNode = new XmlDocument();

                var xmlDocuments = selectedNode as XmlDocument[] ?? selectedNode.ToArray();

                if (xmlDocuments.Any())
                {
                    xmlStadiumNode = xmlDocuments.Single();
                }

                if (xmlStadiumNode.SelectSingleNode("Stadium") != null)
                {
                    var xmlStadium = doc.ImportNode(xmlStadiumNode.SelectSingleNode("Stadium"), true);
                    xmlMatch.AppendChild(xmlStadium);
                }

                return true;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);

                return false;
            }
        }

        public Classes.Classes.SportsDataOutput.SportsData CreateSportsDataDocument()
        {
            var sportsData = new Classes.Classes.SportsDataOutput.SportsData();
            // sportsData.Tournament = new Classes.Classes.SportsDataOutput.Tournament();
            // sportsData.Tournament.TournamentMetaInfo;

            return sportsData;
            
        }
        /// <summary>
        ///     The create document.
        /// </summary>
        /// <returns>
        ///     The <see cref="XmlDocument" />.
        /// </returns>
        public XmlDocument CreateDocument()
        {
            var doc = new XmlDocument();
            var xmlElement = doc.CreateElement("SportsData");
            XmlNode xmlTournament = null;
            XmlNode xmlHeader = null;

            if (this.TournamentNode == null)
            {
                return doc;
            }

            if (this.TournamentNode.SelectSingleNode("Tournament") != null)
            {
                xmlTournament = doc.ImportNode(this.TournamentNode.SelectSingleNode("Tournament"), true);

                xmlElement.AppendChild(xmlTournament);
            }

            if (this.HeaderNode.SelectSingleNode("Header") != null)
            {
                xmlHeader = doc.ImportNode(this.HeaderNode.SelectSingleNode("Header"), true);
                if (xmlTournament != null)
                {
                    xmlTournament.AppendChild(xmlHeader);
                }
            }

            if (this.OrganizationNode.SelectSingleNode("Organisation") != null)
            {
                var xmlOrganization = doc.ImportNode(this.OrganizationNode.SelectSingleNode("Organisation"), true);

                if (xmlHeader != null)
                {
                    xmlHeader.AppendChild(xmlOrganization);
                }
            }

            if (this.AgeCategoryNode.SelectSingleNode("AgeCategory") != null)
            {
                var xmlAgeCategoryNode = doc.ImportNode(this.AgeCategoryNode.SelectSingleNode("AgeCategory"), true);
                if (xmlHeader != null)
                {
                    xmlHeader.AppendChild(xmlAgeCategoryNode);
                }
            }

            if (this.CustomerNode.SelectSingleNode("Customers") != null)
            {
                var xmlCustomerNode = doc.ImportNode(this.CustomerNode.SelectSingleNode("Customers"), true);
                if (xmlHeader != null)
                {
                    xmlHeader.AppendChild(xmlCustomerNode);
                }
            }

            if (this.SportNode.SelectSingleNode("Sport") != null)
            {
                var xmlSportNode = doc.ImportNode(this.SportNode.SelectSingleNode("Sport"), true);
                if (xmlHeader != null)
                {
                    xmlHeader.AppendChild(xmlSportNode);
                }
            }

            var matchDoc = new XmlDocument();

            if (this.MatchNode.SelectSingleNode("Matches") != null)
            {
                var xmlMatchNode = matchDoc.ImportNode(this.MatchNode.SelectSingleNode("Matches"), true);
                matchDoc.AppendChild(xmlMatchNode);
            }

            var xmlMatchesElement = doc.CreateElement("Matches");
            var matchNodeList = matchDoc.SelectNodes("/Matches/Match");

            if (matchNodeList != null)
            {
                foreach (XmlNode mNode in matchNodeList)
                {
                    try
                    {
                        var matchId = Convert.ToInt32(mNode.SelectSingleNode("@Id").Value);

                        var xmlMatch = doc.CreateElement("Match");
                        xmlMatch.SetAttribute("Id", matchId.ToString());
                        xmlMatch.SetAttribute("startdate", mNode.SelectSingleNode("@startdate").Value);
                        xmlMatch.SetAttribute("starttime", mNode.SelectSingleNode("@starttime").Value);

                        this.SetXmlStadiumNode(matchId, doc, xmlMatch);

                        this.SetXmlPartialResultNode(matchId, doc, xmlMatch);

                        // if (this.StadiumNode != null && this.StadiumNode.SelectSingleNode("Stadium") != null)
                        // {
                        // var xmlStadiumNode = doc.ImportNode(this.StadiumNode.SelectSingleNode("Stadium"), true);
                        // xmlMatch.AppendChild(xmlStadiumNode);
                        // }
                        var test = from h in this.HomeTeamNode where h.Key == matchId select h.Value;
                        if (!test.Any())
                        {
                            continue;
                        }

                        Logger.Debug("Placing homeNode element");
                        XmlNode homeNode = (from h in this.HomeTeamNode where h.Key == matchId select h.Value).Single();

                        if (homeNode.SelectSingleNode("HomeTeam") != null)
                        {
                            var xmlHomeTeamNode = doc.ImportNode(homeNode.SelectSingleNode("HomeTeam"), true);
                            xmlMatch.AppendChild(xmlHomeTeamNode);
                        }

                        Logger.Debug("Placing awayNode element");
                        XmlNode awayNode = (from a in this.AwayTeamNode where a.Key == matchId select a.Value).Single();

                        if (awayNode.SelectSingleNode("AwayTeam") != null)
                        {
                            var xmlAwayTeamNode = doc.ImportNode(awayNode.SelectSingleNode("AwayTeam"), true);
                            xmlMatch.AppendChild(xmlAwayTeamNode);
                        }

                        Logger.Debug("Placing refereeTest element");
                        try
                        {
                            var xmlRefereeNode = (from r in this.RefereeNode where r.Key == matchId select r.Value).Single();

                            if (xmlRefereeNode.SelectSingleNode("Referees") != null)
                            {
                                var xmlReferee = doc.ImportNode(xmlRefereeNode.SelectSingleNode("Referees"), true);
                                xmlMatch.AppendChild(xmlReferee);
                            }
                        }
                        catch (Exception exception)
                        {
                            Logger.Debug(exception);
                        }

                        Logger.Debug("Placing Partial Results element");
                        try
                        {
                            var xmlPartialResultNode = (from p in this.PartialResultNode where p.Key == matchId select p.Value).Single();

                            if (xmlPartialResultNode.SelectSingleNode("PartialResult") != null)
                            {
                                var importNode = doc.ImportNode(xmlPartialResultNode.SelectSingleNode("PartialResult"), true);
                                xmlMatch.AppendChild(importNode);
                            }
                        }
                        catch (Exception exception)
                        {
                            Logger.Debug(exception);
                        }
                        // if (this.RefereeNode != null && this.RefereeNode.SelectSingleNode("Referees") != null)
                        // {
                        // XmlNode xmlRefereeNode = doc.ImportNode(this.RefereeNode.SelectSingleNode("Referees"), true);
                        // xmlMatch.AppendChild(xmlRefereeNode);
                        // }
                        try
                        {
                            if (this.PlayerNode != null)
                            {
                                if (this.PlayerNode.Any())
                                {
                                    var selectedNode = from p in this.PlayerNode where p.Key == matchId select p.Value;

                                    var xmlPlayerNode = new XmlDocument();
                                    var xmlDocuments = selectedNode as XmlDocument[] ?? selectedNode.ToArray();

                                    if (xmlDocuments.Any())
                                    {
                                        xmlPlayerNode = xmlDocuments.Single();
                                    }

                                    if (xmlPlayerNode.SelectSingleNode("Players") != null)
                                    {
                                        var xmlPlayer = doc.ImportNode(xmlPlayerNode.SelectSingleNode("Players"), true);
                                        xmlMatch.AppendChild(xmlPlayer);
                                    }
                                }
                            }
                        }
                        catch (Exception exception)
                        {
                            Logger.Error(exception);
                        }

                        try
                        {
                            if (this.IncidentNode != null)
                            {
                                var selectedNode = from r in this.IncidentNode where r.Key == matchId select r.Value;
                                var xmlIncidentNode = new XmlDocument();

                                var xmlDocuments = selectedNode as XmlDocument[] ?? selectedNode.ToArray();
                                if (xmlDocuments.Any())
                                {
                                    xmlIncidentNode = xmlDocuments.Single();
                                }

                                if (xmlIncidentNode.SelectSingleNode("Events") != null)
                                {
                                    var xmlIncidents = doc.ImportNode(xmlIncidentNode.SelectSingleNode("Events"), true);
                                    xmlMatch.AppendChild(xmlIncidents);
                                }
                            }
                        }
                        catch (Exception exception)
                        {
                            Logger.Debug(exception);
                        }

                        xmlMatchesElement.AppendChild(xmlMatch);
                    }
                    catch (Exception exception)
                    {
                        Logger.Error(exception);
                    }
                }
            }

            doc.AppendChild(xmlElement);

            // XmlNode matchNode = doc.ImportNode(matchDoc.SelectSingleNode("Matches"), true);
            var tnode = doc.SelectSingleNode("/SportsData/Tournament");
            if (tnode != null)
            {
                tnode.AppendChild(xmlMatchesElement);
            }

            var headerNode = doc.SelectSingleNode("/SportsData/Tournament/Header");
            if (headerNode != null)
            {
                if (this.DocumentTypeNode.SelectSingleNode("DocumentType") != null)
                {
                    var documentTypeNode = doc.ImportNode(this.DocumentTypeNode.SelectSingleNode("DocumentType"), true);
                    headerNode.AppendChild(documentTypeNode);
                }

                if (this.FileNode.SelectSingleNode("File") != null)
                {
                    var fileNode = doc.ImportNode(this.FileNode.SelectSingleNode("File"), true);
                    headerNode.AppendChild(fileNode);
                }
            }

            // node.OwnerDocument.ImportNode(matchNode, true);
            if (this.TournamentTableNode != null)
            {
                var xmlTournamentStandingsNode = doc.ImportNode(this.TournamentTableNode.SelectSingleNode("Standings"), true);
                if (xmlTournament != null)
                {
                    xmlTournament.AppendChild(xmlTournamentStandingsNode);
                }
            }

            return doc;
        }

        /// <summary>
        ///     The create document type node.
        /// </summary>
        public void CreateDocumentTypeNode()
        {
            var doc = new XmlDocument();
            var xmlElement = doc.CreateElement("DocumentType");

            if (!this.HasMatchFacts && !this.HasStanding)
            {
                xmlElement.SetAttribute("type", "resultatmelding");
            }
            else if (this.HasMatchFacts)
            {
                xmlElement.SetAttribute("type", "kampfakta");
            }
            else
            {
                xmlElement.SetAttribute("type", "sportsresults");
            }

            doc.AppendChild(xmlElement);

            this.DocumentTypeNode = doc;
        }

        /// <summary>
        /// The create date time string.
        /// </summary>
        /// <param name="dateTime">
        /// The date time.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string CreateDateTimeString(DateTime dateTime)
        {
            var value = dateTime.ToString("yyyy-MM-dd") + "T" + dateTime.ToString("HH:mm:sszzz");
            Logger.DebugFormat("Generated string: {0}", value.Replace(".", ":"));
            return value.Replace(".", ":");

        }

        /// <summary>
        /// The create file node.
        /// </summary>
        /// <param name="document">
        /// The document.
        /// </param>
        public void CreateFileNode(Document document)
        {
            var doc = new XmlDocument();
            var xmlElement = doc.CreateElement("File");
            var xmlElementName = doc.CreateElement("Name");
            xmlElementName.SetAttribute("FullName", document.FullName);
            xmlElementName.SetAttribute("Type", "xml");
            xmlElementName.InnerText = document.Filename;

            var xmlCreatedDateTime = doc.CreateElement("Created");
            xmlCreatedDateTime.InnerText = this.CreateDateTimeString(document.Created);

            var xmlVersion = doc.CreateElement("Version");
            xmlVersion.InnerText = document.Version.ToString();

            xmlElement.AppendChild(xmlElementName);
            xmlElement.AppendChild(xmlCreatedDateTime);
            xmlElement.AppendChild(xmlVersion);
            doc.AppendChild(xmlElement);

            this.FileNode = doc;
        }

        /// <summary>
        /// The create home team node.
        /// </summary>
        /// <param name="match">
        /// The match.
        /// </param>
        internal void CreateHomeTeamNode(Match match)
        {
            try
            {
                var doc = new XmlDocument();

                var xmlElement = doc.CreateElement("HomeTeam");
                xmlElement.SetAttribute("Id", match.HomeTeamId.ToString());

                var xmlName = doc.CreateElement("Name");

                if (match.HomeTeamShortName != null)
                {
                    xmlName.InnerText = match.HomeTeamShortName.Trim();
                }
                else
                {
                    xmlName.InnerText = match.HomeTeamName.Trim();
                }

                var xmlGoals = doc.CreateElement("Totalgoals");
                xmlGoals.InnerText = match.HomeTeamGoals.ToString();

                xmlElement.AppendChild(xmlName);
                xmlElement.AppendChild(xmlGoals);

                doc.AppendChild(xmlElement);

                if (this.HomeTeamNode == null)
                {
                    Logger.Debug("Creating a new HomeTeamNode for match " + match.Id);
                    this.HomeTeamNode = new Dictionary<int, XmlDocument>();
                }

                if (this.HomeTeamNode.Keys.Contains(match.Id))
                {
                    Logger.Debug("Match " + match.Id + "is already in the dictionary");
                    return;
                }

                Logger.Debug("Adding home team " + match.HomeTeamName + " to HomeTeamNode-Dictionary " + match.Id);
                this.HomeTeamNode.Add(match.Id, doc);
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }
        }

        /// <summary>
        /// The create incident node.
        /// </summary>
        /// <param name="list">
        /// The list.
        /// </param>
        /// <param name="matchId">
        /// The match Id.
        /// </param>
        internal void CreateIncidentNode(List<MatchIncident> list, int matchId)
        {
            try
            {
                var doc = new XmlDocument();

                var xmlElement = doc.CreateElement("Events");

                foreach (var incident in list)
                {
                    var xmlEvent = doc.CreateElement("Event");
                    xmlEvent.SetAttribute("id", incident.IncidentTypeId.ToString());
                    xmlEvent.SetAttribute("teamid", incident.TeamId.ToString());
                    xmlEvent.SetAttribute("value", incident.IncidentShort);
                    xmlEvent.SetAttribute("minute", incident.Time.ToString());
                    xmlEvent.SetAttribute("items", incident.Value.ToString());

                    // xmlPlayer.SetAttribute("id", incident);
                    if (incident.FirstName != null)
                    {
                        var xmlPlayerName = doc.CreateElement("Player");
                        xmlPlayerName.SetAttribute("id", incident.PersonId.ToString());
                        xmlPlayerName.SetAttribute("firstName", incident.FirstName);
                        xmlPlayerName.SetAttribute("lastName", incident.LastName);
                        var fullname = incident.FirstName + " " + incident.LastName;
                        xmlPlayerName.SetAttribute("full", fullname);
                        xmlPlayerName.InnerText = fullname;

                        xmlEvent.AppendChild(xmlPlayerName);
                    }

                    xmlElement.AppendChild(xmlEvent);
                    doc.AppendChild(xmlElement);
                }

                if (this.IncidentNode == null)
                {
                    this.IncidentNode = new Dictionary<int, XmlDocument>();
                }

                // Checking if the key exists or not
                if (this.IncidentNode.ContainsKey(matchId))
                {
                    this.IncidentNode[matchId] = doc;
                }
                else
                {
                    this.IncidentNode.Add(matchId, doc);
                }
            }
            catch (Exception exception)
            {
                Mail.SendException(exception);
                Logger.Error(exception);
            }
        }

        /// <summary>
        /// The create match node.
        /// </summary>
        /// <param name="matches">
        /// The matches.
        /// </param>
        public void CreateMatchNode(List<Match> matches)
        {
            var doc = new XmlDocument();
            var xmlElement = doc.CreateElement("Matches");
            foreach (var match in matches)
            {
                var xmlMatchElement = doc.CreateElement("Match");
                xmlMatchElement.SetAttribute("Id", match.Id.ToString());
                if (match.MatchDate != null)
                {
                    xmlMatchElement.SetAttribute("startdate", match.MatchDate.Value.ToShortDateString());
                }
                xmlMatchElement.SetAttribute("starttime", match.MatchStartTime.ToString());

                var xmlMatchResultFinal = doc.CreateElement("MatchResult");
                xmlMatchResultFinal.SetAttribute("type", "Sluttresultat");
                xmlMatchResultFinal.SetAttribute("hometeam", match.HomeTeamGoals.ToString());
                xmlMatchResultFinal.SetAttribute("awayteam", match.AwayTeamGoals.ToString());
                xmlMatchElement.AppendChild(xmlMatchResultFinal);

                var xmlMatchResultPause = doc.CreateElement("MatchResult");
                xmlMatchResultPause.SetAttribute("type", "Pauseresultat");
                xmlMatchResultPause.SetAttribute("hometeam", match.PartialResult);
                xmlMatchElement.AppendChild(xmlMatchResultPause);

                xmlElement.AppendChild(xmlMatchElement);
            }

            doc.AppendChild(xmlElement);

            this.MatchNode = doc;
        }

        /// <summary>
        /// The create player node.
        /// </summary>
        /// <param name="list">
        /// The list.
        /// </param>
        /// <param name="matchId">
        /// The match Id.
        /// </param>
        internal void CreatePlayerNode(List<Player> list, int matchId)
        {
            var doc = new XmlDocument();

            var xmlElement = doc.CreateElement("Players");
            foreach (var player in list)
            {
                var xmlPlayer = doc.CreateElement("Player");
                xmlPlayer.SetAttribute("id", player.Id.ToString());
                xmlPlayer.SetAttribute("position", player.Position);
                xmlPlayer.SetAttribute("teamid", player.TeamId.ToString());
                xmlPlayer.SetAttribute("shirtnumber", player.PlayerShirtNumber);

                if (player.TeamCaptain == "true")
                {
                    xmlPlayer.SetAttribute("captain", "captain");
                }
                else
                {
                    xmlPlayer.SetAttribute("captain", string.Empty);
                }

                var xmlPlayerName = doc.CreateElement("PlayerName");
                xmlPlayerName.SetAttribute("FirstName", player.FirstName);
                xmlPlayerName.SetAttribute("LastName", player.LastName);
                xmlPlayerName.InnerText = player.FirstName + " " + player.LastName;
                xmlPlayer.AppendChild(xmlPlayerName);
                xmlElement.AppendChild(xmlPlayer);
            }

            if (this.PlayerNode == null)
            {
                this.PlayerNode = new Dictionary<int, XmlDocument>();
            }

            doc.AppendChild(xmlElement);
            if (!this.PlayerNode.Keys.Contains(matchId))
            {
                this.PlayerNode.Add(matchId, doc);
            }
        }

        /// <summary>
        /// The create referee node.
        /// </summary>
        /// <param name="referees">
        /// The referees.
        /// </param>
        /// <param name="matchId">
        /// the match Id
        /// </param>
        internal void CreateRefereeNode(List<Referee> referees, int matchId)
        {
            if (this.RefereeNode == null)
            {
                this.RefereeNode = new Dictionary<int, XmlDocument>();
            }

            try
            {
                var doc = new XmlDocument();

                var xmlElement = doc.CreateElement("Referees");

                foreach (var referee in referees)
                {
                    var xmlReferee = doc.CreateElement("Referee");
                    xmlReferee.SetAttribute("type", referee.RefereeType);
                    xmlReferee.SetAttribute("club", referee.ClubName);
                    xmlReferee.SetAttribute("clubid", referee.ClubId.ToString());
                    xmlReferee.SetAttribute("firstname", referee.FirstName);
                    xmlReferee.SetAttribute("lastname", referee.LastName);
                    xmlReferee.InnerText = referee.RefereeName;

                    xmlElement.AppendChild(xmlReferee);
                }

                doc.AppendChild(xmlElement);
                if (!this.RefereeNode.ContainsKey(matchId))
                {
                    this.RefereeNode.Add(matchId, doc);
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }
        }

        /// <summary>
        /// The create sport node.
        /// </summary>
        /// <param name="organization">
        /// The organization.
        /// </param>
        internal void CreateSportNode(Federation organization)
        {
            var doc = new XmlDocument();
            var xmlElement = doc.CreateElement("Sport");

            // todo: create a discipline attribute telling which discipline this is (mostly the same as sport, but can differ)
            // todo: Add discipline to organization object - must be for this particular job only
            xmlElement.SetAttribute("discipline", organization.DisciplineName);
            xmlElement.SetAttribute("disciplineid", organization.DisciplineId.ToString());
            xmlElement.InnerText = organization.Sport;

            doc.AppendChild(xmlElement);

            this.SportNode = doc;
        }

        /// <summary>
        /// The create stadium node.
        /// </summary>
        /// <param name="venue">
        /// The VenueExtended.
        /// </param>
        /// <param name="spectators">
        /// numbers of specators who viewed this event
        /// </param>
        /// <param name="matchId">
        /// Id of the match
        /// </param>
        /// ///
        internal void CreateStadiumNode(Venue venue, int spectators, int matchId)
        {
            try
            {
                if (this.StadiumNode == null)
                {
                    Logger.DebugFormat("Creating a new AwayTeamNode for match {0}", matchId);
                    this.StadiumNode = new Dictionary<int, XmlDocument>();
                }

                var doc = new XmlDocument();
                var xmlElement = doc.CreateElement("Stadium");
                if (venue != null && venue.VenueId > 0)
                {
                    xmlElement.SetAttribute("latitude", venue.Latitude.ToString(CultureInfo.InvariantCulture));
                    xmlElement.SetAttribute("longitude", venue.Longitude.ToString(CultureInfo.InvariantCulture));
                    xmlElement.SetAttribute("localcouncilname", venue.LocalCouncilName);
                    xmlElement.SetAttribute("spectators", spectators.ToString(CultureInfo.InvariantCulture));
                    xmlElement.InnerText = venue.VenueName.Trim();
                }

                doc.AppendChild(xmlElement);

                if (!this.StadiumNode.ContainsKey(matchId))
                {
                    this.StadiumNode.Add(matchId, doc);
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }
        }

        /// <summary>
        /// The create tournament standing node.
        /// </summary>
        /// <param name="tournamentTables">
        /// The tournament tables.
        /// </param>
        internal void CreateTournamentStandingNode(List<TeamResult> tournamentTables)
        {
            var doc = new XmlDocument();
            var xmlElement = doc.CreateElement("Standings");
            var counter = 0;
            foreach (var tournamentTable in tournamentTables)
            {
                counter++;
                var xmlTeam = doc.CreateElement("Team");
                xmlTeam.SetAttribute("Id", tournamentTable.OrgId.ToString());
                xmlTeam.SetAttribute("TablePosition", counter.ToString());

                var xmlTeamName = doc.CreateElement("Name");
                xmlTeamName.InnerText = tournamentTable.OrgName.Trim();
                xmlTeam.AppendChild(xmlTeamName);

                var xmlPoints = doc.CreateElement("Points");
                xmlPoints.SetAttribute("Total", tournamentTable.TotalPoints.ToString());

                var xmlHomePoints = doc.CreateElement("HomePoints");
                xmlHomePoints.InnerText = tournamentTable.PointsHome.ToString();

                var xmlAwayPoints = doc.CreateElement("AwayPoints");
                xmlAwayPoints.InnerText = tournamentTable.AwayPoints.ToString();

                xmlPoints.AppendChild(xmlHomePoints);
                xmlPoints.AppendChild(xmlAwayPoints);
                xmlTeam.AppendChild(xmlPoints);

                var xmlGoals = doc.CreateElement("Goals");
                xmlGoals.SetAttribute("TotalScored", tournamentTable.GoalsScored.ToString());
                xmlGoals.SetAttribute("TotalAgainst", tournamentTable.GoalsConceeded.ToString());

                var xmlHomeGoals = doc.CreateElement("Home");
                xmlHomeGoals.SetAttribute("Scored", tournamentTable.GoalsScoredHome.ToString());
                xmlHomeGoals.SetAttribute("Against", tournamentTable.GoalsConcededHome.ToString());

                var xmlAwayGoals = doc.CreateElement("Away");
                xmlAwayGoals.SetAttribute("Scored", tournamentTable.GoalsScoredAway.ToString());
                xmlAwayGoals.SetAttribute("Against", tournamentTable.GoalsConcededAway.ToString());

                xmlGoals.AppendChild(xmlHomeGoals);
                xmlGoals.AppendChild(xmlAwayGoals);
                xmlTeam.AppendChild(xmlGoals);

                var xmlMatches = doc.CreateElement("Matches");
                xmlMatches.SetAttribute("Total", tournamentTable.Matches.ToString());
                xmlMatches.SetAttribute("Draw", tournamentTable.Draws.ToString());
                xmlMatches.SetAttribute("Lost", tournamentTable.Losses.ToString());
                xmlMatches.SetAttribute("Won", tournamentTable.Victories.ToString());

                var xmlHomeMatches = doc.CreateElement("HomeMatches");
                xmlHomeMatches.SetAttribute("Won", tournamentTable.VictoriesFulltimeHome.ToString());
                xmlHomeMatches.SetAttribute("Draw", tournamentTable.DrawsHome.ToString());
                xmlHomeMatches.SetAttribute("Lost", tournamentTable.LossesHome.ToString());
                xmlHomeMatches.SetAttribute("Total", tournamentTable.MatchesHome.ToString());

                var xmlAwayMatches = doc.CreateElement("AwayMatches");
                xmlAwayMatches.SetAttribute("Won", tournamentTable.VictoriesFulltimeAway.ToString());
                xmlAwayMatches.SetAttribute("Draw", tournamentTable.DrawsAway.ToString());
                xmlAwayMatches.SetAttribute("Lost", tournamentTable.LossesAway.ToString());
                xmlAwayMatches.SetAttribute("Total", tournamentTable.MatchesAway.ToString());

                xmlMatches.AppendChild(xmlHomeMatches);
                xmlMatches.AppendChild(xmlAwayMatches);
                xmlTeam.AppendChild(xmlMatches);

                xmlElement.AppendChild(xmlTeam);
            }

            doc.AppendChild(xmlElement);

            this.TournamentTableNode = doc;
        }
        /// <summary>
        /// The create away team node.
        /// </summary>
        /// <param name="match">
        /// The match.
        /// </param>
        public void CreateAwayTeamNode(Match match)
        {
            try
            {
                var doc = new XmlDocument();

                var xmlElement = doc.CreateElement("AwayTeam");
                xmlElement.SetAttribute("Id", match.AwayTeamId.ToString());

                var xmlName = doc.CreateElement("Name");
                if (match.AwayTeamShortName != null)
                {
                    xmlName.InnerText = match.AwayTeamShortName.Trim();
                }
                else
                {
                    xmlName.InnerText = match.AwayTeamName.Trim();
                }

                var xmlGoals = doc.CreateElement("Totalgoals");
                xmlGoals.InnerText = match.AwayTeamGoals.ToString();

                xmlElement.AppendChild(xmlName);
                xmlElement.AppendChild(xmlGoals);

                doc.AppendChild(xmlElement);

                if (this.AwayTeamNode == null)
                {
                    Logger.DebugFormat("Creating a new AwayTeamNode for match {0} ", match.Id);
                    this.AwayTeamNode = new Dictionary<int, XmlDocument>();
                }

                if (this.AwayTeamNode.Keys.Contains(match.Id))
                {
                    Logger.DebugFormat("Match {0} is already in the dictionary", match.Id);
                    return;
                }

                Logger.DebugFormat("Adding away team {0} AwayteamNode-Dictonary for matchId: {1}", match.AwayTeamName, match.Id);
                this.AwayTeamNode.Add(match.Id, doc);
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                Logger.Error(exception.StackTrace);
            }
        }

        /// <summary>
        /// The create organization node.
        /// </summary>
        /// <param name="organization">
        /// The organization.
        /// </param>
        public void CreateOrganizationNode(Federation organization)
        {
            var doc = new XmlDocument();
            var xmlElement = doc.CreateElement("Organisation");
            xmlElement.SetAttribute("Name", organization.NameShort);
            xmlElement.InnerText = organization.Name;

            doc.AppendChild(xmlElement);

            this.OrganizationNode = doc;
        }

        /// <summary>
        /// The create tournament node.
        /// </summary>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <param name="tournamentRound">
        /// The tournament round.
        /// </param>
        /// <param name="matchDate">
        /// The match date.
        /// </param>
        /// <param name="document">
        /// The Document object 
        /// </param>
        public void CreateTournamentNode(Tournament tournament, string tournamentRound, DateTime matchDate, Document document)
        {
            // xmlRoot.SetAttribute("Type", _nffDatafileCreator.RenderResult ? "resultat" : "terminliste");
            var doc = new XmlDocument();

            // Create Tournament-MetaInfo node
            var xmlElement = doc.CreateElement("Tournament");
            var xmlTournamentMetaInfo = doc.CreateElement("Tournament-MetaInfo");
            xmlTournamentMetaInfo.SetAttribute("Id", tournament.Id.ToString());
            xmlTournamentMetaInfo.SetAttribute("Name", tournament.Name.Trim());
            xmlTournamentMetaInfo.SetAttribute("Number", tournament.Number.ToString());
            xmlTournamentMetaInfo.SetAttribute("Division", tournament.Division.ToString());
            xmlTournamentMetaInfo.SetAttribute("Round", tournamentRound);

            // var documentId = "T" + tournament.Id + "_" + matchDate.Year + matchDate.Month.ToString().PadLeft(2, '0') + matchDate.Day.ToString().PadLeft(2, '0');
            var documentId = document.Filename;

            if (document.DocumentType == DocumentType.MatchFact)
            {
                documentId += "_Fakta";
            }
            else if (document.DocumentType == DocumentType.Standing)
            {
                documentId += "_ResTab";
            }
            else if (document.DocumentType == DocumentType.Result)
            {
                documentId += "_Res";
            }

            xmlElement.SetAttribute("doc-id", documentId);
            xmlElement.SetAttribute("Type", this.RenderResult ? "resultat" : "terminliste");
            xmlElement.AppendChild(xmlTournamentMetaInfo);

            doc.AppendChild(xmlElement);

            this.TournamentNode = doc;
        }

        /// <summary>
        ///     The create header node.
        /// </summary>
        public void CreateHeaderNode()
        {
            var doc = new XmlDocument();

            var xmlElement = doc.CreateElement("Header");

            doc.AppendChild(xmlElement);

            this.HeaderNode = doc;
        }

        /// <summary>
        /// The create customer node.
        /// </summary>
        /// <param name="customers">
        /// The customers.
        /// </param>
        public void CreateCustomerNode(List<Customer> customers)
        {
            var doc = new XmlDocument();
            var xmlCustomers = doc.CreateElement("Customers");

            // Now we shall add customers
            if (customers == null)
            {
                return;
            }

            foreach (var customer in customers)
            {
                var xmlElement = doc.CreateElement("Customer");
                var xmlCustomerName = doc.CreateElement("CustomerName");
                xmlCustomerName.SetAttribute("Id", customer.Id.ToString());
                xmlCustomerName.SetAttribute("RemoteCustomerId", customer.RemoteCustomerId.ToString());
                var xmlText = doc.CreateTextNode(customer.Name);
                xmlCustomerName.AppendChild(xmlText);
                xmlElement.AppendChild(xmlCustomerName);
                xmlCustomers.AppendChild(xmlElement);
            }

            doc.AppendChild(xmlCustomers);

            this.CustomerNode = doc;
        }

        /// <summary>
        /// The create age category node.
        /// </summary>
        /// <param name="ageCategory">
        /// The age category.
        /// </param>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        public void CreateAgeCategoryNode(AgeCategory ageCategory, Tournament tournament)
        {
            // We need to get the definition Id which we have stored some where
            var doc = new XmlDocument();
            var xmlAgeCategory = doc.CreateElement("AgeCategory");

            Logger.Info("We are creating the age category");
            if (tournament.AgeCategoryDefinitionId != 0)
            {
                Logger.Info("AgeCategoryDefinitionId is not zero");

                if (ageCategory != null)
                {
                    Logger.InfoFormat("AgeCategory is: {0}", ageCategory.Name);
                }

                switch (tournament.AgeCategoryDefinitionId)
                {
                    case 2:
                        xmlAgeCategory.SetAttribute("min-age", ageCategory.FromAge.ToString());
                        xmlAgeCategory.SetAttribute("max-age", ageCategory.ToAge.ToString());
                        xmlAgeCategory.SetAttribute("name", ageCategory.Name);
                        xmlAgeCategory.SetAttribute("id", ageCategory.Id.ToString());
                        if (ageCategory.Name.ToLower().Contains("jenter") || ageCategory.Name.ToLower().Contains("gutter"))
                        {
                            xmlAgeCategory.InnerText = "aldersbestemt";
                        }
                        else
                        {
                            xmlAgeCategory.InnerText = "senior";
                        }

                        break;

                    default:
                        xmlAgeCategory.SetAttribute("min-age", ageCategory.FromAge.ToString());
                        xmlAgeCategory.SetAttribute("max-age", ageCategory.ToAge.ToString());
                        xmlAgeCategory.SetAttribute("name", ageCategory.Name);
                        xmlAgeCategory.SetAttribute("id", ageCategory.Id.ToString());

                        xmlAgeCategory.InnerText = "aldersbestemt";
                        break;
                }
            }
            else
            {
                // In case it is zero (or null)
                if (ageCategory != null)
                {
                    Logger.InfoFormat("AgeCategory is: {0}", ageCategory.Name);

                    xmlAgeCategory.SetAttribute("min-age", ageCategory.FromAge.ToString());
                    xmlAgeCategory.SetAttribute("max-age", ageCategory.ToAge.ToString());
                    xmlAgeCategory.SetAttribute("name", ageCategory.Name);
                    xmlAgeCategory.SetAttribute("id", ageCategory.Id.ToString());
                    xmlAgeCategory.InnerText = "aldersbestemt";
                }
                else
                {
                    if (tournament.Name.ToLower().Contains("divisjon") || tournament.Name.ToLower().Contains("senior") || tournament.Name.ToLower().Contains("menn") || tournament.Name.ToLower().Contains("kvinner"))
                    {
                        xmlAgeCategory.SetAttribute("min-age", "18");
                        xmlAgeCategory.SetAttribute("max-age", "35");
                        xmlAgeCategory.SetAttribute("name", "senior");
                        xmlAgeCategory.SetAttribute("id", "1");
                        xmlAgeCategory.InnerText = "senior";
                    }
                    else
                    {
                        xmlAgeCategory.SetAttribute("min-age", "12");
                        xmlAgeCategory.SetAttribute("max-age", "99");
                        xmlAgeCategory.SetAttribute("name", "aldersbestemt");
                        xmlAgeCategory.SetAttribute("id", "2");
                        xmlAgeCategory.InnerText = "aldersbestemt";
                    }
                }
            }

            doc.AppendChild(xmlAgeCategory);
            this.AgeCategoryNode = doc;
        }
    }
}
