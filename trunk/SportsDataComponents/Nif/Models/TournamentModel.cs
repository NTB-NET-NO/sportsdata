﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Classes.Enums;
using NTB.SportsData.Components.Nif.Creators;
using NTB.SportsData.Components.Nif.Gatherers.DataGatherers;
using NTB.SportsData.Components.Nif.Gatherers.Interfaces;
using NTB.SportsData.Components.PublicRepositories;

namespace NTB.SportsData.Components.Nif.Models
{
    public class TournamentModel
    {

        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(TournamentModel));

        private ITeamSportGatherer _gatherer;

        public Tournament Tournament { get; set; }

        public List<MatchModel> Matches { get; set; }

        public List<TeamResult> TournamentStanding { get; set; }

        public List<Customer> Customers { get; set; }

        public Sport Sport { get; set; }

        public Federation Federation { get; set; }

        public AgeCategory AgeCategory { get; set; }

        public EventType EventType { get; set; }

        /// <summary>
        ///     Gets or sets the data params.
        /// </summary>
        /// 
        public DataFileParams DataParams { get; set; }

        public TournamentModel()
        {
            this._gatherer = new TeamSportGatherer();
        }

        /*
         * In order to make this new MVC-ish design work, I must add some methods in this class/model
         */

        /// <summary>
        /// Set the Event type
        /// </summary>
        /// <param name="tournamentId">the tournament id used to get the event type id</param>
        /// <returns>return the complete event type definition</returns>
        public void GetEventType(int tournamentId)
        {
            var repository = new EventTypeRepository();
            var eventType = repository.GetEventTypeByTournamentId(tournamentId);

            if (eventType == null)
            {
                // We will get the data again and this time with the default value from the config file
                tournamentId = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultEventTypeId"]);
                eventType = repository.GetEventTypeById(tournamentId);
            }

            // Store this value in the object
            this.EventType = eventType;
        }

        public Federation GetFederationByOrgId(int orgId)
        {
            var federationRepository = new FederationRepository();
            return federationRepository.GetFederationById(orgId);
        }

        public int GetUnknownAgeCategoryDefinition(Tournament tournament)
        {
            // Doing something if the definition is not set
            if (tournament.AgeCategoryDefinitionId == 0)
            {
                var tournamentName = tournament.Name.ToLower();
                if (tournamentName.Contains("senior") ||
                    tournamentName.Contains("elite")
                    )
                {
                    return 2;
                }

                return 1;
            }

            return tournament.AgeCategoryDefinitionId;
        }

        public AgeCategory GetUnknownAgeCategory(int tournamentId)
        {
            // Get the tournament
            var tournamentRepository = new TournamentRepository();
            var tournament = tournamentRepository.Get(tournamentId);

            // Get the tournament Age Category Definition based on the name of the tournament - if the id is zero that is
            // Not a very fancy function, could be more complicated, but sufficient enough at the moment
            tournament.AgeCategoryDefinitionId = this.GetUnknownAgeCategoryDefinition(tournament);

            // todo: Finish this part of the code so we can get the general age category for any sport
            // Doing something if the the definition is set, but not the age category
            if (tournament.AgeCategoryDefinitionId == 1)
            {
                var ageRepository = new AgeCategoryRepository();
                // ageRepository.
            }

            if (tournament.AgeCategoryDefinitionId == 2)
            {

            }

            return new AgeCategory();
   ;     }
        /// <summary>
        /// The get age category.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The <see cref="AgeCategory"/>.
        /// </returns>
        public AgeCategory GetAgeCategory(int tournamentId)
        {
            // Connect to the local age Category Repository
            var ageCategoryRepository = new AgeCategoryRepository();

            // Get the ageCategory object from the database
            var ageCategory = ageCategoryRepository.GetAgeCategoryByTournamentId(tournamentId, this.DataParams.SportId);

            // If we cannot find anything, we will go to the source and add
            if (ageCategory == null)
            {
                var remoteAgeCategory = this._gatherer.GetTournamentAgeCategory(tournamentId);

                if (remoteAgeCategory.Count == 0)
                {
                    ageCategory = this.GetUnknownAgeCategory(tournamentId);
                }
                else
                {
                    foreach (var category in remoteAgeCategory)
                    {
                        category.SportId = this.DataParams.SportId;
                        ageCategory = category;
                        ageCategoryRepository.InsertAgeCategory(category, this.DataParams.SportId);
                        ageCategoryRepository.UpdateTournamentAgeCategory(category, tournamentId);
                    }
                }
                
            }

            // If we can get the Age Category Definition (either 1 or 2 based on if it is senior or aldersbestemt), we return that inside the age category
            return ageCategory ?? ageCategoryRepository.GetAgeCategoryDefinitionByTournamentId(tournamentId, this.DataParams.SportId);
        }

        public void CreateTournamentModelFromSync(double interval, Tournament tournament, List<Match> matches)
        {
            try
            {
                this.AgeCategory = this.GetAgeCategory(tournament.Id);

                this.TournamentStanding = null;
                if (tournament.PublishTournamentTable)
                {
                    this.TournamentStanding = this.GetTournamentStanding(tournament);
                }

                var matchCreator = new MatchResultCreator {DataParams = this.DataParams};

                this.Matches = matchCreator.GenerateMatchModelFromSync(matches);
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }
        }

        public void CreateTournamentModelFromCalendar(Tournament tournament, List<Match> matches)
        {
            try
            {
                this.AgeCategory = this.GetAgeCategory(tournament.Id);

                // Just to make sure we don't have a table 
                this.TournamentStanding = null;
                if (tournament.PublishTournamentTable)
                {
                    this.TournamentStanding = this.GetTournamentStanding(tournament);
                }

                var matchCreator = new MatchResultCreator {DataParams = this.DataParams};

                this.Matches = matchCreator.GenerateMatchModelFromSync(matches);
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }
        }

        /// <summary>
        /// Creates the tournament model from a maintenance call
        /// </summary>
        /// <param name="tournament">The tournament</param>
        public void CreateTournamentModelFromMaintenance(Tournament tournament)
        {
            //var gatherer = new SynchronizeGatherer();
            //gatherer.GetChangesMatchResults(dataFileParams, interval);
            this.Tournament = tournament;
            if (tournament.TournamentTypeId == 0)
            {
                tournament = this.GetTournamentByTournamentId(tournament.Id);
                this.Tournament = tournament;
            }
            
            this.AgeCategory = this.GetAgeCategory(tournament.Id);

            this.TournamentStanding = this.GetTournamentStanding(tournament);

            var matchCreator = new MatchResultCreator { DataParams = this.DataParams };
            // Group the matches together by date (which is important when we do maintenance
            var matches = matchCreator.GetMatchesGroupedByDate(tournament);
            
            // Checking if we have a list of matches to process
            if (matches == null)
            {
                // No matches to process, so let's continue the loop
                return;
            }
            // Now that we have a group, we shall use it
            var matchModels = matchCreator.ProcessGroupedMatches(matches, tournament);

            this.Matches = matchModels;

        }

        public List<ChangeInfo> GetTodaysChangesForTeamResult()
        {
            this._gatherer = new TeamSportGatherer();
            if (this.DataParams == null)
            {
                Logger.Error("DataParams is null. We will throw an exception");
                throw new SportsDataException("Data Params is not set. We must quit!");
            }

            var results = this._gatherer.GetTodaysChangesResultTeam(this.DataParams.FederationUserKey,
                this.DataParams.FederationUserPassword,
                this.DataParams.DateStart.AddSeconds(0),
                this.DataParams.DateEnd.AddSeconds(0));

            if (!results.Any())
            {
                return null;
            }

            return results;
        }

        public List<ChangeInfo> GetLatestChangesForTeamResults()
        {
            // get a list of tournaments
            this._gatherer = new TeamSportGatherer();
            if (this.DataParams == null)
            {
                Logger.Error("DataParams is null. We will throw an exception");
                throw new SportsDataException("Data Params is not set. We must quit!");
            }

            var results = this._gatherer.GetChangesResultTeam(this.DataParams.Interval,
                this.DataParams.FederationUserKey,
                this.DataParams.FederationUserPassword,
                this.DataParams.DateStart.AddSeconds(0),
                this.DataParams.DateEnd.AddSeconds(0));

            if (!results.Any())
            {
                return null;
            }

            return results;
        }

        public Tournament GetTournamentByMatchId(int matchId)
        {
            var tournamentRepository = new TournamentRepository();
            var tournament = tournamentRepository.GetTournamentByMatchId(matchId);
            var remotetournament = this._gatherer.GetTournamentByMatchId(matchId);

            return remotetournament;
            

        }

        public Tournament GetTournamentByTournamentId(int tournamentId)
        {
            var tournamentRepository = new TournamentRepository();
            var tournament = tournamentRepository.Get(tournamentId);
            
            var remotetournament = this._gatherer.GetTournamentById(tournamentId);

            if (tournament.Id != 0)
            {
                // Need to make sure we have the type id
                /*
                 * 1 = S Serie
                 * 2 = C Cup
                 * 3 = I Enkeltkamper
                */
                tournament.TournamentTypeId = remotetournament.TournamentTypeId;
                if (tournament.DisciplineId == 0)
                {
                    tournament.DisciplineId = remotetournament.DisciplineId;
                    tournamentRepository.UpdateTournamentDisciplineId(tournament);
                }

                // Checks if the two values are different
                if (tournament.PublishResult != remotetournament.PublishResult)
                {
                    tournament.PublishResult = remotetournament.PublishResult;
                    tournament.PublishTournamentTable = remotetournament.PublishTournamentTable;
                    tournamentRepository.UpdateTournamentPublishStatus(tournament);
                }

                if (tournament.TournamentTypeId == 2)
                {
                    tournament.PublishTournamentTable = false;
                }
            }

            try
            {
                if (tournament.Id == 0)
                {
                    tournament = remotetournament;
                    // Getting the tournament
                    var tournamentAgeCategory = this._gatherer.GetTournamentAgeCategory(tournamentId);

                    // This is a quick and dirty fix that I have to deal with later
                    // @todo techical debt 100
                    if (tournamentAgeCategory.Count > 0)
                    {
                        tournament.AgeCategoryId = tournamentAgeCategory[0].Id;
                        tournament.AgeCategoryDefinitionId = tournamentAgeCategory[0].AgeCategoryDefinitionId;
                    }

                    tournament.SportId = this.Sport.Id;
                    tournamentRepository.InsertOne(tournament);

                    // tournament.DisciplineId = remotetournament.DisciplineId;
                    tournamentRepository.UpdateTournamentDisciplineId(tournament);

                    tournament.PublishTournamentTable = remotetournament.PublishTournamentTable;
                    
                    tournamentRepository.UpdateTournamentPublishStatus(tournament);
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }

            return tournament;
        }

        public List<Match> GetMatchesByTournamentId(int tournamentId, DateTime matchDate)
        {
            // Getting the matches for this tournament
            // var listOfMatches = (from m in matches where m.TournamentId == id select m).ToList();

            var listOfMatches = this._gatherer.GetTournamentMatchesByTournamentIdAndDate(tournamentId, matchDate);
            if (listOfMatches.Count == 0)
            {
                Logger.Info("No matches for this tournament. It could be that the match was played earlier, but registered today");
                return null;
            }
            return listOfMatches;
        }

        // this.ComputeGroupedMatches(groupedMatches, tournament);
        /// <summary>
        ///     The create results.
        /// </summary>
        public Dictionary<Tournament, List<Match>> GetTournamentsAndMatches(List<MatchExtended> matches)
        {
            // Define a list of tournaments
            var tournamentsDictionary = new Dictionary<Tournament, List<Match>>();

            var tournamentRepository = new TournamentRepository();
            foreach (var match in matches)
            {
                // Using the date
                var matchDate = match.Match.MatchDate;
                
                if (matchDate == null)
                {
                    matchDate = DateTime.Today;
                }

                var tournamentId = match.Match.TournamentId;
                var tournamentMatches = this.GetMatchesByTournamentId(tournamentId, Convert.ToDateTime(matchDate));
                //tournamentsDictionary.Add(); = this.GetTournamentsAndMatches(match);

                // Getting the tournament
                var tournament = this._gatherer.GetTournamentById(tournamentId);
                var tournamentAgeCategory = this._gatherer.GetTournamentAgeCategory(tournamentId);

                if (tournamentAgeCategory.Count > 0)
                {
                    tournament.AgeCategoryId = tournamentAgeCategory[0].Id;
                    tournament.AgeCategoryDefinitionId = tournamentAgeCategory[0].AgeCategoryDefinitionId;
                }

                tournament.SportId = this.Sport.Id;

                var localTournament = tournamentRepository.Get(tournament.Id);

                if (localTournament.Id == 0)
                {
                    tournamentRepository.InsertOne(tournament);
                }

                // Adding tournament to dictionary
                tournamentsDictionary.Add(tournament, tournamentMatches);
            }

            return tournamentsDictionary;
        }

        public Sport GetSport(int orgId)
        {
            var sportRepository = new SportRepository();
            var sport = sportRepository.GetSportByOrgId(orgId);

            return sport;
   ;     }

        public List<MatchResult> GetPartialMatchResults(int matchId)
        {
            var partialResults = this._gatherer.GetPartialResultsForMatch(matchId);

            var matchResults = new List<MatchResult>();
            foreach (var partialResult in partialResults)
            {
                var matchResult = new MatchResult
                {
                    AwayTeamGoals = partialResult.AwayGoals,
                    HomeTeamGoals = partialResult.HomeGoals,
                    MatchId = partialResult.MatchId,
                    ResultId = partialResult.PartialResultId,
                    ResultTypeId = partialResult.PartialResultTypeId
                };

            }

            return matchResults;
            
        }

        public List<MatchExtended> GetMatchesByResult(List<ChangeInfo> results)
        {
            var matches = new List<MatchExtended>();
            foreach (var result in results)
            {
                if (result.EntityType != EntityType.Result)
                {
                    continue;
                }

                var matchExtendedInfo = this._gatherer.GetMatchInfoByResultId(result.Id);
                // var matchInfo = this._gatherer.GetMatchInfoByMatchId(matchResult.MatchId);
                
                // now we shall have tournaments
                matches.Add(matchExtendedInfo);
            }
            return matches;
        }

        public List<int> GetTournamentIdsByResult(List<ChangeInfo> results)
        {
            var tournamentIds = new List<int>();
            foreach (var result in results)
            {
                if (result.EntityType != EntityType.Result)
                {
                    continue;
                }

                var matchResult = this._gatherer.GetMatchResultByResultId(result.Id);
                if (matchResult.ResultStatusId != 3 && matchResult.ResultStatusId != 1)
                {
                    continue;
                }

                var matchInfo = this._gatherer.GetMatchInfoByMatchId(matchResult.MatchId);
                
                // Only add the tournament Id if it is not in the list of tournament ids
                if (tournamentIds.Contains(matchInfo.TournamentId))
                {
                    continue;
                }
                tournamentIds.Add(matchInfo.TournamentId);

            }
            return tournamentIds;
        }

        public List<Match> GetTournamentMatchesByResults(List<ChangeInfo> results, ref List<int> tournamentIds)
        {
            var matches = new List<Match>();
            foreach (var result in results)
            {
                if (result.EntityType != EntityType.Result)
                {
                    continue;
                }

                var matchResult = this._gatherer.GetMatchResultByResultId(result.Id);
                if (matchResult.ResultStatusId != 3 && matchResult.ResultStatusId != 1)
                {
                    continue;
                }

                var matchInfo = this._gatherer.GetMatchInfoByMatchId(matchResult.MatchId);
                matchInfo.ResultStatusId = matchResult.ResultStatusId;
                matchInfo.MatchResultList = this.GetPartialMatchResults(matchResult.MatchId);

                // now we shall have tournaments
                tournamentIds = this._gatherer.GetUniqueTournaments(matchInfo.TournamentId, tournamentIds);

                matches.Add(matchInfo);
            }
            return matches;
        }

        /// <summary>
        /// The create standing node.
        /// </summary>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        public List<TeamResult> GetTournamentStanding(Tournament tournament)
        {
            // Get the list of standings from the API
            var standing = new List<TeamResult>();

            //if (tournament.TypeId == 1)
            //{
            //    // Getting the tournament standing.
                
            //}

            standing = this._gatherer.GetTournamentStandingByTournamentId(tournament);

            return standing;
        }
    }
}
