﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchSummaryMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.Nif.Mappers
{
    using Glue;

    using NTB.SportsData.Components.Mappers;
    using NTB.SportsData.Components.NIFConnectService;

    using Match = NTB.SportsData.Classes.Classes.Match;

    /// <summary>
    /// The match summary mapper.
    /// </summary>
    public class MatchSummaryMapper : BaseMapper<MatchSummary, Match>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<MatchSummary, Match> mapper)
        {
            mapper.Relate(x => x.MatchId, y => y.Id);
            mapper.Relate(x => x.MatchDate, y => y.MatchDate);
            mapper.Relate(x => x.ActivityAreaId, y => y.ActivityAreaId);
            mapper.Relate(x => x.ActivityAreaName, y => y.ActivityAreaName);
            mapper.Relate(x => x.ActivityId, y => y.ActivityId);
            mapper.Relate(x => x.Awayteam, y => y.AwayTeamName);
            mapper.Relate(x => x.AwayteamId, y => y.AwayTeamId);
            mapper.Relate(x => x.MatchEndTime, y => y.MatchEndTime);
            mapper.Relate(x => x.MatchNo, y => y.MatchNo);
            mapper.Relate(x => x.MatchStartTime, y => y.MatchStartTime);
            mapper.Relate(x => x.MatchStartTime, y => y.MatchStartTime);
            mapper.Relate(x => x.MatchTypeId, y => y.MatchTypeId);
            mapper.Relate(x => x.ClassCodeId, y => y.ClassCodeId);
            mapper.Relate(x => x.ClassCodeName, y => y.ClassCodeName);
            mapper.Relate(x => x.Hometeam, y => y.HomeTeamName);
            mapper.Relate(x => x.HometeamId, y => y.HomeTeamId);
            mapper.Relate(x => x.RoundId, y => y.RoundId);
            mapper.Relate(x => x.SeasonId, y => y.SeasonId);
            mapper.Relate(x => x.TournamentId, y => y.TournamentId);
            mapper.Relate(x => x.StatusTypeId, y => y.StatusTypeId);
            mapper.Relate(x => x.RoundName, y => y.RoundName);
            mapper.Relate(x => x.TournamentName, y => y.TournamentName);
            mapper.Relate(x => x.VenueUnitName, y => y.VenueUnitName);
            mapper.Relate(x => x.VenueUnitNo, y => y.VenueUnitNo);
        }
    }
}