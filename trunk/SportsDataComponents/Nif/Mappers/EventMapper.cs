﻿ // --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.Nif.Mappers
{
    using Glue;

    using NTB.SportsData.Classes.Classes;
    using NTB.SportsData.Components.Mappers;
    using NTB.SportsData.Components.NIFConnectService;

    /// <summary>
    /// The event mapper.
    /// </summary>
    public class EventMapper : BaseMapper<Event, SportEvent>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<Event, SportEvent> mapper)
        {
            mapper.Relate(x => x.ActivityId, y => y.ActivityId);
            mapper.Relate(x => x.ActivityName, y => y.ActivityName);
            mapper.Relate(x => x.EventId, y => y.EventId);
            mapper.Relate(x => x.EventName, y => y.EventName);
            mapper.Relate(x => x.StartDate, y => y.EventDateStart);
            mapper.Relate(x => x.EndDate, y => y.EventDateEnd);
            mapper.Relate(x => x.Location, y => y.VenueUnitName);
            mapper.Relate(x => x.LocalCouncil, y => y.EventLocation);
            mapper.Relate(x => x.EventTypeName, y => y.EventTypeName);
            mapper.Relate(x => x.EventTypeId, y => y.EventTypeId);
            mapper.Relate(x => x.VenueUnitId, y => y.VenueUnitId);
            mapper.Relate(x => x.AdministrativeOrgId, y => y.AdministrativeOrgId);
            mapper.Relate(x => x.AdministrativeOrgName, y => y.AdministrativeOrgName);
            mapper.Relate(x => x.RegionId, y => y.RegionId);
        }
    }
}
