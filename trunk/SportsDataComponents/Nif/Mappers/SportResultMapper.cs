﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SportResultMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Components.Nif.Mappers
{
    using Glue;

    using NTB.SportsData.Classes.Classes;
    using NTB.SportsData.Components.Mappers;

    using Result = NTB.SportsData.Components.NIFConnectService.Result;

    /// <summary>
    ///     The sport result mapper.
    /// </summary>
    public class SportResultMapper : BaseMapper<Result, SportResult>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<Result, SportResult> mapper)
        {
            mapper.Relate(x => x.ResultId, y => y.ResultId);
            mapper.Relate(x => x.ActivityName, y => y.ActivityName);
            mapper.Relate(x => x.ClassExerciseId, y => y.ClassExerciseId);
            mapper.Relate(x => x.ClassExerciseName, y => y.ClassExerciseName);
            mapper.Relate(x => x.ClassId, y => y.ClassId);
            mapper.Relate(x => x.Club, y => y.Club);
            mapper.Relate(x => x.ClubId, y => y.ClubId);
            mapper.Relate(x => x.CompetitorId, y => y.CompetitorId);
            mapper.Relate(x => x.EventDate, y => y.EventDate);
            mapper.Relate(x => x.EventName, y => y.EventName);
            mapper.Relate(x => x.EventId, y => y.EventId);
            mapper.Relate(x => x.FirstName, y => y.FirstName);
            mapper.Relate(x => x.LastName, y => y.LastName);
            mapper.Relate(x => x.PersonId, y => y.PersonId);
            mapper.Relate(x => x.Rank, y => y.Rank);
            mapper.Relate(x => x.EventTypeText, y => y.EventTypeText);
            mapper.Relate(x => x.TimeBehind, y => y.TimeBehind);
            mapper.Relate(x => x.Time, y => y.Time);
        }
    }
}