﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The tournament mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.Nif.Mappers
{
    using Glue;

    using SportsData.Components.Mappers;
    using NIFConnectService;

    /// <summary>
    /// The tournament mapper.
    /// </summary>
    public class TournamentMapper : BaseMapper<Tournament, Classes.Classes.Tournament>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<Tournament, Classes.Classes.Tournament> mapper)
        {
            mapper.Relate(x => x.TournamentId, y => y.Id);
            mapper.Relate(x => x.TournamentName, y => y.Name);
            mapper.Relate(x => x.ClassCodeId, y => y.AgeCategoryId);
            mapper.Relate(x => x.SeasonId, y => y.SeasonId);
            mapper.Relate(x => x.Division, y => y.Division);
            mapper.Relate(x => x.TournamentTypeId, y => y.TypeId);
        }
    }
}