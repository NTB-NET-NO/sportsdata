﻿using Glue;
using NTB.SportsData.Components.Mappers;
using NTB.SportsData.Components.NIFConnectService;

namespace NTB.SportsData.Components.Nif.Mappers
{
    public class AgeCategoryMapper : BaseMapper<TournamentClass, Classes.Classes.AgeCategory>
    {
        protected override void SetUpMapper(Mapping<TournamentClass, Classes.Classes.AgeCategory> mapper)
        {
            mapper.Relate(x => x.FromAge, y => y.FromAge);
            mapper.Relate(x => x.ToAge, y => y.ToAge);
            mapper.Relate(x => x.ClassId, y => y.Id);
            mapper.Relate(x => x.ClassName, y => y.Name);
            mapper.Relate(x => x.ClassCode, y => y.ClassCode);
            mapper.Relate(x => x.OrgIdOwner, y => y.FederationId);
            mapper.Relate(x => x.Sex, y => y.GenderId);
        }
    }
}