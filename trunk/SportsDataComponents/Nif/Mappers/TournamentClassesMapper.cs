﻿using Glue;
using NTB.SportsData.Components.Mappers;
using NTB.SportsData.Components.NIFConnectService;

namespace NTB.SportsData.Components.Nif.Mappers
{
    public class TournamentClassesMapper : BaseMapper<TournamentClasses1, Classes.Classes.AgeCategory>
    {
        protected override void SetUpMapper(Mapping<TournamentClasses1, Classes.Classes.AgeCategory> mapper)
        {
            mapper.Relate(x => x.ClassId, y => y.Id);
            mapper.Relate(x => x.ClassName, y => y.Name);
            mapper.Relate(x => x.TournamentId, y => y.TournamentId);
        }
    }
}
