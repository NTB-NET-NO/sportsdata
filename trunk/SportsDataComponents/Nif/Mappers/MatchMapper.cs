﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.Nif.Mappers
{
    using Glue;

    using NTB.SportsData.Components.Mappers;

    using Match = NTB.SportsData.Classes.Classes.Match;

    /// <summary>
    /// The match summary mapper.
    /// </summary>
    public class MatchMapper : BaseMapper<NIFConnectService.Match1, Match>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<NIFConnectService.Match1, Match> mapper)
        {
            mapper.Relate(x => x.MatchId, y => y.Id);
            mapper.Relate(x => x.MatchDate, y => y.MatchDate);
            mapper.Relate(x => x.AwayTeam, y => y.AwayTeamName);
            mapper.Relate(x => x.AwayTeamId, y => y.AwayTeamId);
            mapper.Relate(x => x.MatchEndTime, y => y.MatchEndTime);
            mapper.Relate(x => x.MatchNo, y => y.MatchNo);
            mapper.Relate(x => x.MatchStartTime, y => y.MatchStartTime);
            mapper.Relate(x => x.MatchStartTime, y => y.MatchStartTime);
            mapper.Relate(x => x.MatchTypeId, y => y.MatchTypeId);
            mapper.Relate(x => x.ClassCodeId, y => y.ClassCodeId);
            mapper.Relate(x => x.HomeTeam, y => y.HomeTeamName);
            mapper.Relate(x => x.HomeTeamId, y => y.HomeTeamId);
            mapper.Relate(x => x.RoundId, y => y.RoundId);
            mapper.Relate(x => x.TournamentId, y => y.TournamentId);
            mapper.Relate(x => x.StatusTypeId, y => y.StatusTypeId);
        }
    }
}