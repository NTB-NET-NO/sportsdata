﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RefereeMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.Nif.Mappers
{
    using Glue;

    using SportsData.Components.Mappers;
    using NIFConnectService;

    /// <summary>
    /// The referee mapper.
    /// </summary>
    public class RefereeMapper : BaseMapper<Referee1, Classes.Classes.Referee>
    {
        
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<Referee1, Classes.Classes.Referee> mapper)
        {
            mapper.Relate(x => x.RefereeId, y => y.Id);
            mapper.Relate(x => x.RefereeName, y => y.RefereeName);
            mapper.Relate(x => x.RefereeStatus, y => y.RefereeType);
            mapper.Relate(x => x.OrgId, y => y.ClubId);
            mapper.Relate(x => x.OrgName, y => y.ClubName);
        }

    }
}