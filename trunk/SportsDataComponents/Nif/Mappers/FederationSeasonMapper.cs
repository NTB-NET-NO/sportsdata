﻿namespace NTB.SportsData.Components.Nif.Mappers
{
    using Glue;

    using NTB.SportsData.Components.Mappers;

    class FederationSeasonMapper : BaseMapper<NIFConnectService.Season, Classes.Classes.Season>
    {
        protected override void SetUpMapper(Mapping<NIFConnectService.Season, Classes.Classes.Season> mapper)
        {
            mapper.Relate(x => x.SeasonId, y => y.Id);
            mapper.Relate(x => x.SeasonName, y => y.Name);
            mapper.Relate(x => x.FromDate, y => y.StartDate);
            mapper.Relate(x => x.ToDate, y => y.EndDate);
            mapper.Relate(x => x.SeasonStatusId, y => y.StatusId);
        }
    }
}
