﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SeasonTournamentMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The season tournament mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.Nif.Mappers
{
    using Glue;

    using NTB.SportsData.Components.Mappers;
    using NTB.SportsData.Components.NIFConnectService;

    /// <summary>
    /// The season tournament mapper.
    /// </summary>
    internal class SeasonTournamentMapper : BaseMapper<Tournament, Classes.Classes.Tournament>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<Tournament, Classes.Classes.Tournament> mapper)
        {
            mapper.Relate(x => x.SeasonId, y => y.SeasonId);
            mapper.Relate(x => x.TournamentId, y => y.Id);
            mapper.Relate(x => x.TournamentName, y => y.Name);
            mapper.Relate(x => x.TournamentNo, y => y.Number, this.StringToInt32Converter());
            mapper.Relate(x => x.StatusCode, y => y.StatusCode);
            mapper.Relate(x => x.TournamentTypeId, y => y.TypeId);
        }
    }
}