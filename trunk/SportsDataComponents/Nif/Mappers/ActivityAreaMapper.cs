﻿namespace NTB.SportsData.Components.Nif.Mappers
{
    using Glue;

    using NTB.SportsData.Components.Mappers;
    using NTB.SportsData.Components.NIFConnectService;

    public class ActivityAreaMapper : BaseMapper<ActivityArea, Classes.Classes.ActivityArea>
    {
        protected override void SetUpMapper(Mapping<ActivityArea, Classes.Classes.ActivityArea> mapper)
        {
            mapper.Relate(x => x.ActivityAreaId, y => y.ActivityAreaId);
            mapper.Relate(x => x.ActivityAreaName, y => y.ActivityAreaName);
            mapper.Relate(x => x.ActivityId, y => y.ActivityId);
            mapper.Relate(x => x.ActivityName, y => y.ActivityName);
            mapper.Relate(x => x.VenueUnitId, y => y.VenueUnitId);
        }
    }
}
