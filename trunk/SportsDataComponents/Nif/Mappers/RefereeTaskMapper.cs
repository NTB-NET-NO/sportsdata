﻿using Glue;
using NTB.SportsData.Components.Mappers;
using NTB.SportsData.Components.NIFConnectService;

namespace NTB.SportsData.Components.Nif.Mappers
{
    public class RefereeTaskMapper : BaseMapper<RefereeTask1, Classes.Classes.Referee>
    {
        protected override void SetUpMapper(Mapping<RefereeTask1, Classes.Classes.Referee> mapper)
        {
            mapper.Relate(x => x.RefereeId, y => y.Id);
            mapper.Relate(x => x.RefereeFirstName, y => y.FirstName);
            mapper.Relate(x => x.RefereeLastName, y => y.LastName);
            mapper.Relate(x => x.RefereeTaskId, y => y.RefereeTaskId);
            mapper.Relate(x => x.RefereeTaskTypeId, y => y.RefereeTypeId);
            mapper.Relate(x => x.RefereeTaskTypeName, y => y.RefereeType);
        }
    }
}
