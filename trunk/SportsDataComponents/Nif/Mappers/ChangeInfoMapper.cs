﻿using Glue;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Mappers;

namespace NTB.SportsData.Components.Nif.Mappers
{
    public class ChangeInfoMapper : BaseMapper<NIFConnectService.ChangeInfo, ChangeInfo>
    {
        protected override void SetUpMapper(Mapping<NIFConnectService.ChangeInfo, ChangeInfo> mapper)
        {
            mapper.Relate(x => x.Id, y => y.Id);
            mapper.Relate(x => x.ChangeType, y => y.ChangeType, this.RemoteChangeTypeConverter());
            mapper.Relate(x => x.Created, y => y.Created);
            mapper.Relate(x => x.Modified, y => y.Modified);
            mapper.Relate(x => x.EntityType, y => y.EntityType, this.RemoteEntityTypeConverter());
            mapper.Relate(x => x.Name, y => y.Name);
        }
    }
}
