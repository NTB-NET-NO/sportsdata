﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchLineUpMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.Nif.Mappers
{
    using Glue;

    using NTB.SportsData.Classes.Classes;
    using NTB.SportsData.Components.Mappers;
    using NTB.SportsData.Components.NIFConnectService;

    /// <summary>
    /// The match line up mapper.
    /// </summary>
    internal class MatchLineUpMapper : BaseMapper<MatchLineUp, Player>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<MatchLineUp, Player> mapper)
        {
            mapper.Relate(x => x.FirstName, y => y.FirstName);
            mapper.Relate(x => x.LastName, y => y.LastName);
            mapper.Relate(x => x.CaptainYN, y => y.TeamCaptain);
            mapper.Relate(x => x.MatchId, y => y.MatchId);
            mapper.Relate(x => x.PlayedYN, y => y.Played);
            mapper.Relate(x => x.PlayerOfTheMatchYN, y => y.PlayerOfTheMatch);
            mapper.Relate(x => x.Pos, y => y.Position);
            mapper.Relate(x => x.ShirtNo, y => y.PlayerShirtNumber);
            mapper.Relate(x => x.StartingLineUp, y => y.StartingLineUp);
            mapper.Relate(x => x.ViceCaptain1YN, y => y.ViceCaptain1);
            mapper.Relate(x => x.ViceCaptain2YN, y => y.ViceCaptain2);
        }
    }
}