﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VenueExtendedMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The venue extended mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.Nif.Mappers
{
    using Glue;

    using NTB.SportsData.Components.Mappers;
    using NTB.SportsData.Components.NIFConnectService;

    /// <summary>
    /// The venue extended mapper.
    /// </summary>
    public class VenueExtendedMapper : BaseMapper<VenueExtended, Classes.Classes.VenueExtended>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<VenueExtended, Classes.Classes.VenueExtended> mapper)
        {
            mapper.Relate(x => x.VenueId, y => y.Id);
            mapper.Relate(x => x.VenueName, y => y.Name);
            mapper.Relate(x => x.LocalCouncilId, y => y.LocalCouncilId);
            mapper.Relate(x => x.LocalCouncilName, y => y.LocalCouncilName);
            mapper.Relate(x => x.PostName, y => y.Place);
        }
    }
}