﻿using Glue;
using NTB.SportsData.Components.Mappers;
using NTB.SportsData.Components.NIFConnectService;

namespace NTB.SportsData.Components.Nif.Mappers
{
    public class MatchSearchMapper : BaseMapper<MatchSearchResult1, Classes.Classes.Match>
    {
        protected override void SetUpMapper(Mapping<MatchSearchResult1, Classes.Classes.Match> mapper)
        {
            mapper.Relate(x => x.MatchDate, y => y.MatchDate);
            mapper.Relate(x => x.MatchId, y => y.Id);
            mapper.Relate(x => x.ResultStatusId, y => y.ResultStatusId);
            mapper.Relate(x => x.ActivityAreaName, y => y.VenueUnitName);
            mapper.Relate(x => x.ActivityAreaId, y => y.VenueUnitId);
            mapper.Relate(x => x.Spectators, y => y.Spectators);
            mapper.Relate(x => x.AwayteamId, y => y.AwayTeamId);
            mapper.Relate(x => x.AwayteamOrgName, y => y.AwayTeamName);
            mapper.Relate(x => x.HometeamId, y => y.HomeTeamId);
            mapper.Relate(x => x.HometeamOrgName, y => y.HomeTeamName);
            mapper.Relate(x => x.RoundId, y => y.RoundId);
            mapper.Relate(x => x.RoundName, y => y.RoundName);
            mapper.Relate(x => x.MatchStartTime, y => y.MatchStartTime);
            mapper.Relate(x => x.GoalsHome, y => y.HomeTeamGoals);
            mapper.Relate(x => x.GoalsAway, y => y.AwayTeamGoals);
        }
    }
}
