﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VenueUnitMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.Nif.Mappers
{
    using Glue;

    using NTB.SportsData.Components.Mappers;
    using NTB.SportsData.Components.NIFConnectService;

    /// <summary>
    /// The venue unit mapper.
    /// </summary>
    public class VenueUnitMapper : BaseMapper<VenueUnit, Classes.Classes.VenueUnit>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<VenueUnit, Classes.Classes.VenueUnit> mapper)
        {
            mapper.Relate(x => x.VenueUnitId, y => y.VenueUnitId);
            mapper.Relate(x => x.VenueUnitName, y => y.VenueUnitName);
            mapper.Relate(x => x.VenueUnitNo, y => y.VenueUnitNo);
            mapper.Relate(x => x.VenueClassId, y => y.VenueClassId);
            mapper.Relate(x => x.VenueId, y => y.VenueId);
            mapper.Relate(x => x.VenueName, y => y.VenueName);
        }
    }
}