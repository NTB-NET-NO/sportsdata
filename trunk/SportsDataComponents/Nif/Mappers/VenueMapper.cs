﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VenueMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The venue mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.Nif.Mappers
{
    using Glue;

    using NTB.SportsData.Components.Mappers;
    using NTB.SportsData.Components.NIFConnectService;

    /// <summary>
    /// The venue mapper.
    /// </summary>
    public class VenueMapper : BaseMapper<Venue, Classes.Classes.Venue>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<Venue, Classes.Classes.Venue> mapper)
        {
            mapper.Relate(x => x.VenueId, y => y.VenueId);
            mapper.Relate(x => x.VenueName, y => y.VenueName);
            mapper.Relate(x => x.Latitude, y => y.Latitude);
            mapper.Relate(x => x.Longitude, y => y.Longitude);
            mapper.Relate(x => x.LocalCouncilId, y => y.LocalCouncilId);
            mapper.Relate(x => x.LocalCouncilName, y => y.LocalCouncilName);
            mapper.Relate(x => x.ZipCode, y => y.ZipCode);
            mapper.Relate(x => x.City, y => y.City);
            mapper.Relate(x => x.Email, y => y.Email);
        }
    }
}