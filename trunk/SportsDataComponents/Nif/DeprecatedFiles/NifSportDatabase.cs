// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NifSportDatabase.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Components.Nif
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;

    using log4net;

    using NTB.SportsData.Classes.Classes;
    using NTB.SportsData.Components.NIFConnectService;

    /// <summary>
    ///     The nif sport database.
    /// </summary>
    public class NifSportDatabase
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(NifSportDatabase));

        /// <summary>
        ///     Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        ///     Gets or sets the federation id.
        /// </summary>
        public int FederationId { get; set; }

        // public SportsDataDatabase()
        // {
        // // I won't let this do much at this moment

        // }

        /// <summary>
        ///     Get events from today from the database
        /// </summary>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<SportEvent> GetEvents()
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
            {
                try
                {
                    var sqlCommand = new SqlCommand
                                         {
                                             Connection = sqlConnection, 
                                             CommandText = "Service_GetEventsExtra"
                                         };

                    var startDate = DateTime.Today.Date;
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                    {
                        startDate = Convert.ToDateTime(ConfigurationManager.AppSettings["niftestingdate"]);
                    }

                    var sqlFormattedDate = startDate.ToString("yyyy-MM-dd");

                    sqlCommand.Parameters.Add(new SqlParameter("@Today", sqlFormattedDate));

                    sqlCommand.Parameters.Add(new SqlParameter("@FederationId", this.FederationId));

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", this.SportId));

                    // Telling the system that this is a StoredProcedure
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        sqlConnection.Open();
                    }

                    // Creating the SQL-reader.
                    var sqlDataReader = sqlCommand.ExecuteReader();

                    var sportEvents = new List<SportEvent>();
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            var sportEvent = new SportEvent
                                                 {
                                                     EventName = sqlDataReader["Name"].ToString(), 
                                                     EventId = Convert.ToInt32(sqlDataReader["EventId"].ToString()), 
                                                     EventDateStart = Convert.ToDateTime(sqlDataReader["DateStart"].ToString()), 
                                                     EventDateEnd = Convert.ToDateTime(sqlDataReader["DateEnd"].ToString()), 
                                                     EventLocation = sqlDataReader["Location"].ToString(), 
                                                     EventTimeStart = sqlDataReader["TimeStart"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["TimeStart"].ToString()), 
                                                     EventTimeEnd = sqlDataReader["TimeEnd"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["TimeEnd"].ToString()), 
                                                     ActivityId = Convert.ToInt32(sqlDataReader["ActivityId"].ToString()), 
                                                     EventOrganizerId = Convert.ToInt32(sqlDataReader["OrganizerId"].ToString())
                                                 };

                            // Field2 = rdr.GetSqlInt32(Field2_Ordinal).ToNullableInt32()
                            sportEvents.Add(sportEvent);
                        }
                    }

                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }

                    return sportEvents;
                }
                catch (SqlException sqlexception)
                {
                    Logger.Error(sqlexception);
                    return null;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return null;
                }
            }
        }

        /// <summary>
        /// Inserts or updates events in the database
        /// </summary>
        /// <param name="sportEvents">
        /// Holds a list of events that we are to insert or update
        /// </param>
        public void InsertEvent(List<SportEvent> sportEvents)
        {
            Logger.Debug("Number of items: " + sportEvents.Count);
            foreach (var sportEvent in sportEvents)
            {
                Logger.Debug("Checking if " + sportEvent.EventName + " is in the database and if not, we are to insert");

                // First we have to check if the event is in the database already.
                // If it is, we need to check if some of the values that we have has been changed
                // If some data has been changed, we need to change them in our database
                // So we have to either update or insert

                // Can we do this in StoredProcedures? (The whole thing in one go...)

                // We don't do it in one go. What we do is get the data from the database, then check against the object
                using (var sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    try
                    {
                        var command = new SqlCommand
                                          {
                                              Connection = sqlConnection, 
                                              CommandText = "Service_CheckEventsByFederationAndSport"
                                          };

                        // Adding eventId
                        command.Parameters.Add(new SqlParameter("@EventId", sportEvent.EventId));

                        // Adding sportId
                        command.Parameters.Add(new SqlParameter("@SportId", sportEvent.ActivityId));

                        // Adding federationId
                        command.Parameters.Add(new SqlParameter("@FederationId", sportEvent.AdministrativeOrgId));

                        // Telling the system that this is a StoredProcedure
                        command.CommandType = CommandType.StoredProcedure;

                        if (sqlConnection.State != ConnectionState.Open)
                        {
                            sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                            sqlConnection.Open();
                        }

                        // Creating the SQL-reader.
                        var sqlDataReader = command.ExecuteReader();

                        if (sqlDataReader.HasRows)
                        {
                            var updateEvent = false;

                            // We are now checking what we have gotten and if there are changes.
                            // If there are changes, we shall update.
                            while (sqlDataReader.Read())
                            {
                                if (sportEvent.EventName != sqlDataReader["EventName"].ToString())
                                {
                                    updateEvent = true;
                                }

                                if (sportEvent.EventDateStart != Convert.ToDateTime(sqlDataReader["DateStart"].ToString()))
                                {
                                    updateEvent = true;
                                }

                                if (sportEvent.EventDateEnd != Convert.ToDateTime(sqlDataReader["DateEnd"].ToString()))
                                {
                                    updateEvent = true;
                                }

                                if (sportEvent.EventLocation != sqlDataReader["Location"].ToString())
                                {
                                    updateEvent = true;
                                }

                                if (sportEvent.EventTimeStart != null)
                                {
                                    if (sportEvent.EventTimeStart != Convert.ToInt32(sqlDataReader["TimeStart"].ToString()))
                                    {
                                        updateEvent = true;
                                    }
                                }

                                if (sportEvent.EventTimeEnd != null)
                                {
                                    if (sportEvent.EventTimeEnd != Convert.ToInt32(sqlDataReader["TimeEnd"].ToString()))
                                    {
                                        updateEvent = true;
                                    }
                                }

                                if (sportEvent.EventOrganizerId != Convert.ToInt32(sqlDataReader["OrganizerId"].ToString()))
                                {
                                    updateEvent = true;
                                }

                                if (sportEvent.ActivityId != Convert.ToInt32(sqlDataReader["ActivityId"].ToString()))
                                {
                                    updateEvent = true;
                                }

                                if (updateEvent)
                                {
                                    if (sportEvent.EventDateEnd == null)
                                    {
                                        sportEvent.EventDateEnd = sportEvent.EventDateStart;
                                    }

                                    using (var myUpdateConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                                    {
                                        Logger.Debug("We are updating!");

                                        // We shall update the event
                                        var sqlCommand = new SqlCommand
                                                             {
                                                                 Connection = myUpdateConnection, 
                                                                 CommandText = "Service_UpdateEvent"
                                                             };
                                        sqlCommand.Parameters.Add(new SqlParameter("@EventId", sportEvent.EventId));
                                        sqlCommand.Parameters.Add(new SqlParameter("@ActivityId", sportEvent.ActivityId));
                                        sqlCommand.Parameters.Add(new SqlParameter("@OrganizerId", sportEvent.EventOrganizerId));
                                        sqlCommand.Parameters.Add(new SqlParameter("@EventName", sportEvent.EventName));
                                        sqlCommand.Parameters.Add(new SqlParameter("@Location", sportEvent.EventLocation));
                                        sqlCommand.Parameters.Add(new SqlParameter("@DateStart", sportEvent.EventDateStart));
                                        sqlCommand.Parameters.Add(new SqlParameter("@DateEnd", sportEvent.EventDateEnd));
                                        sqlCommand.Parameters.Add(new SqlParameter("@TimeStart", sportEvent.EventTimeStart));
                                        sqlCommand.Parameters.Add(new SqlParameter("@TimeEnd", sportEvent.EventTimeEnd));
                                        sqlCommand.Parameters.Add(new SqlParameter("@OrganizationId", sportEvent.AdministrativeOrgId));
                                        sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportEvent.ParentActivityId));

                                        // Telling the system that this is a StoredProcedure
                                        sqlCommand.CommandType = CommandType.StoredProcedure;

                                        if (myUpdateConnection.State != ConnectionState.Open)
                                        {
                                            myUpdateConnection.Open();
                                        }

                                        sqlCommand.ExecuteNonQuery();

                                        if (myUpdateConnection.State != ConnectionState.Closed)
                                        {
                                            myUpdateConnection.Close();
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            // We have not found a row and so we shall insert
                            if (sportEvent.EventDateEnd == null)
                            {
                                sportEvent.EventDateEnd = sportEvent.EventDateStart;
                            }

                            using (var myUpdateConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                            {
                                Logger.Debug("We are Inserting!");

                                // We shall update the event
                                var sqlCommand = new SqlCommand
                                                     {
                                                         Connection = myUpdateConnection, 
                                                         CommandText = "Service_InsertEvent"
                                                     };
                                sqlCommand.Parameters.Add(new SqlParameter("@EventId", sportEvent.EventId));
                                sqlCommand.Parameters.Add(new SqlParameter("@ActivityId", sportEvent.ActivityId));
                                sqlCommand.Parameters.Add(new SqlParameter("@OrganizerId", sportEvent.EventOrganizerId));
                                sqlCommand.Parameters.Add(new SqlParameter("@EventName", sportEvent.EventName));
                                sqlCommand.Parameters.Add(new SqlParameter("@Location", sportEvent.EventLocation));
                                sqlCommand.Parameters.Add(new SqlParameter("@DateStart", sportEvent.EventDateStart));
                                sqlCommand.Parameters.Add(new SqlParameter("@DateEnd", sportEvent.EventDateEnd));
                                sqlCommand.Parameters.Add(new SqlParameter("@OrganizationId", sportEvent.AdministrativeOrgId));
                                sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportEvent.ParentActivityId));

                                if (sportEvent.EventTimeStart != null)
                                {
                                    sqlCommand.Parameters.Add(new SqlParameter("@TimeStart", sportEvent.EventTimeStart));
                                }

                                if (sportEvent.EventTimeEnd != null)
                                {
                                    sqlCommand.Parameters.Add(new SqlParameter("@TimeEnd", sportEvent.EventTimeEnd));
                                }

                                // Telling the system that this is a StoredProcedure
                                sqlCommand.CommandType = CommandType.StoredProcedure;

                                if (myUpdateConnection.State != ConnectionState.Open)
                                {
                                    myUpdateConnection.Open();
                                }

                                sqlCommand.ExecuteNonQuery();

                                if (myUpdateConnection.State != ConnectionState.Closed)
                                {
                                    myUpdateConnection.Close();
                                }
                            }
                        }

                        if (sqlConnection.State != ConnectionState.Closed)
                        {
                            sqlConnection.Close();
                        }
                    }
                    catch (SqlException sqlexception)
                    {
                        Logger.Error(sqlexception);
                    }
                    catch (Exception exception)
                    {
                        Logger.Error(exception);
                    }

                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }

                    // return null;
                }
            }
        }

        /// <summary>
        /// The update event data.
        /// </summary>
        /// <param name="eventId">
        /// The event id.
        /// </param>
        public void UpdateEventData(int eventId)
        {
            // We don't do it in one go. What we do is get the data from the database, then check against the object
            // using (SqlConnection sqlConnection sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"])
            using (var sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
            {
                try
                {
                    var command = new SqlCommand
                                      {
                                          Connection = sqlConnection, 
                                          CommandText = "Service_SetDownloadEvent"
                                      };

                    command.Parameters.Add(new SqlParameter("@EventId", eventId));

                    // Telling the system that this is a StoredProcedure
                    command.CommandType = CommandType.StoredProcedure;

                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        sqlConnection.Open();
                    }

                    command.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Logger.Error(ex.Message);
                    Logger.Error(ex);
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    Logger.Error(ex);
                }
            }
        }

        /// <summary>
        /// The set download.
        /// </summary>
        /// <param name="eventId">
        /// The event id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool SetDownload(int eventId)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
            {
                try
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        sqlConnection.Open();
                    }

                    var command = new SqlCommand
                                      {
                                          Connection = sqlConnection, 
                                          CommandText = "Service_SetDownload"
                                      };

                    command.Parameters.Add(new SqlParameter("@EventId", eventId));

                    // Telling the system that this is a StoredProcedure
                    command.CommandType = CommandType.StoredProcedure;

                    if (command.ExecuteNonQuery() == 1)
                    {
                        if (sqlConnection.State != ConnectionState.Closed)
                        {
                            sqlConnection.Close();
                        }

                        return true;
                    }

                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }

                    return false;
                }
                catch (SqlException exception)
                {
                    Logger.Error(exception);
                    return false;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return false;
                }
            }
        }

        /// <summary>
        /// The check municipaltity table.
        /// </summary>
        /// <param name="localCounsils">
        /// The local counsils.
        /// </param>
        /// <param name="region">
        /// The region.
        /// </param>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool CheckMunicipaltityTable(Region[] localCounsils, Region region, int sportId)
        {
            Logger.Info("Inserting data in Municipality database table for district " + region.RegionName);

            /**
             * We are looping through the list of municipalities which depends on the district we are looping over
             * 
             */
            foreach (var localCounsil in localCounsils)
            {
                using (var sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        sqlConnection.Open();
                    }

                    // We are checking if the current ID and the district ID is in the database
                    Logger.Info("Checking Municipality database table");

                    // Using Stored Procedure: Service_GetMunicipalities
                    var sqlCommand = new SqlCommand("Service_GetMunicipalities", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@MunicipalityId", region.RegionId));

                    sqlCommand.Parameters.Add(new SqlParameter("DistrictId", region.RegionId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    var reader = sqlCommand.ExecuteReader();

                    // If we don't get a hit, we shall insert into the database
                    if (!reader.HasRows)
                    {
                        // Using Stored Procedure: Service_InsertMunicipalities 
                        var commandInsert = new SqlCommand("Service_InsertMunicipalities", sqlConnection);

                        commandInsert.Parameters.Add(new SqlParameter("@MunicipalityId", localCounsil.RegionId));

                        commandInsert.Parameters.Add(new SqlParameter("@MunicipalityName", localCounsil.RegionName));

                        commandInsert.Parameters.Add(new SqlParameter("@DistrictId", region.ParentRegionId));

                        commandInsert.Parameters.Add(new SqlParameter("SportId", sportId));

                        commandInsert.CommandType = CommandType.StoredProcedure;

                        // We must populate the database
                        commandInsert.ExecuteNonQuery();
                    }

                    reader.Close();
                    sqlCommand.Dispose();

                    sqlConnection.Close();
                }

                // if it has rows, we shall loop through and check if there are any new districts 
                // @todo: Check if district exists or not
                return true;
            }

            return true;
        }
    }
}