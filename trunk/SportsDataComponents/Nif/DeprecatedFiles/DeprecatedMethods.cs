// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DeprecatedMethods.cs" company="Norsk Telegrambyr� AS">
//   Copyright (c) Norsk Telegrambyr� AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.Nif
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.Linq;
    using System.Xml;

    using log4net;

    using NTB.SportsData.Classes.Classes;
    using NTB.SportsData.Classes.Enums;
    using NTB.SportsData.Components.Facade;
    using NTB.SportsData.Components.Repositories;

    /// <summary>
    /// The deprecated methods.
    /// </summary>
    internal class DeprecatedMethods
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(NifTeamSportDataFileController));

        /// <summary>
        /// The if team sport data file controller.
        /// </summary>
        private readonly NifTeamSportDataFileController nifTeamSportDataFileController;

        /// <summary>
        /// Initializes a new instance of the <see cref="DeprecatedMethods"/> class.
        /// </summary>
        /// <param name="nifTeamSportDataFileController">
        /// The nif team sport data file controller.
        /// </param>
        public DeprecatedMethods(NifTeamSportDataFileController nifTeamSportDataFileController)
        {
            this.nifTeamSportDataFileController = nifTeamSportDataFileController;
        }


        /// <summary>
        /// Filters out the penalty incidents in the list. We
        /// </summary>
        /// <param name="matchIncidents">
        /// The match incident
        /// </param>
        /// <param name="sportId">
        /// The sport id
        /// </param>
        /// <returns>
        /// List of penalty incidents
        /// </returns>
        private List<MatchIncident> GetMatchWarnings(List<MatchIncident> matchIncidents, int sportId)
        {
            if (!matchIncidents.Any())
            {
                return new List<MatchIncident>();
            }

            try
            {
                if (sportId == 23)
                {
                    var penaltyIncidents = new List<MatchIncident>();
                    foreach (var matchIncident in matchIncidents.Where(x => x.IncidentSubType != null))
                    {
                        if (matchIncident.IncidentType.ToLower()
                            .Contains("advarsel"))
                        {
                            // todo: We need to get this value from a database
                            penaltyIncidents.Add(matchIncident);
                        }
                    }

                    var incidents = new List<MatchIncident>();
                    foreach (var matchIncident in penaltyIncidents)
                    {
                        var currentMatchIncident = matchIncident;

                        // filteredMatches = (from m in matches where m.StatusCode != null select m).ToList();
                        var incidentDetails = (from m in matchIncidents where m.ParentId == currentMatchIncident.MatchIncidentId && m.IncidentSubTypeId == 200186 select m).ToList();

                        if (incidentDetails.Any())
                        {
                            // Adding text to define the parent action
                            foreach (var si in incidentDetails)
                            {
                                si.IncidentShort = "advarsel";
                            }

                            // Add to list of score incidents
                            incidents.AddRange(incidentDetails);
                        }
                    }

                    matchIncidents = incidents;
                }

                return matchIncidents;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);

                return new List<MatchIncident>();
            }
        }

        /// <summary>
        /// Filter out the penalty match incidents. These are not linked to any player
        /// </summary>
        /// <param name="matchIncidents">
        /// List of total match incidents
        /// </param>
        /// <returns>
        /// List of match penalty incidents
        /// </returns>
        private List<MatchIncident> GetMatchPenalties(List<MatchIncident> matchIncidents)
        {
            if (!matchIncidents.Any())
            {
                return new List<MatchIncident>();
            }

            try
            {
                var penaltyIncidents = new List<MatchIncident>();

                foreach (var matchIncident in matchIncidents)
                {
                    if (matchIncident.IncidentType.ToLower()
                        .Contains("minutter"))
                    {
                        matchIncident.IncidentShort = matchIncident.IncidentType;
                        penaltyIncidents.Add(matchIncident);
                    }
                }

                matchIncidents = penaltyIncidents;

                return matchIncidents;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);

                return new List<MatchIncident>();
            }
        }

        /// <summary>
        ///     The get tournaments.
        /// </summary>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        private List<Tournament> GetTournaments()
        {
            Logger.Info("Getting tournaments from database");

            try
            {
                var tournamentRepository = new TournamentRepository();

                if (this.DataParams.SeasonId == 0)
                {
                    var seasonRepository = new SeasonRepository();
                    var seasons = seasonRepository.GetSeasonsBySportId(this.DataParams.SportId);
                    if (seasons.Count == 0)
                    {
                    }

                    if (this.DataParams.DisciplineId > 0)
                    {
                        seasons = seasonRepository.GetSeasonBySportIdAndDisciplineId(this.DataParams.SportId, this.DataParams.DisciplineId);
                    }

                    if (!seasons.Any())
                    {
                        Logger.Info("No seasons found, we are returning an empty list");
                        return new List<Tournament>();
                    }

                    var seasonId = (from s in seasons where s.Active select s.Id).Single();

                    this.DataParams.SeasonId = seasonId;
                }

                return tournamentRepository.GetCustomerTournaments(this.DataParams.SportId, this.DataParams.SeasonId);
            }
            catch (Exception exception)
            {
                Logger.Error("An error occured while getting tournaments!");
                Logger.Error(exception);
                Logger.Error(exception.StackTrace);

                return new List<Tournament>();
            }
        }

        /// <summary>
        /// The get tournament matches.
        /// </summary>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        private List<Match> GetTournamentMatches(int tournamentId)
        {
            var facade = new TeamSportFacade();

            return facade.GetTodaysMatchesByTournamentId(tournamentId);
        }

        /// <summary>
        /// The create match results.
        /// </summary>
        /// <param name="tournaments">
        /// The tournaments.
        /// </param>
        /// <exception cref="Exception">
        /// Throws an exception if something goes wrong
        /// </exception>
        public void Deprecated_CreateMatchResults(List<Tournament> tournaments)
        {
            // Set the match repository
            var matchRepository = new MatchRepository();

            this.nifTeamSportDataFileController.xmlTeamSportDocument = new NifTeamSportXmlDocument
                                                                          {
                                                                              RenderResult = false
                                                                          };

            var testing = Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]);

            if (this.nifTeamSportDataFileController.DataParams.PushResults)
            {
                this.nifTeamSportDataFileController.xmlTeamSportDocument.RenderResult = true;
            }

            // Create the organization object, add discipline to it and we are good to go!
            var organization = this.nifTeamSportDataFileController.GetOrganizationFromSportId(this.nifTeamSportDataFileController.DataParams.SportId);
            var discipline = this.nifTeamSportDataFileController.GetFederationDisciplineById(this.nifTeamSportDataFileController.DataParams.DisciplineId);
            if (discipline == null)
            {
                throw new Exception("Discipline is null!");
            }

            organization.DisciplineId = discipline.Id;
            organization.DisciplineName = discipline.Name;

            foreach (var tournament in tournaments)
            {
                Logger.InfoFormat("Processing tournament {0} with id {1}", tournament.Name, tournament.Id);

                // this is just a cleaner thing. We add this because we need to check if there is a type Id in the tournament
                if (tournament.TypeId == 0)
                {
                    var remoteTournament = this.nifTeamSportDataFileController.GetTournamentTypeId(tournament.Id);

                    tournament.TypeId = remoteTournament.TypeId;

                    var tournamentRepository = new TournamentRepository();
                    tournamentRepository.UpdateTournamentType(tournament);
                }

                var matches = this.nifTeamSportDataFileController.GetMatchResults(tournament);

                if (matches.Count == 0)
                {
                    Logger.Info("No matches without a result found");
                    continue;
                }

                Logger.Info("We have found " + matches.Count + " matches.");

                // This loop is just here for debugging proposes. 
                if (testing)
                {
                    NifTeamSportDataFileController.DebugMatchLoop(matches);
                }

                // Get a list of matches that has a result
                var filteredMatches = this.nifTeamSportDataFileController.FilterMatches(matches);

                if (!filteredMatches.Any())
                {
                    Logger.InfoFormat("After filtering the information from NIF, we have {0} matches to process", filteredMatches.Count);
                    continue;
                }

                var tournamentRound = "0";

                var startDate = this.nifTeamSportDataFileController.SetDate(testing);
                var endDate = this.nifTeamSportDataFileController.SetDate(testing);

                this.nifTeamSportDataFileController.LocalMatches = matchRepository.GetMatchesByDateInterval(startDate, endDate, tournament.Id);

                // var filteredDatabaseMatches = new List<Match>();
                if (this.nifTeamSportDataFileController.LocalMatches != null)
                {
                    this.nifTeamSportDataFileController.LocalMatches = (from m in this.nifTeamSportDataFileController.LocalMatches where m.Downloaded select m).ToList();
                }

                Logger.Debug("Number of files downloaded: " + this.nifTeamSportDataFileController.LocalMatches.Count);
                Logger.Debug("Number of matches with a result " + filteredMatches.Count);

                // We must find out if we are in maintenance or not
                if (this.nifTeamSportDataFileController.DataParams.Maintenance == false)
                {
                    if (this.nifTeamSportDataFileController.LocalMatches.Count == filteredMatches.Count)
                    {
                        Logger.InfoFormat("All matches for tournament {0} ({1}) ) has been downloaded and processed", tournament.Name, tournament.Id);
                        continue;
                    }
                }

                if (filteredMatches.Count == 0)
                {
                    Logger.Debug("No matches to be processed");
                    return;
                }

                var finalMatches = filteredMatches.Where(m => m.Downloaded == false)
                    .ToList();

                filteredMatches = finalMatches;

                // If we get an empty list of matches we return...
                if (filteredMatches.Count == 0)
                {
                    Logger.Debug("After last filtering, there was no matches to be processed");
                    return;
                }

                var groupedMatches = (from f in filteredMatches group f by f.MatchDate into matchGroup orderby matchGroup.Key select matchGroup).ToList();

                foreach (var matchGroup in groupedMatches)
                {
                    // Check if we have results
                    this.nifTeamSportDataFileController.xmlTeamSportDocument.Result = this.nifTeamSportDataFileController.HasResults(matchGroup);

                    foreach (var match in matchGroup)
                    {
                        var currentlyFilteredMatches = matchGroup.ToList();
                        this.nifTeamSportDataFileController.LocalMatches = this.nifTeamSportDataFileController.LocalMatches.Where(x => x.MatchDate == matchGroup.Key)
                            .ToList();
                        this.nifTeamSportDataFileController.xmlTeamSportDocument.HasMatchFacts = false;

                        // Let's check if the match is in the database
                        var databaseMatch = this.nifTeamSportDataFileController.DataParams.Results ? matchRepository.GetMatchByMatchIdAndSportId(match.Id, match.SportId) : matchRepository.GetScheduledMatchByMatchIdAndSportId(match.Id, match.SportId);

                        if (databaseMatch == null)
                        {
                            // We must add the match to database
                            Logger.Info("Inserting match " + match.Id + " into database");

                            matchRepository.InsertOne(match);
                        }

                        // Setting downloaded to false
                        match.Downloaded = false;
                        if (this.nifTeamSportDataFileController.DataParams.OperationMode != OperationMode.Maintenance)
                        {
                            // todo: We are to use the old match if a new match is generated, otherwise we are creating files with only one result in it

                            // if (databaseMatch != null && databaseMatch.Downloaded && filteredMatches.Count >= filteredDatabaseMatches.Count)
                            if (databaseMatch != null && currentlyFilteredMatches.Count == this.nifTeamSportDataFileController.LocalMatches.Count)
                            {
                                Logger.Info("Match " + match.Id + ": " + match.HomeTeamName.Trim() + " - " + match.AwayTeamName.Trim() + " already downloaded. No need processing");
                                match.Downloaded = true;

                                continue;
                            }

                            // Just making sure downloaded is false now that we are in maintenance mode
                        }

                        // I need to get the pause results
                        var facade = new TeamSportFacade();
                        var partialresults = facade.GetPartialResultsByMatchId(match.Id);

                        if (partialresults != null)
                        {
                            this.nifTeamSportDataFileController.xmlTeamSportDocument.CreatePartialResults(partialresults, match.Id);
                        }

                        // We should actually get more information about the match.
                        var matchSummary = this.nifTeamSportDataFileController.GetMatchSummary(match.Id);

                        var venue = new Venue();
                        if (matchSummary.ActivityAreaId != null)
                        {
                            var id = Convert.ToInt32(matchSummary.ActivityAreaId);

                            venue = this.nifTeamSportDataFileController.GetMatchVenue(id);
                        }

                        // Get the match players - and only the players
                        match.MatchPlayers = this.nifTeamSportDataFileController.GetMatchPlayers(match)
                            .Where(x => x.SquadIndividualCategoryId == 1)
                            .ToList();

                        // Get the match incidents
                        var matchIncidents = this.nifTeamSportDataFileController.GetMatchIncidents(match);

                        //// We are to get everything from the summary
                        // Cleaning the incidents list a bit.
                        match.MatchIncidents = this.nifTeamSportDataFileController.CleanMatchIncidents(matchIncidents, match.SportId, match.MatchPlayers);

                        match.Referees = this.nifTeamSportDataFileController.GetMatchReferees(match);

                        foreach (var referee in match.Referees)
                        {
                            if (referee.RefereeName == null)
                            {
                                continue;
                            }

                            Logger.DebugFormat("Name of logger: {0}", referee.RefereeName);
                            try
                            {
                                if (referee.RefereeName.Contains(","))
                                {
                                    var refereePart = referee.RefereeName.Split(',');
                                    referee.FirstName = refereePart[0].Trim();
                                    referee.LastName = refereePart[1].Trim();
                                }
                                else
                                {
                                    var refereePart = referee.RefereeName.Split(' ');
                                    referee.FirstName = refereePart[0].Trim();

                                    var lastnameArray = refereePart.Skip(1);

                                    referee.LastName = string.Join(" ", lastnameArray);
                                }
                            }
                            catch (Exception exception)
                            {
                                Logger.Error(exception);
                            }
                        }

                        if (match.MatchPlayers != null && match.MatchPlayers.Count > 0)
                        {
                            Logger.Info("Creating playernode for match " + match.Id);
                            this.nifTeamSportDataFileController.xmlTeamSportDocument.CreatePlayerNode(match.MatchPlayers, match.Id);
                        }

                        if (match.MatchIncidents != null && match.MatchIncidents.Count > 0)
                        {
                            Logger.Info("Creating MatchIncidents-node for match " + match.Id);
                            this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateIncidentNode(match.MatchIncidents, match.Id);
                            this.nifTeamSportDataFileController.xmlTeamSportDocument.HasMatchFacts = true;
                        }

                        if (match.Referees != null && match.Referees.Count > 0)
                        {
                            Logger.InfoFormat("Creating referee-node(s) for match {0}", match.Id);
                            this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateRefereeNode(match.Referees, match.Id);
                        }

                        Logger.InfoFormat("Creating Stadium-node for match {0}", match.Id);
                        this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateStadiumNode(venue, match.Spectators, match.Id);

                        Logger.Info("Creating HomeTeam-node for match " + match.Id);
                        this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateHomeTeamNode(match);

                        Logger.Debug("Creating AwayTeam-node for match " + match.Id);
                        this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateAwayTeamNode(match);

                        tournamentRound = match.RoundId.ToString(CultureInfo.InvariantCulture);
                    }

                    // Get the list of standings from the API
                    var standing = new List<TeamResult>();

                    if (tournament.TypeId == 1)
                    {
                        standing = this.nifTeamSportDataFileController.GetTournamentStanding(tournament.Id);
                    }

                    Logger.Info("Check if we are to create standing:  " + standing.Count);
                    if (standing.Count > 0)
                    {
                        Logger.Info("Creating Standing-node for Tournament " + tournament.Id);
                        this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateTournamentStandingNode(standing);
                        this.nifTeamSportDataFileController.xmlTeamSportDocument.HasStanding = true;
                    }

                    // Creating the document type node
                    Logger.InfoFormat("Creating filename for tournament {0}", tournament.Id);

                    var matchDate = new DateTime();
                    try
                    {
                        var foundMatchDates = (from mg in matchGroup where mg.MatchDate == matchGroup.Key select mg).ToList();

                        if (foundMatchDates.Any())
                        {
                            matchDate = matchGroup.Key;
                        }
                    }
                    catch (Exception exception)
                    {
                        Logger.ErrorFormat("An error occured while getting the date of the match: {0}", exception);
                    }

                    var filename = this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateFileNameBase(organization, tournament, matchDate);

                    // now let's check if we have this document in the database
                    var repository = new DocumentRepository();
                    var version = repository.GetDocumentVersionByTournamentId(tournament.Id);

                    var fullFilename = string.Empty;

                    var documentType = DocumentType.Result;
                    if (this.nifTeamSportDataFileController.DataParams.Results)
                    {
                        if (this.nifTeamSportDataFileController.xmlTeamSportDocument.HasStanding && this.nifTeamSportDataFileController.xmlTeamSportDocument.HasMatchFacts)
                        {
                            fullFilename = filename + "_Fakta_V" + version + ".xml";

                            documentType = DocumentType.MatchFact;
                        }
                        else if (this.nifTeamSportDataFileController.xmlTeamSportDocument.HasStanding && !this.nifTeamSportDataFileController.xmlTeamSportDocument.HasMatchFacts)
                        {
                            fullFilename = filename + "_ResTab_V" + version + ".xml";
                            documentType = DocumentType.Standing;
                        }
                        else if (!this.nifTeamSportDataFileController.xmlTeamSportDocument.HasStanding && !this.nifTeamSportDataFileController.xmlTeamSportDocument.HasMatchFacts)
                        {
                            fullFilename = filename + "_Res_V" + version + ".xml";
                            documentType = DocumentType.Result;
                        }

                        if (!this.nifTeamSportDataFileController.xmlTeamSportDocument.HasStanding && this.nifTeamSportDataFileController.xmlTeamSportDocument.HasMatchFacts)
                        {
                            fullFilename = filename + "_Fakta_V" + version + ".xml";
                            documentType = DocumentType.MatchFact;
                        }
                    }
                    else if (this.nifTeamSportDataFileController.DataParams.Results == false)
                    {
                        fullFilename = filename + "_Sched_V" + version + ".xml";
                        documentType = DocumentType.Schedule;
                    }

                    var document = new Document
                                       {
                                           Created = DateTime.Now, 
                                           Filename = filename, 
                                           SportId = this.nifTeamSportDataFileController.DataParams.SportId, 
                                           TournamentId = tournament.Id, 
                                           Version = version, 
                                           FullName = fullFilename, 
                                           DocumentType = documentType
                                       };

                    if (version == 1)
                    {
                        repository.InsertOne(document);
                    }
                    else
                    {
                        repository.Update(document);
                    }

                    var ageCategory = this.nifTeamSportDataFileController.GetAgeCategory(tournament.Id);
                    Logger.Info("Creating AgeCategory-node for Tournament " + tournament.Id);
                    if (ageCategory != null)
                    {
                        if (ageCategory.Name.ToLower() == "aldersbestemt")
                        {
                            this.nifTeamSportDataFileController.xmlTeamSportDocument.HasMatchFacts = false;
                            this.nifTeamSportDataFileController.xmlTeamSportDocument.HasStanding = true;
                        }
                    }

                    this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateAgeCategoryNode(ageCategory, tournament);
                    this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateMatchNode(matches);

                    Logger.Info("Creating Header-node for Tournament " + tournament.Id);
                    this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateHeaderNode();

                    Logger.Info("Creating Tournament-node for Tournament " + tournament.Id);
                    this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateTournamentNode(tournament, tournamentRound, matches[0].MatchDate, document);

                    Logger.Info("Creating Organization-node for Tournament " + tournament.Id);
                    this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateOrganizationNode(organization);

                    Logger.Info("Creating Sport-node for Tournament " + tournament.Id);
                    this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateSportNode(organization);

                    var customers = this.nifTeamSportDataFileController.GetListOfCustomers(tournament);
                    Logger.Info("Number of customers to receive this file " + customers.Count);

                    Logger.Info("Creating Customer-node for Tournament " + tournament.Id);
                    this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateCustomerNode(customers);

                    /*
                     * Document moved from here
                     */
                    this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateFileNode(document);

                    this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateDocumentTypeNode();

                    // Creating the document node
                    var doc = this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateDocument();

                    foreach (var outputFolder in this.nifTeamSportDataFileController.DataParams.FileOutputFolder)
                    {
                        var path = outputFolder;

                        if (this.nifTeamSportDataFileController.DataParams.FileOutputFolder == null)
                        {
                            if (this.nifTeamSportDataFileController.DataParams.SportId == 4)
                            {
                                path = (from o in this.nifTeamSportDataFileController.DataParams.FileOutputFolders where o.DisciplineId == this.nifTeamSportDataFileController.DataParams.DisciplineId select o.OutputPath).Single();
                            }
                            else
                            {
                                path = (from o in this.nifTeamSportDataFileController.DataParams.FileOutputFolders where o.SportId == this.nifTeamSportDataFileController.DataParams.SportId select o.OutputPath).Single();
                            }
                        }

                        Logger.InfoFormat("Write document {0} to path {1}", filename, path);
                        this.nifTeamSportDataFileController.xmlTeamSportDocument.WriteDocument(doc, path);
                    }

                    // We must get the Id of the matches, then update the database
                    // SetMatchDownloaded
                    var downloadedMatches = doc.SelectNodes("/SportsData/Tournament/Matches/Match");
                    if (downloadedMatches == null || downloadedMatches.Count <= 0)
                    {
                        continue;
                    }

                    foreach (var matchIdNode in from XmlNode node in downloadedMatches select node.SelectSingleNode("@Id"))
                    {
                        if (matchIdNode == null)
                        {
                            Logger.Error("Match Id not collected!!!");
                            throw new Exception("No Match Id Collected while setting Download to 1");
                        }

                        var matchId = Convert.ToInt32(matchIdNode.Value);

                        if (this.nifTeamSportDataFileController.DataParams.Results)
                        {
                            matchRepository.SetMatchDownloaded(matchId);
                        }
                        else
                        {
                            if (this.nifTeamSportDataFileController.DataParams.OperationMode == OperationMode.Distributor)
                            {
                                matchRepository.SetScheduleMatchDownloaded(matchId);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     The create results.
        ///     This method is deprecated because it uses the Search function on the API which is really heavy on the system
        /// </summary>
        public void Deprecated_CreateResults()
        {
            var tournaments = this.GetTournaments();

            if (tournaments == null)
            {
                return;
            }

            this.Deprecated_CreateMatchResults(tournaments);
        }

        /// <summary>
        ///     The get tournaments.
        /// </summary>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        private List<Tournament> GetTournaments()
        {
            Logger.Info("Getting tournaments from database");

            try
            {
                var tournamentRepository = new TournamentRepository();

                if (this.nifTeamSportDataFileController.DataParams.SeasonId == 0)
                {
                    var seasonRepository = new SeasonRepository();
                    var seasons = seasonRepository.GetSeasonsBySportId(this.nifTeamSportDataFileController.DataParams.SportId);
                    if (seasons.Count == 0)
                    {
                    }

                    if (this.nifTeamSportDataFileController.DataParams.DisciplineId > 0)
                    {
                        seasons = seasonRepository.GetSeasonBySportIdAndDisciplineId(this.nifTeamSportDataFileController.DataParams.SportId, this.nifTeamSportDataFileController.DataParams.DisciplineId);
                    }

                    if (!seasons.Any())
                    {
                        Logger.Info("No seasons found, we are returning an empty list");
                        return new List<Tournament>();
                    }

                    var seasonId = (from s in seasons where s.Active select s.Id).Single();

                    this.nifTeamSportDataFileController.DataParams.SeasonId = seasonId;
                }

                return tournamentRepository.GetCustomerTournaments(this.nifTeamSportDataFileController.DataParams.SportId, this.nifTeamSportDataFileController.DataParams.SeasonId);
            }
            catch (Exception exception)
            {
                Logger.Error("An error occured while getting tournaments!");
                Logger.Error(exception);
                Logger.Error(exception.StackTrace);

                return new List<Tournament>();
            }
        }
    }
}

namespace NTB.SportsData.Components.Nif
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.Xml;

    using NTB.SportsData.Classes.Classes;
    using NTB.SportsData.Classes.Enums;
    using NTB.SportsData.Components.Facade;
    using NTB.SportsData.Components.Repositories;

    /// <summary>
    /// The deprecated methods.
    /// </summary>
    internal class DeprecatedMethods
    {
        /// <summary>
        /// The nif team sport data file controller.
        /// </summary>
        private readonly NifTeamSportDataFileController nifTeamSportDataFileController;

        /// <summary>
        /// Initializes a new instance of the <see cref="DeprecatedMethods"/> class.
        /// </summary>
        /// <param name="nifTeamSportDataFileController">
        /// The nif team sport data file controller.
        /// </param>
        public DeprecatedMethods(NifTeamSportDataFileController nifTeamSportDataFileController)
        {
            this.nifTeamSportDataFileController = nifTeamSportDataFileController;
        }

        /// <summary>
        /// The create match results.
        /// </summary>
        /// <param name="tournaments">
        /// The tournaments.
        /// </param>
        /// <exception cref="Exception">
        /// Throws an exception if something goes wrong
        /// </exception>
        public void Deprecated_CreateMatchResults(List<Tournament> tournaments)
        {
            // Set the match repository
            var matchRepository = new MatchRepository();

            this.nifTeamSportDataFileController.xmlTeamSportDocument = new NifTeamSportXmlDocument
                                                                           {
                                                                               RenderResult = false
                                                                           };

            var testing = Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]);

            if (this.nifTeamSportDataFileController.DataParams.PushResults)
            {
                this.nifTeamSportDataFileController.xmlTeamSportDocument.RenderResult = true;
            }

            // Create the organization object, add discipline to it and we are good to go!
            var organization = this.nifTeamSportDataFileController.GetOrganizationFromSportId(this.nifTeamSportDataFileController.DataParams.SportId);
            var discipline = this.nifTeamSportDataFileController.GetFederationDisciplineById(this.nifTeamSportDataFileController.DataParams.DisciplineId);
            if (discipline == null)
            {
                throw new Exception("Discipline is null!");
            }

            organization.DisciplineId = discipline.Id;
            organization.DisciplineName = discipline.Name;

            foreach (var tournament in tournaments)
            {
                NifTeamSportDataFileController.Logger.InfoFormat("Processing tournament {0} with id {1}", tournament.Name, tournament.Id);

                // this is just a cleaner thing. We add this because we need to check if there is a type Id in the tournament
                if (tournament.TypeId == 0)
                {
                    var remoteTournament = this.nifTeamSportDataFileController.GetTournamentTypeId(tournament.Id);

                    tournament.TypeId = remoteTournament.TypeId;

                    var tournamentRepository = new TournamentRepository();
                    tournamentRepository.UpdateTournamentType(tournament);
                }

                var matches = this.nifTeamSportDataFileController.GetMatchResults(tournament);

                if (matches.Count == 0)
                {
                    NifTeamSportDataFileController.Logger.Info("No matches without a result found");
                    continue;
                }

                NifTeamSportDataFileController.Logger.Info("We have found " + matches.Count + " matches.");

                // This loop is just here for debugging proposes. 
                if (testing)
                {
                    NifTeamSportDataFileController.DebugMatchLoop(matches);
                }

                // Get a list of matches that has a result
                var filteredMatches = this.nifTeamSportDataFileController.FilterMatches(matches);

                if (!filteredMatches.Any())
                {
                    NifTeamSportDataFileController.Logger.InfoFormat("After filtering the information from NIF, we have {0} matches to process", filteredMatches.Count);
                    continue;
                }

                var tournamentRound = "0";

                var startDate = this.nifTeamSportDataFileController.SetDate(testing);
                var endDate = this.nifTeamSportDataFileController.SetDate(testing);

                this.nifTeamSportDataFileController.LocalMatches = matchRepository.GetMatchesByDateInterval(startDate, endDate, tournament.Id);

                // var filteredDatabaseMatches = new List<Match>();
                if (this.nifTeamSportDataFileController.LocalMatches != null)
                {
                    this.nifTeamSportDataFileController.LocalMatches = (from m in this.nifTeamSportDataFileController.LocalMatches where m.Downloaded select m).ToList();
                }

                NifTeamSportDataFileController.Logger.Debug("Number of files downloaded: " + this.nifTeamSportDataFileController.LocalMatches.Count);
                NifTeamSportDataFileController.Logger.Debug("Number of matches with a result " + filteredMatches.Count);

                // We must find out if we are in maintenance or not
                if (this.nifTeamSportDataFileController.DataParams.Maintenance == false)
                {
                    if (this.nifTeamSportDataFileController.LocalMatches.Count == filteredMatches.Count)
                    {
                        NifTeamSportDataFileController.Logger.InfoFormat("All matches for tournament {0} ({1}) ) has been downloaded and processed", tournament.Name, tournament.Id);
                        continue;
                    }
                }

                if (filteredMatches.Count == 0)
                {
                    NifTeamSportDataFileController.Logger.Debug("No matches to be processed");
                    return;
                }

                var finalMatches = filteredMatches.Where(m => m.Downloaded == false)
                    .ToList();

                filteredMatches = finalMatches;

                // If we get an empty list of matches we return...
                if (filteredMatches.Count == 0)
                {
                    NifTeamSportDataFileController.Logger.Debug("After last filtering, there was no matches to be processed");
                    return;
                }

                var groupedMatches = (from f in filteredMatches group f by f.MatchDate into matchGroup orderby matchGroup.Key select matchGroup).ToList();

                foreach (var matchGroup in groupedMatches)
                {
                    // Check if we have results
                    this.nifTeamSportDataFileController.xmlTeamSportDocument.Result = this.nifTeamSportDataFileController.HasResults(matchGroup);

                    foreach (var match in matchGroup)
                    {
                        var currentlyFilteredMatches = matchGroup.ToList();
                        this.nifTeamSportDataFileController.LocalMatches = this.nifTeamSportDataFileController.LocalMatches.Where(x => x.MatchDate == matchGroup.Key)
                            .ToList();
                        this.nifTeamSportDataFileController.xmlTeamSportDocument.HasMatchFacts = false;

                        // Let's check if the match is in the database
                        var databaseMatch = this.nifTeamSportDataFileController.DataParams.Results ? matchRepository.GetMatchByMatchIdAndSportId(match.Id, match.SportId) : matchRepository.GetScheduledMatchByMatchIdAndSportId(match.Id, match.SportId);

                        if (databaseMatch == null)
                        {
                            // We must add the match to database
                            NifTeamSportDataFileController.Logger.Info("Inserting match " + match.Id + " into database");

                            matchRepository.InsertOne(match);
                        }

                        // Setting downloaded to false
                        match.Downloaded = false;
                        if (this.nifTeamSportDataFileController.DataParams.OperationMode != OperationMode.Maintenance)
                        {
                            // todo: We are to use the old match if a new match is generated, otherwise we are creating files with only one result in it

                            // if (databaseMatch != null && databaseMatch.Downloaded && filteredMatches.Count >= filteredDatabaseMatches.Count)
                            if (databaseMatch != null && currentlyFilteredMatches.Count == this.nifTeamSportDataFileController.LocalMatches.Count)
                            {
                                NifTeamSportDataFileController.Logger.Info("Match " + match.Id + ": " + match.HomeTeamName.Trim() + " - " + match.AwayTeamName.Trim() + " already downloaded. No need processing");
                                match.Downloaded = true;

                                continue;
                            }

                            // Just making sure downloaded is false now that we are in maintenance mode
                        }

                        // I need to get the pause results
                        var facade = new TeamSportFacade();
                        var partialresults = facade.GetPartialResultsByMatchId(match.Id);

                        if (partialresults != null)
                        {
                            this.nifTeamSportDataFileController.xmlTeamSportDocument.CreatePartialResults(partialresults, match.Id);
                        }

                        // We should actually get more information about the match.
                        var matchSummary = this.nifTeamSportDataFileController.GetMatchSummary(match.Id);

                        var venue = new Venue();
                        if (matchSummary.ActivityAreaId != null)
                        {
                            var id = Convert.ToInt32(matchSummary.ActivityAreaId);

                            venue = this.nifTeamSportDataFileController.GetMatchVenue(id);
                        }

                        // Get the match players - and only the players
                        match.MatchPlayers = this.nifTeamSportDataFileController.GetMatchPlayers(match)
                            .Where(x => x.SquadIndividualCategoryId == 1)
                            .ToList();

                        // Get the match incidents
                        var matchIncidents = this.nifTeamSportDataFileController.GetMatchIncidents(match);

                        //// We are to get everything from the summary
                        // Cleaning the incidents list a bit.
                        match.MatchIncidents = this.nifTeamSportDataFileController.CleanMatchIncidents(matchIncidents, match.SportId, match.MatchPlayers);

                        match.Referees = this.nifTeamSportDataFileController.GetMatchReferees(match);

                        foreach (var referee in match.Referees)
                        {
                            if (referee.RefereeName == null)
                            {
                                continue;
                            }

                            NifTeamSportDataFileController.Logger.DebugFormat("Name of logger: {0}", referee.RefereeName);
                            try
                            {
                                if (referee.RefereeName.Contains(","))
                                {
                                    var refereePart = referee.RefereeName.Split(',');
                                    referee.FirstName = refereePart[0].Trim();
                                    referee.LastName = refereePart[1].Trim();
                                }
                                else
                                {
                                    var refereePart = referee.RefereeName.Split(' ');
                                    referee.FirstName = refereePart[0].Trim();

                                    var lastnameArray = refereePart.Skip(1);

                                    referee.LastName = string.Join(" ", lastnameArray);
                                }
                            }
                            catch (Exception exception)
                            {
                                NifTeamSportDataFileController.Logger.Error(exception);
                            }
                        }

                        if (match.MatchPlayers != null && match.MatchPlayers.Count > 0)
                        {
                            NifTeamSportDataFileController.Logger.Info("Creating playernode for match " + match.Id);
                            this.nifTeamSportDataFileController.xmlTeamSportDocument.CreatePlayerNode(match.MatchPlayers, match.Id);
                        }

                        if (match.MatchIncidents != null && match.MatchIncidents.Count > 0)
                        {
                            NifTeamSportDataFileController.Logger.Info("Creating MatchIncidents-node for match " + match.Id);
                            this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateIncidentNode(match.MatchIncidents, match.Id);
                            this.nifTeamSportDataFileController.xmlTeamSportDocument.HasMatchFacts = true;
                        }

                        if (match.Referees != null && match.Referees.Count > 0)
                        {
                            NifTeamSportDataFileController.Logger.InfoFormat("Creating referee-node(s) for match {0}", match.Id);
                            this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateRefereeNode(match.Referees, match.Id);
                        }

                        NifTeamSportDataFileController.Logger.InfoFormat("Creating Stadium-node for match {0}", match.Id);
                        this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateStadiumNode(venue, match.Spectators, match.Id);

                        NifTeamSportDataFileController.Logger.Info("Creating HomeTeam-node for match " + match.Id);
                        this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateHomeTeamNode(match);

                        NifTeamSportDataFileController.Logger.Debug("Creating AwayTeam-node for match " + match.Id);
                        this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateAwayTeamNode(match);

                        tournamentRound = match.RoundId.ToString(CultureInfo.InvariantCulture);
                    }

                    // Get the list of standings from the API
                    var standing = new List<TeamResult>();

                    if (tournament.TypeId == 1)
                    {
                        standing = this.nifTeamSportDataFileController.GetTournamentStanding(tournament.Id);
                    }

                    NifTeamSportDataFileController.Logger.Info("Check if we are to create standing:  " + standing.Count);
                    if (standing.Count > 0)
                    {
                        NifTeamSportDataFileController.Logger.Info("Creating Standing-node for Tournament " + tournament.Id);
                        this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateTournamentStandingNode(standing);
                        this.nifTeamSportDataFileController.xmlTeamSportDocument.HasStanding = true;
                    }

                    // Creating the document type node
                    NifTeamSportDataFileController.Logger.InfoFormat("Creating filename for tournament {0}", tournament.Id);

                    var matchDate = new DateTime();
                    try
                    {
                        var foundMatchDates = (from mg in matchGroup where mg.MatchDate == matchGroup.Key select mg).ToList();

                        if (foundMatchDates.Any())
                        {
                            matchDate = matchGroup.Key;
                        }
                    }
                    catch (Exception exception)
                    {
                        NifTeamSportDataFileController.Logger.ErrorFormat("An error occured while getting the date of the match: {0}", exception);
                    }

                    var filename = this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateFileNameBase(organization, tournament, matchDate);

                    // now let's check if we have this document in the database
                    var repository = new DocumentRepository();
                    var version = repository.GetDocumentVersionByTournamentId(tournament.Id);

                    var fullFilename = string.Empty;

                    var documentType = DocumentType.Result;
                    if (this.nifTeamSportDataFileController.DataParams.Results)
                    {
                        if (this.nifTeamSportDataFileController.xmlTeamSportDocument.HasStanding && this.nifTeamSportDataFileController.xmlTeamSportDocument.HasMatchFacts)
                        {
                            fullFilename = filename + "_Fakta_V" + version + ".xml";

                            documentType = DocumentType.MatchFact;
                        }
                        else if (this.nifTeamSportDataFileController.xmlTeamSportDocument.HasStanding && !this.nifTeamSportDataFileController.xmlTeamSportDocument.HasMatchFacts)
                        {
                            fullFilename = filename + "_ResTab_V" + version + ".xml";
                            documentType = DocumentType.Standing;
                        }
                        else if (!this.nifTeamSportDataFileController.xmlTeamSportDocument.HasStanding && !this.nifTeamSportDataFileController.xmlTeamSportDocument.HasMatchFacts)
                        {
                            fullFilename = filename + "_Res_V" + version + ".xml";
                            documentType = DocumentType.Result;
                        }

                        if (!this.nifTeamSportDataFileController.xmlTeamSportDocument.HasStanding && this.nifTeamSportDataFileController.xmlTeamSportDocument.HasMatchFacts)
                        {
                            fullFilename = filename + "_Fakta_V" + version + ".xml";
                            documentType = DocumentType.MatchFact;
                        }
                    }
                    else if (this.nifTeamSportDataFileController.DataParams.Results == false)
                    {
                        fullFilename = filename + "_Sched_V" + version + ".xml";
                        documentType = DocumentType.Schedule;
                    }

                    var document = new Document
                                       {
                                           Created = DateTime.Now, 
                                           Filename = filename, 
                                           SportId = this.nifTeamSportDataFileController.DataParams.SportId, 
                                           TournamentId = tournament.Id, 
                                           Version = version, 
                                           FullName = fullFilename, 
                                           DocumentType = documentType
                                       };

                    if (version == 1)
                    {
                        repository.InsertOne(document);
                    }
                    else
                    {
                        repository.Update(document);
                    }

                    var ageCategory = this.nifTeamSportDataFileController.GetAgeCategory(tournament.Id);
                    NifTeamSportDataFileController.Logger.Info("Creating AgeCategory-node for Tournament " + tournament.Id);
                    if (ageCategory != null)
                    {
                        if (ageCategory.Name.ToLower() == "aldersbestemt")
                        {
                            this.nifTeamSportDataFileController.xmlTeamSportDocument.HasMatchFacts = false;
                            this.nifTeamSportDataFileController.xmlTeamSportDocument.HasStanding = true;
                        }
                    }

                    this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateAgeCategoryNode(ageCategory, tournament);
                    this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateMatchNode(matches);

                    NifTeamSportDataFileController.Logger.Info("Creating Header-node for Tournament " + tournament.Id);
                    this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateHeaderNode();

                    NifTeamSportDataFileController.Logger.Info("Creating Tournament-node for Tournament " + tournament.Id);
                    this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateTournamentNode(tournament, tournamentRound, matches[0].MatchDate, document);

                    NifTeamSportDataFileController.Logger.Info("Creating Organization-node for Tournament " + tournament.Id);
                    this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateOrganizationNode(organization);

                    NifTeamSportDataFileController.Logger.Info("Creating Sport-node for Tournament " + tournament.Id);
                    this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateSportNode(organization);

                    var customers = this.nifTeamSportDataFileController.GetListOfCustomers(tournament);
                    NifTeamSportDataFileController.Logger.Info("Number of customers to receive this file " + customers.Count);

                    NifTeamSportDataFileController.Logger.Info("Creating Customer-node for Tournament " + tournament.Id);
                    this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateCustomerNode(customers);

                    /*
                     * Document moved from here
                     */
                    this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateFileNode(document);

                    this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateDocumentTypeNode();

                    // Creating the document node
                    var doc = this.nifTeamSportDataFileController.xmlTeamSportDocument.CreateDocument();

                    foreach (var outputFolder in this.nifTeamSportDataFileController.DataParams.FileOutputFolder)
                    {
                        var path = outputFolder;

                        if (this.nifTeamSportDataFileController.DataParams.FileOutputFolder == null)
                        {
                            if (this.nifTeamSportDataFileController.DataParams.SportId == 4)
                            {
                                path = (from o in this.nifTeamSportDataFileController.DataParams.FileOutputFolders where o.DisciplineId == this.nifTeamSportDataFileController.DataParams.DisciplineId select o.OutputPath).Single();
                            }
                            else
                            {
                                path = (from o in this.nifTeamSportDataFileController.DataParams.FileOutputFolders where o.SportId == this.nifTeamSportDataFileController.DataParams.SportId select o.OutputPath).Single();
                            }
                        }

                        NifTeamSportDataFileController.Logger.InfoFormat("Write document {0} to path {1}", filename, path);
                        this.nifTeamSportDataFileController.xmlTeamSportDocument.WriteDocument(doc, path);
                    }

                    // We must get the Id of the matches, then update the database
                    // SetMatchDownloaded
                    var downloadedMatches = doc.SelectNodes("/SportsData/Tournament/Matches/Match");
                    if (downloadedMatches == null || downloadedMatches.Count <= 0)
                    {
                        continue;
                    }

                    foreach (var matchIdNode in from XmlNode node in downloadedMatches select node.SelectSingleNode("@Id"))
                    {
                        if (matchIdNode == null)
                        {
                            NifTeamSportDataFileController.Logger.Error("Match Id not collected!!!");
                            throw new Exception("No Match Id Collected while setting Download to 1");
                        }

                        var matchId = Convert.ToInt32(matchIdNode.Value);

                        if (this.nifTeamSportDataFileController.DataParams.Results)
                        {
                            matchRepository.SetMatchDownloaded(matchId);
                        }
                        else
                        {
                            if (this.nifTeamSportDataFileController.DataParams.OperationMode == OperationMode.Distributor)
                            {
                                matchRepository.SetScheduleMatchDownloaded(matchId);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     The create results.
        ///     This method is deprecated because it uses the Search function on the API which is really heavy on the system
        /// </summary>
        public void Deprecated_CreateResults()
        {
            var tournaments = this.nifTeamSportDataFileController.GetTournaments();

            if (tournaments == null)
            {
                return;
            }

            this.Deprecated_CreateMatchResults(tournaments);
        }

        /// <summary>
        /// The get match results.
        /// </summary>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        /// 
        private List<Match> Deprecated_GetMatchResults(Tournament tournament)
        {
            var creator = new NifMatchDataFileCreator { DataFileParams = this.DataParams };

            // Get list of matches
            var matches = new List<Match>();

            var result = creator.GetRemoteMatchesByTournamentId(this.DataParams.DateStart, this.DataParams.DateEnd, tournament.Id);

            if (!result.Any())
            {
                Logger.Info("No matches found for tournament " + tournament.Name);
                return new List<Match>();
            }

            foreach (var match in result)
            {
                match.TournamentId = tournament.Id;
                match.SportId = this.DataParams.SportId;
            }

            matches.AddRange(result);

            return matches;
        }

        // private void oldCode(List<MatchIncident> matchIncidents, int sportId)
        // {
        // var goalIncidents = new List<MatchIncident>();
        // foreach (var matchIncident in matchIncidents.Where(x => x.IncidentSubType != null))
        // {
        // if (matchIncident.IncidentSubType.ToLower() == "m�l")
        // {
        // goalIncidents.Add(matchIncident);
        // }
        // }

        // // We shall now get the player who scored
        // var scoreIncidents = new List<MatchIncident>();
        // foreach (var matchIncident in goalIncidents)
        // {
        // var currentMatchIncident = matchIncident;
        // // filteredMatches = (from m in matches where m.StatusCode != null select m).ToList();
        // var scoreIncidentDetails = (from m in matchIncidents 
        // where m.ParentId == currentMatchIncident.MatchIncidentId 
        // && m.IncidentSubType.ToLower() == "kaster"
        // select m).ToList();

        // if (scoreIncidentDetails.Any())
        // {
        // // Adding text to define the parent action
        // foreach (var si in scoreIncidentDetails)
        // {
        // si.IncidentShort = "m�l";

        // }

        // // Add to list of score incidents
        // scoreIncidents.AddRange(scoreIncidentDetails);
        // }
        // }

        // matchIncidents = scoreIncidents;
        // }

        // // List<MatchIncident> matchIncidents = repository.GetMatchIncidents(match.Id);

        // return matchIncidents;

        }
    }
}