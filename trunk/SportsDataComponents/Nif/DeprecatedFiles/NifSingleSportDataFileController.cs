﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using log4net.Config;
using NTB.SportsData.Classes.Classes;

namespace NTB.SportsData.Components.Nif
{
    /// <summary>
    ///     This class does the actuall setting up of creating the datafile and the document
    /// </summary>
    public class NifSingleSportDataFileController
    {
        public DataFileParams DataParams { get; set; }

        private static readonly ILog Logger = LogManager.GetLogger(typeof(NifSingleSportDataFileController));

        public NifSingleSportDataFileController()
        {
            // Set up logger
            XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                BasicConfigurator.Configure();
            }
        }

        public bool Configure()
        {
            if (DataParams == null)
            {
                throw new Exception("DataParams is not set!");
            }
            return true;
        }
    }
}
