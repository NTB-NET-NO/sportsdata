// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NifTeamSportDatabase.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Components.Nif
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;

    using log4net;

    using NTB.SportsData.Classes.Classes;
    using NTB.SportsData.Components.Nff;

    /// <summary>
    ///     The nif team sport database.
    /// </summary>
    public class NifTeamSportDatabase
    {
        // Database variables
        #region Constants

        /// <summary>
        ///     The final.
        /// </summary>
        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(NffSportsDataDatabase));

        #endregion

        #region Fields

        /// <summary>
        ///     The _my sql connection.
        /// </summary>
        private SqlConnection mySqlConnection;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The set download.
        /// </summary>
        /// <param name="eventId">
        /// The event id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool SetDownload(int eventId)
        {
            using (this.mySqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
            {
                try
                {
                    if (this.mySqlConnection.State != ConnectionState.Open)
                    {
                        this.mySqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        this.mySqlConnection.Open();
                    }

                    var command = new SqlCommand
                                      {
                                          Connection = this.mySqlConnection, 
                                          CommandText = "Service_SetDownload"
                                      };

                    command.Parameters.Add(new SqlParameter("@EventId", eventId));

                    // Telling the system that this is a StoredProcedure
                    command.CommandType = CommandType.StoredProcedure;

                    if (command.ExecuteNonQuery() == 1)
                    {
                        if (this.mySqlConnection.State != ConnectionState.Closed)
                        {
                            this.mySqlConnection.Close();
                        }

                        return true;
                    }

                    if (this.mySqlConnection.State != ConnectionState.Closed)
                    {
                        this.mySqlConnection.Close();
                    }

                    return false;
                }
                catch (SqlException exception)
                {
                    Logger.Error(exception);
                    return false;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return false;
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     The get matches.
        /// </summary>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        /// <exception cref="NotImplementedException">
        ///     Not implemented
        /// </exception>
        internal List<TeamSportEvent> GetMatches()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}