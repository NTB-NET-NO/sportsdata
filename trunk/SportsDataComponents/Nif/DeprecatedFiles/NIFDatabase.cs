// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NIFDatabase.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Components.Nif
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;

    using log4net;

    using NTB.SportsData.Classes.Classes;

    // Adding log4net support

    /// <summary>
    ///     The nif database.
    /// </summary>
    public class NifDatabase
    {
        /// <summary>
        ///     The final.
        /// </summary>
        private const bool Final = false;

        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(NifDatabase));

        /// <summary>
        ///     The _my sql connection.
        /// </summary>
        private SqlConnection mySqlConnection;

        /// <summary>
        ///     The _sqlreader.
        /// </summary>
        private SqlDataReader sqlreader;

        // Can I have getters and setters here?

        /// <summary>
        ///     Initializes a new instance of the <see cref="NifDatabase" /> class.
        /// </summary>
        public NifDatabase()
        {
            var connectionString = ConfigurationManager.AppSettings["ConnectionString"];
            this.mySqlConnection = new SqlConnection(connectionString);

            if (this.mySqlConnection.State != ConnectionState.Open)
            {
                // Open the connection to the database
                this.mySqlConnection.Open();
            }
        }

        /// <summary>
        ///     Gets a value indicating whether final result.
        /// </summary>
        public bool FinalResult
        {
            get
            {
                return Final;
            }
        }

        /// <summary>
        ///     The close.
        /// </summary>
        public void Close()
        {
            if (this.mySqlConnection.State == ConnectionState.Open)
            {
                this.mySqlConnection.Close();
            }
        }

        /// <summary>
        ///     This method initiates the object. We are here checking if District-table and Tournament-Table are populated
        /// </summary>
        public void Init()
        {
        }

        /// <summary>
        /// The get tournament by match id.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int GetTournamentByMatchId(int matchId)
        {
            var tournamentId = 0;
            var myCommand = this.Command("Service_GetTournamentIdByMatchId");
            myCommand.CommandType = CommandType.StoredProcedure;

            myCommand.Parameters.Add(new SqlParameter("@MatchId", matchId));

            var reader = myCommand.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    tournamentId = Convert.ToInt32(reader["TournamentId"].ToString());
                }
            }

            return tournamentId;
        }

        /// <summary>
        ///     The add tournament data.
        /// </summary>
        public void AddTournamentData()
        {
        }

        /// <summary>
        ///     The update tournament data.
        /// </summary>
        public void UpdateTournamentData()
        {
        }

        #region Daily

        /// <summary>
        ///     The delete documents.
        /// </summary>
        /// <returns>
        ///     The <see cref="int" />.
        /// </returns>
        public int DeleteDocuments()
        {
            try
            {
                // We are cleaning up document table so that we are ready to create new documents for today
                const string SqlQuery = "DELETE FROM Document";

                var commander = this.Command(SqlQuery);

                var sqlreader = commander.ExecuteReader();

                return sqlreader.RecordsAffected;

                // sqlconnection.CreateCommand
            }
            catch (SqlException sqlexception)
            {
                Logger.Error(sqlexception);
                return 0;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                return 0;
            }
        }

        #endregion

        /// <summary>
        /// The command.
        /// </summary>
        /// <param name="sqlQuery">
        /// The sql query.
        /// </param>
        /// <returns>
        /// The <see cref="SqlCommand"/>.
        /// </returns>
        public SqlCommand Command(string sqlQuery)
        {
            var command = new SqlCommand
                              {
                                  Connection = this.mySqlConnection, 
                                  CommandText = sqlQuery
                              };

            // sqlreader = command.ExecuteReader();
            return command;
        }

        /// <summary>
        ///     The get one result file.
        /// </summary>
        /// <returns>
        ///     The <see cref="string" />.
        /// </returns>
        public string GetOneResultFile()
        {
            const string StrSql = "SELECT TOP(1) DocumentData FROM Document;";

            this.mySqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]);
            if (this.mySqlConnection.State != ConnectionState.Open)
            {
                Logger.Debug("Start connecting!");

                this.mySqlConnection.Open();

                Logger.Debug("End connecting!");

                // Now performing the Query
                var command = new SqlCommand
                                  {
                                      Connection = this.mySqlConnection, 
                                      CommandText = StrSql
                                  };

                this.sqlreader = command.ExecuteReader();
                var documentData = string.Empty;
                if (this.sqlreader.HasRows)
                {
                    // sqlreader["HomeTeam"].ToString();
                    try
                    {
                        while (this.sqlreader.Read())
                        {
                            documentData = this.sqlreader["DocumentData"].ToString();
                        }
                    }
                    catch (SqlException sqlexception)
                    {
                        Logger.Error(sqlexception);
                    }
                    catch (Exception exception)
                    {
                        Logger.Error(exception);
                    }
                }

                this.mySqlConnection.Close();

                return documentData;
            }

            return string.Empty;
        }

        /// <summary>
        /// This method shall be renamed to something else. It shall also return a list of customers
        /// </summary>
        /// <param name="matchId">
        /// the match id
        /// </param>
        /// <param name="sportId">
        /// the sport id
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Customer> GetListOfCustomers(int matchId, int sportId)
        {
            using (var myConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
            {
                var myCommand = new SqlCommand("ServiceGetJobIdAndTournamentIdByMatchId", myConnection);

                myCommand.Parameters.Add(new SqlParameter("@MatchId", matchId));

                myCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                myCommand.CommandType = CommandType.StoredProcedure;
                if (myConnection.State != ConnectionState.Open)
                {
                    myConnection.Open();
                }

                var mySqlDataReader = myCommand.ExecuteReader();

                // xTODO: Continue here
                /**
                 * What we are to do: 
                 * First we get the TournamentId and JobId. We then use this to find
                 * the customers that shall have these data.
                 * We are to return a list of customernames
                 * 
                 */
                var myCustomers = new List<Customer>();
                if (mySqlDataReader.HasRows)
                {
                    while (mySqlDataReader.Read())
                    {
                        var myCustomer = new Customer
                                             {
                                                 Name = "Norsk Telegrambyrå"
                                             };

                        myCustomers.Add(myCustomer);
                    }

                    return myCustomers;
                }

                // logger.Debug("No customers needs these data");
                // TODO: Change this to return null-list
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["livetesting"]))
                {
                    var myCustomer = new Customer();
                    myCustomer.Name = "Norsk Telegrambyrå";
                    myCustomers.Add(myCustomer);
                    return myCustomers;
                }

                myConnection.Close();

                // TODO: Change this to return the Customer-list
                return myCustomers;
            }
        }

        /// <summary>
        ///     Method to find out if the matches table is empty or not.
        ///     Returns the number of matches we have stored in the database
        /// </summary>
        /// <returns>
        ///     Returns an integer
        /// </returns>
        public int CheckMatchesTable()
        {
            try
            {
                using (var myConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    var myCommand = new SqlCommand("Service_GetNumberOfMatches", myConnection)
                                        {
                                            CommandType = CommandType.StoredProcedure
                                        };

                    if (myConnection.State != ConnectionState.Open)
                    {
                        myConnection.Open();
                    }

                    var mySqlDataReader = myCommand.ExecuteReader();

                    var numberOfMatches = 0;
                    if (mySqlDataReader.HasRows)
                    {
                        while (mySqlDataReader.Read())
                        {
                            numberOfMatches = Convert.ToInt32(mySqlDataReader["NumberOfMatches"].ToString());
                        }

                        return numberOfMatches;
                    }

                    myConnection.Close();
                    myConnection.Dispose();
                    return 0;
                }
            }
            catch (SqlException sqlexception)
            {
                Logger.Debug(sqlexception);
                Logger.Debug(sqlexception);
                return 0;
            }
            catch (Exception exception)
            {
                Logger.Debug(exception);
                Logger.Debug(exception);
                return 0;
            }
        }

        /// <summary>
        /// The set download.
        /// </summary>
        /// <param name="eventId">
        /// The event id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool SetDownload(int eventId)
        {
            using (var myConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
            {
                try
                {
                    var command = new SqlCommand
                                      {
                                          Connection = myConnection, 
                                          CommandText = "Service_SetDownload"
                                      };

                    // var sqlFormattedDate = myDateTime.Date.ToString("yyyy-MM-dd HH:mm:ss");
                    // new DateTime();
                    command.Parameters.Add(new SqlParameter("@EventId", eventId));

                    // Telling the system that this is a StoredProcedure
                    command.CommandType = CommandType.StoredProcedure;

                    if (myConnection.State != ConnectionState.Open)
                    {
                        myConnection.Open();
                    }

                    if (command.ExecuteNonQuery() == 1)
                    {
                        if (myConnection.State != ConnectionState.Closed)
                        {
                            myConnection.Close();
                        }

                        return true;
                    }

                    if (myConnection.State != ConnectionState.Closed)
                    {
                        myConnection.Close();
                    }

                    return false;
                }
                catch (SqlException exception)
                {
                    Logger.Error(exception);
                    return false;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return false;
                }
            }
        }

        /// <summary>
        ///     Get events from today from the database
        /// </summary>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<SportEvent> GetEvents()
        {
            using (var myConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
            {
                try
                {
                    var command = new SqlCommand
                                      {
                                          Connection = myConnection, 
                                          CommandText = "Service_GetEvents"
                                      };

                    // var sqlFormattedDate = myDateTime.Date.ToString("yyyy-MM-dd HH:mm:ss");
                    // DateTime myDateTime = new DateTime();
                    command.Parameters.Add("@Today", SqlDbType.DateTime);
                    command.Parameters["@Today"].Value = DateTime.Today;

                    // Telling the system that this is a StoredProcedure
                    command.CommandType = CommandType.StoredProcedure;

                    if (myConnection.State != ConnectionState.Open)
                    {
                        myConnection.Open();
                    }

                    // Creating the SQL-reader.
                    this.sqlreader = command.ExecuteReader();

                    var myEvents = new List<SportEvent>();
                    if (this.sqlreader.HasRows)
                    {
                        while (this.sqlreader.Read())
                        {
                            var myEvent = new SportEvent
                                              {
                                                  EventName = this.sqlreader["Name"].ToString(), 
                                                  EventId = Convert.ToInt32(this.sqlreader["EventId"].ToString()), 
                                                  EventDateStart = Convert.ToDateTime(this.sqlreader["DateStart"].ToString()), 
                                                  EventDateEnd = Convert.ToDateTime(this.sqlreader["DateEnd"].ToString()), 
                                                  EventLocation = this.sqlreader["Location"].ToString(), 
                                                  EventTimeStart = this.sqlreader["TimeStart"] == DBNull.Value ? 0 : Convert.ToInt32(this.sqlreader["TimeStart"].ToString()), 
                                                  EventTimeEnd = this.sqlreader["TimeEnd"] == DBNull.Value ? 0 : Convert.ToInt32(this.sqlreader["TimeEnd"].ToString()), 
                                                  ActivityId = Convert.ToInt32(this.sqlreader["ActivityId"].ToString()), 
                                                  EventOrganizerId = Convert.ToInt32(this.sqlreader["OrganizerId"].ToString())
                                              };

                            // Field2 = rdr.GetSqlInt32(Field2_Ordinal).ToNullableInt32()
                            myEvents.Add(myEvent);
                        }
                    }

                    if (myConnection.State != ConnectionState.Closed)
                    {
                        myConnection.Close();
                    }

                    return myEvents;
                }
                catch (SqlException sqlexception)
                {
                    Logger.Error(sqlexception);
                    return null;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return null;
                }
            }
        }

        /// <summary>
        /// Inserts or updates events in the database
        /// </summary>
        /// <param name="myEvents">
        /// Holds a list of events that we are to insert or update
        /// </param>
        public void InsertEvent(List<SportEvent> myEvents)
        {
            Logger.Debug("Number of items: " + myEvents.Count);
            foreach (var myEvent in myEvents)
            {
                Logger.Debug("Checking if " + myEvent.EventName + " is in the database and if not, we are to insert");

                // First we have to check if the event is in the database already.
                // If it is, we need to check if some of the values that we have has been changed
                // If some data has been changed, we need to change them in our database
                // So we have to either update or insert

                // Can we do this in StoredProcedures? (The whole thing in one go...)

                // We don't do it in one go. What we do is get the data from the database, then check against the object
                using (var myConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    try
                    {
                        var command = new SqlCommand
                                          {
                                              Connection = myConnection, 
                                              CommandText = "Service_CheckEvents"
                                          };

                        command.Parameters.Add(new SqlParameter("@EventId", myEvent.EventId));

                        // Telling the system that this is a StoredProcedure
                        command.CommandType = CommandType.StoredProcedure;

                        if (myConnection.State != ConnectionState.Open)
                        {
                            myConnection.Open();
                        }

                        // Creating the SQL-reader.
                        this.sqlreader = command.ExecuteReader();

                        if (this.sqlreader.HasRows)
                        {
                            var updateEvent = false;

                            // We are now checking what we have gotten and if there are changes.
                            // If there are changes, we shall update.
                            while (this.sqlreader.Read())
                            {
                                if (myEvent.EventName != this.sqlreader["EventName"].ToString())
                                {
                                    updateEvent = true;
                                }

                                if (myEvent.EventDateStart != Convert.ToDateTime(this.sqlreader["DateStart"].ToString()))
                                {
                                    updateEvent = true;
                                }

                                if (myEvent.EventDateEnd != Convert.ToDateTime(this.sqlreader["DateEnd"].ToString()))
                                {
                                    updateEvent = true;
                                }

                                if (myEvent.EventLocation != this.sqlreader["Location"].ToString())
                                {
                                    updateEvent = true;
                                }

                                if (myEvent.EventTimeStart != null)
                                {
                                    if (myEvent.EventTimeStart != Convert.ToInt32(this.sqlreader["TimeStart"].ToString()))
                                    {
                                        updateEvent = true;
                                    }
                                }

                                if (myEvent.EventTimeEnd != null)
                                {
                                    if (myEvent.EventTimeEnd != Convert.ToInt32(this.sqlreader["TimeEnd"].ToString()))
                                    {
                                        updateEvent = true;
                                    }
                                }

                                if (myEvent.EventOrganizerId != Convert.ToInt32(this.sqlreader["OrganizerId"].ToString()))
                                {
                                    updateEvent = true;
                                }

                                if (myEvent.ActivityId != Convert.ToInt32(this.sqlreader["ActivityId"].ToString()))
                                {
                                    updateEvent = true;
                                }

                                if (updateEvent)
                                {
                                    if (myEvent.EventDateEnd == null)
                                    {
                                        myEvent.EventDateEnd = myEvent.EventDateStart;
                                    }

                                    using (var myUpdateConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                                    {
                                        Logger.Debug("We are updating!");

                                        // We shall update the event
                                        var myUpdateCommand = new SqlCommand
                                                                  {
                                                                      Connection = myUpdateConnection, 
                                                                      CommandText = "Service_UpdateEvent"
                                                                  };
                                        myUpdateCommand.Parameters.Add(new SqlParameter("@EventId", myEvent.EventId));
                                        myUpdateCommand.Parameters.Add(new SqlParameter("@ActivityId", myEvent.ActivityId));
                                        myUpdateCommand.Parameters.Add(new SqlParameter("@OrganizerId", myEvent.EventOrganizerId));
                                        myUpdateCommand.Parameters.Add(new SqlParameter("@EventName", myEvent.EventName));
                                        myUpdateCommand.Parameters.Add(new SqlParameter("@Location", myEvent.EventLocation));
                                        myUpdateCommand.Parameters.Add(new SqlParameter("@DateStart", myEvent.EventDateStart));
                                        myUpdateCommand.Parameters.Add(new SqlParameter("@DateEnd", myEvent.EventDateEnd));
                                        myUpdateCommand.Parameters.Add(new SqlParameter("@TimeStart", myEvent.EventTimeStart));
                                        myUpdateCommand.Parameters.Add(new SqlParameter("@TimeEnd", myEvent.EventTimeEnd));

                                        // Telling the system that this is a StoredProcedure
                                        myUpdateCommand.CommandType = CommandType.StoredProcedure;

                                        if (myUpdateConnection.State != ConnectionState.Open)
                                        {
                                            myUpdateConnection.Open();
                                        }

                                        myUpdateCommand.ExecuteNonQuery();

                                        if (myUpdateConnection.State != ConnectionState.Closed)
                                        {
                                            myUpdateConnection.Close();
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            // We have not found a row and so we shall insert
                            if (myEvent.EventDateEnd == null)
                            {
                                myEvent.EventDateEnd = myEvent.EventDateStart;
                            }

                            using (var myUpdateConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                            {
                                Logger.Debug("We are Inserting!");

                                // We shall update the event
                                var myUpdateCommand = new SqlCommand
                                                          {
                                                              Connection = myUpdateConnection, 
                                                              CommandText = "Service_InsertEvent"
                                                          };
                                myUpdateCommand.Parameters.Add(new SqlParameter("@EventId", myEvent.EventId));
                                myUpdateCommand.Parameters.Add(new SqlParameter("@ActivityId", myEvent.ActivityId));
                                myUpdateCommand.Parameters.Add(new SqlParameter("@OrganizerId", myEvent.EventOrganizerId));
                                myUpdateCommand.Parameters.Add(new SqlParameter("@EventName", myEvent.EventName));
                                myUpdateCommand.Parameters.Add(new SqlParameter("@Location", myEvent.EventLocation));
                                myUpdateCommand.Parameters.Add(new SqlParameter("@DateStart", myEvent.EventDateStart));
                                myUpdateCommand.Parameters.Add(new SqlParameter("@DateEnd", myEvent.EventDateEnd));
                                if (myEvent.EventTimeStart != null)
                                {
                                    myUpdateCommand.Parameters.Add(new SqlParameter("@TimeStart", myEvent.EventTimeStart));
                                }

                                if (myEvent.EventTimeEnd != null)
                                {
                                    myUpdateCommand.Parameters.Add(new SqlParameter("@TimeEnd", myEvent.EventTimeEnd));
                                }

                                // Telling the system that this is a StoredProcedure
                                myUpdateCommand.CommandType = CommandType.StoredProcedure;

                                if (myUpdateConnection.State != ConnectionState.Open)
                                {
                                    myUpdateConnection.Open();
                                }

                                myUpdateCommand.ExecuteNonQuery();

                                if (myUpdateConnection.State != ConnectionState.Closed)
                                {
                                    myUpdateConnection.Close();
                                }
                            }
                        }

                        if (myConnection.State != ConnectionState.Closed)
                        {
                            myConnection.Close();
                        }
                    }
                    catch (SqlException sqlexception)
                    {
                        Logger.Error(sqlexception);
                    }
                    catch (Exception exception)
                    {
                        Logger.Error(exception);
                    }

                    if (myConnection.State != ConnectionState.Closed)
                    {
                        myConnection.Close();
                    }

                    // return null;
                }
            }
        }

        #region Weekly

        // public int deleteData()
        // {
        // try
        // {
        // // xTODO: Fix Document-table so that we have a Downloaded flag there to
        // string SQLQuery = "DELETE FROM Matches WHERE Downloaded=1; DELETE FROM Document";

        // SqlCommand commander = Command(SQLQuery);

        // SqlDataReader sqlreader = commander.ExecuteReader();

        // return sqlreader.RecordsAffected;

        // }
        // catch (SqlException sqlexception)
        // {
        // logger.Error(sqlexception);
        // return 0;
        // }
        // catch (Exception exception)
        // {
        // logger.Error(exception);
        // return 0;
        // }
        // finally
        // {

        // }
        // }
        #endregion
    }
}