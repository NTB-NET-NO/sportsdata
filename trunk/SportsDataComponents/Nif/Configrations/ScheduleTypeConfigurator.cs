﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScheduleTypeConfigure.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The schedule type configure.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.Nif.Configurations
{
    using System;
    using System.Xml;

    using log4net;

    using NTB.SportsData.Classes.Enums;
    using NTB.SportsData.Components.Nif.Interfaces;

    using Quartz;

    /// <summary>
    /// The schedule type configure.
    /// </summary>
    public class ScheduleTypeConfigure 
    {
        /// <summary>
        ///     Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ScheduleTypeConfigure));

        /// <summary>
        /// Gets or sets a value indicating whether schedule type.
        /// </summary>
        public ScheduleType ScheduleType { get; set; }

        /// <summary>
        /// Gets or sets the instance name.
        /// </summary>
        public string InstanceName { get; set; }

        /// <summary>
        /// Gets or sets the operation mode.
        /// </summary>
        public OperationMode OperationMode { get; set; }

        /// <summary>
        /// Gets or sets the file output folder.
        /// </summary>
        public string FileOutputFolder { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether populate database.
        /// </summary>
        public bool PopulateDatabase { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether purge.
        /// </summary>
        public bool Purge { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether create xml.
        /// </summary>
        public bool CreateXml { get; set; }

        /// <summary>
        /// Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        /// Gets or sets the federation id.
        /// </summary>
        public int FederationId { get; set; }

        /// <summary>
        /// Gets or sets the discipline id.
        /// </summary>
        public int DisciplineId { get; set; }

        /// <summary>
        /// The setup schedule type.
        /// </summary>
        /// <param name="nodeList">
        /// The node list.
        /// </param>
        /// <param name="results">
        /// The results.
        /// </param>
        /// <param name="scheduleMessage">
        /// The schedule message.
        /// </param>
        /// <param name="duration">
        /// The duration.
        /// </param>
        /// <param name="niFweeklyTrigger">
        /// The ni fweekly trigger.
        /// </param>
        /// <param name="nifWeeklyJobDetails">
        /// The nif weekly job details.
        /// </param>
        /// <param name="scheduler">
        /// The scheduler.
        /// </param>
        /// <exception cref="Exception">
        /// throws exception on error
        /// </exception>
        public void SetupScheduleType(XmlNodeList nodeList, bool results, bool scheduleMessage, int duration, ITrigger niFweeklyTrigger, IJobDetail nifWeeklyJobDetails, IScheduler scheduler)
        {
            if (nodeList != null)
            {
                foreach (XmlNode node in nodeList)
                {
                    if (node.Attributes == null)
                    {
                        throw new Exception("Node attributes cannot be null");
                    }

                    var scheduleId = GetScheduleId(node);

                    // Splitting the time into two values so that we can create a cron job
                    var scheduleTimeArray = node.Attributes["Time"].Value.Split(new[] { ':' });

                    var minutes = GetScheduleMinutes(scheduleTimeArray);

                    var hours = GetScheduleHours(scheduleTimeArray);

                    results = GetScheduleResults(node, results, ref scheduleMessage);

                    duration = GetScheduleDuration(node, duration);

                    var scheduleWeek = GetScheduleWeek(node);

                    var scheduleDay = GetScheduleDay(node);

                    var dto = GetDayOfWeek(scheduleDay, hours, minutes);

                    // Creating a simple Scheduler
                    var calendarIntervalSchedule = CalendarIntervalScheduleBuilder.Create();

                    // Creating the weekly Trigger using the stuff that we've calculated
                    niFweeklyTrigger = TriggerBuilder.Create()
                        .WithDescription(this.InstanceName)
                        .WithIdentity(this.InstanceName + "_" + scheduleId, "GroupNIF")
                        .StartAt(dto)
                        .WithSchedule(calendarIntervalSchedule.WithIntervalInWeeks(Convert.ToInt32(scheduleWeek)))
                        .Build();

                    // This part might be moved
                    switch (this.OperationMode)
                    {
                        case OperationMode.Gatherer:
                            nifWeeklyJobDetails = JobBuilder.Create<NifTeamSportJobBuilder>()
                                .WithIdentity("job_" + this.InstanceName + "_" + scheduleId, "GroupNIF")
                                .WithDescription(this.InstanceName)
                                .Build();
                            break;
                        case OperationMode.Purge:
                            nifWeeklyJobDetails = JobBuilder.Create<PurgeSportsData>()
                                .WithIdentity("job_" + this.InstanceName + "_" + scheduleId, "GroupNIF")
                                .WithDescription(this.InstanceName)
                                .Build();
                            break;
                        case OperationMode.Distributor:
                            nifWeeklyJobDetails = JobBuilder.Create<NifTeamSportJobBuilder>()
                                .WithIdentity("job_" + this.InstanceName + "_" + scheduleId, "GroupNIF")
                                .WithDescription(this.InstanceName)
                                .Build();
                            break;
                        case OperationMode.Hold:
                            break;
                    }

                    if (this.FileOutputFolder != null || this.FileOutputFolder != string.Empty)
                    {
                        nifWeeklyJobDetails.JobDataMap["OutputFolder"] = this.FileOutputFolder;
                    }

                    nifWeeklyJobDetails.JobDataMap["InsertDatabase"] = this.PopulateDatabase;
                    nifWeeklyJobDetails.JobDataMap["ScheduleType"] = this.ScheduleType.ToString();
                    nifWeeklyJobDetails.JobDataMap["Results"] = results;
                    nifWeeklyJobDetails.JobDataMap["ScheduleMessage"] = scheduleMessage;
                    nifWeeklyJobDetails.JobDataMap["Duration"] = duration;
                    nifWeeklyJobDetails.JobDataMap["Purge"] = this.Purge;
                    nifWeeklyJobDetails.JobDataMap["CreateXML"] = this.CreateXml;
                    nifWeeklyJobDetails.JobDataMap["SportId"] = this.SportId;
                    nifWeeklyJobDetails.JobDataMap["FederationId"] = this.FederationId;
                    nifWeeklyJobDetails.JobDataMap["DisciplineId"] = this.DisciplineId;
                    nifWeeklyJobDetails.JobDataMap["OperationMode"] = this.OperationMode.ToString();
                }
            }

            if (nifWeeklyJobDetails == null)
            {
                return;
            }

            Logger.Debug("Setting up and starting weeklyJobDetail job " + nifWeeklyJobDetails.Description + " using trigger : " + niFweeklyTrigger.Description);
            scheduler.ScheduleJob(nifWeeklyJobDetails, niFweeklyTrigger);
        }
    }
}