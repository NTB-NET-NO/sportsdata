﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsData.Components.Nif.Configrations
{
    using System.Xml;

    using log4net;

    using NTB.SportsData.Classes.Enums;
    using NTB.SportsData.Components.Nif.Interfaces;

    public class PollStyleConfigurator
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(PollStyleConfigurator));

        /// <summary>
        /// The setup poll style.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        /// <exception cref="ArgumentOutOfRangeException">
        /// </exception>
        /// <exception cref="Exception">
        /// </exception>
        /// <exception cref="NotSupportedException">
        /// </exception>
        /// <exception cref="ArgumentException">
        /// </exception>
        private void SetupPollStyle(XmlNode configNode)
        {
            try
            {
                APollStyleFactory pollStyleFactory;
                // Switch on pollstyle
                switch (this.PollStyle)
                {
                    case PollStyle.Continous:

                        pollStyleFactory.SetupPollStyle(configNode);
                        // pollStyleFactorythis.SetupContinousPollStyle(configNode);

                        break;

                    // Code for Pubnub push technology
                    // CONTINUE HERE TOMORROW! (Is this done, what am I to do here tomorrow) (2012-03-09)
                    case PollStyle.PushSubscribe:

                        break;

                    case PollStyle.CalendarPoll:

                        // Getting the times for this job
                        this.SetupCalendarPollStyle(configNode);

                        break;

                    case PollStyle.Scheduled:

                        /* 
                            schedule = configNode.Attributes["Schedule"].Value;
                            TimeSpan s = Utilities.GetScheduleInterval(schedule);
                            logger.DebugFormat("Schedule: {0} Calculated time to next run: {1}", schedule, s);
                            pollTimer.Interval = s.TotalMilliseconds;
                            */
                        throw new NotSupportedException("Invalid polling style for this job type");

                    case PollStyle.FileSystemWatch:

                        // Check for config overrides
                        /*
                            if (configNode.Attributes.GetNamedItem("BufferSize") != null)
                                bufferSize = Convert.ToInt32(configNode.Attributes["BufferSize"].Value);
                            logger.DebugFormat("FileSystemWatcher buffer size: {0}", bufferSize);

                            //Set values
                            filesWatcher.Path = fileInputFolder;
                            filesWatcher.Filter = fileFilter;
                            filesWatcher.IncludeSubdirectories = includeSubdirs;
                            filesWatcher.InternalBufferSize = bufferSize;

                            //Do not start the event watcher here, wait until after the startup cleaning job
                            pollTimer.Interval = 5000; // short startup;
                            */
                        throw new NotSupportedException("Invalid polling style for this job type");

                    default:

                        // Unsupported pollstyle for this object
                        throw new NotSupportedException("Invalid polling style for this job type (" + this.PollStyle + ")");
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing PollStyle-specific values in XML configuration node: " + ex.Message, ex);
            }
        }

        public PollStyle PollStyle { get; set; }

        /// <summary>
        /// The setup calendar poll style.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        /// <exception cref="ArgumentOutOfRangeException">
        /// </exception>
        /// <exception cref="Exception">
        /// </exception>
        private void SetupCalendarPollStyle(XmlNode configNode)
        {
            
        }

        /// <summary>
        /// The setup continous poll style.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        private void SetupContinousPollStyle(XmlNode configNode)
        {
            if (configNode.Attributes != null)
            {
                this.interval = Convert.ToInt32(configNode.Attributes["Interval"].Value);
            }

            var folderNodeList = configNode.SelectNodes("../NIFTeamComponent[@Name='" + this.InstanceName + "']/Folders/Folder");

            var folderJobs = 0;
            if (folderNodeList != null)
            {
                folderJobs = folderNodeList.Count;
            }

            // <Folder Type="XMLOutputFolder" Mode="FILE" Results="True" PushSchedule="False">C:\Utvikling\norm\xml\NTBSportsData</Folder>
            // string OutputFolder = "";
            Logger.Debug("Number of Folders: " + folderJobs);
            if (folderNodeList != null)
            {
                foreach (XmlNode foldernode in folderNodeList)
                {
                    if (foldernode.Attributes != null && foldernode.Attributes["Result"] != null)
                    {
                        this.pushResults = Convert.ToBoolean(foldernode.Attributes["Result"].Value);
                    }

                    if (foldernode.Attributes != null && foldernode.Attributes["PushSchedule"] != null)
                    {
                        this.pushSchedule = Convert.ToBoolean(foldernode.Attributes["PushSchedule"].Value);
                    }

                    this.fileOutputFolder = foldernode.InnerText;
                }
            }

            if (this.interval == 0)
            {
                this.interval = 5000;
            }

            this.interval = this.interval * 60 * 1000; // Adding so that it is one minute
            this.pollTimer.Interval = this.interval; // short startup - interval * 1000;
            Logger.DebugFormat("Poll interval: {0} seconds", this.interval);
        }
    }
}
