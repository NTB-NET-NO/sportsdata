﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using log4net;
using log4net.DateFormatter;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Maintenance.Extension;
using NTB.SportsData.Components.Nif.Models;

namespace NTB.SportsData.Components.Nif.Controller
{
    public partial class TeamResultController
    {
        /// <summary>
        ///     Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(TeamResultController));

        public void GetTodaysMatches(DataFileParams dataFileParams)
        {
            // The start value is last time we ran this command
            var dateStart = dataFileParams.DateStart;
            var dateEnd = dataFileParams.DateEnd;

            // Checking if we are in testing mode
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
            {
                dateStart = DateTime.Parse(ConfigurationManager.AppSettings["niftestingdate"])
                    .AddHours(DateTime.Now.Hour)
                    .AddMinutes(DateTime.Now.Minute);

                dateEnd = DateTime.Parse(ConfigurationManager.AppSettings["niftestingdate"])
                    .AddHours(DateTime.Now.Hour)
                    .AddMinutes(DateTime.Now.Minute);

                Logger.DebugFormat("Testing mode - DateStart: {0}, DateEnd: {1}", dateStart, dateEnd);
            }

            // Create tourmanent model
            var model = new TournamentModel();

            model.DataParams = dataFileParams;

            // Send value to view
            var changes = model.GetLatestChangesForTeamResults();
            var matches = model.GetMatchesByResult(changes);
            var tournaments = model.GetTournamentsAndMatches(matches);

            
        }

        public void GetMaintenanceMatchResult(DataFileParams dataFileParams, List<Tournament> tournaments, List<Customer> customers)
        {
            /*
             * @todo: Remember that the start date and end date must be taken from the XML File
             * @todo: Continue to work on the maintenance work
             * 
             */
            // The start value is last time we ran this command
            var dateStart = dataFileParams.DateStart;
            var dateEnd = dataFileParams.DateEnd;

            Logger.DebugFormat("DateStart: {0}, DateEnd: {1}", dateStart, dateEnd);

            // Create tourmanent model


            // We must loop over date from start to end. 
            var dateTimeExtension = new DateTimeExtension();

            // xmlRoot.SetAttribute("Type", _nffDatafileCreator.RenderResult ? "resultat" : "terminliste");
            foreach (var tournament in tournaments)
            {

                foreach (var day in dateTimeExtension.EachDay(dateStart, dateEnd))
                {
                    Logger.DebugFormat("Parsing for tournament: {0} ({1})", tournament.Name, tournament.Id);
                    dataFileParams.DateStart = day;
                    dataFileParams.DateEnd = day;
                    Logger.DebugFormat("Parsing for date: dateStart {0}, dateEnd {1}", dataFileParams.DateStart.ToShortDateString(), dataFileParams.DateEnd.ToShortDateString());

                    var model = new TournamentModel
                    {
                        DataParams = dataFileParams
                    };
                    model.Sport = model.GetSport(dataFileParams.FederationId);
                    model.Federation = model.GetFederationByOrgId(dataFileParams.FederationId);

                    model.Tournament = tournament;
                    model.CreateTournamentModelFromMaintenance(tournament);

                    if (model.Matches == null)
                    {
                        continue;
                    }

                    if (model.Tournament.PublishResult)
                    {
                        dataFileParams.Results = true;
                    }

                    if (model.TournamentStanding != null)
                    {
                        model.Tournament.PublishTournamentTable = true;
                    }

                    var matchFacts = this.CreateMatchFacts(model);

                    var documentModel = new DocumentModel { DataFileParams = dataFileParams };
                    documentModel.HasMatchFatcts = matchFacts;

                    documentModel.Customers = customers;

                    model.GetEventType(tournament.Id);

                    //if (documentModel.Customers.Count == 0)
                    //{
                    //    continue;
                    //}

                    // todo: Fix this so we have version at the end of the file
                    // Todo: Fix so it sets fakta, restab and res depending on the information
                    var version = documentModel.GetDocumentVersionByTournamentId(tournament.Id);
                    documentModel.CreateDocument(model.Tournament, day, version);
                    documentModel.StoreDocumentInDatabase(version);

                    foreach (var documentType in documentModel.Document.DocumentTypes)
                    {
                        documentModel.Document.DocumentType = documentType;
                        var teamSportsDataViewModel = new TeamSportsDataViewModel();
                        teamSportsDataViewModel.Tournament = model.Tournament;
                        teamSportsDataViewModel.Customers = documentModel.Customers;
                        teamSportsDataViewModel.Matches = model.Matches;
                        teamSportsDataViewModel.AgeCategory = model.AgeCategory;
                        teamSportsDataViewModel.Sport = model.Sport;
                        teamSportsDataViewModel.Federation = model.Federation;
                        teamSportsDataViewModel.DocumentType = documentType;
                        teamSportsDataViewModel.Document = documentModel.Document;
                        teamSportsDataViewModel.TournamentStandingList = model.TournamentStanding;
                        teamSportsDataViewModel.EventType = model.EventType;
                        teamSportsDataViewModel.SportsData = teamSportsDataViewModel.PopulateSportsData();

                        foreach (var outputFolder in dataFileParams.FileOutputFolders.Where(x => x.SportId == dataFileParams.SportId))
                        {
                            var filename = teamSportsDataViewModel.SportsData.Tournament.Header.File.FileName.FullName; // documentModel.Document.Filename + ".xml";
                            this.WriteXmlDocument(teamSportsDataViewModel, outputFolder.OutputPath, filename);
                        }
                    }

                }
            }
        }

        public bool CreateMatchFacts(TournamentModel model)
        {
            try
            {
                var filteredMatches = model.Matches.Select(x => x.MatchIncidents != null && x.MatchIncidents.Count > 0);
                var matchFacts = filteredMatches.Any();

                return matchFacts;
            }
            catch (Exception exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Produces results based on the Maintenance file
        /// </summary>
        /// <param name="dataFileParams">
        ///     the data File Params
        /// </param>
        /// <param name="tournaments">
        ///     The List of tournaments
        /// </param>
        /// <param name="customerId">
        ///     The customer id
        /// </param>
        /// <param name="remoteCustomerNumber">
        ///     The customer number remote
        /// </param>
        public void GetMaintenanceMatchResult(DataFileParams dataFileParams, List<Tournament> tournaments, int customerId, int remoteCustomerNumber)
        {
            /*
             * @todo: Remember that the start date and end date must be taken from the XML File
             * @todo: Continue to work on the maintenance work
             * 
             */
            // The start value is last time we ran this command
            var dateStart = dataFileParams.DateStart;
            var dateEnd = dataFileParams.DateEnd;

            Logger.DebugFormat("DateStart: {0}, DateEnd: {1}", dateStart, dateEnd);

            // Create tourmanent model
            

            // We must loop over date from start to end. 
            var dateTimeExtension = new DateTimeExtension();

            // xmlRoot.SetAttribute("Type", _nffDatafileCreator.RenderResult ? "resultat" : "terminliste");
            foreach (var tournament in tournaments)
            {
                
                foreach (var day in dateTimeExtension.EachDay(dateStart, dateEnd))
                {
                    Logger.DebugFormat("Parsing for tournament: {0} ({1})", tournament.Name, tournament.Id);
                    dataFileParams.DateStart = day;
                    dataFileParams.DateEnd = day;
                    Logger.DebugFormat("Parsing for date: dateStart {0}, dateEnd {1}", dataFileParams.DateStart.ToShortDateString(), dataFileParams.DateEnd.ToShortDateString());

                    var model = new TournamentModel
                    {
                        DataParams = dataFileParams
                    };
                    model.Sport = model.GetSport(dataFileParams.FederationId);
                    model.Federation = model.GetFederationByOrgId(dataFileParams.FederationId);

                    model.Tournament = tournament;
                    model.CreateTournamentModelFromMaintenance(tournament);

                    if (model.Matches == null)
                    {
                        continue;
                    }
                    
                    if (model.Tournament.PublishResult)
                    {
                        dataFileParams.Results = true;
                    }

                    
                    var matchFacts = this.HasMatchFacts(model);

                    var documentModel = new DocumentModel { DataFileParams = dataFileParams };
                    documentModel.HasMatchFatcts = matchFacts;

                    // todo: Fix this since we already have the customer or customers.
                    documentModel.Customers = documentModel.GetListOfCustomers(tournament);
                    if (documentModel.Customers.Count == 0)
                    {
                        continue;
                    }

                    // todo: Fix this so we have version at the end of the file
                    // Todo: Fix so it sets fakta, restab and res depending on the information
                    

                    var currentDay = day;

                    var version = documentModel.GetDocumentVersionByTournamentId(tournament.Id);
                    documentModel.CreateDocument(tournament, currentDay, version);

                    documentModel.StoreDocumentInDatabase(version);

                    foreach (var documentType in documentModel.Document.DocumentTypes)
                    {
                        var matchesForTheDay = model.Matches.Where(x => x.Match.MatchDate == currentDay).ToList();
                        if (!matchesForTheDay.Any())
                        {
                            // If we don't get any matches for this specific day, we just continue
                            continue;
                        }
                        var teamSportsDataViewModel = new TeamSportsDataViewModel();
                        
                        teamSportsDataViewModel.Tournament = model.Tournament;
                        
                        teamSportsDataViewModel.Customers = documentModel.Customers;

                        documentModel.Document.DocumentType = documentType;

                        // making sure we are only putting the list of matches in the document
                        teamSportsDataViewModel.Matches = matchesForTheDay;
                        teamSportsDataViewModel.AgeCategory = model.AgeCategory;
                        teamSportsDataViewModel.Sport = model.Sport;
                        teamSportsDataViewModel.Federation = model.Federation;
                        teamSportsDataViewModel.DocumentType = documentType;
                        teamSportsDataViewModel.Document = documentModel.Document;
                        teamSportsDataViewModel.TournamentStandingList = model.TournamentStanding;
                        teamSportsDataViewModel.SportsData = teamSportsDataViewModel.PopulateSportsData();

                        foreach (var outputFolder in dataFileParams.FileOutputFolders.Where(x => x.SportId == dataFileParams.SportId))
                        {
                            var filename = teamSportsDataViewModel.SportsData.Tournament.Header.File.FileName.FullName; // documentModel.Document.Filename + ".xml";
                            this.WriteXmlDocument(teamSportsDataViewModel, outputFolder.OutputPath, filename);
                        }
                    }
                    
                }
            }
        }

        public bool HasMatchFacts(TournamentModel model)
        {
            var filteredMatches = model.Matches.Select(x => x.MatchIncidents.Count > 0);
            var matchFacts = filteredMatches.Any();
            return matchFacts;
        }

        public void GetMatchResultsFromCalendarJob(DataFileParams dataFileParams)
        {
            var dateStart = dataFileParams.DateStart;
            var dateEnd = dataFileParams.DateEnd;

            Logger.DebugFormat("DateStart: {0}, DateEnd: {1}", dateStart, dateEnd);

            dataFileParams.DateStart = dateStart;
            dataFileParams.DateEnd = dateEnd;

            // Create tourmanent model
            var model = new TournamentModel
            {
                DataParams = dataFileParams
            };
            model.Sport = model.GetSport(dataFileParams.FederationId);
            model.Federation = model.GetFederationByOrgId(dataFileParams.FederationId);

            // Get the latest changes
            var changes = model.GetLatestChangesForTeamResults();
            if (changes == null)
            {
                this.StoreLatestDownloadDateTime(dataFileParams, this.CurrentDateTime);
                Logger.Info("No changes");
                return;
            }

            /*
             * The reason for not using tournament Ids is because we might get results from earlier days
             * This of course means (or could mean) we are producing more files than really needed.
             * Since we don't know if the date is different, we cannot really filter.
             */
            // var tournamentIds = model.GetTournamentIdsByResult(changes);

            // Getting the matches
            var matches = model.GetMatchesByResult(changes);

            // Call process matches and do the processing.
            this.ProcessMatches(matches, dataFileParams, model, null);
        }

        public void ProcessMatches(List<MatchExtended> matches, DataFileParams dataFileParams, TournamentModel model, double? interval)
        {
            Logger.Info("================================================================");
            Logger.Info("Start processing all matches based on registered results at NIF.");
            Logger.Info("================================================================");
            var groupedMatches = matches.GroupBy(x => x.Match.MatchDate).ToList();

            foreach (var currentGroupMatches in groupedMatches)
            {
                if (currentGroupMatches.Key == null)
                {
                    Logger.Debug("For some reason the key is null");
                    // continue;
                }

                try
                {
                    if (currentGroupMatches.Key != null)
                    {
                        Logger.DebugFormat("DateTime for this group is {0}", currentGroupMatches.Key.Value);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);

                    // Continuing the loop. I don't think there is much we can do in the coming loop.
                    // continue;
                }

                foreach (var match in currentGroupMatches)
                {
                    try
                    {
                        
                        if (match.Match.MatchDate != null)
                        {
                            dataFileParams.DateStart = match.Match.MatchDate.Value;
                            dataFileParams.DateEnd = match.Match.MatchDate.Value;
                        }

                        var tournamentId = match.Match.TournamentId;
                        Logger.InfoFormat("Getting the tournament based on the tournament id {0} in the match object", tournamentId);

                        
                        if (tournamentId == 0)
                        {
                            var tempTournamentId = model.GetTournamentByMatchId(match.Match.Id);
                            tournamentId = tempTournamentId.Id;
                        }

                        // todo: this will be removed when we get the tournament Object into the MatchExtended Object
                        var tournament = model.GetTournamentByTournamentId(tournamentId);

                        // If the discipline-id is different, then we just continue. No need to continue with the loop for this match
                        if (tournament.DisciplineId != dataFileParams.DisciplineId)
                        {
                            Logger.InfoFormat("Tournament {0} (disciplineId: {1}) is not set for this job. Will go to next match.", tournament.Name, tournament.DisciplineId);
                            continue;
                        }

                        if (tournament.PublishResult == false)
                        {
                            Logger.InfoFormat("Tournament {0} (id: {1}) is set not to be published. We will continue", tournament.Name, tournament.Id);
                            continue;
                        }

                        // todo: This method might be changed when we get the GetMatchExtendedByTournament
                        var tournamentMatches = model.GetMatchesByTournamentId(tournamentId, dataFileParams.DateStart);
                        if (tournamentMatches == null)
                        {
                            Logger.InfoFormat("No matches for tournament {0} (id: {1}). We will continue", tournament.Name, tournament.Id);
                            continue;
                        }

                        model.Tournament = tournament;
                        if (interval != null)
                        {
                            model.CreateTournamentModelFromSync(interval.Value, tournament, tournamentMatches);
                        }
                        else
                        {
                            model.CreateTournamentModelFromCalendar(tournament, tournamentMatches);
                        }

                        if (model.Matches == null)
                        {
                            continue;
                        }

                        model.GetEventType(tournamentId);

                        var documentModel = new DocumentModel { DataFileParams = dataFileParams };
                        documentModel.Customers = documentModel.GetListOfCustomers(tournament);

                        if (documentModel.Customers.Count == 0)
                        {
                            Logger.InfoFormat("No customers are subscribing to tournament {0} ({1})", tournament.Name, tournament.Id);
                            continue;
                        }

                        if (model.Tournament.PublishResult)
                        {
                            dataFileParams.Results = true;
                        }

                        if (model.TournamentStanding != null)
                        {
                            model.Tournament.PublishTournamentTable = true;
                        }

                        // documentModel.HasMatchFatcts = matchFacts;
                        var version = documentModel.GetDocumentVersionByTournamentId(tournament.Id);
                        documentModel.CreateDocument(tournament, dataFileParams.DateStart, version);

                        documentModel.StoreDocumentInDatabase(version);

                        foreach (var documentType in documentModel.Document.DocumentTypes)
                        {
                            // Now the model has been populated. 
                            // Let's call the view with the data
                            var teamSportsDataViewModel = new TeamSportsDataViewModel();
                            teamSportsDataViewModel.DocumentType = documentType;
                            documentModel.Document.DocumentType = documentType;
                            teamSportsDataViewModel.Tournament = model.Tournament;
                            teamSportsDataViewModel.EventType = model.EventType;
                            teamSportsDataViewModel.Customers = documentModel.Customers;
                            teamSportsDataViewModel.Matches = model.Matches;
                            teamSportsDataViewModel.AgeCategory = model.AgeCategory;
                            teamSportsDataViewModel.Sport = model.Sport;
                            teamSportsDataViewModel.Federation = model.Federation;
                            teamSportsDataViewModel.Document = documentModel.Document;
                            teamSportsDataViewModel.TournamentStandingList = model.TournamentStanding;
                            teamSportsDataViewModel.SportsData = teamSportsDataViewModel.PopulateSportsData();

                            foreach (var outputFolder in dataFileParams.FileOutputFolders)
                            {
                                var filename = teamSportsDataViewModel.SportsData.Tournament.Header.File.FileName.FullName;
                                this.WriteXmlDocument(teamSportsDataViewModel, outputFolder.OutputPath, filename);
                            }
                        }
                    }
                    catch (Exception exception)
                    {
                        Logger.Error(exception);
                    }
                }
            }
            Logger.Info("=========================================================");
            Logger.Info("Processed all matches based on registered results at NIF.");
            Logger.Info("=========================================================");
            this.StoreLatestDownloadDateTime(dataFileParams, this.CurrentDateTime);
        }

        public void GetChangesMatchResult(DataFileParams dataFileParams, double interval)
        {
            // The start value is last time we ran this command
            var dateStart = this.GetLatestDownloadDateTime(dataFileParams, interval);
            var dateEnd = DateTime.Now;

            Logger.DebugFormat("DateStart: {0}, DateEnd: {1}", dateStart, dateEnd);

            // Checking if we are in testing mode
            dateStart = this.SetGathererDateTime(interval, dateStart, ref dateEnd);

            dataFileParams.DateStart = dateStart;
            dataFileParams.DateEnd = dateEnd;

            // Create tourmanent model
            var model = new TournamentModel
            {
                DataParams = dataFileParams
            };
            model.Sport = model.GetSport(dataFileParams.FederationId);
            model.Federation = model.GetFederationByOrgId(dataFileParams.FederationId);
            
            // Get the latest changes
            var changes = model.GetLatestChangesForTeamResults();
            if (changes == null)
            {
                this.StoreLatestDownloadDateTime(dataFileParams, this.CurrentDateTime);
                Logger.Info("No changes");
                return;
            }

            /*
             * The reason for not using tournament Ids is because we might get results from earlier days
             * This of course means (or could mean) we are producing more files than really needed.
             * Since we don't know if the date is different, we cannot really filter.
             */
            // var tournamentIds = model.GetTournamentIdsByResult(changes);

            // Getting the matches
            var matches = model.GetMatchesByResult(changes);

            // Looping over matches 
            this.ProcessMatches(matches, dataFileParams, model, interval);
        }

        public void WriteXmlDocument(TeamSportsDataViewModel model, string exportPath, string fileName)
        {
            var stringWriter = new StringWriter();
            using (var xmlWriter = new XmlTextWriter(stringWriter))
            {
                var xmlSerializer = new XmlSerializer(typeof(Classes.Classes.SportsDataOutput.SportsData));
                xmlWriter.Formatting = Formatting.Indented;
                try
                {
                    xmlSerializer.Serialize(xmlWriter, model.SportsData);
                    // xmlSerializer.Serialize(xmlWriter, new Classes.Classes.SportsDataOutput.SportsData());

                    var di = new DirectoryInfo(exportPath);
                    if (!di.Exists)
                    {
                        di.Create();
                    }

                    var xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(stringWriter.ToString());

                    var fullFileName = Path.Combine(exportPath, fileName);
                    using (TextWriter textWriter = new StreamWriter(fullFileName, false, Encoding.UTF8))
                    {
                        Logger.InfoFormat("Saving XML-document: {0}", fileName);
                        xmlDoc.Save(textWriter);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException != null)
                    {
                        Logger.Error(exception.InnerException.Message);
                        Logger.Error(exception.InnerException.StackTrace);
                    }
                }
            }
        }
        public DateTime SetGathererDateTime(double interval, DateTime dateStart, ref DateTime dateEnd)
        {
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
            {
                if (DateTime.Now.Hour < 19)
                {
                    dateStart = DateTime.Parse(ConfigurationManager.AppSettings["niftestingdate"])
                        .AddHours(DateTime.Now.Hour)
                        .AddMinutes(DateTime.Now.Minute)
                        .AddMilliseconds(-interval);

                    dateEnd = DateTime.Parse(ConfigurationManager.AppSettings["niftestingdate"])
                        .AddHours(19)
                        .AddMinutes(59);
                }
                else
                {
                    dateStart = DateTime.Parse(ConfigurationManager.AppSettings["niftestingdate"])
                        .AddHours(19)
                        .AddMinutes(0);


                    dateEnd = DateTime.Parse(ConfigurationManager.AppSettings["niftestingdate"])
                        .AddHours(DateTime.Now.Hour)
                        .AddMinutes(DateTime.Now.Minute);
                }


                
            }
            
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["synctesting"]))
            {
                dateStart = DateTime.Parse(ConfigurationManager.AppSettings["nifsyncteststartdatetime"]);
                dateEnd = DateTime.Parse(ConfigurationManager.AppSettings["nifsynctestenddatetime"]);
            }

            Logger.DebugFormat("Testing mode - DateStart: {0}, DateEnd: {1}", dateStart, dateEnd);


            return dateStart;
        }
    }
}
