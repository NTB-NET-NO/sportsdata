﻿using System.Collections.Generic;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nif.Controller;
using NTB.SportsData.Components.Nif.Facade;
using NTB.SportsData.Components.PublicRepositories;

namespace NTB.SportsData.Components.Nif.Creators
{
    public class MaintenanceResultsCreator
    {
        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(MaintenanceResultsCreator));

        /// <summary>
        ///     Gets or sets the data params.
        /// </summary>
        public DataFileParams DataParams { get; set; }

        /// <summary>
        /// The create maintenance results.
        /// </summary>
        /// <param name="tournaments">
        /// The tournaments.
        /// </param>
        public List<Tournament> Create(List<Tournament> tournaments)
        {
            if (tournaments == null)
            {
                return null;
            }

            // We need to add more information to the tournament object
            var tournamentRepository = new TournamentRepository();

            var listOfTournaments = new List<Tournament>();
            foreach (var tournament in tournaments)
            {
                var currentTournament = tournamentRepository.Get(tournament.Id);
                if (currentTournament.Id == 0)
                {
                    // We must get the information from remote
                    var facade = new TeamSportFacade();
                    currentTournament = facade.GetTournamentById(tournament.Id);
                }

                if (currentTournament != null)
                {
                    listOfTournaments.Add(currentTournament);
                }
            }

            this.DataParams.Maintenance = true;

            // this.CreateMatchResults(listOfTournaments);

            return listOfTournaments;
        }


        public void CreateCustomerDataFile(List<Customer> customers, int remoteCustomerNumber, List<Tournament> tournaments)
        {
            
            var controller = new TeamResultController();
            
            controller.GetMaintenanceMatchResult(this.DataParams, tournaments, customers);
        }
    }
}
