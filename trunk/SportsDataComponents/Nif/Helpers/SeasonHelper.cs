﻿using System;
using System.Collections.Generic;
using System.Linq;
using NTB.SportsData.Classes.Classes;

namespace NTB.SportsData.Components.Nif.Helpers
{
    public class SeasonHelper
    {
        public int DisciplineId { get; set; }
        public Season GetActiveSeasonWithDisciplineId(List<Season> seasons)
        {
            if (seasons.Count == 0)
            {
                return new Season();
            }

            seasons = this.CheckDates(seasons);

            var filteredSeason = (from f in seasons where f.Active && f.DisciplineId == this.DisciplineId select f).ToList();

            if (filteredSeason.Count() == 1)
            {
                return filteredSeason.Single();
            }

            if (this.DisciplineId > 0)
            {
                filteredSeason = (from f in seasons
                    where (DateTime.Today.Date >= f.StartDate.Date && DateTime.Today.Date <= f.EndDate.Date)
                          && f.Active && f.DisciplineId == this.DisciplineId
                    select f).ToList();
            }
            else
            {
                filteredSeason = (from f in seasons
                                  where (DateTime.Today.Date >= f.StartDate.Date && DateTime.Today.Date <= f.EndDate.Date)
                                  && f.Active 
                                  select f).ToList();
            }
            

            //filteredSeason = (from f in filteredSeason
            //    where f.Active && f.DisciplineId == this.DisciplineId
            //    select f).ToList();

            if (filteredSeason.Count == 0)
            {
                throw new SportsDataException("No active season set for this sport. Please activate a season in Sports Data Admin");
            }

            return filteredSeason.Single();
        }

        public List<Season> CheckDates(List<Season> seasons)
        {
            foreach (var season in seasons)
            {
                if (season.StartDate <= season.EndDate)
                {
                    continue;
                }

                var tempStartDate = season.StartDate;
                var tempEndDate = season.EndDate;
                season.StartDate = tempEndDate;
                season.EndDate = tempStartDate;
            }

            return seasons;
        }
        public Season GetActiveSeason(List<Season> seasons)
        {
            if (seasons.Count == 0)
            {
                return new Season();
            }

            seasons = this.CheckDates(seasons);

            var filteredSeason = (from f in seasons where f.Active select f).ToList();

            if (filteredSeason.Count > 0)
            {
                filteredSeason = (from f in seasons
                                  where (DateTime.Today.Date >= f.StartDate.Date && DateTime.Today.Date <= f.EndDate.Date) &&
                                  f.Active
                                  select f).ToList();
            }
            
            if (filteredSeason.Count == 1)
            {
                return filteredSeason.Single();
            }

            // This is a bad solution, but it "works"
            return filteredSeason[0];
        }
    }
}
