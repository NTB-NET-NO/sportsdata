﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SingleSportFacade.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The single sport facade.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.Nif.Facade
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using log4net;

    using NTB.SportsData.Classes.Classes;
    using NTB.SportsData.Components.Nif.Mappers;
    using NTB.SportsData.Components.NIFConnectService;
    using NTB.SportsData.Components.RemoteRepositories.DataMappers;
    using NTB.SportsData.Components.RemoteRepositories.Interfaces;

    using EventClass = NTB.SportsData.Classes.Classes.EventClass;

    /// <summary>
    /// The single sport facade.
    /// </summary>
    public class SingleSportFacade : ISingleSportFacade
    {
        #region Fields

        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SingleSportFacade));

        /// <summary>
        /// The _event result data mapper.
        /// </summary>
        private readonly IEventResultRemoteDataMapper eventResultDataMapper;

        /// <summary>
        /// The _sport event data mapper.
        /// </summary>
        private readonly IEventRemoteDataMapper sportEventDataMapper;

        /// <summary>
        ///     The sync remote data mapper
        /// </summary>
        private readonly ISynchRemoteRepository syncRemoteDataMapper;

        /// <summary>
        ///     The organization data mapper
        /// </summary>
        private readonly IOrganizationDataMapper orgDataMapper;

        /// <summary>
        ///     The remote organization data mapper
        /// </summary>
        private readonly IOrganizationRemoteDataMapper orgRemoteDataMapper;

        /// <summary>
        /// The class exercise remote data mapper.
        /// </summary>
        private readonly IClassExerciseRemoteDataMapper classExerciseRemoteDataMapper;

        /// <summary>
        /// The person remote data mapper.
        /// </summary>
        private readonly IPersonRemoteDataMapper personRemoteDataMapper;

        /// <summary>
        /// The venue remote data mapper
        /// </summary>
        private readonly IVenueRemoteDataMapper venueRemoteDataMapper;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SingleSportFacade"/> class.
        /// </summary>
        public SingleSportFacade()
        {
            this.sportEventDataMapper = new EventRemoteRepository();
            this.eventResultDataMapper = new EventResultRemoteRepository();
            this.syncRemoteDataMapper = new SynchRemoteRepository();
            this.classExerciseRemoteDataMapper = new ClassExerciseRemoteRepository();
            this.personRemoteDataMapper = new PersonRemoteRepository();
            this.orgRemoteDataMapper = new OrgRemoteRepository();
            this.venueRemoteDataMapper = new VenueRemoteRepository();
        }

        #endregion

        /// <summary>
        /// The get event info.
        /// </summary>
        /// <param name="eventId">
        /// The event id.
        /// </param>
        /// <returns>
        /// The <see cref="SportEvent"/>.
        /// </returns>
        public SportEvent GetEventInfo(int eventId)
        {
            try
            {
                var result = this.sportEventDataMapper.GetEventById(eventId);

                var mapper = new EventMapper();

                var remoteEvent = mapper.Map(result, new SportEvent());

                if (result.VenueUnitId == null)
                {
                    return remoteEvent;
                }

                var venueUnitId = Convert.ToInt32(result.VenueUnitId);

                var venueResult = this.venueRemoteDataMapper.GetVenueUnitById(venueUnitId);

                remoteEvent.VenueUnitId = venueResult.VenueUnitId;

                remoteEvent.VenueUnitName = venueResult.VenueUnitName;

                return remoteEvent;

            }
            catch (Exception exception)
            {
                Logger.ErrorFormat("An error occured: {0}", exception);
                Logger.Error(exception.StackTrace);

                return new SportEvent();
            }
        }

        /// <summary>
        /// The get sync sport event.
        /// </summary>
        /// <param name="userKey">
        /// The user key.
        /// </param>
        /// <param name="clientPassword">
        /// The client password.
        /// </param>
        /// <param name="eventStartDate">
        /// The event start date.
        /// </param>
        /// <param name="eventEndDate">
        /// The event end date.
        /// </param>
        /// <returns>
        /// The <see cref="SportEvent"/>.
        /// </returns>
        public SportEvent GetSyncSportEvent(string userKey, string clientPassword, DateTime eventStartDate, DateTime eventEndDate)
        {
            this.syncRemoteDataMapper.FederationUserKey = userKey;
            this.syncRemoteDataMapper.ClientPassword = clientPassword;
            this.syncRemoteDataMapper.EventStartDate = eventStartDate;
            this.syncRemoteDataMapper.EventEndDate = eventEndDate;
            var result = this.syncRemoteDataMapper.GetSportsResultsForOrganisation();

            // with the result, we must now go down again and get the event, because now we only have one result line which has been changed
            var eventResult = this.eventResultDataMapper.GetResultByResultId(result.First().Id);

            // With eventResult we shall now get the Event - and then exercise classes for this event
            var sportEvent = this.sportEventDataMapper.GetEventById(eventResult.EventId);

            var mapper = new EventMapper();

            return mapper.Map(sportEvent, new SportEvent());
        }

        /// <summary>
        /// The check sync sport result.
        /// This method checks if we have had any results the last hour. If we do, we return true, otherwise we return false
        /// </summary>
        /// <param name="userKey">
        /// The user key.
        /// </param>
        /// <param name="clientPassword">
        /// The client password.
        /// </param>
        /// <param name="eventStartDate">
        /// The event start date.
        /// </param>
        /// <param name="eventEndDate">
        /// The event end date.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool CheckSyncSportResult(string userKey, string clientPassword, DateTime eventStartDate, DateTime eventEndDate)
        {
            try
            {
                this.syncRemoteDataMapper.FederationUserKey = userKey;
                this.syncRemoteDataMapper.ClientPassword = clientPassword;
                this.syncRemoteDataMapper.EventStartDate = eventStartDate;
                this.syncRemoteDataMapper.EventEndDate = eventEndDate;
                var listOfResults = this.syncRemoteDataMapper.GetSportEventsUpdates();

                if (listOfResults.Any())
                {
                    return true;
                }
                return false;
            }
            catch (Exception exception)
            {
                Logger.ErrorFormat("An exception occured: {0}", exception);
                Logger.Error(exception.StackTrace);

                return false;
            }
        }

        /// <summary>
        /// The get sync sport result.
        /// </summary>
        /// <param name="userKey">
        /// The user key.
        /// </param>
        /// <param name="clientPassword">
        /// The client password.
        /// </param>
        /// <param name="eventStartDate">
        /// The event start date.
        /// </param>
        /// <param name="eventEndDate">
        /// The event end date.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<SportResult> GetSyncSportResult(string userKey, string clientPassword, DateTime eventStartDate, DateTime eventEndDate)
        {
            try
            {
                this.syncRemoteDataMapper.FederationUserKey = userKey;
                this.syncRemoteDataMapper.ClientPassword = clientPassword;
                this.syncRemoteDataMapper.EventStartDate = eventStartDate;
                this.syncRemoteDataMapper.EventEndDate = eventEndDate;
                var listOfResults = this.syncRemoteDataMapper.GetSportsResultsForOrganisation();

                if (!listOfResults.Any())
                {
                    return new List<SportResult>();
                }

                // Now we must loop the result
                var results = new List<SportResult>();

                var resultMapper = new SportResultMapper();
                foreach (var result in listOfResults)
                {
                    // with the result, we must now go down again and get the event, because now we only have one result line which has been changed
                    var eventResult = this.eventResultDataMapper.GetResultByResultId(result.Id);

                    // Now we map the competitor into a sportResult object
                    var sportResult = resultMapper.Map(eventResult, new SportResult());

                    // But we must have more information
                    var person = this.GetPersonById(sportResult.PersonId);

                    var clubInfo = this.orgRemoteDataMapper.GetOrganizationByOrgId(sportResult.ClubId);

                    sportResult.ZipCode = person.HomeAddress.ZipCode;
                    sportResult.City = person.HomeAddress.City;
                    sportResult.RegionId = Convert.ToInt32(clubInfo.LocalCouncilId);
                    sportResult.RegionName = clubInfo.LocalCouncilName;

                    results.Add(sportResult);
                }

                //var orderedResults = results.OrderBy(x => x.ClassExerciseId);

                //// Now we must create a dictionary and order by classExercise
                //var resultDictionary = new Dictionary<int, List<SportResult>>();
                //foreach (var orderedResult in orderedResults)
                //{
                //    if (!resultDictionary.ContainsKey(orderedResult.ClassExerciseId))
                //    {
                //        resultDictionary.Add();
                //    }
                //}

                return results;
            }
            catch (Exception exception)
            {
                Logger.ErrorFormat("An exception occured: {0}", exception);
                Logger.Error(exception.StackTrace);

                return new List<SportResult>();
            }
        }

        /// <summary>
        /// The get sync sport events.
        /// </summary>
        /// <param name="userKey">
        /// The user key.
        /// </param>
        /// <param name="clientPassword">
        /// The client password.
        /// </param>
        /// <param name="eventStartDate">
        /// The event start date.
        /// </param>
        /// <param name="eventEndDate">
        /// The event end date.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public List<SportEvent> GetSyncSportEvents(string userKey, string clientPassword, DateTime eventStartDate, DateTime eventEndDate)
        {
            throw new NotImplementedException();
        }

        #region Public Methods and Operators

        /// <summary>
        /// The get event results.
        /// </summary>
        /// <param name="eventId">
        /// The event id.
        /// </param>
        /// <param name="federationId">
        /// The federation id.
        /// </param>
        /// <returns>
        /// The <see cref="Dictionary"/>.
        /// </returns>
        public Dictionary<EventClass, List<Athlete>> GetEventResults(int eventId, int federationId)
        {
            var result = this.eventResultDataMapper.GetEventResultByEventId(eventId, federationId);

            return result;
        }

        /// <summary>
        /// The get events by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public List<SportEvent> GetEventsBySportId(int sportId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get events from NIF Database based on Organization Id
        /// </summary>
        /// <param name="orgId">
        /// The organization id
        /// </param>
        /// <param name="startDate">
        /// The start date
        /// </param>
        /// <param name="endDate">
        /// the end date
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<SportEvent> GetOrgEvents(int orgId, DateTime startDate, DateTime endDate)
        {
            var result = this.sportEventDataMapper.GetOrgEvents(orgId, startDate, endDate);

            var mapper = new SportEventMapper();
            var remoteEvent = result.Select(row => mapper.Map(row, new SportEvent())).ToList();

            return remoteEvent;
        }

        #endregion

        #region private methods

        /// <summary>
        ///     Gets the person by Id
        /// </summary>
        /// <param name="personId">person id</param>
        /// <returns>A person object</returns>
        private Person GetPersonById(int personId)
        {
            return this.personRemoteDataMapper.GetPersonById(personId);
        }

        #endregion

    }
}