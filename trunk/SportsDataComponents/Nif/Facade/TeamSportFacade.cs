﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TeamSportFacade.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nif.Facade.Interfaces;
using NTB.SportsData.Components.Nif.Mappers;
using NTB.SportsData.Components.NIFConnectService;
using NTB.SportsData.Components.RemoteRepositories.DataMappers;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;
using AgeCategoryMapper = NTB.SportsData.Components.Nif.Mappers.AgeCategoryMapper;
using ChangeInfo = NTB.SportsData.Classes.Classes.ChangeInfo;
using Match = NTB.SportsData.Classes.Classes.Match;
using PartialResult = NTB.SportsData.Classes.Classes.PartialResult;
using Referee = NTB.SportsData.Classes.Classes.Referee;
using Result = NTB.SportsData.Classes.Classes.Result;
using TeamResult = NTB.SportsData.Classes.Classes.TeamResult;
using Tournament = NTB.SportsData.Classes.Classes.Tournament;

namespace NTB.SportsData.Components.Nif.Facade
{
    /// <summary>
    ///     The team sport facade.
    /// </summary>
    public class TeamSportFacade : ITeamSportFacade
    {
        /// <summary>
        ///     The sync remote data mapper
        /// </summary>
        private readonly ISynchRemoteRepository _syncRemoteDataMapper;

        /// <summary>
        ///     The match incident data mapper.
        /// </summary>
        private readonly IMatchIncidentDataMapper _matchIncidentDataMapper;

        /// <summary>
        ///     The match remote data mapper.
        /// </summary>
        private readonly IMatchRemoteDataMapper _matchRemoteDataMapper;

        /// <summary>
        ///     The referee remote repository.
        /// </summary>
        private readonly IRefereeRemoteDataMapper _refereeRemoteRepository;

        /// <summary>
        ///     The team remote data mapper.
        /// </summary>
        private readonly ITeamRemoteDataMapper _teamRemoteDataMapper;

        /// <summary>
        ///     The team result data mapper.
        /// </summary>
        private readonly ITeamResultDataMapper _teamResultDataMapper;

        /// <summary>
        ///     The tournament remote data mapper.
        /// </summary>
        private readonly ITournamentRemoteDataMapper _tournamentRemoteDataMapper;

        /// <summary>
        ///     Initializes a new instance of the <see cref="TeamSportFacade" /> class.
        ///     Initializes a new instance of the <see cref="SingleSportFacade" /> class.
        /// </summary>
        public TeamSportFacade()
        {
            this._syncRemoteDataMapper = new SynchRemoteRepository();
            this._teamResultDataMapper = new TeamResultRemoteRepository();
            this._matchRemoteDataMapper = new MatchRemoteRepository();
            this._refereeRemoteRepository = new RefereeRemoteRepository();
            this._tournamentRemoteDataMapper = new TournamentRemoteRepository();
            this._matchIncidentDataMapper = new MatchIncidentRemoteRepository();
            this._teamRemoteDataMapper = new TeamRemoteRepository();
        }

        /// <summary>
        ///     The get tournament by id.
        /// </summary>
        /// <param name="id">
        ///     The id.
        /// </param>
        /// <returns>
        ///     The <see cref="Classes.Classes.Tournament" />.
        /// </returns>
        public Tournament GetTournamentById(int id)
        {
            Logger.DebugFormat("Getting tournament by Id: {0}", id);
            var response = this._tournamentRemoteDataMapper.GetTournamentById(id);

            if (response == null)
            {
                Logger.Debug("Could not find the tournament with the Id for the sport");
                return null;
            }

            

            var tournamentClassesMapper = new TournamentClassesMapper();
            var ageCategory = new AgeCategory();
            if (response.Classes != null)
            {
                if (response.Classes.Length > 0)
                {
                    var ageCategories = response.Classes.Select(row => tournamentClassesMapper.Map(row, new AgeCategory()))
                        .ToList();

                    ageCategory = ageCategories[0];
                }

            }
            var mapper = new Tournament1Mapper();
            var returnTournament = mapper.Map(response, new Tournament());
            returnTournament.AgeCategory = ageCategory;

            return returnTournament;
        }

        /// <summary>
        ///     The get partial results by match id.
        /// </summary>
        /// <param name="matchId">
        ///     The match id.
        /// </param>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<PartialResult> GetPartialResultsByMatchId(int matchId)
        {
            Logger.DebugFormat("Getting Partial Results By Match Id: {0}", matchId);
            var remoteRepository = new MatchRemoteRepository();

            var response = remoteRepository.GetPartialResults(matchId);

            var mapper = new PartialResultMapper();

            if (response == null)
            {
                return new List<PartialResult>();
            }

            var partialResults = response.Select(row => mapper.Map(row, new PartialResult()))
                .ToList();

            return partialResults;
        }

        public List<AgeCategory> GetTournamentAgeCategory(int tournamentId)
        {
            Logger.DebugFormat("Getting AgeCategory by Tournament Id: {0}", tournamentId);
            var tournamentRemoteRepository = new TournamentRemoteRepository();
            var classCodesIds = tournamentRemoteRepository.GetTournamentClassesByTournamentid(tournamentId);

            var classCodes = new List<TournamentClass>();
            var ageCategoryRemoteRepository = new AgeCategoryRemoteRepository();
            foreach (var classCodeId in classCodesIds)
            {
                var ageCategories = ageCategoryRemoteRepository.GetClassCodeByCodeId(classCodeId.ClassId);

                classCodes.AddRange(ageCategories);
            }

            var mapper = new AgeCategoryMapper();

            var returnAgeCategories = classCodes.Select(row => mapper.Map(row, new AgeCategory()))
                .ToList();

            foreach (var returnAgeCategory in returnAgeCategories)
            {
                if (returnAgeCategory.Name.Contains("Jenter") || returnAgeCategory.Name.Contains("Gutter"))
                {
                    returnAgeCategory.AgeCategoryDefinitionId = 1;
                    returnAgeCategory.AgeCategoryDefinition = "aldersbestemt";
                }
                else
                {
                    returnAgeCategory.AgeCategoryDefinitionId = 2;
                    returnAgeCategory.AgeCategoryDefinition = "senior";
                }
                
            }

            return returnAgeCategories;
        }

        /// <summary>
        ///     The get changes team sport.
        /// </summary>
        /// <param name="userKey">
        ///     The user key.
        /// </param>
        /// <param name="clientPassword">
        ///     The client password.
        /// </param>
        /// <param name="eventStartDate">
        ///     The event start date.
        /// </param>
        /// <param name="eventEndDate">
        ///     The event end date.
        /// </param>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<ChangeInfo> GetChangesTeamSport(string userKey, string clientPassword, DateTime eventStartDate,
            DateTime eventEndDate)
        {
            Logger.DebugFormat("Getting Changes Team Sport: Start {0}, end {1}", eventStartDate, eventEndDate);
            this._syncRemoteDataMapper.FederationUserKey = userKey;
            this._syncRemoteDataMapper.ClientPassword = clientPassword;

            this._syncRemoteDataMapper.EventStartDate = eventStartDate;
            this._syncRemoteDataMapper.EventEndDate = eventEndDate;

            var listOfResults = this._syncRemoteDataMapper.GetChangesResultTeam();

            var mapper = new ChangeInfoMapper();
            return listOfResults.Select(row => mapper.Map(row, new ChangeInfo())).ToList();
            // If we have a result, we will return it, otherwise we will return null;
        }

        /// <summary>
        ///     The get match result by result id.
        /// </summary>
        /// <param name="resultId">
        ///     The result id.
        /// </param>
        /// <returns>
        ///     The <see cref="Classes.Classes.Result" />.
        /// </returns>
        public Result GetMatchResultByResultId(int resultId)
        {
            Logger.DebugFormat("Getting Match Result by Result Id: {0}", resultId);
            var result = this._teamResultDataMapper.GetMatchResultByResultId(resultId);
            var resultMapper = new ResultMapper();

            return resultMapper.Map(result, new Result());
        }

        /// <summary>
        ///     The get match info by match id.
        /// </summary>
        /// <param name="matchId">
        ///     The match id.
        /// </param>
        /// <returns>
        ///     The <see cref="Classes.Classes.Match" />.
        /// </returns>
        public Match GetMatchInfoByMatchId(int matchId)
        {
            Logger.DebugFormat("Getting Match Info by Match Id: {0}", matchId);
            var matchInfo = this._matchRemoteDataMapper.GetMatchInfoByMatchId(matchId);
            var matchMapper = new PublicMatchMapper();
            return matchMapper.Map(matchInfo, new Match());
        }

        public MatchExtended GetMatchInfoByResultId(int resultId)
        {
            Logger.DebugFormat("Getting Match Info by Match Id: {0}", resultId);

            // When we call this method we get the match, the partial results and the result
            var matchResultPartialResultsResponse = this._matchRemoteDataMapper.GetMatchInfoByResultId(resultId);
            var matchMapper = new PublicMatchMapper();
            var resultMapper = new ResultMapper();
            var partialResultMapper = new PartialResultMapper();

            var mappedMatch = matchMapper.Map(matchResultPartialResultsResponse.Match, new Match());
            var mappedResult = resultMapper.Map(matchResultPartialResultsResponse.Result, new Result());
            var mappedPartialResults = matchResultPartialResultsResponse.PartialResults.Select(row => partialResultMapper.Map(row, new PartialResult())).ToList();

            var matchExtended = new MatchExtended
            {
                Match = mappedMatch,
                MatchResult = mappedResult,
                PartialResults = mappedPartialResults
            };

            return matchExtended;
        }

        /// <summary>
        ///     The get match referees.
        /// </summary>
        /// <param name="matchId">
        ///     The match id.
        /// </param>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Referee> GetMatchReferees(int matchId)
        {
            Logger.DebugFormat("Getting Match Referees by Match Id: {0}", matchId);
            return this._refereeRemoteRepository.GetRefereeByMatchId(matchId);
        }

        public List<Match> SearchMatchesByDateAndTournamentId(int tournamentId, DateTime startDate, DateTime endDate)
        {
            Logger.DebugFormat("Getting Matches by Tournament ({0}) and Date: Staring {1}, ending {2}", tournamentId,
               startDate, endDate);
            var tournamentMatches = this._matchRemoteDataMapper.SearchMatchesByDateAndTournamentId(tournamentId, startDate.Date, endDate.Date);

            if (tournamentMatches.Count == 0)
            {
                return new List<Match>();
            }
            var matchMapper = new MatchSearchMapper();

            var refereeMapper = new RefereeTaskMapper();

            // Look at this code later
            foreach (var m in tournamentMatches)
            {
                if (m.GoalsHome == null || m.GoalsAway == null)
                {
                    // We need to get the result from the match by using another method
                    var matchResults = this._matchRemoteDataMapper.GetMatchResultByMatchId(m.MatchId);
                    if (matchResults != null)
                    {
                        m.GoalsHome = matchResults.HomeGoals;
                        m.GoalsAway = matchResults.AwayGoals;
                    }
                }
            }

            // Filter out the matches which do not have a result
            tournamentMatches = tournamentMatches.Where(m => m.GoalsHome != null || m.GoalsAway != null).ToList();

            var returnTournamentMatches = tournamentMatches.Select(row => matchMapper.Map(row, new Match())).ToList();
            
            foreach (var tournamentMatch in tournamentMatches)
            {
                var currentTournamentMatch = tournamentMatch;
                var referees = tournamentMatch.RefereeTasks.Select(row => refereeMapper.Map(row, new Referee())).ToList();

                var returnMatch = (from m in returnTournamentMatches where m.Id == currentTournamentMatch.MatchId select m).Single();

                // todo: We should also update the SportsData Insert match table here

                returnMatch.Referees = referees;
            }

            

            return returnTournamentMatches;
        }
        /// <summary>
        ///     The get matches by tournament id and date.
        /// </summary>
        /// <param name="tournamentId">
        ///     The tournament id.
        /// </param>
        /// <param name="dateStart">
        ///     The date start.
        /// </param>
        /// <param name="dateEnd">
        ///     The date end.
        /// </param>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Match> GetMatchesByTournamentIdAndDate(int tournamentId, DateTime dateStart, DateTime dateEnd)
        {
            Logger.DebugFormat("Getting Matches by Tournament ({0}) and Date: Staring {1}, ending {2}", tournamentId,
                dateStart, dateEnd);
            var tournamentMatches = this._matchRemoteDataMapper.GetMatchSummariesByTournamentId(tournamentId);
            tournamentMatches =
                tournamentMatches.Where(
                    x =>
                        x.MatchDate != null && x.MatchDate.Value.Date >= dateStart.Date &&
                        x.MatchDate.Value.Date <= dateEnd.Date)
                    .ToList();

            var matchMapper = new MatchSummaryMapper();

            return tournamentMatches.Select(row => matchMapper.Map(row, new Match()))
                .ToList();
        }

        /// <summary>
        ///     The get tournament standing by tournament id.
        /// </summary>
        /// <param name="tournamentId">
        ///     The tournament id.
        /// </param>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<TeamResult> GetTournamentStandingByTournamentId(int tournamentId)
        {
            Logger.DebugFormat("Getting Tournament Standing By Tournament Id: {0}", tournamentId);
            try
            {
                var tournamentRemoteRepository = new TournamentRemoteRepository();

                var mapper = new TeamResultMapper();

                var result = tournamentRemoteRepository.GetTournamentTable(tournamentId);
                if (result == null)
                {
                    return null;
                }

                return result.Select(row => mapper.Map(row, new TeamResult()))
                    .ToList();
            }
            catch (Exception exception)
            {
                ErrorLogger.Error(exception);

                return null;
            }
        }

        /// <summary>
        ///     The get match incident by match id.
        /// </summary>
        /// <param name="matchId">
        ///     The match id.
        /// </param>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<MatchEvent> GetMatchIncidentByMatchId(int matchId)
        {
            Logger.DebugFormat("Getting Match Incidents By Match Id: {0}", matchId);
            var result = this._matchIncidentDataMapper.GetMatchIncidentsWithSummariesSetToTrue(matchId);
            if (result.Count == 0)
            {
                result = this._matchIncidentDataMapper.GetMatchIncidentsWithSummariesSetToFalse(matchId);
            }

            var mapper = new MatchEventMapper();
            return result.Select(row => mapper.Map(row, new MatchEvent())).ToList();

        }

        /// <summary>
        ///     The get match players by match id.
        /// </summary>
        /// <param name="matchId">
        ///     The match id.
        /// </param>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Player> GetMatchPlayersByMatchId(int matchId)
        {
            Logger.DebugFormat("Getting Team Players by Match Id: {0}", matchId);
            var response = this._teamRemoteDataMapper.GetTeamPlayersByMatchId(matchId);

            var mapper = new SquadIndividualMapper();

            return response.Select(row => mapper.Map(row, new Player()))
                .ToList();
        }

        public Tournament GetTournamentByMatchId(int matchId)
        {
            var response = this._tournamentRemoteDataMapper.GetTournamentByMatchId(matchId);
            var mapper = new Tournament1Mapper();

            var returnTournament = mapper.Map(response, new Tournament());

            return returnTournament;
        }

        #region Static Fields

        /// <summary>
        ///     Creating the static Error Logger
        /// </summary>
        private static readonly ILog ErrorLogger = LogManager.GetLogger("ErrorAppenderLogger");

        /// <summary>
        ///     Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(TeamSportFacade));

        #endregion


        
    }
}