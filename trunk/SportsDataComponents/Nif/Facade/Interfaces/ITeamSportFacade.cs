﻿using System;
using System.Collections.Generic;
using NTB.SportsData.Classes.Classes;

namespace NTB.SportsData.Components.Nif.Facade.Interfaces
{
    public interface ITeamSportFacade
    {
        List<ChangeInfo> GetChangesTeamSport(string userKey, string clientPassword, DateTime eventStartDate,
            DateTime eventEndDate);

        List<Match> GetMatchesByTournamentIdAndDate(int tournamentId, DateTime dateStart, DateTime dateEnd);

        List<MatchEvent> GetMatchIncidentByMatchId(int matchId);

        Match GetMatchInfoByMatchId(int matchId);

        MatchExtended GetMatchInfoByResultId(int resultId);

        List<Player> GetMatchPlayersByMatchId(int matchId);

        List<Referee> GetMatchReferees(int matchId);

        // Result GetMatchResultByMatchId(int matchId);

        Result GetMatchResultByResultId(int resultId);

        List<PartialResult> GetPartialResultsByMatchId(int matchId);

        Tournament GetTournamentById(int id);

        Tournament GetTournamentByMatchId(int matchId);

        List<TeamResult> GetTournamentStandingByTournamentId(int tournamentId);

        List<AgeCategory> GetTournamentAgeCategory(int tournamentId);

        List<Match> SearchMatchesByDateAndTournamentId(int tournamentId, DateTime startDate, DateTime endDate);
    }
}
