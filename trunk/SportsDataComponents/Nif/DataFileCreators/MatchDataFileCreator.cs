﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NifMatchDataFileCreator.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The nif match data file creator.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using NTB.SportsData.Components.PublicRepositories;

namespace NTB.SportsData.Components.Nif.DataFileCreators
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using log4net;
    using log4net.Config;

    using Classes.Classes;
    using Mappers;
    using RemoteRepositories.DataMappers;
    using RemoteRepositories.Interfaces;

    /// <summary>
    ///     The nif match data file creator.
    /// </summary>
    public class MatchDataFileCreator : IDataFileCreator<Match>
    {
        #region Static Fields

        /// <summary>
        ///     The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(MatchDataFileCreator));

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="MatchDataFileCreator" /> class.
        /// </summary>
        public MatchDataFileCreator()
        {
            // Set up logger
            XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                BasicConfigurator.Configure();
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the data file params.
        /// </summary>
        public DataFileParams DataFileParams { get; set; }

        #endregion

        #region Explicit Interface Methods

        /// <summary>
        ///     The get.
        /// </summary>
        /// <returns>
        ///     The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        /// <exception cref="NotImplementedException">
        ///     Not implemented
        /// </exception>
        List<Match> IDataFileCreator<Match>.Get()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add missing matches.
        /// </summary>
        /// <param name="localMatches">
        /// The local matches.
        /// </param>
        /// <param name="remoteMatches">
        /// The remote matches.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Match> AddMissingMatches(List<Match> localMatches, List<Match> remoteMatches)
        {
            try
            {
                var filteredMatches = new List<Match>();

                if (localMatches.Any())
                {
                    foreach (var remoteMatch in remoteMatches)
                    {
                        var addMatch = true;
                        foreach (var match in localMatches)
                        {
                            if (remoteMatch.Id == match.Id)
                            {
                                addMatch = false;
                            }
                        }

                        if (addMatch)
                        {
                            filteredMatches.Add(remoteMatch);
                        }
                    }
                }
                else
                {
                    filteredMatches = remoteMatches;
                }

                return filteredMatches;
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return remoteMatches;
            }
        }

        /// <summary>
        ///     The configure.
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        /// <exception cref="Exception">
        ///     Throws an exception is no parameters has been set
        /// </exception>
        public bool Configure()
        {
            if (this.DataFileParams == null)
            {
                throw new Exception("DataFileParams is not set!");
            }

            return true;
        }

        /// <summary>
        /// The create result.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public void CreateResult(List<Match> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The find.
        /// </summary>
        /// <param name="remoteEvents">
        /// The remote events.
        /// </param>
        /// <param name="events">
        /// The events.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public List<Match> Find(List<Match> remoteEvents, List<Match> events)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     The find.
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        /// <exception cref="NotImplementedException">
        ///     Not implemented
        /// </exception>
        public bool Find()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     The get.
        /// </summary>
        /// <exception cref="NotImplementedException">
        ///     Not implemented
        /// </exception>
        public void Get()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get customer tournaments.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <param name="seasonId">
        /// The season id.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Tournament> GetCustomerTournaments(int sportId, int seasonId)
        {
            var repository = new TournamentRepository();
            return repository.GetCustomerTournaments(sportId, seasonId);
        }

        /// <summary>
        /// The get local by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Match> GetLocalBySportId(int sportId)
        {
            var matchRepository = new MatchRepository();
            return matchRepository.GetMatchesBySportId(sportId);
        }

        /// <summary>
        /// The get local matches by tournament id.
        /// </summary>
        /// <param name="dateStart">
        /// The date start.
        /// </param>
        /// <param name="dateEnd">
        /// The date end.
        /// </param>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Match> GetLocalMatchesByTournamentId(DateTime dateStart, DateTime dateEnd, int tournamentId)
        {
            var repository = new MatchRepository();

            return repository.GetMatchesByDateInterval(dateStart, dateEnd, tournamentId);
        }

        /// <summary>
        /// The get remote by org id.
        /// </summary>
        /// <param name="dateStart">
        /// The date start.
        /// </param>
        /// <param name="dateEnd">
        /// The date end.
        /// </param>
        /// <param name="orgId">
        /// The org id.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public List<Match> GetRemoteByOrgId(DateTime dateStart, DateTime dateEnd, int orgId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The get remote matches by tournament id.
        /// </summary>
        /// <param name="dateStart">
        /// The date start.
        /// </param>
        /// <param name="dateEnd">
        /// The date end.
        /// </param>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Match> Deprecated_GetRemoteMatchesByTournamentId(DateTime dateStart, DateTime dateEnd, int tournamentId)
        {
            try
            {
                var repository = new MatchRemoteRepository();
                var result = repository.GetMatchesByDateInterval(dateStart, dateEnd, tournamentId);

                var mapper = new MatchSearchResultMapper();

                if (result == null)
                {
                    return new List<Match>();
                }

                if (result.Any())
                {
                    // var seasonTournamentMapper = new SeasonTournamentMapper();
                    Logger.Debug("Mapping match to local match object");
                    var matches = result.Select(row => mapper.Map(row, new Match())).ToList();

                    return matches;
                }

                return new List<Match>();
            }
            catch (Exception exception)
            {
                Logger.ErrorFormat("An exception occured: {0}", exception);
                return new List<Match>();
            }
        }

        /// <summary>
        /// The get todays local matches by sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The
        ///     <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Match> GetTodaysLocalMatchesBySportId(int sportId)
        {
            var repository = new MatchRepository();
            var matches = repository.GetMatchesBySportId(sportId);

            return matches.Where(m => m.MatchDate == DateTime.Today).ToList();
        }

        /// <summary>
        /// The insert all.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public void InsertAll(List<Match> domainobject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The insert one.
        /// </summary>
        /// <param name="domainobject">
        /// The domainobject.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public void InsertOne(Match domainobject)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}