// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NifSportsDataJobListener.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The nif sports data job listener.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Components.Nif.JobListeners
{
    using System;

    using log4net;
    using log4net.Config;

    using Quartz;

    /// <summary>
    /// The nif sports data job listener.
    /// </summary>
    internal class NifSportsDataJobListener : IJobListener
    {
        #region Static Fields

        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(NifSportsDataJobListener));

        #endregion

        #region Fields

        /// <summary>
        /// The job being executed.
        /// </summary>
        private bool JobBeingExecuted = false;

        /// <summary>
        /// The _name.
        /// </summary>
        private string _name = string.Empty;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes static members of the <see cref="NifSportsDataJobListener"/> class.
        /// </summary>
        static NifSportsDataJobListener()
        {
            // Set up logger
            XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                BasicConfigurator.Configure();
            }

            Logger.Debug("SportsDataJobListener is created");
        }

        #endregion

        // public void JobToBeExecuted(JobExecutionContext context)
        // {
        // logger.Debug("Setting jobBeingExecuted to true!");
        // jobBeingExecuted = true;

        // // IScheduler sched = context.Scheduler;

        // }
        #region Public Properties

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name
        {
            get
            {
                return this._name;
            }

            set
            {
                this._name = value;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The job execution vetoed.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// Not implemented
        /// </exception>
        public void JobExecutionVetoed(IJobExecutionContext context)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The job to be executed.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public void JobToBeExecuted(IJobExecutionContext context)
        {
            Logger.Debug("Setting jobBeingExecuted to true!");
            this.JobBeingExecuted = true;

            // throw new NotImplementedException();
        }

        /// <summary>
        /// The job was executed.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="jobException">
        /// The job exception.
        /// </param>
        public void JobWasExecuted(IJobExecutionContext context, JobExecutionException jobException)
        {
            Logger.Debug("Setting jobBeingExecuted to false!");
            this.JobBeingExecuted = false;

            // throw new NotImplementedException();
        }

        #endregion
    }
}