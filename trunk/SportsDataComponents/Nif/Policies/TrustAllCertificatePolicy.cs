namespace NTB.SportsData.Components.Nif.Policies
{
    using System.Net;
    using System.Security.Cryptography.X509Certificates;

    public class TrustAllCertificatePolicy
    {
        /// <summary>
        /// The check validation result.
        /// </summary>
        /// <param name="sp">
        /// The sp.
        /// </param>
        /// <param name="cert">
        /// The cert.
        /// </param>
        /// <param name="req">
        /// The req.
        /// </param>
        /// <param name="problem">
        /// The problem.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool CheckValidationResult(ServicePoint sp, X509Certificate cert, WebRequest req, int problem)
        {
            return true;
        }
    }
}