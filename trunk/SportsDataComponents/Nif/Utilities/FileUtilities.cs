﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FileUtilities.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The file utilities.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.Nif.Utilities
{
    using System;
    using System.IO;

    using log4net;

    /// <summary>
    /// The file utilities.
    /// </summary>
    public class FileUtilities
    {
        /// <summary>
        /// Gets or sets the file output folder.
        /// </summary>
        public string FileOutputFolder { get; set; }

        /// <summary>
        /// The create output folder.
        /// </summary>
        /// <exception cref="ArgumentException">
        /// Throws an exception on error
        /// </exception>
        public void CreateOutputFolder()
        {
            // Checking if file folders exists
            try
            {
                if (this.FileOutputFolder != null)
                {
                    if (!Directory.Exists(this.FileOutputFolder))
                    {
                        Directory.CreateDirectory(this.FileOutputFolder);
                    }
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid, unknown or missing file folder: " + this.FileOutputFolder, ex);
            }
        }
    }
}