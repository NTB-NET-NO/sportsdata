﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NifTeamSportJobBuilder.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The nif team sport job builder.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using log4net;
using log4net.Config;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Classes.Enums;
using NTB.SportsData.Components.Nif.Controller;
using NTB.SportsData.Components.Nif.Creators;
using NTB.SportsData.Components.Nif.DataFileCreators;
using NTB.SportsData.Components.Nif.Facade;
using NTB.SportsData.Components.PublicRepositories;
using Quartz;

namespace NTB.SportsData.Components.Nif.JobBuilders
{
    /// <summary>
    ///     The nif team sport job builder.
    /// </summary>
    public class NifTeamSportJobBuilder : IJob
    {
        #region Static Fields

        /// <summary>
        ///     Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(NifJobBuilder));

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes static members of the <see cref="NifTeamSportJobBuilder" /> class.
        ///     Initializes static members of the <see cref="NifJobBuilder" /> class.
        /// </summary>
        static NifTeamSportJobBuilder()
        {
            // Set up logger
            XmlConfigurator.Configure();
            if (!LogManager.GetRepository()
                .Configured)
            {
                BasicConfigurator.Configure();
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The execute.
        /// </summary>
        /// <param name="context">
        ///     The context.
        /// </param>
        public void Execute(IJobExecutionContext context)
        {
            Logger.Info("Executing JobExecutionContext: " + context.JobDetail.Description);
            var outputFolder = new List<string>();

            var createXml = false;

            var insertDatabase = false;

            var results = false;

            var dateOffset = DateTime.Now;

            var duration = 0;

            var sportId = 0;

            var federationId = 0;

            var disciplineId = 0;

            var federationKey = 0;

            var operationMode = OperationMode.Gatherer;

            var scheduleType = ScheduleType.Daily;
            
            var userName = string.Empty;

            var userPassword = string.Empty;

            // Mapping information sent from Component
            var dataMap = context.JobDetail.JobDataMap;

            // Populate variables to be used to 
            try
            {
                var outputFolders = (List<string>) dataMap["OutputFolder"];
                outputFolder.AddRange(outputFolders);
                
                insertDatabase = dataMap.GetBoolean("InsertDatabase");
                Logger.Debug(insertDatabase ? "InsertDatabase: true" : "InsertDatabase: false");

                scheduleType = (ScheduleType) Enum.Parse(typeof(ScheduleType), dataMap.GetString("ScheduleType"));
                Logger.Debug("ScheduleType: " + scheduleType);

                results = dataMap.GetBoolean("Results");
                Logger.Debug(results ? "Results: true" : "Results: false");

                var scheduleMessage = dataMap.GetBoolean("ScheduleMessage");
                if (results && scheduleMessage == false)
                {
                    scheduleMessage = true;
                }

                Logger.Debug(scheduleMessage ? "ScheduleMessage: true" : "ScheduleMessage: false");

                duration = dataMap.GetInt("Duration");
                Logger.Debug("Duration: " + duration);

                createXml = dataMap.GetBoolean("CreateXML");
                Logger.Debug(createXml ? "CreateXML: true" : "CreateXML: false");

                sportId = dataMap.GetInt("SportId");

                federationId = dataMap.GetInt("FederationId");

                disciplineId = dataMap.GetInt("DisciplineId");

                federationKey = dataMap.GetInt("FederationKey");

                var userKey = ConfigurationManager.AppSettings["NIFServiceUsername"];
                
                var serviceKey = ConfigurationManager.AppSettings["NIFServiceKey"];

                userName = userKey.Replace(serviceKey, federationKey.ToString());

                userPassword = ConfigurationManager.AppSettings["NIFServicePassword"];

                if (dataMap.Keys.Contains("DateOffset"))
                {
                    dateOffset = dataMap.GetDateTime("DateOffset");
                    Logger.Debug("DateOffset: " + dateOffset.ToShortDateString());
                }

                operationMode = (OperationMode) Enum.Parse(typeof(OperationMode), dataMap.GetString("OperationMode"));
            }
            catch (Exception exception)
            {
                Logger.Debug(exception);
                Logger.Debug(exception.StackTrace);
            }

            // Creating the DataFileCreator object
            var dataFileParams = new DataFileParams
            {
                FileOutputFolder = outputFolder,
                
                SportId = sportId,
                FederationId = federationId,
                DisciplineId = disciplineId,
                FederationUserKey = userName,
                FederationKey = federationKey,
                FederationUserPassword = userPassword
            };

            var outputFolderList = this.SetOutputFolderList(outputFolder, sportId, disciplineId, federationId);

            dataFileParams.FileOutputFolders = outputFolderList;

            if (insertDatabase)
            {
                Logger.Debug("We shall insert data in the database");

                // creator.UpdateDatabase = true;
            }

            // This parameter tells us if we are to add results-tags to the XML
            dataFileParams.Results = false;
            dataFileParams.PushResults = false;
            if (results)
            {
                Logger.Debug("We shall also render result");
                dataFileParams.Results = true;
                dataFileParams.PushResults = true;
            }

            this.SetDuration(duration, dataFileParams, dateOffset);

            Logger.Debug("We are about to create XML File");
            dataFileParams.CreateXml = createXml;

            var matchDataFileCreator = new MatchDataFileCreator {DataFileParams = dataFileParams};

            // Configuring the creator object
            if (!matchDataFileCreator.Configure())
            {
                return;
            }

            // We are to distribute results
            if (operationMode == OperationMode.Distributor)
            {
                this.SetupDistributorOperationMode(scheduleType, dataFileParams);
                // teamResultController.GetChangesMatchResult(dataFileParams); // .Create();
            }

            if (operationMode == OperationMode.Gatherer)
            {
                this.RunGathererOperationMode(sportId, disciplineId, dataFileParams, matchDataFileCreator);
            }
            else
            {
                var creator = new TeamResultController(); 
                
                Logger.Debug("Running CreateResults method");
                creator.GetMatchResultsFromCalendarJob(dataFileParams);
            }
        }

        public void SetupDistributorOperationMode(ScheduleType scheduleType, DataFileParams dataFileParams)
        {
            Logger.Debug("Start setting up Distribution Operation  mode");
            var dateStart = DateTime.Today.Date;
            var dateEnd = DateTime.Now.Date;

            // Checking if we are in testing mode
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
            {
                dateStart = DateTime.Parse(ConfigurationManager.AppSettings["niftestingdate"]);
            }

            if (scheduleType == ScheduleType.Daily)
            {
                dateStart = DateTime.Now.AddDays(-1);

                // We are taking of the last five minutes because NIF has a five minute cache
                dateEnd = DateTime.Now.AddMinutes(-5);
            }

            dataFileParams.DateStart = dateStart;
            dataFileParams.DateEnd = dateEnd;

            var teamResultController = new TeamResultController();
            teamResultController.GetMatchResultsFromCalendarJob(dataFileParams);
            
            Logger.Debug("Done setting up Distribution Operation  mode");
        }

        public void RunGathererOperationMode(int sportId, int disciplineId, DataFileParams dataFileParams, MatchDataFileCreator matchDataFileCreator)
        {
            // var tournamentCreator = new TournamentDataFileCreator();
            var seasonRepository = new SeasonRepository();
            var seasons = seasonRepository.GetSeasonsBySportId(sportId);

            if (disciplineId > 0)
            {
                seasons = seasonRepository.GetSeasonBySportIdAndDisciplineId(sportId, disciplineId);
            }

            var seasonId = (from s in seasons where s.Active select s.Id).Single();

            // We only get the tournaments where we have customers subscribing
            // This so we don't have to get all results
            // Eventually all customers will end up subscribing to all tournaments
            var tournamentCreator = new TournamentDataFileCreator();
            var customerTournaments = tournamentCreator.GetCustomerTournaments(sportId, seasonId);

            var remoteMatches = new List<Match>();

            var localMatches = new List<Match>();

            if (customerTournaments.Count > 0)
            {
                foreach (var tournament in customerTournaments)
                {
                    Logger.Info("Checking matches for " + tournament.Name);
                    try
                    {
                        var facade = new TeamSportFacade();

                        var matches = facade.GetMatchesByTournamentIdAndDate(tournament.Id, dataFileParams.DateStart,
                            dataFileParams.DateEnd);

                        if (matches == null || matches.Count == 0)
                        {
                            Logger.Info("No matches found remotedly");
                            continue;
                        }

                        Logger.InfoFormat("{0} matches found", matches.Count);

                        // Adding tournament id and sport id to objects
                        foreach (var match in matches)
                        {
                            match.TournamentId = tournament.Id;
                            match.SportId = sportId;
                        }

                        remoteMatches.AddRange(matches);
                    }
                    catch (Exception exception)
                    {
                        Logger.Error(exception.Message);
                        Logger.Error(exception.StackTrace);
                    }

                    try
                    {
                        Logger.Info("Checking for matches in local database");
                        var matches = matchDataFileCreator.GetLocalMatchesByTournamentId(dataFileParams.DateStart,
                            dataFileParams.DateEnd, tournament.Id);

                        if (matches != null)
                        {
                            Logger.InfoFormat("{0} matches found in local database", matches.Count);
                            localMatches.AddRange(matches);
                        }
                    }
                    catch (Exception exception)
                    {
                        Logger.Error(exception.Message);
                        Logger.Error(exception.StackTrace);
                    }

                    // If we have information from API. we shall insert
                    if (!remoteMatches.Any())
                    {
                        continue;
                    }

                    try
                    {
                        var matches = localMatches.Any()
                            ? matchDataFileCreator.AddMissingMatches(localMatches, remoteMatches)
                            : remoteMatches;
                        var repository = new MatchRepository();

                        Logger.InfoFormat("Inserting {0} matches to database", matches.Count);
                        repository.InsertAll(matches);
                    }
                    catch (Exception exception)
                    {
                        Logger.Error(exception.Message);
                        Logger.Error(exception.StackTrace);
                    }
                }
            }
            else
            {
                Logger.Info("Customer has no tournaments configured / set up");
            }
        }

        public void SetDuration(int duration, DataFileParams dataFileParams, DateTime dateOffset)
        {
            Logger.Debug("Duration: " + duration);
            if (duration > 0)
            {
                Logger.Debug("We shall create a longer period of matches");
                dataFileParams.Duration = duration;

                // Creating the date start
                dataFileParams.DateStart = DateTime.Today;

                if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                {
                    dataFileParams.DateStart = DateTime.Parse("2011-05-16");
                }

                dataFileParams.DateEnd = dataFileParams.DateStart.AddDays(duration);
            }
            else if (duration < 0)
            {
                var datestart = DateTime.Now.AddDays(duration);

                dataFileParams.DateStart = datestart;

                dataFileParams.DateEnd = DateTime.Today;
            }
            else
            {
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                {
                    var testDate = ConfigurationManager.AppSettings["niftestingdate"];
                    dataFileParams.DateStart = DateTime.Parse(testDate);
                    dataFileParams.DateEnd = DateTime.Parse(testDate);
                }
                else
                {
                    dataFileParams.DateStart = DateTime.Today;
                    dataFileParams.DateEnd = DateTime.Today;

                    if (dateOffset != DateTime.Today)
                    {
                        dataFileParams.DateStart = dateOffset;
                        dataFileParams.DateEnd = dateOffset;
                    }

                    Logger.Debug("Date Start: " + dataFileParams.DateStart);
                    Logger.Debug("Date End: " + dataFileParams.DateEnd);
                }
            }
        }

        public List<OutputFolder> SetOutputFolderList(List<string> outputFolder, int sportId, int disciplineId, int federationId)
        {
            var outputFolderList = new List<OutputFolder>();
            foreach (var folder in outputFolder)
            {
                var output = new OutputFolder
                {
                    OutputPath = folder,
                    SportId = sportId,
                    DisciplineId = disciplineId,
                    OrgId = federationId
                };

                outputFolderList.Add(output);
            }
            return outputFolderList;
        }

        #endregion
    }
}