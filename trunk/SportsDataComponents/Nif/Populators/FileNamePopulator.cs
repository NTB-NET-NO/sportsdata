﻿using System;
using NTB.SportsData.Classes.Classes;

namespace NTB.SportsData.Components.Nif.Populators
{
    public class FileNamePopulator
    {
        /// <summary>
        ///     Gets or sets a value indicating whether has match facts.
        /// </summary>
        public bool HasMatchFacts { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether has standing.
        /// </summary>
        public bool HasStanding { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether match facts.
        /// </summary>
        public bool MatchFacts { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether render result.
        /// </summary>
        public bool RenderResult { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether result.
        /// </summary>
        public bool Result { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether standing.
        /// </summary>
        public bool Standing { get; set; }

        /// <summary>
        /// The create file name base.
        /// </summary>
        /// <param name="organization">
        /// The organization.
        /// </param>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <param name="dateTime">
        /// The date time.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string CreateFileNameBase(Federation organization, Tournament tournament, DateTime dateTime)
        {
            var fileDate = dateTime.ToString("yyyyMMdd");
            // var fileDate = dateTime.Year + dateTime.Month.ToString().PadLeft(2, '0') + dateTime.Day.ToString().PadLeft(2, '0');

            return string.Format("NTB_SportsData_{0}_{1}_T{2}", fileDate, organization.NameShort, tournament.Id);
        }
    }
}
