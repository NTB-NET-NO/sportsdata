﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.PublicRepositories;

namespace NTB.SportsData.Components.Nif.Populators
{
    public class FederationPopulator
    {
        /// <summary>
        /// The get organization from sport id.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see cref="Federation"/>.
        /// </returns>
        public Federation GetFederationFromSportId(int sportId)
        {
            var orgRepository = new FederationRepository();
            return orgRepository.GetFederationBySportId(sportId);
        }

        /// <summary>
        /// The get federation discipline by id.
        /// </summary>
        /// <param name="disciplineId">
        /// The discipline id.
        /// </param>
        /// <returns>
        /// The <see cref="Discipline"/>.
        /// </returns>
        public Discipline GetFederationDisciplineById(int disciplineId)
        {
            var repository = new FederationRepository();
            return repository.GetFederationDisciplineById(disciplineId);
        }

    }
}
