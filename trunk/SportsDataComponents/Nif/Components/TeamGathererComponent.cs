// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NIFTeamGathererComponent.cs" company="Norsk Telegrambyr� AS">
//   Copyright (c) Norsk Telegrambyr� AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using NTB.SportsData.Components.Nif.Controller;

namespace NTB.SportsData.Components.Nif.Components
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Security;
    using System.Security.Cryptography.X509Certificates;
    using System.Threading;
    using System.Timers;
    using System.Xml;

    using log4net;
    using log4net.Config;

    using Classes.Classes;
    using Classes.Enums;
    using Abstracts;
    using DataFileCreators;
    using Factories;
    using RemoteRepositories.Interfaces;

    using Quartz;

    // Logging

    // Adding support for methods in the Utilities namespace
    // using NTB.SportsData.Utilities;

    // Quartz support

    /// <summary>
    ///     The nif team gatherer component.
    /// </summary>
    public partial class NifTeamGathererComponent : Component, IBaseSportsDataInterfaces
    {
        /// <summary>
        ///     Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(NifTeamGathererComponent));

        /// <summary>
        ///     Busy status event
        /// </summary>
        private readonly AutoResetEvent _busyEvent = new AutoResetEvent(true);

        /// <summary>
        ///     Exit control event
        /// </summary>
        private readonly AutoResetEvent _stopEvent = new AutoResetEvent(false);

        /// <summary>
        ///     The season id.
        /// </summary>
        private readonly int _seasonId = 0;

        /// <summary>
        ///     Flag to indicate sucessful configure. Instance will not start polling nor handle messages if not properly
        ///     Configured
        /// </summary>
        /// <remarks>Holds the Configured status of the instance. <c>True</c> means successfully Configured</remarks>
        private bool _configured;

        /// <summary>
        ///     The create xml.
        /// </summary>
        private bool _createXml;

        /// <summary>
        ///     The Configured interval for this instance, used for Continous polling
        /// </summary>
        /// <remarks>
        ///     <para>Indicates the interval time in seconds for <c>Continous</c>polling. 60 means that the job runs every minute.</para>
        ///     <para>Default value: <c>60</c></para>
        /// </remarks>
        /// <summary>
        ///     Input file folder
        /// </summary>
        /// <remarks>
        ///     File folder where incoming files are read from. Inputs can be modified by
        ///     <see>
        ///         <cref>fileFilter</cref>
        ///     </see>
        ///     and
        ///     <see>
        ///         <cref>includeSubdirs</cref>
        ///     </see>
        /// </remarks>
        private List<string> _fileOutputFolders;

        /// <summary>
        ///     The Configured job name for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="InstanceName" /></remarks>
        private string _name;

        /// <summary>
        ///     The populate database.
        /// </summary>
        private bool _populateDatabase;

        /// <summary>
        ///     The purge.
        /// </summary>
        private bool _purge;

        /// <summary>
        ///     The push results.
        /// </summary>
        private bool _pushResults;

        /// <summary>
        ///     The push schedule.
        /// </summary>
        private bool _pushSchedule;

        /// <summary>
        ///     The schedule type.
        /// </summary>
        private ScheduleType _scheduleType;

        /// <summary>
        ///     Initializes static members of the <see cref="NifTeamGathererComponent" /> class.
        /// </summary>
        static NifTeamGathererComponent()
        {
            // Set up logger
            XmlConfigurator.Configure();
            if (!LogManager.GetRepository()
                     .Configured)
            {
                BasicConfigurator.Configure();
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="NifTeamGathererComponent" /> class.
        /// </summary>
        public NifTeamGathererComponent()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NifTeamGathererComponent"/> class.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        public NifTeamGathererComponent(IContainer container)
        {
            container.Add(this);

            this.InitializeComponent();
        }

        /// <summary>
        ///     Gets or sets the discipline id.
        /// </summary>
        public int DisciplineId { get; set; }

        /// <summary>
        /// Gets or sets the federation key.
        /// </summary>
        public int FederationKey { get; set; }

        /// <summary>
        ///     Gets or sets the federation id.
        /// </summary>
        public int FederationId { get; set; }

        /// <summary>
        ///     Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        ///     Gets or sets the component state.
        /// </summary>
        public ComponentState ComponentState { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether database populated.
        /// </summary>
        public bool DatabasePopulated { get; set; }

        /// <summary>
        ///     Gets a value indicating whether enabled.
        /// </summary>
        public bool Enabled
        {
            get
            {
                return this._Enabled;
            }
        }

        /// <summary>
        ///     Gets the name of the instance.
        /// </summary>
        /// <value>The name of the instance.</value>
        /// <remarks>The Configured instance job name.</remarks>
        public string InstanceName
        {
            get
            {
                return this._name;
            }
        }

        /// <summary>
        ///     Gets or sets the operation mode.
        /// </summary>
        /// <remarks>
        ///     <c>Gatherer</c> is the only valid mode, its hard coded for this component.
        /// </remarks>
        public OperationMode OperationMode { get; set; }

        /// <summary>
        ///     Gets the poll style.
        /// </summary>
        /// <value>The poll style.</value>
        /// <remarks>Contionous, Scheduled and FileSystemWatch are valid for <c>Gatherer</c> objects.</remarks>
        public PollStyle PollStyle
        {
            get
            {
                return this.pollStyle;
            }
        }

        /// <summary>
        /// The validate server certificate.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="certificate">
        /// The certificate.
        /// </param>
        /// <param name="chain">
        /// The chain.
        /// </param>
        /// <param name="sslPolicyErrors">
        /// The ssl policy errors.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        /// <summary>
        ///     The get component state.
        /// </summary>
        /// <returns>
        ///     The <see cref="ComponentState" />.
        /// </returns>
        public ComponentState GetComponentState(bool startupState)
        {
            // If we are in operation mode Synch Distributor, we should check if the file has been updated lately
            if (this.OperationMode == OperationMode.SynchDistributor)
            {
                return this.CheckFileAgeOfRunningComponent(startupState);
            }
            return ComponentState.Running;
        }

        public ComponentState CheckFileAgeOfRunningComponent(bool startupState)
        {
            // Create a method for this test (so we can unit test it also
            var synchFileName = this.InstanceName.ToLower() + ".txt";

            var path = ConfigurationManager.AppSettings["FileStorePath"];

            var filePath = Path.Combine(path, synchFileName);

            var fileInfo = new FileInfo(filePath);

            if (startupState == false)
            {
                if (!fileInfo.Exists)
                {
                    return ComponentState.Halted;
                }

                var fileCreationTime = fileInfo.LastWriteTime;
                var now = DateTime.Now;
                var difference = now - fileCreationTime;
                if (difference.TotalMinutes > 15)
                {
                    return ComponentState.Halted;
                }
            }

            if (startupState)
            {
                // Update the file
                var latestDownloadedDateTime = DateTime.Now;
                File.WriteAllText(filePath, latestDownloadedDateTime.ToString("yyyy-MM-dd") + " " + latestDownloadedDateTime.ToString("HH:mm:ss"));
            }
            // F� ut interval for henting - Dette m� vel settes i config av komponenten
            // var maxFileAge = this.Enabled;

            // Get the 
            return ComponentState.Running;
        }

        /// <summary>
        /// The configure.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        /// <exception cref="ArgumentException">
        /// returns an exception if something goes wrong
        /// </exception>
        /// <exception cref="Exception">
        /// returns an exception if something goes wrong
        /// </exception>
        /// <exception cref="NotSupportedException">
        /// returns an exception if something goes wrong
        /// </exception>
        public void Configure(XmlNode configNode)
        {
            var fileStorePath = ConfigurationManager.AppSettings["FileStorePath"];
            var fileStorePathDirectoryInfo = new DirectoryInfo(fileStorePath);
            if (!fileStorePathDirectoryInfo.Exists)
            {
                fileStorePathDirectoryInfo.Create();
            }


            Logger.Debug("Node: " + configNode.Name);
            if (configNode.Attributes != null)
            {
                Logger.Debug("Name attribute: " + configNode.Attributes.GetNamedItem("Name"));

                // Basic configuration sanity check
                if (configNode.Name != "NIFTeamComponent" || configNode.Attributes.GetNamedItem("Name") == null)
                {
                    throw new ArgumentException("The XML configuration node passed is invalid", "configNode");
                }

                if (Thread.CurrentThread.Name == null)
                {
                    Thread.CurrentThread.Name = configNode.Attributes.GetNamedItem("Name")
                        .ToString();
                }

                this._name = configNode.Attributes["Name"].Value;
                this._Enabled = Convert.ToBoolean(configNode.Attributes["Enabled"].Value);
                
                if (this.Enabled == false)
                {
                    // Finish configuration
                    ThreadContext.Stacks["NDC"].Pop();
                    this._configured = true;

                    return;
                }

                this.SetOperationMode(configNode);

                this.SetPollStyle(configNode);

                this.SetScheduleType(configNode);

                this.SetProduceResult(configNode);

                this.SetPushSchedule(configNode);
            }

            // Getting the name of this Component instance
            try
            {
                ThreadContext.Stacks["NDC"].Push(this.InstanceName);

                // This value is used to tell that we shall insert into database... I just wonder if we shall do it
                // on the schedule-level... Well, we try here first
                // DONE: Consider using schedule-level to decide if we are to insert into database or not

                // Check if we are to insert the data into the database
                if (configNode.Attributes != null && configNode.Attributes["InsertDatabase"] != null)
                {
                    this._populateDatabase = Convert.ToBoolean(configNode.Attributes["InsertDatabase"].Value);
                }

                // Check if we are to create an XML-File
                if (configNode.Attributes != null && configNode.Attributes["XML"] != null)
                {
                    this._createXml = Convert.ToBoolean(configNode.Attributes["XML"].Value);
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to configure this job instance!", ex);
            }

            // Some values that we need.

            // This boolean variable is used to tell us that the database shall be populated or not
            this._populateDatabase = false;

            this._purge = false;

            this._createXml = false;

            this.SetFederationId(configNode);

            this.SetSportId(configNode);
            
            this.SetDisciplineId(configNode);

            this.SetOutputFolders(configNode);

            // We shall only get this setting if we are in synch distribution mode
            if (this.OperationMode == OperationMode.SynchDistributor)
            {
                this.SetFederationKey(configNode);
            }

            if (this.pollStyle == PollStyle.CalendarPoll)
            {
                this.SetFederationKey(configNode);
            }

            if (this._Enabled)
            {
                // Checking if file folders exists
                this.CreateOutputFolders();

                // Creating Instance Name
                try
                {
                    // Switch on pollstyle
                    APollStyleFactory pollStyleFactory;
                    switch (this.pollStyle)
                    {
                        case PollStyle.Continous:
                            pollStyleFactory = new PollStyleContinousFactory();
                            pollStyleFactory.ConfigurePollStyle(configNode);
                            pollStyleFactory.FileOutputFolder = this._fileOutputFolders;
                            pollStyleFactory.PushSchedule = this._pushSchedule;
                            pollStyleFactory.PushResults = this._pushResults;
                            pollStyleFactory.SportId = this.SportId;
                            pollStyleFactory.DisciplineId = this.DisciplineId;
                            pollStyleFactory.FederationId = this.FederationId;
                            pollStyleFactory.FederationKey = this.FederationKey;

                            this.pollTimer.Interval = pollStyleFactory.Interval;
                            break;

                        // Code for Pubnub push technology
                        // CONTINUE HERE TOMORROW! (Is this done, what am I to do here tomorrow) (2012-03-09)
                        case PollStyle.PushSubscribe:

                            break;

                        case PollStyle.CalendarPoll:
                            pollStyleFactory = new PollStyleCalendarFactory();
                            pollStyleFactory.CreateXml = this._createXml;
                            pollStyleFactory.InstanceName = this.InstanceName;
                            pollStyleFactory.SportId = this.SportId;
                            pollStyleFactory.DisciplineId = this.DisciplineId;
                            pollStyleFactory.FederationId = this.FederationId;
                            pollStyleFactory.FederationKey = this.FederationKey;
                            pollStyleFactory.FileOutputFolder = this._fileOutputFolders;
                            pollStyleFactory.OperationMode = this.OperationMode;
                            pollStyleFactory.PopulateDatabase = this._populateDatabase;
                            pollStyleFactory.Purge = this._purge;
                            pollStyleFactory.ScheduleType = this._scheduleType;

                            pollStyleFactory.ConfigurePollStyle(configNode);

                            // Getting the times for this job
                            try
                            {
                                TournamentDataFileCreator dataFile = null;
                                if (pollStyleFactory.DataFileParams != null)
                                {
                                    dataFile = new TournamentDataFileCreator
                                                   {
                                                       TeamDataFileParams = pollStyleFactory.DataFileParams
                                                   };
                                }

                                if (dataFile != null && dataFile.Configure() == false)
                                {
                                    throw new SportsDataException("Problems initiating database tables");
                                }
                            }
                            catch (SchedulerConfigException sce)
                            {
                                Logger.Debug("A SchedulerConfigException has occured: ", sce);
                            }
                            catch (SchedulerException se)
                            {
                                Logger.Debug("A schedulerException has occured: ", se);
                            }
                            catch (Exception e)
                            {
                                Logger.Debug("An exception has occured", e);
                            }

                            break;

                        case PollStyle.Scheduled:
                            throw new NotSupportedException("Invalid polling style for this job type");

                        case PollStyle.FileSystemWatch:
                            throw new NotSupportedException("Invalid polling style for this job type");

                        default:

                            // Unsupported pollstyle for this object
                            throw new NotSupportedException("Invalid polling style for this job type (" + this.pollStyle + ")");
                    }
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing PollStyle-specific values in XML configuration node: " + ex.Message, ex);
                }
            }

            var alertReceivers = this.AlertReceivers(configNode.SelectSingleNode("../NIFTeamComponent[@Name='" + this.InstanceName + "']/AlertReceivers"));

            // Now we must check that our databases are set up for this configured sport (federation)
            var dataFileParams = new DataFileParams
                                     {
                                         CreateXml = this._createXml, 
                                         FederationId = this.FederationId, 
                                         FileOutputFolder = this._fileOutputFolders, 
                                         SportId = this.SportId, 
                                         DisciplineId = this.DisciplineId, 
                                         AlertReceivers = alertReceivers
                                     };

            // Checking if we are in synch distributor mode. Then we shall add the federation key
            if (this.OperationMode == OperationMode.SynchDistributor)
            {
                dataFileParams.FederationKey = this.FederationKey;
            }

            // If we are in PollStyle.Continous, then we shall add the interval to the DataParam object
            if (this.pollStyle == PollStyle.Continous)
            {
                dataFileParams.Interval = Convert.ToInt32(this.pollTimer.Interval);
            }

            var creator = new TournamentDataFileCreator
                              {
                                  TeamDataFileParams = dataFileParams
                              };

            try
            {
                if (!creator.Configure())
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    this._configured = false;
                }
            }
            catch (Exception exception)
            {
                ThreadContext.Stacks["NDC"].Pop();
                this._configured = false;
                throw new ArgumentException("Cannot populate database" + exception.Message, exception);
            }

            // Finish configuration
            ThreadContext.Stacks["NDC"].Pop();
            this._configured = true;
        }

        /// <summary>
        ///     The start.
        /// </summary>
        /// <exception cref="SportsDataException">
        ///     Throws an exception if something goes wrong
        /// </exception>
        public void Start()
        {
            ServicePointManager.ServerCertificateValidationCallback = ValidateServerCertificate;

            if (!this._configured)
            {
                throw new SportsDataException("NIFWSComponent is not properly Configured. NIFWSComponent::Start() Aborted!");
            }

            if (!this._Enabled)
            {
                throw new SportsDataException("NIFWSComponent is not Enabled. NIFWSComponent::Start() Aborted!");
            }

            APollStyleFactory pollStyleFactory;

            // dataFileCreator.CreateDataFileFromGet();
            if (this.pollStyle == PollStyle.Continous)
            {
                pollStyleFactory = new PollStyleContinousFactory();
                pollStyleFactory.StartPollStyle();

                this.pollTimer.Enabled = true;
            }

            if (this.pollStyle == PollStyle.CalendarPoll)
            {
                pollStyleFactory = new PollStyleCalendarFactory();
                pollStyleFactory.StartPollStyle();
            }

            this._stopEvent.Reset();
            this._busyEvent.Set();

            // Starting poll-timer
            // pollTimer.Interval = 2000;
            this.pollTimer.Enabled = true;
            this.pollTimer.Start();
        }

        /// <summary>
        ///     The stop.
        /// </summary>
        /// <exception cref="SportsDataException">
        ///     Throws an exception if something goes wrong
        /// </exception>
        public void Stop()
        {
            if (!this._configured)
            {
                throw new SportsDataException(string.Format("{0} is not properly Configured. {0}::Stop() Aborted", this._name));
            }

            if (!this._Enabled)
            {
                throw new SportsDataException(string.Format("{0} is not properly Configured. {0}::Stop() Aborted", this._name));
            }

            APollStyleFactory pollStyleFactory;

            // dataFileCreator.CreateDataFileFromGet();
            if (this.pollStyle == PollStyle.Continous)
            {
                pollStyleFactory = new PollStyleContinousFactory();
                pollStyleFactory.StartPollStyle();

                this.pollTimer.Enabled = false;
                this.pollTimer.Stop();
            }

            // Check status - Handle busy polltimer loops
            if (this.pollStyle == PollStyle.CalendarPoll)
            {
                pollStyleFactory = new PollStyleCalendarFactory();
                pollStyleFactory.InstanceName = this.InstanceName;
                pollStyleFactory.StopPollStyle();
            }
        }

        /// <summary>
        /// Setting the Alert Receivers
        /// </summary>
        /// <param name="configNode">
        /// Contains the element AlertReceivers in the XML
        /// </param>
        /// <returns>
        /// List of Alert Receivers - persons who shall receive an e-mail if something happens in the service
        /// </returns>
        public List<AlertReceiver> AlertReceivers(XmlNode configNode)
        {
            if (configNode == null)
            {
                return new List<AlertReceiver>();
            }

            var alertReceivers = new List<AlertReceiver>();
            try
            {
                var xmlNodes = configNode.SelectNodes("Receiver");

                if (xmlNodes == null)
                {
                    return new List<AlertReceiver>();
                }

                foreach (XmlNode xmlNode in xmlNodes)
                {
                    var alertReceiver = new AlertReceiver();
                    if (xmlNode.Attributes != null)
                    {
                        alertReceiver.AlertLevel = (AlertLevel)Enum.Parse(typeof(AlertLevel), xmlNode.Attributes["level"].Value, true);
                        alertReceiver.Receiver = xmlNode.InnerText;
                    }

                    alertReceivers.Add(alertReceiver);
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }

            return alertReceivers;
        }

        /// <summary>
        ///     The create output folders.
        /// </summary>
        private void CreateOutputFolders()
        {
            if (!this._fileOutputFolders.Any())
            {
                return;
            }

            foreach (var outputFolder in this._fileOutputFolders)
            {
                try
                {
                    if (!Directory.Exists(outputFolder))
                    {
                        Directory.CreateDirectory(outputFolder);
                    }
                }
                catch (Exception ex)
                {
                    // throw new ArgumentException("Invalid, unknown or missing file folder: " + this.fileOutputFolders, ex);
                    ThreadContext.Stacks["NDC"].Pop();
                    Logger.Error(ex);
                }
            }
        }

        /// <summary>
        /// The set output folders.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        private void SetOutputFolders(XmlNode configNode)
        {
            try
            {
                this._fileOutputFolders = new List<string>();

                // Find the file folder to work with
                var folderNodes = configNode.SelectNodes("Folders/Folder[@Type='XMLOutputFolder']");

                // Checking if there are any folders defined.
                if (folderNodes != null)
                {
                    foreach (XmlNode folderNode in folderNodes)
                    {
                        // XmlNode folderNode = configNode.SelectSingleNode("Folders/Folder[@Type='XMLOutputFolder']");
                        // configNode.SelectSingleNode("Folder").Attributes("XMLOutputFolder");
                        if (folderNode != null)
                        {
                            this._fileOutputFolders.Add(folderNode.InnerText);
                        }

                        Logger.InfoFormat("Gatherer job - Polling: {0} / File output folder: {1} / Enabled: {2}", Enum.GetName(typeof(PollStyle), this.pollStyle), this._fileOutputFolders, this._Enabled);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get output-folder", ex);
            }
        }

        /// <summary>
        /// The set sport id.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        /// <exception cref="ArgumentException">
        /// Throws an exception if something goes wrong
        /// </exception>
        private void SetSportId(XmlNode configNode)
        {
            // Getting and setting the Federation Id and the Sport Id
            try
            {
                var federationNode = configNode.SelectSingleNode("Federation");

                if (federationNode != null && federationNode.Attributes != null)
                {
                    if (federationNode.Attributes["sportId"] != null)
                    {
                        this.SportId = Convert.ToInt32(federationNode.Attributes["sportId"].Value);
                    }
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing Federation if and sport id values in XML configuration node", ex);
            }
        }

        /// <summary>
        /// The set federation id.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        /// <exception cref="ArgumentException">
        /// Throws an exception if something goes wrong
        /// </exception>
        private void SetFederationId(XmlNode configNode)
        {
            // Getting and setting the Federation Id and the Sport Id
            try
            {
                var federationNode = configNode.SelectSingleNode("Federation");

                if (federationNode == null || federationNode.Attributes == null)
                {
                    return;
                }

                if (federationNode.Attributes["orgId"] != null)
                {
                    this.FederationId = Convert.ToInt32(federationNode.Attributes["orgId"].Value);
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing Federation if and sport id values in XML configuration node", ex);
            }
        }

        /// <summary>
        /// The set federation id.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        /// <exception cref="ArgumentException">
        /// Throws an exception if something goes wrong
        /// </exception>
        private void SetFederationKey(XmlNode configNode)
        {
            // No need to add this value if we are not in synch distributor mode
            var returnFromMethod = this.OperationMode != OperationMode.SynchDistributor;

            if (this.pollStyle == PollStyle.CalendarPoll)
            {
                returnFromMethod = false;
            }

            if (returnFromMethod)
            {
                return;
            }

            // Getting and setting the Federation Key 
            try
            {
                var federationNode = configNode.SelectSingleNode("Federation");

                if (federationNode == null || federationNode.Attributes == null)
                {
                    throw new ArgumentException("Invalid or missing Federation node or attributes in XML configuration node. Please check your configuration file.");
                }

                if (federationNode.Attributes["key"] == null)
                {
                    throw new ArgumentException("Invalid or missing Federation key attribute in XML configuration node. Please check your configuration file.");
                }

                this.FederationKey = Convert.ToInt32(federationNode.Attributes["key"].Value);
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing Federation if and sport id values in XML configuration node", ex);
            }
        }

        /// <summary>
        /// The set discipline id.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        /// <exception cref="ArgumentException">
        /// Throws an exception if something goes wrong
        /// </exception>
        private void SetDisciplineId(XmlNode configNode)
        {
            // Getting and setting the Federation Id and the Sport Id
            try
            {
                var federationNode = configNode.SelectSingleNode("Federation");

                if (federationNode == null || federationNode.Attributes == null)
                {
                    return;
                }

                if (federationNode.Attributes["disciplineId"] != null)
                {
                    this.DisciplineId = Convert.ToInt32(federationNode.Attributes["disciplineId"].Value);
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing Federation if and sport id values in XML configuration node", ex);
            }
        }

        /// <summary>
        /// The set schedule type.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        /// <exception cref="ArgumentException">
        /// throws an exception on error
        /// </exception>
        private void SetScheduleType(XmlNode configNode)
        {
            // Getting the ScheduleType which tells if this what type of scheduling we are doing
            try
            {
                if (configNode.Attributes != null)
                {
                    this._scheduleType = (ScheduleType)Enum.Parse(typeof(ScheduleType), configNode.Attributes["ScheduleType"].Value, true);
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing ScheduleType values in XML configuration node", ex);
            }
        }

        /// <summary>
        /// The set poll style.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        /// <exception cref="ArgumentException">
        /// throws an exception on error
        /// </exception>
        private void SetPollStyle(XmlNode configNode)
        {
            // Getting the PollStyle which tells if this what type of PollStyle this is
            try
            {
                if (configNode.Attributes != null)
                {
                    this.pollStyle = (PollStyle)Enum.Parse(typeof(PollStyle), configNode.Attributes["PollStyle"].Value, true);
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing PollStyle values in XML configuration node", ex);
            }
        }

        /// <summary>
        /// The set operation mode.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        /// <exception cref="ArgumentException">
        /// throws an exception on error
        /// </exception>
        private void SetOperationMode(XmlNode configNode)
        {
            // Getting the OperationMode which tells if this what type of OperationMode this is
            try
            {
                if (configNode.Attributes != null)
                {
                    this.OperationMode = (OperationMode)Enum.Parse(typeof(OperationMode), configNode.Attributes["OperationMode"].Value, true);
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing OperationMode values in XML configuration node", ex);
            }
        }

        #region Methods

        /// <summary>
        /// The set push schedule.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        /// <exception cref="ArgumentException">
        /// throws an exception on error
        /// </exception>
        private void SetPushSchedule(XmlNode configNode)
        {
            // Getting the Result attribute
            try
            {
                if (configNode.Attributes != null)
                {
                    if (configNode.Attributes["Result"] != null)
                    {
                        this._pushResults = Convert.ToBoolean(configNode.Attributes["Result"].Value);
                    }
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing Result values in XML configuration node", ex);
            }
        }

        /// <summary>
        /// The set produce result.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        /// <exception cref="ArgumentException">
        /// throws an exception on error
        /// </exception>
        private void SetProduceResult(XmlNode configNode)
        {
            // Getting the Push Schedule attribute
            try
            {
                if (configNode.Attributes != null)
                {
                    if (configNode.Attributes["PushSchedule"] != null)
                    {
                        this._pushResults = Convert.ToBoolean(configNode.Attributes["PushSchedule"].Value);
                    }
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing PushSchedule values in XML configuration node", ex);
            }
        }

        /// <summary>
        /// The poll timer_ elapsed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void PollTimerElapsed(object sender, ElapsedEventArgs e)
        {
            
            /*
             * We can consider adding getting data here if the time (day + hour + minute) hits. Otherwize we shall do nothing.
             * 
             */
            ThreadContext.Properties["JOBNAME"] = this.InstanceName;

            Logger.Debug("NIFTeamGathererComponent::pollTimer_Elapsed() hit");
            Logger.DebugFormat("We have the following settings: Interval: {0}", this.pollTimer.Interval);

            // _busyEvent.WaitOne();
            this.pollTimer.Stop();
            try
            {
                this._busyEvent.WaitOne();
                var dataFileParams = new DataFileParams
                {
                    CreateXml = this._createXml,
                    Results = this._pushResults,
                    PopulateDatabase = this._populateDatabase,
                    PushResults = this._pushResults,
                    PushSchedule = this._pushSchedule,
                    FederationId = this.FederationId,
                    SportId = this.SportId,
                    SeasonId = this._seasonId,
                    DisciplineId = this.DisciplineId,
                    PollStyle = this.PollStyle,
                    OperationMode = this.OperationMode,
                    FederationKey = this.FederationKey,
                    FederationUserPassword = ConfigurationManager.AppSettings["NIFServicePassword"],
                    FileStorePath = ConfigurationManager.AppSettings["FileStorePath"],
                    InstanceName = this.InstanceName

                };

                var userKey = ConfigurationManager.AppSettings["NIFServiceUsername"];
                var serviceKey = ConfigurationManager.AppSettings["NIFServiceKey"];
                dataFileParams.FederationUserKey = userKey.Replace(serviceKey, this.FederationKey.ToString());

                dataFileParams.FileOutputFolders = new List<OutputFolder>();
                foreach (var fileOutputFolder in this._fileOutputFolders)
                {
                    var outputFolder = new OutputFolder();
                    outputFolder.OutputPath = fileOutputFolder;
                    outputFolder.SportId = this.SportId;
                    outputFolder.DisciplineId = this.DisciplineId;
                    outputFolder.OrgId = this.FederationId;
                    dataFileParams.FileOutputFolders.Add(outputFolder);
                }

                switch (this.pollStyle)
                {
                    // For continuous and scheduled do a simple folder item traversal
                    case PollStyle.Scheduled:
                    case PollStyle.Continous:

                        if (this.OperationMode == OperationMode.SynchDistributor)
                        {
                            var teamResultController = new TeamResultController();
                            teamResultController.GetChangesMatchResult(dataFileParams, this.pollTimer.Interval);
                        }

                        if (this.OperationMode == OperationMode.Distributor)
                        {
                            var dateStart = DateTime.Today.Date;

                            // Checking if we are in testing mode
                            if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                            {
                                dateStart = DateTime.Parse(ConfigurationManager.AppSettings["niftestingdate"]);
                            }

                            dataFileParams.DateStart = dateStart;
                            dataFileParams.DateEnd = dateStart;

                            var teamResultController = new TeamResultController();
                            teamResultController.GetChangesMatchResult(dataFileParams, this.pollTimer.Interval);
                            //var creator = new TeamResultsCreator()
                            //                             {
                            //                                 DataParams = dataFileParams
                            //                             };

                            Logger.Debug("Running CreateResults method");
                            // teamResultController.GetChangesMatchResult(dataFileParams); // .Create();
                        }

                        // Gatherer is used to just store the data in a database
                        if (this.OperationMode == OperationMode.Gatherer)
                        {
                            var dateStart = DateTime.Today.Date;

                            // Checking if we are in testing mode
                            if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                            {
                                dateStart = DateTime.Parse(ConfigurationManager.AppSettings["niftestingdate"]);
                            }

                            dataFileParams.DateStart = dateStart;
                            dataFileParams.DateEnd = dateStart;

                            //var creator = new TeamResultsCreator()
                            //                             {
                            //                                 DataParams = dataFileParams
                            //                             };

                            Logger.Debug("Running CreateResults method");
                            var teamResultController = new TeamResultController();

                            // creator.Create();
                        }

                        this.pollTimer.Start();

                        break;

                    case PollStyle.CalendarPoll:

                        // @todo: need to do some work here
                        Logger.Debug("Disabling pollTimer");
                        this.pollTimer.Enabled = false;

                        // _busyEvent.WaitOne();
                        break;

                    case PollStyle.PushSubscribe:

                        // Subscriber key =             sub-4fb246b0-982a-11e1-ad0b-e19db246ca40
                        // Staging channel =          THE_FIKS_STAGING_CHANNEL

                        // We need to check the service state
                        this.pollTimer.Start();
                        break;

                    /*
                 * M� legge inn i databasen om denne turneringen er push eller pull enablet.
                 * Sjekke om vi skal bruke den andre l�sningen for push...
                 * Anders g�r over om dagens nye l�sning er 1:1 gammel l�sning.
                 */
                }

                this._busyEvent.Set();
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                if (exception.InnerException != null)
                {
                    Logger.Error(exception.InnerException.Message);
                    Logger.Error(exception.InnerException.StackTrace);
                }
                this._busyEvent.Set();
            }
        }

        #endregion

        /// <summary>
        ///     The trust all certificate policy.
        /// </summary>
        public class TrustAllCertificatePolicy : ICertificatePolicy
        {
            /// <summary>
            /// The check validation result.
            /// </summary>
            /// <param name="sp">
            /// The sp.
            /// </param>
            /// <param name="cert">
            /// The cert.
            /// </param>
            /// <param name="req">
            /// The req.
            /// </param>
            /// <param name="problem">
            /// The problem.
            /// </param>
            /// <returns>
            /// The <see cref="bool"/>.
            /// </returns>
            public bool CheckValidationResult(ServicePoint sp, X509Certificate cert, WebRequest req, int problem)
            {
                return true;
            }
        }
    }
}