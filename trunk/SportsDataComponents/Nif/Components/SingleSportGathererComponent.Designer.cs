// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NIFGathererComponent.Designer.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the NifSingleSportGathererComponent type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.Nif.Components
{
    using System.ComponentModel;
    using System.Timers;

    using NTB.SportsData.Classes.Enums;

    /// <summary>
    /// The nif gatherer component.
    /// </summary>
    public partial class NifSingleSportGathererComponent
    {
        /// <summary>
        /// The polling style for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="PollStyle"/></remarks>
        protected PollStyle pollStyle;

        /// <summary>
        /// The Enabled status for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="Enabled"/></remarks>
        protected bool _Enabled;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// The service timer.
        /// </summary>
        private Timer ServiceTimer;

        /// <summary>
        /// The poll timer.
        /// </summary>
        private Timer pollTimer;

        /// <summary>
        /// Gets or sets the maintenance mode.
        /// </summary>
        public MaintenanceMode MaintenanceMode { get; set; }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ServiceTimer = new System.Timers.Timer();
            this.pollTimer = new System.Timers.Timer();
            ((System.ComponentModel.ISupportInitialize)(this.ServiceTimer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pollTimer)).BeginInit();
            // 
            // ServiceTimer
            // 
            this.ServiceTimer.Interval = 2000D;
            // 
            // pollTimer
            // 
            this.pollTimer.Interval = 60000D;
            this.pollTimer.Elapsed += new System.Timers.ElapsedEventHandler(this.PollTimerElapsed);
            ((System.ComponentModel.ISupportInitialize)(this.ServiceTimer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pollTimer)).EndInit();

        }

        #endregion
    }
}
