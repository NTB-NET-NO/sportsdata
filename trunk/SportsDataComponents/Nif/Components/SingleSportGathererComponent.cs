using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Timers;
using System.Xml;
using log4net;
using log4net.Config;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Classes.Enums;
using NTB.SportsData.Components.Nif.DataFileCreators;
using NTB.SportsData.Components.Nif.JobBuilders;
using NTB.SportsData.Components.RemoteRepositories.Interfaces;
using Quartz;
using Quartz.Impl;
using Quartz.Impl.Matchers;

namespace NTB.SportsData.Components.Nif.Components
{
    // Logging

    // using NTB.SportsData.Utilities;

    // Quartz support

    /// <summary>
    ///     The nif gatherer component.
    /// </summary>
    public partial class NifSingleSportGathererComponent : Component, IBaseSportsDataInterfaces
    {
        /// <summary>
        ///     Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(NifSingleSportGathererComponent));

        /// <summary>
        ///     Busy status event
        /// </summary>
        private readonly AutoResetEvent busyEvent = new AutoResetEvent(true);

        /// <summary>
        ///     Exit control event
        /// </summary>
        private readonly AutoResetEvent stopEvent = new AutoResetEvent(false);

        /// <summary>
        ///     Flag to indicate sucessful configure. Instance will not start polling nor handle messages if not properly
        ///     Configured
        /// </summary>
        /// <remarks>Holds the Configured status of the instance. <c>True</c> means successfully Configured</remarks>
        private bool configured;

        /// <summary>
        ///     The create xml.
        /// </summary>
        private bool createXml;

        /// <summary>
        ///     Body for email notifications
        /// </summary>
        /// <remarks>The body is built during processing and used when sending the email when processing completes</remarks>
        private string emailBody;

        /// <summary>
        ///     Send Email-notifications when a new messages is processed
        /// </summary>
        /// <remarks>Set to an email address. Multiple adresses are supported, separate with <c>;</c></remarks>
        private string emailNotification;

        /// <summary>
        ///     Subject for email notifications
        /// </summary>
        /// <remarks>The subject is built during processing and used when sending the email when processing completes</remarks>
        private string emailSubject;

        /// <summary>
        ///     Error-Retry flag
        /// </summary>
        /// <remarks>
        ///     The flag is being set if a network or EWS error is encountered. Current operations are being aborted for later
        ///     retry.
        /// </remarks>
        private bool errorRetry = false;

        /// <summary>
        ///     The federation id.
        /// </summary>
        private int federationId;

        /// <summary>
        ///     The Federation Key
        /// </summary>
        private int federationKey;

        /// <summary>
        ///     The federation
        /// </summary>
        private string federationUserId;

        /// <summary>
        ///     File folder where completed files are stored
        /// </summary>
        /// <remarks>
        ///     <para>File folder where completed files are archived. Subdirectories for years, months and dates are created.</para>
        ///     <para>If this is not set, imported files are deleted instead of archived.</para>
        /// </remarks>
        private string fileDoneFolder;

        /// <summary>
        ///     Error file folder
        /// </summary>
        /// <remarks>
        ///     <para>File folder where failing files are stored.</para>
        ///     <para>If this is not set, failing files are deleted instead of saved.</para>
        /// </remarks>
        private string fileErrorFolder;

        /// <summary>
        ///     Input file folder
        /// </summary>
        /// <remarks>
        ///     File folder where incoming files are read from. Inputs can be modified by
        ///     <see>
        ///         <cref>fileFilter</cref>
        ///     </see>
        ///     and
        ///     <see>
        ///         <cref>includeSubdirs</cref>
        ///     </see>
        /// </remarks>
        private List<string> fileOutputFolder;

        /// <summary>
        ///     The interval.
        /// </summary>
        private int interval = 60;

        /// <summary>
        ///     The Configured job name for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="InstanceName" /></remarks>
        private string name;

        /// <summary>
        ///     The populate database.
        /// </summary>
        private bool populateDatabase;

        /// <summary>
        ///     The purge.
        /// </summary>
        private bool purge;

        /// <summary>
        ///     The push results.
        /// </summary>
        private bool pushResults;

        /// <summary>
        ///     The push schedule.
        /// </summary>
        private bool pushSchedule;

        /// <summary>
        ///     The scheduler.
        /// </summary>
        private IScheduler scheduler;

        /// <summary>
        ///     schedulerFactory to be used to create scheduled tasks
        /// </summary>
        private StdSchedulerFactory schedulerFactory = new StdSchedulerFactory();

        /// <summary>
        ///     The schedule type.
        /// </summary>
        private ScheduleType scheduleType;

        // Which is related to this

        /// <summary>
        ///     The season id.
        /// </summary>
        private int seasonId = 0;

        /// <summary>
        ///     The sport id.
        /// </summary>
        private int sportId;

        /// <summary>
        ///     Initializes static members of the <see cref="NifSingleSportGathererComponent" /> class.
        ///     Initializes the
        ///     <see>
        ///         <cref>GathererComponent</cref>
        ///     </see>
        ///     class.
        /// </summary>
        static NifSingleSportGathererComponent()
        {
            // Set up logger
            XmlConfigurator.Configure();
            if (!LogManager.GetRepository()
                .Configured)
            {
                BasicConfigurator.Configure();
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="NifSingleSportGathererComponent" /> class.
        /// </summary>
        public NifSingleSportGathererComponent()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NifSingleSportGathererComponent"/> class.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        public NifSingleSportGathererComponent(IContainer container)
        {
            container.Add(this);

            this.InitializeComponent();
        }

        /// <summary>
        ///     Gets a value indicating whether the Enabled status of the instance.
        /// </summary>
        /// <value>The Enabled status.</value>
        /// <remarks><c>True</c> if the job instance is Enabled.</remarks>
        public bool Enabled
        {
            get { return this._Enabled; }
        }

        /// <summary>
        ///     Gets the name of the instance.
        /// </summary>
        /// <value>The name of the instance.</value>
        /// <remarks>The Configured instance job name.</remarks>
        public string InstanceName
        {
            get { return this.name; }
        }

        /// <summary>
        ///     Gets or sets the operation mode.
        /// </summary>
        /// <value>The operation mode.</value>
        /// <remarks><c>Gatherer</c> is the only valid mode, its hard coded for this component.</remarks>
        public OperationMode OperationMode { get; set; }

        // {
        // get { return OperationMode.Gatherer; }
        // set { throw new NotImplementedException(); }
        // }

        /// <summary>
        ///     Gets the poll style.
        /// </summary>
        /// <value>The poll style.</value>
        /// <remarks>Contionous, Scheduled and FileSystemWatch are valid for <c>Gatherer</c> objects.</remarks>
        public PollStyle PollStyle
        {
            get { return this.pollStyle; }
        }

        /// <summary>
        /// The configure.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        public void Configure(XmlNode configNode)
        {
            Logger.Debug("Node: " + configNode.Name);
            if (configNode.Attributes != null)
            {
                this.name = configNode.Attributes["Name"].Value;
                this._Enabled = Convert.ToBoolean(configNode.Attributes["Enabled"].Value);
                
                ThreadContext.Stacks["NDC"].Push(this.InstanceName);

                if (this._Enabled == false)
                {
                    // Finish configuration
                    ThreadContext.Stacks["NDC"].Pop();
                    this.configured = true;

                    return;
                }

                Logger.Debug("Name attribute: " + configNode.Attributes.GetNamedItem("Name"));

                // Basic configuration sanity check
                if (configNode.Name != "NIFSingleSportComponent" || configNode.Attributes.GetNamedItem("Name") == null)
                {
                    throw new ArgumentException("The XML configuration node passed is invalid", "configNode");
                }

                if (Thread.CurrentThread.Name == null)
                {
                    Thread.CurrentThread.Name = configNode.Attributes.GetNamedItem("Name")
                        .ToString();
                }

                // Getting the OperationMode which tells if this what type of OperationMode this is
                try
                {
                    this.OperationMode = (OperationMode) Enum.Parse(typeof(OperationMode), configNode.Attributes["OperationMode"].Value, true);
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing OperationMode values in XML configuration node", ex);
                }

                // Getting the PollStyle which tells if this what type of PollStyle this is
                try
                {
                    this.pollStyle = (PollStyle) Enum.Parse(typeof(PollStyle), configNode.Attributes["PollStyle"].Value, true);
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing PollStyle values in XML configuration node", ex);
                }

                // Getting the ScheduleType which tells if this what type of scheduling we are doing
                try
                {
                    this.scheduleType = (ScheduleType) Enum.Parse(typeof(ScheduleType), configNode.Attributes["ScheduleType"].Value, true);

                    // ScheduleType = (ScheduleT
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing ScheduleType values in XML configuration node", ex);
                }
            }


            // Some values that we need.

            // This boolean variable is used to tell us that the database shall be populated or not
            this.populateDatabase = false;

            this.purge = false;

            this.createXml = false;

            // Getting the name of this Component instance
            try
            {
                // This value is used to tell that we shall insert into database... I just wonder if we shall do it
                // on the schedule-level... Well, we try here first
                // DONE: Consider using schedule-level to decide if we are to insert into database or not

                // Check if we are to insert the data into the database
                if (configNode.Attributes != null && configNode.Attributes["InsertDatabase"] != null)
                {
                    this.populateDatabase = Convert.ToBoolean(configNode.Attributes["InsertDatabase"].Value);
                }

                // Check if we are to create an XML-File
                if (configNode.Attributes != null && configNode.Attributes["XML"] != null)
                {
                    this.createXml = Convert.ToBoolean(configNode.Attributes["XML"].Value);
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to configure this job instance!", ex);
            }

            try
            {
                var federationNode = configNode.SelectSingleNode("Federation");

                if (federationNode != null && federationNode.Attributes != null)
                {
                    this.federationId = Convert.ToInt32(federationNode.Attributes["orgId"].Value);
                    this.sportId = Convert.ToInt32(federationNode.Attributes["sportId"].Value);

                    // This might not be present, so we better check for it.
                    if (federationNode.Attributes["key"] != null)
                    {
                        this.federationKey = Convert.ToInt32(federationNode.Attributes["key"].Value);
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Fatal("Not possible to get FederationId or SportId", exception);
            }

            try
            {
                // Find the file folder to work with
                // DONE: This could be a nodelist though - need to check if we get more than one result
                var folderNodes = configNode.SelectNodes("Folders/Folder[@Type='XMLOutputFolder']");
                if (folderNodes != null)
                {
                    foreach (XmlNode folderNode in folderNodes)
                    {
                        // XmlNode folderNode = configNode.SelectSingleNode("Folders/Folder[@Type='XMLOutputFolder']");
                        // configNode.SelectSingleNode("Folder").Attributes("XMLOutputFolder");
                        if (folderNode != null)
                        {
                            this.fileOutputFolder.Add(folderNode.InnerText);
                        }

                        Logger.InfoFormat("Gatherer job - Polling: {0} / File output folder: {1} / Enabled: {2}", Enum.GetName(typeof(PollStyle), this.pollStyle), this.fileOutputFolder, this._Enabled);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get output-folder", ex);
            }

            if (this._Enabled)
            {
                // Checking if file folders exists
                foreach (var outputFolder in this.fileOutputFolder)
                {
                    try
                    {
                        if (outputFolder != null)
                        {
                            if (!Directory.Exists(outputFolder))
                            {
                                Directory.CreateDirectory(outputFolder);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ThreadContext.Stacks["NDC"].Pop();
                        // throw new ArgumentException("Invalid, unknown or missing file folder: " + this.fileOutputFolder, ex);
                        Logger.Error(ex);
                    }
                }


                // Creating Instance Name

                #region Set up polling

                try
                {
                    // Switch on pollstyle
                    switch (this.pollStyle)
                    {
                            #region PollStyle.Continous

                        case PollStyle.Continous:
                            if (configNode.Attributes != null)
                            {
                                // It's by minutes, not seconds. So we multiply with 60 and then with 1000
                                this.interval = Convert.ToInt32(configNode.Attributes["Interval"].Value)*60*1000;
                            }

                            var folderNodeList = configNode.SelectNodes("../NIFSingleSportComponent[@Name='" + this.InstanceName + "']/Folders/Folder");

                            var folderJobs = 0;
                            if (folderNodeList != null)
                            {
                                folderJobs = folderNodeList.Count;
                            }

                            // <Folder Type="XMLOutputFolder" Mode="FILE" Results="True" PushSchedule="False">C:\Utvikling\norm\xml\NTBSportsData</Folder>
                            // string OutputFolder = "";
                            Logger.Debug("Number of Folders: " + folderJobs);
                            if (folderNodeList != null)
                            {
                                foreach (XmlNode foldernode in folderNodeList)
                                {
                                    if (foldernode.Attributes != null && foldernode.Attributes["Result"] != null)
                                    {
                                        this.pushResults = Convert.ToBoolean(foldernode.Attributes["Result"].Value);
                                    }

                                    if (foldernode.Attributes != null && foldernode.Attributes["PushSchedule"] != null)
                                    {
                                        this.pushSchedule = Convert.ToBoolean(foldernode.Attributes["PushSchedule"].Value);
                                    }

                                    this.fileOutputFolder.Add(foldernode.InnerText);
                                }
                            }

                            if (this.interval == 0)
                            {
                                this.interval = 6000; // Setting the interval to one minute
                            }

                            this.pollTimer.Interval = this.interval; // short startup - interval * 1000;
                            Logger.DebugFormat("Poll interval: {0} seconds", this.interval);

                            break;

                            #endregion

                            #region PollStyle.SynchDistributor

                            // We are checking if there are new results before we go after them.
                        case PollStyle.SynchDistributor:

                            // We need to get the Federation Key 
                            var applicationKey = ConfigurationManager.AppSettings["NIFServiceKey"];
                            this.federationUserId = ConfigurationManager.AppSettings["NIFServiceUsername"].Replace(applicationKey, this.federationKey.ToString());
                            break;

                            #endregion

                            #region PollStyle.CalendarPoll

                        case PollStyle.CalendarPoll:

                            // Getting the times for this job
                            try
                            {
                                IJobDetail nifWeeklyJobDetail = null;

                                // = JobBuilder.Create<TournamentMatches>().WithIdentity("Job1", "Group1").Build();
                                ITrigger nifWeeklyTrigger = null;

                                Logger.Info("Setting up schedulers");
                                Logger.Info("--------------------------------------------------------------");

                                // Getting the number of jobs to create
                                var nodeList = configNode.SelectNodes("../NIFSingleSportComponent[@Name='" + this.InstanceName + "']/Schedules/Schedule");

                                var jobs = 0;
                                if (nodeList != null)
                                {
                                    jobs = nodeList.Count;
                                }

                                Logger.Debug("Number of jobs: " + jobs);

                                // Creating a string to store the ScheduleID
                                string scheduleId;

                                // Creating a boolean variable to store if we are to get results out or not
                                var results = false;

                                // Creating an integer value to be used to tell the duration of the gathering of data
                                var duration = 0;

                                // Creating a boolean variable to store if this is a schedule-message og not
                                var scheduleMessage = false;

                                // Creating integer for offset
                                // This variable is used to tell that we are to get tomorrows matches or yesterdays matches
                                // Used to create the 'Todays matches' for newspapers
                                // So a positive number is most commonly used
                                var dateTimeOffset = DateTime.Today;

                                Logger.Debug("Schedule of type: " + this.scheduleType);

                                // Getting the scheduler
                                var scheduler1 = this.schedulerFactory.GetScheduler();

                                switch (this.scheduleType)
                                {
                                        #region ScheduleType.Daily

                                    case ScheduleType.Daily:
                                        if (nodeList != null)
                                        {
                                            foreach (XmlNode node in nodeList)
                                            {
                                                scheduleId = node.Attributes["Id"].Value;

                                                // Creating the offset time 
                                                var offset = 0;
                                                if (node.Attributes["Offset"] != null)
                                                {
                                                    offset = Convert.ToInt32(node.Attributes["Offset"].Value);
                                                }

                                                // If the value is larger or less than 0
                                                if (offset > 0 || offset < 0)
                                                {
                                                    if (offset > 0)
                                                    {
                                                        dateTimeOffset = DateTime.Today.AddDays(offset);
                                                    }
                                                    else if (offset < 0)
                                                    {
                                                        var fromDays = TimeSpan.FromDays(offset);
                                                        dateTimeOffset = DateTime.Today.Subtract(fromDays);
                                                    }
                                                }

                                                // Splitting the time into two values so that we can create a cron job
                                                var scheduleTimeArray = node.Attributes["Time"].Value.Split(':');

                                                // If minutes contains two zeros (00), we change it to one zero (0) 
                                                // If not, scheduler won't understand
                                                if (scheduleTimeArray[1] == "00")
                                                {
                                                    scheduleTimeArray[1] = "0";
                                                }

                                                // Doing the same thing for hours
                                                if (scheduleTimeArray[0] == "00")
                                                {
                                                    scheduleTimeArray[0] = "0";
                                                }

                                                // Checking if there is a setting for results
                                                if (node.Attributes["Results"] != null)
                                                {
                                                    results = Convert.ToBoolean(node.Attributes["Results"].Value);
                                                    scheduleMessage = !results;
                                                }

                                                // Checking if there is a setting for Duration
                                                if (node.Attributes["Duration"] != null)
                                                {
                                                    duration = Convert.ToInt32(node.Attributes["Duration"].Value);
                                                }

                                                /*
                                             * Creating cron expressions and more. 
                                             * This is the cron syntax:
                                             *  Seconds
                                             *  Minutes
                                             *  Hours
                                             *  Day-of-Month
                                             *  Month
                                             *  Day-of-Week
                                             *  Year (optional field)
                                             */

                                                // Creating the daily cronexpression
                                                var stringCronExpression = "0 " + scheduleTimeArray[1] + " " + scheduleTimeArray[0] + " " + "? " + "* " + "* ";

                                                // Setting up the CronTrigger
                                                Logger.Debug("Setting up the CronTrigger with following pattern: " + stringCronExpression);

                                                // Setting up the Daily CronTrigger
                                                // new CronExpression(stringCronExpression);

                                                // dailyCronTrigger = new CronTrigger(InstanceName + ScheduleID, InstanceName);
                                                // dailyCronTrigger.CronExpression = cronExpression;

                                                // Creating the daily Cron Trigger
                                                var nifDailyCronTrigger = TriggerBuilder.Create()
                                                    .WithIdentity(this.InstanceName + "_" + scheduleId, "GroupNIF")
                                                    .WithDescription(this.InstanceName)
                                                    .WithCronSchedule(stringCronExpression)
                                                    .Build();

                                                Logger.Debug("dailyCronTrigger: " + nifDailyCronTrigger.Description);

                                                // Creating the jobDetail 
                                                var nifDailyJobDetail = JobBuilder.Create<NifJobBuilder>()
                                                    .WithIdentity("job_" + this.InstanceName + "_" + scheduleId, "GroupNIF")
                                                    .WithDescription(this.InstanceName)
                                                    .Build();

                                                if (this.fileOutputFolder != null)
                                                {
                                                    nifDailyJobDetail.JobDataMap["OutputFolder"] = this.fileOutputFolder;
                                                }

                                                nifDailyJobDetail.JobDataMap["InsertDatabase"] = this.populateDatabase;
                                                nifDailyJobDetail.JobDataMap["ScheduleType"] = this.scheduleType.ToString();
                                                nifDailyJobDetail.JobDataMap["Results"] = results;
                                                nifDailyJobDetail.JobDataMap["ScheduleMessage"] = scheduleMessage;
                                                nifDailyJobDetail.JobDataMap["Duration"] = duration;
                                                nifDailyJobDetail.JobDataMap["Purge"] = this.purge;
                                                nifDailyJobDetail.JobDataMap["CreateXML"] = this.createXml;
                                                nifDailyJobDetail.JobDataMap["DateOffset"] = dateTimeOffset;
                                                nifDailyJobDetail.JobDataMap["SportId"] = this.sportId;
                                                nifDailyJobDetail.JobDataMap["FederationId"] = this.federationId;

                                                if (nifDailyJobDetail != null)
                                                {
                                                    // dailyJobDetail.AddJobListener(JobListenerName);
                                                    Logger.Debug("Setting up and starting dailyJobDetail job " + nifDailyJobDetail.Description + " using trigger : " + nifDailyCronTrigger.Description);

                                                    // scheduler.ScheduleJob(dailyJobDetail, dailyCronTrigger[a]);
                                                    scheduler1.ScheduleJob(nifDailyJobDetail, nifDailyCronTrigger);
                                                }
                                            }
                                        }

                                        break;

                                        #endregion

                                        #region ScheduleType.Weekly

                                    case ScheduleType.Weekly:
                                        if (nodeList != null)
                                        {
                                            foreach (XmlNode node in nodeList)
                                            {
                                                scheduleId = node.Attributes["Id"].Value;

                                                // Splitting the time into two values so that we can create a cron job
                                                var scheduleTimeArray = node.Attributes["Time"].Value.Split(':');

                                                // If minutes contains two zeros (00), we change it to one zero (0) 
                                                // If not, scheduler won't understand
                                                if (scheduleTimeArray[1] == "00")
                                                {
                                                    scheduleTimeArray[1] = "0";
                                                }

                                                var minutes = Convert.ToInt32(scheduleTimeArray[1]);

                                                // Doing the same thing for hours
                                                if (scheduleTimeArray[0] == "00")
                                                {
                                                    scheduleTimeArray[0] = "0";
                                                }

                                                var hours = Convert.ToInt32(scheduleTimeArray[0]);

                                                // Checking if there is a setting for results
                                                if (node.Attributes["Results"] != null)
                                                {
                                                    results = Convert.ToBoolean(node.Attributes["Results"].Value);
                                                    scheduleMessage = !results;
                                                }

                                                // Checking if there is a setting for Duration
                                                if (node.Attributes["Duration"] != null)
                                                {
                                                    duration = Convert.ToInt32(node.Attributes["Duration"].Value);
                                                }

                                                var scheduleWeek = string.Empty;
                                                if (node.Attributes["Week"] != null)
                                                {
                                                    scheduleWeek = node.Attributes["Week"].Value;
                                                }

                                                var scheduleDay = string.Empty;
                                                if (node.Attributes["DayOfWeek"] != null)
                                                {
                                                    scheduleDay = node.Attributes["DayOfWeek"].Value;
                                                }

                                                // If we use the DateTimeOffset it means that we have to find out if the day has passed or not.
                                                // So we have to find out which day it is today
                                                var today = DateTime.Today.DayOfWeek;
                                                var numDayOfWeek = 0;
                                                switch (today.ToString())
                                                {
                                                    case "Monday":
                                                        numDayOfWeek = 1;
                                                        break;
                                                    case "Tuesday":
                                                        numDayOfWeek = 2;
                                                        break;

                                                    case "Wednesday":
                                                        numDayOfWeek = 3;
                                                        break;

                                                    case "Thursday":
                                                        numDayOfWeek = 4;
                                                        break;

                                                    case "Friday":
                                                        numDayOfWeek = 5;
                                                        break;

                                                    case "Saturday":
                                                        numDayOfWeek = 6;
                                                        break;

                                                    case "Sunday":
                                                        numDayOfWeek = 0;
                                                        break;
                                                }

                                                // Checking if numDayOfWeek is smaller than ScheduleDay
                                                DateTimeOffset dto = DateTime.Today;

                                                // Get the scheduled day
                                                var scheduledDay = Convert.ToInt32(scheduleDay);

                                                int days;
                                                if (numDayOfWeek <= scheduledDay)
                                                {
                                                    // It is, so we need to find out when the next scheduling should happen
                                                    days = scheduledDay - numDayOfWeek; // this can be zero

                                                    dto = DateTime.Today.AddDays(days)
                                                        .AddHours(hours)
                                                        .AddMinutes(minutes)
                                                        .ToUniversalTime();

                                                    // <Schedule Id="1" Time="15:12" DayOfWeek="1" Week="2"/>
                                                }
                                                else if (numDayOfWeek > scheduledDay)
                                                {
                                                    const int Weekdays = 7;
                                                    var daysLeft = Weekdays - numDayOfWeek;
                                                    days = daysLeft + scheduledDay;

                                                    dto = DateTime.Today.AddDays(days)
                                                        .AddHours(hours)
                                                        .AddMinutes(minutes)
                                                        .ToUniversalTime();
                                                }

                                                // Creating a simple Scheduler
                                                var calendarIntervalSchedule = CalendarIntervalScheduleBuilder.Create();

                                                // Creating the weekly Trigger using the stuff that we've calculated
                                                nifWeeklyTrigger = TriggerBuilder.Create()
                                                    .WithDescription(this.InstanceName)
                                                    .WithIdentity(this.InstanceName + "_" + scheduleId, "GroupNIF")
                                                    .StartAt(dto)
                                                    .WithSchedule(calendarIntervalSchedule.WithIntervalInWeeks(Convert.ToInt32(scheduleWeek)))
                                                    .Build();

                                                // This part might be moved
                                                if (OperationMode.Gatherer == this.OperationMode)
                                                {
                                                    nifWeeklyJobDetail = JobBuilder.Create<NifJobBuilder>()
                                                        .WithIdentity("job_" + this.InstanceName + "_" + scheduleId, "GroupNIF")
                                                        .WithDescription(this.InstanceName)
                                                        .Build();
                                                }
                                                else if (this.OperationMode == OperationMode.Purge)
                                                {
                                                    nifWeeklyJobDetail = JobBuilder.Create<PurgeSportsData>()
                                                        .WithIdentity("job_" + this.InstanceName + "_" + scheduleId, "GroupNIF")
                                                        .WithDescription(this.InstanceName)
                                                        .Build();
                                                }
                                                else if (this.OperationMode == OperationMode.Distributor)
                                                {
                                                }
                                                else if (this.OperationMode == OperationMode.Hold)
                                                {
                                                }

                                                if (this.fileOutputFolder != null || (!this.fileOutputFolder.Any()))
                                                {
                                                    nifWeeklyJobDetail.JobDataMap["OutputFolder"] = this.fileOutputFolder;
                                                }

                                                nifWeeklyJobDetail.JobDataMap["InsertDatabase"] = this.populateDatabase;
                                                nifWeeklyJobDetail.JobDataMap["ScheduleType"] = this.scheduleType.ToString();
                                                nifWeeklyJobDetail.JobDataMap["Results"] = results;
                                                nifWeeklyJobDetail.JobDataMap["ScheduleMessage"] = scheduleMessage;
                                                nifWeeklyJobDetail.JobDataMap["Duration"] = duration;
                                                nifWeeklyJobDetail.JobDataMap["Purge"] = this.purge;
                                                nifWeeklyJobDetail.JobDataMap["CreateXML"] = this.createXml;
                                                nifWeeklyJobDetail.JobDataMap["SportId"] = this.sportId;
                                                nifWeeklyJobDetail.JobDataMap["FederationId"] = this.federationId;
                                            }
                                        }

                                        if (nifWeeklyJobDetail != null)
                                        {
                                            Logger.Debug("Setting up and starting weeklyJobDetail job " + nifWeeklyJobDetail.Description + " using trigger : " + nifWeeklyTrigger.Description);
                                            scheduler1.ScheduleJob(nifWeeklyJobDetail, nifWeeklyTrigger);
                                        }

                                        break;

                                        #endregion

                                        // case ScheduleType.Monthly:
                                    // // doing Monthly stuff here
                                    // throw new NotSupportedException("Monthly scheduletype is not implemented!");
                                    // break;

                                    // case ScheduleType.Yearly:
                                    // // doing yearly Stuff here
                                    // throw new NotSupportedException("Yearly scheduletype is not implemented!");
                                    // break;
                                }

                                // We might move the code regarding jobdetails here... 

                                // Configuring the datafile-object so that we know that the databases are up and running

                                // We don't have to check for datafile stuff when we only do purge...
                                if (this.OperationMode != OperationMode.Purge)
                                {
                                    var dataFileParams = new DataFileParams
                                    {
                                        CreateXml = this.createXml,
                                        FederationId = this.federationId,
                                        FileOutputFolder = this.fileOutputFolder,
                                        PopulateDatabase = this.populateDatabase,
                                        PushResults = this.pushResults,
                                        PushSchedule = this.pushSchedule,
                                        SportId = this.sportId
                                    };
                                    var datafile = new NifSingleSportDataFileCreator {DataFileParams = dataFileParams};

                                    if (datafile.Configure() == false)
                                    {
                                        throw new Exception("Problems initiating database tables");
                                    }
                                }

                                // Catching exceptions that might occur
                            }
                            catch (SchedulerConfigException sce)
                            {
                                Logger.Debug("A SchedulerConfigException has occured: ", sce);
                            }
                            catch (SchedulerException se)
                            {
                                Logger.Debug("A schedulerException has occured: ", se);
                            }
                            catch (Exception e)
                            {
                                Logger.Debug("An exception has occured", e);
                            }

                            break;

                            #endregion

                            #region PollStyle.Scheduled

                        case PollStyle.Scheduled:

                            /* 
                            schedule = configNode.Attributes["Schedule"].Value;
                            TimeSpan s = Utilities.GetScheduleInterval(schedule);
                            logger.DebugFormat("Schedule: {0} Calculated time to next run: {1}", schedule, s);
                            pollTimer.Interval = s.TotalMilliseconds;
                            */
                            throw new NotSupportedException("Invalid polling style for this job type");

                            #endregion

                            #region PollStyle.FileSystemWatch

                        case PollStyle.FileSystemWatch:

                            // Check for config overrides
                            /*
                            if (configNode.Attributes.GetNamedItem("BufferSize") != null)
                                bufferSize = Convert.ToInt32(configNode.Attributes["BufferSize"].Value);
                            logger.DebugFormat("FileSystemWatcher buffer size: {0}", bufferSize);

                            //Set values
                            filesWatcher.Path = fileInputFolder;
                            filesWatcher.Filter = fileFilter;
                            filesWatcher.IncludeSubdirectories = includeSubdirs;
                            filesWatcher.InternalBufferSize = bufferSize;

                            //Do not start the event watcher here, wait until after the startup cleaning job
                            pollTimer.Interval = 5000; // short startup;
                            */
                            throw new NotSupportedException("Invalid polling style for this job type");

                            #endregion

                        default:

                            // Unsupported pollstyle for this object
                            throw new NotSupportedException("Invalid polling style for this job type (" + this.pollStyle + ")");
                    }
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing PollStyle-specific values in XML configuration node: " + ex.Message, ex);
                }

                #endregion
            }

            // Finish configuration
            ThreadContext.Stacks["NDC"].Pop();
            this.configured = true;
        }

        /// <summary>
        /// The get component state.
        /// </summary>
        /// <returns>
        /// The <see cref="ComponentState"/>.
        /// </returns>
        public ComponentState GetComponentState(bool componentState)
        {
            return ComponentState.Running;
        }

        /// <summary>
        ///     The start.
        /// </summary>
        public void Start()
        {
            ServicePointManager.ServerCertificateValidationCallback = ValidateServerCertificate;
            var startDate = DateTime.Today.Date;
            var endDate = DateTime.Today.Date;

            if (!this.configured)
            {
                throw new SportsDataException("NIFWSComponent is not properly Configured. NIFWSComponent::Start() Aborted!");
            }

            if (!this._Enabled)
            {
                throw new SportsDataException("NIFWSComponent is not Enabled. NIFWSComponent::Start() Aborted!");
            }

            if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
            {
                startDate = Convert.ToDateTime(ConfigurationManager.AppSettings["niftestingdate"]);
                endDate = Convert.ToDateTime(ConfigurationManager.AppSettings["niftestingdate"]);
            }

            var dataFileParam = new DataFileParams
            {
                CreateXml = this.createXml,
                Results = true,
                FileOutputFolder = this.fileOutputFolder,
                PopulateDatabase = this.populateDatabase,
                PushResults = this.pushResults,
                PushSchedule = this.pushSchedule,
                SportId = this.sportId,
                FederationId = this.federationId,
                DateEnd = endDate,
                DateStart = startDate
            };

            // I want the service to populate todays sports on a restart
            var dataFileCreator = new NifSingleSportDataFileCreator {DataFileParams = dataFileParam};

            dataFileCreator.Get();

            try
            {
                // We are to check if there are any results today 
                // And if there are, we shall check when we downloaded them last
                // And if there are updated results, we shall download them

                // Consider making this a method so we can use it under timer_elapsed also. I guess it is more or less the same procedure

                // todo: We need to go over this code as it does not work in the current state

                /*
                 * 1) We are getting a list of results from - maybe - many events
                 * 2) We must check if the list of result is done or not. 
                 * 3) we must create the XML
                 */
                if (this.pollStyle == PollStyle.SynchDistributor)
                {
                    Logger.Info("Getting list of events");

                    // This will actually return a list of results
                    var eventList = dataFileCreator.GetEventList(this.federationUserId);

                    var sportList = new List<SportEvent>();

                    // Find out if the events are in the database
                    foreach (var sportEvent in eventList)
                    {
                        var localSportEvent = dataFileCreator.IsEventInDatabase(sportEvent.EventId);

                        if (localSportEvent == null)
                        {
                            // We are inserting
                            // We should get more information about the event.
                            var updatedSportEvent = dataFileCreator.GetEventInformation(sportEvent);

                            // Insert into database
                            dataFileCreator.InsertOne(updatedSportEvent);

                            sportList.Add(updatedSportEvent);
                        }
                        else
                        {
                            // Check if data has been changed
                            if (dataFileCreator.IsEventChanged(sportEvent, localSportEvent))
                            {
                                // Get the information we need
                                var updatedSportEvent = dataFileCreator.GetEventInformation(sportEvent);

                                // We must update
                                dataFileCreator.UpdateOne(updatedSportEvent);

                                sportList.Add(updatedSportEvent);
                            }
                        }
                    }

                    // Now we shall filter only todays events
                    var todaysEvents = from s in sportList where s.EventDateStart != null && s.EventDateStart.Value.Date == DateTime.Today.Date select s;

                    if (!todaysEvents.Any())
                    {
                        return;
                    }

                    // Creating the list of events
                    // dataFileCreator.CreateResult(todaysEvents.ToList());
                }

                if (this.pollStyle == PollStyle.CalendarPoll)
                {
                    Logger.Info("Setting up jobListener and scheduler");

                    // starting the scheduler
                    var nifScheduler = this.schedulerFactory.GetScheduler();

                    var jobGroupNames = nifScheduler.GetJobGroupNames();

                    foreach (var jobGroupName in jobGroupNames)
                    {
                        Logger.Debug("JobGroupName: " + jobGroupName);
                        if (jobGroupName == "GroupNIF")
                        {
                            Logger.Debug("Key: " + nifScheduler.GetJobKeys(GroupMatcher<JobKey>.GroupContains(jobGroupName)));

                            var groupMatcher = GroupMatcher<JobKey>.GroupContains(jobGroupName);
                            var jobKeys = nifScheduler.GetJobKeys(groupMatcher);

                            foreach (var jobKey in jobKeys)
                            {
                                Logger.Debug("Name: " + jobKey.Name);
                                Logger.Debug("Group: " + jobKey.Group);
                            }
                        }
                    }

                    var triggerGroupNames = nifScheduler.GetTriggerGroupNames();

                    foreach (var triggerGroupName in triggerGroupNames)
                    {
                        Logger.Debug("TriggerGroupName: " + triggerGroupName);
                    }

                    var listOfJobs = nifScheduler.GetJobGroupNames();
                    foreach (var job in listOfJobs)
                    {
                        Logger.Debug("Job: " + job);
                    }

                    Logger.Debug("This Component name: " + this.name);

                    // Starting scheduler
                    nifScheduler.Start();
                }
            }
            catch (SchedulerException se)
            {
                Logger.Debug(se);
            }

            this.stopEvent.Reset();
            this.busyEvent.Set();

            // Starting poll-timer
            // pollTimer.Interval = 2000;
            this.pollTimer.Enabled = true;
            this.pollTimer.Start();
        }

        /// <summary>
        ///     The stop.
        /// </summary>
        public void Stop()
        {
            if (!this.configured)
            {
                throw new SportsDataException(this.name + " is not properly Configured. " + this.name + "::Stop() Aborted");
            }

            if (!this._Enabled)
            {
                throw new SportsDataException(this.name + " is not properly Configured. " + this.name + "::Stop() Aborted");
            }

            // Check status - Handle busy polltimer loops
            if (this.pollStyle == PollStyle.CalendarPoll)
            {
                try
                {
                    Logger.Debug("Deleting / stopping jobs!");
                    ISchedulerFactory sf = new StdSchedulerFactory();
                    var sched = sf.GetScheduler();

                    // Shutting down scheduler
                    sched.GetJobGroupNames();

                    var keys = new List<JobKey>(sched.GetJobKeys(GroupMatcher<JobKey>.GroupEquals(this.InstanceName)));

                    // Looping through each key as we'd like to know when things go right and not!
                    keys.ForEach(
                        key =>
                        {
                            var detail = sched.GetJobDetail(key);
                            sched.DeleteJob(key);

                            Logger.Debug("Shutting down: " + detail.Description + "(" + key.Name + ")" + " Group: " + key.Group);
                        });

                    sched.Shutdown();
                }
                catch (SchedulerException sex)
                {
                    Logger.Fatal(sex);
                }
                catch (Exception ex)
                {
                    Logger.Error("Problems closing NIF WebClients!", ex);
                }
            }
        }

        /// <summary>
        ///     Gets or sets the component state.
        /// </summary>
        public ComponentState ComponentState { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether database populated.
        /// </summary>
        public bool DatabasePopulated { get; set; }

        /// <summary>
        /// Static logger
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="certificate">
        /// The certificate.
        /// </param>
        /// <param name="chain">
        /// The chain.
        /// </param>
        /// <param name="sslPolicyErrors">
        /// The ssl Policy Errors.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        /// <summary>
        /// The poll timer_ elapsed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void PollTimerElapsed(object sender, ElapsedEventArgs e)
        {
            /*
             * We can consider adding getting data here if the time (day + hour + minute) hits. Otherwize we shall do nothing.
             * 
             */
            ThreadContext.Properties["JOBNAME"] = this.InstanceName;

            Logger.Debug("NIFGathererComponent::pollTimer_Elapsed() hit");
            Logger.Debug("We have the following settings: Interval: " + this.pollTimer.Interval);

            var dataFileParam = new DataFileParams();

            var dataFileCreator = new NifSingleSportDataFileCreator();

            var events = new List<SportEvent>();

            // _busyEvent.WaitOne();
            this.pollTimer.Stop();

            switch (this.pollStyle)
            {
                // For continuous and scheduled do a simple folder item traversal
                case PollStyle.Scheduled:
                case PollStyle.Continous:

                    // distributor mode: when the system is checking against database and sends out the data
                    if (this.OperationMode == OperationMode.Distributor)
                    {
                        dataFileParam = new DataFileParams
                        {
                            CreateXml = this.createXml,
                            Results = true,
                            FileOutputFolder = this.fileOutputFolder,
                            PopulateDatabase = this.populateDatabase,
                            PushResults = this.pushResults,
                            PushSchedule = this.pushSchedule,
                            SportId = this.sportId,
                            FederationId = this.federationId,
                            DateStart = DateTime.Today,
                            DateEnd = DateTime.Today
                        };

                        if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                        {
                            dataFileParam.DateStart = DateTime.Parse(ConfigurationManager.AppSettings["niftestingdate"]);
                            dataFileParam.DateEnd = DateTime.Parse(ConfigurationManager.AppSettings["niftestingdate"]);
                        }

                        dataFileCreator = new NifSingleSportDataFileCreator {DataFileParams = dataFileParam};

                        events = dataFileCreator.Get();

                        var remoteEvents = dataFileCreator.GetRemoteByOrgId(dataFileParam.DateStart, dataFileParam.DateEnd, this.federationId);

                        if (!events.Any() || events == null)
                        {
                            events = remoteEvents;
                        }

                        var filteredEvents = dataFileCreator.Find(remoteEvents, events);
                        if (filteredEvents.Any())
                        {
                            dataFileCreator.InsertAll(filteredEvents);

                            // We add the new events we have found
                            events.AddRange(filteredEvents);
                        }

                        // dataFileCreator.CreateResult(events);
                    }

                    // Gatherer mode: when you get data from the information source and store it in the database
                    if (this.OperationMode == OperationMode.Gatherer)
                    {
                        dataFileParam = new DataFileParams
                        {
                            CreateXml = false,
                            Results = false,
                            FileOutputFolder = this.fileOutputFolder,
                            PopulateDatabase = this.populateDatabase,
                            PushResults = this.pushResults,
                            PushSchedule = this.pushSchedule,
                            SportId = this.sportId,
                            FederationId = this.federationId,
                            DateEnd = DateTime.Today,
                            DateStart = DateTime.Today
                        };

                        if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                        {
                            dataFileParam.DateStart = DateTime.Parse(ConfigurationManager.AppSettings["niftestingdate"]);
                            dataFileParam.DateEnd = DateTime.Parse(ConfigurationManager.AppSettings["niftestingdate"]);
                        }

                        dataFileCreator = new NifSingleSportDataFileCreator {DataFileParams = dataFileParam};

                        events = dataFileCreator.GetRemoteByOrgId(dataFileParam.DateStart, dataFileParam.DateEnd, this.federationId);

                        dataFileCreator.InsertAll(events);
                    }

                    if (this.OperationMode == OperationMode.SynchDistributor)
                    {
                        dataFileParam = new DataFileParams
                        {
                            CreateXml = this.createXml,
                            Results = true,
                            FileOutputFolder = this.fileOutputFolder,
                            PopulateDatabase = this.populateDatabase,
                            PushResults = this.pushResults,
                            PushSchedule = this.pushSchedule,
                            SportId = this.sportId,
                            FederationId = this.federationId,
                            DateStart = DateTime.Now.AddHours(-1),
                            DateEnd = DateTime.Now
                        };

                        if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                        {
                            dataFileParam.DateStart = DateTime.Parse(ConfigurationManager.AppSettings["niftestingdate"])
                                .AddHours(13);
                            dataFileParam.DateEnd = DateTime.Parse(ConfigurationManager.AppSettings["niftestingdate"])
                                .AddHours(14);
                        }

                        dataFileCreator = new NifSingleSportDataFileCreator {DataFileParams = dataFileParam};

                        // events = dataFileCreator.GetRemoteByOrgId(dataFileParam.DateStart, dataFileParam.DateEnd, this.FederationId);
                        var sportEvent = dataFileCreator.GetTodaysResults(this.federationKey);

                        if (sportEvent == null)
                        {
                            return;
                        }

                        // If we have received a list of events from the federation, we must go over these
                        if (!sportEvent.Keys.Any())
                        {
                            return;
                        }

                        foreach (var spEvent in sportEvent.Keys)
                        {
                            dataFileCreator.InsertOne(spEvent);
                        }

                        if (sportEvent.Values.Any())
                        {
                            // This is where we shall create the XML for this result
                            dataFileCreator.CreateEventResult(sportEvent);
                        }

                        // dataFileCreator.CreateResult(events);
                        // We need to check the service state
                        // this.pollTimer.Start();
                    }

                    this.pollTimer.Start();

                    break;

                case PollStyle.CalendarPoll:

                    // @todo: need to do some work here
                    Logger.Debug("Disabling pollTimer");
                    this.pollTimer.Enabled = false;

                    // _busyEvent.WaitOne();
                    break;

                /*
             * M� legge inn i databasen om denne turneringen er push eller pull enablet.
             * Sjekke om vi skal bruke den andre l�sningen for push...
             * Anders g�r over om dagens nye l�sning er 1:1 gammel l�sning.
             */
            }
        }
    }
}