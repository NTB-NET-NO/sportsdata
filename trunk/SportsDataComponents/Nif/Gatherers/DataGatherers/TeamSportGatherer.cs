﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TeamSportGatherer.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System.Configuration;
using NTB.SportsData.Components.Nif.Facade.Interfaces;

namespace NTB.SportsData.Components.Nif.Gatherers.DataGatherers
{
    using System;
    using System.Collections.Generic;

    using Classes.Classes;
    using Facade;
    using Interfaces;

    /// <summary>
    ///     The team sport gatherer.
    /// </summary>
    public class TeamSportGatherer : ITeamSportGatherer
    {
        /// <summary>
        /// Gets or sets the match date start.
        /// </summary>
        public DateTime MatchDateStart { get; set; }

        /// <summary>
        /// Gets or sets the match date end.
        /// </summary>
        public DateTime MatchDateEnd { get; set; }

        /// <summary>
        /// The facade.
        /// </summary>
        private readonly ITeamSportFacade _facade;

        public TeamSportGatherer()
        {
             this._facade = new TeamSportFacade();
        }

        public List<Match> GetTournamentMaintenanceMatchesByTournamentIdAndDate(int tournamentId, DateTime matchStartDate, DateTime matchEndDate)
        {

            return this._facade.SearchMatchesByDateAndTournamentId(tournamentId, matchStartDate, matchEndDate);
        }

        public Tournament GetTournamentByMatchId(int matchId)
        {
            return this._facade.GetTournamentByMatchId(matchId);
        }
        public List<Match> GetTournamentMatchesByTournamentIdAndDate(int tournamentId, DateTime? matchDate)
        {
            var dateStart = this.SetDate();
            var dateEnd = this.SetDate();

            if (matchDate == null)
            {
                return this._facade.SearchMatchesByDateAndTournamentId(tournamentId, dateStart, dateEnd);
            }

            dateStart = matchDate.Value;
            dateEnd = matchDate.Value;
            return this._facade.SearchMatchesByDateAndTournamentId(tournamentId, dateStart, dateEnd);
        }

        /// <summary>
        /// The get match referees by match id.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public List<Referee> GetMatchRefereesByMatchId(int matchId)
        {
            return this._facade.GetMatchReferees(matchId);
        }

        /// <summary>
        /// The get match players by match id.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public List<Player> GetMatchPlayersByMatchId(int matchId)
        {
            return this._facade.GetMatchPlayersByMatchId(matchId);
        }

        /// <summary>
        /// The get match incidents by match id.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<MatchEvent> GetMatchIncidentsByMatchId(int matchId)
        {
            return this._facade.GetMatchIncidentByMatchId(matchId);
        }

        public List<ChangeInfo> GetChangesResultTeam(double interval, string userKey, string clientPassword, DateTime eventStartDate, DateTime eventEndDate)
        {
            return this._facade.GetChangesTeamSport(userKey, clientPassword, eventStartDate, eventEndDate);
        }

        public Result GetMatchResultByResultId(int resultId)
        {
            return this._facade.GetMatchResultByResultId(resultId);
        }

        public Match GetMatchInfoByMatchId(int matchId)
        {
            return this._facade.GetMatchInfoByMatchId(matchId);
        }

        /// <summary>
        /// The get match result by match id.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        /// <returns>
        /// The <see cref="Result"/>.
        /// </returns>
        //public Result GetMatchResultByMatchId(int matchId)
        //{
        //    return this._facade.GetMatchResultByMatchId(matchId);
        //}

        /// <summary>
        /// The get tournament standing by tournament id.
        /// </summary>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<TeamResult> GetTournamentStandingByTournamentId(Tournament tournament)
        {
            //if (tournament.TypeId != 1)
            //{
            //    return null;
            //}

            try
            {
                if (tournament.TournamentTypeId < 2)
                {
                    return this._facade.GetTournamentStandingByTournamentId(tournament.Id);
                }

                return null;
            }
            catch (Exception exception)
            {

                return null;
            }
        }

        public List<int> GetUniqueTournaments(int tournamentId, List<int> tournamentIds)
        {
            if (!tournamentIds.Contains(tournamentId))
            {
                tournamentIds.Add(tournamentId);
            }
            
            return tournamentIds;
        }

        public Tournament GetTournamentById(int id)
        {
            return this._facade.GetTournamentById(id);
        }

        public List<PartialResult> GetPartialResultsForMatch(int matchId)
        {
            return this._facade.GetPartialResultsByMatchId(matchId);
        }

        public List<AgeCategory> GetTournamentAgeCategory(int tournamentId)
        {
            return this._facade.GetTournamentAgeCategory(tournamentId);
        }

        /// <summary>
        /// The set date.
        /// </summary>
        /// <returns>
        /// The <see cref="DateTime"/>.
        /// </returns>
        public DateTime SetDate()
        {
            var dateTime = DateTime.Today;
            var testing = Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]);
            if (testing)
            {
                dateTime = DateTime.Parse(ConfigurationManager.AppSettings["niftestingdate"]);
            }

            return dateTime;
        }


        public List<ChangeInfo> GetTodaysChangesResultTeam(string userKey, string clientPassword, DateTime eventStartDate, DateTime eventEndDate)
        {
            return this._facade.GetChangesTeamSport(userKey, clientPassword, eventStartDate, eventEndDate);
        }


        public MatchExtended GetMatchInfoByResultId(int resultId)
        {
            return this._facade.GetMatchInfoByResultId(resultId);
        }
    }
}