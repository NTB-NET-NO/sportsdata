﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITeamSportGatherer.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace NTB.SportsData.Components.Nif.Gatherers.Interfaces
{
    using System.Collections.Generic;

    using Classes.Classes;

    /// <summary>
    /// The TeamSportGatherer interface.
    /// </summary>
    /// <remarks>
    /// The way you have to query the remote data store you have to do the following
    /// 1) Call GetChangesResultTeam to see if there are any changes
    /// 2) Call GetMatchResultByResultId to most of all get the MatchId (ResultTeamService.GetMatchResultByResultId)
    /// 3) Call GetMatchInfoByMatchId to get the information about the match, but most importantly to get the Tournament Id (ResultTeamService.GetMatchInfo)
    /// 
    /// This where the getting really starts
    /// 4) Call GetTournamentMatchesByTournamentId to get all matches for today (TournamentService.GetTournamentMatches)
    /// 4.1) In case we get many changes, we will filter the list of tournaments so we don't call for matches from the same tournament... this to reduce api-requests.
    /// 5) Two options here:
    /// 5.1) Loop: GetMatchResultByMatchId to get the results based on the match Id (ResultTeamService.GetMatchResult)
    /// 5.2) Call GetMatchResultsForTournament which will get all the results for this tourmanent. We can use that one to filter against the list of matches (ResultTeamService.GetMatchResultsForTournament)
    ///  
    /// Because we want to get the referees, players and incidents, we are getting this also
    /// 6) Call GetMatchRefereesByMatchId to get the referees (ResultTeamService.GetMatchReferees)
    /// 7) Call GetMatchPlayersByMatchId to get the players (MatchService.GetMatchLineUps)
    /// 8) Call GetMatchIncidentsByMatchId to get the match incidents... (ResultTeamService.GetMatchIncidents)
    /// 
    /// Now we can get the standing
    /// 9) Call GetTournamentStandingByTournamentId (TournamentService.GetTournamentResults)
    /// </remarks>
    public interface ITeamSportGatherer
    {
        List<ChangeInfo> GetChangesResultTeam(double interval, string userKey, string clientPassword,
            DateTime eventStartDate, DateTime eventEndDate);

        Result GetMatchResultByResultId(int resultId);

        Match GetMatchInfoByMatchId(int matchId);

        // Result GetMatchResultByMatchId(int matchId);

        List<Match> GetTournamentMatchesByTournamentIdAndDate(int tournamentId, DateTime? matchDate);

        List<Referee> GetMatchRefereesByMatchId(int matchId);

        List<Player> GetMatchPlayersByMatchId(int matchId);

        List<MatchEvent> GetMatchIncidentsByMatchId(int matchId);

        List<TeamResult> GetTournamentStandingByTournamentId(Tournament tournament);

        List<int> GetUniqueTournaments(int tournamentId, List<int> tournamentIds);

        Tournament GetTournamentById(int id);

        Tournament GetTournamentByMatchId(int matchId);

        List<PartialResult> GetPartialResultsForMatch(int matchId);

        List<AgeCategory> GetTournamentAgeCategory(int tournamentId);

        List<Match> GetTournamentMaintenanceMatchesByTournamentIdAndDate(int tournamentId, DateTime matchStartDate, DateTime matchEndDate);

        List<ChangeInfo> GetTodaysChangesResultTeam(string userKey, string clientPassword, DateTime eventStartDate, DateTime eventEndDate);

        MatchExtended GetMatchInfoByResultId(int resultId);
    }
}