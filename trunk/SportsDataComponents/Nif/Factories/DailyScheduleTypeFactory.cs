﻿using System;
using System.Xml;
using NTB.SportsData.Classes.Enums;
using NTB.SportsData.Components.Nif.Abstracts;
using NTB.SportsData.Components.Nif.JobBuilders;
using Quartz;

namespace NTB.SportsData.Components.Nif.Factories
{
    /// <summary>
    /// The daily schedule type factory.
    /// </summary>
    public class DailyScheduleTypeFactory : AScheduleTypeFactory
    {
        /// <summary>
        /// The setup schedule type.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="results">
        /// The results.
        /// </param>
        /// <param name="scheduleMessage">
        /// The schedule message.
        /// </param>
        /// <param name="duration">
        /// The duration.
        /// </param>
        /// <param name="scheduler">
        /// The scheduler.
        /// </param>
        /// <returns>
        /// The <see cref="DateTimeOffset"/>.
        /// </returns>
        public override DateTimeOffset SetupScheduleType(XmlNode node, bool results, bool scheduleMessage, int duration, IScheduler scheduler)
        {
            if (node == null)
            {
                return new DateTimeOffset();
            }

            if (node.Attributes == null)
            {
                return new DateTimeOffset();
            }

            var scheduleId = node.Attributes["Id"].Value;

            // Creating the offset time 
            var offset = 0;
            if (node.Attributes["Offset"] != null)
            {
                offset = Convert.ToInt32(node.Attributes["Offset"].Value);
            }

            // If the value is larger or less than 0
            if (offset > 0 || offset < 0)
            {
                if (offset > 0)
                {
                    this.DateTimeOffset = DateTime.Today.AddDays(offset);
                }
                else if (offset < 0)
                {
                    var tsOffset = TimeSpan.FromDays(offset);
                    this.DateTimeOffset = DateTime.Today.Subtract(tsOffset);
                }
            }

            // Splitting the time into two values so that we can create a cron job
            var scheduleTimeArray = node.Attributes["Time"].Value.Split(new[] { ':' });

            // If minutes contains two zeros (00), we change it to one zero (0) 
            // If not, scheduler won't understand
            if (scheduleTimeArray[1] == "00")
            {
                scheduleTimeArray[1] = "0";
            }

            // Doing the same thing for hours
            if (scheduleTimeArray[0] == "00")
            {
                scheduleTimeArray[0] = "0";
            }

            // Checking if there is a setting for results
            if (node.Attributes["Result"] != null)
            {
                results = Convert.ToBoolean(node.Attributes["Result"].Value);
                scheduleMessage = !results;
            }


            // Checking if there is a setting for Duration
            if (node.Attributes["Duration"] != null)
            {
                duration = Convert.ToInt32(node.Attributes["Duration"].Value);
            }

            /*
                                 * Creating cron expressions and more. 
                                 * This is the cron syntax:
                                 *  Seconds
                                 *  Minutes
                                 *  Hours
                                 *  Day-of-Month
                                 *  Month
                                 *  Day-of-Week
                                 *  Year (optional field)
                                 */

            // Creating the daily cronexpression
            var stringCronExpression = "0 " + scheduleTimeArray[1] + " " + scheduleTimeArray[0] + " " + "? " + "* " + "* ";

            // Setting up the CronTrigger
            Logger.Debug("Setting up the CronTrigger with following pattern: " + stringCronExpression);

            // Setting up the Daily CronTrigger
            // new CronExpression(stringCronExpression);

            // dailyCronTrigger = new CronTrigger(InstanceName + ScheduleID, InstanceName);
            // dailyCronTrigger.CronExpression = cronExpression;

            // Creating the daily Cron Trigger
            this.Trigger = TriggerBuilder.Create()
                .WithIdentity(this.InstanceName + "_" + scheduleId, "GroupNIF")
                .WithDescription(this.InstanceName)
                .WithCronSchedule(stringCronExpression)
                .Build();

            Logger.Debug("dailyCronTrigger: " + this.Trigger.Description);

            // Creating the jobDetail 
            this.JobDetail = JobBuilder.Create<NifTeamSportJobBuilder>()
                .WithIdentity("job_" + this.InstanceName + "_" + scheduleId, "GroupNIF")
                .WithDescription(this.InstanceName)
                .Build();

            if (this.FileOutputFolder != null)
            {
                this.JobDetail.JobDataMap["OutputFolder"] = this.FileOutputFolder;
            }

            this.JobDetail.JobDataMap["ScheduleType"] = ScheduleType.Daily.ToString();
            this.JobDetail.JobDataMap["InsertDatabase"] = this.PopulateDatabase;
            this.JobDetail.JobDataMap["ScheduleType"] = this.ScheduleType.ToString();
            this.JobDetail.JobDataMap["Results"] = results;
            this.JobDetail.JobDataMap["OperationMode"] = this.OperationMode.ToString();
            this.JobDetail.JobDataMap["ScheduleMessage"] = scheduleMessage;
            this.JobDetail.JobDataMap["Duration"] = duration;
            this.JobDetail.JobDataMap["Purge"] = this.Purge;
            this.JobDetail.JobDataMap["CreateXML"] = this.CreateXml;
            this.JobDetail.JobDataMap["DateOffset"] = this.DateTimeOffset;
            this.JobDetail.JobDataMap["JobName"] = "job_" + this.InstanceName + "_" + scheduleId;
            this.JobDetail.JobDataMap["SportId"] = this.SportId;
            this.JobDetail.JobDataMap["FederationId"] = this.FederationId;
            this.JobDetail.JobDataMap["FederationKey"] = this.FederationKey;
            this.JobDetail.JobDataMap["DisciplineId"] = this.DisciplineId;

            if (this.JobDetail == null)
            {
                return new DateTimeOffset();
            }
            
            // dailyJobDetail.AddJobListener(JobListenerName);
            Logger.Debug("Setting up and starting dailyJobDetail job " + this.JobDetail.Description + " using trigger : " + this.Trigger.Description);

            // Schedule the task
            var returnValue = scheduler.ScheduleJob(this.JobDetail, this.Trigger);

            // scheduler.ScheduleJob(dailyJobDetail, dailyCronTrigger[a]);
            return returnValue;
        }

        /// <summary>
        /// The populate job detail.
        /// </summary>
        /// <param name="results">
        /// The results.
        /// </param>
        /// <param name="scheduleMessage">
        /// The schedule message.
        /// </param>
        /// <param name="duration">
        /// The duration.
        /// </param>
        /// <param name="jobDetail">
        /// The job detail.
        /// </param>
        public override void PopulateJobDetail(bool results, bool scheduleMessage, int duration, IJobDetail jobDetail)
        {
            var fileOutputFolder = this.FileOutputFolder;
            if (fileOutputFolder != null)
            {
                jobDetail.JobDataMap["OutputFolder"] = this.FileOutputFolder;
            }

            jobDetail.JobDataMap["InsertDatabase"] = this.PopulateDatabase;
            jobDetail.JobDataMap["ScheduleType"] = this.ScheduleType.ToString();
            jobDetail.JobDataMap["Results"] = results;
            jobDetail.JobDataMap["ScheduleMessage"] = scheduleMessage;
            jobDetail.JobDataMap["Duration"] = duration;
            jobDetail.JobDataMap["Purge"] = this.Purge;
            jobDetail.JobDataMap["CreateXML"] = this.CreateXml;
            jobDetail.JobDataMap["SportId"] = this.SportId;
            jobDetail.JobDataMap["FederationId"] = this.FederationId;
            jobDetail.JobDataMap["DisciplineId"] = this.DisciplineId;
            jobDetail.JobDataMap["OperationMode"] = this.OperationMode.ToString();
            jobDetail.JobDataMap["DateOffset"] = this.DateTimeOffset;
        }

        /// <summary>
        /// The set job detail.
        /// </summary>
        /// <param name="scheduleId">
        /// The schedule id.
        /// </param>
        /// <returns>
        /// The <see cref="IJobDetail"/>.
        /// </returns>
        /// <exception cref="ArgumentOutOfRangeException">
        /// throws an exception on error
        /// </exception>
        public override IJobDetail SetJobDetail(string scheduleId)
        {
            IJobDetail jobDetail = null;

            // This part might be moved
            switch (this.OperationMode)
            {
                case OperationMode.Gatherer:
                    jobDetail = JobBuilder.Create<NifTeamSportJobBuilder>()
                        .WithIdentity("job_" + this.InstanceName + "_" + scheduleId, "GroupNIF")
                        .WithDescription(this.InstanceName)
                        .Build();
                    break;
                case OperationMode.Purge:
                    jobDetail = JobBuilder.Create<PurgeSportsData>()
                        .WithIdentity("job_" + this.InstanceName + "_" + scheduleId, "GroupNIF")
                        .WithDescription(this.InstanceName)
                        .Build();
                    break;
                case OperationMode.Distributor:
                    jobDetail = JobBuilder.Create<NifTeamSportJobBuilder>()
                        .WithIdentity("job_" + this.InstanceName + "_" + scheduleId, "GroupNIF")
                        .WithDescription(this.InstanceName)
                        .Build();
                    break;
                case OperationMode.Hold:
                    break;
                case OperationMode.Update:
                    break;
                case OperationMode.Maintenance:
                    break;
                case OperationMode.SynchDistributor:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return jobDetail;
        }

        public int FederationKey { get; set; }
    }
}