﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WeeklyScheduleTypeFactory.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The schedule type configure.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components.Nif.Factories
{
    using System;
    using System.Xml;

    using Classes.Enums;
    using Abstracts;
    using JobBuilders;

    using Quartz;

    /// <summary>
    /// The schedule type configure.
    /// </summary>
    public class WeeklyScheduleTypeFactory : AScheduleTypeFactory
    {
        /// <summary>
        /// The setup schedule type.
        /// </summary>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="results">
        /// The results.
        /// </param>
        /// <param name="scheduleMessage">
        /// The schedule message.
        /// </param>
        /// <param name="duration">
        /// The duration.
        /// </param>
        /// <param name="scheduler">
        /// The scheduler.
        /// </param>
        /// <returns>
        /// The <see cref="DateTimeOffset"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Throws an exception on error
        /// </exception>
        public override DateTimeOffset SetupScheduleType(XmlNode node, bool results, bool scheduleMessage, int duration, IScheduler scheduler)
        {
            if (node == null)
            {
                throw new SportsDataException("The list of nodes cannot be null");
            }

            if (node.Attributes == null)
            {
                return new DateTimeOffset();
            }

            var scheduleId = this.GetScheduleId(node);

            // Splitting the time into two values so that we can create a cron job
            var scheduleTimeArray = node.Attributes["Time"].Value.Split(':');

            var minutes = this.GetScheduleMinutes(scheduleTimeArray);

            var hours = this.GetScheduleHours(scheduleTimeArray);

            results = this.GetScheduleResults(node, results, ref scheduleMessage);

            duration = this.GetScheduleDuration(node, duration);

            var scheduleWeek = this.GetScheduleWeek(node);

            var scheduleDay = this.GetScheduleDay(node);

            var dto = this.GetDayOfWeek(scheduleDay, hours, minutes);

            // Creating a simple Scheduler
            var calendarIntervalSchedule = CalendarIntervalScheduleBuilder.Create();

            // Creating the weekly Trigger using the stuff that we've calculated
            this.Trigger = TriggerBuilder.Create()
                .WithDescription(this.InstanceName)
                .WithIdentity(this.InstanceName + "_" + scheduleId, "GroupNIF")
                .StartAt(dto)
                .WithSchedule(calendarIntervalSchedule.WithIntervalInWeeks(Convert.ToInt32(scheduleWeek)))
                .Build();

            this.JobDetail = this.SetJobDetail(scheduleId);

            if (this.FileOutputFolder != null)
            {
                this.JobDetail.JobDataMap["OutputFolder"] = this.FileOutputFolder;
            }

            this.JobDetail.JobDataMap["ScheduleType"] = ScheduleType.Weekly.ToString();
            this.JobDetail.JobDataMap["InsertDatabase"] = this.PopulateDatabase;
            this.JobDetail.JobDataMap["ScheduleType"] = this.ScheduleType.ToString();
            this.JobDetail.JobDataMap["Results"] = results;
            this.JobDetail.JobDataMap["OperationMode"] = this.OperationMode.ToString();
            this.JobDetail.JobDataMap["ScheduleMessage"] = scheduleMessage;
            this.JobDetail.JobDataMap["Duration"] = duration;
            this.JobDetail.JobDataMap["Purge"] = this.Purge;
            this.JobDetail.JobDataMap["CreateXML"] = this.CreateXml;
            this.JobDetail.JobDataMap["DateOffset"] = this.DateTimeOffset;
            this.JobDetail.JobDataMap["JobName"] = "job_" + this.InstanceName + "_" + scheduleId;
            this.JobDetail.JobDataMap["SportId"] = this.SportId;
            this.JobDetail.JobDataMap["FederationId"] = this.FederationId;
            this.JobDetail.JobDataMap["FederationKey"] = this.FederationKey;
            this.JobDetail.JobDataMap["DisciplineId"] = this.DisciplineId;


            this.PopulateJobDetail(results, scheduleMessage, duration, this.JobDetail);


            if (this.JobDetail == null)
            {
                return new DateTimeOffset();
            }

            Logger.Debug("Setting up and starting weeklyJobDetail job " + this.JobDetail.Description + " using trigger : " + this.Trigger.Description);
            return scheduler.ScheduleJob(this.JobDetail, this.Trigger);
        }

        /// <summary>
        /// The populate job detail.
        /// </summary>
        /// <param name="results">
        /// The results.
        /// </param>
        /// <param name="scheduleMessage">
        /// The schedule message.
        /// </param>
        /// <param name="duration">
        /// The duration.
        /// </param>
        /// <param name="jobDetail">
        /// The job detail.
        /// </param>
        public override void PopulateJobDetail(bool results, bool scheduleMessage, int duration, IJobDetail jobDetail)
        {
            jobDetail.JobDataMap["OutputFolder"] = this.FileOutputFolder;

            jobDetail.JobDataMap["InsertDatabase"] = this.PopulateDatabase;
            jobDetail.JobDataMap["ScheduleType"] = this.ScheduleType.ToString();
            jobDetail.JobDataMap["Results"] = results;
            jobDetail.JobDataMap["ScheduleMessage"] = scheduleMessage;
            jobDetail.JobDataMap["Duration"] = duration;
            jobDetail.JobDataMap["Purge"] = this.Purge;
            jobDetail.JobDataMap["CreateXML"] = this.CreateXml;
            jobDetail.JobDataMap["SportId"] = this.SportId;
            jobDetail.JobDataMap["FederationId"] = this.FederationId;
            jobDetail.JobDataMap["DisciplineId"] = this.DisciplineId;
            jobDetail.JobDataMap["OperationMode"] = this.OperationMode.ToString();
        }

        /// <summary>
        /// The set job detail.
        /// </summary>
        /// <param name="scheduleId">
        /// The schedule id.
        /// </param>
        /// <returns>
        /// The <see cref="IJobDetail"/>.
        /// </returns>
        /// <exception cref="ArgumentOutOfRangeException">
        /// throws an exception on error
        /// </exception>
        public override IJobDetail SetJobDetail(string scheduleId)
        {
            IJobDetail jobDetail = null;

            // This part might be moved
            switch (this.OperationMode)
            {
                case OperationMode.Gatherer:
                    jobDetail = JobBuilder.Create<NifTeamSportJobBuilder>()
                        .WithIdentity("job_" + this.InstanceName + "_" + scheduleId, "GroupNIF")
                        .WithDescription(this.InstanceName)
                        .Build();
                    break;
                case OperationMode.Purge:
                    jobDetail = JobBuilder.Create<PurgeSportsData>()
                        .WithIdentity("job_" + this.InstanceName + "_" + scheduleId, "GroupNIF")
                        .WithDescription(this.InstanceName)
                        .Build();
                    break;
                case OperationMode.Distributor:
                    jobDetail = JobBuilder.Create<NifTeamSportJobBuilder>()
                        .WithIdentity("job_" + this.InstanceName + "_" + scheduleId, "GroupNIF")
                        .WithDescription(this.InstanceName)
                        .Build();
                    break;
                case OperationMode.Hold:
                    break;
                case OperationMode.Update:
                    break;
                case OperationMode.Maintenance:
                    break;
                case OperationMode.SynchDistributor:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return jobDetail;
        }

        public int FederationKey { get; set; }
    }
}