﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SportsDataJobListener.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The sports data job listener.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Components
{
    using System;

    // Using log4net
    using log4net;

    // Using Quartz.net
    using Quartz;

    /// <summary>
    /// The sports data job listener.
    /// </summary>
    internal class SportsDataJobListener : IJobListener
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static ILog logger = null;

        /// <summary>
        /// The _ name.
        /// </summary>
        private string _Name = string.Empty;

        /// <summary>
        /// The job being executed.
        /// </summary>
        protected bool jobBeingExecuted = false;

        /// <summary>
        /// Initializes static members of the <see cref="SportsDataJobListener"/> class.
        /// </summary>
        static SportsDataJobListener()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }

            logger = LogManager.GetLogger(typeof(SportsDataJobListener));

            logger.Debug("SportsDataJobListener is created");
        }

        // public void JobToBeExecuted(JobExecutionContext context)
        // {
        // logger.Debug("Setting jobBeingExecuted to true!");
        // jobBeingExecuted = true;

        // // IScheduler sched = context.Scheduler;

        // }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name
        {
            get
            {
                return this._Name;
            }

            set
            {
                this._Name = value;
            }
        }

        /// <summary>
        /// The job to be executed.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public void JobToBeExecuted(IJobExecutionContext context)
        {
            logger.Debug("Setting jobBeingExecuted to true!");
            this.jobBeingExecuted = true;

            // throw new NotImplementedException();
        }

        /// <summary>
        /// The job was executed.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="jobException">
        /// The job exception.
        /// </param>
        public void JobWasExecuted(IJobExecutionContext context, JobExecutionException jobException)
        {
            logger.Debug("Setting jobBeingExecuted to false!");
            this.jobBeingExecuted = false;

            // throw new NotImplementedException();
        }

        /// <summary>
        /// The job execution vetoed.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void JobExecutionVetoed(IJobExecutionContext context)
        {
            throw new NotImplementedException();
        }
    }
}