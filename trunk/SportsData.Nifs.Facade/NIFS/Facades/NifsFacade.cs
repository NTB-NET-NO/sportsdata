﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NifsFacade.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The nifs facade.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using NTB.SportsData.Nifs.Facade.NIFS.Interfaces;
using NTB.SportsData.Nifs.Services.NIFS.DataMappers;
using NTB.SportsData.Nifs.Services.NIFS.Interfaces;
using NTB.SportsData.Nifs.Services.NIFS.Models;

namespace NTB.SportsData.Nifs.Facade.NIFS.Facades
{
    
    /// <summary>
    /// The nifs facade.
    /// </summary>
    public class NifsFacade : INifsFacade
    {
        /// <summary>
        /// The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(NifsFacade));

        ///// <summary>
        ///// The _class code data mapper.
        ///// </summary>
        //private readonly IItemsDataMapper itemsDataMapper;

        ///// <summary>
        ///// The country item view.
        ///// </summary>
        //private List<CountryItemView> CountryItemView = new List<CountryItemView>();

        ///// <summary>
        ///// Initializes a new instance of the <see cref="NifsFacade"/> class.
        ///// </summary>
        //public NifsFacade()
        //{
        //    this.itemsDataMapper = new ItemsDataMapper();
        //}

        ///// <summary>
        ///// The get all countries.
        ///// </summary>
        ///// <returns>
        ///// The <see>
        /////         <cref>List</cref>
        /////     </see>
        /////     .
        ///// </returns>
        //public List<CountryItemView> GetAllCountries()
        //{
        //    try
        //    {
        //        var result = this.itemsDataMapper.GetAllCountries();

        //        this.RecursiveMapping(result);
        //        return new List<CountryItemView>();
        //    }
        //    catch (Exception exception)
        //    {
        //        Logger.Error(exception);

        //        return null;
        //    }
        //}

        ///// <summary>
        ///// The recursive mapping.
        ///// </summary>
        ///// <param name="item">
        ///// The item.
        ///// </param>
        ///// <returns>
        ///// The <see>
        /////         <cref>List</cref>
        /////     </see>
        /////     .
        ///// </returns>
        //private List<CountryItemView> RecursiveMapping(List<Items> item)
        //{
        //    foreach (var i in item)
        //    {
        //        var countryViewItem = new CountryItemView { Label = i.Label };
        //        var data = new Data
        //                       {
        //                           Assists = i.Data.Assists, 
        //                           Attendances = i.Data.Attendances, 
        //                           Corners = i.Data.Corners, 
        //                           Goalscorers = i.Data.Goalscorers, 
        //                           HalfTimeScore = i.Data.HalfTimeScore, 
        //                           Id = i.Data.Id, 
        //                           IndirectAssists = i.Data.IndirectAssists, 
        //                           MinutesPlayed = i.Data.MinutesPlayed, 
        //                           Name = i.Data.Name, 
        //                           Penalties = i.Data.Penalties, 
        //                           Ratings = i.Data.Ratings, 
        //                           RedCards = i.Data.RedCards, 
        //                           Referees = i.Data.Referees, 
        //                           Shots = i.Data.Shots, 
        //                           Squads = i.Data.Squads, 
        //                           Stadiums = i.Data.Stadiums, 
        //                           Type = i.Data.Type, 
        //                           Uid = i.Data.Uid
        //                       };

        //        countryViewItem.Data = data;

        //        if (i.Children.Any())
        //        {
        //            countryViewItem.Children = this.RecursiveMapping(i.Children);
        //        }

        //        this.CountryItemView.Add(countryViewItem);
        //    }

        //    return this.CountryItemView;
        //}
    }
}