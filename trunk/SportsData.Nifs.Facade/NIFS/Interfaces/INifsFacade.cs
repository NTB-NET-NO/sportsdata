﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="INifsFacade.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The NifsFacade interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;

namespace NTB.SportsData.Nifs.Facade.NIFS.Interfaces
{
    /// <summary>
    /// The NifsFacade interface.
    /// </summary>
    public interface INifsFacade
    {
        /// <summary>
        /// The get all countries.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        // List<CountryItemView> GetAllCountries();
    }
}