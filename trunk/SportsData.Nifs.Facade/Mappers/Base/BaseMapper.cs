﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BaseMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The base mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using Glue;
using Glue.Converters;

namespace NTB.SportsData.Nifs.Facade.Mappers.Base
{
    /// <summary>
    /// The base mapper.
    /// </summary>
    /// <typeparam name="TLeftType">
    /// The left type
    /// </typeparam>
    /// <typeparam name="TRightType">
    /// The right type
    /// </typeparam>
    public abstract class BaseMapper<TLeftType, TRightType>
    {
        /// <summary>
        /// The _map.
        /// </summary>
        private readonly Mapping<TLeftType, TRightType> _map;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseMapper{TLeftType,TRightType}"/> class.
        /// </summary>
        protected BaseMapper()
            : this(null, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseMapper{TLeftType,TRightType}"/> class.
        /// </summary>
        /// <param name="creatorTowardsLeft">
        /// The creator towards left.
        /// </param>
        /// <param name="creatorTowardsRight">
        /// The creator towards right.
        /// </param>
        protected BaseMapper(Func<TRightType, TLeftType> creatorTowardsLeft, Func<TLeftType, TRightType> creatorTowardsRight)
        {
            this._map = new Mapping<TLeftType, TRightType>(creatorTowardsLeft, creatorTowardsRight);
            this.SetUpMapper(this._map);
        }

        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected abstract void SetUpMapper(Mapping<TLeftType, TRightType> mapper);

        /// <summary>
        /// The map.
        /// </summary>
        /// <param name="from">
        /// The from.
        /// </param>
        /// <param name="to">
        /// The to.
        /// </param>
        /// <returns>
        /// The <see cref="TRightType"/>.
        /// </returns>
        public virtual TRightType Map(TLeftType from, TRightType to)
        {
            return this._map.Map(from, to);
        }

        /// <summary>
        /// The map.
        /// </summary>
        /// <param name="from">
        /// The from.
        /// </param>
        /// <param name="to">
        /// The to.
        /// </param>
        /// <returns>
        /// The <see cref="TLeftType"/>.
        /// </returns>
        public virtual TLeftType Map(TRightType from, TLeftType to)
        {
            return this._map.Map(from, to);
        }

        /// <summary>
        /// The get mapper.
        /// </summary>
        /// <returns>
        /// The <see cref="Mapping"/>.
        /// </returns>
        public Mapping<TLeftType, TRightType> GetMapper()
        {
            return this._map;
        }

        /// <summary>
        /// The string to int converter.
        /// </summary>
        public void StringToIntConverter()
        {
            this._map.AddConverter(Converting.BetweenIntAndString());
        }

        /// <summary>
        /// The nullable int to int converter.
        /// </summary>
        /// <returns>
        /// The <see cref="IConverter"/>.
        /// </returns>
        public IConverter NullableIntToIntConverter()
        {
            return new QuickConverter<int?, int>(this.ConvertNullableIntToInt32, this.ConvertInt32ToNull);
        }

        /// <summary>
        /// The convert int 32 to null.
        /// </summary>
        /// <param name="i">
        /// The i.
        /// </param>
        /// <returns>
        /// The <see cref="int?"/>.
        /// </returns>
        private int? ConvertInt32ToNull(int i)
        {
            if (i == 0)
            {
                return null;
            }

            return i;
        }

        /// <summary>
        /// The convert nullable int to int 32.
        /// </summary>
        /// <param name="i">
        /// The i.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        private int ConvertNullableIntToInt32(int? i)
        {
            return i != null ? Convert.ToInt32(i) : 0;
        }

        /// <summary>
        /// The string to date time converter.
        /// </summary>
        /// <returns>
        /// The <see cref="IConverter"/>.
        /// </returns>
        public IConverter StringToDateTimeConverter()
        {
            return new QuickConverter<string, DateTime>(this.ConvertStringToDateTime, this.ConvertDateToString);
        }

        /// <summary>
        /// The convert date to string.
        /// </summary>
        /// <param name="dateTime">
        /// The date time.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string ConvertDateToString(DateTime dateTime)
        {
            return dateTime.ToLongDateString();
        }

        /// <summary>
        /// The convert string to date time.
        /// </summary>
        /// <param name="s">
        /// The s.
        /// </param>
        /// <returns>
        /// The <see cref="DateTime"/>.
        /// </returns>
        private DateTime ConvertStringToDateTime(string s)
        {
            return Convert.ToDateTime(s);
        }

        /// <summary>
        /// The string to int 32 converter.
        /// </summary>
        /// <returns>
        /// The <see cref="IConverter"/>.
        /// </returns>
        public IConverter StringToInt32Converter()
        {
            var converter = new QuickConverter<string, int>(this.ConvertStringToInt32, this.ConvertInt32ToString);

            return converter;
        }

        /// <summary>
        /// The convert int 32 to string.
        /// </summary>
        /// <param name="i">
        /// The i.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string ConvertInt32ToString(int i)
        {
            return i.ToString();
        }

        /// <summary>
        /// The convert string to int 32.
        /// </summary>
        /// <param name="s">
        /// The s.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        private int ConvertStringToInt32(string s)
        {
            return s == string.Empty ? 0 : Convert.ToInt32(s);
        }

        /// <summary>
        /// The yes no converter.
        /// </summary>
        public void YesNoConverter()
        {
            var converter = new QuickConverter<string, bool>(fromString => fromString != "N", fromBool => fromBool ? "Y" : "N");

            // or ?
            this._map.AddConverter(converter);

            // return converter;
        }

        /// <summary>
        /// The string to time int converter.
        /// </summary>
        /// <returns>
        /// The <see cref="IConverter"/>.
        /// </returns>
        public IConverter StringToTimeIntConverter()
        {
            var converter = new QuickConverter<string, int>(this.ConvertStringToTimeInt, this.ConvertFromTimeIntToString);

            return converter;
        }

        /// <summary>
        /// The convert from time int to string.
        /// </summary>
        /// <param name="i">
        /// The i.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string ConvertFromTimeIntToString(int i)
        {
            // A Time String is like this: 10:00:00
            var timeString = i.ToString();

            return timeString;
        }

        /// <summary>
        /// The convert string to time int.
        /// </summary>
        /// <param name="s">
        /// The s.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        private int ConvertStringToTimeInt(string s)
        {
            var dt = Convert.ToDateTime(s);
            var stringTime = dt.ToShortTimeString().Replace(":", string.Empty);
            return Convert.ToInt32(stringTime);
        }
    }
}