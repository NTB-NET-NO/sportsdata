﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CountryItemViewMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The country item view mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Glue;
using NTB.SportsData.Nifs.Facade.Mappers.Base;
using NTB.SportsData.Nifs.Services.NIFS.Models;

namespace NTB.SportsData.Nifs.Facade.Mappers.NIFS
{
    /// <summary>
    /// The country item view mapper.
    /// </summary>
    public class CountryItemViewMapper : BaseMapper<Items, CountryItemView>
    {
        /// <summary>
        /// The set up mapper.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        protected override void SetUpMapper(Mapping<Items, CountryItemView> mapper)
        {
            mapper.Relate(x => x.Label, y => y.Label);
            mapper.Relate(x => x.Data, y => y.Data);
            mapper.Relate(x => x.Children, y => y.Children);
        }
    }
}