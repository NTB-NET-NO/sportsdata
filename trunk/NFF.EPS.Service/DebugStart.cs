﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DebugStart.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The debug start.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service
{
    using System;
    using System.Windows.Forms;

    /// <summary>
    /// The debug start.
    /// </summary>
    internal static class DebugStart
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new DebugForm());
        }
    }
}