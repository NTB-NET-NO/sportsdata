﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DebugForm.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The Debug form
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service
{
    using System;
    using System.Windows.Forms;

    using Components;

    using log4net;

    /// <summary>
    /// The debug form.
    /// </summary>
    public partial class DebugForm : Form
    {
        /// <summary>
        /// Static _logger
        /// </summary>
        private static ILog logger = LogManager.GetLogger(typeof(DebugForm));

        /// <summary>
        /// The actual service object
        /// </summary>
        /// <remarks>Does the actuall work</remarks>
        private readonly MainServiceComponent service;

        /// <summary>
        /// Initializes a new instance of the <see cref="DebugForm"/> class.
        /// </summary>
        /// <remarks>
        /// Default constructor
        /// </remarks>
        public DebugForm()
        {
            ThreadContext.Properties["JOBNAME"] = "SportsDataService-Debug";
            log4net.Config.XmlConfigurator.Configure();

            logger = LogManager.GetLogger(typeof(DebugForm));

            this.service = new MainServiceComponent();

            this.InitializeComponent();
        }

        /// <summary>
        /// The debug form_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void DebugFormLoad(object sender, EventArgs e)
        {
            try
            {
                logger.Info("SportsDataService DEBUGMODE starting...");
                this._startupConfigTimer.Start();
            }
            catch (Exception ex)
            {
                logger.Fatal("SportsDataService DEBUGMODE DID NOT START properly", ex);
            }
        }

        /// <summary>
        /// The debug form_ form closing.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void DebugFormFormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                this.service.Stop();
                logger.Info("SportsDataService DEBUGMODE stopped");
            }
            catch (Exception ex)
            {
                logger.Fatal("SportsDataService DEBUGMODE DID NOT STOP properly", ex);
            }
        }

        /// <summary>
        /// The _startup config timer_ tick.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void StartupConfigTimerTick(object sender, EventArgs e)
        {
            // Kill the timer
            this._startupConfigTimer.Stop();

            // And configure
            try
            {
                this.service.Configure();
                this.service.Start();
                logger.Info("SportsDataService DEBUGMODE started");
            }
            catch (Exception ex)
            {
                logger.Fatal("SportsDataService DEBUGMODE DID NOT START properly", ex);
            }
        }
    }
}