﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainSportsDataService.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The main ntb sports data service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service
{
    using System;
    using System.ServiceProcess;
    using System.Timers;

    // Adding Log4Net - logging support
    using log4net;

    // Adding reference to components
    using Components;

    /// <summary>
    /// The main ntb sports data service.
    /// </summary>
    public partial class MainNtbSportsDataService : ServiceBase
    {
        /// <summary>
        /// The service.
        /// </summary>
        private readonly MainServiceComponent service;

        /// <summary>
        /// The _logger.
        /// </summary>
        private static ILog _logger = LogManager.GetLogger(typeof(MainNtbSportsDataService));

        /// <summary>
        /// Initializes a new instance of the <see cref="MainNtbSportsDataService"/> class.
        /// </summary>
        public MainNtbSportsDataService()
        {
            // log4net.Config.XmlConfigurator.Configure();
            ThreadContext.Properties["JOBNAME"] = "NTB.SportsData.Service";
            log4net.Config.XmlConfigurator.Configure();

            _logger = LogManager.GetLogger(typeof(MainNtbSportsDataService));

            _logger.Info("In MainNTBSportsDataService - starting up");

            // Creating the new MainServiceComponent
            this.service = new MainServiceComponent();

            // Initialising this main component
            this.InitializeComponent();
        }

        /// <summary>
        /// The on start.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        protected override void OnStart(string[] args)
        {
            try
            {
                _logger.Info("NTB SportsDataService starting...");
                this._startupConfigTimer.Start();
            }
            catch (Exception ex)
            {
                _logger.Fatal("NTBSportsDataService DID NOT START properly - Terminating!", ex);
                throw;
            }
        }

        /// <summary>
        /// The on stop.
        /// </summary>
        protected override void OnStop()
        {
            try
            {
                _logger.Info("Stopping service");
                this.service.Stop();

                _startupConfigTimer.Enabled = false;
            }
            catch (Exception ex)
            {
                _logger.Fatal("NTB SporstDataService did not stop properly - Terminating!", ex);
                throw;
            }
        }

        /// <summary>
        /// The service timer_ elapsed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ServiceTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _startupConfigTimer.Stop();

            try
            {
                this.service.Configure();
                this.service.Start();
                _logger.Info("MainSportsDataService Started!");
            }
            catch (Exception ex)
            {
                _logger.Fatal("MainSportsDataService DID NOT START properly - TERMINATING!", ex);

                throw;
            }
        }
    }
}