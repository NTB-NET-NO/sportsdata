﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceStart.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The service start.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service
{
    using System.ServiceProcess;

    // Adding support for log4Net logging
    using log4net;
    using log4net.Config;

    /// <summary>
    /// The service start.
    /// </summary>
    internal static class ServiceStart
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(MainNtbSportsDataService));

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        private static void Main()
        {
            XmlConfigurator.Configure();
            MDC.Set("JOBNAME", "NTB.SportsData.Service");
            Logger.Info("---------------------------------------------------------");
            Logger.Info("---       Starting up the NTB SportsData Service      ---");
            Logger.Info("---------------------------------------------------------");

            ServiceBase[] servicesToRun = new ServiceBase[] { new MainNtbSportsDataService() };
            ServiceBase.Run(servicesToRun);
        }
    }
}