﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ItemsDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The items data mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using log4net;
using Newtonsoft.Json;
using NTB.SportsData.Nifs.Services.NIFS.Interfaces;
using NTB.SportsData.Nifs.Services.NIFS.Models;
using RestSharp;

namespace NTB.SportsData.Nifs.Services.NIFS.DataMappers
{
    /// <summary>
    /// The items data mapper.
    /// </summary>
    public class ItemsDataMapper : IItemsDataMapper
    {
        /// <summary>
        /// The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(ItemsDataMapper));

        /// <summary>
        /// The get all countries.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Items> GetAllCountries()
        {
            // The URL for the Rest API
            // const string ApiUrl = "http://api.nifs.no/usr/local/nifs/www/nifs.api.no/api/v1/countries/";
            var apiUrl = ConfigurationManager.AppSettings["NifsRootUrl"];
            apiUrl += "countries";

            // Creating the client
            var client = new RestClient(apiUrl);
            var startYear = DateTime.Today.AddYears(-1).Year;
            var endYear = DateTime.Today.Year;
            var request = new RestRequest(string.Format("?withTournaments=1&withStages=1&level=2&fromYear={0}&toYear={1}", startYear, endYear), Method.GET);

            Logger.Debug(request.ToString());
            var response = client.Execute(request);
            var content = response.Content;

            var countries = JsonConvert.DeserializeObject<List<CountryTournamentView>>(content).OrderBy(x => x.Priority);

            var nodes = new List<Items>();
            foreach (var ctw in countries)
            {
                var node = new Items { Label = ctw.Name, Data = new Data { Id = Convert.ToInt32(ctw.Id), Type = "Country" } };

                if (node.Children == null)
                {
                    node.Children = new List<Items>();
                }

                foreach (var tournament in ctw.Tournaments)
                {
                    var tournamentNode = new Items() { Label = tournament.Name, Data = new Data() { Id = tournament.Id, Type = "Tournament" } };

                    foreach (var stage in tournament.Stages)
                    {
                        if (tournamentNode.Children == null)
                        {
                            tournamentNode.Children = new List<Items>();
                        }

                        var stageNode = new Items() { Label = stage.FullName, Data = new Data() { Id = stage.Id, Type = "Stage" } };
                        tournamentNode.Children.Add(stageNode);
                    }

                    node.Children.Add(tournamentNode);
                }

                nodes.Add(node);
            }

            return nodes;
        }
    }
}