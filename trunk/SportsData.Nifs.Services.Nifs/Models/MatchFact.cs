﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchFact.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The match fact.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace NTB.SportsData.Nifs.Services.NIFS.Models
{
    /// <summary>
    /// The match fact.
    /// </summary>
    public class MatchFact
    {
        /// <summary>
        /// Gets or sets the time stamp.
        /// </summary>
        [JsonProperty("timestamp")]
        public DateTime TimeStamp { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        [JsonProperty("result")]
        public Result Result { get; set; }

        /// <summary>
        /// Gets or sets the home team.
        /// </summary>
        [JsonProperty("homeTeam")]
        public MatchTeam HomeTeam { get; set; }

        /// <summary>
        /// Gets or sets the away team.
        /// </summary>
        [JsonProperty("awayTeam")]
        public MatchTeam AwayTeam { get; set; }

        /// <summary>
        /// Gets or sets the stage.
        /// </summary>
        [JsonProperty("stage")]
        public Stage Stage { get; set; }

        /// <summary>
        /// Gets or sets the match events.
        /// </summary>
        [JsonProperty("matchEvents")]
        public List<MatchEvent> MatchEvents { get; set; }
    }
}