﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentMatch.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The tournament match.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using Newtonsoft.Json;

namespace NTB.SportsData.Nifs.Services.NIFS.Models
{
    /// <summary>
    /// The tournament match.
    /// </summary>
    public class TournamentMatch
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the match date time.
        /// </summary>
        [JsonProperty("timestamp")]
        public DateTime MatchDateTime { get; set; }

        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        [JsonProperty("result")]
        public MatchResult Result { get; set; }

        /// <summary>
        /// Gets or sets the home team.
        /// </summary>
        [JsonProperty("hometeam")]
        public MatchTeam HomeMatchTeam { get; set; }

        /// <summary>
        /// Gets or sets the away team.
        /// </summary>
        [JsonProperty("awayteam")]
        public MatchTeam AwayMatchTeam { get; set; }

        /// <summary>
        /// Gets or sets the match status id.
        /// </summary>
        [JsonProperty("matchStatusId")]
        public int MatchStatusId { get; set; }

        /// <summary>
        /// Gets or sets the match status text.
        /// </summary>
        [JsonProperty("matchStatusText")]
        public string MatchStatusText { get; set; }

        /// <summary>
        /// Gets or sets the match type id.
        /// </summary>
        [JsonProperty("matchTypeId")]
        public int? MatchTypeId { get; set; }

        /// <summary>
        /// Gets or sets the attendance.
        /// </summary>
        [JsonProperty("attendance")]
        public int? Attendance { get; set; }

        /// <summary>
        /// Gets or sets the round.
        /// </summary>
        [JsonProperty("round")]
        public int? Round { get; set; }

        /// <summary>
        /// Gets or sets the comment.
        /// </summary>
        [JsonProperty("comment")]
        public string Comment { get; set; }

        /// <summary>
        /// Gets or sets the stage id.
        /// </summary>
        [JsonProperty("stageId")]
        public int? StageId { get; set; }

        [JsonProperty("stage")]
        public Stage Stage { get; set; }
    }
}