using Newtonsoft.Json;

namespace NTB.SportsData.Nifs.Services.NIFS.Models
{
    /// <summary>
    /// The position.
    /// </summary>
    public class Position
    {
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        [JsonProperty("position")]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the x.
        /// </summary>
        [JsonProperty("x")]
        public int? x { get; set; }

        /// <summary>
        /// Gets or sets the y.
        /// </summary>
        [JsonProperty("y")]
        public int? y { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the uid.
        /// </summary>
        [JsonProperty("uid")]
        public string Uid { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }
    }
}