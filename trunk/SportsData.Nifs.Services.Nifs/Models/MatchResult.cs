﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchResult.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The match result.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Newtonsoft.Json;

namespace NTB.SportsData.Nifs.Services.NIFS.Models
{
    /// <summary>
    /// The match result.
    /// </summary>
    public class MatchResult
    {
        /// <summary>
        /// Gets or sets the home half time score.
        /// </summary>
        [JsonProperty("homeScore45")]
        public int? HomeHalfTimeScore { get; set; }

        /// <summary>
        /// Gets or sets the away half time score.
        /// </summary>
        [JsonProperty("awayScore45")]
        public int? AwayHalfTimeScore { get; set; }

        /// <summary>
        /// Gets or sets the home full time score.
        /// </summary>
        [JsonProperty("homeScore90")]
        public int? HomeFullTimeScore { get; set; }

        /// <summary>
        /// Gets or sets the away full time score.
        /// </summary>
        [JsonProperty("awayScore90")]
        public int? AwayFullTimeScore { get; set; }
    }
}