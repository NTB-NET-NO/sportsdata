﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CountryRoot.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The country root.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Nifs.Services.NIFS.Models
{
    /// <summary>
    /// The country root.
    /// </summary>
    public class CountryRoot
    {
        /// <summary>
        /// The countries.
        /// </summary>
        public CountryTournamentView[] CountriesTournamentView;
    }
}