﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Person.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The person.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using Newtonsoft.Json;

namespace NTB.SportsData.Nifs.Services.NIFS.Models
{
    /// <summary>
    /// The person.
    /// </summary>
    public class Person
    {
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the uid.
        /// </summary>
        [JsonProperty("uid")]
        public string Uid { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [JsonProperty("name")]
        public string FullName { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        [JsonProperty("lastName")]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the nick name.
        /// </summary>
        [JsonProperty("nickName")]
        public string NickName { get; set; }

        /// <summary>
        /// Gets or sets the birth date.
        /// </summary>
        [JsonProperty("birthDate")]
        public DateTime BirthDate { get; set; }

        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        [JsonProperty("gender")]
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        [JsonProperty("height")]
        public int Height { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether starts match.
        /// </summary>
        [JsonProperty("startsMatch")]
        public bool StartsMatch { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether starts on the bench.
        /// </summary>
        [JsonProperty("startsOnTheBench")]
        public bool StartsOnTheBench { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether currently on the field.
        /// </summary>
        [JsonProperty("currentlyOnTheField")]
        public bool CurrentlyOnTheField { get; set; }

        /// <summary>
        /// Gets or sets the enters field minute.
        /// </summary>
        [JsonProperty("entersFieldMinute")]
        public int? EntersFieldMinute { get; set; }

        /// <summary>
        /// Gets or sets the leaves field minute.
        /// </summary>
        [JsonProperty("leavesFieldMinute")]
        public int? LeavesFieldMinute { get; set; }

        /// <summary>
        /// Gets or sets the minutes played.
        /// </summary>
        [JsonProperty("minutesPlayed")]
        public int? MinutesPlayed { get; set; }

        /// <summary>
        /// Gets or sets the shirt number.
        /// </summary>
        [JsonProperty("shirtNumber")]
        public int? ShirtNumber { get; set; }

        /// <summary>
        /// Gets or sets the person id.
        /// </summary>
        [JsonProperty("personId")]
        public int PersonId { get; set; }

        /// <summary>
        /// Gets or sets the match id.
        /// </summary>
        [JsonProperty("matchId")]
        public int MatchId { get; set; }

        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        [JsonProperty("position")]
        public Position Position { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        [JsonProperty("country")]
        public Country Country { get; set; }
    }
}