﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentTableTeam.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The tournament table team.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Newtonsoft.Json;

namespace NTB.SportsData.Nifs.Services.NIFS.Models
{
    /// <summary>
    /// The tournament table team.
    /// </summary>
    public class TournamentTableTeam
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the uid.
        /// </summary>
        [JsonProperty("uid")]
        public string Uid { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the place.
        /// </summary>
        [JsonProperty("place")]
        public int Place { get; set; }

        /// <summary>
        /// Gets or sets the played.
        /// </summary>
        [JsonProperty("played")]
        public int Played { get; set; }

        /// <summary>
        /// Gets or sets the won.
        /// </summary>
        [JsonProperty("won")]
        public int Won { get; set; }

        /// <summary>
        /// Gets or sets the draw.
        /// </summary>
        [JsonProperty("draw")]
        public int Draw { get; set; }

        /// <summary>
        /// Gets or sets the lost.
        /// </summary>
        [JsonProperty("lost")]
        public int Lost { get; set; }

        /// <summary>
        /// Gets or sets the goals scored.
        /// </summary>
        [JsonProperty("goalsScored")]
        public int GoalsScored { get; set; }

        /// <summary>
        /// Gets or sets the goals conceded.
        /// </summary>
        [JsonProperty("goalsConceded")]
        public int GoalsConceded { get; set; }

        /// <summary>
        /// Gets or sets the points.
        /// </summary>
        [JsonProperty("points")]
        public int Points { get; set; }

        /// <summary>
        /// Gets or sets the last six matches.
        /// </summary>
        [JsonProperty("lastSixMatches")]
        public string LastSixMatches { get; set; }

        /// <summary>
        /// Gets or sets the change since last round.
        /// </summary>
        [JsonProperty("changeSinceLastRound")]
        public int ChangeSinceLastRound { get; set; }
    }
}