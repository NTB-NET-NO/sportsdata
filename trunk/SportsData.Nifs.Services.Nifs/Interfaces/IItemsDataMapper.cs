﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IItemsDataMapper.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The ItemsDataMapper interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using NTB.SportsData.Nifs.Services.NIFS.Models;

namespace NTB.SportsData.Nifs.Services.NIFS.Interfaces
{
    /// <summary>
    /// The ItemsDataMapper interface.
    /// </summary>
    public interface IItemsDataMapper
    {
        /// <summary>
        /// The get all countries.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        List<Items> GetAllCountries();
    }
}