﻿using System.Xml.Serialization;

namespace NTB.SportsData.Classes.Classes
{
    // Consider doing this class static
    [XmlRoot("Customer")]
    public class Customer
    {
        [XmlAttribute("Id")]
        public int Id { get; set; }

        [XmlAttribute("RemoteCustomerId")]
        public int RemoteCustomerId { get; set; }

        [XmlText]
        public string Name { get; set; }
    }
}
