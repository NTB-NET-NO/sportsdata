﻿using System;
using NTB.SportsData.Classes.Enums;

namespace NTB.SportsData.Classes.Classes
{
    public class ChangeInfo
    {
        public int Id { get; set; }

        public ChangeType ChangeType { get; set; }

        public EntityType EntityType { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

        public string Name { get; set; }
    }
}