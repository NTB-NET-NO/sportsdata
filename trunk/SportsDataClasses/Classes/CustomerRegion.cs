﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsData.Classes.Classes
{
    public class CustomerRegion
    {
        /// <summary>
        ///     Gets or sets the Region Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        ///     Gets or sets the Region Name
        /// </summary>
        public string Name { get; set; }

    }
}
