﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace NTB.SportsData.Classes.Classes.SportsDataOutput
{
    public class TournamentStandingTable
    {
        [XmlArray("Team")]
        public List<Standing.TeamResult> Team { get; set; }
    }
}