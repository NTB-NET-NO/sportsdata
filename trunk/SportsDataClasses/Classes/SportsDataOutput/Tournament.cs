﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace NTB.SportsData.Classes.Classes.SportsDataOutput
{
    public class Tournament
    {
        [XmlAttribute("Type")]
        public string Type { get; set; }

        [XmlAttribute("doc-id")]
        public string DocId { get; set; }

        [XmlElement("Tournament-MetaInfo")]
        public TournamentMetaInfo TournamentMetaInfo { get; set; }

        [XmlElement("Header")]
        public Header Header { get; set; }

        [XmlArray("Matches")]
        [XmlArrayItem("Match")]
        public List<TournamentMatch> Matches { get; set; }

        [XmlArray("Standings")]
        [XmlArrayItem("Team")]
        public List<Standing.TeamResult> TournamentTable { get; set; }
    }
}
