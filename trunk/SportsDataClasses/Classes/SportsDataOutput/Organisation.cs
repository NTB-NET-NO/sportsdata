﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NTB.SportsData.Classes.Classes.SportsDataOutput
{
    public class Organisation
    {
        [XmlAttribute("Name")]
        public string ShortName { get; set; }

        [XmlAttribute("Id")]
        public int Id { get; set; }

        [XmlText]
        public string Content { get; set; }


        
    }
}
