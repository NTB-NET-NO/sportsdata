﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace NTB.SportsData.Classes.Classes.SportsDataOutput
{
    [XmlRoot]
    public class Header
    {
        [XmlElement("Organisation")]
        public Organisation Organisation { get; set; }

        [XmlElement("AgeCategory")]
        public AgeCategory AgeCategory { get; set; }

        [XmlArray("Customers")]
        [XmlArrayItem("Customer")]
        public List<Customer> Customers { get; set; }

        [XmlElement("Sport")]
        public Sport Sport { get; set; }

        [XmlElement("DocumentType")]
        public DocumentType DocumentType { get; set; }

        [XmlElement("File")]
        public File File { get; set; }

        [XmlElement("EventType")]
        public EventType EventType { get; set; }
    }
}
