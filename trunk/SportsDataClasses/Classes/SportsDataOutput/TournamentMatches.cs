﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace NTB.SportsData.Classes.Classes.SportsDataOutput
{
    public class TournamentMatches
    {
        [XmlArray("Matches")]
        [XmlArrayItem("Match")]
        public List<TournamentMatch> Matches { get; set; }
    }
}
