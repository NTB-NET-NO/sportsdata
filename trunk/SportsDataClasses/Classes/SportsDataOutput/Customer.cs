﻿using System.Xml.Serialization;

namespace NTB.SportsData.Classes.Classes.SportsDataOutput
{
    [XmlRoot("Customer")]
    public class Customer
    {
        [XmlElement("CustomerName")]
        public CustomerName CustomerName { get; set; }
    }
}
