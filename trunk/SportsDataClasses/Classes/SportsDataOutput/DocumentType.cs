﻿using System.Xml.Serialization;

namespace NTB.SportsData.Classes.Classes.SportsDataOutput
{
    public class DocumentType
    {
        [XmlAttribute("type")]
        public string Type { get; set; }
    }
}