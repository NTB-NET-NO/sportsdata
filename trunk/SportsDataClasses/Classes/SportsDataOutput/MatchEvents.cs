﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace NTB.SportsData.Classes.Classes.SportsDataOutput
{
    [XmlRoot("Events")]
    public class MatchEvents
    {
        [XmlElement("Event")]
        public List<MatchEvent> Events { get; set; }
    }
}
