﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NTB.SportsData.Classes.Classes.SportsDataOutput
{
    public class MatchResult
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("hometeam")]
        public int HomeTeamGoals { get; set; }

        [XmlAttribute("awayteam")]
        public int AwayTeamGoals { get; set; }
    }
}
