﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NTB.SportsData.Classes.Classes.SportsDataOutput
{
    [XmlRoot("Name")]
    public class FileName
    {
        [XmlAttribute("FullName")]
        public string FullName { get; set; }

        [XmlAttribute("Type")]
        public string Type { get; set; }

        [XmlText]
        public string Content { get; set; }
    }
}
