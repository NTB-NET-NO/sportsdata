﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NTB.SportsData.Classes.Classes.SportsDataOutput
{
    public class MatchEvent
    {
        [XmlAttribute("id")]
        public int Id { get; set; }

        [XmlAttribute("teamid")]
        public int TeamId { get; set; }

        [XmlAttribute("value")]
        public string Value { get; set; }

        [XmlAttribute("minute")]
        public string Minute { get; set; }

        [XmlAttribute("items")]
        public int Items { get; set; }

        [XmlElement("Player")]
        public Player Player { get; set; }

        [XmlAttribute("typeid")]
        public int TypeId { get; set; }
    }
}
