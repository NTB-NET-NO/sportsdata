﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NTB.SportsData.Classes.Classes.SportsDataOutput
{
    [XmlRoot("Name")]
    public class TeamName
    {
        [XmlText]
        public string Name { get; set; }

        [XmlAttribute("short")]
        public string ShortName { get; set; }
    }
}
