﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace NTB.SportsData.Classes.Classes.SportsDataOutput
{
    [XmlRoot("File")]
    public class File
    {
        [XmlElement("Name")]
        public FileName FileName { get; set; }

        [XmlElement("Version")]
        public FileVersion FileVersion { get; set; }

        [XmlElement("Created")]
        public FileCreated FileCreated { get; set; }
    }
}
