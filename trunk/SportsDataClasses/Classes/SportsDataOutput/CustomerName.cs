﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NTB.SportsData.Classes.Classes.SportsDataOutput
{
    public class CustomerName
    {
        [XmlAttribute("Id")]
        public int Id { get; set; }

        [XmlAttribute("RemoteCustomerId")]
        public int RemoteCustomerId { get; set; }

        [XmlText]
        public string Content { get; set; }
    }
}
