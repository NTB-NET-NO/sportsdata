﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NTB.SportsData.Classes.Classes.SportsDataOutput
{
    [XmlRoot("EventType")]
    public class EventType
    {
        [XmlAttribute("id")]
        public int Id { get; set; }

        [XmlText]
        public string Name { get; set; }
    }
}
