﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NTB.SportsData.Classes.Classes.SportsDataOutput
{
    [XmlRoot("AgeCategory")]
    public class AgeCategory
    {
        [XmlAttribute("id")]
        public int Id { get; set; }

        [XmlAttribute("min-age")]
        public int MinimumAge { get; set; }

        [XmlAttribute("max-age")]
        public int MaximumAge { get; set; }

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlText]
        public string Content { get; set; }
    }
}
