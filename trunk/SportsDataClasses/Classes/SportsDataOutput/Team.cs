﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NTB.SportsData.Classes.Classes.SportsDataOutput
{
    public class Team
    {
        [XmlAttribute("id")]
        public int Id { get; set; }

        [XmlAttribute("alignment")]
        public string Alignment { get; set; }

        [XmlElement("Name")]
        public TeamName TeamName { get; set; }

        [XmlElement("Totalgoals")]
        public int TotalGoals { get; set; }

        [XmlArray("Players")]
        [XmlArrayItem("Player")]
        public List<Player> Players { get; set; }

        [XmlElement("PartialResults")]
        public List<PartialResult> PartialResults { get; set; }
    }
}
