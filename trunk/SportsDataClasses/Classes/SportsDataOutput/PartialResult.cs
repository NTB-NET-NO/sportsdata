﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NTB.SportsData.Classes.Classes.SportsDataOutput
{
    public class PartialResult
    {
        [XmlAttribute("id")]
        public int Id { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("value")]
        public int Value { get; set; }

        public int AwayTeamGoals { get; set; }

        public int HomeTeamGoals { get; set; }

        public int TypeId { get; set; }
    }
}
