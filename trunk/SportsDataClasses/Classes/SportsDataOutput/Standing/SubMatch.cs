﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NTB.SportsData.Classes.Classes.SportsDataOutput.Standing
{
    public class SubMatch
    {
        [XmlAttribute("Won")]
        public int Won { get; set; }

        [XmlAttribute("Draw")]
        public int Draw { get; set; }

        [XmlAttribute("Lost")]
        public int Lost { get; set; }
    }
}
