﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Xml.Serialization;

namespace NTB.SportsData.Classes.Classes.SportsDataOutput.Standing
{
    public class Goal
    {
        [XmlAttribute("TotalScored")]
        public int TotalScored { get; set; }

        [XmlAttribute("TotalAgainst")]
        public int TotalAgainst { get; set; }

        [XmlElement("Home")]
        public SubGoal HomeGoal { get; set; }

        [XmlElement("Away")]
        public SubGoal AwayGoal { get; set; }
    }
}
