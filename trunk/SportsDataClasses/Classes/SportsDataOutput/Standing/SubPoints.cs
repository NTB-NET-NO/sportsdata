﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NTB.SportsData.Classes.Classes.SportsDataOutput.Standing
{
    public class SubPoints
    {
        [XmlText]
        public int Content { get; set; }
    }
}
