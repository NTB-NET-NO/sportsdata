﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NTB.SportsData.Classes.Classes.SportsDataOutput.Standing
{
    public class Match
    {
        [XmlAttribute("Total")]
        public int Total { get; set; }

        [XmlElement("HomeMatches")]
        public SubMatch HomeMatch { get; set; }

        [XmlElement("AwayMatches")]
        public SubMatch AwayMatch { get; set; }
    }
}
