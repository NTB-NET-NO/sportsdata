﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NTB.SportsData.Classes.Classes.SportsDataOutput.Standing
{
    public class Points
    {
        [XmlAttribute("Total")]
        public int Total { get; set; }

        [XmlElement("HomePoints")]
        public SubPoints HomePoints { get; set; }

        [XmlElement("AwayPoints")]
        public SubPoints AwayPoints { get; set; }
    }
}
