﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NTB.SportsData.Classes.Classes.SportsDataOutput.Standing
{
    public class SubGoal
    {
        [XmlAttribute("Scored")]
        public int Scored { get; set; }

        [XmlAttribute("Against")]
        public int Against { get; set; }
    }
}
