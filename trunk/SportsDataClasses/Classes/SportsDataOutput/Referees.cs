﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace NTB.SportsData.Classes.Classes.SportsDataOutput
{
    [XmlRoot("Referees")]
    public class Referees
    {
        [XmlElement("Referee")]
        public List<Referee> Referee { get; set; }
    }
}
