﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace NTB.SportsData.Classes.Classes.SportsDataOutput
{
    public class Sport
    {
        [XmlAttribute("id")]
        public int Id { get; set; }

        [XmlText]
        public string Content { get; set; }
    }
}
