﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsData.Classes.Classes
{
    public class VenueUnit
    {
        public int VenueUnitId { get; set; }

        public string VenueUnitName { get; set; }

        public string VenueUnitNo { get; set; }

        public int VenueClassId { get; set; }

        public int VenueId { get; set; }

        public string VenueName { get; set; }
    }
}
