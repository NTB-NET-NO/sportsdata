﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsData.Classes.Enums;

namespace NTB.SportsData.Classes.Classes
{
    public class SingleSportDocument
    {
        public string Filename { get; set; }

        public int Version { get; set; }

        public DateTime Created { get; set; }

        public int EventId { get; set; }

        public int SportId { get; set; }

        public string FullName { get; set; }

        public DocumentType DocumentType { get; set; }
    }
}
