﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScheduleTime.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The schedule time.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Classes.Classes
{
    /// <summary>
    /// The schedule time.
    /// </summary>
    public class ScheduleTime
    {
        /// <summary>
        /// Gets or sets the hour.
        /// </summary>
        public string Hour { get; set; }

        /// <summary>
        /// Gets or sets the minutes.
        /// </summary>
        public string Minutes { get; set; }
    }
}