// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SportResult.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The sport result.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Classes.Classes
{
    using System;

    /// <summary>
    /// The sport result.
    /// </summary>
    public class SportResult
    {
        /// <summary>
        /// Gets or sets the activity name.
        /// </summary>
        public string ActivityName { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the nationality.
        /// </summary>
        public string Nationality { get; set; }

        /// <summary>
        /// Gets or sets the club.
        /// </summary>
        public string Club { get; set; }

        /// <summary>
        /// Gets or sets the rank.
        /// </summary>
        public int? Rank { get; set; }

        /// <summary>
        /// Gets or sets the zip code.
        /// </summary>
        public string ZipCode { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the ranking points.
        /// </summary>
        public double? RankingPoints { get; set; }

        /// <summary>
        /// Gets or sets the time.
        /// </summary>
        public DateTime? Time { get; set; }

        /// <summary>
        /// Gets or sets the time behind.
        /// </summary>
        public DateTime? TimeBehind { get; set; }

        /// <summary>
        /// Gets or sets the start no.
        /// </summary>
        public int StartNo { get; set; }

        /// <summary>
        /// Gets or sets the result values.
        /// </summary>
        public int ResultValues { get; set; }

        /// <summary>
        /// Gets or sets the team.
        /// </summary>
        public string Team { get; set; }

        /// <summary>
        /// Gets or sets the event name.
        /// </summary>
        public string EventName { get; set; }

        /// <summary>
        /// Gets or sets the class exercise name.
        /// </summary>
        public string ClassExerciseName { get; set; }

        /// <summary>
        /// Gets or sets the event type text.
        /// </summary>
        public string EventTypeText { get; set; }

        /// <summary>
        /// Gets or sets the event date.
        /// </summary>
        public string EventDate { get; set; }

        /// <summary>
        /// Gets or sets the event id.
        /// </summary>
        public int EventId { get; set; }

        /// <summary>
        /// Gets or sets the competitor id.
        /// </summary>
        public int CompetitorId { get; set; }

        /// <summary>
        /// Gets or sets the club id.
        /// </summary>
        public int ClubId { get; set; }

        /// <summary>
        /// Gets or sets the person id.
        /// </summary>
        public int PersonId { get; set; }

        /// <summary>
        /// Gets or sets the team id.
        /// </summary>
        public int? TeamId { get; set; }

        /// <summary>
        /// Gets or sets the hour.
        /// </summary>
        public int? Hour { get; set; }

        /// <summary>
        /// Gets or sets the minute.
        /// </summary>
        public int Minute { get; set; }

        /// <summary>
        /// Gets or sets the second.
        /// </summary>
        public int Second { get; set; }

        /// <summary>
        /// Gets or sets the hour behind.
        /// </summary>
        public int? HourBehind { get; set; }

        /// <summary>
        /// Gets or sets the minute behind.
        /// </summary>
        public int MinuteBehind { get; set; }

        /// <summary>
        /// Gets or sets the second behind.
        /// </summary>
        public int SecondBehind { get; set; }

        /// <summary>
        /// Gets or sets the person gender.
        /// </summary>
        public string PersonGender { get; set; }

        /// <summary>
        /// Gets or sets the class exercise id.
        /// </summary>
        public int ClassExerciseId { get; set; }

        /// <summary>
        /// Gets or sets the result id.
        /// </summary>
        public int ResultId { get; set; }

        /// <summary>
        /// Gets or sets the class id.
        /// </summary>
        public int ClassId { get; set; }

        /// <summary>
        /// Gets or sets the region id.
        /// </summary>
        public int RegionId { get; set; }

        /// <summary>
        /// Gets or sets the region name.
        /// </summary>
        public string RegionName { get; set; }
    }
}