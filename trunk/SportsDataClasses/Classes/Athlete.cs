// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Athlete.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The athlete.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Classes.Classes
{
    using System;

    /// <summary>
    /// The athlete.
    /// </summary>
    public class Athlete
    {
        /// <summary>
        /// Gets or sets the person id.
        /// </summary>
        public int PersonId { get; set; }

        /// <summary>
        /// Gets or sets the rank.
        /// </summary>
        public int? Rank { get; set; }

        /// <summary>
        /// Gets or sets the competitor id.
        /// </summary>
        public int CompetitorId { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets the nationality.
        /// </summary>
        public string Nationality { get; set; }

        /// <summary>
        /// Gets or sets the club id.
        /// </summary>
        public int ClubId { get; set; }

        /// <summary>
        /// Gets or sets the club name.
        /// </summary>
        public string ClubName { get; set; }

        /// <summary>
        /// Gets or sets the start no.
        /// </summary>
        public int StartNo { get; set; }

        /// <summary>
        /// Gets or sets the team.
        /// </summary>
        public string Team { get; set; }

        /// <summary>
        /// Gets or sets the team id.
        /// </summary>
        public int? TeamId { get; set; }

        /// <summary>
        /// Gets or sets the sport event id.
        /// </summary>
        public int SportEventId { get; set; }

        /// <summary>
        /// Gets or sets the time.
        /// </summary>
        public DateTime? Time { get; set; }

        /// <summary>
        /// Gets or sets the time behind.
        /// </summary>
        public DateTime? TimeBehind { get; set; }

        /// <summary>
        /// Gets or sets the club.
        /// </summary>
        public Club Club { get; set; }
    }
}