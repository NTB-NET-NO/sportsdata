﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PartialResult.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The partial result.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Classes.Classes
{
    /// <summary>
    /// The partial result.
    /// </summary>
    public class PartialResult
    {
        /// <summary>
        /// Gets or sets the away goals.
        /// </summary>
        public int? AwayGoals { get; set; }

        /// <summary>
        /// Gets or sets the home goals.
        /// </summary>
        public int? HomeGoals { get; set; }

        /// <summary>
        /// Gets or sets the match id.
        /// </summary>
        public int MatchId { get; set; }

        /// <summary>
        /// Gets or sets the partial match result.
        /// </summary>
        public string PartialMatchResult { get; set; }

        /// <summary>
        /// Gets or sets the partial result id.
        /// </summary>
        public int PartialResultId { get; set; }

        /// <summary>
        /// Gets or sets the partial result type id.
        /// </summary>
        public int PartialResultTypeId { get; set; }
    }
}