﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Document.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The document.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;

namespace NTB.SportsData.Classes.Classes
{
    using System;

    using NTB.SportsData.Classes.Enums;

    /// <summary>
    /// The document.
    /// </summary>
    public class Document
    {
        /// <summary>
        /// Gets or sets the filename.
        /// </summary>
        public string Filename { get; set; }

        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        public int Version { get; set; }

        /// <summary>
        /// Gets or sets the created.
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// Gets or sets the tournament id.
        /// </summary>
        public int TournamentId { get; set; }

        /// <summary>
        /// Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        /// Gets or sets the full name.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Gets or sets the document type.
        /// </summary>
        public DocumentType DocumentType { get; set; }

        /// <summary>
        /// The different types of documents
        /// </summary>
        public List<DocumentType> DocumentTypes { get; set; }

        /// <summary>
        /// The document Id we are giving the document
        /// </summary>
        public string DocumentId { get; set; }
    }
}