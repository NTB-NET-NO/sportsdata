namespace NTB.SportsData.Classes.Classes
{
    public class Club
    {
        public int ClubId { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string RegionName { get; set; }
        public string Postal { get; set; }
    }
}