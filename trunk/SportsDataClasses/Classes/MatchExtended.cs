﻿using System.Collections.Generic;

namespace NTB.SportsData.Classes.Classes
{
    public class MatchExtended
    {
        public Match Match { get; set; }

        public List<PartialResult> PartialResults { get; set; }

        public Result MatchResult { get; set; }
    }
}
