// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SportEvent.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The sport event.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Classes.Classes
{
    using System;

    /// <summary>
    /// The sport event.
    /// </summary>
    public class SportEvent
    {
        /// <summary>
        /// Gets or sets the event id.
        /// </summary>
        public int EventId { get; set; }

        /// <summary>
        /// Gets or sets the event organizer id.
        /// </summary>
        public int? EventOrganizerId { get; set; }

        /// <summary>
        /// Gets or sets the activity id.
        /// </summary>
        public int? ActivityId { get; set; }

        /// <summary>
        /// Gets or sets the event name.
        /// </summary>
        public string EventName { get; set; }

        /// <summary>
        /// Gets or sets the event location.
        /// </summary>
        public string EventLocation { get; set; }

        /// <summary>
        /// Gets or sets the event district.
        /// </summary>
        public string EventDistrict { get; set; }

        /// <summary>
        /// Gets or sets the activity name.
        /// </summary>
        public string ActivityName { get; set; }

        /// <summary>
        /// Gets or sets the event organization name.
        /// </summary>
        public string EventOrganizationName { get; set; }

        /// <summary>
        /// Gets or sets the event date start.
        /// </summary>
        public DateTime? EventDateStart { get; set; }

        /// <summary>
        /// Gets or sets the event date end.
        /// </summary>
        public DateTime? EventDateEnd { get; set; }

        /// <summary>
        /// Gets or sets the event time start.
        /// </summary>
        public int? EventTimeStart { get; set; }

        /// <summary>
        /// Gets or sets the event time end.
        /// </summary>
        public int? EventTimeEnd { get; set; }

        /// <summary>
        /// Gets or sets the administrative org id.
        /// </summary>
        public int AdministrativeOrgId { get; set; }

        /// <summary>
        /// Gets or sets the administrative org name.
        /// </summary>
        public string AdministrativeOrgName { get; set; }

        /// <summary>
        /// Gets or sets the administrative org short name.
        /// </summary>
        public string AdministrativeOrgShortName { get; set; }

        /// <summary>
        /// Gets or sets the parent activity.
        /// </summary>
        public string ParentActivity { get; set; }

        /// <summary>
        /// Gets or sets the parent activity id.
        /// </summary>
        public int ParentActivityId { get; set; }

        /// <summary>
        /// Gets or sets the class exercise id.
        /// </summary>
        public int ClassExerciseId { get; set; }

        /// <summary>
        /// Gets or sets the class exercise name.
        /// </summary>
        public string ClassExerciseName { get; set; }

        /// <summary>
        ///     Gets or sets the venue unit name
        /// </summary>
        public string VenueUnitName { get; set; }

        /// <summary>
        ///     Gets or sets the venue unit id
        /// </summary>
        public int VenueUnitId { get; set; }

        /// <summary>
        ///     Gets or sets the Start Council
        /// </summary>
        public string StartCouncil { get; set; }

        /// <summary>
        ///     Gets or sets the End Council
        /// </summary>
        public string EndCouncil { get; set; }

        /// <summary>
        ///     Gets or sets the Council Id
        /// </summary>
        public int CounsilId { get; set; }

        /// <summary>
        ///     Gets or sets the Modified Date value
        /// </summary>
        public DateTime ModifiedDate { get; set; }

        /// <summary>
        ///     Gets or sets The Change Type
        /// </summary>
        public string ChangeType { get; set; }

        /// <summary>
        ///     Gets or sets The Entity Type
        /// </summary>
        public string EntityType { get; set; }

        /// <summary>
        ///     Gets or sets The Event Type Name
        /// </summary>
        public string EventTypeName { get; set; }

        /// <summary>
        ///     Gets or sets The Event Type Id
        /// </summary>
        public int EventTypeId { get; set; }

        /// <summary>
        ///     Gets or sets the Region Id
        /// </summary>
        public int RegionId { get; set; }
    }
}