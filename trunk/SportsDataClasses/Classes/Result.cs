﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Result.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Classes.Classes
{
    /// <summary>
    /// The result.
    /// </summary>
    public class Result
    {
        /// <summary>
        /// Gets or sets the match id.
        /// </summary>
        public int MatchId { get; set; }

        /// <summary>
        /// Gets or sets the away goals.
        /// </summary>
        public int? AwayGoals { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether away team not met.
        /// </summary>
        public bool AwayTeamNotMet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether away team walkover.
        /// </summary>
        public bool AwayTeamWalkover { get; set; }

        /// <summary>
        /// Gets or sets the counting away goals.
        /// </summary>
        public int? CountingAwayGoals { get; set; }

        /// <summary>
        /// Gets or sets the counting away points.
        /// </summary>
        public int? CountingAwayPoints { get; set; }

        /// <summary>
        /// Gets or sets the counting base away points.
        /// </summary>
        public int? CountingBaseAwayPoints { get; set; }

        /// <summary>
        /// Gets or sets the counting bonus away points.
        /// </summary>
        public int? CountingBonusAwayPoints { get; set; }

        /// <summary>
        /// Gets or sets the counting base home points.
        /// </summary>
        public int? CountingBaseHomePoints { get; set; }

        /// <summary>
        /// Gets or sets the home goals.
        /// </summary>
        public int? HomeGoals { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether home team not met.
        /// </summary>
        public bool HomeTeamNotMet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether home team walkover.
        /// </summary>
        public bool HomeTeamWalkover { get; set; }

        /// <summary>
        /// Gets or sets the counting home goals.
        /// </summary>
        public int? CountingHomeGoals { get; set; }

        /// <summary>
        /// Gets or sets the counting home points.
        /// </summary>
        public int? CountingHomePoints { get; set; }

        /// <summary>
        /// Gets or sets the counting bonus home points.
        /// </summary>
        public int? CountingBonusHomePoints { get; set; }

        /// <summary>
        /// Gets or sets the counting match result.
        /// </summary>
        public string CountingMatchResult { get; set; }

        /// <summary>
        /// Gets or sets the result status id.
        /// </summary>
        public int ResultStatusId { get; set; }

        /// <summary>
        /// Gets or sets the result id.
        /// </summary>
        public int ResultId { get; set; }

        /// <summary>
        /// Gets or sets the result type id.
        /// </summary>
        public int? ResultTypeId { get; set; }

        /// <summary>
        /// Gets or sets the counting result type id.
        /// </summary>
        public int? CountingResultTypeId { get; set; }

        /// <summary>
        /// Gets or sets the match result.
        /// </summary>
        public string MatchResult { get; set; }

        /// <summary>
        /// Gets or sets the specators
        /// </summary>
        public int Specators { get; set; }
    }
}