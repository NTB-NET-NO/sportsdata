﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsData.Classes.Classes
{
    public class District
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string CoAddress { get; set; }

        public string Email { get; set; }

        public string Fax { get; set; }

        public string Homepage { get; set; }

        public string Phone { get; set; }

        public string PostalCode { get; set; }

        public int SportId { get; set; }
    }
}
