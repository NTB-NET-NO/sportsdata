﻿namespace NTB.SportsData.Classes.Classes
{
    public class ActivityArea
    {
        public int ActivityAreaId { get; set; }

        public string ActivityAreaName { get; set; }

        public int ActivityId { get; set; }

        public string ActivityName { get; set; }

        public int VenueUnitId { get; set; }
    }
}
