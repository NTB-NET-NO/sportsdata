﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsData.Classes.Classes
{
    public class OutputFolder
    {
        public string OutputPath { get; set; }

        public int? DisciplineId { get; set; }

        public int OrgId { get; set; }

        public int SportId { get; set; }

        public string Type { get; set; }

        public bool Result { get; set; }

        public string Product { get; set; }

        public string PostFix { get; set; }

        public string PreFix { get; set; }
    }
}
