﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClassExercise.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The class exercise.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Classes.Classes
{
    /// <summary>
    /// The class exercise.
    /// </summary>
    public class ClassExercise
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }
    }
}