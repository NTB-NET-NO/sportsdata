﻿namespace NTB.SportsData.Classes.Classes
{
    /// <summary>
    /// The country.
    /// </summary>
    public class Country
    {
        /// <summary>
        /// Gets or sets the id of the country
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the name of the country
        /// </summary>
        public string Name { get; set; }
    }
}
