﻿using System;

namespace NTB.SportsData.Classes.Classes
{
    public class MatchResult
    {
        public int? AwayTeamGoals { get; set; }

        public string Comment { get; set; }

        public int? HomeTeamGoals { get; set; }

        public DateTime? LastChangeDate { get; set; }

        public int MatchId { get; set; }

        public bool MatchResultSetInFiks { get; set; }

        public int ResultId { get; set; }

        public int ResultTypeId { get; set; }

        public string ResultTypeName { get; set; }
    }
}
