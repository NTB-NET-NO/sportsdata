﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Organization.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Classes.Classes
{
    /// <summary>
    ///     The organization.
    /// </summary>
    public class Federation
    {
        /// <summary>
        /// Gets or sets the discipline id.
        /// </summary>
        public int DisciplineId { get; set; }

        /// <summary>
        /// Gets or sets the discipline name.
        /// </summary>
        public string DisciplineName { get; set; }

        /// <summary>
        ///     Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Gets or sets the name short.
        /// </summary>
        public string NameShort { get; set; }

        /// <summary>
        ///     Gets or sets the single sport.
        /// </summary>
        public int SingleSport { get; set; }

        /// <summary>
        ///     Gets or sets the sport.
        /// </summary>
        public string Sport { get; set; }

        /// <summary>
        ///     Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        ///     Gets or sets the team sport.
        /// </summary>
        public int TeamSport { get; set; }
    }
}