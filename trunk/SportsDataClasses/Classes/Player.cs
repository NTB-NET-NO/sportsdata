﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsData.Classes.Classes
{
    public class Player
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int TeamId { get; set; }

        public string TeamCaptain { get; set; }

        public int ClubId { get; set; }

        public string ClubName { get; set; }

        public string Debutant { get; set; }

        public int MatchId { get; set; }

        public string MatchIndividualType { get; set; }

        public int MatchIndividualTypeId { get; set; }

        public string TeamName { get; set; }

        public string TeamNo { get; set; }

        public string PlayerShirtNumber { get; set; }

        public string Position { get; set; }

        public int PositionId { get; set; }

        public string Substitute { get; set; }

        public int SubstitutedWithPlayerId { get; set; }

        public string Nationality { get; set; }

        public string Played { get; set; }

        public string PlayerOfTheMatch { get; set; }

        public string StartingLineUp { get; set; }

        public string ViceCaptain1 { get; set; }

        public string ViceCaptain2 { get; set; }

        public int SquadIndividualCategoryId { get; set; }

        public string SquadIndividualCategoryName { get; set; }
    }
}
