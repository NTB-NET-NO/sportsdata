﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsData.Classes.Classes
{
    public class VenueExtended
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int LocalCouncilId { get; set; }

        public string LocalCouncilName { get; set; }

        public string Place { get; set; }

        public string Number { get; set; }
    }
}
