﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsData.Classes.Classes
{
    public class Sport
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
