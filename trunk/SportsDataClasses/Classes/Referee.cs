﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsData.Classes.Classes
{
    public class Referee
    {
        public int Id { get; set; }
        
        public string RefereeName { get; set; }
        
        public int RefereeTypeId { get; set; }
        
        public string RefereeType { get; set; }
        
        public int ClubId { get; set; }
        
        public string ClubName { get; set;}
        
        public string FirstName { get; set; }
        
        public string LastName { get; set; }

        public int RefereeTaskId { get; set; }
    }
}
