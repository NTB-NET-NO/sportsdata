﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsData.Classes.Classes
{
    public class EventClass
    {
        /// <summary>
        ///     Gets or sets the id of this class
        /// </summary>
        public int ClassId { get; set; }

        /// <summary>
        ///     Gets or sets the name of this class
        /// </summary>
        public string ClassName { get; set; }

        // Set the name of the event
        public string EventName { get; set; }

        // ... and the id
        public int EventId { get; set; }
    }
}
