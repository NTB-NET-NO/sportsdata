﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsData.Classes.Enums;

namespace NTB.SportsData.Classes.Classes
{
    public class AlertReceiver
    {
        public AlertLevel AlertLevel { get; set; }

        public string Receiver { get; set; }
    }
}
