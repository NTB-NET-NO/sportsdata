﻿using System;

namespace NTB.SportsData.Classes.Classes
{
    public class TournamentTable
    {
        public int MatchId { get; set; }

        public int AwayPoints { get; set; }

        public int HomePoints { get; set; }

        public int NoAgainst { get; set; }

        public int NoAwayWins { get; set; }

        public int NoAwayPartialGoals { get; set; }

        public int NoAwayPartialAgainst { get; set; }

        public int NoAwayMatches { get; set; }

        public int NoAwayLosses { get; set; }

        public int NoAwayGoals { get; set; }

        public int NoAwayDraws { get; set; }

        public int NoAwayAgainst { get; set; }

        public int NoAwayPoints { get; set; }

        public int NoDiff { get; set; }

        public int NoDraws { get; set; }

        public int NoGoals { get; set; }

        public int NoHomeAgainst { get; set; }

        public int NoHomeDraws { get; set; }

        public int NoHomeGoals { get; set; }

        public int NoHomeLosses { get; set; }
        
        public int NoHomeMatches { get; set; }

        public int NoHomePoints { get; set; }
        
        public int NoHomePartialAgainst { get; set; }
        
        public int NoHomePartialGoals { get; set; }
        
        public int NoHomeWins { get; set; }
        
        public int NoLosses { get; set; }
        
        public int NoMatches { get; set; }
        
        public int NoPartialAgainst { get; set; }
        
        public int NoPartialGoals { get; set; }
        
        public int NoWins { get; set; }
        
        public int OrderIdentity { get; set; }
        
        public int OrgElementId { get; set; }
        
        public int Points { get; set; }
        
        public int SuddenDeathLosses { get; set; }
        
        public int SuddenDeathLossesAway { get; set; }
        
        public int SuddenDeathLossesHome { get; set; }
        
        public int SuddenDeathWins { get; set; }
        
        public int SuddenDeathWinsAway { get; set; }
        
        public int SuddenDeathWinsHome { get; set; }

        public int TournamentId { get; set; }
        
        public string TeamName { get; set; }

        public string TeamNameInTournament { get; set; }
        
        public int TeamId { get; set; }

        public string DiffString { get; set; }

        public int MatchClassId { get; set; }

        public int MatchGroupId { get; set; }

        public int? TablePosition { get; set; }

        public int TournamentTeamId { get; set; }

        public int? AwayGoalDifference { get; set; }

        public int? HomeGoalDifference { get; set; }

        public int TotalGoalsAgainst { get; set; }

        public int? TotalGoalDifference { get; set; }

        public DateTime? LastChangedDate { get; set; }
    }
}