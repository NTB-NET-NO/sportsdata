﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsData.Classes.Classes
{
    public class MatchIncident
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string IncidentShort { get; set; }

        public string IncidentType { get; set; }

        public int IncidentTypeId { get; set; }

        public int MatchId { get; set; }

        public int Period { get; set; }

        public string Reason { get; set; }

        public int Time { get; set; }

        public int Value { get; set; }

        public int TeamId { get; set; }

        public Player Player { get; set; }

        public int MatchIncidentId { get; set; }

        public string IncidentSubType { get; set; }

        public int IncidentSubTypeId { get; set; }

        public int PersonId { get; set; }

        public int ParentId { get; set; }

        public int PlayerId { get; set; }

        public string ConnectedToEvent { get; set; }
    }
}
