﻿namespace NTB.SportsData.Classes.Classes
{
    public class AgeCategory
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int FromAge { get; set; }

        public int ToAge { get; set; }

        public int FederationId { get; set; }

        public int SportId { get; set; }

        public string ShortName { get; set; }

        public string ClassCode { get; set; }

        public int GenderId { get; set; }

        public int AgeCategoryDefinitionId { get; set; }

        public string AgeCategoryDefinition { get; set; }

        public int TournamentId { get; set; }
    }
}
