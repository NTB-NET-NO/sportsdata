﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchEvent.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Classes.Classes
{
    /// <summary>
    /// The match event.
    /// </summary>
    public class MatchEvent
    {
        /// <summary>
        /// Gets or sets the away goals.
        /// </summary>
        public int? AwayGoals { get; set; }

        /// <summary>
        /// Gets or sets the comment.
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Gets or sets the connected to event.
        /// </summary>
        public string ConnectedToEvent { get; set; }

        /// <summary>
        /// Gets or sets the home goals.
        /// </summary>
        public int? HomeGoals { get; set; }

        /// <summary>
        /// Gets or sets the match event id.
        /// </summary>
        public int MatchEventId { get; set; }

        /// <summary>
        /// Gets or sets the match event type.
        /// </summary>
        public string MatchEventType { get; set; }

        /// <summary>
        /// Gets or sets the match event type id.
        /// </summary>
        public int MatchEventTypeId { get; set; }

        /// <summary>
        /// Gets or sets the match id.
        /// </summary>
        public int MatchId { get; set; }

        /// <summary>
        /// Gets or sets the minute.
        /// </summary>
        public int Minute { get; set; }

        /// <summary>
        /// Gets or sets the player id.
        /// </summary>
        public int PlayerId { get; set; }

        /// <summary>
        /// Gets or sets the player name.
        /// </summary>
        public string PlayerName { get; set; }

        /// <summary>
        /// Gets or sets the second yellow card.
        /// </summary>
        public bool SecondYellowCard { get; set; }

        /// <summary>
        /// Gets or sets the team id.
        /// </summary>
        public int TeamId { get; set; }

        /// <summary>
        /// Gets or sets the team name.
        /// </summary>
        public string TeamName { get; set; }

        public int PersonInfoHidden { get; set; }

        public string MatchEventShort { get; set; }

        public int PersonId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int? Time { get; set; }

        public int? ParentId { get; set; }

        public int Value { get; set; }
    }
}