﻿namespace NTB.SportsData.Classes.Classes
{
    public class Municipality
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int DistrictId { get; set; }

        public int SportId { get; set; }
    }
}