﻿
using System;

namespace NTB.SportsData.Classes.Classes
{
    public class Tournament
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int SportId { get; set; }

        public int Number { get; set; }

        public int SeasonId { get; set; }

        public int TypeId { get; set; }
        
        public int DistrictId { get; set; }

        public int DisciplineId { get; set; }

        public int AgeCategoryId { get; set; }

        public int AgeCategoryDefinitionId { get; set; }

        public int GenderId { get; set; }

        public string StatusCode { get; set; }

        public bool NationalTeamTournament { get; set; }

        public int Division { get; set; }

        public bool Push { get; set; }

        public string TournamentNumber { get; set; }

        public int TournamentTypeId { get; set; }

        public int? Group { get; set; }

        public int HalfTimeBreakInMin { get; set; }

        public DateTime? LastChangedDate { get; set; }

        public int MatchPeriodDurationInMin { get; set; }

        public int ResultReportingMaxNumberOfGoals { get; set; }

        public int NumberOfTeamsQualifiedForRelegation { get; set; }

        public int NumberOfTeamsQualifiedForPromotion { get; set; }

        public int? NumberOfTeams { get; set; }

        public int NumberOfRelegatedTeams { get; set; }

        public int NumberOfPromotedTeams { get; set; }

        public int NumberOfMatchPeriods { get; set; }

        public bool PublishResult { get; set; }

        public bool PublishTournamentTable { get; set; }

        public bool PushChanges { get; set; }

        public bool ResultReportingAllowed { get; set; }

        public int? TournamentAgeCategory { get; set; }

        public int ResultReportingDaysOpenForRegistrationBeforeMatchStart { get; set; }

        public string TournamentTableComment { get; set; }

        public DateTime? StartDateFirstTournamentRound { get; set; }

        public int TournamentStatusId { get; set; }

        public AgeCategory AgeCategory { get; set; }

        public string SportName { get; set; }
    }
}
