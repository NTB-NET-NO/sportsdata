﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsData.Classes.Classes
{
    public class EventType
    {
        public int EventTypeId { get; set; }

        public string EventTypeName { get; set; }
    }
}
