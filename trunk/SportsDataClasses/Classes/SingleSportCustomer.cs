﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsData.Classes.Classes
{
    public class SingleSportCustomer
    {
        /// <summary>
        ///     The Customer Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        ///     The Name of the customer
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        ///     The remote Customer Id 
        /// </summary>
        public int RemoteCustomerId { get; set; }

        /// <summary>
        ///     List of regions for this customer
        /// </summary>
        public List<CustomerRegion> CustomerRegions { get; set; }
    }
}
