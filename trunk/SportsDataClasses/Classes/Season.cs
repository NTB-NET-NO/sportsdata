using System;

namespace NTB.SportsData.Classes.Classes
{
    public class Season
    {
        public int Id { get; set; }
        
        public string Name { get; set; }
        
        public DateTime StartDate { get; set; }
        
        public DateTime EndDate { get; set; }
        
        public int SportId { get; set; }

        public int DisciplineId { get; set; }
        
        public bool Active { get; set; }

        public int StatusId { get; set; }
        
    }
}
