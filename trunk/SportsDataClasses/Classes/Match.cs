﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Match.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.SportsData.Classes.Classes
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    ///     The match.
    /// </summary>
    public class Match
    {
        /// <summary>
        ///     Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        ///     Gets or sets the activity id.
        /// </summary>
        public int ActivityId { get; set; }

        /// <summary>
        ///     Gets or sets the class code no.
        /// </summary>
        public string ClassCodeNo { get; set; }

        /// <summary>
        ///     Gets or sets the class code name.
        /// </summary>
        public string ClassCodeName { get; set; }

        /// <summary>
        ///     Gets or sets the match no.
        /// </summary>
        public string MatchNo { get; set; }

        /// <summary>
        ///     Gets or sets the match date.
        /// </summary>
        public DateTime? MatchDate { get; set; }

        /// <summary>
        ///     Gets or sets the match end time.
        /// </summary>
        public int MatchEndTime { get; set; }

        /// <summary>
        ///     Gets or sets the match start time.
        /// </summary>
        public int MatchStartTime { get; set; }

        /// <summary>
        ///     Gets or sets the match type id.
        /// </summary>
        public int MatchTypeId { get; set; }

        /// <summary>
        ///     Gets or sets the round id.
        /// </summary>
        public int RoundId { get; set; }

        /// <summary>
        ///     Gets or sets the round name.
        /// </summary>
        public string RoundName { get; set; }

        /// <summary>
        ///     Gets or sets the tournament id.
        /// </summary>
        public int TournamentId { get; set; }

        /// <summary>
        ///     Gets or sets the tournament id.
        /// </summary>
        public int TournamentRoundNumber { get; set; }

        /// <summary>
        ///     Gets or sets the tournament id.
        /// </summary>
        public int TournamentRoundName { get; set; }

        /// <summary>
        ///     Gets or sets the venue unit id.
        /// </summary>
        public int VenueUnitId { get; set; }

        /// <summary>
        ///     Gets or sets the venue name.
        /// </summary>
        public string VenueName { get; set; }

        /// <summary>
        ///     Gets or sets the spectators.
        /// </summary>
        public int Spectators { get; set; }

        /// <summary>
        ///     Gets or sets the home team id.
        /// </summary>
        public int HomeTeamId { get; set; }

        /// <summary>
        ///     Gets or sets the home team name.
        /// </summary>
        public string HomeTeamName { get; set; }

        /// <summary>
        ///     Gets or sets the home team goals.
        /// </summary>
        public int? HomeTeamGoals { get; set; }

        /// <summary>
        ///     Gets or sets the away team id.
        /// </summary>
        public int AwayTeamId { get; set; }

        /// <summary>
        ///     Gets or sets the away team name.
        /// </summary>
        public string AwayTeamName { get; set; }

        /// <summary>
        ///     Gets or sets the away team goals.
        /// </summary>
        public int? AwayTeamGoals { get; set; }

        /// <summary>
        ///     Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether downloaded.
        /// </summary>
        public bool Downloaded { get; set; }

        /// <summary>
        ///     Gets or sets the partial result.
        /// </summary>
        public string PartialResult { get; set; }

        /// <summary>
        ///     Gets or sets the match result.
        /// </summary>
        public string MatchResult { get; set; }

        /// <summary>
        ///     Gets or sets the result formatted.
        /// </summary>
        public string ResultFormatted { get; set; }

        /// <summary>
        ///     Gets or sets the home team players.
        /// </summary>
        public List<Player> HomeTeamPlayers { get; set; }

        /// <summary>
        ///     Gets or sets the away team players.
        /// </summary>
        public List<Player> AwayTeamPlayers { get; set; }

        /// <summary>
        ///     Gets or sets the match players.
        /// </summary>
        public List<Player> MatchPlayers { get; set; }

        /// <summary>
        ///     Gets or sets the match incidents.
        /// </summary>
        public List<MatchIncident> MatchIncidents { get; set; }

        /// <summary>
        ///     Gets or sets the referees.
        /// </summary>
        public List<Referee> Referees { get; set; }

        /// <summary>
        ///     Gets or sets the status code.
        /// </summary>
        public string StatusCode { get; set; }

        /// <summary>
        ///     Gets or sets the away club id.
        /// </summary>
        public int AwayClubId { get; set; }

        /// <summary>
        ///     Gets or sets the away club name.
        /// </summary>
        public string AwayClubName { get; set; }

        /// <summary>
        ///     Gets or sets the match name.
        /// </summary>
        public string MatchName { get; set; }

        /// <summary>
        ///     Gets or sets the match date string.
        /// </summary>
        public string MatchDateString { get; set; }

        /// <summary>
        ///     Gets or sets the match comment.
        /// </summary>
        public string MatchComment { get; set; }

        /// <summary>
        ///     Gets or sets the home club name.
        /// </summary>
        public string HomeClubName { get; set; }

        /// <summary>
        ///     Gets or sets the home club id.
        /// </summary>
        public int HomeClubId { get; set; }

        /// <summary>
        ///     Gets or sets the class id.
        /// </summary>
        public int ClassId { get; set; }

        /// <summary>
        ///     Gets or sets the reason.
        /// </summary>
        public string Reason { get; set; }

        /// <summary>
        ///     Gets or sets the sort order.
        /// </summary>
        public string SortOrder { get; set; }

        /// <summary>
        ///     Gets or sets the winner.
        /// </summary>
        public string Winner { get; set; }

        /// <summary>
        ///     Gets or sets the update time stamp.
        /// </summary>
        public string UpdateTimeStamp { get; set; }

        /// <summary>
        ///     Gets or sets the update time stamp string.
        /// </summary>
        public string UpdateTimeStampString { get; set; }

        /// <summary>
        ///     Gets or sets the home team goals string.
        /// </summary>
        public string HomeTeamGoalsString { get; set; }

        /// <summary>
        ///     Gets or sets the away team goals string.
        /// </summary>
        public string AwayTeamGoalsString { get; set; }

        /// <summary>
        ///     Gets or sets the match group id.
        /// </summary>
        public int MatchGroupId { get; set; }

        /// <summary>
        ///     Gets or sets the end game level.
        /// </summary>
        public int EndGameLevel { get; set; }

        /// <summary>
        ///     Gets or sets the result status id.
        /// </summary>
        public int ResultStatusId { get; set; }

        /// <summary>
        ///     Gets or sets the match published.
        /// </summary>
        public bool? MatchPublished { get; set; }

        /// <summary>
        ///     Gets or sets the tournament name.
        /// </summary>
        public string TournamentName { get; set; }

        /// <summary>
        ///     Gets or sets the away team short name.
        /// </summary>
        public string AwayTeamShortName { get; set; }

        /// <summary>
        ///     Gets or sets the home team short name.
        /// </summary>
        public string HomeTeamShortName { get; set; }

        /// <summary>
        ///     Gets or sets the activity area id.
        /// </summary>
        public int? ActivityAreaId { get; set; }

        /// <summary>
        ///     Gets or sets the activity area name.
        /// </summary>
        public string ActivityAreaName { get; set; }

        /// <summary>
        ///     Gets or sets the class code id.
        /// </summary>
        public int? ClassCodeId { get; set; }

        /// <summary>
        ///     Gets or sets the season id.
        /// </summary>
        public int? SeasonId { get; set; }

        /// <summary>
        ///     Gets or sets the status type id.
        /// </summary>
        public int? StatusTypeId { get; set; }

        /// <summary>
        /// Gets or sets the venue unit name.
        /// </summary>
        public string VenueUnitName { get; set; }

        /// <summary>
        /// Gets or sets the venue unit no.
        /// </summary>
        public string VenueUnitNo { get; set; }

        /// <summary>
        /// Gets or sets the tournament age category id
        /// </summary>
        public int TournamentAgeCategoryId { get; set; }

        public List<MatchEvent> MatchEventList { get; set; }

        public List<MatchResult> MatchResultList { get; set; }
    }
}