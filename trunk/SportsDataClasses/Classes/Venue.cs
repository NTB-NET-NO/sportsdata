﻿using System.Xml.Serialization;

namespace NTB.SportsData.Classes.Classes
{
    [XmlRoot("Venue")]
    public class Venue
    {
        [XmlAttribute("Id")]
        public int VenueId { get; set; }

        [XmlText]
        public string  VenueName { get; set; }

        [XmlAttribute("VenueNo")]
        public string VenueUnitNo { get; set; }

        [XmlAttribute("Latitude")]
        public double Latitude { get; set; }

        [XmlAttribute("Longitude")]
        public double Longitude { get; set; }

        [XmlAttribute("LocalCouncilId")]
        public int LocalCouncilId { get; set; }

        [XmlAttribute("LocalCouncilName")]
        public string LocalCouncilName { get; set; }

        [XmlAttribute("ZipCode")]
        public string ZipCode { get; set; }

        [XmlAttribute("City")]
        public string City { get; set; }

        [XmlAttribute("Email")]
        public string Email { get; set; }
    }
}
