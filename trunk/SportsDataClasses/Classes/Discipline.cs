﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Discipline.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Classes.Classes
{
    /// <summary>
    /// The discipline.
    /// </summary>
    public class Discipline
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the org id.
        /// </summary>
        public int OrgId { get; set; }

        #endregion
    }
}