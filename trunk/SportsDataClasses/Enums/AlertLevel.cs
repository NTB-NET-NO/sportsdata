﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.SportsData.Classes.Enums
{
    public enum AlertLevel
    {
        /// <summary>
        ///     States that the level of alert is error
        /// </summary>
        Error,

        Info,

        Fatal
    }
}
