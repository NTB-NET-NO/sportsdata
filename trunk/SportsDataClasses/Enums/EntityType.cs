﻿namespace NTB.SportsData.Classes.Enums
{
    public enum EntityType
    {
        Unknown = 0,
        
        Person = 1,
        
        Organization = 2,
        
        Function = 3,

        Qualification = 4,

        Course = 5,

        Event = 6,

        License = 7,

        Venue = 8,

        Result = 9,

        MatchIncident = 10,

        Tournament = 11,

        Team = 12,

        Match = 13
    }
}