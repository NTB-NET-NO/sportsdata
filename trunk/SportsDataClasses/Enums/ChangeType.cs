﻿namespace NTB.SportsData.Classes.Enums
{
    public enum ChangeType
    {
        Unknown = 0,
        Created = 1,
        Modified = 2,
        Deleted = 3
    }
}