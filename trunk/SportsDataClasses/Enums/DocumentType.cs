namespace NTB.SportsData.Classes.Enums
{
    public enum DocumentType
    {
        /// <summary>
        ///     Results has the result of the match
        /// </summary>
        Result,

        /// <summary>
        ///     Standing has the result and also the standing table
        /// </summary>
        Standing,

        /// <summary>
        ///     MatchFacts also has more information about the match
        /// </summary>
        MatchFact,

        /// <summary>
        ///     Schedule only has information about the match (start and end)
        /// </summary>
        Schedule
    }
}


