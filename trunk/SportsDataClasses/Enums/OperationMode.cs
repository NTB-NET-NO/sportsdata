namespace NTB.SportsData.Classes.Enums
{
    /// <summary>
    /// Used to specify the different operating modes. Primarily used for the common EWS poller component.
    /// </summary>
    public enum OperationMode
    {
        /// <summary>
        /// Gatherer mode collects Data web services, saves the data in an XML-file 
        /// </summary>
        Gatherer,
        
        /// <summary>
        /// Purge mode is where we delete the data in the database. 
        /// </summary>
        Purge,

        /// <summary>
        /// Distributor mode makes the document ready for distribution to customers. If must find out which customer shall have what data and what format
        /// </summary>
        Distributor,

        /// <summary>
        /// Hold mode makes the document ready for distribution to customers, but holds it until the time of distribution has hit.
        /// </summary>
        Hold,

        /// <summary>
        /// Update mode checks tournaments against municipalities and adds to the customer profile. We are adding with accepted false so that an administrator can go in and confirm the jobs
        /// </summary>
        Update,

        /// <summary>
        /// Check mode checks for matches that has not been downloaded. It is sort of also a maintenance mode
        /// </summary>
        Maintenance,

        /// <summary>
        /// Mode to be used when we are to get latest sports events from NIF
        /// </summary>
        SynchDistributor

    }
}