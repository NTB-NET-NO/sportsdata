﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Mail.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The mail.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Net.Mail;

    using log4net;

    /// <summary>
    /// The mail.
    /// </summary>
    public static class Mail
    {
        // This class shall be used to send out mail messages when there is an exception

        /// <summary>
        /// Static logger
        /// </summary>
        public static readonly ILog Logger = LogManager.GetLogger(typeof(Mail));

        /// <summary>
        /// The send mail.
        /// </summary>
        /// <param name="mailMessage">
        /// The mail message.
        /// </param>
        public static void SendMail(string mailMessage)
        {
            var fromAddress = ConfigurationManager.AppSettings["SMTPFromAddress"];
            var toAddress = ConfigurationManager.AppSettings["SMTPToAddress"];
            var smtpServer = ConfigurationManager.AppSettings["SMTPServer"];
            var addresses = new List<string>();

            if (!toAddress.Contains(";"))
            {
                addresses.Add(toAddress);
            }
            else
            {
                var toAddresses = toAddress.Split(';');

                addresses.AddRange(toAddresses);
            }

            foreach (var message in addresses.Select(address => new MailMessage(fromAddress, address)))
            {
                message.Subject = "Error in SportsData Service";
                message.Body = mailMessage;

                var client = new SmtpClient(smtpServer);
                try
                {
                    client.Send(message);
                }
                catch (Exception sendException)
                {
                    Logger.Error(sendException.Message);
                    Logger.Error(sendException.StackTrace);
                }
            }
        }

        /// <summary>
        /// The send exception.
        /// </summary>
        /// <param name="exception">
        /// The exception.
        /// </param>
        public static void SendException(Exception exception)
        {
            var fromAddress = ConfigurationManager.AppSettings["SMTPFromAddress"];
            var toAddress = ConfigurationManager.AppSettings["SMTPToAddress"];
            var smtpServer = ConfigurationManager.AppSettings["SMTPServer"];
            var addresses = new List<string>();

            if (!toAddress.Contains(";"))
            {
                addresses.Add(toAddress);
            }
            else
            {
                var toAddresses = toAddress.Split(';');

                addresses.AddRange(toAddresses);
            }

            foreach (var message in addresses.Select(address => new MailMessage(fromAddress, address)))
            {
                var mailMessage = "An error has occured in SportsData Service: " + exception.Message + Environment.NewLine;
                mailMessage += "Stack Trace: " + exception.StackTrace + Environment.NewLine;

                if (exception.InnerException != null)
                {
                    mailMessage += "Inner Exception: " + exception.InnerException.Message + Environment.NewLine;
                    mailMessage += exception.InnerException.StackTrace + Environment.NewLine;
                }

                message.Subject = "Error in SportsData Service";
                message.Body = mailMessage;

                var client = new SmtpClient(smtpServer);
                try
                {
                    client.Send(message);
                }
                catch (Exception sendException)
                {
                    Logger.Error(sendException.Message);
                    Logger.Error(sendException.StackTrace);
                }
            }
        }
    }
}