﻿using System;
using System.Globalization;


// Adding support for log4net
using log4net;

namespace NTB.SportsData.Utilities
{
    public static class Utilities
    {
        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(Utilities));

        

        /// <summary>
        /// Initializes the <see cref="Utilities"/> class.
        /// </summary>
        static Utilities()
        {
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
                log4net.Config.BasicConfigurator.Configure();

            //Load watermark cache
            //iptcSequenceFile = ConfigurationManager.AppSettings["IPTCSequenceCache"];
            //if (!String.IsNullOrEmpty(iptcSequenceFile))
            //{
            //    Exception ex;
            //    IPTCSequenceCache.LoadFromFile(iptcSequenceFile, out iptcSequenceCache, out ex);
            //    if (ex != null)
            //    {
            //        logger.WarnFormat("Utilities::Utilities() Failed to load IPTCSequence cache {0}: {1}", iptcSequenceFile, ex.Message);
            //        iptcSequenceCache = new IPTCSequenceCache();
            //    }
            //    logger.InfoFormat("Utilities::Utilities() IPTCSequence cache {0} contains {1} items", iptcSequenceFile, iptcSequenceCache.IPTCSequence.Count);
            //}
        }


        

        /// <summary>
        /// Calculate the next time a <c>Scheduled</c> job should run
        /// </summary>
        /// <param name="schedule">The configured schedule to parse.</param>
        /// <returns>
        /// A TimeSpan that indicates how long is until the next Scheduled run.
        /// </returns>
        /// <remarks>
        /// The function is used to calculate run times for <c>Scheduled</c> jobs. It takes a time definition, which is a
        /// formatted time string, and tries to parse it as a time of day. If unsuccessfull 01:00 is used. A TimeSpan between now and the
        /// time of day parsed time of day is calculcated. It is used directly as a polling delay to ensure that a job is executed at the given time.
        /// </remarks>
        public static TimeSpan GetScheduleInterval(String schedule)
        {
            //Vars
            DateTime tmp;
            DateTime now = DateTime.Now;

            try
            {
                //Try to parse the time given
                tmp = DateTime.Parse(schedule, CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                //Default to 01:00
                Logger.Warn("ComponentUtils::SetScheduleInterval() failed to parse schedule string: " + schedule, ex);
                tmp = DateTime.Parse("01:00");
            }

            //Determine next runtime
            DateTime next = new DateTime(now.Year, now.Month, now.Day, tmp.Hour, tmp.Minute, 0);

            //Add a day if we passed for today
            if (tmp.Hour < now.Hour ||
                 (tmp.Hour == now.Hour && tmp.Minute <= now.Minute))
            {
                next = next.AddDays(1);
            }

            //Calculated actuall interval
            TimeSpan interval = next - now;
            Logger.DebugFormat("ComponentUtils::GetScheduleInterval() calculated next scheduled poll: {0}", next.ToString("yyyy-MM-dd HH:mm:ss"));
            Logger.DebugFormat("ComponentUtils::GetScheduleInterval() interval milliseconds: {0}", interval.TotalMilliseconds);
            return interval;
        }
    }
}
