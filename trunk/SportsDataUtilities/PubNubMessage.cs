﻿using System.Collections.Generic;

namespace NTB.SportsData.Utilities
{
    public class PubNubMessage 
    {
        public string MessageType { get; set; }
        public string ObjectType { get; set; }
        public string ObjectId { get; set; }
        public Dictionary<string, string> Properties { get; set; }
        public string TimeStamp { get; set; }
        public string ErrorMessage { get; set; }
        public string ErrorNumber { get; set; }

        // This shall get the token for last message
        public long ServerStamp { get; set; }



        ////private List<PubNubMessage> PubNubMessages = new List<PubNubMessage>();
        ////public IEnumerator<PubNubMessage> GetEnumerator()
        ////{
        ////    return this.PubNubMessages.GetEnumerator();
        ////}

        ////IEnumerator IEnumerable.GetEnumerator()
        ////{
        ////    return GetEnumerator();
        ////    // throw new NotImplementedException();
        ////}

        ////public IEnumerator GetEnumerator()
        ////{
        ////    return (IEnumerator)this;

        ////    // throw new NotImplementedException();
        ////}

        //public IEnumerator<PubNubMessage> GetEnumerator()
        //{
        //    return this.GetEnumerator();
        //}

        //IEnumerator IEnumerable.GetEnumerator()
        //{
        //    return GetEnumerator();
        //}
    }
}
