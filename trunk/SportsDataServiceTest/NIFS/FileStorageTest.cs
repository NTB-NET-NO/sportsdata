﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nifs.Classes.SportsMl;
using NTB.SportsData.Components.Nifs.Storages;

namespace NTB.SportsData.Test.NIFS
{
    [TestClass]
    public class FileStorageTest
    {
        [TestMethod]
        public void TestGetVersionNumber()
        {
            var fileStorage = new FileStorage();

            var sportsContent = new SportsContent();
            sportsContent.SportsMetaData = new SportsMetaData();
            sportsContent.SportsMetaData.SportsContentCodes = new SportsContentCodes();
            sportsContent.SportsMetaData.SportsContentCodes.SportsContentCode = new SportsContentCode[3];

            var versionSportsContentCode = new SportsContentCode();
            versionSportsContentCode.CodeType = "ntbspcode:version";
            versionSportsContentCode.CodeName = "10";
            versionSportsContentCode.CodeKey = "v:10";
            
            var scc1 = new SportsContentCode();
            var scc2 = new SportsContentCode();

            sportsContent.SportsMetaData.SportsContentCodes.SportsContentCode[0] = scc1;
            sportsContent.SportsMetaData.SportsContentCodes.SportsContentCode[1] = scc2;
            sportsContent.SportsMetaData.SportsContentCodes.SportsContentCode[2] = versionSportsContentCode;


            var actual = fileStorage.GetVersionNumber(sportsContent);
            const string expected = "10";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestGetVersionNumber_TwoDifferentCodes()
        {
            var fileStorage = new FileStorage();

            var sportsContent = new SportsContent();
            sportsContent.SportsMetaData = new SportsMetaData();
            sportsContent.SportsMetaData.SportsContentCodes = new SportsContentCodes();
            sportsContent.SportsMetaData.SportsContentCodes.SportsContentCode = new SportsContentCode[3];

            var versionSportsContentCode = new SportsContentCode();
            versionSportsContentCode.CodeType = "ntbspcode:version";
            versionSportsContentCode.CodeName = "10";
            versionSportsContentCode.CodeKey = "v:10";

            var scc1 = new SportsContentCode();
            scc1.CodeType = "ntbspcode:somecode";
            scc1.CodeName = "burp";
            scc1.CodeKey = "somecode:burp";
            var scc2 = new SportsContentCode();
            scc2.CodeType = "ntbspcode:someothercode";
            scc2.CodeName = "fart";
            scc2.CodeKey = "code:fart";

            sportsContent.SportsMetaData.SportsContentCodes.SportsContentCode[0] = scc1;
            sportsContent.SportsMetaData.SportsContentCodes.SportsContentCode[2] = scc2;
            sportsContent.SportsMetaData.SportsContentCodes.SportsContentCode[1] = versionSportsContentCode;


            var actual = fileStorage.GetVersionNumber(sportsContent);
            const string expected = "10";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestCreateSlugLine()
        {
            var fileStorage = new FileStorage();
            var exportPath = new OutputFolder
            {
                PostFix = "resmsg"
            };

            var currentSlugLine = "fotball-premierleague-data";
            var actual = fileStorage.CreateSlugLine(exportPath, currentSlugLine);

            const string expected = "fotball-premierleague-resmsg";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestCreateSlugLine_LongDashedTournamentName()
        {
            var fileStorage = new FileStorage();
            var exportPath = new OutputFolder
            {
                PostFix = "restab"
            };

            const string currentSlugLine = "fotball-premier-really-long-name-of-league-data";
            var actual = fileStorage.CreateSlugLine(exportPath, currentSlugLine);

            const string expected = "fotball-premier-really-long-name-of-league-restab";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestCreateSlugLine_LongDashedTournamentName_WitoutDataAtEnd()
        {
            var fileStorage = new FileStorage();
            var exportPath = new OutputFolder
            {
                PostFix = "restab"
            };

            const string currentSlugLine = "fotball-premier-really-long-name-of-league";
            var actual = fileStorage.CreateSlugLine(exportPath, currentSlugLine);

            const string expected = "fotball-premier-really-long-name-of-league-restab";

            Assert.AreNotEqual(expected, actual);
        }
    }
}
