﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SyncRemoteRepositoryTest.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The sync remote repository test.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Test
{
    using System;
    using System.Configuration;
    using System.ServiceModel;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using NTB.SportsData.Components.RemoteRepositories.DataMappers;

    /// <summary>
    /// The sync remote repository test.
    /// </summary>
    [TestClass]
    public class SyncRemoteRepositoryTest
    {
        /// <summary>
        /// The get sports results for organisation.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(FaultException))]
        public void GetSportsResultsForOrganisationFails()
        {
            // this is for judo
            const string FederationKey = "12653683";
            var repository = new SynchRemoteRepository();
            var applicationKey = ConfigurationManager.AppSettings["NIFServiceKey"];
            var federationUserId = ConfigurationManager.AppSettings["NIFServiceUsername"].Replace(applicationKey, FederationKey);
            var password = ConfigurationManager.AppSettings["NIFServicePassword"];

            repository.ClientPassword = password;
            repository.FederationUserKey = federationUserId;

            var count = repository.GetSportsResultsForOrganisation().Count;

            // Assert.AreEqual(Expected, actual);
            Assert.Fail();
        }

        /// <summary>
        /// The get sports results for organisation.
        /// </summary>
        [TestMethod]
        public void GetSportsResultsForOrganisationToday()
        {
            // this is for judo
            const string FederationKey = "12653683";
            var repository = new SynchRemoteRepository();
            var applicationKey = ConfigurationManager.AppSettings["NIFServiceKey"];
            var federationUserId = ConfigurationManager.AppSettings["NIFServiceUsername"].Replace(applicationKey, FederationKey);
            var password = ConfigurationManager.AppSettings["NIFServicePassword"];

            repository.ClientPassword = password;
            repository.FederationUserKey = federationUserId;
            repository.EventEndDate = DateTime.Now;
            repository.EventStartDate = DateTime.Now.AddHours(-1);

            var actual = repository.GetSportsResultsForOrganisation().Count;

            const int Expected = 0;

            Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        /// The get sports results for organisation.
        /// </summary>
        [TestMethod]
        public void GetSportsResultsForOrganisationTestDate()
        {
            // this is for judo
            const string FederationKey = "12653683";
            var repository = new SynchRemoteRepository();
            var applicationKey = ConfigurationManager.AppSettings["NIFServiceKey"];
            var federationUserId = ConfigurationManager.AppSettings["NIFServiceUsername"].Replace(applicationKey, FederationKey);
            var password = ConfigurationManager.AppSettings["NIFServicePassword"];

            repository.ClientPassword = password;
            repository.FederationUserKey = federationUserId;

            repository.EventStartDate = DateTime.Parse("2015-10-26T11:00:00");
            repository.EventEndDate = DateTime.Parse("2015-10-26T12:00:00");

            var actual = repository.GetSportsResultsForOrganisation().Count;

            const int Expected = 38;

            Assert.AreEqual(Expected, actual);
        }
    }
}