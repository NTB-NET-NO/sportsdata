﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NTB.SportsData.Components.NIFConnectService;
using NTB.SportsData.Components.RemoteRepositories.DataMappers;

namespace NTB.SportsData.Test
{
    [TestClass]
    public class TournamentRemoteRepositoryTest
    {
        [TestMethod]
        public void GetTournamentTableFailTest()
        {
            var repository = new TournamentRemoteRepository();
            var actual = repository.GetPublicTournamentTable(368295);

            Assert.IsNull(actual);
        }
    }
}
