﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nif.Helpers;

namespace NTB.SportsData.Test
{
    [TestClass]
    public class SeasonHelperTest
    {
        [TestMethod]
        public void CheckDatesTest()
        {
            var helper = new SeasonHelper();
            helper.DisciplineId = 210;
            var listOfSeasons = new List<Season>
            {
                new Season()
                {
                    Active = true,
                    DisciplineId = 210,
                    EndDate = DateTime.Parse("2016-05-01"),
                    StartDate = DateTime.Parse("2017-06-30"),
                    Id = 200765,
                    Name = "Håndballsesongen 2016/2017",
                    SportId = 23,
                    StatusId = 1
                }
            };

            var returnVal = helper.CheckDates(listOfSeasons);
            var actual = returnVal[0].EndDate;
            var expected = DateTime.Parse("2017-06-30");

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetActiveSeasonWithDisciplineIdTest()
        {
            var helper = new SeasonHelper();
            helper.DisciplineId = 210;
            var listOfSeasons = new List<Season>
            {
                new Season()
                {
                    Active = true,
                    DisciplineId = 210,
                    StartDate = DateTime.Parse("2016-05-01"),
                    EndDate = DateTime.Parse("2017-06-30"),
                    Id = 200765,
                    Name = "Håndballsesongen 2016/2017",
                    SportId = 23,
                    StatusId = 1
                },
                new Season()
                {
                    Active = true,
                    DisciplineId = 210,
                    StartDate = DateTime.Parse("2015-07-01 00:00:00.000"),
                    EndDate = DateTime.Parse("2016-06-30 00:00:00.000"),
                    Id = 200739,
                    Name = "Håndballsesongen 2015/2016",
                    SportId = 23,
                    StatusId = 1
                },
                new Season()
                {
                    Active = true,
                    DisciplineId = 285,
                    StartDate = DateTime.Parse("2016-05-01 00:00:00.000"),
                    EndDate = DateTime.Parse("2016-12-20 00:00:00.000"),
                    Id = 200739,
                    Name = "Beach-håndball sesong 2016",
                    SportId = 23,
                    StatusId = 1
                }
            };

            var actual = helper.GetActiveSeasonWithDisciplineId(listOfSeasons).Id;
            const int expected = 200765;

            Assert.AreEqual(expected, actual);
        }
    }
}
