﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NifTeamSportXmlDocumentTest.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The nif team sport xml document test.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nif.DocumentCreators;
using NTB.SportsData.Components.Nif.Populators;

namespace NTB.SportsData.Test.NIF
{
    /// <summary>
    /// The nif team sport xml document test.
    /// </summary>
    [TestClass]
    public class TeamSportXmlDocumentTest
    {
        /// <summary>
        /// The create file name base.
        /// </summary>
        [TestMethod]
        public void CreateFileNameBase()
        {
            var dateTime = DateTime.Now;

            var tournament = new Tournament() { Id = 12345678 };

            var organisation = new Federation()
                                   {
                                       Name = "Norges Idrettsforbund",
                                       NameShort = "NIF"
                                   };

            var populator = new FileNamePopulator();

            var actual = populator.CreateFileNameBase(organisation, tournament, dateTime);

            const string expected = "NTB_SportsData_20151110_NIF_T12345678";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void CreateDateTimeString_TestingNoSpaces()
        {
            var dateTime = DateTime.Now;

            var teamSportXmlDocument = new TeamSportXmlDocumentCreator();

            var response = teamSportXmlDocument.CreateDateTimeString(dateTime);
            
            Assert.IsFalse(response.Contains(" "), "response: " + response);
        }

        [TestMethod]
        public void CreateDateTimeString_TestingDatePart()
        {
            var dateTime = DateTime.Now;

            var teamSportXmlDocument = new TeamSportXmlDocumentCreator();

            var response = teamSportXmlDocument.CreateDateTimeString(dateTime);
            var parts = response.Split('T');

            var actual = parts.First();

            var expected = "2015-11-10";

            Assert.AreEqual(actual, expected);
        }

        [TestMethod]
        public void CreateDateTimeString_TestingTimeZone()
        {
            var dateTime = DateTime.Now;

            var teamSportXmlDocument = new TeamSportXmlDocumentCreator();

            var response = teamSportXmlDocument.CreateDateTimeString(dateTime);
            var parts = response.Split('+');

            var actual = "+" + parts.Last();

            var expected = "+01:00";

            Assert.AreEqual(actual, expected);
        }

        /// <summary>
        /// The set xml stadium node test without stadium.
        /// </summary>
        [TestMethod]
        public void SetXmlStadiumNodeTestWithoutStadium()
        {
            var matchId = 1234678;

            var xmlDoc = new XmlDocument();

            xmlDoc.CreateElement("root");

            Dictionary<int, XmlDocument> stadiumNode;
            stadiumNode = new Dictionary<int, XmlDocument>();
            stadiumNode.Add(matchId, xmlDoc);

            var xmlMatch = xmlDoc.CreateElement("Match");
            xmlMatch.SetAttribute("Id", matchId.ToString());

            // Now we are to test
            var teamSportXmlDocument = new TeamSportXmlDocumentCreator();
            var response = teamSportXmlDocument.SetXmlStadiumNode(matchId, xmlDoc, xmlMatch);

            var expected = true;
            Assert.AreEqual(expected, response);
        }
    }
}