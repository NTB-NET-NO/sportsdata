﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nif.Populators;

namespace NTB.SportsData.Test.NIF
{
    [TestClass]
    public class FileNamePopulatorTest
    {
        [TestMethod]
        public void TestMethod_CreateFileNameBase_Fail()
        {
            var populator = new FileNamePopulator();
            var federation = new Federation
            {
                NameShort = "NHF"
            };

            var tournament = new Tournament
            {
                Id = 1234576
            };

            var dateTime = DateTime.Today;

            var actual = populator.CreateFileNameBase(federation, tournament, dateTime);
            const string expected = "";

            Assert.AreNotEqual(expected, actual);
        }

        [TestMethod]
        public void TestMethod_CreateFileNameBase_Succeed()
        {
            var populator = new FileNamePopulator();
            var federation = new Federation
            {
                NameShort = "NHF"
            };

            var tournament = new Tournament
            {
                Id = 1234576
            };

            var dateTime = DateTime.Today;

            var actual = populator.CreateFileNameBase(federation, tournament, dateTime);
            const string expected = "NTB_SportsData_20170118_NHF_T1234576";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestMethod_CreateFileNameBase_SpecifiedDate()
        {
            var populator = new FileNamePopulator();
            var federation = new Federation
            {
                NameShort = "NHF"
            };

            var tournament = new Tournament
            {
                Id = 1234576
            };

            var dateTime = DateTime.Parse("2016-02-14");

            var actual = populator.CreateFileNameBase(federation, tournament, dateTime);
            const string expected = "NTB_SportsData_20160214_NHF_T1234576";

            Assert.AreEqual(expected, actual);
        }
    }
}
