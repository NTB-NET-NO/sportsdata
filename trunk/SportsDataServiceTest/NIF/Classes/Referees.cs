﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace NTB.SportsData.Test.NIF.Classes
{
    [XmlRoot("Referees")]
    public class Referees
    {
        [XmlElement("Referee")]
        public List<Referee> Referee { get; set; }
    }
}
