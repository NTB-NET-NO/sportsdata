﻿using System.Xml.Serialization;

namespace NTB.SportsData.Test.NIF.Classes
{
    public class Referee
    {
        [XmlAttribute("id")]
        public int Id { get; set; }

        [XmlAttribute("clubid")]
        public int ClubId { get; set; }

        [XmlAttribute("typeid")]
        public int TypeId { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("club")]
        public string Club { get; set; }

        [XmlAttribute("firstname")]
        public string FirstName { get; set; }

        [XmlAttribute("lastname")]
        public string LastName { get; set; }

        [XmlText]
        public string FormattedName { get; set; }
    }
}
