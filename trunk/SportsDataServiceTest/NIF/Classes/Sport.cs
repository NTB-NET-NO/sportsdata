﻿using System.Xml.Serialization;

namespace NTB.SportsData.Test.NIF.Classes
{
    public class Sport
    {
        [XmlAttribute("id")]
        public int Id { get; set; }

        [XmlText]
        public string Content { get; set; }
    }
}
