﻿using System.Xml.Serialization;

namespace NTB.SportsData.Test.NIF.Classes
{
    public class Stadium
    {
        [XmlAttribute("latitude")]
        public double Latitude { get; set; }

        [XmlAttribute("longitude")]
        public double Longitude { get; set; }

        [XmlAttribute("localcouncilname")]
        public string LocalCouncilName { get; set; }

        [XmlAttribute("spectators")]
        public int Spectators { get; set; }

        [XmlText]
        public string Name { get; set; }
    }
}
