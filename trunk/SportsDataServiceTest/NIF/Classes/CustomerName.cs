﻿using System.Xml.Serialization;

namespace NTB.SportsData.Test.NIF.Classes
{
    public class CustomerName
    {
        [XmlAttribute("Id")]
        public int Id { get; set; }

        [XmlAttribute("RemoteCustomerId")]
        public int RemoteCustomerId { get; set; }

        [XmlText]
        public string Content { get; set; }
    }
}
