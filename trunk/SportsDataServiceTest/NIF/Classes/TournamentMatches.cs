﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace NTB.SportsData.Test.NIF.Classes
{
    public class TournamentMatches
    {
        [XmlArray("Matches")]
        [XmlArrayItem("Match")]
        public List<TournamentMatch> Matches { get; set; }
    }
}
