﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace NTB.SportsData.Test.NIF.Classes
{
    [XmlRoot("Events")]
    public class MatchEvents
    {
        [XmlElement("Event")]
        public List<MatchEvent> Events { get; set; }
    }
}
