﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace NTB.SportsData.Test.NIF.Classes
{
    public class TournamentMatch
    {
        [XmlAttribute("Id")]
        public int Id { get; set; }

        [XmlAttribute("startdate")]
        public string StartDate { get; set; }

        [XmlAttribute("starttime")]
        public string StartTime { get; set; }

        [XmlElement("Stadium")]
        public Stadium Stadium { get; set; }

        [XmlElement("HomeTeam")]
        public Team HomeTeam { get; set; }

        [XmlElement("AwayTeam")]
        public Team AwayTeam { get; set; }

        [XmlArray("Referees")]
        [XmlArrayItem("Referee")]
        public List<Referee> Referees { get; set; }

        [XmlArray("Events")]
        [XmlArrayItem("Event")]
        public List<MatchEvent> MatchEvents { get; set; }

        [XmlArray("MatchResults")]
        [XmlArrayItem("MatchResult")]
        public List<MatchResult> MatchResults { get; set; }
    }
}
