﻿using System.Xml.Serialization;

namespace NTB.SportsData.Test.NIF.Classes
{
    public class Organisation
    {
        [XmlAttribute("Name")]
        public string ShortName { get; set; }

        [XmlAttribute("Id")]
        public int Id { get; set; }

        [XmlText]
        public string Content { get; set; }


        
    }
}
