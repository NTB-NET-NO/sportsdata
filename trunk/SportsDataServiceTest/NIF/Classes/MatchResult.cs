﻿using System.Xml.Serialization;

namespace NTB.SportsData.Test.NIF.Classes
{
    public class MatchResult
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("hometeam")]
        public int HomeTeamGoals { get; set; }

        [XmlAttribute("awayteam")]
        public int AwayTeamGoals { get; set; }
    }
}
