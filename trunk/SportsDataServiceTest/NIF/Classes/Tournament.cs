﻿using System.Collections.Generic;
using System.Xml.Serialization;
using NTB.SportsData.Classes.Classes;

namespace NTB.SportsData.Test.NIF.Classes
{
    public class Tournament
    {
        [XmlAttribute("Type")]
        public string Type { get; set; }

        [XmlAttribute("doc-id")]
        public string DocId { get; set; }

        [XmlElement("Tournament-MetaInfo")]
        public TournamentMetaInfo TournamentMetaInfo { get; set; }

        [XmlElement("Header")]
        public Header Header { get; set; }

        [XmlArray("Matches")]
        [XmlArrayItem("Match")]
        public List<TournamentMatch> Matches { get; set; }
        
        [XmlArray("Standings")]
        [XmlArrayItem("Team")]
        public List<Standing.TeamResult> TournamentTable { get; set; }
    }
}
