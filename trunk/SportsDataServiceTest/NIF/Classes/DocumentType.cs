﻿using System.Xml.Serialization;

namespace NTB.SportsData.Test.NIF.Classes
{
    public class DocumentType
    {
        [XmlAttribute("type")]
        public string Type { get; set; }
    }
}
