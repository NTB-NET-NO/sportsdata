﻿using System.Xml.Serialization;

namespace NTB.SportsData.Test.NIF.Classes
{
    [XmlRoot("Name")]
    public class TeamName
    {
        [XmlText]
        public string Name { get; set; }

        [XmlAttribute("short")]
        public string ShortName { get; set; }
    }
}
