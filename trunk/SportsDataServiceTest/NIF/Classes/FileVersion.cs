﻿using System.Xml.Serialization;

namespace NTB.SportsData.Test.NIF.Classes
{
    public class FileVersion
    {
        [XmlText]
        public int Content { get; set; }
    }
}
