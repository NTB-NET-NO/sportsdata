﻿using System.Xml.Serialization;

namespace NTB.SportsData.Test.NIF.Classes
{
    public class Player
    {
        [XmlAttribute("id")]
        public int Id { get; set; }

        [XmlAttribute("firstName")]
        public string FirstName { get; set; }

        [XmlAttribute("lastName")]
        public string LastName { get; set; }

        [XmlAttribute("full")]
        public string FullName { get; set; }

        [XmlAttribute("position")]
        public string Position { get; set; }

        [XmlAttribute("shirtnumber")]
        public string ShirtNumber { get; set; }

        [XmlAttribute("captain")]
        public string Captain { get; set; }

        [XmlText]
        public string FormattedName { get; set; }
    }
}
