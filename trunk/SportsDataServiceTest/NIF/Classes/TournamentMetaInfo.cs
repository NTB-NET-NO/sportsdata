﻿using System;
using System.Xml.Serialization;

namespace NTB.SportsData.Test.NIF.Classes
{
    [XmlRoot("Tournament-MetaInfo")]
    public class TournamentMetaInfo
    {
        [XmlAttribute("Id")]
        public string Id { get; set; }

        [XmlAttribute("Name")]
        public string Name { get; set; }

        [XmlAttribute("Number")]
        public string Number { get; set; }

        [XmlAttribute("Division")]
        public string Division { get; set; }

        [XmlAttribute("MatchDate")]
        public DateTime MatchDate{ get; set; }

        [XmlAttribute("RoundNumber")]
        public string RoundNumber { get; set; }

        [XmlAttribute("roundName")]
        public string RoundName { get; set; }

        [XmlElement("District")]
        public District District { get; set; }
    }
}
