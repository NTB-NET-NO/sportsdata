﻿using System.Xml.Serialization;

namespace NTB.SportsData.Test.NIF.Classes
{
    [XmlRoot("Customer")]
    public class Customer
    {
        [XmlElement("CustomerName")]
        public CustomerName CustomerName { get; set; }
    }
}
