﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace NTB.SportsData.Test.NIF.Classes
{
    [XmlRoot]
    public class Header
    {
        [XmlElement(("Header"))]
        public Organisation Organisation { get; set; }

        [XmlElement("AgeCategory")]
        public AgeCategory AgeCategory { get; set; }

        [XmlElement("Customers")]
        public List<Customer> Customers { get; set; }

        [XmlElement("Sport")]
        public Sport Sport { get; set; }

        [XmlElement("DocumentType")]
        public DocumentType DocumentType { get; set; }

        [XmlElement("File")]
        public File File { get; set; }
    }
}
