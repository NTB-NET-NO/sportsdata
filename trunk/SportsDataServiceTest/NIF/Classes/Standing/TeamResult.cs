﻿using System.Xml.Serialization;

namespace NTB.SportsData.Test.NIF.Classes.Standing
{
    public class TeamResult
    {
        [XmlAttribute("Id")]
        public int Id { get; set; }

        [XmlAttribute("TablePosition")]
        public int TablePosition { get; set; }

        [XmlElement("Name")]
        public Name Name { get; set; }

        [XmlElement("Points")]
        public Points Points { get; set; }

        [XmlElement("Goals")]
        public Goal Goal { get; set; }

        [XmlElement("Matches")]
        public Match Match { get; set; }
    }
}
