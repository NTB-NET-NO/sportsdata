﻿using System.Xml.Serialization;

namespace NTB.SportsData.Test.NIF.Classes.Standing
{
    public class SubMatch
    {
        [XmlAttribute("Won")]
        public int Won { get; set; }

        [XmlAttribute("Draw")]
        public int Draw { get; set; }

        [XmlAttribute("Lost")]
        public int Lost { get; set; }
    }
}
