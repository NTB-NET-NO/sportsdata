﻿using System.Xml.Serialization;

namespace NTB.SportsData.Test.NIF.Classes.Standing
{
    public class SubPoints
    {
        [XmlText]
        public int Content { get; set; }
    }
}
