﻿using System.Xml.Serialization;

namespace NTB.SportsData.Test.NIF.Classes.Standing
{
    public class Name
    {
        [XmlText]
        public string Content { get; set; }
    }
}
