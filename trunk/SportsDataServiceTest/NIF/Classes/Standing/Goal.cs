﻿using System.Xml.Serialization;

namespace NTB.SportsData.Test.NIF.Classes.Standing
{
    public class Goal
    {
        [XmlAttribute("TotalScored")]
        public int TotalScored { get; set; }

        [XmlAttribute("TotalAgainst")]
        public int TotalAgainst { get; set; }

        [XmlElement("Home")]
        public SubGoal HomeGoal { get; set; }

        [XmlElement("Away")]
        public SubGoal AwayGoal { get; set; }
    }
}
