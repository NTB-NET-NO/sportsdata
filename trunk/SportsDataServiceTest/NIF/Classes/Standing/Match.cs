﻿using System.Xml.Serialization;

namespace NTB.SportsData.Test.NIF.Classes.Standing
{
    public class Match
    {
        [XmlAttribute("Total")]
        public int Total { get; set; }

        [XmlElement("HomeMatches")]
        public SubMatch HomeMatch { get; set; }

        [XmlElement("AwayMatches")]
        public SubMatch AwayMatch { get; set; }
    }
}
