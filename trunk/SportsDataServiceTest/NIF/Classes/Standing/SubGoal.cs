﻿using System.Xml.Serialization;

namespace NTB.SportsData.Test.NIF.Classes.Standing
{
    public class SubGoal
    {
        [XmlAttribute("Scored")]
        public int Scored { get; set; }

        [XmlAttribute("Against")]
        public int Against { get; set; }
    }
}
