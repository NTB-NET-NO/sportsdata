﻿using System.Xml.Serialization;

namespace NTB.SportsData.Test.NIF.Classes.Standing
{
    public class Points
    {
        [XmlAttribute("Total")]
        public int Total { get; set; }

        [XmlElement("HomePoints")]
        public SubPoints HomePoints { get; set; }

        [XmlElement("AwayPoints")]
        public SubPoints AwayPoints { get; set; }
    }
}
