﻿using System.Xml.Serialization;

namespace NTB.SportsData.Test.NIF.Classes
{
    [XmlRoot("Name")]
    public class FileName
    {
        [XmlAttribute("FullName")]
        public string FullName { get; set; }

        [XmlAttribute("Type")]
        public string Type { get; set; }

        [XmlText]
        public string Content { get; set; }
    }
}
