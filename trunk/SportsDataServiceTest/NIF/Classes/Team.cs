﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace NTB.SportsData.Test.NIF.Classes
{
    public class Team
    {
        [XmlAttribute("id")]
        public int Id { get; set; }

        [XmlAttribute("alignment")]
        public string Alignment { get; set; }

        [XmlElement("Name")]
        public TeamName TeamName { get; set; }

        [XmlElement("Players")]
        public List<Player> Players { get; set; }

        [XmlElement("PartialResults")]
        public List<PartialResult> PartialResults { get; set; }
    }
}
