﻿using System.Xml.Serialization;

namespace NTB.SportsData.Test.NIF.Classes
{
    public class FileCreated
    {
        [XmlText]
        public string Content { get; set; }
    }
}
