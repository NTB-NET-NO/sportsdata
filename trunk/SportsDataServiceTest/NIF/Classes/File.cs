﻿using System.Xml.Serialization;

namespace NTB.SportsData.Test.NIF.Classes
{
    [XmlRoot("File")]
    public class File
    {
        [XmlElement("Name")]
        public FileName FileName { get; set; }

        [XmlElement("Version")]
        public FileVersion FileVersion { get; set; }

        [XmlElement("Created")]
        public FileCreated FileCreated { get; set; }
    }
}
