﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NifTeamGathererComponentTest.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   This is a test class for NifTeamGathererComponentTest and is intended
//   to contain all NifTeamGathererComponentTest Unit Tests
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using System.Xml;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Classes.Enums;
using NTB.SportsData.Components.Nif.Components;

namespace NTB.SportsData.Test.NIF
{
    /// <summary>
    ///This is a test class for NifTeamGathererComponentTest and is intended
    ///to contain all NifTeamGathererComponentTest Unit Tests
    ///</summary>
    [TestClass()]
    public class TeamGathererComponentTest
    {
        /// <summary>
        /// The test context instance.
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return this.testContextInstance;
            }

            set
            {
                this.testContextInstance = value;
            }
        }

        /// <summary>
        ///A test for AlertReceiverses
        ///</summary>
        [TestMethod()]
        public void AlertReceiversTest()
        {
            var target = new NifTeamGathererComponent(); // TODO: Initialize to an appropriate value

            var doc = new XmlDocument();
            var root = doc.CreateElement("AlertReceivers");
            var receiver = doc.CreateElement("Receiver");
            receiver.SetAttribute("level", "ERROR");
            var text = doc.CreateTextNode("trond.huso@ntb.no");
            receiver.AppendChild(text);
            root.AppendChild(receiver);

            receiver = doc.CreateElement("Receiver");
            receiver.SetAttribute("level", "ERROR");
            text = doc.CreateTextNode("anders.sivesind@ntb.no");
            receiver.AppendChild(text);
            root.AppendChild(receiver);
            doc.AppendChild(root);

            var configNode = doc.SelectSingleNode("/AlertReceivers");

            var actual = target.AlertReceivers(configNode);

            var expected = new List<AlertReceiver>();
            var alertReceiver = new AlertReceiver { AlertLevel = AlertLevel.Error, Receiver = "trond.huso@ntb.no" };

            expected.Add(alertReceiver);
            var alertReceiver1 = new AlertReceiver { AlertLevel = AlertLevel.Error, Receiver = "anders.sivesind@ntb.no" };

            expected.Add(alertReceiver1);

            Assert.AreEqual(expected.Count, actual.Count);
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext)
        // {
        // }
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup()
        // {
        // }
        // Use TestInitialize to run code before running each test
        // [TestInitialize()]
        // public void MyTestInitialize()
        // {
        // }
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup()
        // {
        // }
        #endregion
    }
}