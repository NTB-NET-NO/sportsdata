﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nif.DataFileController;

namespace NTB.SportsData.Test.NIF
{
    [TestClass]
    public class TeamSportDataFileControllerTest
    {
        /// <summary>
        /// The compare local and remote lists.
        /// </summary>
        [TestMethod]
        public void CompareLocalAndRemoteListsEqual()
        {
            var remoteList = new List<Match>();
            var localList = new List<Match>();

            for (var i = 1; i <= 10; i++)
            {
                var match = new Match()
                {
                    Id = i + 1000
                };

                remoteList.Add(match);
            }

            for (var i = 1; i <= 10; i++)
            {
                var match = new Match()
                {
                    Id = i + 1000
                };

                localList.Add(match);
            }

            var controller = new TeamSportDataFileController();
            //var actual = controller.CompareLocalAndRemoteLists(remoteList, localList, new Tournament());

            //const bool Expected = true;
            //Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        /// The compare local and remote lists.
        /// </summary>
        [TestMethod]
        public void CompareLocalAndRemoteListsNotEqual()
        {
            var remoteList = new List<Match>();
            var localList = new List<Match>();

            for (var i = 1; i <= 5; i++)
            {
                var match = new Match()
                {
                    Id = i + 1000
                };

                remoteList.Add(match);
            }

            for (var i = 1; i <= 10; i++)
            {
                var match = new Match()
                {
                    Id = i + 1000
                };

                localList.Add(match);
            }

            var controller = new TeamSportDataFileController();
            //var actual = controller.CompareLocalAndRemoteLists(remoteList, localList, new Tournament());

            //const bool Expected = false;
            //Assert.AreEqual(Expected, actual);
        }
    }
}
