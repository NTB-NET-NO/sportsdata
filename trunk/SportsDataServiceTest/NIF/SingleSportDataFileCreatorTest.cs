﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NIFSingleSportDataFileCreatorTest.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   Summary description for NIFSingleSportDataFileCreatorTest
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Test.NIF
{
    
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Components.Nif.DataFileCreators;

    /// <summary>
    /// Summary description for NIFSingleSportDataFileCreatorTest
    /// </summary>
    [TestClass]
    public class SingleSportDataFileCreatorTest
    {
        /// <summary>
        /// The test context instance.
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        /// Initializes a new instance of the <see cref="NifSingleSportDataFileCreatorTest"/> class.
        /// </summary>
        public SingleSportDataFileCreatorTest()
        {
            // TODO: Add constructor logic here
        }

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return this.testContextInstance;
            }

            set
            {
                this.testContextInstance = value;
            }
        }

        /// <summary>
        /// The create short organization name.
        /// </summary>
        [TestMethod]
        public void CreateShortOrganizationName()
        {
            var creator = new NifSingleSportDataFileCreator();

            const string TestString = "Norges Judoforbund";

            var actual = creator.CreateShortOrganizationName(TestString);

            var expected = "NJF";

            Assert.AreEqual(expected, actual);
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        #endregion
    }
}