﻿using System;
using System.Configuration;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nif.Controller;

namespace NTB.SportsData.Test.NIF
{
    [TestClass]
    public class TeamResultControllerTest
    {
        [TestMethod]
        public void GetValidDownloadTimeCurrentDateTimeTest()
        {
            var controller = new TeamResultController();

            var currentDateTime = DateTime.Now;

            var storedDateTime = DateTime.Parse("2016-09-15 12.16.32");

            var expected = controller.CurrentDateTime;

            double interval = 60000;
            var actual = controller.GetValidDownloadTime(currentDateTime, storedDateTime, interval);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetHourDifferenceTest()
        {
            var controller = new TeamResultController();

            var currentDateTime = DateTime.Parse("2016-09-14 17.16.32");

            var storedDateTime = DateTime.Parse("2016-09-15 17.16.32");

            var expected = 24;

            var actual = Convert.ToInt32(controller.GetHourDifference(currentDateTime, storedDateTime));

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetValidDownloadTimeStoredDateTimeTest()
        {
            var controller = new TeamResultController();

            var currentDateTime = DateTime.Now;

            var storedDateTime = DateTime.Parse("2016-09-15 17.16.32");

            var expected = storedDateTime;

            double interval = 60000;

            var actual = controller.GetValidDownloadTime(currentDateTime, storedDateTime, interval);

            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void GetLatestDownloadDateTimeFirstDateTimeTest()
        {
            /**
             * This test shall delete the file and so we shall be sure the date-time is now - 60 seconds
             */
            var controller = new TeamResultController();
            var dataFileParams = new DataFileParams();
            dataFileParams.FileStorePath = ConfigurationManager.AppSettings["FileStorePath"];
            dataFileParams.InstanceName = "TeamResultControllerTest";
            double interval = 60000;

            // Deleting the file if it exists
            var filename = Path.Combine(dataFileParams.FileStorePath, dataFileParams.InstanceName.ToLower() + ".txt");
            var fileInfo = new FileInfo(filename);
            if (fileInfo.Exists)
            {
                fileInfo.Delete();
            }
            var actual = controller.GetLatestDownloadDateTime(dataFileParams, interval);

            var expected = controller.CurrentDateTime.AddMilliseconds(-interval);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetLatestDownloadDateUseStoredDateTest()
        {
            var controller = new TeamResultController();
            var dataFileParams = new DataFileParams();
            dataFileParams.FileStorePath = ConfigurationManager.AppSettings["FileStorePath"];
            dataFileParams.InstanceName = "TeamResultControllerTest";
            double interval = 60000;

            var actual = controller.GetLatestDownloadDateTime(dataFileParams, interval);

            var expected = controller.CurrentDateTime.AddMilliseconds(-interval);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetLatestStoredDownloadDateTimeNullTest()
        {
            var controller = new TeamResultController();
            var dataFileParams = new DataFileParams();
            dataFileParams.FileStorePath = ConfigurationManager.AppSettings["FileStorePath"];
            dataFileParams.InstanceName = "TeamResultControllerNullTest";
        
            var actual = controller.GetLatestStoredDownloadDateTime(dataFileParams);

            Assert.IsNull(actual);
        }

        [TestMethod]
        public void GetLatestStoredDownloadDateTimeTest()
        {
            var controller = new TeamResultController();
            var dataFileParams = new DataFileParams();
            dataFileParams.FileStorePath = ConfigurationManager.AppSettings["FileStorePath"];
            dataFileParams.InstanceName = "TeamResultControllerTest";

            var actual = controller.GetLatestStoredDownloadDateTime(dataFileParams);

            var expected = DateTime.Parse("2016-09-15 17.49.08");
            
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetLatestDownloadDateTime()
        {
            var controller = new TeamResultController();
            var dataFileParams = new DataFileParams();
            dataFileParams.FileStorePath = ConfigurationManager.AppSettings["FileStorePath"];
            dataFileParams.InstanceName = "TeamResultControllerTest";
            double interval = 60000;
            var actual = controller.GetLatestDownloadDateTime(dataFileParams, interval);

            var expected = DateTime.Parse("2016-09-15 17.49.08");

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetLatestDownloadDateTimeOlderThanSixHoursTest()
        {
            var controller = new TeamResultController();
            var dataFileParams = new DataFileParams();
            dataFileParams.FileStorePath = ConfigurationManager.AppSettings["FileStorePath"];
            dataFileParams.InstanceName = "TeamResultControllerTest";
            double interval = 60000;
            var actual = controller.GetLatestDownloadDateTime(dataFileParams, interval);

            var expected = controller.CurrentDateTime.AddMilliseconds(-interval); 

            Assert.AreEqual(expected, actual);
        }
    }
}
