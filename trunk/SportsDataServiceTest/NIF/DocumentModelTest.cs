﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Classes.Enums;
using NTB.SportsData.Components.Nif.Models;
using NTB.SportsData.Components.Nif.Populators;

namespace NTB.SportsData.Test.NIF
{
    [TestClass]
    public class DocumentModelTest
    {
        [TestMethod]
        public void TestSetDocumentTypes_CheckForTwo()
        {
            var model = new DocumentModel();
            model.DataFileParams = new DataFileParams();
            model.DataFileParams.Results = true;
            model.DataFileParams.PushResults = true;
            var populator = new FileNamePopulator();
            populator.HasMatchFacts = true;
            var documentTypes = model.SetDocumentTypes(populator);

            const int expected = 2;

            var actual = documentTypes.Count;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestSetDocumentTypes_CheckForOne()
        {
            var model = new DocumentModel();
            model.DataFileParams = new DataFileParams();
            model.DataFileParams.Results = true;
            model.DataFileParams.PushResults = true;
            var populator = new FileNamePopulator();
            populator.RenderResult = true;

            var documentTypes = model.SetDocumentTypes(populator);

            const int expected = 1;

            var actual = documentTypes.Count;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestSetDocumentTypes_CheckForDocumentTypeMatchFact()
        {
            var model = new DocumentModel();
            model.DataFileParams = new DataFileParams();
            model.DataFileParams.Results = true;
            model.DataFileParams.PushResults = true;
            var populator = new FileNamePopulator();
            populator.HasMatchFacts = true;
            var documentTypes = model.SetDocumentTypes(populator);

            const DocumentType expected = DocumentType.MatchFact;

            var actual = documentTypes[0];

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestSetDocumentTypes_CheckForDocumentTypeResult()
        {
            var model = new DocumentModel();
            model.DataFileParams = new DataFileParams();
            model.DataFileParams.Results = true;
            model.DataFileParams.PushResults = true;
            var populator = new FileNamePopulator();
            
            var documentTypes = model.SetDocumentTypes(populator);

            const DocumentType expected = DocumentType.Result;

            var actual = documentTypes[0];

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestSetDocumentTypes_CheckForDocumentTypeStanding()
        {
            var model = new DocumentModel();
            model.DataFileParams = new DataFileParams();
            model.DataFileParams.Results = true;
            model.DataFileParams.PushResults = true;
            var populator = new FileNamePopulator();
            populator.HasStanding = true;
            var documentTypes = model.SetDocumentTypes(populator);

            const DocumentType expected = DocumentType.Standing;

            var actual = documentTypes[0];

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestSetDocumentTypes_CheckForDocumentTypeRenderResult()
        {
            var model = new DocumentModel();
            model.DataFileParams = new DataFileParams();
            model.DataFileParams.Results = false;
            model.DataFileParams.PushResults = true;
            var populator = new FileNamePopulator();
            populator.RenderResult = false;
            var documentTypes = model.SetDocumentTypes(populator);

            const DocumentType expected = DocumentType.Schedule;

            var actual = documentTypes[0];

            Assert.AreEqual(expected, actual);
        }

        // PopulateFileNamePopulator

        [TestMethod]
        public void TestPopulateFileNamePopulator_PublishTournamentTable()
        {
            var model = new DocumentModel();
            model.HasMatchFatcts = false;

            // PublishTournamentTable
            model.DataFileParams = new DataFileParams();
            model.DataFileParams.Results = false;
            model.DataFileParams.PushResults = true;

            var tournament = new Tournament();
            tournament.PublishTournamentTable = true;
            var populator = model.PopulateFileNamePopulator(tournament);
            var actual = populator.HasStanding;
            
            const bool expected = true;


            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestPopulateFileNamePopulator_HasMatchFacts()
        {
            var model = new DocumentModel();
            model.HasMatchFatcts = true;

            // PublishTournamentTable
            model.DataFileParams = new DataFileParams();
            model.DataFileParams.Results = false;
            model.DataFileParams.PushResults = true;

            var tournament = new Tournament();
            var populator = model.PopulateFileNamePopulator(tournament);
            var actual = populator.HasMatchFacts;

            const bool expected = true;


            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_CreateDocument_Today()
        {
            // Tournament tournament, DateTime dateTime, int version
            var model = new DocumentModel();
            model.HasMatchFatcts = true;

            // PublishTournamentTable
            model.DataFileParams = new DataFileParams();
            model.DataFileParams.Results = false;
            model.DataFileParams.PushResults = true;
            model.DataFileParams.DisciplineId = 72;
            model.DataFileParams.SportId = 23;

            var populator = new FileNamePopulator();
            
            var tournament = new Tournament
            {
                Id = 1234576,
                DisciplineId = 72
            };

            var dateTime = DateTime.Today;

            const int version = 1;

            model.CreateDocument(tournament,dateTime, version);

            var actual = model.Document.Filename;
            var expected = string.Format("NTB_SportsData_{0}_NHF_T1234576", DateTime.Today.ToString("yyyyMMdd"));

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_CreateDocument_DefinedDate()
        {
            // Tournament tournament, DateTime dateTime, int version
            var model = new DocumentModel();
            model.HasMatchFatcts = true;

            // PublishTournamentTable
            model.DataFileParams = new DataFileParams();
            model.DataFileParams.Results = false;
            model.DataFileParams.PushResults = true;
            model.DataFileParams.DisciplineId = 72;
            model.DataFileParams.SportId = 23;

            var tournament = new Tournament
            {
                Id = 1234576
            };

            var dateTime = DateTime.Parse("2016-02-14");

            const int version = 1;

            model.CreateDocument(tournament, dateTime, version);

            var actual = model.Document.Filename;
            const string expected = "NTB_SportsData_20160214_NHF_T1234576";

            Assert.AreEqual(expected, actual);
        }
    }
}
