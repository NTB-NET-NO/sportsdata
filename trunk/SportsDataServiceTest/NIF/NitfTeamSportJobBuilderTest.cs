﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NTB.SportsData.Classes.Classes;
using NTB.SportsData.Components.Nif.JobBuilders;

namespace NTB.SportsData.Test.NIF
{
    [TestClass]
    public class NitfTeamSportJobBuilderTest
    {
        [TestMethod]
        public void TestMethod_SetDuration_Today()
        {
            var JobBuilder = new NifTeamSportJobBuilder();
            var dataFileParams = new DataFileParams();
            JobBuilder.SetDuration(0, dataFileParams, DateTime.Today);

            var actual = dataFileParams.DateStart = DateTime.Today.Date;
            var expected = DateTime.Parse("2017-01-18").Date;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestMethod_SetDuration_Yesterday()
        {
            var JobBuilder = new NifTeamSportJobBuilder();
            var dataFileParams = new DataFileParams();
            JobBuilder.SetDuration(0, dataFileParams, DateTime.Today.AddDays(-1));

            var actual = dataFileParams.DateStart = DateTime.Today.AddDays(-1).Date;
            var expected = DateTime.Parse("2017-01-17").Date;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestMethod_SetDuration_Tomorrow()
        {
            var JobBuilder = new NifTeamSportJobBuilder();
            var dataFileParams = new DataFileParams();
            JobBuilder.SetDuration(0, dataFileParams, DateTime.Today.AddDays(+1));

            var actual = dataFileParams.DateStart = DateTime.Today.AddDays(+1).Date;
            var expected = DateTime.Parse("2017-01-19").Date;

            Assert.AreEqual(expected, actual);
        }
    }
}
