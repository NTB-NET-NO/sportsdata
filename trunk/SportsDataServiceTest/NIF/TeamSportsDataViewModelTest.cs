﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NTB.SportsData.Components.Nif.Models;
using NTB.SportsData.Classes.Classes;

namespace NTB.SportsData.Test.NIF
{
    [TestClass]
    public class TeamSportsDataViewModelTest
    {
        [TestMethod]
        public void TestCreateDateTimeString()
        {
            var model = new TeamSportsDataViewModel();

            const string expected = "2017-01-02T13:04:00+01:00";

            var testDateTimeString = new DateTime(2017,01,02, 13, 04, 0);
            var actual = model.CreateDateTimeString(testDateTimeString);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestCreateDocumentDateTimeFromMatchDate()
        {
            var model = new TeamSportsDataViewModel();

            var match = new Match();

            match.MatchDate = new DateTime(2017, 1, 2);

            match.MatchStartTime = 2045;
            
            var expected = new DateTime(2017,1,2,20,45,0);

            // var returnValue = model.CreateDocumentDateTimeFromMatchDate(match);
            var actual = model.CreateDocumentDateTimeFromMatchDate(match);
                // returnValue.ToString("yyyy-MM-dd") + "T" + returnValue.ToString("HH:mm:sszzz").Replace(".", ":");

            Assert.AreEqual(expected, actual);

        }

        [TestMethod]
        public void TestCreateDocumentDateTimeFromMatchDateWithNoMatchDateTime()
        {
            var model = new TeamSportsDataViewModel();

            var match = new Match();

            match.MatchDate = new DateTime(2017, 1, 2);

            // match.MatchStartTime = 2045;

            var expected = new DateTime(2017, 1, 2);

            // var returnValue = model.CreateDocumentDateTimeFromMatchDate(match);
            var actual = model.CreateDocumentDateTimeFromMatchDate(match);
            // returnValue.ToString("yyyy-MM-dd") + "T" + returnValue.ToString("HH:mm:sszzz").Replace(".", ":");

            Assert.AreEqual(expected, actual);

        }

        [TestMethod]
        public void TestCreateDocumentDateTimeFromMatchDateWithErrorMatchDate()
        {
            var model = new TeamSportsDataViewModel();

            var match = new Match();

            match.MatchDate = new DateTime(2017, 1, 2);

            match.MatchStartTime = 9999;

            var expected = new DateTime(2017, 1, 2);

            // var returnValue = model.CreateDocumentDateTimeFromMatchDate(match);
            var actual = model.CreateDocumentDateTimeFromMatchDate(match);
            // returnValue.ToString("yyyy-MM-dd") + "T" + returnValue.ToString("HH:mm:sszzz").Replace(".", ":");

            Assert.AreEqual(expected, actual);

        }

        [TestMethod]
        public void TestCreateDocumentDateTimeFromMatchDateWithNoMatchDate()
        {
            var model = new TeamSportsDataViewModel();

            var match = new Match();

            // match.MatchDate = new DateTime(2017, 1, 2);

            match.MatchStartTime = 9999;

            var expected = new DateTime(2017, 1, 2);

            // var returnValue = model.CreateDocumentDateTimeFromMatchDate(match);
            var actual = model.CreateDocumentDateTimeFromMatchDate(match);
            // returnValue.ToString("yyyy-MM-dd") + "T" + returnValue.ToString("HH:mm:sszzz").Replace(".", ":");

            Assert.AreEqual(expected, actual);

        }
    }
}
