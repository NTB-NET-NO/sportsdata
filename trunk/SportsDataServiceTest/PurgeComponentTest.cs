﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PurgeComponentTest.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The purge component test.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace NTB.SportsData.Test
{
    using System.Xml;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Components.Components;
    using Classes.Classes;
    
    /// <summary>
    /// The purge component test.
    /// </summary>
    [TestClass]
    public class PurgeComponentTest
    {
        /// <summary>
        /// The xml configuration enabled document.
        /// </summary>
        private const string XmlConfigurationDocumentEnabled = @"<PurgeComponent Id='400' Name='Weekly_Purge' Enabled='True' OperationMode='Purge' PollStyle='CalendarPoll' XML='True' InsertDatabase='True' ContentType='Schedule' ScheduleType='Weekly'>
    <Schedules>
      <Schedule Id='400-1' Time='08:30' DayOfWeek='5' Week='2' Duration='30'/>
    </Schedules>
  </PurgeComponent>";

        /// <summary>
        /// The xml configuration disabled document.
        /// </summary>
        private const string XmlConfigurationDocumentDisabled = @"<PurgeComponent Id='400' Name='Weekly_Purge' Enabled='False' OperationMode='Purge' PollStyle='CalendarPoll' XML='True' InsertDatabase='True' ContentType='Schedule' ScheduleType='Weekly'>
    <Schedules>
      <Schedule Id='400-1' Time='08:30' DayOfWeek='5' Week='2' Duration='30'/>
    </Schedules>
  </PurgeComponent>";

        /// <summary>
        /// The get schedule id
        /// </summary>
        [TestMethod]
        public void GetScheduleId()
        {
            var doc = new XmlDocument();

            doc.LoadXml(XmlConfigurationDocumentEnabled);

            var xmlNode = doc.SelectSingleNode("//PurgeComponent/Schedules/Schedule[@Id='400-1']");

            var component = new PurgeComponent();

            var actual = component.GetScheduleId(xmlNode);

            const string Expected = "400-1";

            Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        /// The failing create schedule time.
        /// </summary>
        [TestMethod]
        public void FailingCreateScheduleTime()
        {
            var doc = new XmlDocument();

            doc.LoadXml(XmlConfigurationDocumentEnabled);

            var xmlNode = doc.SelectSingleNode("//PurgeComponent/Schedules/Schedule[@Id='400-1']");

            var component = new PurgeComponent();

            var actual = component.CreateScheduleTime(xmlNode);

            // We are only testing if the method is returning OK, not that it fails
            var scheduleTime = new ScheduleTime()
            {
                Hour = "14",
                Minutes = "45"
            };
            var expected = scheduleTime.Hour;

            Assert.AreNotEqual(expected, actual.Hour);
        }

        /// <summary>
        /// The create schedule time.
        /// </summary>
        [TestMethod]
        public void CreateScheduleTime()
        {
            var doc = new XmlDocument();

            doc.LoadXml(XmlConfigurationDocumentEnabled);

            var xmlNode = doc.SelectSingleNode("//PurgeComponent/Schedules/Schedule[@Id='400-1']");

            var component = new PurgeComponent();

            var actual = component.CreateScheduleTime(xmlNode);

            // We are only testing if the method is returning OK, not that it fails
            var scheduleTime = new ScheduleTime()
                                   {
                                       Hour = "08",
                                       Minutes = "30"
                                   };
            var expected = scheduleTime.Hour;

            Assert.AreEqual(expected, actual.Hour);
        }

        /// <summary>
        /// The configure operation mode.
        /// </summary>
        [TestMethod]
        public void ConfigureOperationModeEnabled()
        {
            var doc = new XmlDocument();

            doc.LoadXml(XmlConfigurationDocumentEnabled);

            var xmlNode = doc.SelectSingleNode("//PurgeComponent");

            var component = new PurgeComponent();

            var actual = component.ConfigureOperationMode(xmlNode);

            const bool Expected = true;

            Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        /// The configure operation mode.
        /// </summary>
        [TestMethod]
        public void ConfigureOperationModeDisabled()
        {
            var doc = new XmlDocument();

            doc.LoadXml(XmlConfigurationDocumentDisabled);

            var xmlNode = doc.SelectSingleNode("//PurgeComponent");

            var component = new PurgeComponent();

            var actual = component.ConfigureOperationMode(xmlNode);

            // We are only testing if the method is returning OK, not that it fails
            const bool Expected = true;

            Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        /// The configure operation mode.
        /// </summary>
        [TestMethod]
        public void ConfigurePollStyle()
        {
            var doc = new XmlDocument();

            doc.LoadXml(XmlConfigurationDocumentEnabled);

            var xmlNode = doc.SelectSingleNode("//PurgeComponent");

            var component = new PurgeComponent();

            var actual = component.ConfigurePollStyleType(xmlNode);

            // We are only testing if the method is returning OK, not that it fails
            const bool Expected = true;

            Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        /// The configure operation mode.
        /// </summary>
        [TestMethod]
        public void ConfigureJobEnabled()
        {
            var doc = new XmlDocument();

            doc.LoadXml(XmlConfigurationDocumentEnabled);

            var xmlNode = doc.SelectSingleNode("//PurgeComponent");

            var component = new PurgeComponent();

            var actual = component.ConfigureJobEnabled(xmlNode);

            // We are only testing if the method is returning OK, not that it fails
            const bool Expected = true;

            Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        /// The configure operation mode.
        /// </summary>
        [TestMethod]
        public void ConfigurePollStyleCalendarPoll()
        {
            var doc = new XmlDocument();

            doc.LoadXml(XmlConfigurationDocumentEnabled);

            var xmlNode = doc.SelectSingleNode("//PurgeComponent");

            var component = new PurgeComponent();

            var actual = component.ConfigurePollStyleCalendarPoll(xmlNode);

            // We are only testing if the method is returning OK, not that it fails
            const bool Expected = true;

            Assert.AreEqual(Expected, actual);
        }

        /// <summary>
        /// The Configure Quartz Job Duration.
        /// </summary>
        [TestMethod]
        public void ConfigureQuartzJobDuration()
        {
            var doc = new XmlDocument();

            doc.LoadXml(XmlConfigurationDocumentEnabled);

            var xmlNode = doc.SelectSingleNode("//PurgeComponent/Schedules/Schedule[@Id='400-1']");

            var component = new PurgeComponent();

            var actual = component.ConfigureQuartzJobDuration(xmlNode);

            // We are only testing if the method is returning OK, not that it fails
            const int Expected = 30;

            Assert.AreEqual(Expected, actual);
        }
    }
}
