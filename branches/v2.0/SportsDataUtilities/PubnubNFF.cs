﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Web.Script.Serialization;
using NTB.SportsData.Utilities.Interfaces;


namespace NTB.SportsData.Utilities
{
    /**
     * PubNub 3.0 Real-time Push Cloud API
     *
     * @author Stephen Blum
     * @package pubnub
     */

    public class PubnubNff : PushNetwork
    {
        private string _origin = "pubsub.pubnub.com";
        private const int Limit = 1800;
        private string _publishKey = "";
        private string _subscribeKey = "";
        private string _secretKey = "";
        private bool _ssl;



        

        /**
         * PubNub 3.0
         *
         * Prepare PubNub Class State.
         *
         * @param string Publish Key.
         * @param string Subscribe Key.
         * @param string Secret Key.
         * @param bool SSL Enabled.
         */
        public PubnubNff(
            string publishKey,
            string subscribeKey,
            string secretKey,
            bool sslOn
        )
        {
            Init(publishKey, subscribeKey, secretKey, sslOn);
        }

        /**
         * PubNub 2.0 Compatibility
         *
         * Prepare PubNub Class State.
         *
         * @param string Publish Key.
         * @param string Subscribe Key.
         */
        public PubnubNff(
            string publishKey,
            string subscribeKey
        )
        {
            Init(publishKey, subscribeKey, "", false);
        }

        /**
         * PubNub 3.0 without SSL
         *
         * Prepare PubNub Class State.
         *
         * @param string Publish Key.
         * @param string Subscribe Key.
         * @param string Secret Key.
         */
        public PubnubNff(
            string publishKey,
            string subscribeKey,
            string secretKey
        )
        {
            Init(publishKey, subscribeKey, secretKey, false);
        }

        /**
         * Init
         *
         * Prepare PubNub Class State.
         *
         * @param string Publish Key.
         * @param string Subscribe Key.
         * @param string Secret Key.
         * @param bool SSL Enabled.
         */
        public override void Init(
            string publishKey,
            string subscribeKey,
            string secretKey,
            bool sslOn
        )
        {
            _publishKey = publishKey;
            _subscribeKey = subscribeKey;
            _secretKey = secretKey;
            _ssl = sslOn;

            // SSL On?
            if (_ssl)
            {
                _origin = "https://" + _origin;
            }
            else
            {
                _origin = "http://" + _origin;
            }
        }

        /**
         * History
         *
         * Load history from a channel.
         *
         * @param String channel name.
         * @param int limit history count response.
         * @return ListArray of history.
         */
        public override List<PubNubMessage> History(string channel, int limit)
        {
            List<string> url = new List<string> {"history", _subscribeKey, channel, "0", limit.ToString()};

            var history = _request(url);
            var pubNubHistory = new List<PubNubMessage>(history.Count);
            var serializer = new JavaScriptSerializer();
            pubNubHistory.AddRange(history.
                Select(t => serializer.Serialize(t).Replace(@"//Date(", @"\/Date(").Replace(@")//", @")\/")).
                Select(msg => serializer.Deserialize<PubNubMessage>(msg)));
            return pubNubHistory;

        }

        /**
         * Publish
         *
         * Send a message to a channel.
         *
         * @param String channel name.
         * @param List<object> info.
         * @return bool false on fail.
         */
        public override List<object> Publish(string channel, object message)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            // Generate String to Sign
            string signature = "0";
            if (_secretKey.Length > 0)
            {
                StringBuilder stringToSign = new StringBuilder();
                stringToSign
                    .Append(_publishKey)
                    .Append('/')
                    .Append(_subscribeKey)
                    .Append('/')
                    .Append(_secretKey)
                    .Append('/')
                    .Append(channel)
                    .Append('/')
                    .Append(serializer.Serialize(message));

                // Sign Message
                signature = Md5(stringToSign.ToString());
            }

            // Build URL
            List<string> url = new List<string>
                {
                    "publish",
                    _publishKey,
                    _subscribeKey,
                    signature,
                    channel,
                    "0",
                    serializer.Serialize(message)
                };

            // Return JSONArray
            return _request(url);
        }
        
        /**
         * Subscribe
         *
         * This function is BLOCKING.
         * Listen for a message on a channel.
         *
         * @param string channel name.
         * @param Procedure function callback.
         */
        public override void Subscribe(string channel, Procedure callback)
        {
            _subscribe(channel, callback, 0);
        }

        /**
         * Subscribe - Private Interface
         *
         * @param string channel name.
         * @param Procedure function callback.
         * @param string timetoken.
         */
        private void _subscribe(
            string channel,
            Procedure callback,
            object timetoken
        )
        {
            // Begin Recusive Subscribe
            try
            {
                // Build URL
                List<string> url = new List<string>();
                url.Add("subscribe");
                url.Add(_subscribeKey);
                url.Add(channel);
                url.Add("0");
                url.Add(timetoken.ToString());

                // Wait for Message
                List<object> response = _request(url);

                // Update TimeToken
                if (response[1].ToString().Length > 0)
                    timetoken =  response[1];

                // Run user Callback and Reconnect if user permits.
                //foreach (object message in (object[])response[0])
                //{
                //    if (!callback(message)) return;
                //}
                // Run user Callback and Reconnect if user permits.
                foreach (object message in (object[])response[0])
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();

                    var msg = serializer.Serialize(message).Replace(@"//Date(", @"\/Date(").Replace(@")//", @")\/"); ;
                    var pubNub = serializer.Deserialize<PubNubMessage>(msg);

                    if (!callback(pubNub)) return;
                }


                // Keep listening if Okay.
                _subscribe(channel, callback, timetoken);
            }
            catch
            {
                System.Threading.Thread.Sleep(1000);
                _subscribe(channel, callback, timetoken);
            }
        }

        /**
         * Time
         *
         * Timestamp from PubNub Cloud.
         *
         * @return object timestamp.
         */
        public override object Time()
        {
            List<string> url = new List<string> {"time", "0"};

            List<object> response = _request(url);
            return response[0];
        }

        /**
         * Request URL
         *
         * @param List<string> request of url directories.
         * @return List<object> from JSON response.
         */
        private List<object> _request(List<string> url_components)
        {
            int count = 0;
            byte[] buf = new byte[8192];
            StringBuilder url = new StringBuilder();
            StringBuilder sb = new StringBuilder();

            JavaScriptSerializer serializer = new JavaScriptSerializer();

            // Add Origin To The Request
            url.Append(_origin);

            // Generate URL with UTF-8 Encoding
            foreach (string urlBit in url_components)
            {
                url.Append("/");
                url.Append(_encodeURIcomponent(urlBit));
            }

            // Fail if string too long
            if (url.Length > Limit)
            {
                List<object> tooLong = new List<object> {0, "Message Too Long."};
                return tooLong;
            }

            // Create Request
            HttpWebRequest request = (HttpWebRequest)
                WebRequest.Create(url.ToString());

            // Set Timeout
            request.Timeout = 200000;
            request.ReadWriteTimeout = 200000;

            // Receive Response
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream resStream = response.GetResponseStream();

            // Read
            do
            {
                if (resStream != null) count = resStream.Read(buf, 0, buf.Length);
                if (count != 0)
                {
                    string temp = Encoding.UTF8.GetString(buf, 0, count);
                    sb.Append(temp);
                }
            } while (count > 0);

            // Parse Response
            string message = sb.ToString();

            return serializer.Deserialize<List<object>>(message);
        }

        private string _encodeURIcomponent(string s)
        {
            StringBuilder o = new StringBuilder();
            foreach (char ch in s)
            {
                if (isUnsafe(ch))
                {
                    o.Append('%');
                    o.Append(toHex(ch / 16));
                    o.Append(toHex(ch % 16));
                }
                else o.Append(ch);
            }
            return o.ToString();
        }

        private char toHex(int ch)
        {
            return (char)(ch < 10 ? '0' + ch : 'A' + ch - 10);
        }

        private bool isUnsafe(char ch)
        {
            return " ~`!@#$%^&*()+=[]\\{}|;':\",./<>?".IndexOf(ch) >= 0;
        }

        public static string Md5(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] data = Encoding.Default.GetBytes(text);
            byte[] hash = md5.ComputeHash(data);
            return hash.Aggregate("", (current, b) => current + String.Format("{0:x2}", b));
        }
    }




}
