using System.Collections.Generic;

namespace NTB.SportsData.Utilities.Interfaces
{
    public abstract class PushNetwork
    {
        public delegate bool Procedure(PubNubMessage message);

        public abstract void Init(string publishKey, string subscribeKey, string secretKey, bool sslOn);
        public abstract List<PubNubMessage> History(string channel, int limit);
        public abstract List<object> Publish(string channel, object message);
        public abstract void Subscribe(string channel, Procedure callback);
        public abstract object Time();
    }
}
