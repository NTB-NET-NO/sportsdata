﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsData.Service.Services.NFF.NFFProdService;

namespace NTB.SportsData.Service.Services.NFF.Interfaces
{
    public interface ISeasonDataMapper
    {
        List<Season> GetSeasons();
    }
}
