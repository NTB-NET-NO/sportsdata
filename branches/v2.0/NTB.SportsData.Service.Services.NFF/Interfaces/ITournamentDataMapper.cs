﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsData.Service.Services.NFF.NFFProdService;

namespace NTB.SportsData.Service.Services.NFF.Interfaces
{
    public interface ITournamentDataMapper
    {
        Tournament GetTournament(int tournamentId);
        List<TournamentTableTeam> GetTournamentStanding(int tournamentId);
        List<Tournament> GetTournamentsByDistrict(int districtId, int seasonId);
    }
}
