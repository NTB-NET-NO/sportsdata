﻿using System.Collections.Generic;
using NTB.SportsData.Service.Services.NFF.NFFProdService;

namespace NTB.SportsData.Service.Services.NFF.Interfaces
{
    public interface IDistrictDataMapper
    {
        List<District> GetDistricts();
    }
}
