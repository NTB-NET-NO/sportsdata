﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsData.Service.Services.NFF.NFFProdService;

namespace NTB.SportsData.Service.Services.NFF.Interfaces
{
    public interface IMatchDataMapper
    {
        Match GetMatch(int matchId);
        List<Match> GetMatchesByTournament(int tournamentId, bool includeSquad, bool includeReferees, bool includeResults, bool includeEvents, DateTime? todayDateTime);

        List<Match> GetMatchesByDistrict(int districtId, DateTime matchStartDate, DateTime matchEndDate, bool includeSquad, bool includeReferees, bool includeResults,
                                              bool includeEvents, DateTime? todayDateTime);

        List<Match> GetMatchesByDateInterval(DateTime dateStart, DateTime dateEnd, int districtId, bool includeSquad, bool includeReferees, bool includeResults, bool includeEvents);
    }
}
