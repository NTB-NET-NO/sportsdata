﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using NTB.SportsData.Service.Services.NFF.Interfaces;
using NTB.SportsData.Service.Services.NFF.NFFProdService;
using log4net;

namespace NTB.SportsData.Service.Services.NFF.Repositories
{
    public class MunicipalityRepository : IRepository<Municipality>, IDisposable, IMunicipalityDataMapper
    {
        private readonly MetaServiceClient _serviceClient = new MetaServiceClient();
        private static readonly ILog Logger = LogManager.GetLogger(typeof(MunicipalityRepository));

        public MunicipalityRepository()
        {
            if (_serviceClient.ClientCredentials != null)
            {
                _serviceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServiceUsername"];
                _serviceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicePassword"];
            }

            // Set up logger
            log4net.Config.XmlConfigurator.Configure();

            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        public int InsertOne(Municipality domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Municipality> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Municipality domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Municipality domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Municipality> GetAll()
        {
            throw new NotImplementedException();
        }

        public Municipality Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<Municipality> GetMunicipalitiesByDistrict(int districtId)
        {
            return _serviceClient.GetMunicipalitiesbyDistrict(districtId).ToList();
        }
    }
}
