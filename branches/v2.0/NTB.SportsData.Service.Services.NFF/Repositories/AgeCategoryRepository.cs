﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

using NTB.SportsData.Service.Services.NFF.Interfaces;
using NTB.SportsData.Service.Services.NFF.NFFProdService;

using log4net;

namespace NTB.SportsData.Service.Services.NFF.Repositories
{
    public class AgeCategoryRepository : IRepository<AgeCategoryTournament>, IDisposable, IAgeCategoryDataMapper 
    {
        private readonly MetaServiceClient _serviceClient = new MetaServiceClient();
        private static readonly ILog Logger = LogManager.GetLogger(typeof(AgeCategoryRepository));

        public AgeCategoryRepository()
        {
            if (_serviceClient.ClientCredentials != null)
            {
                _serviceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServiceUsername"];
                _serviceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicePassword"];
            }

            // Set up logger
            log4net.Config.XmlConfigurator.Configure();

            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }
        public int InsertOne(AgeCategoryTournament domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<AgeCategoryTournament> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(AgeCategoryTournament domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(AgeCategoryTournament domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<AgeCategoryTournament> GetAll()
        {
            throw new NotImplementedException();
        }

        public AgeCategoryTournament Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<AgeCategoryTournament> GetAgeCategoriesTournament()
        {
            return _serviceClient.GetAgeCategoriesTournament().ToList();
        }
    }
}
