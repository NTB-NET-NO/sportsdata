﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using NTB.SportsData.Service.Services.NFF.Interfaces;
using NTB.SportsData.Service.Services.NFF.NFFProdService;

using log4net;

namespace NTB.SportsData.Service.Services.NFF.Repositories
{
    public class MatchRepository : IRepository<Match>, IDisposable, IMatchDataMapper
    {
        private readonly TournamentServiceClient _serviceClient = new TournamentServiceClient();

        private static readonly ILog Logger = LogManager.GetLogger(typeof(MatchRepository));

        public MatchRepository()
        {
            if (_serviceClient.ClientCredentials != null)
            {
                _serviceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServiceUsername"];
                _serviceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicePassword"];
            }

            // Set up logger
            log4net.Config.XmlConfigurator.Configure();

            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }
        public int InsertOne(Match domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Match> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Match domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Match domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Match> GetAll()
        {
            throw new NotImplementedException();
        }

        public Match Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Match GetMatch(int matchId)
        {
            try
            {
                return _serviceClient.GetMatch(matchId, true, true, true, true);
            }
            catch (Exception exception)
            {
                Logger.Info("MatchId: " + matchId);
                Logger.Info(exception.Message);

                return new Match();
            }
        }

        public List<Match> GetMatchesByTournament(int tournamentId, bool includeSquad, bool includeReferees, bool includeResults,
                                              bool includeEvents, DateTime? todayDateTime)
        {
            return
                _serviceClient.GetMatchesByTournament(tournamentId, includeSquad, includeReferees, includeResults,
                                                      includeEvents, todayDateTime).ToList();
        }

        public List<Match> GetMatchesByDistrict(int districtId, DateTime matchStartDate, DateTime matchEndDate, bool includeSquad, bool includeReferees,
                                         bool includeResults, bool includeEvents, DateTime? todayDateTime)
        {
            return _serviceClient.GetMatchesByDistrict(districtId, matchStartDate, matchEndDate, includeSquad,
                                                       includeReferees, includeResults, includeEvents, todayDateTime).ToList();
        }

        public List<Match> GetMatchesByDateInterval(DateTime dateStart, DateTime dateEnd, int districtId, bool includeSquad,
                                             bool includeReferees, bool includeResults, bool includeEvents)
        {
            return
                _serviceClient.GetMatchesByDateInterval(dateStart, dateEnd, districtId, includeSquad, includeReferees,
                                                        includeResults, includeEvents).ToList();
        }
    }
}
