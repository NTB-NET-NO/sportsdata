﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using NTB.SportsData.Service.Services.NFF.Interfaces;
using NTB.SportsData.Service.Services.NFF.NFFProdService;
using log4net;

namespace NTB.SportsData.Service.Services.NFF.Repositories
{
    public class DistrictRepository : IRepository<District>, IDisposable, IDistrictDataMapper
    {
        private readonly OrganizationServiceClient _serviceClient = new OrganizationServiceClient();
        private static readonly ILog Logger = LogManager.GetLogger(typeof(AgeCategoryRepository));

        public DistrictRepository()
        {
            if (_serviceClient.ClientCredentials != null)
            {
                _serviceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServiceUsername"];
                _serviceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicePassword"];
            }

            // Set up logger
            log4net.Config.XmlConfigurator.Configure();

            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }
        public int InsertOne(District domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<District> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(District domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(District domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<District> GetAll()
        {
            throw new NotImplementedException();
        }

        public District Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<District> GetDistricts()
        {
            return _serviceClient.GetDistricts().ToList();
        }
    }
}
