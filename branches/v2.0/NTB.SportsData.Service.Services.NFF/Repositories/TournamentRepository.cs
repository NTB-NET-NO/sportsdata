﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using NTB.SportsData.Service.Services.NFF.Interfaces;
using NTB.SportsData.Service.Services.NFF.NFFProdService;
using log4net;

namespace NTB.SportsData.Service.Services.NFF.Repositories
{
    public class TournamentRepository : IRepository<Tournament>, IDisposable, ITournamentDataMapper 
    {
        private readonly TournamentServiceClient _serviceClient = new TournamentServiceClient();
        private static readonly ILog Logger = LogManager.GetLogger(typeof(TournamentRepository));

        public TournamentRepository()
        {
            if (_serviceClient.ClientCredentials != null)
            {
                _serviceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NFFServiceUsername"];
                _serviceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NFFServicePassword"];
            }

            // Set up logger
            log4net.Config.XmlConfigurator.Configure();

            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }
        public int InsertOne(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Tournament> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Tournament> GetAll()
        {
            throw new NotImplementedException();
        }

        public Tournament Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Tournament GetTournament(int tournamentId)
        {
            return _serviceClient.GetTournament(tournamentId);
        }

        public List<TournamentTableTeam> GetTournamentStanding(int tournamentId)
        {
            return _serviceClient.GetTournamentStanding(tournamentId, null).ToList();
        }

        public List<Tournament> GetTournamentsByDistrict(int districtId, int seasonId)
        {
            return _serviceClient.GetTournamentsByDistrict(districtId, seasonId, null).ToList();
        }
    }
}
