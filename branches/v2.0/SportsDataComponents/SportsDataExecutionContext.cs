﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SportsDataExecutionContext.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Components
{
    using System;

    using Quartz;

    using QuartzScheduler;

    /// <summary>
    /// The sports data execution context.
    /// </summary>
    internal class SportsDataExecutionContext : IJobExecutionContext
    {
        /// <summary>
        /// Gets or sets the calendar.
        /// </summary>
        public ICalendar Calendar { get; set; }

        /// <summary>
        /// Gets or sets the fire instance id.
        /// </summary>
        public string FireInstanceId { get; set; }

        /// <summary>
        /// Gets or sets the fire time utc.
        /// </summary>
        public DateTimeOffset? FireTimeUtc { get; set; }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public object Get(object key)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets or sets the job detail.
        /// </summary>
        public IJobDetail JobDetail { get; set; }

        /// <summary>
        /// Gets or sets the job instance.
        /// </summary>
        public IJob JobInstance { get; set; }

        /// <summary>
        /// Gets or sets the job run time.
        /// </summary>
        public TimeSpan JobRunTime { get; set; }

        /// <summary>
        /// Gets or sets the merged job data map.
        /// </summary>
        public JobDataMap MergedJobDataMap { get; set; }

        /// <summary>
        /// Gets or sets the next fire time utc.
        /// </summary>
        public DateTimeOffset? NextFireTimeUtc { get; set; }

        /// <summary>
        /// Gets or sets the previous fire time utc.
        /// </summary>
        public DateTimeOffset? PreviousFireTimeUtc { get; set; }

        /// <summary>
        /// The put.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="objectValue">
        /// The object value.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Put(object key, object objectValue)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets or sets a value indicating whether recovering.
        /// </summary>
        public bool Recovering { get; set; }

        /// <summary>
        /// Gets or sets the refire count.
        /// </summary>
        public int RefireCount { get; set; }

        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public object Result
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Gets the scheduled fire time utc.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public DateTimeOffset? ScheduledFireTimeUtc
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Gets the scheduler.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public IScheduler Scheduler
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Gets the trigger.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public ITrigger Trigger
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        ICalendar IJobExecutionContext.Calendar
        {
            get { throw new NotImplementedException(); }
        }

        IJobDetail IJobExecutionContext.JobDetail
        {
            get { throw new NotImplementedException(); }
        }

        IJob IJobExecutionContext.JobInstance
        {
            get { throw new NotImplementedException(); }
        }

        JobDataMap IJobExecutionContext.MergedJobDataMap
        {
            get { throw new NotImplementedException(); }
        }

        public TriggerKey RecoveringTriggerKey
        {
            get { throw new NotImplementedException(); }
        }

        IScheduler IJobExecutionContext.Scheduler
        {
            get { throw new NotImplementedException(); }
        }

        ITrigger IJobExecutionContext.Trigger
        {
            get { throw new NotImplementedException(); }
        }
    }
}