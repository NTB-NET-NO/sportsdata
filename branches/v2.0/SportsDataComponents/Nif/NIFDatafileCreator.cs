// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NIFDatafileCreator.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using NTB.SportsData.Service.Components.Factories;
using NTB.SportsData.Service.Components.Repositories;
using NTB.SportsData.Service.Domain.Classes;
using log4net;

namespace NTB.SportsData.Service.Components.Nif
{
    // Adding support for log4net
        // NFF service references
    
    /// <summary>
    /// The nif datafile creator.
    /// </summary>
    public class NifDatafileCreator
    {
        
        /// <summary>
        /// The match id.
        /// </summary>
        public int MatchId = 0;

        /// <summary>
        /// Static logger
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(NifDatafileCreator));
        
        /// <summary>
        /// The _date duration.
        /// </summary>
        private int _dateDuration;

        /// <summary>
        /// Initializes a new instance of the <see cref="NifDatafileCreator"/> class. 
        /// Constructor for the NIFDatafileCreator object. This is used to create Structures
        /// </summary>
        public NifDatafileCreator()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        #region getters and setters

        /// <summary>
        /// Gets or sets the federation id.
        /// </summary>
        public int FederationId { get; set; }

        /// <summary>
        /// Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        /// Gets or sets the output folder.
        /// </summary>
        public string OutputFolder { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether update database.
        /// </summary>
        public bool UpdateDatabase { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether create xml.
        /// </summary>
        public bool CreateXml { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether render result.
        /// </summary>
        public bool RenderResult { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether schedule message.
        /// </summary>
        public bool ScheduleMessage { get; set; }

        /// <summary>
        /// Gets or sets the date start.
        /// </summary>
        public DateTime DateStart { get; set; }

        /// <summary>
        /// Gets or sets the date end.
        /// </summary>
        public DateTime DateEnd { get; set; }

        /// <summary>
        /// Gets or sets the customers.
        /// </summary>
        public List<Customer> Customers { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether push results.
        /// </summary>
        public bool PushResults { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether push schedule.
        /// </summary>
        public bool PushSchedule { get; set; }

        /// <summary>
        /// Gets or sets the duration.
        /// </summary>
        public int Duration
        {
            get
            {
                return _dateDuration;
            }

            set
            {
                if (value > 0)
                {
                    _dateDuration = value;
                }
            }
        }

        // private void GetOrganizations()
        // {

        // }

        // private void Results()
        // {

        // }

        // private void SetTodayDates()
        // {
        // // Checking if we are to use a test date or not
        // this.DateStart = Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]) == true ? DateTime.Parse("2011-05-16") : DateTime.Today;

        // // If this parameter is set, we shall also add duration date
        // if (this._dateDuration > 0)
        // {
        // DateEnd = DateStart.AddDays(this._dateDuration);
        // }
        // }
        #endregion getters and setters

        #region Configuration

        /// <summary>
        /// This method is used to configure the datafile object. It shall check if we have rows in Tournament and Districts Databases
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Configure()
        {
            try
            {
                Logger.Debug("Configuring DatafileCreator");

                return true;
            }
            catch (Exception e)
            {
                Logger.Error("An exception has occured: " + e);
                return false;
            }
        }

        #endregion Configuration


        /// <summary>
        /// Create Data File Form Get 
        /// </summary>
        public void CreateDataFileFromGet()
        {
            // This means that we shall create results files, we shall also update the database and set download to true
            if (RenderResult)
            {
                Logger.Debug("We are to create results, and then also XML");
                CheckEventsAgainstDatabase();

                return;
            }

            Logger.Debug("We are to create a document or populate the database with data");
            PopulateEventsDatabaseTable();
        }

        
        /// <summary>
        /// The _ populate events database table.
        /// </summary>
        private void PopulateEventsDatabaseTable()
        {
            Logger.Debug("We are in PopulateEventsDatabaseTable");

            var facade = new FacadeFactory().GetFacade(SportId);


            var organizationDataMapper = new OrganizationDataMapper();

            Organization organization = organizationDataMapper.GetOrgIdBySportId(SportId);

            Activity activity = facade.GetActivityByOrgId(organization.OrganizationId, organization.SportId);
            
            if (activity == null)
            {
                throw new Exception("Cannot find Activity for this Organization");
            }

            List<SportEvent> sportEvents = facade.GetEventsBySportId(SportId);

            // Insert data into database
            var eventRepostory = new EventDataMapper();
            eventRepostory.InsertAll(sportEvents);
        }

        /// <summary>
        /// This method is used to check which events are in the database that we can query from the NIF Services
        /// </summary>
        private void CheckEventsAgainstDatabase()
        {
            // Creating the sportDataBase object with parameters sport and federation
            EventDataMapper eventDataMapper = new EventDataMapper();
            List<SportEvent> listOfSportsEvents = eventDataMapper.GetAll().ToList();

            var facade = new FacadeFactory().GetFacade(SportId);

            if (!listOfSportsEvents.Any())
            {
                // Now we need to update the database or check online
                PopulateEventsDatabaseTable();
            }

            Logger.Info("We have " + listOfSportsEvents.Count() + " items to check");

            foreach (SportEvent databaseEvent in listOfSportsEvents)
            {
                SportEvent sportEvent = new SportEvent();
                SportEvent remoteSportEvent = facade.GetEventByEventId(databaseEvent.EventId);

                List<Activity> activities =
                    facade.GetAllFirstLevelActivities(Convert.ToInt32(databaseEvent.ParentActivityId));


                List<Activity> queryActivity = new List<Activity>(
                    from ac in activities
                    where
                        remoteSportEvent.ParentActivityId != null
                        && ac.ActivityId == remoteSportEvent.ParentActivityId.Value
                    select ac);

                foreach (Activity activity in queryActivity)
                {
                    sportEvent.ParentActivity = activity.ActivityName;
                    sportEvent.ParentActivityId = activity.ActivityId;
                }
            }
        }
    }
}