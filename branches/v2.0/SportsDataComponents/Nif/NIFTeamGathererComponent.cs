// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NIFTeamGathererComponent.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the NifTeamGathererComponent type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Xml;
using NTB.SportsData.Service.Components.Interfaces;
using NTB.SportsData.Service.Components.TeamSport;
using NTB.SportsData.Service.Domain.Enums;
using log4net;
using Quartz;
using Quartz.Impl;
using Quartz.Impl.Matchers;

namespace NTB.SportsData.Service.Components.Nif
{
    using NTB.SportsData.Service.Components.QuartzScheduler;

    // Logging
        // Adding support for methods in the Utilities namespace
    // using NTB.SportsData.Utilities;
        // Quartz support

    /// <summary>
    /// The nif team gatherer component.
    /// </summary>
    public partial class NifTeamGathererComponent : Component, IBaseSportsDataInterfaces
    {
        /// <summary>
        /// The Configured job name for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="InstanceName"/></remarks>
        protected string Name;

        /// <summary>
        /// Flag to indicate sucessful configure. Instance will not start polling nor handle messages if not properly Configured
        /// </summary>
        /// <remarks>Holds the Configured status of the instance. <c>True</c> means successfully Configured</remarks>
        protected bool Configured = false;

        /// <summary>
        /// Error-Retry flag
        /// </summary>
        /// <remarks>The flag is being set if a network or EWS error is encountered. Current operations are being aborted for later retry.</remarks>
        protected bool ErrorRetry = false;

        /// <summary>
        /// Send Email-notifications when a new messages is processed
        /// </summary>
        /// <remarks>Set to an email address. Multiple adresses are supported, separate with <c>;</c></remarks>
        protected string EmailNotification;

        /// <summary>
        /// Subject for email notifications
        /// </summary>
        /// <remarks>The subject is built during processing and used when sending the email when processing completes</remarks>
        protected string EmailSubject;

        /// <summary>
        /// Body for email notifications
        /// </summary>
        /// <remarks>The body is built during processing and used when sending the email when processing completes</remarks>
        protected string EmailBody;

        /// <summary>
        /// The schedule type.
        /// </summary>
        protected ScheduleType ScheduleType;

        /// <summary>
        /// The populate database.
        /// </summary>
        protected bool PopulateDatabase = false;

        /// <summary>
        /// The purge.
        /// </summary>
        protected bool Purge = false;

        /// <summary>
        /// The create xml.
        /// </summary>
        protected bool CreateXml = false;

        /// <summary>
        /// schedulerFactory to be used to create scheduled tasks
        /// </summary>
        protected StdSchedulerFactory SchedulerFactory = new StdSchedulerFactory();

        /// <summary>
        /// The scheduler.
        /// </summary>
        protected IScheduler Scheduler;

        /// <summary>
        /// The interval.
        /// </summary>
        protected int Interval = 60;

        /// <summary>
        /// The push results.
        /// </summary>
        protected bool PushResults = false;

        /// <summary>
        /// The push schedule.
        /// </summary>
        protected bool PushSchedule = false;

        /// <summary>
        /// The Configured interval for this instance, used for Continous polling
        /// </summary>
        /// <remarks>
        ///   <para>Indicates the interval time in seconds for <c>Continous</c>polling. 60 means that the job runs every minute.</para>
        ///   <para>Default value: <c>60</c></para>
        /// </remarks>
        /// <summary>
        /// Input file folder
        /// </summary>
        /// <remarks>
        /// File folder where incoming files are read from. Inputs can be modified by <see>
        ///                                                                               <cref>fileFilter</cref>
        ///                                                                           </see>
        ///     and <see>
        ///             <cref>includeSubdirs</cref>
        ///         </see>
        /// </remarks>
        protected string FileOutputFolder;

        /// <summary>
        /// Error file folder
        /// </summary>
        /// <remarks>
        ///   <para>File folder where failing files are stored.</para>
        ///   <para>If this is not set, failing files are deleted instead of saved.</para>
        /// </remarks>
        protected string FileErrorFolder;

        /// <summary>
        /// File folder where completed files are stored
        /// </summary>
        /// <remarks>
        ///   <para>File folder where completed files are archived. Subdirectories for years, months and dates are created.</para>
        ///   <para>If this is not set, imported files are deleted instead of archived.</para>
        /// </remarks>
        protected string FileDoneFolder;

        /// <summary>
        /// The season id.
        /// </summary>
        protected int SeasonId = 0;

        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(NifGathererComponent));

        /// <summary>
        /// Busy status event
        /// </summary>
        private readonly AutoResetEvent _busyEvent = new AutoResetEvent(true);

        /// <summary>
        /// Exit control event
        /// </summary>
        private readonly AutoResetEvent _stopEvent = new AutoResetEvent(false);

        /// <summary>
        /// The _data file creator.
        /// </summary>
        private DataFileCreator _dataFileCreator = new DataFileCreator();

        /// <summary>
        /// The _daily job listnerer.
        /// </summary>
        private NifTeamSportsDataJobListener _dailyJobListnerer = new NifTeamSportsDataJobListener();

        /// <summary>
        /// The _weekly job listener.
        /// </summary>
        private NifTeamSportsDataJobListener _weeklyJobListener = new NifTeamSportsDataJobListener();

        /// <summary>
        /// The _biweekly job listener.
        /// </summary>
        private NifTeamSportsDataJobListener _biweeklyJobListener = new NifTeamSportsDataJobListener();

        /// <summary>
        /// Initializes static members of the <see cref="NifTeamGathererComponent"/> class.
        /// </summary>
        static NifTeamGathererComponent()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NifTeamGathererComponent"/> class.
        /// </summary>
        public NifTeamGathererComponent()
        {
            InitializeComponent();
        }

        /// <summary>
        /// The validate server certificate.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="certificate">
        /// The certificate.
        /// </param>
        /// <param name="chain">
        /// The chain.
        /// </param>
        /// <param name="sslPolicyErrors">
        /// The ssl policy errors.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool ValidateServerCertificate(
            object sender,
            X509Certificate certificate,
            X509Chain chain,
            SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        /// <summary>
        /// The trust all certificate policy.
        /// </summary>
        public class TrustAllCertificatePolicy : ICertificatePolicy
        {
            /// <summary>
            /// The check validation result.
            /// </summary>
            /// <param name="sp">
            /// The sp.
            /// </param>
            /// <param name="cert">
            /// The cert.
            /// </param>
            /// <param name="req">
            /// The req.
            /// </param>
            /// <param name="problem">
            /// The problem.
            /// </param>
            /// <returns>
            /// The <see cref="bool"/>.
            /// </returns>
            public bool CheckValidationResult(ServicePoint sp, X509Certificate cert, WebRequest req, int problem)
            {
                return true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether enabled.
        /// </summary>
        public bool Enabled
        {
            get { return _Enabled; }
        }

        /// <summary>
        /// Gets the name of the instance.
        /// </summary>
        /// <value>The name of the instance.</value>
        /// <remarks>The Configured instance job name.</remarks>
        public string InstanceName
        {
            get { return Name; }
        }

        /// <summary>
        /// Gets the operation mode. 
        /// </summary>
        /// <value>The operation mode.</value>
        /// <remarks><c>Gatherer</c> is the only valid mode, its hard coded for this component.</remarks>
        public OperationMode OperationMode { get; set; }
        
        /// <summary>
        /// Gets the poll style.
        /// </summary>
        /// <value>The poll style.</value>
        /// <remarks>Contionous, Scheduled and FileSystemWatch are valid for <c>Gatherer</c> objects.</remarks>
        public PollStyle PollStyle
        {
            get { return pollStyle; }
        }

        /// <summary>
        /// Gets or sets the sport id.
        /// </summary>
        protected int SportId { get; set; }

        /// <summary>
        /// Gets or sets the federation id.
        /// </summary>
        protected int FederationId { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="NifTeamGathererComponent"/> class.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        public NifTeamGathererComponent(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        /// <summary>
        /// The configure.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        /// <exception cref="ArgumentException">
        /// returns an exception if something goes wrong
        /// </exception>
        /// <exception cref="Exception">
        /// returns an exception if something goes wrong
        /// </exception>
        /// <exception cref="NotSupportedException">
        /// returns an exception if something goes wrong
        /// </exception>
        public void Configure(XmlNode configNode)
        {
            Logger.Debug("Node: " + configNode.Name);
            if (configNode.Attributes != null)
            {
                Logger.Debug("Name attribute: " + configNode.Attributes.GetNamedItem("Name"));


                #region Basic configuration


                // Basic configuration sanity check
                if (configNode.Name != "NIFTeamComponent" || configNode.Attributes.GetNamedItem("Name") == null)
                {
                    throw new ArgumentException("The XML configuration node passed is invalid", "configNode");
                }

                if (Thread.CurrentThread.Name == null)
                {
                    Thread.CurrentThread.Name = configNode.Attributes.GetNamedItem("Name").ToString();
                }

                #endregion

                #region Basic config

                // Getting the OperationMode which tells if this what type of OperationMode this is
                try
                {
                    OperationMode = (OperationMode)Enum.Parse(typeof(OperationMode), configNode.Attributes["OperationMode"].Value, true);
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing OperationMode values in XML configuration node", ex);
                }

                // Getting the PollStyle which tells if this what type of PollStyle this is
                try
                {
                    pollStyle = (PollStyle)Enum.Parse(typeof(PollStyle), configNode.Attributes["PollStyle"].Value, true);
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing PollStyle values in XML configuration node", ex);
                }


                // Getting the ScheduleType which tells if this what type of scheduling we are doing
                try
                {
                    ScheduleType = (ScheduleType)Enum.Parse(typeof(ScheduleType), configNode.Attributes["ScheduleType"].Value, true); // ScheduleType = (ScheduleT
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing ScheduleType values in XML configuration node", ex);
                }
            }

                #endregion

            // Some values that we need.

            // This boolean variable is used to tell us that the database shall be populated or not
            PopulateDatabase = false;

            Purge = false;

            CreateXml = false;

            // Getting the name of this Component instance
            try
            {
                if (configNode.Attributes != null)
                {
                    Name = configNode.Attributes["Name"].Value;
                    _Enabled = Convert.ToBoolean(configNode.Attributes["Enabled"].Value);
                }
                ThreadContext.Stacks["NDC"].Push(InstanceName);

                // This value is used to tell that we shall insert into database... I just wonder if we shall do it
                // on the schedule-level... Well, we try here first
                // DONE: Consider using schedule-level to decide if we are to insert into database or not


                // Check if we are to insert the data into the database
                if (configNode.Attributes != null && configNode.Attributes["InsertDatabase"] != null)
                {
                    PopulateDatabase = Convert.ToBoolean(configNode.Attributes["InsertDatabase"].Value);
                }

                // Check if we are to create an XML-File
                if (configNode.Attributes != null && configNode.Attributes["XML"] != null)
                {
                    CreateXml = Convert.ToBoolean(configNode.Attributes["XML"].Value);
                }

            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to configure this job instance!", ex);
            }

            // Getting and setting the Federation Id and the Sport Id
            try
            {
                XmlNode federationNode = configNode.SelectSingleNode("Federation");

                if (federationNode != null && federationNode.Attributes != null)
                {
                    if (federationNode.Attributes["Id"] != null)
                    {
                        if (federationNode.Attributes["orgId"] != null)
                        {
                            FederationId = Convert.ToInt32(federationNode.Attributes["orgId"].Value);
                        }

                        if (federationNode.Attributes["sportId"] != null)
                        {
                            SportId = Convert.ToInt32(federationNode.Attributes["sportId"].Value);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException(
                    "Invalid or missing Federation if and sport id values in XML configuration node", ex);
            }


            #region File Configuration
            try
            {
                // Find the file folder to work with
                // DONE: This could be a nodelist though - need to check if we get more than one result
                XmlNodeList folderNodes = configNode.SelectNodes("Folders/Folder[@Type='XMLOutputFolder']");
                if (folderNodes != null)
                    foreach (XmlNode folderNode in folderNodes)
                    {
                        // XmlNode folderNode = configNode.SelectSingleNode("Folders/Folder[@Type='XMLOutputFolder']");
                        // configNode.SelectSingleNode("Folder").Attributes("XMLOutputFolder");
                        if (folderNode != null)
                        {
                            FileOutputFolder = folderNode.InnerText;

                        }

                        Logger.InfoFormat("Gatherer job - Polling: {0} / File output folder: {1} / Enabled: {2}",
                                          Enum.GetName(typeof(PollStyle), pollStyle), FileOutputFolder, _Enabled);
                    }
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get output-folder", ex);
            }

            #endregion

            if (_Enabled)
            {
                #region File folders to access
                // Checking if file folders exists
                try
                {
                    if (FileOutputFolder != null)
                    {
                        if (!Directory.Exists(FileOutputFolder))
                        {
                            Directory.CreateDirectory(FileOutputFolder);
                        }
                    }


                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid, unknown or missing file folder: " + FileOutputFolder, ex);
                }

                #endregion

                // Creating Instance Name

                #region Set up polling
                try
                {
                    //Switch on pollstyle
                    switch (pollStyle)
                    {
                        #region PollStyle.Continous
                        case PollStyle.Continous:
                            if (configNode.Attributes != null)
                                Interval = Convert.ToInt32(configNode.Attributes["Interval"].Value);
                            XmlNodeList folderNodeList =
                                    configNode.SelectNodes("../NIFTeamComponent[@Name='" + InstanceName + "']/Folders/Folder");


                            int folderJobs = 0;
                            if (folderNodeList != null)
                            {
                                folderJobs = folderNodeList.Count;
                            }

                            // <Folder Type="XMLOutputFolder" Mode="FILE" Results="True" PushSchedule="False">C:\Utvikling\norm\xml\NTBSportsData</Folder>
                            // string OutputFolder = "";
                            Logger.Debug("Number of Folders: " + folderJobs);
                            if (folderNodeList != null)
                            {
                                foreach (XmlNode foldernode in folderNodeList)
                                {
                                    if (foldernode.Attributes != null && foldernode.Attributes["Result"] != null)
                                    {
                                        PushResults = Convert.ToBoolean(foldernode.Attributes["Result"].Value);
                                    }

                                    if (foldernode.Attributes != null && foldernode.Attributes["PushSchedule"] != null)
                                    {
                                        PushSchedule = Convert.ToBoolean(foldernode.Attributes["PushSchedule"].Value);
                                    }

                                    FileOutputFolder = foldernode.InnerText;
                                }
                            }

                            if (Interval == 0)
                            {
                                Interval = 5000;
                            }
                            Interval = Interval * 60; // Adding so that it is one minute
                            pollTimer.Interval = Interval; // short startup - interval * 1000;
                            Logger.DebugFormat("Poll interval: {0} seconds", Interval);

                            break;
                        #endregion

                        #region PollStyle.PushSubscribe

                        // Code for Pubnub push technology
                        // CONTINUE HERE TOMORROW! (Is this done, what am I to do here tomorrow) (2012-03-09)
                        case PollStyle.PushSubscribe:

                            break;
                        #endregion

                        #region PollStyle.CalendarPoll
                        case PollStyle.CalendarPoll:
                            // Getting the times for this job

                            try
                            {
                                IJobDetail niFweeklyJobDetail = null;
                                // = JobBuilder.Create<TournamentMatches>().WithIdentity("Job1", "Group1").Build();

                                ITrigger niFweeklyTrigger = null;


                                Logger.Info("Setting up schedulers");
                                Logger.Info("--------------------------------------------------------------");

                                // Getting the number of jobs to create
                                XmlNodeList nodeList =
                                    configNode.SelectNodes("../NIFTeamComponent[@Name='" + InstanceName + "']/Schedules/Schedule");

                                int jobs = 0;
                                if (nodeList != null)
                                {
                                    jobs = nodeList.Count;
                                }
                                Logger.Debug("Number of jobs: " + jobs);

                                // Creating a string to store the ScheduleID
                                string scheduleId;

                                // Creating a boolean variable to store if we are to get results out or not
                                bool results = false;

                                // Creating an integer value to be used to tell the duration of the gathering of data
                                int duration = 0;

                                // Creating a boolean variable to store if this is a schedule-message og not
                                bool scheduleMessage = false;

                                // Creating integer for offset
                                // This variable is used to tell that we are to get tomorrows matches or yesterdays matches
                                // Used to create the 'Todays matches' for newspapers
                                // So a positive number is most commonly used

                                DateTime dtOffset = DateTime.Today;


                                Logger.Debug("Schedule of type: " + ScheduleType.ToString());

                                // Getting the scheduler
                                var scheduler1 = SchedulerFactory.GetScheduler();

                                switch (ScheduleType)
                                {

                                    #region ScheduleType.Daily
                                    case ScheduleType.Daily:
                                        if (nodeList != null)
                                            foreach (XmlNode node in nodeList)
                                            {
                                                scheduleId = node.Attributes["Id"].Value;

                                                // Creating the offset time 
                                                int offset = 0;
                                                if (node.Attributes["Offset"] != null)
                                                {
                                                    offset = Convert.ToInt32(node.Attributes["Offset"].Value);
                                                }

                                                // If the value is larger or less than 0
                                                if (offset > 0 || offset < 0)
                                                {
                                                    if (offset > 0)
                                                    {
                                                        dtOffset = DateTime.Today.AddDays(offset);
                                                    }
                                                    else if (offset < 0)
                                                    {
                                                        TimeSpan tsOffset = TimeSpan.FromDays(offset);
                                                        dtOffset = DateTime.Today.Subtract(tsOffset);
                                                    }
                                                }
                                                // Splitting the time into two values so that we can create a cron job
                                                string[] scheduleTimeArray =
                                                    node.Attributes["Time"].Value.Split(new[] { ':' });

                                                // If minutes contains two zeros (00), we change it to one zero (0) 
                                                // If not, scheduler won't understand
                                                if (scheduleTimeArray[1] == "00")
                                                {
                                                    scheduleTimeArray[1] = "0";
                                                }

                                                // Doing the same thing for hours
                                                if (scheduleTimeArray[0] == "00")
                                                {
                                                    scheduleTimeArray[0] = "0";
                                                }

                                                // Checking if there is a setting for results
                                                if (node.Attributes["Results"] != null)
                                                {
                                                    results = Convert.ToBoolean(node.Attributes["Results"].Value);
                                                    scheduleMessage = !results;
                                                }

                                                // Checking if there is a setting for Duration
                                                if (node.Attributes["Duration"] != null)
                                                {
                                                    duration = Convert.ToInt32(node.Attributes["Duration"].Value);
                                                }

                                                /*
                                             * Creating cron expressions and more. 
                                             * This is the cron syntax:
                                             *  Seconds
                                             *  Minutes
                                             *  Hours
                                             *  Day-of-Month
                                             *  Month
                                             *  Day-of-Week
                                             *  Year (optional field)
                                             */



                                                // Creating the daily cronexpression
                                                string stringCronExpression = "0 " +
                                                                              scheduleTimeArray[1] + " " +
                                                                              scheduleTimeArray[0] + " " +
                                                                              "? " +
                                                                              "* " +
                                                                              "* ";


                                                // Setting up the CronTrigger
                                                Logger.Debug("Setting up the CronTrigger with following pattern: " + stringCronExpression);

                                                // Setting up the Daily CronTrigger
                                                // new CronExpression(stringCronExpression);


                                                // dailyCronTrigger = new CronTrigger(InstanceName + ScheduleID, InstanceName);
                                                // dailyCronTrigger.CronExpression = cronExpression;


                                                // Creating the daily Cron Trigger
                                                ITrigger niFdailyCronTrigger = TriggerBuilder.Create()
                                                                                             .WithIdentity(InstanceName + "_" + scheduleId, "GroupNIF")
                                                                                             .WithDescription(InstanceName)
                                                                                             .WithCronSchedule(stringCronExpression)
                                                                                             .Build();






                                                Logger.Debug("dailyCronTrigger: " + niFdailyCronTrigger.Description);

                                                // Creating the jobDetail 
                                                IJobDetail niFdailyJobDetail = TeamSportJobBuilder.Create<NifJobBuilder>()
                                                                                         .WithIdentity("job_" + InstanceName + "_" + scheduleId, "GroupNIF")
                                                                                         .WithDescription(InstanceName)
                                                                                         .Build();

                                                if (FileOutputFolder != null)
                                                {
                                                    niFdailyJobDetail.JobDataMap["OutputFolder"] = FileOutputFolder;
                                                }
                                                niFdailyJobDetail.JobDataMap["InsertDatabase"] = PopulateDatabase;
                                                niFdailyJobDetail.JobDataMap["ScheduleType"] = ScheduleType.ToString();
                                                niFdailyJobDetail.JobDataMap["Results"] = results;
                                                niFdailyJobDetail.JobDataMap["ScheduleMessage"] = scheduleMessage;
                                                niFdailyJobDetail.JobDataMap["Duration"] = duration;
                                                niFdailyJobDetail.JobDataMap["Purge"] = Purge;
                                                niFdailyJobDetail.JobDataMap["CreateXML"] = CreateXml;
                                                niFdailyJobDetail.JobDataMap["DateOffset"] = dtOffset;

                                                if (niFdailyJobDetail != null)
                                                {
                                                    // dailyJobDetail.AddJobListener(JobListenerName);

                                                    Logger.Debug("Setting up and starting dailyJobDetail job " + niFdailyJobDetail.Description +
                                                                 " using trigger : " + niFdailyCronTrigger.Description);
                                                    // scheduler.ScheduleJob(dailyJobDetail, dailyCronTrigger[a]);
                                                    scheduler1.ScheduleJob(niFdailyJobDetail, niFdailyCronTrigger);
                                                }

                                            }
                                        break;
                                    #endregion

                                    #region ScheduleType.Weekly
                                    case ScheduleType.Weekly:
                                        if (nodeList != null)
                                            foreach (XmlNode node in nodeList)
                                            {

                                                scheduleId = node.Attributes["Id"].Value;

                                                // Splitting the time into two values so that we can create a cron job
                                                string[] scheduleTimeArray =
                                                    node.Attributes["Time"].Value.Split(new[] { ':' });

                                                // If minutes contains two zeros (00), we change it to one zero (0) 
                                                // If not, scheduler won't understand
                                                if (scheduleTimeArray[1] == "00")
                                                {
                                                    scheduleTimeArray[1] = "0";
                                                }
                                                int minutes = Convert.ToInt32(scheduleTimeArray[1]);

                                                // Doing the same thing for hours
                                                if (scheduleTimeArray[0] == "00")
                                                {
                                                    scheduleTimeArray[0] = "0";
                                                }
                                                int hours = Convert.ToInt32(scheduleTimeArray[0]);


                                                // Checking if there is a setting for results
                                                if (node.Attributes["Results"] != null)
                                                {
                                                    results = Convert.ToBoolean(node.Attributes["Results"].Value);
                                                    scheduleMessage = !results;
                                                }

                                                // Checking if there is a setting for Duration
                                                if (node.Attributes["Duration"] != null)
                                                {
                                                    duration = Convert.ToInt32(node.Attributes["Duration"].Value);
                                                }

                                                string scheduleWeek = "";
                                                if (node.Attributes["Week"] != null)
                                                {
                                                    scheduleWeek =
                                                        node.Attributes["Week"].Value;
                                                }


                                                string scheduleDay = "";
                                                if (node.Attributes["DayOfWeek"] != null)
                                                {
                                                    scheduleDay = node.Attributes["DayOfWeek"].Value;
                                                }


                                                // If we use the DateTimeOffset it means that we have to find out if the day has passed or not.
                                                // So we have to find out which day it is today
                                                DayOfWeek today = DateTime.Today.DayOfWeek;
                                                int numDayOfWeek = 0;
                                                switch (today.ToString())
                                                {
                                                    case "Monday":
                                                        numDayOfWeek = 1;
                                                        break;
                                                    case "Tuesday":
                                                        numDayOfWeek = 2;
                                                        break;

                                                    case "Wednesday":
                                                        numDayOfWeek = 3;
                                                        break;

                                                    case "Thursday":
                                                        numDayOfWeek = 4;
                                                        break;

                                                    case "Friday":
                                                        numDayOfWeek = 5;
                                                        break;

                                                    case "Saturday":
                                                        numDayOfWeek = 6;
                                                        break;

                                                    case "Sunday":
                                                        numDayOfWeek = 0;
                                                        break;


                                                }

                                                // Checking if numDayOfWeek is smaller than ScheduleDay
                                                DateTimeOffset dto = DateTime.Today;

                                                // Get the scheduled day
                                                int scheduledDay = Convert.ToInt32(scheduleDay);

                                                int days;
                                                if (numDayOfWeek <= scheduledDay)
                                                {

                                                    // It is, so we need to find out when the next scheduling should happen
                                                    days = scheduledDay - numDayOfWeek; // this can be zero

                                                    dto = DateTime.Today.AddDays(days).AddHours(hours).AddMinutes(minutes).ToUniversalTime();

                                                    // <Schedule Id="1" Time="15:12" DayOfWeek="1" Week="2"/>

                                                }
                                                else if (numDayOfWeek > scheduledDay)
                                                {

                                                    const int weekdays = 7;
                                                    int daysLeft = weekdays - numDayOfWeek;
                                                    days = daysLeft + scheduledDay;

                                                    dto = DateTime.Today.AddDays(days).AddHours(hours).AddMinutes(minutes).ToUniversalTime();

                                                }

                                                // Creating a simple Scheduler
                                                CalendarIntervalScheduleBuilder calendarIntervalSchedule =
                                                    CalendarIntervalScheduleBuilder.Create();


                                                // Creating the weekly Trigger using the stuff that we've calculated
                                                niFweeklyTrigger = TriggerBuilder.Create()
                                                                                 .WithDescription(InstanceName)
                                                                                 .WithIdentity(InstanceName + "_" + scheduleId, "GroupNIF")
                                                                                 .StartAt(dto)
                                                                                 .WithSchedule(calendarIntervalSchedule.WithIntervalInWeeks(Convert.ToInt32(scheduleWeek)))
                                                                                 .Build();

                                                // This part might be moved
                                                if (OperationMode.Gatherer == OperationMode)
                                                {
                                                    niFweeklyJobDetail = TeamSportJobBuilder.Create<NifJobBuilder>()
                                                                                   .WithIdentity("job_" + InstanceName + "_" + scheduleId, "GroupNIF")
                                                                                   .WithDescription(InstanceName)
                                                                                   .Build();
                                                }
                                                else if (OperationMode == OperationMode.Purge)
                                                {

                                                    niFweeklyJobDetail = TeamSportJobBuilder.Create<PurgeSportsData>()
                                                                                   .WithIdentity("job_" + InstanceName + "_" + scheduleId, "GroupNIF")
                                                                                   .WithDescription(InstanceName)
                                                                                   .Build();

                                                }
                                                else if (OperationMode == OperationMode.Distributor)
                                                {

                                                }
                                                else if (OperationMode == OperationMode.Hold)
                                                {

                                                }

                                                if (FileOutputFolder != null || FileOutputFolder != "")
                                                {
                                                    niFweeklyJobDetail.JobDataMap["OutputFolder"] = FileOutputFolder;
                                                }
                                                niFweeklyJobDetail.JobDataMap["InsertDatabase"] = PopulateDatabase;
                                                niFweeklyJobDetail.JobDataMap["ScheduleType"] = ScheduleType.ToString();
                                                niFweeklyJobDetail.JobDataMap["Results"] = results;
                                                niFweeklyJobDetail.JobDataMap["ScheduleMessage"] = scheduleMessage;
                                                niFweeklyJobDetail.JobDataMap["Duration"] = duration;
                                                niFweeklyJobDetail.JobDataMap["Purge"] = Purge;
                                                niFweeklyJobDetail.JobDataMap["CreateXML"] = CreateXml;
                                            }

                                        if (niFweeklyJobDetail != null)
                                        {
                                            Logger.Debug("Setting up and starting weeklyJobDetail job " + niFweeklyJobDetail.Description +
                                                " using trigger : " + niFweeklyTrigger.Description);
                                            scheduler1.ScheduleJob(niFweeklyJobDetail, niFweeklyTrigger);

                                        }

                                        break;

                                    #endregion

                                    //case ScheduleType.Monthly:
                                    //    // doing Monthly stuff here
                                    //    throw new NotSupportedException("Monthly scheduletype is not implemented!");
                                    //    break;

                                    //case ScheduleType.Yearly:
                                    //    // doing yearly Stuff here
                                    //    throw new NotSupportedException("Yearly scheduletype is not implemented!");
                                    //    break;

                                }

                                // We might move the code regarding jobdetails here... 

                                // Configuring the datafile-object so that we know that the databases are up and running

                                // We don't have to check for datafile stuff when we only do purge...
                                if (OperationMode != OperationMode.Purge)
                                {
                                    NifDatafileCreator datafile = new NifDatafileCreator();
                                    if (datafile.Configure() == false)
                                    {
                                        throw new Exception("Problems initiating database tables");
                                    }
                                }

                                // Catching exceptions that might occur
                            }
                            catch (SchedulerConfigException sce)
                            {
                                Logger.Debug("A SchedulerConfigException has occured: ", sce);
                            }
                            catch (SchedulerException se)
                            {
                                Logger.Debug("A schedulerException has occured: ", se);
                            }
                            catch (Exception e)
                            {
                                Logger.Debug("An exception has occured", e);
                            }

                            break;
                        #endregion

                        #region PollStyle.Scheduled
                        case PollStyle.Scheduled:
                            /* 
                            schedule = configNode.Attributes["Schedule"].Value;
                            TimeSpan s = Utilities.GetScheduleInterval(schedule);
                            logger.DebugFormat("Schedule: {0} Calculated time to next run: {1}", schedule, s);
                            pollTimer.Interval = s.TotalMilliseconds;
                            */
                            throw new NotSupportedException("Invalid polling style for this job type");

                        #endregion

                        #region PollStyle.FileSystemWatch
                        case PollStyle.FileSystemWatch:
                            //Check for config overrides
                            /*
                            if (configNode.Attributes.GetNamedItem("BufferSize") != null)
                                bufferSize = Convert.ToInt32(configNode.Attributes["BufferSize"].Value);
                            logger.DebugFormat("FileSystemWatcher buffer size: {0}", bufferSize);

                            //Set values
                            filesWatcher.Path = fileInputFolder;
                            filesWatcher.Filter = fileFilter;
                            filesWatcher.IncludeSubdirectories = includeSubdirs;
                            filesWatcher.InternalBufferSize = bufferSize;

                            //Do not start the event watcher here, wait until after the startup cleaning job
                            pollTimer.Interval = 5000; // short startup;
                            */
                            throw new NotSupportedException("Invalid polling style for this job type");

                        #endregion



                        default:
                            //Unsupported pollstyle for this object
                            throw new NotSupportedException("Invalid polling style for this job type (" + pollStyle + ")");
                    }
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing PollStyle-specific values in XML configuration node: " + ex.Message, ex);
                }
                #endregion
            }
            //Finish configuration
            ThreadContext.Stacks["NDC"].Pop();
            Configured = true;
        }

        /// <summary>
        /// The start.
        /// </summary>
        /// <exception cref="SportsDataException">
        /// </exception>
        public void Start()
        {
            ServicePointManager.ServerCertificateValidationCallback =
                ValidateServerCertificate;


            if (!Configured)
            {
                throw new SportsDataException("NIFWSComponent is not properly Configured. NIFWSComponent::Start() Aborted!");
            }

            if (!_Enabled)
            {
                throw new SportsDataException("NIFWSComponent is not Enabled. NIFWSComponent::Start() Aborted!");
            }

            // I want the service to populate todays sports on a restart
            NifDatafileCreator dataFileCreator = new NifDatafileCreator
            {
                CreateXml = CreateXml,
                RenderResult = false, // By doing this, we are populating the database
                OutputFolder = FileOutputFolder,
                UpdateDatabase = PopulateDatabase,
                PushResults = PushResults,
                PushSchedule = PushSchedule,
                DateStart = DateTime.Today,
                DateEnd = DateTime.Today
            };

            dataFileCreator.CreateDataFileFromGet();


            try
            {
                if (pollStyle == PollStyle.CalendarPoll)
                {
                    Logger.Info("Setting up jobListener and scheduler");

                    // starting the scheduler
                    var nifScheduler = SchedulerFactory.GetScheduler();

                    // Creating the job listener
                    // new NIFSportsDataJobListener();

                    IList<string> jobGroupNames = nifScheduler.GetJobGroupNames();

                    foreach (string jobGroupName in jobGroupNames)
                    {

                        Logger.Debug("JobGroupName: " + jobGroupName);
                        if (jobGroupName == "GroupNIF")
                        {
                            Logger.Debug("Key: " + nifScheduler.GetJobKeys(GroupMatcher<JobKey>.GroupContains(jobGroupName)));

                            var groupMatcher = GroupMatcher<JobKey>.GroupContains(jobGroupName);
                            var jobKeys = nifScheduler.GetJobKeys(groupMatcher);

                            foreach (JobKey jobKey in jobKeys)
                            {
                                Logger.Debug("Name: " + jobKey.Name);
                                Logger.Debug("Group: " + jobKey.Group);
                            }
                        }

                    }

                    IList<string> triggerGroupNames = nifScheduler.GetTriggerGroupNames();

                    foreach (string triggerGroupName in triggerGroupNames)
                    {
                        Logger.Debug("TriggerGroupName: " + triggerGroupName);
                    }

                    IList<string> listOfJobs = nifScheduler.GetJobGroupNames();
                    foreach (string job in listOfJobs)
                    {
                        Logger.Debug("Job: " + job);

                    }

                    Logger.Debug("This Component name: " + Name);



                    // Starting scheduler
                    nifScheduler.Start();
                }
            }
            catch (SchedulerException se)
            {
                Logger.Debug(se);
            }

            _stopEvent.Reset();
            _busyEvent.Set();

            // Starting poll-timer
            // pollTimer.Interval = 2000;
            pollTimer.Enabled = true;
            pollTimer.Start();
        }

        public void Stop()
        {
            if (!Configured)
            {
                throw new SportsDataException(Name + " is not properly Configured. " + Name + "::Stop() Aborted");
            }

            if (!_Enabled)
            {
                throw new SportsDataException(Name + " is not properly Configured. " + Name + "::Stop() Aborted");
            }


            #region Stopping Scheduler
            // Check status - Handle busy polltimer loops
            if (pollStyle == PollStyle.CalendarPoll)
            {
                try
                {
                    Logger.Debug("Deleting / stopping jobs!");
                    ISchedulerFactory sf = new StdSchedulerFactory();
                    IScheduler sched = sf.GetScheduler();
                    // Shutting down scheduler

                    sched.GetJobGroupNames();

                    List<JobKey> keys = new
                        List<JobKey>(sched.GetJobKeys(GroupMatcher<JobKey>.GroupEquals(InstanceName)));
                    // Looping through each key as we'd like to know when things go right and not!
                    keys.ForEach(key =>
                    {
                        IJobDetail detail = sched.GetJobDetail(key);
                        sched.DeleteJob(key);

                        Logger.Debug("Shutting down: " + detail.Description + "(" + key.Name + ")" + " Group: " + key.Group);
                    });

                    sched.Shutdown();

                }
                catch (SchedulerException sex)
                {
                    Logger.Fatal(sex);
                }

                catch (Exception ex)
                {
                    Logger.Error("Problems closing NIF WebClients!", ex);

                }
            }
            #endregion
        }

        public ComponentState ComponentState { get; set; }

        private void pollTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            /*
             * We can consider adding getting data here if the time (day + hour + minute) hits. Otherwize we shall do nothing.
             * 
             */
            ThreadContext.Properties["JOBNAME"] = InstanceName;

            Logger.Debug("NIFGathererComponent::pollTimer_Elapsed() hit");
            Logger.Debug("We have the following settings: Interval: " + pollTimer.Interval.ToString());

            // _busyEvent.WaitOne();
            pollTimer.Stop();


            switch (pollStyle)
            {
                //For continuous and scheduled do a simple folder item traversal
                case PollStyle.Scheduled:
                case PollStyle.Continous:
                    if (OperationMode == OperationMode.Distributor)
                    {
                        // foo();
                    }

                    if (OperationMode == OperationMode.Gatherer)
                    {
                        // We shall then do stuff
                        // A continous mode must be about results, anything else would be just dumb
                        NifDatafileCreator dataFileCreator = new NifDatafileCreator
                        {
                            CreateXml = CreateXml,
                            RenderResult = true,
                            OutputFolder = FileOutputFolder,
                            UpdateDatabase = PopulateDatabase,
                            PushResults = PushResults,
                            PushSchedule = PushSchedule
                        };

                        dataFileCreator.CreateDataFileFromGet();
                    }
                    pollTimer.Start();

                    break;

                case PollStyle.CalendarPoll:

                    // @todo: need to do some work here
                    Logger.Debug("Disabling pollTimer");
                    pollTimer.Enabled = false;
                    // _busyEvent.WaitOne();
                    break;

                case PollStyle.PushSubscribe:

                    // Subscriber key =             sub-4fb246b0-982a-11e1-ad0b-e19db246ca40
                    // Staging channel =          THE_FIKS_STAGING_CHANNEL

                    // We need to check the service state


                    pollTimer.Start();
                    break;

                /*
                 * M� legge inn i databasen om denne turneringen er push eller pull enablet.
                 * Sjekke om vi skal bruke den andre l�sningen for push...
                 * Anders g�r over om dagens nye l�sning er 1:1 gammel l�sning.
                 */


            }
        }


        public bool DatabasePopulated { get; set; }
        
    }
}
