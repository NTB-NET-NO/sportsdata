using System;
using Quartz;
using log4net;

// Using Quartz.net

namespace NTB.SportsData.Service.Components.Nif
{
    class NifSportsDataJobListener : IJobListener
    {
        static readonly ILog Logger = LogManager.GetLogger(typeof(NifSportsDataJobListener));

        protected Boolean JobBeingExecuted = false;
        private string _name = "";

        static NifSportsDataJobListener()
        {
            //Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
                log4net.Config.BasicConfigurator.Configure();

            Logger.Debug("SportsDataJobListener is created");
            
        }

        

        

        //public void JobToBeExecuted(JobExecutionContext context)
        //{
        //    logger.Debug("Setting jobBeingExecuted to true!");
        //    jobBeingExecuted = true;

        //    // IScheduler sched = context.Scheduler;

        //}

        
        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }

       

        public void JobToBeExecuted(IJobExecutionContext context)
        {
            Logger.Debug("Setting jobBeingExecuted to true!");
            JobBeingExecuted = true;


            // throw new NotImplementedException();
        }

        public void JobWasExecuted(IJobExecutionContext context, JobExecutionException jobException)
        {
            Logger.Debug("Setting jobBeingExecuted to false!");
            JobBeingExecuted = false;


            // throw new NotImplementedException();
        }

        public void JobExecutionVetoed(IJobExecutionContext context)
        {
            throw new NotImplementedException();
        }
    }
}
