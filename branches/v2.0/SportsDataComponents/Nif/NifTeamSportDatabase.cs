using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using NTB.SportsData.Service.Domain.Classes;
using log4net;

namespace NTB.SportsData.Service.Components.Nif
{
    public class NifTeamSportDatabase
    {
        // Database variables
        private SqlDataReader _sqlreader;
        private SqlConnection _mySqlConnection;


        static readonly ILog Logger = LogManager.GetLogger(typeof(NifTeamSportDatabase));

        private const Boolean Final = false;

        public bool SetDownload(int eventId)
        {
            using (_mySqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
            {
                try
                {
                    if (_mySqlConnection.State != ConnectionState.Open)
                    {
                        _mySqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        _mySqlConnection.Open();
                    }
                    SqlCommand command = new SqlCommand
                    {
                        Connection = _mySqlConnection,
                        CommandText = "Service_SetDownload"
                    };

                    command.Parameters.Add(
                             new SqlParameter("@EventId", eventId));

                    // Telling the system that this is a StoredProcedure
                    command.CommandType = CommandType.StoredProcedure;



                    if (command.ExecuteNonQuery() == 1)
                    {
                        if (_mySqlConnection.State != ConnectionState.Closed)
                        {
                            _mySqlConnection.Close();
                        }
                        return true;
                    }
                    if (_mySqlConnection.State != ConnectionState.Closed)
                    {
                        _mySqlConnection.Close();
                    }
                    return false;
                }
                catch (SqlException exception)
                {
                    Logger.Error(exception);
                    return false;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return false;
                }
            }

        }

        internal System.Collections.Generic.List<TeamSportEvent> GetMatches()
        {
            throw new NotImplementedException();
        }
    }
}
