using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using NTB.SportsData.Service.Domain.Classes;
using log4net;

// Adding log4net support
// Sportsdata projects


namespace NTB.SportsData.Service.Components.Nif
{
    public class NifDatabase
    {
        // Database variables
        private SqlDataReader _sqlreader;
        private SqlConnection _mySqlConnection;

        static readonly ILog Logger = LogManager.GetLogger(typeof(NifDatafileCreator));

        private const Boolean Final = false;

        // Can I have getters and setters here?

        public bool FinalResult
        {
            get
            {
                return Final;
            }
        }



        public NifDatabase()
        {

            string connectionString = ConfigurationManager.AppSettings["ConnectionString"];
            _mySqlConnection = new SqlConnection(connectionString);

            if (_mySqlConnection.State != ConnectionState.Open)
            {
                // Open the connection to the database
                _mySqlConnection.Open();
            }


        }

        public void Close()
        {
            if (_mySqlConnection.State == ConnectionState.Open)
            {

                _mySqlConnection.Close();
            }
        }

        #region getters and setters
        

        #endregion getters and setters


        /// <summary>
        /// This method initiates the object. We are here checking if District-table and Tournament-Table are populated
        /// </summary>
        public void Init()
        {

        }

        public int GetTournamentByMatchId(int matchId)
        {
            int tournamentId = 0;
            SqlCommand myCommand = Command("Service_GetTournamentIdByMatchId");
            myCommand.CommandType = CommandType.StoredProcedure;

            myCommand.Parameters.Add(
                new SqlParameter("@MatchId", matchId));

            SqlDataReader reader = myCommand.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    tournamentId = Convert.ToInt32(reader["TournamentId"].ToString());
                }
            }

            return tournamentId;
        }

        
        public void AddTournamentData()
        {
        }

        public void UpdateTournamentData()
        {
        }



        #region Weekly
        //public int deleteData()
        //{
        //    try
        //    {
        //        // xTODO: Fix Document-table so that we have a Downloaded flag there to
        //        string SQLQuery = "DELETE FROM Matches WHERE Downloaded=1; DELETE FROM Document";

        //        SqlCommand commander = Command(SQLQuery);


        //        SqlDataReader sqlreader = commander.ExecuteReader();

        //        return sqlreader.RecordsAffected;

        //    }
        //    catch (SqlException sqlexception)
        //    {
        //        logger.Error(sqlexception);
        //        return 0;
        //    }
        //    catch (Exception exception)
        //    {
        //        logger.Error(exception);
        //        return 0;
        //    }
        //    finally
        //    {


        //    }
        //}



        #endregion

        #region Daily
        public int DeleteDocuments()
        {
            try
            {
                // We are cleaning up document table so that we are ready to create new documents for today
                const string sqlQuery = "DELETE FROM Document";

                SqlCommand commander = Command(sqlQuery);

                SqlDataReader sqlreader = commander.ExecuteReader();

                return sqlreader.RecordsAffected;
                // sqlconnection.CreateCommand
            }
            catch (SqlException sqlexception)
            {
                Logger.Error(sqlexception);
                return 0;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                return 0;
            }
        }
        #endregion




        public SqlCommand Command(string sqlQuery)
        {
            SqlCommand command = new SqlCommand {Connection = _mySqlConnection, CommandText = sqlQuery};

            // sqlreader = command.ExecuteReader();

            return command;
        }


        



        public string GetOneResultFile()
        {
            const string strSql = "SELECT TOP(1) DocumentData FROM Document;";

            _mySqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]);
            if (_mySqlConnection.State != ConnectionState.Open)
            {
                Logger.Debug("Start connecting!");

                _mySqlConnection.Open();

                Logger.Debug("End connecting!");

                // Now performing the Query


                SqlCommand command = new SqlCommand {Connection = _mySqlConnection, CommandText = strSql};

                _sqlreader = command.ExecuteReader();
                string documentData = "";
                if (_sqlreader.HasRows)
                {
                    // sqlreader["HomeTeam"].ToString();
                    try
                    {
                        while (_sqlreader.Read())
                        {
                            documentData = _sqlreader["DocumentData"].ToString();
                        }
                    }
                    catch (SqlException sqlexception)
                    {
                        Logger.Error(sqlexception);
                    }

                    catch (Exception exception)
                    {
                        Logger.Error(exception);
                    }
                }

                _mySqlConnection.Close();

                return documentData;

            }
            return "";
        }


        /// <summary>
        /// This method shall be renamed to something else. It shall also return a list of customers
        /// </summary>
        /// <param name="matchId"></param>
        /// <param name="sportId"></param>
        /// <returns></returns>
        public List<Customer> GetListOfCustomers(int matchId, int sportId)
        {

            using (SqlConnection myConnection =
                new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
            {
                SqlCommand myCommand = new SqlCommand("ServiceGetJobIdAndTournamentIdByMatchId", myConnection);

                myCommand.Parameters.Add(
                    new SqlParameter("@MatchId", matchId));

                myCommand.Parameters.Add(
                    new SqlParameter("@SportId", sportId));


                myCommand.CommandType = CommandType.StoredProcedure;
                if (myConnection.State != ConnectionState.Open)
                {
                    myConnection.Open();
                }

                SqlDataReader mySqlDataReader = myCommand.ExecuteReader();

                // xTODO: Continue here
                /**
                 * What we are to do: 
                 * First we get the TournamentId and JobId. We then use this to find
                 * the customers that shall have these data.
                 * We are to return a list of customernames
                 * 
                 */
                List<Customer> myCustomers = new List<Customer>();
                if (mySqlDataReader.HasRows)
                {

                    while (mySqlDataReader.Read())
                    {
                        Customer myCustomer = new Customer {Name = "Norsk Telegrambyrå"};

                        myCustomers.Add(myCustomer);
                    }

                    return myCustomers;
                }
                
                // logger.Debug("No customers needs these data");
                // TODO: Change this to return null-list
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["livetesting"]))
                {
                    Customer myCustomer = new Customer();
                    myCustomer.Name = "Norsk Telegrambyrå";
                    myCustomers.Add(myCustomer);
                    return myCustomers;
                }

                myConnection.Close();

                // TODO: Change this to return the Customer-list
                return myCustomers;
            }

        }

        /// <summary>
        /// Method to find out if the matches table is empty or not. 
        /// Returns the number of matches we have stored in the database
        /// </summary>
        /// <returns>Integer</returns>
        public int CheckMatchesTable()
        {
            
            try {
            using (SqlConnection myConnection =
                new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
            {
                SqlCommand myCommand = new SqlCommand("Service_GetNumberOfMatches", myConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                if (myConnection.State != ConnectionState.Open)
                {
                    myConnection.Open();
                }

                SqlDataReader mySqlDataReader = myCommand.ExecuteReader();

                int numberOfMatches = 0;
                if (mySqlDataReader.HasRows)
                {
                    while (mySqlDataReader.Read())
                    {
                        numberOfMatches = Convert.ToInt32(mySqlDataReader["NumberOfMatches"].ToString());
                    }

                    return numberOfMatches;
                }

                myConnection.Close();
                myConnection.Dispose();
                return 0;
            }
            } catch (SqlException sqlexception) {
                Logger.Debug(sqlexception);
                Logger.Debug(sqlexception);
                return 0;
            } catch (Exception exception) {
                Logger.Debug(exception);
                Logger.Debug(exception);
                return 0;
            }

        }

        


        public bool SetDownload(int eventId)
        {
            using (
                SqlConnection myConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
            {
                try
                {

                    SqlCommand command = new SqlCommand {Connection = myConnection, CommandText = "Service_SetDownload"};

                    // var sqlFormattedDate = myDateTime.Date.ToString("yyyy-MM-dd HH:mm:ss");
                    // new DateTime();

                    command.Parameters.Add(
                             new SqlParameter("@EventId", eventId));

                    // Telling the system that this is a StoredProcedure
                    command.CommandType = CommandType.StoredProcedure;

                    if (myConnection.State != ConnectionState.Open)
                    {
                        myConnection.Open();
                    }

                    if (command.ExecuteNonQuery() == 1)
                    {
                        if (myConnection.State != ConnectionState.Closed) {
                            myConnection.Close();
                        }
                        return true;
                    }
                    if (myConnection.State != ConnectionState.Closed) {
                        myConnection.Close();
                    }
                    return false;
                }
                catch (SqlException exception)
                {
                    Logger.Error(exception);
                    return false;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return false;
                }
            }

        }
        /// <summary>
        /// Get events from today from the database
        /// </summary>
        public List<SportEvent> GetEvents()
        {

            using (
                SqlConnection myConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
            {
                try
                {

                    SqlCommand command = new SqlCommand {Connection = myConnection, CommandText = "Service_GetEvents"};

                    // var sqlFormattedDate = myDateTime.Date.ToString("yyyy-MM-dd HH:mm:ss");
                    // DateTime myDateTime = new DateTime();

                    command.Parameters.Add("@Today", SqlDbType.DateTime);
                    command.Parameters["@Today"].Value = DateTime.Today;

                    // Telling the system that this is a StoredProcedure
                    command.CommandType = CommandType.StoredProcedure;

                    if (myConnection.State != ConnectionState.Open)
                    {
                        myConnection.Open();
                    }

                    // Creating the SQL-reader.
                    _sqlreader = command.ExecuteReader();

                    List<SportEvent> myEvents = new List<SportEvent>();
                    if (_sqlreader.HasRows)
                    {
                        while (_sqlreader.Read())
                        {
                            SportEvent myEvent = new SportEvent
                                {
                                    EventName = _sqlreader["Name"].ToString(),
                                    EventId = Convert.ToInt32(_sqlreader["EventId"].ToString()),
                                    EventDateStart = Convert.ToDateTime(_sqlreader["DateStart"].ToString()),
                                    EventDateEnd = Convert.ToDateTime(_sqlreader["DateEnd"].ToString()),
                                    EventLocation = _sqlreader["Location"].ToString(),
                                    EventTimeStart = _sqlreader["TimeStart"] == DBNull.Value
                                                         ? 0
                                                         : Convert.ToInt32(_sqlreader["TimeStart"].ToString()),
                                    EventTimeEnd = _sqlreader["TimeEnd"] == DBNull.Value
                                                       ? 0
                                                       : Convert.ToInt32(_sqlreader["TimeEnd"].ToString()),
                                    ActivityId = Convert.ToInt32(_sqlreader["ActivityId"].ToString()),
                                    EventOrganizerId = Convert.ToInt32(_sqlreader["OrganizerId"].ToString())
                                };

                            // Field2 = rdr.GetSqlInt32(Field2_Ordinal).ToNullableInt32()

                            myEvents.Add(myEvent);

                        }


                    }
                    if (myConnection.State != ConnectionState.Closed)
                    {
                        myConnection.Close();
                    }
                    return myEvents;

                }
                catch (SqlException sqlexception)
                {
                    Logger.Error(sqlexception);
                    return null;
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                    return null;
                }
            }
        }

        /// <summary>
        /// Inserts or updates events in the database
        /// </summary>
        /// <param name="myEvents">Holds a list of events that we are to insert or update</param>
        public void InsertEvent(List<SportEvent> myEvents)
        {
            Logger.Debug("Number of items: " + myEvents.Count);
            foreach (SportEvent myEvent in myEvents)
            {
                Logger.Debug("Checking if " + myEvent.EventName + " is in the database and if not, we are to insert");
                // First we have to check if the event is in the database already.
                // If it is, we need to check if some of the values that we have has been changed
                // If some data has been changed, we need to change them in our database
                // So we have to either update or insert

                // Can we do this in StoredProcedures? (The whole thing in one go...)

                // We don't do it in one go. What we do is get the data from the database, then check against the object
                using (
                SqlConnection myConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    try
                    {

                        SqlCommand command = new SqlCommand
                            {
                                Connection = myConnection,
                                CommandText = "Service_CheckEvents"
                            };

                        command.Parameters.Add(
                             new SqlParameter("@EventId", myEvent.EventId));
                        // Telling the system that this is a StoredProcedure
                        command.CommandType = CommandType.StoredProcedure;

                        if (myConnection.State != ConnectionState.Open)
                        {
                            myConnection.Open();
                        }

                        // Creating the SQL-reader.
                        _sqlreader = command.ExecuteReader();

                        if (_sqlreader.HasRows)
                        {
                            bool updateEvent = false;
                            // We are now checking what we have gotten and if there are changes.
                            // If there are changes, we shall update.
                            while (_sqlreader.Read())
                            {
                                if (myEvent.EventName != _sqlreader["EventName"].ToString())
                                {
                                    updateEvent = true;
                                }

                                if (myEvent.EventDateStart != Convert.ToDateTime(_sqlreader["DateStart"].ToString()))
                                {
                                    updateEvent = true;
                                }

                                if (myEvent.EventDateEnd != Convert.ToDateTime(_sqlreader["DateEnd"].ToString()))
                                {
                                    updateEvent = true;
                                }

                                if (myEvent.EventLocation != _sqlreader["Location"].ToString())
                                {
                                    updateEvent = true;
                                }

                                if (myEvent.EventTimeStart != null)
                                {
                                    if (myEvent.EventTimeStart != Convert.ToInt32(_sqlreader["TimeStart"].ToString()))
                                    {
                                        updateEvent = true;
                                    }
                                }

                                if (myEvent.EventTimeEnd != null)
                                {
                                    if (myEvent.EventTimeEnd != Convert.ToInt32(_sqlreader["TimeEnd"].ToString()))
                                    {
                                        updateEvent = true;
                                    }
                                }

                                if (myEvent.EventOrganizerId != Convert.ToInt32(_sqlreader["OrganizerId"].ToString()))
                                {
                                    updateEvent = true;
                                }

                                if (myEvent.ActivityId != Convert.ToInt32(_sqlreader["ActivityId"].ToString()))
                                {
                                    updateEvent = true;
                                }

                                if (updateEvent)
                                {
                                    if (myEvent.EventDateEnd == null)
                                    {
                                        myEvent.EventDateEnd = myEvent.EventDateStart;
                                    }
                                    using (SqlConnection myUpdateConnection =
                                        new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                                    {
                                        Logger.Debug("We are updating!");
                                        // We shall update the event
                                        SqlCommand myUpdateCommand = new SqlCommand
                                            {
                                                Connection = myUpdateConnection,
                                                CommandText = "Service_UpdateEvent"
                                            };
                                        myUpdateCommand.Parameters.Add(
                                             new SqlParameter("@EventId", myEvent.EventId));
                                        myUpdateCommand.Parameters.Add(
                                             new SqlParameter("@ActivityId", myEvent.ActivityId));
                                        myUpdateCommand.Parameters.Add(
                                             new SqlParameter("@OrganizerId", myEvent.EventOrganizerId));
                                        myUpdateCommand.Parameters.Add(
                                             new SqlParameter("@EventName", myEvent.EventName));
                                        myUpdateCommand.Parameters.Add(
                                             new SqlParameter("@Location", myEvent.EventLocation));
                                        myUpdateCommand.Parameters.Add(
                                             new SqlParameter("@DateStart", myEvent.EventDateStart));
                                        myUpdateCommand.Parameters.Add(
                                             new SqlParameter("@DateEnd", myEvent.EventDateEnd));
                                        myUpdateCommand.Parameters.Add(
                                             new SqlParameter("@TimeStart", myEvent.EventTimeStart));
                                        myUpdateCommand.Parameters.Add(
                                             new SqlParameter("@TimeEnd", myEvent.EventTimeEnd));

                                        // Telling the system that this is a StoredProcedure
                                        myUpdateCommand.CommandType = CommandType.StoredProcedure;

                                        if (myUpdateConnection.State != ConnectionState.Open)
                                        {
                                            myUpdateConnection.Open();
                                        }

                                        myUpdateCommand.ExecuteNonQuery();

                                        if (myUpdateConnection.State != ConnectionState.Closed) {
                                            myUpdateConnection.Close();
                                        }
                                    }
                                }
                            }



                        }
                        else
                        {
                            // We have not found a row and so we shall insert
                            if (myEvent.EventDateEnd == null)
                            {
                                myEvent.EventDateEnd = myEvent.EventDateStart;
                            }

                            using (SqlConnection myUpdateConnection =
                                        new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                            {
                                Logger.Debug("We are Inserting!");
                                // We shall update the event
                                SqlCommand myUpdateCommand = new SqlCommand
                                    {
                                        Connection = myUpdateConnection,
                                        CommandText = "Service_InsertEvent"
                                    };
                                myUpdateCommand.Parameters.Add(
                                    new SqlParameter("@EventId", myEvent.EventId));
                                myUpdateCommand.Parameters.Add(
                                     new SqlParameter("@ActivityId", myEvent.ActivityId));
                                myUpdateCommand.Parameters.Add(
                                     new SqlParameter("@OrganizerId", myEvent.EventOrganizerId));
                                myUpdateCommand.Parameters.Add(
                                     new SqlParameter("@EventName", myEvent.EventName));
                                myUpdateCommand.Parameters.Add(
                                     new SqlParameter("@Location", myEvent.EventLocation));
                                myUpdateCommand.Parameters.Add(
                                     new SqlParameter("@DateStart", myEvent.EventDateStart));
                                myUpdateCommand.Parameters.Add(
                                     new SqlParameter("@DateEnd", myEvent.EventDateEnd));
                                if (myEvent.EventTimeStart != null)
                                {
                                    myUpdateCommand.Parameters.Add(
                                         new SqlParameter("@TimeStart", myEvent.EventTimeStart));
                                }

                                if (myEvent.EventTimeEnd != null)
                                {
                                    myUpdateCommand.Parameters.Add(
                                         new SqlParameter("@TimeEnd", myEvent.EventTimeEnd));
                                }
                                // Telling the system that this is a StoredProcedure
                                myUpdateCommand.CommandType = CommandType.StoredProcedure;

                                if (myUpdateConnection.State != ConnectionState.Open)
                                {
                                    myUpdateConnection.Open();
                                }

                                myUpdateCommand.ExecuteNonQuery();

                                if (myUpdateConnection.State != ConnectionState.Closed)
                                {
                                    myUpdateConnection.Close();
                                }
                            }
                        }

                        if (myConnection.State != ConnectionState.Closed)
                        {
                            myConnection.Close();
                        }

                    }
                    catch (SqlException sqlexception)
                    {
                        Logger.Error(sqlexception);
                    }
                    catch (Exception exception)
                    {
                        Logger.Error(exception);
                    }

                    if (myConnection.State != ConnectionState.Closed)
                    {
                        myConnection.Close();
                    }

                    // return null;
                }


            }
        }
    }
}
