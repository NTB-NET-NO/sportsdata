// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NIFJobBuilder.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Configuration;
using log4net;
using Quartz;

namespace NTB.SportsData.Service.Components.Nif
{
    // Adding support for log4net
        // Adding support for Quartz Scheduler

    /// <summary>
    /// The nif job builder.
    /// </summary>
    internal class NifJobBuilder : IJob
    {
        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(NifJobBuilder));

        /// <summary>
        /// Initializes static members of the <see cref="NifJobBuilder"/> class.
        /// </summary>
        static NifJobBuilder()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        // This class shall be used to get the matches from Norwegian Soccer Federation

        /// <summary>
        /// The execute.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public void Execute(IJobExecutionContext context)
        {
            Logger.Info("Executing JobExecutionContext: " + context.JobDetail.Description);
            string outputFolder = string.Empty;

            bool createXml = false;
            bool insertDatabase = false;
            bool results = false;
            DateTime dateOffset = DateTime.Now;
            int duration = 0;
            int sportId = 0;
            int federationId = 0;

            // Mapping information sent from Component
            JobDataMap dataMap = context.JobDetail.JobDataMap;

            // Populate variables to be used to 
            try
            {
                outputFolder = dataMap.GetString("OutputFolder");
                Logger.Debug("OutputFolder: " + outputFolder);

                insertDatabase = dataMap.GetBoolean("InsertDatabase");
                Logger.Debug(insertDatabase ? "InsertDatabase: true" : "InsertDatabase: false");

                string scheduleType = dataMap.GetString("ScheduleType");
                Logger.Debug("ScheduleType: " + scheduleType);

                results = dataMap.GetBoolean("Results");
                Logger.Debug(results ? "Results: true" : "Results: false");

                bool scheduleMessage = dataMap.GetBoolean("ScheduleMessage");
                Logger.Debug(scheduleMessage ? "ScheduleMessage: true" : "ScheduleMessage: false");

                duration = dataMap.GetInt("Duration");
                Logger.Debug("Duration: " + duration);

                createXml = dataMap.GetBoolean("CreateXML");
                Logger.Debug(createXml ? "CreateXML: true" : "CreateXML: false");

                sportId = dataMap.GetInt("SportId");

                federationId = dataMap.GetInt("FederationId");

                if (dataMap.Keys.Contains("DateOffset"))
                {
                    dateOffset = dataMap.GetDateTime("DateOffset");
                    Logger.Debug("DateOffset: " + dateOffset.ToShortDateString());
                }
            }
            catch (Exception exception)
            {
                Logger.Debug(exception);
                Logger.Debug(exception.StackTrace);
            }

            // Creating the DataFileCreator object
            NifDatafileCreator nifDatafileCreator = new NifDatafileCreator
                    {
                        OutputFolder = outputFolder,
                        SportId = sportId,
                        FederationId = federationId
                    };

            Logger.Debug("Outputfolder: " + outputFolder);

            if (insertDatabase)
            {
                Logger.Debug("We shall insert data in the database");
                nifDatafileCreator.UpdateDatabase = true;
            }

            // This parameter tells us if we are to add results-tags to the XML
            if (results)
            {
                Logger.Debug("We shall also render result");
                nifDatafileCreator.RenderResult = true;
            }

            Logger.Debug("Duration: " + duration);
            if (duration > 0)
            {
                Logger.Debug("We shall create a longer period of matches");
                nifDatafileCreator.Duration = duration;

                // Creating the date start
                nifDatafileCreator.DateStart = DateTime.Today;

                if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                {
                    nifDatafileCreator.DateStart = DateTime.Parse("2011-05-16");
                }

                nifDatafileCreator.DateEnd = nifDatafileCreator.DateStart.AddDays(duration);
            }
            else if (duration < 0)
            {
                DateTime datestart = DateTime.Now.AddDays(duration);

                nifDatafileCreator.DateStart = datestart;

                nifDatafileCreator.DateEnd = DateTime.Today;
            }
            else
            {
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                {
                    nifDatafileCreator.DateStart = DateTime.Parse("2011-05-16");
                    nifDatafileCreator.DateEnd = DateTime.Parse("2011-05-16");
                }
                else
                {
                    nifDatafileCreator.DateStart = DateTime.Today;
                    nifDatafileCreator.DateEnd = DateTime.Today;

                    if (dateOffset != DateTime.Today)
                    {
                        nifDatafileCreator.DateStart = dateOffset;
                        nifDatafileCreator.DateEnd = dateOffset;
                    }

                    Logger.Debug("Date Start: " + nifDatafileCreator.DateStart.ToString());
                    Logger.Debug("Date End: " + nifDatafileCreator.DateEnd.ToString());
                }
            }

            Logger.Debug("We are about to create XML File");
            nifDatafileCreator.CreateXml = createXml;

            nifDatafileCreator.CreateDataFileFromGet();
        }
    }
}