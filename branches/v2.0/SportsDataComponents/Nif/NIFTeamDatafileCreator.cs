// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NIFTeamDatafileCreator.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using NTB.SportsData.Service.Domain.Classes;
using log4net;

namespace NTB.SportsData.Service.Components.Nif
{
    using NTB.SportsData.Service.Components.NIFProdService;

    // Adding support for log4net
        // Adding additional NTB SportsData 
    
    /// <summary>
    /// The nif team datafile creator.
    /// </summary>
    public class NifTeamDatafileCreator
    {
        
        /// <summary>
        /// The tournament id.
        /// </summary>
        private const int TournamentId = 0;

        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(NifDatafileCreator));



        /// <summary>
        /// The _date duration.
        /// </summary>
        private int _dateDuration;

        /// <summary>
        /// The match id.
        /// </summary>
        private int MatchId = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="NifTeamDatafileCreator"/> class. 
        /// Constructor for the NIFDatafileCreator object. This is used to create Structures
        /// </summary>
        public NifTeamDatafileCreator()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        #region getters and setters

        /// <summary>
        /// Gets or sets the output folder.
        /// </summary>
        public string OutputFolder { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether update database.
        /// </summary>
        public bool UpdateDatabase { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether create xml.
        /// </summary>
        public bool CreateXml { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether render result.
        /// </summary>
        public bool RenderResult { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether schedule message.
        /// </summary>
        public bool ScheduleMessage { get; set; }

        /// <summary>
        /// Gets or sets the date start.
        /// </summary>
        public DateTime DateStart { get; set; }

        /// <summary>
        /// Gets or sets the date end.
        /// </summary>
        public DateTime DateEnd { get; set; }

        /// <summary>
        /// Gets or sets the customers.
        /// </summary>
        public List<Customer> Customers { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether push results.
        /// </summary>
        public bool PushResults { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether push schedule.
        /// </summary>
        public bool PushSchedule { get; set; }

        /// <summary>
        /// Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        /// Gets or sets the federationd id.
        /// </summary>
        public int FederationdId { get; set; }

        /// <summary>
        /// Gets or sets the duration.
        /// </summary>
        public int Duration
        {
            get
            {
                return _dateDuration;
            }

            set
            {
                if (value > 0)
                {
                    _dateDuration = value;
                }
            }
        }

        #endregion getters and setters

        #region Configuration

        /// <summary>
        /// This method is used to configure the datafile object. It shall check if we have rows in Tournament and Districts Databases
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Configure()
        {
            try
            {
                Logger.Debug("Configuring DatafileCreator");

                return true;
            }
            catch (Exception e)
            {
                Logger.Error("An exception has occured: " + e);
                return false;
            }
        }

        #endregion Configuration

        #region Create Data File From Push Or Get

        /// <summary>
        /// This method is used to check which events are in the database that we can query from the NIF Services
        /// </summary>
        private void _CheckTournamentAgainstDatabase()
        {
            NifTeamSportDatabase teamSportDatabase = new NifTeamSportDatabase();

            // List<TeamSportEvent> listOfSportsEvents = teamSportDatabase.GetMatches();
            // if (listOfSportsEvents != null)
            // {
            // Logger.Info("We have " + listOfSportsEvents.Count() + " items to check");

            // foreach (TeamSportEvent myDatabaseEvent in listOfSportsEvents)
            // {
            // Logger.Info("Checking " + myDatabaseEvent.EventName + " (" + myDatabaseEvent.EventId + ")");
            // EventRequest eventRequest = new EventRequest
            // {
            // User = _inn.User,
            // FunctionalityId = _inn.FunctionalityId,
            // Id = myDatabaseEvent.EventId
            // };

            // EventCompleteResponse eventResponse = EventClient.GetSingleEventComplete(eventRequest);

            // if (eventResponse.Success)
            // {
            // EventComplete nifEvent = eventResponse.Event;
            // XmlDocument doc;

            // SportEvent sportEvent = new SportEvent
            // {
            // EventId = nifEvent.Event.EventId,
            // EventName = nifEvent.Event.EventName,
            // EventLocation = nifEvent.Event.Location,
            // EventDateStart = nifEvent.Event.StartDate,
            // EventDateEnd = nifEvent.Event.EndDate,
            // EventOrganizerId = nifEvent.Event.ArrangingOrgId,
            // AdministrativeOrgId = nifEvent.Event.AdministrativeOrgId,
            // AdministrativeOrgName = nifEvent.Event.AdministrativeOrgName,
            // EventOrganizationName = nifEvent.Event.ArrangingOrgName,
            // ActivityId = nifEvent.Event.ActivityId,
            // ActivityName = nifEvent.Event.ActivityName
            // };

            // // We also need to find out which federation is involved
            // ActivityRequest activityRequest = new ActivityRequest
            // {
            // FunctionalityId = _inn.FunctionalityId,
            // User = _inn.User
            // };

            // ActivityResponse activityResponse = ActivityServiceV2Client.GetAllFirstLevelActivities(activityRequest); // ActivityClient.GetAllFirstLevelActivities(activityRequest);
            // if (activityResponse.Success)
            // {
            // Activity[] activities = activityResponse.Activities;

            // List<Activity> queryActivity = new List<Activity>(
            // from ac in activities
            // where nifEvent.Event.ParentActivityId != null && ac.ActivityId == nifEvent.Event.ParentActivityId.Value
            // select ac);

            // foreach (Activity activity in queryActivity)
            // {
            // sportEvent.ParentActivity = activity.ActivityName;
            // sportEvent.ParentActivityId = activity.ActivityId;
            // }

            // }

            // OrgSearchRequest orgSearchRequest = new OrgSearchRequest
            // {
            // FunctionalityId = _inn.FunctionalityId,
            // User = _inn.User
            // };

            // OrgCriteria orgCriteria = new OrgCriteria
            // {
            // ActivityId = Convert.ToInt32(nifEvent.Event.ActivityId),
            // MaxRowCount = 10,
            // SortOrder = "Ascending"
            // };

            // orgSearchRequest.OrgCriteria = orgCriteria;

            // OrgSearchResponse orgSearchResponse = OrgClient.SearchOrg(orgSearchRequest);

            // if (orgSearchResponse.Success)
            // {
            // OrgSearchResult[] results = orgSearchResponse.OrgSearchResult;

            // foreach (OrgSearchResult orgSearchResult in results)
            // {
            // sportEvent.AdministrativeOrgId = orgSearchResult.Id;
            // sportEvent.AdministrativeOrgName = orgSearchResult.Name;
            // sportEvent.EventLocation = orgSearchResult.LocalCouncilName;
            // }
            // }

            // if (RenderResult)
            // {

            // // if (NIFEvent.Event.
            // // Then we need to get the results for this event
            // ResultByEventIdRequest resultRequest = new ResultByEventIdRequest
            // {
            // User = _inn.User,
            // FunctionalityId = _inn.FunctionalityId,
            // EventId = myDatabaseEvent.EventId
            // };

            // // ResultsResponse response = ResultTeamClient. // GetResultsByEventIdOrClassExerciseId(resultRequest);

            // // Checking if we really got something out of this
            // if (response.Success)
            // {

            // List<Result> results = new List<Result>(response.Results);

            // if (results.Count > 0)
            // {
            // // This one gives an error on grouping
            // var resultQuery = from myResults in results
            // where myResults.Rank > 0
            // orderby myResults.Rank
            // group myResults by myResults.ClassExerciseName
            // into resultsGroup
            // orderby resultsGroup.Key
            // select resultsGroup;

            // // Looping list
            // List<SportResult> listOfSportsResults = new List<SportResult>();

            // foreach (var nameGroup in resultQuery)
            // {
            // //txtResults.Text += nameGroup.Key + "\r\n";
            // foreach (Result result in nameGroup.Distinct())
            // {
            // PersonRequest1 personRequest = new PersonRequest1
            // {
            // FunctionalityId = _inn.FunctionalityId,
            // User = _inn.User,
            // Id = result.PersonId
            // };

            // PersonResponse1 personResponse1 = PersonClient.GetPerson(personRequest);

            // string city = "";
            // string zipcode = "";
            // if (personResponse1.Success)
            // {
            // Person1 person = personResponse1.Person;

            // city = person.HomeAddress.City;
            // zipcode = person.HomeAddress.ZipCode;
            // }
            // int hour = 0;
            // int minute = 0;
            // int second = 0;

            // if (result.Time != null)
            // {
            // hour = result.Time.Value.Hour;
            // minute = result.Time.Value.Minute;
            // second = result.Time.Value.Second;
            // }

            // int hourbehind = 0;
            // int minutebehind = 0;
            // int secondbehind = 0;
            // if (result.TimeBehind != null)
            // {
            // hourbehind = result.TimeBehind.Value.Hour;
            // minutebehind = result.TimeBehind.Value.Minute;
            // secondbehind = result.TimeBehind.Value.Second;
            // }

            // SportResult sportResult = new SportResult
            // {
            // ActivityName = result.ActivityName,
            // FirstName = result.FirstName,
            // LastName = result.LastName,
            // Nationality = result.Nationality,
            // Club = result.Club,
            // Rank = result.Rank,
            // RankingPoints = result.RankingPoints,
            // Hour = hour,
            // Minute = minute,
            // Second = second,
            // HourBehind = hourbehind,
            // MinuteBehind = minutebehind,
            // SecondBehind = secondbehind,
            // StartNo = result.StartNo,
            // Team = result.Team,
            // EventName = result.EventName,
            // ClassExerciseName = result.ClassExerciseName,
            // EventTypeText = result.EventTypeText,
            // EventDate = result.EventDate,
            // EventId = result.EventId,
            // CompetitorId = result.CompetitorId,
            // ClubId = result.ClubId,
            // PersonId = result.PersonId,
            // TeamId = result.TeamId,
            // ZipCode = zipcode,
            // City = city

            // };

            // listOfSportsResults.Add(sportResult);

            // }
            // }

            // Logger.Debug("Checking if we are to generate XML for Results");
            // if (PushResults)
            // {
            // Logger.Info("Creating the XML-file for Results");
            // doc = CreateEventXml(sportEvent, listOfSportsResults);
            // CreateXmlFile(doc, sportEvent);
            // }

            // }
            // }
            // }
            // Logger.Debug("Checking if we are to generate XML for Schedule");
            // if (PushSchedule)
            // {
            // Logger.Info("Creating the XML-file for Schedule");
            // doc = CreateEventXml(sportEvent);
            // CreateXmlFile(doc, sportEvent);

            // teamSportDatabase.SetDownload(sportEvent.EventId);

            // }
            // }

            // }

            // }
        }

        /// <summary>
        /// The create data file from get.
        /// </summary>
        public void CreateDataFileFromGet()
        {
            // This means that we shall create results files, we shall also update the database and set download to true
            if (RenderResult)
            {
                Logger.Debug("We are to create results, and then also XML");

                // _CheckEventsAgainstDatabase();
            }

            // This means that we are to populate or update the database with data, and not sending out results
            if (RenderResult == false)
            {
                Logger.Debug("We are to create a document or populate the database with data");
                _PopulateEventsDatabaseTable(SportId);
            }
        }

        #endregion

        /// <summary>
        /// The _ populate events database table.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        private void _PopulateEventsDatabaseTable(int sportId)
        {
            Logger.Debug("We are in _PopulateEventsDatabaseTable");

            // Then we shall only create event list and store this in the database
            var searchRequest = new EventSearchRequest
            {
                StartDate = DateStart,
                EndDate = DateEnd
            };

            var searchResponse = EventClient.SearchEvents(searchRequest);

            if (searchResponse.Success)
            {
                EventSearchResult[] eventSearchResults = searchResponse.Events;

                // 207 = soccer

                // Getting all events, except those that are defined as football/soccer (which we get from NFF)
                var eventsQuery = from myEvent in eventSearchResults where myEvent.ActivityId != 207 select myEvent;

                var searchResults = eventsQuery as EventSearchResult[] ?? eventsQuery.ToArray();
                Logger.Debug("We are to insert " + searchResults.Count() + " rows of data");
                List<SportEvent> myEvents =
                    searchResults.Select(
                        searchResult =>
                        new SportEvent
                        {
                            EventId = searchResult.EventId,
                            EventName = searchResult.EventName,
                            EventLocation = searchResult.Location,
                            EventDateStart = searchResult.StartDate,
                            EventDateEnd = searchResult.EndDate,
                            EventTimeStart = searchResult.StartTime,
                            EventTimeEnd = searchResult.EndTime,
                            EventOrganizerId = searchResult.ArrangingOrgId,
                            EventOrganizationName = searchResult.ArrangingOrgName,
                            ActivityId = searchResult.ActivityId,
                            ActivityName = searchResult.ActivityName
                        }).ToList();

                // Now we have to add more data to what we got from NIF
                List<SportEvent> sportsEvents = new List<SportEvent>();

                // Looping the list
                foreach (SportEvent thisEvent in myEvents)
                {
                    // Trying to find the main organization behind the event. 
                    FederationByOrgRequest federationByOrgRequest = new FederationByOrgRequest
                    {
                        FunctionalityId =
                            _inn
                            .FunctionalityId,
                        User = _inn.User
                    };

                    if (thisEvent.EventOrganizerId != null)
                    {
                        federationByOrgRequest.OrgId = thisEvent.EventOrganizerId.Value;

                        var federationByOrgResponse =
                            OrgClientV2.GetFederationByOrg(federationByOrgRequest);

                        if (federationByOrgResponse.Success)
                        {
                            OrgRequest orgRequest = new OrgRequest
                            {
                                
                                Id = federationByOrgResponse.FederationId
                            };

                            OrgResponse orgResponse = OrgClient.GetOrg(orgRequest);

                            if (orgResponse.Success)
                            {
                                thisEvent.AdministrativeOrgShortName = orgResponse.Org.ShortName;
                                thisEvent.AdministrativeOrgName = orgResponse.Org.Name;
                                thisEvent.AdministrativeOrgId = orgResponse.Org.Id;

                                FederationDisciplineRequest federationDisciplineRequest =
                                    new FederationDisciplineRequest
                                    {
                                        OrgId = thisEvent.AdministrativeOrgId,
                                    };
                                var federationDisciplineResponse =
                                    FederationServiceClient.GetFederationDisciplines(federationDisciplineRequest);

                                if (federationDisciplineResponse.Success)
                                {
                                    FederationDiscipline[] disciplines =
                                        federationDisciplineResponse.FederationDiscipline;

                                    foreach (FederationDiscipline discipline in disciplines)
                                    {
                                        if (discipline.ActivityId == thisEvent.ParentActivityId)
                                        {
                                            thisEvent.ParentActivity = discipline.ActivityName;
                                            thisEvent.ParentActivityId = discipline.ActivityId;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    sportsEvents.Add(thisEvent);
                }

                // Now that we have a list of Event Objects we can start pushing these into the database
                // We could do that above though, but since we don't know if we are to create XMLs and Database-objects,
                // Then we have to do both
                NifSportDatabase sportsDataDatabase = new NifSportDatabase();
                sportsDataDatabase.InsertEvent(sportsEvents);

                if (CreateXml)
                {
                    foreach (SportEvent sportsEvent in sportsEvents)
                    {
                        XmlDocument xmlDocument = CreateEventXml(sportsEvent);

                        CreateXmlFile(xmlDocument, sportsEvent, sportId);
                    }
                }
            }
        }

        /// <summary>
        /// Method that creates the database-structure
        /// </summary>
        /// <param name="sportEvent">
        /// An Event object that is used to create the XML file
        /// </param>
        /// <param name="listOfSportsResults">
        /// A Result object that holdes data for creating results XML-portion
        /// </param>
        /// <returns>
        /// The <see cref="XmlDocument"/>.
        /// </returns>
        private XmlDocument CreateEventXml(SportEvent sportEvent, IEnumerable<SportResult> listOfSportsResults)
        {
            // Creating xml-document
            Logger.Debug("I am now creating the XML Structure!");

            // Calling the createEventXML-method with ONE parameter
            XmlDocument doc = CreateEventXml(sportEvent);

            Logger.Debug("Done creating the XMLDocument object!");
            try
            {
                // To indicate some type of status
                /*
                 * 0 - not complete
                 * 1 - complete 
                 * ?
                 * 
                 * Only to be used when generating results
                 */
                XmlElement xmlResults = doc.CreateElement("SportsResults");

                var resultQuery = from results in listOfSportsResults
                                  group results by results.ClassExerciseName
                                  into resultsGroup orderby resultsGroup.Key select resultsGroup;

                foreach (var nameGroup in resultQuery)
                {
                    // Creating the XML Element SportResult - This is the root for these results
                    XmlElement xmlResult = doc.CreateElement("SportResult");

                    XmlElement xmlSportsResultMetaData = doc.CreateElement("SportResult-MetaInfo");

                    XmlElement xmlClassExerciseName = doc.CreateElement("ClassExercise-Name");
                    XmlText xmlText = doc.CreateTextNode(nameGroup.Key);
                    xmlClassExerciseName.AppendChild(xmlText);

                    XmlElement xmlGender = doc.CreateElement("Gender");
                    if (nameGroup.Key.ToLower().Contains("kvinner"))
                    {
                        xmlText = doc.CreateTextNode("Female");
                    }
                    else if (nameGroup.Key.ToLower().Contains("jenter"))
                    {
                        xmlText = doc.CreateTextNode("Female");
                    }
                    else if (nameGroup.Key.ToLower().Contains("menn"))
                    {
                        xmlText = doc.CreateTextNode("Male");
                    }
                    else if (nameGroup.Key.ToLower().Contains("gutter"))
                    {
                        xmlText = doc.CreateTextNode("Male");
                    }
                    else
                    {
                        xmlText = doc.CreateTextNode(string.Empty);
                    }

                    xmlGender.AppendChild(xmlText);

                    xmlSportsResultMetaData.AppendChild(xmlClassExerciseName);
                    xmlSportsResultMetaData.AppendChild(xmlGender);

                    int srCounter = 0;
                    foreach (SportResult mySportsResult in nameGroup)
                    {
                        if (srCounter == 0)
                        {
                            xmlSportsResultMetaData.SetAttribute("Id", mySportsResult.EventId.ToString());
                            xmlResult.AppendChild(xmlSportsResultMetaData);
                            srCounter = 1;
                        }

                        XmlElement xmlPlayer = doc.CreateElement("Player");
                        xmlPlayer.SetAttribute("PlayerId", mySportsResult.PersonId.ToString());

                        XmlElement xmlPlayerMetaInfo = doc.CreateElement("Player-MetaInfo");

                        xmlPlayerMetaInfo.SetAttribute("start-number", mySportsResult.StartNo.ToString());

                        xmlPlayerMetaInfo.SetAttribute("competitorid", mySportsResult.CompetitorId.ToString());

                        XmlElement xmlPlayerName = doc.CreateElement("Name");
                        xmlPlayerName.SetAttribute("FirstName", mySportsResult.FirstName);
                        xmlPlayerName.SetAttribute("LastName", mySportsResult.LastName);

                        if (mySportsResult.PersonGender != null)
                        {
                            xmlPlayerName.SetAttribute("Gender", mySportsResult.PersonGender);
                        }

                        xmlText = doc.CreateTextNode(mySportsResult.FirstName + " " + mySportsResult.LastName);
                        xmlPlayerName.AppendChild(xmlText);

                        XmlElement xmlTeam = doc.CreateElement("Team");
                        if (mySportsResult.TeamId != null)
                        {
                            xmlTeam.SetAttribute("TeamId", mySportsResult.TeamId.ToString());
                        }

                        xmlText = doc.CreateTextNode(string.Empty);
                        if (mySportsResult.Team != null)
                        {
                            xmlText = doc.CreateTextNode(mySportsResult.Team);
                        }

                        xmlTeam.AppendChild(xmlText);

                        XmlElement xmlClub = doc.CreateElement("Club");
                        xmlClub.SetAttribute("ClubId", mySportsResult.ClubId.ToString());
                        xmlText = doc.CreateTextNode(mySportsResult.Club);
                        xmlClub.AppendChild(xmlText);

                        XmlElement xmlNationality = doc.CreateElement("Nationality");
                        xmlText = doc.CreateTextNode(string.Empty);
                        if (mySportsResult.Nationality != null)
                        {
                            xmlText = doc.CreateTextNode(mySportsResult.Nationality);
                        }

                        xmlNationality.AppendChild(xmlText);

                        XmlElement xmlPlayerStats = doc.CreateElement("Player-Stats");
                        XmlElement xmlRank = doc.CreateElement("Rank");
                        if (mySportsResult.Rank != null)
                        {
                            xmlRank.SetAttribute("value", mySportsResult.Rank.ToString());
                        }

                        if (mySportsResult.RankingPoints != null)
                        {
                            xmlRank.SetAttribute("ranking-points", mySportsResult.RankingPoints.ToString());
                        }

                        xmlRank.SetAttribute("result-values", mySportsResult.ResultValues.ToString());

                        xmlPlayerStats.AppendChild(xmlRank);

                        XmlElement xmlTime = doc.CreateElement("Time");
                        xmlTime.SetAttribute("hour", mySportsResult.Hour.ToString().PadLeft(2, '0'));
                        xmlTime.SetAttribute("minute", mySportsResult.Minute.ToString().PadLeft(2, '0'));
                        xmlTime.SetAttribute("second", mySportsResult.Second.ToString().PadLeft(2, '0'));
                        xmlTime.SetAttribute("hourbehind", mySportsResult.HourBehind.ToString().PadLeft(2, '0'));
                        xmlTime.SetAttribute("minutebehind", mySportsResult.MinuteBehind.ToString().PadLeft(2, '0'));
                        xmlTime.SetAttribute("secondbehind", mySportsResult.SecondBehind.ToString().PadLeft(2, '0'));

                        xmlPlayerStats.AppendChild(xmlTime);

                        // mySportsResult.Time
                        xmlPlayerMetaInfo.AppendChild(xmlPlayerName);
                        xmlPlayerMetaInfo.AppendChild(xmlNationality);
                        xmlPlayerMetaInfo.AppendChild(xmlTeam);
                        xmlPlayerMetaInfo.AppendChild(xmlClub);
                        xmlPlayer.AppendChild(xmlPlayerMetaInfo);
                        xmlPlayer.AppendChild(xmlPlayerStats);

                        xmlResult.AppendChild(xmlPlayer);
                    }

                    // xmlResults.AppendChild(xmlSportsResultMetaData);
                    xmlResults.AppendChild(xmlResult);
                }

                // End inner loop
                if (doc.DocumentElement != null)
                {
                    doc.DocumentElement.AppendChild(xmlResults);
                }

                return doc;
            }
            catch (XmlException exception)
            {
                // Logging the exception
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
                return null;
            }
            catch (Exception exception)
            {
                // Logging the exception
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// Method that creates the database-structure
        /// </summary>
        /// <param name="sportEvent">
        /// An Event object that is used to create the XML file
        /// </param>
        /// <returns>
        /// The <see cref="XmlDocument"/>.
        /// </returns>
        private XmlDocument CreateEventXml(SportEvent sportEvent)
        {
            // Creating xml-document
            Logger.Debug("I am now creating the XML Structure!");

            XmlDocument doc = new XmlDocument(); // Create the XML Declaration, and append it to XML document

            Logger.Debug("Done creating the XMLDocument object!");
            try
            {
                XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", null, null);
                Logger.Debug("Done creating the XmlDeclaration!");

                doc.AppendChild(dec); // Create the root element
                Logger.Debug("Done Create the root element!");

                /* XmlElement xmlFile,
                    xmlFileName,
                    xmlFileVersion,
                    xmlFileCreated, xmlEventDate; */
                XmlText xmlText;

                // To indicate some type of status
                /*
                 * 0 - not complete
                 * 1 - complete 
                 * ?
                 * 
                 * Only to be used when generating results
                 */

                // We need to check this number against how many matches to distribute (with results)
                // so we have to contact the database
                // string SQLQueryCheckNumberOfMatches = "SELECT ";
                XmlElement xmlMainRoot = doc.CreateElement("SportsData");
                XmlElement xmlSportsEvent = doc.CreateElement("SportEvent");

                xmlSportsEvent.SetAttribute("Type", RenderResult == false ? "terminliste" : "resultat");

                // Set the district-information
                // District District = oclient.GetDistrict(tournament.DistrictId);
                Logger.Debug("About to create more information on Tournament and more");

                XmlElement xmlEventMeta = doc.CreateElement("SportEvent-MetaInfo");
                xmlEventMeta.SetAttribute("Id", sportEvent.EventId.ToString());
                xmlEventMeta.SetAttribute("Name", sportEvent.EventName);
                if (sportEvent.EventDateStart != null)
                {
                    xmlEventMeta.SetAttribute(
                        "Date-Start", 
                        sportEvent.EventDateStart.Value.Year.ToString() + "-"
                        + sportEvent.EventDateStart.Value.Month.ToString().PadLeft(2, '0') + "-"
                        + sportEvent.EventDateStart.Value.Day.ToString().PadLeft(2, '0'));
                }

                if (sportEvent.EventDateEnd != null)
                {
                    xmlEventMeta.SetAttribute(
                        "Date-End", 
                        sportEvent.EventDateEnd.Value.Year.ToString() + "-"
                        + sportEvent.EventDateEnd.Value.Month.ToString().PadLeft(2, '0') + "-"
                        + sportEvent.EventDateEnd.Value.Day.ToString().PadLeft(2, '0'));
                }

                if (sportEvent.EventDateStart != null)
                {
                    xmlEventMeta.SetAttribute(
                        "Time-Start", 
                        sportEvent.EventDateStart.Value.Hour.ToString().PadLeft(2, '0') + ":"
                        + sportEvent.EventDateStart.Value.Minute.ToString().PadLeft(2, '0') + ":"
                        + sportEvent.EventDateStart.Value.Second.ToString().PadLeft(2, '0'));
                }

                if (sportEvent.EventDateEnd != null)
                {
                    xmlEventMeta.SetAttribute(
                        "Time-End", 
                        sportEvent.EventDateEnd.Value.Hour.ToString().PadLeft(2, '0') + ":"
                        + sportEvent.EventDateEnd.Value.Minute.ToString().PadLeft(2, '0') + ":"
                        + sportEvent.EventDateEnd.Value.Second.ToString().PadLeft(2, '0'));
                }

                XmlElement xmlLocation = doc.CreateElement("Location");
                if (sportEvent.EventLocation != null)
                {
                    xmlText = doc.CreateTextNode(sportEvent.EventLocation);
                    xmlLocation.AppendChild(xmlText);
                }

                // District
                XmlElement xmlDistrict = doc.CreateElement("District");
                xmlDistrict.SetAttribute("Name", sportEvent.EventDistrict);
                if (sportEvent.EventDistrict != null)
                {
                    xmlText = doc.CreateTextNode(sportEvent.EventDistrict);
                    xmlDistrict.AppendChild(xmlText);
                }

                // Add the siblings
                xmlEventMeta.AppendChild(xmlLocation);
                xmlEventMeta.AppendChild(xmlDistrict);

                xmlSportsEvent.AppendChild(xmlEventMeta);

                // Creating Header area
                XmlElement xmlHeader = doc.CreateElement("Header");

                // Adding placeholder for main organisation
                XmlElement xmlOrganisation = doc.CreateElement("Organisation");
                if (sportEvent.AdministrativeOrgName != null)
                {
                    xmlOrganisation.SetAttribute("Name", sportEvent.AdministrativeOrgName);
                }

                xmlHeader.AppendChild(xmlOrganisation);

                // Adding customer information
                // TODO: Add code to get the customers from the database
                XmlElement xmlCustomers = doc.CreateElement("Customers");
                XmlElement xmlCustomer = doc.CreateElement("Customer");
                xmlText = doc.CreateTextNode("NTB");
                xmlCustomer.AppendChild(xmlText);
                xmlCustomers.AppendChild(xmlCustomer);
                xmlHeader.AppendChild(xmlCustomers);
                xmlText.Value = string.Empty;

                // Adding information about the sport
                // TODO: Add support for the main sport, the one below is sort of the genre of the sport
                XmlElement xmlSport = doc.CreateElement("Sport");

                if (sportEvent.ActivityName != null)
                {
                    // xmlText = doc.CreateTextNode(MyEvent.ActivityName);
                    xmlSport.SetAttribute("genre", sportEvent.ActivityName);
                    xmlSport.SetAttribute("genreid", sportEvent.ActivityId.ToString());
                }

                if (sportEvent.ParentActivity != null)
                {
                    xmlSport.SetAttribute("sportid", sportEvent.ParentActivityId.ToString());
                    xmlText = doc.CreateTextNode(sportEvent.ParentActivity);
                }

                xmlSport.AppendChild(xmlText);
                xmlHeader.AppendChild(xmlSport);

                // Creating the XML Element Organizor
                var xmlEventOrganizator = doc.CreateElement("Event-Organizator");
                xmlEventOrganizator.SetAttribute("id", sportEvent.EventOrganizerId.ToString());
                xmlEventOrganizator.SetAttribute("name", sportEvent.EventOrganizerName);
                if (sportEvent.EventOrganizerName != null)
                {
                    xmlText = doc.CreateTextNode(sportEvent.EventOrganizerName);
                    xmlEventOrganizator.AppendChild(xmlText);
                }
                
                xmlHeader.AppendChild(xmlEventOrganizator);

                // Now we shall do the results
                xmlSportsEvent.AppendChild(xmlHeader);
                xmlMainRoot.AppendChild(xmlSportsEvent);
                doc.AppendChild(xmlMainRoot);

                return doc;
            }
            catch (XmlException exception)
            {
                // Logging the exception
                Logger.Error(exception);
                return null;
            }
            catch (Exception exception)
            {
                // Logging the exception
                Logger.Error(exception);
                return null;
            }
        }

        /// <summary>
        /// The create xml file.
        /// </summary>
        /// <param name="doc">
        /// The doc.
        /// </param>
        /// <param name="sportEvent">
        /// The sport event.
        /// </param>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        public void CreateXmlFile(XmlDocument doc, SportEvent sportEvent, int sportId)
        {
            XPathNavigator navigator = doc.CreateNavigator();
            XPathNodeIterator iterator = navigator.Select("/SportsData/SportEvent/SportEvent-MetaInfo/@Id");

            iterator.MoveNext();

            int eventId = Convert.ToInt32(iterator.Current.Value);

            Filename filename = CreateFileName(sportEvent, sportId);

            // Writing to file
            if (OutputFolder == @"\" || string.IsNullOrEmpty(OutputFolder))
            {
                OutputFolder = ConfigurationManager.AppSettings["OutPath"];
            }

            // Adding file information now
            /*
             * <File>
        <Name>NTBSportsData_31082012T1230073705_ONFF_D1_T129933_V0.xml</Name>
        <Version>0</Version>
        <Created>31082012T1230073705</Created>
      </File>
             */
            XmlNodeList nodeList = doc.GetElementsByTagName("Sport");
            foreach (XmlNode node in nodeList)
            {
                if (node.OuterXml.Contains("Sport"))
                {
                    XmlElement xmlFile = doc.CreateElement("File");
                    XmlElement xmlFileName = doc.CreateElement("Name");
                    XmlText xmlText = doc.CreateTextNode(filename.Name + ".xml");
                    xmlFileName.AppendChild(xmlText);

                    // We need to check against database to see if we've created this file before
                    XmlElement xmlVersion = doc.CreateElement("Version");
                    xmlText = doc.CreateTextNode("1");
                    xmlVersion.AppendChild(xmlText);

                    // It's not always that we are getting tournament, sometime we get Event
                    XmlElement xmlFileCreated = doc.CreateElement("Created");
                    xmlText =
                        doc.CreateTextNode(
                            DateTime.Today.Year + DateTime.Today.Month.ToString().PadLeft(2, '0')
                            + DateTime.Today.Day.ToString().PadLeft(2, '0'));
                    xmlFileCreated.AppendChild(xmlText);

                    xmlFile.AppendChild(xmlFileName);
                    xmlFile.AppendChild(xmlVersion);
                    xmlFile.AppendChild(xmlFileCreated);

                    XmlNode parent = node.ParentNode;
                    if (parent != null)
                    {
                        parent.AppendChild(xmlFile);
                    }
                }
            }

            filename.FullName = filename.Name + ".xml";
            filename.Path = OutputFolder;
            Logger.Debug("Filename: " + filename.FullName);

            try
            {
                if (!Directory.Exists(OutputFolder))
                {
                    Logger.Debug("I am creating outputfolder: " + OutputFolder);
                    Directory.CreateDirectory(OutputFolder);
                }
                else
                {
                    Directory.SetCurrentDirectory(OutputFolder);
                }

                // Creating the complete Filename
                string pathAndFilename = filename.Path + @"\" + filename.FullName;
                Logger.Debug("Testing to save: " + pathAndFilename);

                // string completeFilename = @"c:\" + DocumentName;
                XmlWriterSettings writerSettings = new XmlWriterSettings
                                                       {
                                                           Indent = true, 
                                                           OmitXmlDeclaration = false, 
                                                           Encoding = Encoding.UTF8
                                                       };

                // Consider: Change Using to While so that we can loop incase the file is in use...
                using (XmlWriter writer = XmlWriter.Create(pathAndFilename, writerSettings))
                {
                    try
                    {
                        {
                            doc.Save(writer);

                            // Now we can update the database as well
                            NifSportDatabase db = new NifSportDatabase();
                            db.UpdateEventData(eventId);
                        }
                    }
                    catch (IOException ioex)
                    {
                        Logger.Debug(ioex);
                    }
                }
            }
            catch (XmlException xex)
            {
                Logger.Error("Problems writing file to: " + filename.FullName, xex);
            }
            catch (IOException ioex)
            {
                Logger.Error(ioex);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        /// <summary>
        /// The create file name.
        /// </summary>
        /// <param name="sportEvent">
        /// The sport event.
        /// </param>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <returns>
        /// The <see cref="NTB.SportsData.Service.Domain.Classes.Filename"/>.
        /// </returns>
        private Filename CreateFileName(SportEvent sportEvent, int sportId)
        {
            // Creating some variables that are being used in this method
            string connectionString = ConfigurationManager.AppSettings["ConnectionString"];
            string filename = string.Empty;
            int documentVersion = 1;

            Filename fileName = new Filename();

            try
            {
                SqlConnection sqlConnection = new SqlConnection(connectionString);
                SqlCommand sqlCommand = new SqlCommand("Service_GetDocumentByTournamentId", sqlConnection)
                                            {
                                                CommandType
                                                    =
                                                    CommandType
                                                    .StoredProcedure
                                            };

                Logger.Debug("GetDocumentByTournamentId: " + sportEvent.EventId);

                sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", sportEvent.EventId));

                sqlConnection.Open();

                SqlDataReader mySqlDataReader = sqlCommand.ExecuteReader();

                if (mySqlDataReader.HasRows)
                {
                    while (mySqlDataReader.Read())
                    {
                        string documentName = mySqlDataReader["DocumentName"].ToString();
                        documentVersion = Convert.ToInt32(mySqlDataReader["DocumentVersion"]) + 1;

                        // Creating the new filename
                        filename = documentName;
                    }

                    // Closing connection
                    sqlConnection.Close();

                    sqlCommand = new SqlCommand("Service_UpdateDocument", sqlConnection)
                                     {
                                         CommandType =
                                             CommandType
                                             .StoredProcedure
                                     };

                    // Setting the DocumentName
                    Logger.Debug("DocumentVersion: " + documentVersion);
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentVersion", documentVersion));

                    // Setting the DocumentName
                    Logger.Debug("DocumentCreated: " + DateTime.Now.ToString());
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentCreated", DateTime.Now));

                    // Setting the TournamentId
                    Logger.Debug("TournamentId: " + TournamentId);
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", sportEvent.EventId));

                    // Open the connection
                    sqlConnection.Open();

                    // Run the query
                    sqlCommand.ExecuteNonQuery();
                }
                else
                {
                    // We have not found a document produced for this tournament
                    // We generate the filename, and then just the stem
                    DateTime todayFile = DateTime.Today;

                    // We are not adding XML to the filename, this will be added when we are creating the rest
                    filename = "NTB_SportsData_" + todayFile.Year + todayFile.Month.ToString().PadLeft(2, '0')
                               + todayFile.Day.ToString().PadLeft(2, '0') + "_"
                               + sportEvent.ActivityName.Replace(@" ", string.Empty) + "_E" + sportEvent.EventId;

                    // Closing connection
                    sqlConnection.Close();

                    // We must now add this to the database
                    // sqlConnection = new SqlConnection(connectionString);
                    sqlCommand = new SqlCommand("Service_InsertDocument", sqlConnection)
                                     {
                                         CommandType =
                                             CommandType
                                             .StoredProcedure
                                     };

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", sportEvent.EventId));

                    // TODO: Change this to really get it from the database!
                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                    // Setting the version to 1
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentVersion", documentVersion));

                    // Setting the DocumentName
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentName", filename));

                    // Setting the DocumentName
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentCreated", DateTime.Now));

                    // Open the connection
                    sqlConnection.Open();

                    // Run the query
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlexception)
            {
                Logger.Error(sqlexception.Message);
                Logger.Error(sqlexception.StackTrace);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }

            fileName.Name = filename;
            fileName.Version = documentVersion;
            fileName.Created = DateTime.Now;

            filename += "_V" + documentVersion;

            // Writing to file
            if (OutputFolder == @"\" || string.IsNullOrEmpty(OutputFolder))
            {
                OutputFolder = ConfigurationManager.AppSettings["OutPath"];
            }

            fileName.FullName = filename + ".xml";
            fileName.Path = OutputFolder;
            Logger.Debug("Filename: " + fileName.FullName);

            return fileName;
        }
    }
}