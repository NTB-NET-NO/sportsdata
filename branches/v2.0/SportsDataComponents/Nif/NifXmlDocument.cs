// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NifXmlDocument.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Components.Nif
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using System.Xml.XPath;
    
    using log4net;

    using NTB.SportsData.Service.Domain.Classes;

    // adding support for log4net

    // Using classes

    /// <summary>
    /// The nif xml document.
    /// </summary>
    public class NifXmlDocument
    {
        /// <summary>
        /// The logger.
        /// </summary>
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(NifXmlDocument));

        /// <summary>
        /// Initializes a new instance of the <see cref="NifXmlDocument"/> class. 
        /// Constructor for the NIFDatafileCreator object. This is used to create Structures
        /// </summary>
        public NifXmlDocument()
        {
            {
                // Set up logger
                log4net.Config.XmlConfigurator.Configure();
                if (!LogManager.GetRepository().Configured)
                {
                    log4net.Config.BasicConfigurator.Configure();
                }
            }
        }

        /// <summary>
        /// Gets or sets the output folder.
        /// </summary>
        public string OutputFolder { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether render result.
        /// </summary>
        public bool RenderResult { get; set; }

        /// <summary>
        /// Method that creates the database-structure
        /// </summary>
        /// <param name="sportEvent">
        /// An Event object that is used to create the XML file
        /// </param>
        /// <param name="listOfSportsResults">
        /// A Result object that holdes data for creating results XML-portion
        /// </param>
        /// <returns>
        /// An Xml Document
        /// </returns>
        public XmlDocument CreateEventXml(SportEvent sportEvent, IEnumerable<SportResult> listOfSportsResults)
        {
            // Creating xml-document
            Logger.Debug("I am now creating the XML Structure!");

            // Calling the createEventXML-method with ONE parameter
            var doc = this.CreateEventXml(sportEvent);

            Logger.Debug("Done creating the XMLDocument object!");
            try
            {
                // To indicate some type of status
                /*
                 * 0 - not complete
                 * 1 - complete 
                 * ?
                 * 
                 * Only to be used when generating results
                 */
                XmlElement xmlResults = doc.CreateElement("SportsResults");

                var resultQuery = from results in listOfSportsResults
                                  group results by results.ClassExerciseName
                                      into resultsGroup
                                      orderby resultsGroup.Key
                                      select resultsGroup;

                foreach (var nameGroup in resultQuery)
                {
                    // Creating the XML Element SportResult - This is the root for these results
                    XmlElement xmlResult = doc.CreateElement("SportResult");

                    XmlElement xmlSportsResultMetaData = doc.CreateElement("SportResult-MetaInfo");

                    XmlElement xmlClassExerciseName = doc.CreateElement("ClassExercise-Name");
                    XmlText xmlText = doc.CreateTextNode(nameGroup.Key);
                    xmlClassExerciseName.AppendChild(xmlText);

                    XmlElement xmlGender = doc.CreateElement("Gender");
                    if (nameGroup.Key.ToLower().Contains("kvinner"))
                    {
                        xmlText = doc.CreateTextNode("Female");
                    }
                    else if (nameGroup.Key.ToLower().Contains("jenter"))
                    {
                        xmlText = doc.CreateTextNode("Female");
                    }
                    else if (nameGroup.Key.ToLower().Contains("menn"))
                    {
                        xmlText = doc.CreateTextNode("Male");
                    }
                    else if (nameGroup.Key.ToLower().Contains("gutter"))
                    {
                        xmlText = doc.CreateTextNode("Male");
                    }
                    else
                    {
                        xmlText = doc.CreateTextNode(string.Empty);
                    }

                    xmlGender.AppendChild(xmlText);

                    xmlSportsResultMetaData.AppendChild(xmlClassExerciseName);
                    xmlSportsResultMetaData.AppendChild(xmlGender);

                    int counter = 0;

                    foreach (SportResult sportsResult in nameGroup)
                    {
                        if (counter == 0)
                        {
                            xmlSportsResultMetaData.SetAttribute("Id", sportsResult.EventId.ToString());
                            xmlResult.AppendChild(xmlSportsResultMetaData);
                            counter = 1;
                        }

                        XmlElement xmlPlayer = doc.CreateElement("Player");
                        xmlPlayer.SetAttribute("PlayerId", sportsResult.PersonId.ToString());

                        XmlElement xmlPlayerMetaInfo = doc.CreateElement("Player-MetaInfo");

                        xmlPlayerMetaInfo.SetAttribute("start-number", sportsResult.StartNo.ToString());

                        xmlPlayerMetaInfo.SetAttribute("competitorid", sportsResult.CompetitorId.ToString());

                        XmlElement xmlPlayerName = doc.CreateElement("Name");
                        xmlPlayerName.SetAttribute("FirstName", sportsResult.FirstName);
                        xmlPlayerName.SetAttribute("LastName", sportsResult.LastName);

                        if (sportsResult.PersonGender != null)
                        {
                            xmlPlayerName.SetAttribute("Gender", sportsResult.PersonGender);
                        }

                        xmlText = doc.CreateTextNode(sportsResult.FirstName + " " + sportsResult.LastName);
                        xmlPlayerName.AppendChild(xmlText);

                        XmlElement xmlTeam = doc.CreateElement("Team");
                        if (sportsResult.TeamId != null)
                        {
                            xmlTeam.SetAttribute("TeamId", sportsResult.TeamId.ToString());
                        }

                        xmlText = doc.CreateTextNode(string.Empty);
                        if (sportsResult.Team != null)
                        {
                            xmlText = doc.CreateTextNode(sportsResult.Team);
                        }

                        xmlTeam.AppendChild(xmlText);

                        XmlElement xmlClub = doc.CreateElement("Club");
                        xmlClub.SetAttribute("ClubId", sportsResult.ClubId.ToString());
                        xmlText = doc.CreateTextNode(sportsResult.Club);
                        xmlClub.AppendChild(xmlText);

                        XmlElement xmlNationality = doc.CreateElement("Nationality");
                        xmlText = doc.CreateTextNode(string.Empty);
                        if (sportsResult.Nationality != null)
                        {
                            xmlText = doc.CreateTextNode(sportsResult.Nationality);
                        }

                        xmlNationality.AppendChild(xmlText);

                        XmlElement xmlPlayerStats = doc.CreateElement("Player-Stats");
                        XmlElement xmlRank = doc.CreateElement("Rank");
                        if (sportsResult.Rank != null)
                        {
                            xmlRank.SetAttribute("value", sportsResult.Rank.ToString());
                        }

                        if (sportsResult.RankingPoints != null)
                        {
                            xmlRank.SetAttribute("ranking-points", sportsResult.RankingPoints.ToString());
                        }

                        xmlRank.SetAttribute("result-values", sportsResult.ResultValues.ToString());

                        xmlPlayerStats.AppendChild(xmlRank);

                        XmlElement xmlTime = doc.CreateElement("Time");
                        xmlTime.SetAttribute("hour", sportsResult.Hour.ToString().PadLeft(2, '0'));
                        xmlTime.SetAttribute("minute", sportsResult.Minute.ToString().PadLeft(2, '0'));
                        xmlTime.SetAttribute("second", sportsResult.Second.ToString().PadLeft(2, '0'));
                        xmlTime.SetAttribute("hourbehind", sportsResult.HourBehind.ToString().PadLeft(2, '0'));
                        xmlTime.SetAttribute("minutebehind", sportsResult.MinuteBehind.ToString().PadLeft(2, '0'));
                        xmlTime.SetAttribute("secondbehind", sportsResult.SecondBehind.ToString().PadLeft(2, '0'));

                        xmlPlayerStats.AppendChild(xmlTime);

                        // mySportsResult.Time
                        xmlPlayerMetaInfo.AppendChild(xmlPlayerName);
                        xmlPlayerMetaInfo.AppendChild(xmlNationality);
                        xmlPlayerMetaInfo.AppendChild(xmlTeam);
                        xmlPlayerMetaInfo.AppendChild(xmlClub);
                        xmlPlayer.AppendChild(xmlPlayerMetaInfo);
                        xmlPlayer.AppendChild(xmlPlayerStats);

                        xmlResult.AppendChild(xmlPlayer);
                    }

                    // xmlResults.AppendChild(xmlSportsResultMetaData);
                    xmlResults.AppendChild(xmlResult);
                }

                // End inner loop
                if (doc.DocumentElement != null)
                {
                    doc.DocumentElement.AppendChild(xmlResults);
                }

                return doc;
            }
            catch (XmlException exception)
            {
                // Logging the exception
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
                return null;
            }
            catch (Exception exception)
            {
                // Logging the exception
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// Method that creates the database-structure
        /// </summary>
        /// <param name="sportEvent">
        /// An Event object that is used to create the XML file
        /// </param>
        /// <returns>
        /// The <see cref="XmlDocument"/>.
        /// </returns>
        public XmlDocument CreateEventXml(SportEvent sportEvent)
        {
            // Creating xml-document
            Logger.Debug("I am now creating the XML Structure!");

            var doc = new XmlDocument(); // Create the XML Declaration, and append it to XML document

            Logger.Debug("Done creating the XMLDocument object!");
            try
            {
                XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", null, null);
                Logger.Debug("Done creating the XmlDeclaration!");

                doc.AppendChild(dec); // Create the root element
                Logger.Debug("Done Create the root element!");

                /* XmlElement xmlFile,
                    xmlFileName,
                    xmlFileVersion,
                    xmlFileCreated, xmlEventDate; */
                XmlText xmlText;

                // To indicate some type of status
                /*
                 * 0 - not complete
                 * 1 - complete 
                 * ?
                 * 
                 * Only to be used when generating results
                 */

                // We need to check this number against how many matches to distribute (with results)
                // so we have to contact the database
                // string SQLQueryCheckNumberOfMatches = "SELECT ";
                var xmlMainRoot = doc.CreateElement("SportsData");
                var xmlSportsEvent = doc.CreateElement("SportEvent");

                xmlSportsEvent.SetAttribute("Type", RenderResult == false ? "terminliste" : "resultat");

                // Set the district-information
                // District District = oclient.GetDistrict(tournament.DistrictId);
                Logger.Debug("About to create more information on Tournament and more");

                XmlElement xmlEventMeta = doc.CreateElement("SportEvent-MetaInfo");
                xmlEventMeta.SetAttribute("Id", sportEvent.EventId.ToString());
                xmlEventMeta.SetAttribute("Name", sportEvent.EventName);
                if (sportEvent.EventDateStart != null)
                {
                    xmlEventMeta.SetAttribute(
                        "Date-Start",
                        sportEvent.EventDateStart.Value.Year + @"-" + sportEvent.EventDateStart.Value.Month.ToString().PadLeft(2, '0') + @"-" + sportEvent.EventDateStart.Value.Day.ToString().PadLeft(2, '0'));
                }

                if (sportEvent.EventDateEnd != null)
                {
                    xmlEventMeta.SetAttribute(
                        "Date-End", sportEvent.EventDateEnd.Value.Year.ToString() + "-" + sportEvent.EventDateEnd.Value.Month.ToString().PadLeft(2, '0') + "-" + sportEvent.EventDateEnd.Value.Day.ToString().PadLeft(2, '0'));
                }

                if (sportEvent.EventDateStart != null)
                {
                    xmlEventMeta.SetAttribute(
                        "Time-Start", sportEvent.EventDateStart.Value.Hour.ToString().PadLeft(2, '0') + ":" + sportEvent.EventDateStart.Value.Minute.ToString().PadLeft(2, '0') + ":" + sportEvent.EventDateStart.Value.Second.ToString().PadLeft(2, '0'));
                }

                if (sportEvent.EventDateEnd != null)
                {
                    xmlEventMeta.SetAttribute(
                        "Time-End", sportEvent.EventDateEnd.Value.Hour.ToString().PadLeft(2, '0') + ":" + sportEvent.EventDateEnd.Value.Minute.ToString().PadLeft(2, '0') + ":" + sportEvent.EventDateEnd.Value.Second.ToString().PadLeft(2, '0'));
                }

                XmlElement xmlLocation = doc.CreateElement("Location");
                if (sportEvent.EventLocation != null)
                {
                    xmlText = doc.CreateTextNode(sportEvent.EventLocation);
                    xmlLocation.AppendChild(xmlText);
                }

                // District
                XmlElement xmlDistrict = doc.CreateElement("District");
                xmlDistrict.SetAttribute("Name", sportEvent.EventDistrict);
                if (sportEvent.EventDistrict != null)
                {
                    xmlText = doc.CreateTextNode(sportEvent.EventDistrict);
                    xmlDistrict.AppendChild(xmlText);
                }

                // Add the siblings
                xmlEventMeta.AppendChild(xmlLocation);
                xmlEventMeta.AppendChild(xmlDistrict);

                xmlSportsEvent.AppendChild(xmlEventMeta);

                // Creating Header area
                XmlElement xmlHeader = doc.CreateElement("Header");

                // Adding placeholder for main organisation
                XmlElement xmlOrganisation = doc.CreateElement("Organisation");
                if (sportEvent.AdministrativeOrgName != null)
                {
                    xmlOrganisation.SetAttribute("Name", sportEvent.AdministrativeOrgName);
                }

                xmlHeader.AppendChild(xmlOrganisation);

                // Adding customer information
                // TODO: Add code to get the customers from the database
                XmlElement xmlCustomers = doc.CreateElement("Customers");
                XmlElement xmlCustomer = doc.CreateElement("Customer");
                xmlText = doc.CreateTextNode("NTB");
                xmlCustomer.AppendChild(xmlText);
                xmlCustomers.AppendChild(xmlCustomer);
                xmlHeader.AppendChild(xmlCustomers);
                xmlText.Value = string.Empty;

                // Adding information about the sport
                // TODO: Add support for the main sport, the one below is sort of the genre of the sport
                XmlElement xmlSport = doc.CreateElement("Sport");

                if (sportEvent.ActivityName != null)
                {
                    // xmlText = doc.CreateTextNode(MyEvent.ActivityName);
                    xmlSport.SetAttribute("genre", sportEvent.ActivityName);
                    xmlSport.SetAttribute("genreid", sportEvent.ActivityId.ToString());
                }

                if (sportEvent.ParentActivity != null)
                {
                    xmlSport.SetAttribute("sportid", sportEvent.ParentActivityId.ToString());
                    xmlText = doc.CreateTextNode(sportEvent.ParentActivity);
                }

                xmlSport.AppendChild(xmlText);
                xmlHeader.AppendChild(xmlSport);

                // Creating the XML Element Organizor
                XmlElement xmlEventOrganizator = doc.CreateElement("Event-Organizator");
                xmlEventOrganizator.SetAttribute("id", sportEvent.EventOrganizerId.ToString());
                xmlEventOrganizator.SetAttribute("name", sportEvent.EventOrganizerName);
                if (sportEvent.EventOrganizerName != null)
                {
                    xmlText = doc.CreateTextNode(sportEvent.EventOrganizerName);
                    xmlEventOrganizator.AppendChild(xmlText);
                }

                xmlHeader.AppendChild(xmlEventOrganizator);

                // Now we shall do the results
                xmlSportsEvent.AppendChild(xmlHeader);
                xmlMainRoot.AppendChild(xmlSportsEvent);
                doc.AppendChild(xmlMainRoot);

                return doc;
            }
            catch (XmlException exception)
            {
                // Logging the exception
                Logger.Error(exception);
                return null;
            }
            catch (Exception exception)
            {
                // Logging the exception
                Logger.Error(exception);
                return null;
            }
        }

        /// <summary>
        /// The create xml file.
        /// </summary>
        /// <param name="doc">
        /// The doc.
        /// </param>
        /// <param name="sportEvent">
        /// The sport event.
        /// </param>
        public void CreateXmlFile(XmlDocument doc, SportEvent sportEvent)
        {
            XPathNavigator navigator = doc.CreateNavigator();
            XPathNodeIterator iterator = navigator.Select("/SportsData/SportEvent/SportEvent-MetaInfo/@Id");

            iterator.MoveNext();

            int eventId = Convert.ToInt32(iterator.Current.Value);

            Filename filename = CreateFileName(sportEvent);

            // Writing to file
            if (OutputFolder == @"\" || string.IsNullOrEmpty(OutputFolder))
            {
                OutputFolder = ConfigurationManager.AppSettings["OutPath"];
            }

            // Adding file information now
            /*
             * <File>
        <Name>NTBSportsData_31082012T1230073705_ONFF_D1_T129933_V0.xml</Name>
        <Version>0</Version>
        <Created>31082012T1230073705</Created>
      </File>
             */
            XmlNodeList nodeList = doc.GetElementsByTagName("Sport");
            foreach (XmlNode node in nodeList)
            {
                if (node.OuterXml.Contains("Sport"))
                {
                    XmlElement xmlFile = doc.CreateElement("File");
                    XmlElement xmlFileName = doc.CreateElement("Name");
                    XmlText xmlText = doc.CreateTextNode(filename.Name + ".xml");
                    xmlFileName.AppendChild(xmlText);

                    // We need to check against database to see if we've created this file before
                    XmlElement xmlVersion = doc.CreateElement("Version");
                    xmlText = doc.CreateTextNode("1");
                    xmlVersion.AppendChild(xmlText);

                    // It's not always that we are getting tournament, sometime we get Event
                    XmlElement xmlFileCreated = doc.CreateElement("Created");
                    xmlText =
                        doc.CreateTextNode(
                            DateTime.Today.Year + DateTime.Today.Month.ToString().PadLeft(2, '0')
                            + DateTime.Today.Day.ToString().PadLeft(2, '0'));
                    xmlFileCreated.AppendChild(xmlText);

                    xmlFile.AppendChild(xmlFileName);
                    xmlFile.AppendChild(xmlVersion);
                    xmlFile.AppendChild(xmlFileCreated);

                    XmlNode parent = node.ParentNode;
                    if (parent != null)
                    {
                        parent.AppendChild(xmlFile);
                    }
                }
            }

            filename.FullName = filename.Name + ".xml";
            filename.Path = OutputFolder;
            Logger.Debug("Filename: " + filename.FullName);

            try
            {
                if (!Directory.Exists(OutputFolder))
                {
                    Logger.Debug("I am creating outputfolder: " + OutputFolder);
                    Directory.CreateDirectory(OutputFolder);
                }
                else
                {
                    Directory.SetCurrentDirectory(OutputFolder);
                }

                // Creating the complete Filename
                string pathAndFilename = filename.Path + @"\" + filename.FullName;
                Logger.Debug("Testing to save: " + pathAndFilename);

                // string completeFilename = @"c:\" + DocumentName;
                XmlWriterSettings writerSettings = new XmlWriterSettings
                {
                    Indent = true,
                    OmitXmlDeclaration = false,
                    Encoding = Encoding.UTF8
                };

                // Consider: Change Using to While so that we can loop incase the file is in use...
                using (XmlWriter writer = XmlWriter.Create(pathAndFilename, writerSettings))
                {
                    try
                    {
                        {
                            doc.Save(writer);

                            // Now we can update the database as well
                            NifSportDatabase db = new NifSportDatabase();
                            db.UpdateEventData(eventId);
                        }
                    }
                    catch (IOException ioex)
                    {
                        Logger.Debug(ioex);
                    }
                }
            }
            catch (XmlException xex)
            {
                Logger.Error("Problems writing file to: " + filename.FullName, xex);
            }
            catch (IOException ioex)
            {
                Logger.Error(ioex);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        /// <summary>
        /// The create file name.
        /// </summary>
        /// <param name="sportEvent">
        /// The sport event.
        /// </param>
        /// <returns>
        /// The <see cref="NTB.SportsData.Service.Domain.Classes.Filename"/>.
        /// </returns>
        private Filename CreateFileName(SportEvent sportEvent)
        {
            // Creating some variables that are being used in this method
            string connectionString = ConfigurationManager.AppSettings["ConnectionString"];
            string filename = string.Empty;
            int documentVersion = 1;

            Filename fileName = new Filename();

            try
            {
                SqlConnection sqlConnection = new SqlConnection(connectionString);
                SqlCommand sqlCommand = new SqlCommand("Service_GetDocumentByTournamentId", sqlConnection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                Logger.Debug("GetDocumentByTournamentId: " + sportEvent.EventId);

                sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", sportEvent.EventId));

                sqlConnection.Open();

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        string documentName = sqlDataReader["DocumentName"].ToString();
                        if (sqlDataReader["DocumentVersion"] != DBNull.Value)
                        {
                            documentVersion = Convert.ToInt32(sqlDataReader["DocumentVersion"]) + 1;
                        }

                        // Creating the new filename
                        filename = documentName;
                    }

                    // Closing connection
                    sqlConnection.Close();

                    sqlCommand = new SqlCommand("Service_UpdateDocument", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    // Setting the DocumentVersion
                    Logger.Debug("DocumentVersion: " + documentVersion);
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentVersion", documentVersion));

                    // Setting the DocumentCreated
                    Logger.Debug("DocumentCreated: " + DateTime.Today);
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentCreated", DateTime.Now));

                    // Setting the TournamentId
                    Logger.Debug("EventId: " + sportEvent.EventId);
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", sportEvent.EventId));

                    // Open the connection
                    sqlConnection.Open();

                    // Run the query
                    sqlCommand.ExecuteNonQuery();
                }
                else
                {
                    // We have not found a document produced for this tournament
                    // We generate the filename, and then just the stem
                    DateTime todayFile = DateTime.Today;

                    // We are not adding XML to the filename, this will be added when we are creating the rest
                    filename = "NTB_SportsData_" + todayFile.Year + todayFile.Month.ToString().PadLeft(2, '0')
                               + todayFile.Day.ToString().PadLeft(2, '0') + @"_"
                               + sportEvent.ClassExerciseId + @"_"
                               + sportEvent.ActivityName.Replace(" ", string.Empty) 
                               + @"_E" + sportEvent.EventId;

                    // Closing connection
                    sqlConnection.Close();

                    // We must now add this to the database
                    // sqlConnection = new SqlConnection(connectionString);
                    sqlCommand = new SqlCommand("Service_InsertDocument", sqlConnection)
                    { 
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", sportEvent.EventId));

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportEvent.ActivityId));

                    // DONE: Change this to really get it from the database or Configuration!

                    // Setting the version to 1
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentVersion", documentVersion));

                    // Setting the DocumentName
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentName", filename));

                    // Setting the DocumentName
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentCreated", DateTime.Now));

                    // Open the connection
                    sqlConnection.Open();

                    // Run the query
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlexception)
            {
                Logger.Error(sqlexception.Message);
                Logger.Error(sqlexception.StackTrace);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }

            fileName.Name = filename;
            fileName.Version = documentVersion;
            fileName.Created = DateTime.Now;

            filename += "_V" + documentVersion;

            // Writing to file
            if (OutputFolder == @"\" || string.IsNullOrEmpty(OutputFolder))
            {
                OutputFolder = ConfigurationManager.AppSettings["OutPath"];
            }

            fileName.FullName = filename + ".xml";
            fileName.Path = OutputFolder;
            Logger.Debug("Filename: " + fileName.FullName);

            return fileName;
        }
    }
}