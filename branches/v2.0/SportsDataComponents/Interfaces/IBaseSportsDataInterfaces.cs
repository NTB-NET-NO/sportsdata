// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IBaseSportsDataInterfaces.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System.Xml;
using NTB.SportsData.Service.Domain.Enums;

namespace NTB.SportsData.Service.Components.Interfaces
{
    /// <summary>
    /// The BaseSportsDataInterfaces interface.
    /// </summary>
    public interface IBaseSportsDataInterfaces
    {
        /// <summary>
        /// Gets a value indicating whether the Enabled status of the instance.
        /// </summary>
        /// <value>The Enabled status.</value>
        bool Enabled { get; }

        /// <summary>
        /// Gets or sets a value indicating whether database populated.
        /// </summary>
        bool DatabasePopulated { get; set; }

        /// <summary>
        /// Gets the name of the instance.
        /// </summary>
        /// <value>The name of the instance.</value>
        string InstanceName { get; }

        /// <summary>
        /// Gets the operation mode.
        /// </summary>
        /// <value>The operation mode.</value>
        OperationMode OperationMode { get; }

        /// <summary>
        /// Gets the poll style.
        /// </summary>
        /// <value>The poll style.</value>
        PollStyle PollStyle { get; }

        /// <summary>
        /// Gets the ComponentState
        /// </summary>
        ComponentState ComponentState { get; }

        /// <summary>
        /// Gets the maintenance mode.
        /// </summary>
        /// <value>The operation mode.</value>
        MaintenanceMode MaintenanceMode { get; }

        /// <summary>
        /// Configure the component instance with all necessary data
        /// </summary>
        /// <param name="configNode">
        /// XmlNode that contains the configuration information
        /// </param>
        void Configure(XmlNode configNode);

        /// <summary>
        /// Starts the polling for this instance
        /// </summary>
        void Start();

        /// <summary>
        /// Stops the polling for this instance
        /// </summary>
        void Stop();
    }
}