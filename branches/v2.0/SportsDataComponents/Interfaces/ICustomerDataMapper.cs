﻿using System.Collections.Generic;
using NTB.SportsData.Service.Domain.Classes;

namespace NTB.SportsData.Service.Components.Interfaces
{
    public interface ICustomerDataMapper
    {
        Customer[] GetListOfCustomersByMatchId(int matchId);
        List<Customer> GetListOfCustomersByMatch(Match match);
        List<Customer> GetListOfCustomersByTournament(Tournament tournament);
    }
}
