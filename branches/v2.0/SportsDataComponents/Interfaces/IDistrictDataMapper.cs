﻿using System.Collections.Generic;
using NTB.SportsData.Service.Domain.Classes;

namespace NTB.SportsData.Service.Components.Interfaces
{
    interface IDistrictDataMapper
    {
        bool CheckDistrictsTable(List<District> districts, int sportId);
        List<District> CheckDistrictsTableByDistrict(List<District> districts, int sportId);
    }
}
