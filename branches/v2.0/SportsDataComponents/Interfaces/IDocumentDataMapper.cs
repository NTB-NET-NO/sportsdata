﻿using NTB.SportsData.Service.Domain.Classes;

namespace NTB.SportsData.Service.Components.Interfaces
{
    interface IDocumentDataMapper
    {
        int DeleteDocuments();
        Filename GetDocumentByTournament(Tournament tournament);
    }
}
