﻿using System.Collections.Generic;
using NTB.SportsData.Service.Domain.Classes;

namespace NTB.SportsData.Service.Components.Interfaces
{
    interface IAgeCategoryDataMapper
    {
        List<AgeCategory> CheckAgeCategories(List<AgeCategory> ageCategories, int sportId);
    }
}
