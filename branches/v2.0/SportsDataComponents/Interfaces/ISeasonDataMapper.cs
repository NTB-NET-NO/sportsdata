﻿using System.Collections.Generic;
using NTB.SportsData.Service.Domain.Classes;

namespace NTB.SportsData.Service.Components.Interfaces
{
    interface ISeasonDataMapper
    {
        List<Season> CheckSeasons(List<Season> seasons);
    }
}
