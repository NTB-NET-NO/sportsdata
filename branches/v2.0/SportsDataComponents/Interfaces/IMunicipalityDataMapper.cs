﻿using System.Collections.Generic;
using NTB.SportsData.Service.Domain.Classes;

namespace NTB.SportsData.Service.Components.Interfaces
{
    interface IMunicipalityDataMapper
    {
        bool CheckMunicipalityTable(int sportId);

        List<Municipality> CheckMunicipalityTableAgainstMunicipalities(List<Municipality> municipalities);
    }
}
