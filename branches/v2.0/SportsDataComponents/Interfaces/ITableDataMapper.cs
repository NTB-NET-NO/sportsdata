﻿using System;

namespace NTB.SportsData.Service.Components.Interfaces
{
    internal interface ITableDataMapper
    {
        DateTime? GetTableCheck();
        void SetTableCheck();
    }
}