﻿using System;
using System.Collections.Generic;
using NTB.SportsData.Service.Domain.Classes;

namespace NTB.SportsData.Service.Components.Interfaces
{
    interface IMatchDataMapper
    {
        List<int> FindMissingMatches();
        List<int> FindAllMissingMatches();
        void UpdateMatchNotFound(Match match, Tournament tournament);
        void InsertMatchNotFound(Match match, Tournament tournament);
        int GetNumberOfMatches();
        bool FindMatchByMatch(Match match);
        List<Match> CheckMatches(List<Match> matches);
        List<Match> GetMatchesByDate(DateTime matchDateTime, int sportId);
    }
}
