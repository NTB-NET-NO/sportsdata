﻿using System.Collections.Generic;
using System.Xml;
using NTB.SportsData.Service.Domain.Classes;


namespace NTB.SportsData.Service.Components.Interfaces
{
    interface IXmlDocumentStructureCreator
    {
        XmlDocument CreateCustomerStructure(List<Customer> customers);
        XmlDocument CreateTournamentStructure(Tournament tournament);
        XmlDocument CreateTournamentMetaInfoStructure(Tournament tournament);
        XmlDocument CreateDistrictElement(District district);
        XmlDocument CreateMatchStructure(List<Match> matches, bool renderResult);
        XmlDocument CreateStandingStructure(List<TournamentTableTeam> standings);
        XmlDocument CreateSportStructure(Organization organization);
        XmlDocument CreateAgeCategoryStructure(Tournament tournament, AgeCategory ageCategory);
    }
}
