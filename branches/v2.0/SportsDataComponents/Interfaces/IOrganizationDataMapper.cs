﻿using NTB.SportsData.Service.Domain.Classes;

namespace NTB.SportsData.Service.Components.Interfaces
{
    interface IOrganizationDataMapper
    {
        Organization GetOrgIdBySportId(int sportId);
    }
}
