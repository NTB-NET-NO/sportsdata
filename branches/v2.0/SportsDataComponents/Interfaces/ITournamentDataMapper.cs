﻿using System.Collections.Generic;
using NTB.SportsData.Service.Domain.Classes;

namespace NTB.SportsData.Service.Components.Interfaces
{
    interface ITournamentDataMapper
    {
        Tournament GetTournamentByMatchId(int matchId);
        bool CheckTournamentById(int tournamentId, int sportId);
        bool CheckTournamentTable(List<Tournament> tournaments, int sportId);
    }
}
