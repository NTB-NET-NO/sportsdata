﻿using System;
using System.Collections.Generic;
using NTB.SportsData.Service.Domain.Classes;
using NTB.SportsData.Service.Domain.Enums;

namespace NTB.SportsData.Service.Components.Interfaces
{
    interface IDataFileCreator
    {
        bool Configure(int sportId);
        List<Match> CreateTotalSchedule(int sportId, List<Season> seasons);
        void CreateDataFile(List<Customer> customers, int eventId, DateTime startDateTime, DateTime endDateTime, int sportId);
        void CreateDataFileMaintenance(int eventId, int sportId);
        AgeCategory GetAgeCategoryByTournament(Tournament tournament, int sportId);
        List<Match> GetMatchesByDate(DateTime matchDateTime, int sportId);
        List<Match> GetMatchesByTournament(int tournamentId, DateTime tournamentDate);

        District GetDistrict(int districtId);
        Tournament GetTournamentById(int tournamentId, int sportId);

        List<TournamentTableTeam> GetTournamentStanding(Tournament tournament, int sportId);

        MessageType CreateDocumentType(bool renderResult, bool standing);

    }
}
