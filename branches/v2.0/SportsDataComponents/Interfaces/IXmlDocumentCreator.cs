﻿using System;
using System.Collections.Generic;
using System.Xml;
using NTB.SportsData.Service.Domain.Classes;
using NTB.SportsData.Service.Domain.Enums;

namespace NTB.SportsData.Service.Components.Interfaces
{
    interface IXmlDocumentCreator
    {
        // void WriteXmlDocument(XmlDocument xmlDocument, string eventName, int eventId, List<Filename> filenames, int? districtId);
        
        MessageType CreateDocumentType(bool renderResult);
        Filename CreateFileName(bool renderResult, DateTime? dateTime, int districtId, int tournamentId, Organization organization);
        XmlDocument CreateCompleteDocumentStructure();
        void WriteXmlDocument(XmlDocument xmlDocument, Filename filename);

    }
}
