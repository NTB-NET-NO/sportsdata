﻿using System;
using System.Collections.Generic;
using NTB.SportsData.Service.Domain.Classes;

namespace NTB.SportsData.Service.Components.Interfaces
{
    interface ITeamSportDataFileCreator
    {
        bool Configure();
        void CreateTotalSchedule();
        void CreateDataFile(List<Customer> customers, int eventId, DateTime startDateTime, DateTime endDateTime);
        void CreateDataFileMaintenance(int eventId);

        List<Match> GetCustomerMatches(int customerId, int remoteCustomerId, int tournamentId, DateTime selectedStartDate,
                                DateTime selectedEndDate);

        AgeCategory GetAgeCategoryByTournamentId(int tournamentId);
    }
}
