﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsData.Service.Domain.Classes;

namespace NTB.SportsData.Service.Components.Interfaces
{
    interface IDataFileConfigurator
    {
        void ConfigureAgeCategory(int sportId);
        void ConfigureSeason(int sportId);
        void ConfigureMunicipalities(int sportId);
        void ConfigureDistricts(int sportId);
        void ConfigureTournaments(District district, int sportId);
        void ConfigureMatches(District district, int sportId);
    }
}
