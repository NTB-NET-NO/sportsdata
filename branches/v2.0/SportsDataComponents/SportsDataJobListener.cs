﻿using System;
using NTB.SportsData.Service.Components.Nff;
using Quartz;
using log4net;
// Using Quartz.net

namespace NTB.SportsData.Service.Components
{
    class SportsDataJobListener : IJobListener
    {
        static ILog logger = null;

        protected Boolean jobBeingExecuted = false;
        private string _Name = "";

        static SportsDataJobListener()
        {
            //Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!log4net.LogManager.GetRepository().Configured)
                log4net.Config.BasicConfigurator.Configure();

            logger = LogManager.GetLogger(typeof(NFFSportsDataJobListener));

            logger.Debug("SportsDataJobListener is created");
            
        }

        

        

        //public void JobToBeExecuted(JobExecutionContext context)
        //{
        //    logger.Debug("Setting jobBeingExecuted to true!");
        //    jobBeingExecuted = true;

        //    // IScheduler sched = context.Scheduler;

        //}

        
        public string Name
        {
            get
            {
                return _Name;
            }

            set
            {
                _Name = value;
            }
        }

       

        public void JobToBeExecuted(IJobExecutionContext context)
        {
            logger.Debug("Setting jobBeingExecuted to true!");
            jobBeingExecuted = true;


            // throw new NotImplementedException();
        }

        public void JobWasExecuted(IJobExecutionContext context, JobExecutionException jobException)
        {
            logger.Debug("Setting jobBeingExecuted to false!");
            jobBeingExecuted = false;


            // throw new NotImplementedException();
        }

        public void JobExecutionVetoed(IJobExecutionContext context)
        {
            throw new NotImplementedException();
        }
    }
}
