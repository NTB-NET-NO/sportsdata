﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PurgeComponent.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ServiceModel;
using System.Threading;
using System.Timers;
using System.Xml;
using NTB.SportsData.Service.Components.Interfaces;
using NTB.SportsData.Service.Domain.Enums;
using log4net;
using Quartz;
using Quartz.Impl;
using Quartz.Impl.Matchers;

namespace NTB.SportsData.Service.Components
{
    // Logging
        // Quartz support
    
    /// <summary>
    /// The purge component.
    /// </summary>
    public partial class PurgeComponent : Component, IBaseSportsDataInterfaces
    {    
        /// <summary>
        /// schedulerFactory to be used to create scheduled tasks
        /// </summary>
        protected StdSchedulerFactory SchedulerFactory = new StdSchedulerFactory();

        /// <summary>
        /// The scheduler.
        /// </summary>
        protected IScheduler Scheduler;

        /// <summary>
        /// The Configured interval for this instance, used for Continous polling
        /// </summary>
        /// <remarks>
        ///   <para>Indicates the interval time in seconds for <c>Continous</c>polling. 60 means that the job runs every minute.</para>
        ///   <para>Default value: <c>60</c></para>
        /// </remarks>
        protected int Interval = 60;

        /// <summary>
        /// The Configured job name for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="InstanceName"/></remarks>
        protected string Name;

        /// <summary>
        /// Flag to indicate sucessful configure. Instance will not start polling nor handle messages if not properly Configured
        /// </summary>
        /// <remarks>Holds the Configured status of the instance. <c>True</c> means successfully Configured</remarks>
        protected bool Configured = false;

        /// <summary>
        /// Error-Retry flag
        /// </summary>
        /// <remarks>The flag is being set if a network or EWS error is encountered. Current operations are being aborted for later retry.</remarks>
        protected bool ErrorRetry = false;

        /// <summary>
        /// Send Email-notifications when a new messages is processed
        /// </summary>
        /// <remarks>Set to an email address. Multiple adresses are supported, separate with <c>;</c></remarks>
        protected string EmailNotification;

        /// <summary>
        /// Subject for email notifications
        /// </summary>
        /// <remarks>The subject is built during processing and used when sending the email when processing completes</remarks>
        protected string EmailSubject;

        /// <summary>
        /// Body for email notifications
        /// </summary>
        /// <remarks>The body is built during processing and used when sending the email when processing completes</remarks>
        protected string EmailBody;

        /// <summary>
        /// The schedule type.
        /// </summary>
        protected ScheduleType ScheduleType;

        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(PurgeComponent));

        /// <summary>
        /// Busy status event
        /// </summary>
        private readonly AutoResetEvent _busyEvent = new AutoResetEvent(true);

        /// <summary>
        /// Exit control event
        /// </summary>
        private readonly AutoResetEvent _stopEvent = new AutoResetEvent(false);
        
        /// <summary>
        /// The instance context.
        /// </summary>
        private InstanceContext instanceContext;

        /// <summary>
        /// Initializes static members of the <see cref="PurgeComponent"/> class. 
        /// </summary>
        static PurgeComponent()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }
        
        #region Common class variables

        #endregion

        #region QuartzListeners

        /// <summary>
        /// The daily job listnerer.
        /// </summary>
        private SportsDataJobListener dailyJobListnerer = new SportsDataJobListener();

        /// <summary>
        /// The weekly job listener.
        /// </summary>
        private SportsDataJobListener weeklyJobListener = new SportsDataJobListener();

        /// <summary>
        /// The biweekly job listener.
        /// </summary>
        private SportsDataJobListener biweeklyJobListener = new SportsDataJobListener();

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PurgeComponent"/> class.
        /// </summary>
        public PurgeComponent()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PurgeComponent"/> class.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        public PurgeComponent(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        /// <summary>
        /// The configure.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        /// <exception cref="ArgumentException">
        /// </exception>
        /// <exception cref="NotSupportedException">
        /// </exception>
        public void Configure(XmlNode configNode)
        {
            Logger.Debug("Node: " + configNode.Name);
            if (configNode.Attributes != null)
            {
                Logger.Debug("Name attribut: " + configNode.Attributes.GetNamedItem("Name"));
            }

            // Some values that we need.

            // This boolean variable is used to tell us that the database shall be populated or not
            bool Purge = false;

            // This string is used to set the name of the job
            string triggerGroupName = string.Empty;

            // Basic configuration sanity check
            if (configNode == null || configNode.Name != "PurgeComponent"
                || configNode.Attributes.GetNamedItem("Name") == null)
            {
                throw new ArgumentException("The XML configuration node passed is invalid", "configNode");
            }

            if (Thread.CurrentThread.Name == null)
            {
                Thread.CurrentThread.Name = configNode.Attributes.GetNamedItem("Name").ToString();
            }

            // Getting the name of this Component instance
            try
            {
                Name = configNode.Attributes["Name"].Value;
                enabled = Convert.ToBoolean(configNode.Attributes["Enabled"].Value);
                log4net.ThreadContext.Stacks["NDC"].Push(InstanceName);

                // This value is used to tell that we shall insert into database... I just wonder if we shall do it
                // on the schedule-level... Well, we try here first
                // TODO: Consider using schedule-level to decide if we are to insert into database or not
                if (configNode.Attributes["InsertDatabase"] != null)
                {
                    DatabasePopulated = Convert.ToBoolean(configNode.Attributes["InsertDatabase"].Value);
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to configure this job instance!", ex);
            }

            // Basic operation
            try
            {
                operationMode =
                    (OperationMode)Enum.Parse(typeof(OperationMode), configNode.Attributes["OperationMode"].Value, true);
            }
            catch (Exception ex)
            {
                log4net.ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing OperationMode values in XML configuration node", ex);
            }

            try
            {
                pollStyle = (PollStyle)Enum.Parse(typeof(PollStyle), configNode.Attributes["PollStyle"].Value, true);
            }
            catch (Exception ex)
            {
                log4net.ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing PollStyle values in XML configuration node", ex);
            }

            try
            {
                this.ScheduleType =
                    (ScheduleType)Enum.Parse(typeof(ScheduleType), configNode.Attributes["ScheduleType"].Value, true);
                    
                    // ScheduleType = (ScheduleT
            }
            catch (Exception ex)
            {
                log4net.ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing ScheduleType values in XML configuration node", ex);
            }

            if (enabled)
            {
                // Creating Instance Name
                triggerGroupName = InstanceName;

                #region Set up polling

                try
                {
                    // Switch on pollstyle
                    switch (pollStyle)
                    {
                            #region PollStyle.CalendarPoll

                        case PollStyle.CalendarPoll:

                            // Getting the times for this job
                            try
                            {
                                IJobDetail dailyJobDetail = null;
                                IJobDetail weeklyJobDetail = null;

                                // = JobBuilder.Create<TournamentMatches>().WithIdentity("Job1", "Group1").Build();
                                ITrigger dailyCronTrigger = null;
                                ITrigger weeklyTrigger = null;

                                Logger.Info("Setting up schedulers");
                                Logger.Info("--------------------------------------------------------------");

                                // Getting the number of jobs to create
                                XmlNodeList nodeList =
                                    configNode.SelectNodes(
                                        "../PurgeComponent[@Name='" + InstanceName + "']/Schedules/Schedule");

                                int jobs = nodeList.Count;
                                Logger.Debug("Number of jobs: " + nodeList.Count.ToString());

                                // Creating a string to store the ScheduleID
                                string ScheduleID = string.Empty;

                                // Creating a boolean variable to store if we are to get results out or not
                                bool Results = false;

                                // Creating an integer value to be used to tell the duration of the gathering of data
                                int Duration = 0;

                                // Creating a boolean variable to store if this is a schedule-message og not
                                bool ScheduleMessage = false;

                                Logger.Debug("Schedule of type: " + ScheduleType.ToString());

                                // Getting the scheduler
                                var scheduler = this.SchedulerFactory.GetScheduler();

                                switch (ScheduleType)
                                {
                                    case ScheduleType.Daily:
                                        foreach (XmlNode node in nodeList)
                                        {
                                            string stringCronExpression = string.Empty;
                                            ScheduleID = node.Attributes["Id"].Value;
                                            DateTime scheduleTime = DateTime.Parse(node.Attributes["Time"].Value);

                                            // Splitting the time into two values so that we can create a cron job
                                            string[] scheduleTimeArray =
                                                node.Attributes["Time"].Value.Split(new[] { ':' });

                                            // If minutes contains two zeros (00), we change it to one zero (0) 
                                            // If not, scheduler won't understand
                                            if (scheduleTimeArray[1].ToString() == "00")
                                            {
                                                scheduleTimeArray[1] = "0";
                                            }

                                            // Doing the same thing for hours
                                            if (scheduleTimeArray[0].ToString() == "00")
                                            {
                                                scheduleTimeArray[0] = "0";
                                            }

                                            // Checking if there is a setting for Duration
                                            if (node.Attributes["Duration"] != null)
                                            {
                                                Duration = Convert.ToInt32(node.Attributes["Duration"].Value);
                                            }

                                            /*
                                             * Creating cron expressions and more. 
                                             * This is the cron syntax:
                                             *  Seconds
                                             *  Minutes
                                             *  Hours
                                             *  Day-of-Month
                                             *  Month
                                             *  Day-of-Week
                                             *  Year (optional field)
                                             */

                                            // Creating the daily cronexpression
                                            stringCronExpression = "0 " + scheduleTimeArray[1].ToString() + " "
                                                                   + scheduleTimeArray[0].ToString() + " " + "? " + "* "
                                                                   + "* ";

                                            // Setting up the CronTrigger
                                            Logger.Debug(
                                                "Setting up the CronTrigger with following pattern: "
                                                + stringCronExpression);

                                            // Setting up the Daily CronTrigger
                                            CronExpression cronExpression = new CronExpression(stringCronExpression);

                                            // dailyCronTrigger = new CronTrigger(InstanceName + ScheduleID, InstanceName);
                                            // dailyCronTrigger.CronExpression = cronExpression;
                                            dailyCronTrigger =
                                                TriggerBuilder.Create()
                                                              .WithIdentity(InstanceName + "_" + ScheduleID, "Group1")
                                                              .WithDescription(InstanceName.ToString())
                                                              .WithCronSchedule(stringCronExpression)
                                                              .Build();

                                            Logger.Debug("dailyCronTrigger: " + dailyCronTrigger.Description);

                                            // Creating the jobDetail 
                                            dailyJobDetail =
                                                JobBuilder.Create<PurgeSportsData>()
                                                          .WithIdentity(
                                                              "job_" + InstanceName + "_" + ScheduleID, "group1")
                                                          .WithDescription(InstanceName.ToString())
                                                          .Build();

                                            dailyJobDetail.JobDataMap["InsertDatabase"] = DatabasePopulated;
                                            dailyJobDetail.JobDataMap["ScheduleType"] = this.ScheduleType;
                                            dailyJobDetail.JobDataMap["ScheduleMessage"] = ScheduleMessage;
                                            dailyJobDetail.JobDataMap["Duration"] = Duration;
                                            dailyJobDetail.JobDataMap["Purge"] = Purge;

                                            if (dailyJobDetail != null)
                                            {
                                                // dailyJobDetail.AddJobListener(JobListenerName);
                                                Logger.Debug(
                                                    "Setting up and starting dailyJobDetail job "
                                                    + dailyJobDetail.Description + " using trigger : "
                                                    + dailyCronTrigger.Description);

                                                // scheduler.ScheduleJob(dailyJobDetail, dailyCronTrigger[a]);
                                                scheduler.ScheduleJob(dailyJobDetail, dailyCronTrigger);
                                            }
                                        }

                                        break;

                                    case ScheduleType.Weekly:
                                        foreach (XmlNode node in nodeList)
                                        {
                                            ScheduleID = node.Attributes["Id"].Value;
                                            DateTime scheduleTime = DateTime.Parse(node.Attributes["Time"].Value);

                                            // Splitting the time into two values so that we can create a cron job
                                            string[] scheduleTimeArray =
                                                node.Attributes["Time"].Value.Split(new[] { ':' });

                                            // If minutes contains two zeros (00), we change it to one zero (0) 
                                            // If not, scheduler won't understand
                                            int minutes = 0;
                                            if (scheduleTimeArray[1].ToString() == "00")
                                            {
                                                scheduleTimeArray[1] = "0";
                                            }

                                            minutes = Convert.ToInt32(scheduleTimeArray[1]);

                                            // Doing the same thing for hours
                                            if (scheduleTimeArray[0].ToString() == "00")
                                            {
                                                scheduleTimeArray[0] = "0";
                                            }

                                            int hours = Convert.ToInt32(scheduleTimeArray[0]);

                                            // Checking if there is a setting for results
                                            if (node.Attributes["Results"] != null)
                                            {
                                                Results = Convert.ToBoolean(node.Attributes["Results"].Value);
                                                ScheduleMessage = !Results;
                                            }

                                            // Checking if there is a setting for Duration
                                            if (node.Attributes["Duration"] != null)
                                            {
                                                Duration = Convert.ToInt32(node.Attributes["Duration"].Value);
                                            }

                                            string purgeWeeks = string.Empty;
                                            if (node.Attributes["Week"] != null)
                                            {
                                                purgeWeeks = node.Attributes["Week"].Value;
                                            }

                                            string scheduleDay = string.Empty;
                                            if (node.Attributes["DayOfWeek"] != null)
                                            {
                                                scheduleDay = node.Attributes["DayOfWeek"].Value;
                                            }

                                            // If we use the DateTimeOffset it means that we have to find out if the day has passed or not.
                                            // So we have to find out which day it is today
                                            DayOfWeek today = DateTime.Today.DayOfWeek;
                                            int numDayOfWeek = 0;
                                            switch (today.ToString())
                                            {
                                                case "Monday":
                                                    numDayOfWeek = 1;
                                                    break;
                                                case "Tuesday":
                                                    numDayOfWeek = 2;
                                                    break;

                                                case "Wednesday":
                                                    numDayOfWeek = 3;
                                                    break;

                                                case "Thursday":
                                                    numDayOfWeek = 4;
                                                    break;

                                                case "Friday":
                                                    numDayOfWeek = 5;
                                                    break;

                                                case "Saturday":
                                                    numDayOfWeek = 6;
                                                    break;

                                                case "Sunday":
                                                    numDayOfWeek = 0;
                                                    break;
                                            }

                                            // Checking if numDayOfWeek is smaller than ScheduleDay
                                            DateTimeOffset dto = DateTime.Today;

                                            // Get the scheduled day
                                            int ScheduledDay = Convert.ToInt32(scheduleDay);

                                            int Days = 0;
                                            if (numDayOfWeek <= ScheduledDay)
                                            {
                                                // It is, so we need to find out when the next scheduling should happen
                                                Days = ScheduledDay - numDayOfWeek; // this can be zero

                                                dto = DateTime.Today.AddDays(Days).AddHours(hours).AddMinutes(minutes);

                                                // <Schedule Id="1" Time="15:12" DayOfWeek="1" Week="2"/>
                                            }
                                            else if (numDayOfWeek > ScheduledDay)
                                            {
                                                int weekdays = 7;
                                                int DaysLeft = weekdays - numDayOfWeek;
                                                Days = DaysLeft + ScheduledDay;

                                                dto = DateTime.Today.AddDays(Days).AddHours(hours).AddMinutes(minutes);
                                            }

                                            // Creating a simple Scheduler
                                            CalendarIntervalScheduleBuilder calendarIntervalSchedule =
                                                CalendarIntervalScheduleBuilder.Create();

                                            // Creating the weekly Trigger using the stuff that we've calculated
                                            weeklyTrigger =
                                                TriggerBuilder.Create()
                                                              .WithDescription(InstanceName.ToString())
                                                              .WithIdentity(InstanceName + "_" + ScheduleID, "Group1")
                                                              .StartAt(dto)
                                                              .WithSchedule(
                                                                  calendarIntervalSchedule.WithIntervalInWeeks(
                                                                      Convert.ToInt32(purgeWeeks)))
                                                              .Build();

                                            // This part might be moved
                                            if (operationMode == OperationMode.Purge)
                                            {
                                                weeklyJobDetail =
                                                    JobBuilder.Create<PurgeSportsData>()
                                                              .WithIdentity(
                                                                  "job_" + InstanceName + "_" + ScheduleID, "group1")
                                                              .WithDescription(InstanceName.ToString())
                                                              .Build();
                                            }

                                            weeklyJobDetail.JobDataMap["ScheduleType"] = this.ScheduleType.ToString();
                                            weeklyJobDetail.JobDataMap["Results"] = Results;
                                            weeklyJobDetail.JobDataMap["ScheduleMessage"] = ScheduleMessage;
                                            weeklyJobDetail.JobDataMap["Weeks"] = purgeWeeks;
                                            weeklyJobDetail.JobDataMap["Purge"] = Purge;
                                        }

                                        if (weeklyJobDetail != null)
                                        {
                                            Logger.Debug(
                                                "Setting up and starting weeklyJobDetail job "
                                                + weeklyJobDetail.Description + " using trigger : "
                                                + weeklyTrigger.Description);
                                            scheduler.ScheduleJob(weeklyJobDetail, weeklyTrigger);
                                        }

                                        break;

                                    case ScheduleType.Monthly:

                                        // doing Monthly stuff here
                                        break;

                                    case ScheduleType.Yearly:

                                        // doing yearly Stuff here
                                        break;
                                }
                            }
                            catch (SchedulerConfigException sce)
                            {
                                Logger.Debug("A SchedulerConfigException has occured: ", sce);
                            }
                            catch (SchedulerException se)
                            {
                                Logger.Debug("A schedulerException has occured: ", se);
                            }
                            catch (Exception e)
                            {
                                Logger.Debug("An exception has occiured", e);
                            }

                            break;

                            #endregion

                            #region PollStyle.Scheduled

                        case PollStyle.Scheduled:

                            /* 
                            schedule = configNode.Attributes["Schedule"].Value;
                            TimeSpan s = Utilities.GetScheduleInterval(schedule);
                            logger.DebugFormat("Schedule: {0} Calculated time to next run: {1}", schedule, s);
                            pollTimer.Interval = s.TotalMilliseconds;
                            */
                            throw new NotSupportedException("Invalid polling style for this job type");

                            #endregion

                            #region PollStyle.FileSystemWatch

                        case PollStyle.FileSystemWatch:
                            throw new NotSupportedException("Invalid polling style for this job type");

                            #endregion

                        default:

                            // Unsupported pollstyle for this object
                            throw new NotSupportedException(
                                "Invalid polling style for this job type (" + pollStyle + ")");
                    }
                }
                catch (Exception ex)
                {
                    log4net.ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException(
                        "Invalid or missing PollStyle-specific values in XML configuration node: " + ex.Message, ex);
                }

                #endregion
            }

            // Finish configuration
            log4net.ThreadContext.Stacks["NDC"].Pop();
            Configured = true;
        }

        /// <summary>
        /// The start.
        /// </summary>
        /// <exception cref="SportsDataException">
        /// </exception>
        public void Start()
        {
            if (!Configured)
            {
                throw new SportsDataException(
                    "PurgeComponent is not properly Configured. PurgeComponent::Start() Aborted!");
            }

            if (!enabled)
            {
                throw new SportsDataException("PurgeComponent is not Enabled. PurgeComponent::Start() Aborted!");
            }

            try
            {
                if (pollStyle == PollStyle.CalendarPoll)
                {
                    Logger.Info("Setting up jobListener and scheduler");

                    // starting the scheduler
                    IJobListener jobListener = new SportsDataJobListener();

                    var scheduler = this.SchedulerFactory.GetScheduler();

                    // This is where we shall call the jobListener and (if necessary) triggerListnerer
                    scheduler.Start();
                }
            }
            catch (SchedulerException se)
            {
                Logger.Debug(se);
            }

            _stopEvent.Reset();
            _busyEvent.Set();

            // Starting poll-timer
            // pollTimer.Interval = 2000;
            pollTimer.Start();
        }

        /// <summary>
        /// The stop.
        /// </summary>
        /// <exception cref="SportsDataException">
        /// </exception>
        public void Stop()
        {
            if (!Configured)
            {
                throw new SportsDataException("PurgeComponent is not properly Configured. NFFComponent::Stop() Aborted");
            }

            if (!enabled)
            {
                throw new SportsDataException("PurgeComponent is not properly Configured. NFFComponent::Stop() Aborted");
            }

            

            if (pollTimer.Enabled == false)
            {
                Logger.Debug("pollTimer is disabled");
            }
            else
            {
                Logger.Debug("pollTimer is Enabled");
            }

            if (pollTimer.Interval == 5000)
            {
                Logger.Debug("pollTimer has an interval of 5000 milliseconds");
            }
            else
            {
                Logger.Debug("pollTimer DOES NOT have an interval of 5000 milliseconds");
            }

            if (ErrorRetry)
            {
                Logger.Debug("We are in ErrorRetry mode!");
            }
            else
            {
                Logger.Debug("We are NOT in ErrorRetry mode!");
            }

            if (pollStyle == PollStyle.FileSystemWatch)
            {
                Logger.Debug("pollStyle is of style FileSystemWatch");
            }
            else
            {
                Logger.Debug("pollStyle is NOT of style FileSystemWatch");
            }

            

            #region Stopping Scheduler

            // Check status - Handle busy polltimer loops
            if (pollStyle == PollStyle.CalendarPoll)
            {
                try
                {
                    Logger.Debug("Deleting / stopping jobs!");
                    ISchedulerFactory sf = new StdSchedulerFactory();
                    IScheduler sched = sf.GetScheduler();

                    // Shutting down scheduler
                    IList<string> groups = sched.GetJobGroupNames();

                    List<JobKey> keys =
                        new List<JobKey>(sched.GetJobKeys(GroupMatcher<JobKey>.GroupEquals(InstanceName)));

                    // Line below marked out as it gives us less information
                    // sched.DeleteJobs(new List<JobKey>(keys));

                    // Looping through each key as we'd like to know when things go right and not!
                    keys.ForEach(
                        key =>
                            {
                                IJobDetail detail = sched.GetJobDetail(key);
                                sched.DeleteJob(key);

                                Logger.Debug(
                                    "Shutting down: " + detail.Description + "(" + key.Name + ")" + " Group: "
                                    + key.Group);
                            });

                    sched.Shutdown();
                }
                catch (SchedulerException sex)
                {
                    Logger.Fatal(sex);
                }
                catch (Exception ex)
                {
                    Logger.Error("Problems closing NFF WebClients!", ex);
                }
            }

            #endregion

            if (!pollTimer.Enabled
                && (ErrorRetry || pollStyle != PollStyle.FileSystemWatch || pollTimer.Interval == 5000))
            {
                Logger.InfoFormat("Waiting for instance to complete work. Job {0}", InstanceName);
            }

            // Signal Stop
            _stopEvent.Set();

            if (!_busyEvent.WaitOne(30000))
            {
                Logger.InfoFormat(
                    "Instance did not complete properly. Data may be inconsistent. Job: {0}", InstanceName);
            }

            // Kill polling
            pollTimer.Stop();
        }

        /// <summary>
        /// Gets the Enabled status of the instance.
        /// </summary>
        /// <value>The Enabled status.</value>
        /// <remarks><c>True</c> if the job instance is Enabled.</remarks>
        public bool Enabled
        {
            get
            {
                return enabled;
            }
        }

        /// <summary>
        /// Gets the name of the instance.
        /// </summary>
        /// <value>The name of the instance.</value>
        /// <remarks>The Configured instance job name.</remarks>
        public string InstanceName
        {
            get
            {
                return Name;
            }
        }

        /// <summary>
        /// Gets the operation mode. 
        /// </summary>
        /// <value>The operation mode.</value>
        /// <remarks><c>Gatherer</c> is the only valid mode, its hard coded for this component.</remarks>
        public OperationMode OperationMode
        {
            get
            {
                return OperationMode.Purge;
            }
        }

        /// <summary>
        /// Gets the poll style.
        /// </summary>
        /// <value>The poll style.</value>
        /// <remarks>Contionous, Scheduled and FileSystemWatch are valid for <c>Gatherer</c> objects.</remarks>
        public PollStyle PollStyle
        {
            get
            {
                return pollStyle;
            }
        }

        /// <summary>
        /// The poll timer_ elapsed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void pollTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            /*
             * We can consider adding getting data here if the time (day + hour + minute) hits. Otherwize we shall do nothing.
             * 
             */
            log4net.ThreadContext.Properties["JOBNAME"] = InstanceName;
            Logger.DebugFormat("PurgeComponent::pollTimer_Elapsed() hit");

            // _busyEvent.WaitOne();
            this.pollTimer.Stop();

            switch (this.pollStyle)
            {
                    // For continuous and scheduled do a simple folder item traversal
                case PollStyle.Scheduled:
                case PollStyle.Continous:
                    if (operationMode == OperationMode.Distributor)
                    {
                        // this.foo();
                    }

                    this.pollTimer.Start();

                    break;

                case PollStyle.CalendarPoll:

                    // @todo: need to do some work here
                    Logger.Debug("Disabling pollTimer");
                    this.pollTimer.Enabled = false;

                    // _busyEvent.WaitOne();
                    break;
            }
        }

        /// <summary>
        /// Gets or sets the component state.
        /// </summary>
        public ComponentState ComponentState { get; set; }

        /// <summary>
        /// Gets or sets the maintenance mode.
        /// </summary>
        public MaintenanceMode MaintenanceMode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether database populated.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public bool DatabasePopulated
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }
    }
}