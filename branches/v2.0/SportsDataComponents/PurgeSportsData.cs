﻿using System;
using System.Data.SqlClient;
using System.Configuration;
using NTB.SportsData.Service.Domain.Enums;
using Quartz;
using log4net;
// Adding support for Quartz Scheduler
// Adding support for log4net

namespace NTB.SportsData.Service.Components
{
    class PurgeSportsData: IJob
    {
        /// <summary>
        /// Static logger
        /// </summary>
        static readonly ILog Logger = LogManager.GetLogger(typeof(PurgeSportsData));

        static PurgeSportsData()
        {
            //Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
                log4net.Config.BasicConfigurator.Configure();
        }

        // Database variables
        private SqlConnection _sqlconnection;

        public void Execute(IJobExecutionContext context)
        {
            Logger.Info("Executing JobExecutionContext" + context.JobDetail.Description);

            // Mapping information sent from Component
            JobDataMap dataMap = context.JobDetail.JobDataMap;

            
            /*
            dataMap.GetBoolean("InsertDatabase");

            dataMap.GetBoolean("Results");

            dataMap.GetBoolean("ScheduleMessage");

            dataMap.GetInt("Duration");

            dataMap.GetBoolean("Purge");
            */


            ScheduleType scheduleType = (ScheduleType)Enum.Parse(typeof(ScheduleType), dataMap.GetString("ScheduleType"), true);

            switch (scheduleType)
            {
                case ScheduleType.Daily:
                    break;
                case ScheduleType.Weekly:
                    throw new Exception("Cannot purge data. No week information has been set in order to do purge!");
            }

            try
            {
                string connectionString = ConfigurationManager.AppSettings["ConnectionString"];
                _sqlconnection = new SqlConnection(connectionString);

                // Open the connection to the database
                _sqlconnection.Open();

                /*
                if (ScheduleType.Weekly == scheduleType)
                {
                    sqlQuery = "DELETE FROM Matches WHERE Downloaded=1 AND Date < '" + purgeDateEnd + "';";
                    sqlQuery += "DELETE FROM Document WHERE (DocumentCreated < ' " + purgeDateEnd + "';";
                }
                else if (ScheduleType.Daily == scheduleType)
                {
                    sqlQuery = "DELETE FROM Matches WHERE Downloaded=1 AND Date = '" + purgeDateEnd + "';";
                    sqlQuery += "DELETE FROM Document WHERE (DocumentCreated = ' " + purgeDateEnd + "';";
                }

                // DELETE FROM Document";

                
                SqlCommand command = new SqlCommand {Connection = _sqlconnection, CommandText = sqlQuery};

                SqlDataReader sqlreader = command.ExecuteReader();
                */
                _sqlconnection.Close();
                 
            }
            catch (SqlException sqlexception)
            {
                Logger.Error("A SQL-Exception has occured while purging data!", sqlexception);
            }
            catch (Exception exception)
            {
                Logger.Error("An exception has occured while purging data!", exception);
            }
            
        }
    }
}
