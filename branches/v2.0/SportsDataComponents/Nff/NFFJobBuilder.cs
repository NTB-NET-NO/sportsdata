// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NFFJobBuilder.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The nff job builder.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using NTB.SportsData.Service.Components.TeamSport;
using NTB.SportsData.Service.Domain.Classes;
using NTB.SportsData.Service.Domain.Enums;
using log4net;
using Quartz;

namespace NTB.SportsData.Service.Components.Nff
{
    // Adding support for log4net
        // Adding support for Quartz Scheduler

    /// <summary>
    /// The nff job builder.
    /// </summary>
    internal class NffJobBuilder : IJob
    {
        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(NffJobBuilder));

        /// <summary>
        /// Initializes static members of the <see cref="NffJobBuilder"/> class.
        /// </summary>
        static NffJobBuilder()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        // This class shall be used to get the matches from Norwegian Soccer Federation

        /// <summary>
        /// The execute.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public void Execute(IJobExecutionContext context)
        {
            Logger.Info("Executing JobExecutionContext" + context.JobDetail.Description);
            string outputFolder = string.Empty;

            string jobName = string.Empty;
            
            string operationMode = string.Empty;

            bool insertDatabase = false;
            
            bool results = false;
            
            DateTime dateOffset = DateTime.Now;
            
            int duration = 0;

            bool totalSchedule = false;

            int sportId = 0;

            // Mapping information sent from Component
            JobDataMap dataMap = context.JobDetail.JobDataMap;

            DateTime eventStartDate = new DateTime();

            // Populate variables to be used to 
            try
            {
                // Cleaning up the code a bit. having all variable implementations here
                outputFolder = dataMap.GetString("OutputFolder");

                jobName = dataMap.GetString("JobName");
                
                operationMode = dataMap.GetString("OperationMode");
                
                insertDatabase = dataMap.GetBoolean("InsertDatabase");
                
                string scheduleType = dataMap.GetString("ScheduleType");
                
                results = dataMap.GetBoolean("Results");
                
                bool scheduleMessage = dataMap.GetBoolean("ScheduleMessage");
                
                duration = dataMap.GetInt("Duration");
                
                bool createXml = dataMap.GetBoolean("CreateXML");

                if (dataMap.Keys.Contains("DateOffset"))
                {
                    dateOffset = dataMap.GetDateTime("DateOffset");
                }

                if (dataMap.Keys.Contains("TotalSchedule"))
                {
                    totalSchedule = dataMap.GetBoolean("TotalSchedule");
                }

                if (dataMap.Keys.Contains("SportId"))
                {
                    sportId = dataMap.GetInt("SportId");
                }

                

                // And all logging here
                Logger.Debug("jobName: " + jobName);
                Logger.Debug("OutputFolder: " + outputFolder);
                Logger.Debug("OperationMode: " + operationMode);
                Logger.Debug(insertDatabase ? "InsertDatabase: true" : "InsertDatabase: false");
                Logger.Debug("ScheduleType: " + scheduleType);
                Logger.Debug(results ? "Results: true" : "Results: false");
                Logger.Debug(scheduleMessage ? "ScheduleMessage: true" : "ScheduleMessage: false");
                Logger.Debug("Duration: " + duration);
                Logger.Debug(createXml ? "CreateXML: true" : "CreateXML: false");
                Logger.Debug("DateOffset: " + dateOffset.ToShortDateString());
            }
            catch (Exception exception)
            {
                Logger.Debug(exception);
                Logger.Debug(exception);
            }

            if (operationMode.ToLower() == "maintenance")
            {
                NffMaintenance maintentance = new NffMaintenance(MaintenanceMode.CheckAllMatches);
                maintentance.CheckAllMissingMatches();
            }
            else
            {
                // Logging to make sure that we have it all
                Logger.Debug("DateOffset in TournamentMatches: " + dateOffset);

                // Creating the DataFileCreator object
                DataFileCreator dataFileCreator = new DataFileCreator
                        {
                            OutputFolder = outputFolder, 
                            JobName = jobName,
                            SportId = sportId
                        };

                Logger.Debug("Outputfolder: " + outputFolder);

                if (insertDatabase)
                {
                    Logger.Debug("We shall insert data in the database");
                    dataFileCreator.UpdateDatabase = true;
                }

                // This parameter tells us if we are to add results-tags to the XML
                if (results)
                {
                    Logger.Debug("We shall also render result");
                    dataFileCreator.RenderResult = true;
                }

                Logger.Debug("Duration: " + duration);
                DateTime eventEndDate;
                if (duration > 0)
                {
                    Logger.Debug("We shall create a longer period of matches");
                    

                    // Creating the date start
                    // dataFileCreator.DateStart = DateTime.Today;

                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                    {
                        eventStartDate = DateTime.Parse("2013-03-31");
                    }

                    eventEndDate = eventStartDate.AddDays(7);
                }
                else
                {
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                    {
                        eventStartDate = DateTime.Parse("2012-03-30");
                        eventEndDate = DateTime.Parse("2012-03-31");
                    }
                    else
                    {
                        eventStartDate = DateTime.Today;
                        eventEndDate = DateTime.Today;

                        if (dateOffset != DateTime.Today)
                        {
                            eventStartDate = dateOffset;
                            eventEndDate = dateOffset;
                        }

                        Logger.Debug("Date Start: " + eventStartDate);
                        Logger.Debug("Date End: " + eventEndDate);
                    }
                }
                List<Customer> customers = new List<Customer>();
                const int eventId = 1;
                
                // Start getting the data
                if (totalSchedule == false)
                {
                    dataFileCreator.CreateDataFile(customers, eventId, eventStartDate, eventEndDate, sportId);    
                    //dataFileCreator.CreateDataFile(); // .CreateDataFileByDistrict();
                }
                else
                {
                    List<Season> seasons = new List<Season>();
                    dataFileCreator.CreateTotalSchedule(sportId, seasons);
                    // dataFileCreator.CreateTotalSchedule();
                }
            }
        }
    }
}