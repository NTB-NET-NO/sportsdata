﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NffMaintenance.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using NTB.SportsData.Service.Components.Repositories;
using NTB.SportsData.Service.Components.TeamSport;
using NTB.SportsData.Service.Domain.Classes;
using NTB.SportsData.Service.Domain.Enums;
using log4net;

namespace NTB.SportsData.Service.Components.Nff
{
    // Adding support for log4net

    /// <summary>
    /// This class is used to do the maintenance that is needed from time to time
    /// </summary>
    public class NffMaintenance
    {
        /// <summary>
        /// The logger.
        /// </summary>
        public static readonly ILog Logger = LogManager.GetLogger(typeof(NffMaintenance));

        /// <summary>
        /// Initializes a new instance of the <see cref="NffMaintenance"/> class.
        /// </summary>
        public NffMaintenance(MaintenanceMode maintenanceMode)
        {
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }

            if (maintenanceMode == MaintenanceMode.CheckAllMatches || maintenanceMode == MaintenanceMode.CheckMatches)
            {
                CheckMissingMatches();
            }
        }

        /// <summary>
        /// Gets or sets the output folder.
        /// </summary>
        public string OutputFolder { get; set; }

        /// <summary>
        /// Method that wraps checking database, and then getting the actual information and then creating the Xml-file
        /// </summary>
        public void CheckMissingMatches()
        {
            var dataMapper = new MatchDataMapper();

            DataFileCreator datafileCreator = new DataFileCreator();
            List<int> missingMatches = dataMapper.FindMissingMatches();

            // If we have matches, then we shall do something with them - like get the data for it
            if (missingMatches.Any())
            {
                foreach (int matchId in missingMatches)
                {
                    Logger.Info("Checking if we have data now for Match Id: " + matchId);
                    datafileCreator.CreateDataFileMaintenance(matchId, 16);
                }
            }
        }

        /// <summary>
        /// The check all missing matches.
        /// </summary>
        public void CheckAllMissingMatches()
        {
            var dataMapper = new MatchDataMapper();
            DataFileCreator datafileCreator = new DataFileCreator();
            List<int> missingMatches = dataMapper.FindAllMissingMatches();

            // If we have matches, then we shall do something with them - like get the data for it
            if (missingMatches.Any())
            {
                foreach (int matchId in missingMatches)
                {
                    Logger.Info("Checking if we have data now for Match Id: " + matchId);
                    datafileCreator.CreateDataFileMaintenance(matchId, 16);
                }
            }
        }

        /// <summary>
        /// The tournament router.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        public void TournamentRouter(string file, int sportId)
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(file);

            string convertionType = string.Empty;

            XmlNode xmlNode = xmlDocument.SelectSingleNode("/SportsData/SportsData-Meta");
            if (xmlNode != null && xmlNode.Attributes != null)
            {
                convertionType = xmlNode.Attributes["type"].Value;
            }

            if (convertionType == "customer")
            {
                SendTournamentsToClient(file, sportId);
            }
            else
            {
                SendTournamentsToAllClients(file, sportId);
            }
        }

        /// <summary>
        /// The send tournaments to all clients.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        /// <param name="sportId">
        /// The sport Id.
        /// </param>
        public void SendTournamentsToAllClients(string file, int sportId)
        {
            Logger.Info(@"===============================================================================");
            Logger.Info(@"                         Start maintaining customer                             ");
            Logger.Info(@"===============================================================================");

            try
            {
                DateTime startDateTime = new DateTime();
                DateTime endDateTime = new DateTime();

                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(file);

                XmlNode xmlNode = xmlDocument.SelectSingleNode("/SportsData/SportsData-Meta");
                if (xmlNode != null && xmlNode.Attributes != null)
                {
                    startDateTime = Convert.ToDateTime(xmlNode.Attributes["startdate"].Value);
                }

                if (xmlNode != null && xmlNode.Attributes != null)
                {
                    endDateTime = Convert.ToDateTime(xmlNode.Attributes["enddate"].Value);
                }

                XmlNodeList xmlNodeList = xmlDocument.SelectNodes("/SportsData/Tournaments/Tournament");
                if (xmlNodeList == null)
                {
                    return;
                }

                DataFileCreator nffDatafileCreator = new DataFileCreator
                                                            {
                                                                SportId = sportId,
                                                                OutputFolder = OutputFolder
                                                            };
                
                foreach (XmlNode tournamentNode in xmlNodeList)
                {
                    int tournamentId = 0;
                    if (tournamentNode.Attributes != null)
                    {
                        tournamentId = Convert.ToInt32(tournamentNode.Attributes["id"].Value);
                    }

                    // Below this we have customers
                    XmlNodeList xmlNodeListCustomers =
                        xmlDocument.SelectNodes("/SportsData/Tournaments/Tournament[@id='" + tournamentId + "']/Customers/Customer");

                    if (xmlNodeListCustomers == null)
                    {
                        return;
                    }

                    // Creating CustomerList
                    List<Customer> customers = 
                        (from XmlNode customerNode in xmlNodeListCustomers
                         let xmlAttributeCollection = customerNode.Attributes
                         where xmlAttributeCollection != null
                         where xmlAttributeCollection != null 
                         select new Customer
                                    {
                                        Id = Convert.ToInt32(xmlAttributeCollection["id"].Value), 
                                        RemoteCustomerId = Convert.ToInt32(xmlAttributeCollection["foreigncustomerid"].Value)
                                    }).ToList();

                    Logger.Info("Checking for Creating Datafile for tournament: " + tournamentId);

                    // nffDatafileCreator.CreateTournamentDataFile(customers, tournamentId, startDateTime, endDateTime);
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
        }

        /// <summary>
        /// The send tournaments to client.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        /// <param name="sportId">
        /// The sport Id.
        /// </param>
        public void SendTournamentsToClient(string file, int sportId)
        {
            Logger.Info(@"===============================================================================");
            Logger.Info(@"                         Start maintaining customer                             ");
            Logger.Info(@"===============================================================================");
            try
            {
                int customerId = 0;
                int remoteCustomerNumber = 0;
                DateTime startDateTime = new DateTime();
                DateTime endDateTime = new DateTime();

                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(file);
                
                XmlNode xmlNode = xmlDocument.SelectSingleNode("/SportsData/Customer");
                if (xmlNode != null && xmlNode.Attributes != null)
                {
                    customerId = XmlConvert.ToInt32(xmlNode.Attributes["id"].Value);
                    remoteCustomerNumber = XmlConvert.ToInt32(xmlNode.Attributes["foreigncustomerid"].Value);
                }

                xmlNode = xmlDocument.SelectSingleNode("/SportsData/Customer/Job");
                if (xmlNode != null && xmlNode.Attributes != null)
                {
                    startDateTime = Convert.ToDateTime(xmlNode.Attributes["startdate"].Value);
                    endDateTime = Convert.ToDateTime(xmlNode.Attributes["enddate"].Value);
                }

                XmlNodeList xmlNodeList = xmlDocument.SelectNodes("/SportsData/Customer/Tournaments/Tournament");
                if (xmlNodeList != null)
                {
                    DataFileCreator nffDatafileCreator = new DataFileCreator
                        {
                            SportId = sportId,
                            OutputFolder = OutputFolder
                        };

                    foreach (int tournamentId in xmlNodeList)
                    {
                        // this should do the same as the marked out line below
                        // nffDatafileCreator.CreateCustomerDataFile(customerId, remoteCustomerNumber, tournamentId, startDateTime, endDateTime);
                    }

                    // nffDatafileCreator.CreateCustomerDataFile(customerId, remoteCustomerNumber, xmlNodeList, startDateTime, endDateTime);

                    //if (startDateTime.CompareTo(DateTime.Today) == 0)
                    //{
                    //    nffDatafileCreator.CreateCustomerDataFile(customerId, remoteCustomerNumber, xmlNodeList, startDateTime, endDateTime);
                        
                    //}
                    //else
                    //{
                    //    for (DateTime currentDate = DateTime.Parse(startDateTime.ToShortDateString());
                    //         endDateTime.CompareTo(currentDate) > 0;
                    //         currentDate = currentDate.AddDays(1.0))
                    //    {
                    //        Logger.Info("Checking for tournaments on date: " + currentDate.ToShortDateString());

                    //        // Looping through the tournament nodes
                    //        foreach (XmlNode tournamentNode in xmlNodeList)
                    //        {
                    //            // Checking that we don't have an empty attribute
                    //            if (tournamentNode.Attributes != null)
                    //            {
                    //                int tournamentId = Convert.ToInt32(tournamentNode.Attributes["id"].Value);

                    //                Logger.Info("Checking for Creating Datafile for tournament: " + tournamentId);

                    //                nffDatafileCreator.CreateCustomerDataFile(
                    //                    customerId, remoteCustomerNumber, tournamentId, currentDate);
                    //            }
                    //        }
                    //    }
                    //}
                }
                Logger.Info(@"===============================================================================");
                Logger.Info(@"                         Done maintaining customer                             ");
                Logger.Info(@"===============================================================================");
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
        }
    }
}