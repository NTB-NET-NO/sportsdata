﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using NTB.SportsData.Service.Components.Interfaces;
using NTB.SportsData.Service.Domain.Classes;
using NTB.SportsData.Service.Domain.Enums;
using log4net;

namespace NTB.SportsData.Service.Components.Nff
{
    public class SoccerXmlDocumentCreator : IXmlDocumentCreator
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SoccerXmlDocumentCreator));

        public SoccerXmlDocumentCreator()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();

            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        public void WriteXmlDocument(XmlDocument xmlDocument, string eventName, int eventId, List<Filename> filenames, int? districtId)
        {
            throw new NotImplementedException();
        }

        public XmlDocument CreateCustomerStructure(List<Customer> customers)
        {
            Logger.Debug("Creating customer structure");
            XmlDocument doc = new XmlDocument();

            XmlElement xmlCustomers = doc.CreateElement("Customers");
            foreach (Customer customer in customers)
            {
                XmlElement xmlCustomer = doc.CreateElement("Customer");
                XmlElement xmlCustomerName = doc.CreateElement("CustomerName");
                xmlCustomerName.SetAttribute("Id", customer.Id.ToString());
                xmlCustomerName.SetAttribute("RemoteCustomerId", customer.RemoteCustomerId.ToString());
                XmlText xmlText = doc.CreateTextNode(customer.Name);
                xmlCustomerName.AppendChild(xmlText);
                xmlCustomer.AppendChild(xmlCustomerName);
                xmlCustomers.AppendChild(xmlCustomer);
            }

            doc.AppendChild(xmlCustomers);

            return doc;
        }

        public string CreateDocumentType(bool renderResult)
        {
            return renderResult ? "resultat" : "terminliste";
        }

        public Filename CreateFileName(bool renderResult, DateTime? dateTime, int districtId, int tournamentId,
                                       Organization organization)
        {
            throw new NotImplementedException();
        }

        public XmlDocument CreateCompleteDocumentStructure()
        {
            throw new NotImplementedException();
        }

        public void WriteXmlDocument(XmlDocument xmlDocument, Filename filename)
        {
            throw new NotImplementedException();
        }

        public XmlDocument CreateFileName()
        {
            throw new NotImplementedException();
        }

        public XmlDocument CreateTournamentStructure(Tournament tournament)
        {
            Logger.Debug("Creating tournament structure");
            XmlDocument doc = new XmlDocument();
            // Create Tournament-MetaInfo node
            XmlElement xmlTournamentMetaInfo = doc.CreateElement("Tournament-MetaInfo");
            xmlTournamentMetaInfo.SetAttribute("Id", tournament.TournamentId.ToString());
            xmlTournamentMetaInfo.SetAttribute("Name", tournament.TournamentName);
            xmlTournamentMetaInfo.SetAttribute("Number", tournament.TournamentNumber);
            xmlTournamentMetaInfo.SetAttribute("Division", tournament.Division.ToString());

            doc.AppendChild(xmlTournamentMetaInfo);

            return doc;
        }

        public XmlDocument CreateDistrictElement(District district)
        {
            Logger.Debug("Creating district element");
            
            XmlDocument doc = new XmlDocument();
            XmlElement xmlDistrict = doc.CreateElement("District");
            xmlDistrict.SetAttribute("Id", district.DistrictId.ToString());
            xmlDistrict.SetAttribute("Name", district.DistrictName);
            XmlText xmlText = doc.CreateTextNode(district.DistrictName);
            xmlDistrict.AppendChild(xmlText);
            
            doc.AppendChild(xmlDistrict);
            
            return doc;
        }

        public XmlDocument CreateMatchStructure(List<Match> matches, bool renderResult)
        {
            Logger.Debug("Creating match structure");
            XmlDocument doc = new XmlDocument();

            XmlElement xmlMatches = doc.CreateElement("Matches");
            foreach (Match match in matches)
            {
                // Creating the match element with attribute
                XmlElement xmlMatch = doc.CreateElement("Match");
                xmlMatch.SetAttribute("Id", match.MatchId.ToString());
                DateTime matchDate = Convert.ToDateTime(match.MatchStartDate);
                xmlMatch.SetAttribute("startdate", matchDate.ToShortDateString());
                xmlMatch.SetAttribute("starttime", matchDate.ToShortTimeString());

                XmlElement xmlHomeTeam = doc.CreateElement("HomeTeam");
                xmlHomeTeam.SetAttribute("Id", match.HomeTeamId.ToString());
                XmlNode xmlHomeTeamName = doc.CreateElement("Name");
                XmlText xmlText = doc.CreateTextNode(match.HomeTeamName.Trim());
                xmlHomeTeamName.AppendChild(xmlText);
                xmlHomeTeam.AppendChild(xmlHomeTeamName);

                XmlElement xmlAwayTeam = doc.CreateElement("AwayTeam");
                xmlAwayTeam.SetAttribute("Id", match.AwayTeamId.ToString());
                XmlNode xmlAwayTeamName = doc.CreateElement("Name");

                // create <name> tag 
                xmlText = doc.CreateTextNode(match.AwayTeamName.Trim());
                xmlAwayTeamName.AppendChild(xmlText);
                xmlAwayTeam.AppendChild(xmlAwayTeamName);

                if (renderResult)
                {
                    MatchResult[] matchResults = match.MatchResultList;

                    foreach (MatchResult result in matchResults)
                    {
                        // ReSharper disable PossibleInvalidOperationException
                        XmlElement xmlMatchResult = doc.CreateElement("MatchResult");
                        xmlMatchResult.SetAttribute("type", result.ResultTypeName);
                        if (result.HomeTeamGoals != null)
                        {
                            xmlMatchResult.SetAttribute("hometeam", result.HomeTeamGoals.ToString());
                        }

                        xmlMatchResult.SetAttribute("awayteam", result.AwayTeamGoals.ToString());
                        xmlMatch.AppendChild(xmlMatchResult);

                        // ReSharper restore PossibleInvalidOperationException
                    }

                    XmlElement xmlGoals;
                    if (match.AwayTeamGoals != null)
                    {
                        string awayTeamGoals = match.AwayTeamGoals.ToString();
                        xmlGoals = doc.CreateElement("Totalgoals");
                        xmlText = doc.CreateTextNode(awayTeamGoals);
                        xmlGoals.AppendChild(xmlText);

                        xmlAwayTeam.AppendChild(xmlGoals);
                    }

                    if (match.HomeTeamGoals != null)
                    {
                        string homeTeamGoals = match.HomeTeamGoals.ToString();

                        xmlText = doc.CreateTextNode(homeTeamGoals);
                        xmlGoals = doc.CreateElement("Totalgoals");
                        xmlGoals.AppendChild(xmlText);

                        xmlHomeTeam.AppendChild(xmlGoals);
                    }

                    XmlElement xmlMatchDate = doc.CreateElement("MatchDate");
                    XmlElement xmlMatchTime = doc.CreateElement("MatchTime");

                    if (match.MatchStartDate.HasValue)
                    {
                        DateTime justDate = match.MatchStartDate.Value;
                        DateTime justTime = match.MatchStartDate.Value;

                        // xmlMatchDate.SetAttribute("Date", match.MatchStartDate.Value.ToString());
                        xmlMatchDate.SetAttribute("Date", justDate.ToShortDateString());

                        // xmlMatchTime.SetAttribute("Time", match.MatchStartDate.Value.ToString());
                        xmlMatchTime.SetAttribute("Time", justTime.ToLongTimeString());

                        // Add readable value in text-node
                        xmlText = doc.CreateTextNode(match.MatchStartDate.Value.ToLongTimeString());
                        xmlMatchTime.AppendChild(xmlText);

                        // Add readable value in text-node
                        xmlText = doc.CreateTextNode(match.MatchStartDate.Value.ToLongDateString());
                        xmlMatchDate.AppendChild(xmlText);
                    }
                    else
                    {
                        xmlText = doc.CreateTextNode("No time");
                        xmlMatchTime.AppendChild(xmlText);

                        xmlText = doc.CreateTextNode("No Date");
                        xmlMatchDate.AppendChild(xmlText);
                    }

                    xmlMatch.AppendChild(xmlMatchDate);
                    xmlMatch.AppendChild(xmlMatchTime);

                    XmlElement xmlPlayers = doc.CreateElement("Players");
                    XmlElement xmlPlayer;
                    XmlElement xmlPlayerName;
                    if (match.HomeTeamPlayers.Any())
                    {
                        xmlPlayers.SetAttribute("Clubid", match.HomeTeamId.ToString());

                        foreach (Player player in match.HomeTeamPlayers)
                        {
                            xmlPlayer = doc.CreateElement("Player");
                            xmlPlayerName = doc.CreateElement("PlayerName");
                            string firstName = string.Empty;
                            string lastName = string.Empty;

                            if (player.PlayerId != null)
                            {
                                xmlPlayer.SetAttribute("id", player.PlayerId.ToString());
                            }

                            if (player.Position != null)
                            {
                                xmlPlayer.SetAttribute("position", player.Position);
                            }

                            if (player.FirstName != null)
                            {
                                xmlPlayerName.SetAttribute("FirstName", player.FirstName);
                                firstName = player.FirstName;
                            }

                            if (player.SurName != null)
                            {
                                xmlPlayerName.SetAttribute("LastName", player.SurName);
                                lastName = player.SurName;
                            }

                            xmlText = doc.CreateTextNode(firstName + " " + lastName);

                            xmlPlayerName.AppendChild(xmlText);
                            xmlPlayer.AppendChild(xmlPlayerName);

                            xmlPlayers.AppendChild(xmlPlayer);
                        }
                    }

                    xmlMatch.AppendChild(xmlPlayers);

                    if (match.AwayTeamPlayers.Any())
                    {
                        xmlPlayers = doc.CreateElement("Players");
                        xmlPlayers.SetAttribute("Clubid", match.AwayTeamId.ToString());

                        foreach (Player player in match.AwayTeamPlayers)
                        {
                            if (player.FirstName != null)
                            {
                                xmlPlayer = doc.CreateElement("Player");
                                xmlPlayer.SetAttribute("id", player.PlayerId.ToString());

                                if (player.Position != null)
                                {
                                    xmlPlayer.SetAttribute("position", player.Position);
                                }

                                xmlPlayerName = doc.CreateElement("PlayerName");
                                xmlPlayerName.SetAttribute("FirstName", player.FirstName);
                                xmlPlayerName.SetAttribute("LastName", player.SurName);

                                xmlText = doc.CreateTextNode(player.FirstName + " " + player.SurName);

                                xmlPlayerName.AppendChild(xmlText);
                                xmlPlayer.AppendChild(xmlPlayerName);

                                xmlPlayers.AppendChild(xmlPlayer);
                            }
                        }

                        xmlMatch.AppendChild(xmlPlayers);
                    }

                    // Can we get events also
                    if (match.MatchEventList != null)
                    {
                        // Has been moved outside foreach so that the structure is like this:
                        // events
                        // event
                        // eventtype
                        // player
                        // and so on...
                        // Fix so that player has same structure as player in match/team structure...
                        XmlElement xmlEvents = doc.CreateElement("Events");
                        foreach (MatchEvent events in match.MatchEventList)
                        {
                            XmlElement xmlEvent = doc.CreateElement("Event");
                            xmlEvent.SetAttribute("id", events.MatchEventId.ToString());
                            xmlEvent.SetAttribute("TeamId", events.TeamId.ToString());

                            XmlElement xmlEventType = doc.CreateElement("EventType");
                            switch (events.MatchEventType)
                            {
                                case "Bytte ut":
                                    xmlEventType.SetAttribute("name", "bytte");
                                    xmlEventType.SetAttribute("value", "ut");
                                    xmlEventType.SetAttribute("guid", events.ConnectedToEvent);
                                    xmlEventType.SetAttribute("minute", events.Minute.ToString());
                                    break;

                                case "Bytte inn":
                                    xmlEventType.SetAttribute("name", "bytte");
                                    xmlEventType.SetAttribute("value", "inn");
                                    xmlEventType.SetAttribute("guid", events.ConnectedToEvent);
                                    xmlEventType.SetAttribute("minute", events.Minute.ToString());
                                    break;

                                case "Straffemål":
                                    xmlEventType.SetAttribute("name", "goal");
                                    xmlEventType.SetAttribute("value", "Straffemål");
                                    xmlEventType.SetAttribute("minute", events.Minute.ToString());
                                    xmlEventType.SetAttribute(
                                        "homegoal", events.HomeGoals.HasValue ? events.HomeGoals.ToString() : "0");
                                    xmlEventType.SetAttribute(
                                        "awaygoal", events.AwayGoals.HasValue ? events.AwayGoals.ToString() : "0");
                                    break;

                                case "Spillemål":
                                    xmlEventType.SetAttribute("name", "goal");
                                    xmlEventType.SetAttribute("value", "Spillemål");
                                    xmlEventType.SetAttribute("minute", events.Minute.ToString());
                                    xmlEventType.SetAttribute(
                                        "homegoal", events.HomeGoals.HasValue ? events.HomeGoals.ToString() : "0");
                                    xmlEventType.SetAttribute(
                                        "awaygoal", events.AwayGoals.HasValue ? events.AwayGoals.ToString() : "0");
                                    break;

                                case "Advarsel":
                                    xmlEventType.SetAttribute("name", "warning");
                                    xmlEventType.SetAttribute("value",
                                                              events.SecondYellowCard ? "Utvisning" : "Advarsel");
                                    xmlEventType.SetAttribute("minute", events.Minute.ToString());
                                    // MatchEvent matchEvent = new MatchEvent();

                                    break;
                            }

                            XmlElement xmlEventPlayer = doc.CreateElement("Player");
                            xmlEventPlayer.SetAttribute("id", events.PlayerId.ToString());

                            XmlElement xmlEventPlayerName = doc.CreateElement("Name");
                            if (events.PlayerName != null)
                            {
                                xmlEventPlayerName.SetAttribute("full", events.PlayerName);
                                xmlText = doc.CreateTextNode(events.PlayerName);
                            }
                            else
                            {
                                xmlText = doc.CreateTextNode(string.Empty);
                            }

                            xmlEventPlayerName.AppendChild(xmlText);
                            xmlEventPlayer.AppendChild(xmlEventPlayerName);
                            xmlEventType.AppendChild(xmlEventPlayer);
                            xmlEvent.AppendChild(xmlEventType);
                            xmlEvents.AppendChild(xmlEvent);

                            // Adding the events to the Match element
                            xmlMatch.AppendChild(xmlEvents);

                            // Logging the different values.
                            Logger.Debug("Event.AwayGoals: " + events.AwayGoals);
                            Logger.Debug("Event.Comment: " + events.Comment);
                            Logger.Debug("Event.ConnectedToEvent: " + events.ConnectedToEvent);
                            Logger.Debug("Event.ExtensionData: " + events.ExtensionData);
                            Logger.Debug("Event.HomeGoals: " + events.HomeGoals);
                            Logger.Debug("Event.MatchEventId: " + events.MatchEventId);

                            Logger.Debug("Event.MatchEventType: " + events.MatchEventType);
                            Logger.Debug("Event.MatchEventTypeId: " + events.MatchEventTypeId);

                            Logger.Debug("Event.Minute: " + events.Minute);
                            Logger.Debug("Event.PersonInfoHidden: " + events.PersonInfoHidden);
                            Logger.Debug("Event.PlayerId: " + events.PlayerId);
                            Logger.Debug("Event.PlayerName: " + events.PlayerName);

                            Logger.Debug("Event.SecondYellowCard: " + events.SecondYellowCard);
                            Logger.Debug("Event.TeamId: " + events.TeamId);
                            Logger.Debug("Event.TeamName: " + events.TeamName);
                        }
                    }

                    Logger.Debug(
                        "ID " + match.MatchId + " Matches: " + match.HomeTeamName + " - " + match.AwayTeamName);
                }

                // sb.AppendLine(Line);
                XmlElement xmlStadium = doc.CreateElement("Stadium");

                if (match.StadiumName != null)
                {
                    xmlStadium.SetAttribute("attendance", match.Spectators.ToString());

                    // sb.AppendLine("Stadion: " + match.StadiumName);
                    xmlText = doc.CreateTextNode(match.StadiumName);
                    xmlStadium.AppendChild(xmlText);
                }

                XmlElement xmlReferees = doc.CreateElement("Referees");

                if (match.Referees.Length > 0)
                {
                    // sb.AppendLine("Dommer: " + match.Referees.);
                    List<Referee> referees = match.Referees.ToList();

                    foreach (Referee referee in referees)
                    {
                        if (referee.FirstName != null)
                        {
                            if (referee.FirstName.Trim() != string.Empty)
                            {
                                XmlElement xmlReferee = doc.CreateElement("Referee");
                                xmlReferee.SetAttribute("FirstName", referee.FirstName);
                                xmlReferee.SetAttribute("LastName", referee.SurName);
                                xmlReferee.SetAttribute("Type", referee.RefereeType);
                                xmlReferee.SetAttribute("Club", referee.RefereeClub);
                                xmlText = doc.CreateTextNode(referee.FirstName + " " + referee.SurName);
                                xmlReferee.AppendChild(xmlText);

                                xmlReferees.AppendChild(xmlReferee);
                            }
                        }
                    }
                }

                xmlMatch.AppendChild(xmlReferees);

                xmlMatch.AppendChild(xmlHomeTeam);
                xmlMatch.AppendChild(xmlAwayTeam);
                xmlMatch.AppendChild(xmlStadium);
                xmlMatches.AppendChild(xmlMatch);
            }

            doc.AppendChild(xmlMatches);

            return doc;
        }

        public XmlDocument CreateStandingStructure(List<TournamentTableTeam> standings)
        {
            XmlDocument doc = new XmlDocument();

            XmlElement xmlStandings = doc.CreateElement("Standings");

            foreach (TournamentTableTeam standing in standings)
            {
                XmlElement xmlStandingTeam = doc.CreateElement("Team");
                xmlStandingTeam.SetAttribute("Id", standing.TeamId.ToString());
                xmlStandingTeam.SetAttribute("TablePosition", standing.TablePosition.ToString());
                XmlElement xmlTeamName = doc.CreateElement("Name");
                XmlText xmlText = doc.CreateTextNode(standing.TeamNameInTournament.Trim());
                xmlTeamName.AppendChild(xmlText);
                xmlStandingTeam.AppendChild(xmlTeamName);

                XmlElement xmlTablePosition = doc.CreateElement("Position");
                xmlText = doc.CreateTextNode(standing.TablePosition.ToString());
                xmlTablePosition.AppendChild(xmlText);

                XmlElement xmlPointsTotal = doc.CreateElement("Points");
                xmlPointsTotal.SetAttribute("Total", standing.TotalPoints.ToString());

                XmlElement xmlGoalDifferenceTotal = doc.CreateElement("GoalDifferences");
                xmlGoalDifferenceTotal.SetAttribute("Total", standing.TotalGoalDifference.ToString());

                XmlElement xmlGoalsTotal = doc.CreateElement("Goals");

                string scoresTotal = string.Empty;

                string againstTotal = string.Empty;

                if (standing.HomeGoalsScored != null)
                {
                    if (standing.AwayGoalsScored != null)
                    {
                        scoresTotal =
                            (standing.HomeGoalsScored + standing.AwayGoalsScored).ToString();
                    }
                }

                if (standing.HomeGoalsAgainst != null)
                {
                    againstTotal =
                        (standing.HomeGoalsAgainst + standing.AwayGoalsAgainst).ToString();
                }

                xmlGoalsTotal.SetAttribute("TotalScored", scoresTotal);
                xmlGoalsTotal.SetAttribute("TotalAgainst", againstTotal);

                int matchesTotal = standing.AwayMatchesPlayed + standing.HomeMatchesPlayed;
                XmlElement xmlMatchesTotal = doc.CreateElement("Matches");
                xmlMatchesTotal.SetAttribute("Total", matchesTotal.ToString());

                // ---- Home Matches
                XmlElement xmlHomeMatches = doc.CreateElement("HomeMatches");
                xmlHomeMatches.SetAttribute("Won", standing.HomeMatchesWon.ToString());
                xmlHomeMatches.SetAttribute("Draw", standing.HomeMatchesDraw.ToString());
                xmlHomeMatches.SetAttribute("Lost", standing.HomeMatchesLost.ToString());
                xmlMatchesTotal.AppendChild(xmlHomeMatches);

                XmlElement xmlHomeGoalsScored = doc.CreateElement("Home");
                xmlHomeGoalsScored.SetAttribute("Scored", standing.HomeGoalsScored.ToString());
                xmlHomeGoalsScored.SetAttribute("Against", standing.HomeGoalsAgainst.ToString());
                xmlGoalsTotal.AppendChild(xmlHomeGoalsScored);

                XmlElement xmlPointsHome = doc.CreateElement("HomePoints");
                xmlText = doc.CreateTextNode(standing.HomePoints.ToString());
                xmlPointsHome.AppendChild(xmlText);
                xmlPointsTotal.AppendChild(xmlPointsHome);

                XmlElement xmlGoalDifferenceHome = doc.CreateElement("GoalDifferenceHome");
                xmlText = doc.CreateTextNode(standing.HomeGoalDifference.ToString());
                xmlGoalDifferenceHome.AppendChild(xmlText);
                xmlGoalDifferenceTotal.AppendChild(xmlGoalDifferenceHome);

                // ---- Away Matches 
                XmlElement xmlAwayMatches = doc.CreateElement("AwayMatches");
                xmlAwayMatches.SetAttribute("Won", standing.AwayMatchesWon.ToString());
                xmlAwayMatches.SetAttribute("Draw", standing.AwayMatchesDraw.ToString());
                xmlAwayMatches.SetAttribute("Lost", standing.AwayMatchesLost.ToString());
                xmlMatchesTotal.AppendChild(xmlAwayMatches);

                XmlElement xmlAwayGoalsScored = doc.CreateElement("Away");
                xmlAwayGoalsScored.SetAttribute("Scored", standing.AwayGoalsScored.ToString());
                xmlAwayGoalsScored.SetAttribute("Against", standing.AwayGoalsAgainst.ToString());
                xmlGoalsTotal.AppendChild(xmlAwayGoalsScored);

                XmlElement xmlPointsAway = doc.CreateElement("AwayPoints");
                xmlText = doc.CreateTextNode(standing.AwayPoints.ToString());
                xmlPointsAway.AppendChild(xmlText);
                xmlPointsTotal.AppendChild(xmlPointsAway);

                XmlElement xmlAwayMatchesPlayed = doc.CreateElement("AwayMatchesPlayed");
                xmlText = doc.CreateTextNode(standing.AwayMatchesPlayed.ToString());
                xmlAwayMatchesPlayed.AppendChild(xmlText);

                XmlElement xmlGoalDifferenceAway = doc.CreateElement("GoalDifferenceAway");
                xmlText = doc.CreateTextNode(standing.AwayGoalDifference.ToString());
                xmlGoalDifferenceAway.AppendChild(xmlText);
                xmlGoalDifferenceTotal.AppendChild(xmlGoalDifferenceAway);

                xmlStandingTeam.AppendChild(xmlPointsTotal);
                xmlStandingTeam.AppendChild(xmlGoalsTotal);
                xmlStandingTeam.AppendChild(xmlMatchesTotal);

                xmlStandings.AppendChild(xmlStandingTeam);
            }

            doc.AppendChild(xmlStandings);

            return doc;
        }

        MessageType IXmlDocumentCreator.CreateDocumentType(bool renderResult)
        {
            throw new NotImplementedException();
        }
    }
}
