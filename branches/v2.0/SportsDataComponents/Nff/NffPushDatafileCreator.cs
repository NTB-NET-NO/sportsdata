// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NffPushDatafileCreator.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the NffPushDatafileCreator type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using NTB.SportsData.Service.Components.Factories;
using NTB.SportsData.Service.Components.Interfaces;
using NTB.SportsData.Service.Components.Repositories;
using NTB.SportsData.Service.Components.TeamSport;
using NTB.SportsData.Service.Domain.Classes;
using NTB.SportsData.Service.Domain.Enums;
using log4net;
using NTB.SportsData.Utilities;

namespace NTB.SportsData.Service.Components.Nff
{
    /// <summary>
    /// The nff push datafile creator.
    /// </summary>
    public class NffPushDatafileCreator : IDataFileCreator
    {
        /// <summary>
        /// The match id.
        /// </summary>
        public int MatchId = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="NffPushDatafileCreator"/> class.
        /// </summary>
        
        public NffPushDatafileCreator()
        {
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        /// <summary>
        /// Creating datafile based on the information in the push message
        /// </summary>
        /// <param name="sportId">
        /// The sport Id.
        /// </param>
        public void CreateDataFileFromPush(int sportId)
        {
            DataFileCreator.Logger.Debug("We are in CreateDataFileFromPush!");

            DataFileCreator.Logger.Debug("We are creating a NFF SportsDataDatabase Object!");

            
            DataFileCreator.Logger.Debug("We have gotten database object");
            try
            {
                var facade = new FacadeFactory().GetFacade(sportId);
                // Now we have an ID, we shall get todays matches
                DataFileCreator.Logger.Info("Getting the Matches Object and putting it into the Match list using MatchId: " + MatchId);
                Match match = facade.GetMatchByMatchId(MatchId);
                DateTime? matchDate = match.MatchStartDate;
                
                // Check if match is in the database
                var matchDataMapper = new MatchDataMapper();
                matchDataMapper.FindMatchByMatch(match);

                DataFileCreator.Logger.Debug(
                    "This is match match.TournamentAgeCategoryId: " + match.TournamentAgeCategoryId);

                DataFileCreator.Logger.Info("Using Matchobject to get the tournamentid: " + match.TournamentId);

                int tournamentId = match.TournamentId;
                if (ConfigurationManager.AppSettings["testing"] == "true")
                {
                    tournamentId = Convert.ToInt32(ConfigurationManager.AppSettings["TestingTournamentId"]);
                }

                DataFileCreator.Logger.Info(
                    "Getting the Tournament Object based on the Tournament ID: " + tournamentId);

                // Getting the Tournament Object based on the TournamentId
                Tournament tournament = facade.GetTournament(tournamentId);

                DataFileCreator.Logger.Debug("Tournament:" + tournament.TournamentName + ".");
                DataFileCreator.Logger.Debug("Done getting Tournament Object");

                List<Customer> customers = new List<Customer>();

                // Checking if the tournament is a national team tournament
                if (tournament.NationalTeamTournament == false)
                {
                    // Now I can check against the database, because I now know that the match exists...
                    var customerDataMapper = new CustomerDataMapper();
                    customers = customerDataMapper.GetListOfCustomersByMatch(match);
                }
                else
                {
                    Customer customer = new Customer { Name = "NTB" };

                    customers.Add(customer);
                }

                DataFileCreator.Logger.Info("Number of Customers: " + customers.Count());

                if (customers.Any())
                {
                    DataFileCreator.Logger.Debug("Number of customers: " + customers.Count());
                    foreach (Customer customer in customers)
                    {
                        DataFileCreator.Logger.Debug("Customer: " + customer.Name);
                    }

                    try
                    {
                        // Inverted the if so that we get fewer levels
                        DataFileCreator.Logger.Debug("publish result: " + tournament.PublishResult);
                        if (!tournament.PublishResult)
                        {
                            DataFileCreator.Logger.Info(
                                "The results from tournament " + tournament.TournamentName + " cannot publish results!");
                            return;
                        }

                        DataFileCreator.Logger.Debug("publish result is true");

                        // Finding out if we are to get referee, squad and match events for this tournament
                        int? agecatid = tournament.AgeCategoryId;

                        List<AgeCategory> tournamentages =
                            new List<AgeCategory>(facade.GetAgeCategories());

                        AgeCategory filteredAgeCategories =
                            tournamentages.Find(t => t.AgeCategoryId == agecatid && t.MaxAge >= 12);

                        // Setting some variables to false
                        bool getSquad = false;
                        bool getReferee = false;
                        bool getEvents = false;

                        // We only want squad, referee and events from mathces that are in the AgeCategory Toppfotball
                        DataFileCreator.Logger.Debug("Checking if we shall have more data");

                        if (filteredAgeCategories == null)
                        {
                            DataFileCreator.Logger.Info("Filter AgeCategory returned no data");
                            return;
                        }

                        // Do I need to do this as I am filtering below?
                        if (filteredAgeCategories.MaxAge < 12)
                        {
                            DataFileCreator.Logger.Info("AgeCategory.MaxAge less than 12. Cannot create data");
                            return;
                        }

                        // Checking if this is a toppfotball classification
                        if (filteredAgeCategories.AgeCategoryName == "Toppfotball")
                        {
                            DataFileCreator.Logger.Debug("Toppfotball!");
                            getSquad = true;
                            getReferee = true;
                            getEvents = true;
                        }

                        DataFileCreator.Logger.Debug("Getting all matches in this tournament for this day!");

                        // We need to change this so that it only generates a file with one date in it - and that date must be from the date in the push
                        // Getting matches that are played today (but then getting more than todays matches)
                        // using the date that came with the Push notification
                        DateTime? tournamentDate = DateTime.Today;
                        if ((matchDate != null) && (matchDate != tournamentDate))
                        {
                            string matchDateString = matchDate.Value.Year + "-" + matchDate.Value.Month + "-"
                                                     + matchDate.Value.Day;

                            tournamentDate = Convert.ToDateTime(matchDateString);
                        }

                        DataFileCreator.Logger.Debug(
                            "Calling GetMatchesByTournament with following parameters (Except tournamentDate):");
                        DataFileCreator.Logger.Debug(
                            "tournamentId; " + tournamentId + ", getSquard: " + getSquad + ", getReferee: " + getReferee
                            + ", getEvents: " + getEvents + ", tournamentDate: " + tournamentDate);

                        // Checking if matchDate is not null. If it is we create current date (and just date)
                        if (matchDate == null)
                        {
                            matchDate = DateTime.Today.Date;
                        }

                        // Code below has been marked out because it generates results from more than one day
                        // List<Match> matches =
                        //    new List<Match>(
                        //        _DataFileCreator.TournamentServiceClient.GetMatchesByTournament(
                        //            tournamentId,
                        //            getSquad,
                        //            getReferee,
                        //            true,
                        //            getEvents,
                        //            tournamentDate)).Where(
                        //                m => m.MatchStartDate.HasValue &&
                        //                 (m.MatchStartDate.Value.Date >= matchDate.Value.Date &&
                        //                  m.MatchStartDate.Value.Date <= DateTime.Today) && m.HomeTeamGoals != null).ToList();

                        // Shall generate a file with only match-date matches
                        List<Match> matches =
                            new List<Match>(
                                facade.GetMatchesByTournament(
                                    tournamentId, getSquad, getReferee, true, getEvents, DateTime.Today.Date)).Where(
                                        m =>
                                        m.MatchStartDate.HasValue
                                        && (m.MatchStartDate.Value.Date == matchDate.Value.Date)
                                        && m.HomeTeamGoals != null).ToList();

                        // If we don't have any matches, well, then we return back home....
                        if (matches.Count == 0)
                        {
                            DataFileCreator.Logger.Info("Date filter returned no matches...");

                            return;
                        }

                        // Creating the Standings object that we will be using when creating XML
                        List<TournamentTableTeam> standings = new List<TournamentTableTeam>();

                        // Checking if this tournament is one that 
                        if (tournament.PublishTournamentTable)
                        {
                            
                            DataFileCreator.Logger.Info("Getting the Standings");

                            try
                            {
                                standings =
                                    new List<TournamentTableTeam>(
                                        facade.GetTournamentStanding(
                                            tournamentId));
                            }
                            catch (Exception exception)
                            {
                                DataFileCreator.Logger.Debug(exception.Message);
                                if (exception.Message == "No tournament standings found.")
                                {
                                    DataFileCreator.Logger.Info(
                                        "No standings found for tournament " + tournament.TournamentName);
                                }
                            }
                        }
                        else
                        {
                            DataFileCreator.Logger.Info(
                                "The standings from tournament " + tournament.TournamentName
                                + " cannot publish results!");
                        }

                        //DataFileCreator.Logger.Info("Creating XML-structure!");

                        //// Creating filename
                        //_DataFileCreator.DistrictId = tournament.DistrictId;

                        //// Creating the XMLDocument
                        //XmlDocument doc = _DataFileCreator.NffXmlDocument.CreateXmlStructure(
                        //    matches, tournament, filteredAgeCategories, standings, false, customers);

                        //List<Filename> fileNames =
                        //    _DataFileCreator.NffXmlDocument.CreateFileName(
                        //        tournament.DistrictId, tournament.TournamentId, null, sportId);

                        //if (doc != null)
                        //{
                        //    // Now creating the XML-file
                        //    DataFileCreator.Logger.Info("Creating XML-File");
                        //    _DataFileCreator.NffXmlDocument.CreateXmlFile(
                        //        doc,
                        //        tournament.TournamentName,
                        //        tournament.TournamentId,
                        //        fileNames,
                        //        DistrictId);
                        //}
                    }
                    catch (Exception exception)
                    {
                        if (exception.Message == "No matches found.")
                        {
                            matchDataMapper.InsertMatchNotFound(match, tournament);
                        }
                        else
                        {
                            DataFileCreator.Logger.Error(exception.Message);
                            DataFileCreator.Logger.Error(exception.StackTrace);

                            // Consider this one and remove if it sends to many mails
                            Mail.SendException(exception);
                        }
                    }

                    // End of MaxAge-check
                }
                else
                {
                    DataFileCreator.Logger.Debug("No customers is receiving this result");
                }

                // Adding it here as well
                DataFileCreator.Logger.Info("Disposing database object");
                
            }
            catch (Exception exception)
            {
                DataFileCreator.Logger.Info("We have received an exception while doing match or tournament lookup");
                DataFileCreator.Logger.Info("====================================================================");
                DataFileCreator.Logger.Error(exception.Message);
                DataFileCreator.Logger.Error(exception.StackTrace);
            }
            finally
            {
                // "Disposing" the DB Object
                DataFileCreator.Logger.Info("Disposing database object in finally");
                
            }
        }

        public bool Configure(int sportId)
        {
            throw new NotImplementedException();
        }

        public List<Match> CreateTotalSchedule(int sportId, List<Season> seasons)
        {
            throw new NotImplementedException();
        }

        public void CreateDataFile(List<Customer> customers, int eventId, DateTime startDateTime, DateTime endDateTime, int sportId)
        {
            throw new NotImplementedException();
        }

        public void CreateDataFileMaintenance(int eventId, int sportId)
        {
            throw new NotImplementedException();
        }

        public AgeCategory GetAgeCategoryByTournament(Tournament tournament, int sportId)
        {
            throw new NotImplementedException();
        }

        public List<Match> GetMatchesByDate(DateTime matchDateTime, int sportId)
        {
            throw new NotImplementedException();
        }

        public List<Match> GetMatchesByTournament(int tournamentId, DateTime tournamentDate)
        {
            throw new NotImplementedException();
        }

        public District GetDistrict(int districtId)
        {
            throw new NotImplementedException();
        }

        public Tournament GetTournamentById(int tournamentId, int sportId)
        {
            throw new NotImplementedException();
        }

        public List<TournamentTableTeam> GetTournamentStanding(Tournament tournament, int sportId)
        {
            throw new NotImplementedException();
        }

        public MessageType CreateDocumentType(bool renderResult, bool standing)
        {
            throw new NotImplementedException();
        }
    }
}