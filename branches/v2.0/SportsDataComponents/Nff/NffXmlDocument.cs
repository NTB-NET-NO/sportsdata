﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NffXmlDocument.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The nff xml document.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace NTB.SportsData.Components.Nff
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;
    // using NTB.SportsData.Components.NFFProdService;
    using NTB.SportsData.Service.Components.Nff;
    using NTB.SportsData.Service.Components.NFFProdService;
    using NTB.SportsData.Service.Domain.Classes;
    using NTB.SportsData.Service.Domain.Enums;
    using Utilities;

    using District = NTB.SportsData.Service.Domain.Classes.District;
    using Match = NTB.SportsData.Service.Domain.Classes.Match;
    using MatchEvent = NTB.SportsData.Service.Domain.Classes.MatchEvent;
    using MatchResult = NTB.SportsData.Service.Domain.Classes.MatchResult;
    using Player = NTB.SportsData.Service.Domain.Classes.Player;
    using Referee = NTB.SportsData.Service.Domain.Classes.Referee;
    // using Referee = NFFProdService.Referee;
    using Tournament = NTB.SportsData.Service.Domain.Classes.Tournament;
    using TournamentTableTeam = NTB.SportsData.Service.Domain.Classes.TournamentTableTeam;

    /// <summary>
    /// The nff xml document.
    /// </summary>
    public class NffXmlDocument
    {
                /// <summary>
        /// The _nff datafile creator.
        /// </summary>
        private readonly NffDatafileCreator _nffDatafileCreator;

        /// <summary>
        /// Initializes a new instance of the <see cref="NffXmlDocument"/> class.
        /// </summary>
        /// <param name="nffDatafileCreator">
        /// The nff datafile creator.
        /// </param>
        public NffXmlDocument(NffDatafileCreator nffDatafileCreator)
        {
            _nffDatafileCreator = nffDatafileCreator;
            Standing = false;
            MatchFacts = false;
        }

        /// <summary>
        /// Gets or sets a value indicating whether standing.
        /// </summary>
        public bool Standing { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether match facts.
        /// </summary>
        public bool MatchFacts { get; set; }

        /// <summary>
        /// This method is used to control the flow of creating the XML-file
        /// We are first calling the createXMLStructure (which creates the XML-file sort of)
        /// Then we are actually saving the file to disk in createXMLFile.
        /// </summary>
        /// <param name="tournament">
        /// Holds the Tournament object with all data and methods
        /// </param>
        /// <param name="matches">
        /// Holds the Match object
        /// </param>
        /// <param name="renderResults">
        /// Holds the bool value of the render Result. If false we shall not render results
        /// </param>
        /// <param name="sportId">
        /// The sport Id.
        /// </param>
        /// <param name="ageCategoryTournament">
        /// The age Category Tournament.
        /// </param>
        public void CreateDataFile(Tournament tournament, List<Match> matches, bool renderResults, int sportId, AgeCategoryTournament ageCategoryTournament)
        {
            // Getting customers
            List<Match> customerMatches = matches;

            var db = new NffSportsDataDatabase();

            List<Customer> listOfCustomers = new List<Customer>();

            foreach (Match match in customerMatches)
            {
                // listOfCustomers = db.GetListOfCustomers(match.MatchId, sportId);
                listOfCustomers = db.GetListOfCustomersByMatchId(match.MatchId, sportId);
            }

            if (!listOfCustomers.Any())
            {
                return;
            }

            _nffDatafileCreator.Customers = listOfCustomers;

            NffDatafileCreator.Logger.Debug("Numbers of customers in : List of customers: " + listOfCustomers.Count());

            NffDatafileCreator.Logger.Debug(@"Tournament: " + tournament.TournamentName + @"(" + tournament.TournamentId + @")");

            List<TournamentTableTeam> standings = null;

            // TODO: This part shall be moved down to the DataFile Creator layer
            if (_nffDatafileCreator.RenderResult)
            {
                try
                {
                    /*
                    * Legger du p� timestamp f�r du endringer gjort etter datoen du sender med. Sender du med NULL blir ikke timestamp tatt hensyn til.
                    */
                    if (tournament.PublishTournamentTable)
                    {
                        if (_nffDatafileCreator.TournamentServiceClient.GetTournamentStanding(
                            tournament.TournamentId, null) != null)
                        {
                            /*
                             * Legger du p� timestamp f�r du endringer gjort etter datoen du sender med. Sender du med NULL blir ikke timestamp tatt hensyn til.
                             */
                            standings =
                                _nffDatafileCreator.TournamentServiceClient.GetTournamentStanding(
                                    tournament.TournamentId, null).ToList();
                        }
                    }
                }
                catch (Exception ex)
                {
                    NffDatafileCreator.Logger.Error(ex);
                }
            }

            // If we are to update the results, we do so here. 
            if (_nffDatafileCreator.UpdateDatabase)
            {
                NffDatafileCreator.Logger.Debug("Updating/Inserting into database");
                db = new NffSportsDataDatabase { SetMatches = matches };

                // then we check against the list of matches that we've gotten
                db.CheckMatches(sportId);
            }

            if (listOfCustomers.Any())
            {
                // If we are to create the XML-file, we do so here.
                if (_nffDatafileCreator.CreateXml)
                {
                    NffDatafileCreator.Logger.Debug("Creating XML Files!");

                    // Creating the XMLDocument
                    XmlDocument doc = CreateXmlStructure(
                        matches, tournament, ageCategoryTournament, standings, false, _nffDatafileCreator.Customers);

                    // Creating filename
                    List<Filename> filenames = CreateFileName(tournament.DistrictId, tournament.TournamentId, null, sportId);

                    // Finally creating the XML-file
                    if (doc != null)
                    {
                        CreateXmlFile(
                            doc, tournament.TournamentName, tournament.TournamentId, filenames, tournament.DistrictId);
                    }
                }
            }
        }

        /// <summary>
        /// The create xml file.
        /// </summary>
        /// <param name="doc">
        /// The doc.
        /// </param>
        /// <param name="tournamentName">
        /// The tournament name.
        /// </param>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <param name="filenames">
        /// The filenames.
        /// </param>
        /// <param name="districtId">
        /// The district id.
        /// </param>
        public void CreateXmlFile(
            XmlDocument doc, string tournamentName, int tournamentId, List<Filename> filenames, int districtId = 0)
        {
            // Getting DistrictName
            foreach (Filename filename in filenames)
            {
                try
                {
                    // Adding the file part to the XML-file
                    // Here we need some File-information
                    string documentName = string.Empty;
                    int documentVersion = 1;

                    // removing possible file element strukture first
                    XmlNode fileNode = doc.SelectSingleNode("/SportsData/Tournament/Header/File");
                    if (fileNode != null)
                    {
                        fileNode.RemoveAll();
                    }

                    XmlNode documentNode = doc.SelectSingleNode("/SportsData/Tournament/Header/DocumentType");

                    // Checking if district-tournament file has been created before
                    XmlNode node = doc.SelectSingleNode("/SportsData/Tournament/Header");

                    // Adding support for document type
                    string fileMessageType = "sportsresults";

                    if (filename != null)
                    {
                        documentVersion = filename.Version;
                        documentName = filename.Name;

                        switch (filename.MessageType)
                        {
                            case MessageType.MatchFacts:
                                fileMessageType = "matchfacts";
                                break;
                            case MessageType.SportMessage:
                                fileMessageType = "sportsmessage";
                                break;
                            case MessageType.SportResult:
                                fileMessageType = "sportsresults";
                                break;
                            case MessageType.SportSchedule:
                                fileMessageType = "sportsschedule";
                                break;
                        }
                    }

                    // XmlText xmlDocumentTypeText = doc.CreateTextNode(filename.MessageType.ToString());
                    // xmlDocumentType.AppendChild(xmlDocumentTypeText);
                    if (fileNode == null)
                    {
                        XmlElement xmlFile = doc.CreateElement("File");

                        XmlElement xmlFileInfo = doc.CreateElement("Name");
                        xmlFileInfo.SetAttribute("FullName", filename.FullName);

                        xmlFileInfo.SetAttribute("Type", "xml");
                        XmlText xmlText = doc.CreateTextNode(documentName);
                        xmlFileInfo.AppendChild(xmlText);
                        xmlFile.AppendChild(xmlFileInfo);

                        XmlElement xmlVersion = doc.CreateElement("Version");
                        xmlText = doc.CreateTextNode(documentVersion.ToString());
                        xmlVersion.AppendChild(xmlText);
                        xmlFile.AppendChild(xmlVersion);

                        XmlElement xmlDocumentType = doc.CreateElement("DocumentType");
                        xmlDocumentType.SetAttribute("type", fileMessageType);

                        xmlFileInfo = doc.CreateElement("Created");
                        DateTime dt = DateTime.Now;
                        string timestamp = dt.ToString("yyyy-MM-ddTHH:mm:ss");
                        xmlText = doc.CreateTextNode(timestamp);
                        xmlFileInfo.AppendChild(xmlText);
                        xmlFile.AppendChild(xmlFileInfo);
                        if (node != null)
                        {
                            node.AppendChild(xmlDocumentType);
                            node.AppendChild(xmlFile);
                        }
                    }
                    else
                    {
                        XmlElement xmlFileInfo = doc.CreateElement("Name");
                        if (filename != null)
                        {
                            xmlFileInfo.SetAttribute("FullName", filename.FullName);
                        }

                        xmlFileInfo.SetAttribute("Type", "xml");
                        XmlText xmlText = doc.CreateTextNode(documentName);
                        xmlFileInfo.AppendChild(xmlText);
                        fileNode.AppendChild(xmlFileInfo);

                        XmlElement xmlVersion = doc.CreateElement("Version");
                        xmlText = doc.CreateTextNode(documentVersion.ToString());
                        xmlVersion.AppendChild(xmlText);
                        fileNode.AppendChild(xmlVersion);

                        // Here we are changing the message type
                        documentNode.Attributes[0].Value = fileMessageType;

                        xmlFileInfo = doc.CreateElement("Created");
                        DateTime dt = DateTime.Now;
                        string timestamp = dt.ToString("yyyy-MM-ddTHH:mm:ss");
                        xmlText = doc.CreateTextNode(timestamp);
                        xmlFileInfo.AppendChild(xmlText);
                        fileNode.AppendChild(xmlFileInfo);
                        if (node != null)
                        {
                            node.AppendChild(fileNode);
                        }
                    }

                    if (districtId != 0)
                    {
                        _nffDatafileCreator.GetDistrictName(districtId);
                    }

                    if (!Directory.Exists(_nffDatafileCreator.OutputFolder))
                    {
                        NffDatafileCreator.Logger.Info(
                            "I am creating outputfolder: " + _nffDatafileCreator.OutputFolder);
                        Directory.CreateDirectory(_nffDatafileCreator.OutputFolder);
                    }
                    else
                    {
                        Directory.SetCurrentDirectory(_nffDatafileCreator.OutputFolder);
                    }

                    // Creating the complete Filename
                    if (filename == null)
                    {
                        continue;
                    }

                    string pathAndFilename = filename.Path + @"\" + filename.FullName;
                    NffDatafileCreator.Logger.Debug("Testing to save: " + pathAndFilename);

                    // string completeFilename = @"c:\" + DocumentName;
                    XmlWriterSettings writerSettings = new XmlWriterSettings
                    {
                        Indent = true,
                        OmitXmlDeclaration = false,
                        Encoding = Encoding.UTF8
                    };

                    // DONE: Change Using to While so that we can loop in case the file is in use...
                    for (int i = 0; i <= 3; i++)
                    {
                        using (XmlWriter writer = XmlWriter.Create(pathAndFilename, writerSettings))
                        {
                            try
                            {
                                NffDatafileCreator.Logger.Info("Saving: " + pathAndFilename);
                                doc.Save(writer);
                                break;
                            }
                            catch (IOException ioex)
                            {
                                NffDatafileCreator.Logger.Error(ioex);
                            }
                        }
                    }
                }
                catch (XmlException xex)
                {
                    NffDatafileCreator.Logger.Error("Problems writing file to: " + filename.FullName, xex);
                }
                catch (IOException ioex)
                {
                    NffDatafileCreator.Logger.Error(ioex);
                }
                catch (Exception ex)
                {
                    NffDatafileCreator.Logger.Error(ex);
                }
            }
        }

        /// <summary>
        /// The create file name.
        /// </summary>
        /// <param name="districtId">
        /// The district id.
        /// </param>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <param name="dateTime">
        /// The Date and Time for generating a filename
        /// </param>
        /// <param name="sportId">
        /// The sport Id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Filename> CreateFileName(int districtId, int tournamentId, DateTime? dateTime, int sportId)
        {
            // Creating some variables that are being used in this method
            string connectionString = ConfigurationManager.AppSettings["ConnectionString"];

            int documentVersion = 1;

            List<Filename> filenames = new List<Filename>();
            Filename fileName = new Filename();

            // We need to check if the file has a table or not
            string postfix = "Res";
            fileName.MessageType = MessageType.SportMessage;

            if (Standing)
            {
                postfix += "Tab"; // Making the postfix ResTab
                fileName.MessageType = MessageType.SportResult;
            }

            if (!_nffDatafileCreator.RenderResult)
            {
                postfix = "Sched";
                fileName.MessageType = MessageType.SportSchedule;
            }

            // We have not found a document produced for this tournament
            // We generate the filename, and then just the stem
            DateTime todayFile = DateTime.Today;
            if (dateTime != null)
            {
                todayFile = Convert.ToDateTime(dateTime);
            }

            // We are not adding XML to the filename, this will be added when we are creating the rest
            string filename = "NTB_SportsData_" + todayFile.Year + todayFile.Month.ToString().PadLeft(2, '0')
                              + todayFile.Day.ToString().PadLeft(2, '0') + "_NFF_D" + districtId + "_T" + tournamentId
                              + "_" + postfix;

            try
            {
                SqlConnection sqlConnection = new SqlConnection(connectionString);
                SqlCommand sqlCommand = new SqlCommand("Service_GetDocumentByTournamentId", sqlConnection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                NffDatafileCreator.Logger.Debug("GetDocumentByTournamentId: " + tournamentId);
                sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", tournamentId));

                sqlConnection.Open();

                SqlDataReader mySqlDataReader = sqlCommand.ExecuteReader();

                if (mySqlDataReader.HasRows)
                {
                    while (mySqlDataReader.Read())
                    {
                        documentVersion = Convert.ToInt32(mySqlDataReader["DocumentVersion"]) + 1;
                    }

                    // Closing connection
                    sqlConnection.Close();

                    sqlCommand = new SqlCommand("Service_UpdateDocument", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    // Setting the DocumentName
                    NffDatafileCreator.Logger.Debug("DocumentVersion: " + documentVersion);
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentVersion", documentVersion));

                    // Setting the TournamentId
                    NffDatafileCreator.Logger.Debug("TournamentId: " + tournamentId);
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", tournamentId));

                    // Setting the DocumentName
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentCreated", DateTime.Now));

                    // Open the connection
                    sqlConnection.Open();

                    // Run the query
                    sqlCommand.ExecuteNonQuery();
                }
                else
                {
                    // Closing connection
                    sqlConnection.Close();

                    // We must now add this to the database
                    // sqlConnection = new SqlConnection(connectionString);
                    sqlCommand = new SqlCommand("Service_InsertDocument", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", tournamentId));

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                    // DONE: Change this to really get it from the database!

                    // Setting the version to 1
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentVersion", documentVersion));

                    // Setting the DocumentName
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentName", filename));

                    // Setting the DocumentName
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentCreated", DateTime.Now));

                    // Open the connection
                    sqlConnection.Open();

                    // Run the query
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlexception)
            {
                NffDatafileCreator.Logger.Error(sqlexception.Message);
                NffDatafileCreator.Logger.Error(sqlexception.StackTrace);
            }
            catch (Exception exception)
            {
                NffDatafileCreator.Logger.Error(exception);
                NffDatafileCreator.Logger.Error(exception);
            }

            fileName.Name = filename;
            fileName.Version = documentVersion;
            fileName.Created = DateTime.Now;

            if (filename == string.Empty)
            {
                filename = fileName.Version.ToString();
                filename += fileName.Created.ToShortDateString();
            }

            filename += "_V" + documentVersion;

            // Writing to file
            if (_nffDatafileCreator.OutputFolder == @"\" || string.IsNullOrEmpty(_nffDatafileCreator.OutputFolder))
            {
                // _nffDatafileCreator.OutputFolder = ConfigurationManager.AppSettings["OutPath"];
                filename += "_" + _nffDatafileCreator.JobName;
            }

            fileName.FullName = filename + ".xml";
            fileName.Path = _nffDatafileCreator.OutputFolder;
            NffDatafileCreator.Logger.Debug("Filename: " + fileName.FullName);

            filenames.Add(fileName);

            if (MatchFacts)
            {
                string filenameFacts = "NTB_SportsData_" + todayFile.Year + todayFile.Month.ToString().PadLeft(2, '0')
                                       + todayFile.Day.ToString().PadLeft(2, '0') + "_NFF_D" + districtId + "_T" + tournamentId
                                       + "_Fakta";

                filenameFacts += "_V" + documentVersion;
                Filename matchFactsFilename = new Filename
                {
                    Name = filenameFacts,
                    Version = documentVersion,
                    Created = DateTime.Now,
                    FullName = filenameFacts + ".xml",
                    Path = _nffDatafileCreator.OutputFolder,
                    MessageType = MessageType.MatchFacts
                };
                filenames.Add(matchFactsFilename);
            }

            return filenames;
        }

        /// <summary>
        /// Method to create the XMLDocument that we are to convert to SportsML later
        /// </summary>
        /// <param name="matches">
        /// List of matches
        /// </param>
        /// <param name="tournament">
        /// Tourmanent object
        /// </param>
        /// <param name="ageCategoryTournament">
        /// Age category tournament
        /// </param>
        /// <param name="listOfStandings">
        /// List of standings
        /// </param>
        /// <param name="finalResult">
        /// If this is final results or not
        /// </param>
        /// <param name="listOfCustomers">
        /// list of customers
        /// </param>
        /// <returns>
        /// Returns XML Document structure
        /// </returns>
        public XmlDocument CreateXmlStructure(
            List<Match> matches,
            Tournament tournament,
            AgeCategoryTournament ageCategoryTournament,
            List<TournamentTableTeam> listOfStandings = null,
            bool finalResult = false,
            List<Customer> listOfCustomers = null)
        {
            // List<Filename> fileNames = null,
            // Line is used for something that I cannot remember right now

            // Creating xml-document
            NffDatafileCreator.Logger.Info("I am now creating the XML Structure for " + tournament.TournamentName);

            XmlDocument doc = new XmlDocument(); // Create the XML Declaration, and append it to XML document

            Standing = false;
            MatchFacts = false;

            NffDatafileCreator.Logger.Debug("Done creating the XMLDocument object!");
            try
            {
                XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", null, null);
                NffDatafileCreator.Logger.Debug("Done creating the XmlDeclaration!");

                doc.AppendChild(dec); // Create the root element
                NffDatafileCreator.Logger.Debug("Done Create the root element!");

                // To indicate some type of status
                /*
                 * 0 - not complete
                 * 1 - complete 
                 * ?
                 * 
                 * Only to be used when generating results
                 */
                int intMatches = matches.Count();
                NffDatafileCreator.Logger.Info("Number of matches: " + intMatches);

                XmlElement xmlMainRoot = doc.CreateElement("SportsData");
                XmlElement xmlRoot = doc.CreateElement("Tournament");

                xmlRoot.SetAttribute("Type", _nffDatafileCreator.RenderResult ? "resultat" : "terminliste");

                // Set the district-information
                District district = _nffDatafileCreator.OrganizationServiceClient.GetDistrict(tournament.DistrictId);

                NffDatafileCreator.Logger.Info("About to create more information on Tournament and more");
                XmlElement xmlDistrict = doc.CreateElement("District");
                xmlDistrict.SetAttribute("Id", district.DistrictId.ToString());

                xmlDistrict.SetAttribute("Name", district.DistrictName);
                XmlText xmlText = doc.CreateTextNode(district.DistrictName);
                xmlDistrict.AppendChild(xmlText);

                // Create Tournament-MetaInfo node
                XmlElement xmlTournamentMetaInfo = doc.CreateElement("Tournament-MetaInfo");
                xmlTournamentMetaInfo.SetAttribute("Id", tournament.TournamentId.ToString());
                xmlTournamentMetaInfo.SetAttribute("Name", tournament.TournamentName);
                xmlTournamentMetaInfo.SetAttribute("Number", tournament.TournamentNumber);
                xmlTournamentMetaInfo.SetAttribute("Division", tournament.Division.ToString());

                List<Match> matchQuery =
                    (from filteredMatch in matches
                     where filteredMatch.TournamentId == tournament.TournamentId
                     select filteredMatch).Take(1).ToList();
                Match matchResult = new Match();

                foreach (Match filteredMatch in matchQuery)
                {
                    matchResult.MatchStartDate = filteredMatch.MatchStartDate;
                    matchResult.TournamentRoundNumber = filteredMatch.TournamentRoundNumber;

                    xmlTournamentMetaInfo.SetAttribute("MatchDate", matchResult.MatchStartDate.ToString());
                    xmlTournamentMetaInfo.SetAttribute("RoundNumber", matchResult.TournamentRoundNumber.ToString());

                    // ReSharper disable PossibleInvalidOperationException
                    string docid = "T" + tournament.TournamentNumber + "R" + matchResult.TournamentRoundNumber + "_"
                                   + matchResult.MatchStartDate.Value.Year
                                   + matchResult.MatchStartDate.Value.Month.ToString().PadLeft(2, '0')
                                   + matchResult.MatchStartDate.Value.Day.ToString().PadLeft(2, '0');
                    xmlRoot.SetAttribute("doc-id", docid);

                    // ReSharper restore PossibleInvalidOperationException
                }

                xmlTournamentMetaInfo.AppendChild(xmlDistrict);
                xmlRoot.AppendChild(xmlTournamentMetaInfo);
                NffDatafileCreator.Logger.Debug("Done create more information on Tournament and more");

                // Header information
                XmlElement xmlHeader = doc.CreateElement("Header");

                // Add Information about Organisation
                NffDatafileCreator.Logger.Info("Creating Organization information for the XML-file");
                XmlElement xmlOrganisation = doc.CreateElement("Organisation");
                xmlOrganisation.SetAttribute("Name", "NFF");
                xmlText = doc.CreateTextNode("Norges Fotballforbund");
                xmlOrganisation.AppendChild(xmlText);
                xmlHeader.AppendChild(xmlOrganisation);

                XmlElement xmlAgeCategory = doc.CreateElement("AgeCategory");

                if (ageCategoryTournament != null)
                {
                    if (!tournament.NationalTeamTournament)
                    {
                        switch (ageCategoryTournament.AgeCategoryId)
                        {
                            case 18:
                            case 21:
                                xmlAgeCategory.SetAttribute("min-age", ageCategoryTournament.MinAge.ToString());
                                xmlAgeCategory.SetAttribute("max-age", ageCategoryTournament.MaxAge.ToString());
                                xmlAgeCategory.SetAttribute("name", ageCategoryTournament.AgeCategoryName);
                                xmlAgeCategory.SetAttribute("id", ageCategoryTournament.AgeCategoryId.ToString());
                                xmlAgeCategory.InnerText = "senior";
                                break;

                            default:
                                xmlAgeCategory.SetAttribute("min-age", ageCategoryTournament.MinAge.ToString());
                                xmlAgeCategory.SetAttribute("max-age", ageCategoryTournament.MaxAge.ToString());
                                xmlAgeCategory.SetAttribute("name", ageCategoryTournament.AgeCategoryName);
                                xmlAgeCategory.SetAttribute("id", ageCategoryTournament.AgeCategoryId.ToString());
                                xmlAgeCategory.InnerText = "aldersbestemt";
                                break;
                        }
                    }
                    else
                    {
                        xmlAgeCategory.SetAttribute("min-age", ageCategoryTournament.MinAge.ToString());
                        xmlAgeCategory.SetAttribute("max-age", ageCategoryTournament.MaxAge.ToString());
                        xmlAgeCategory.SetAttribute("name", ageCategoryTournament.AgeCategoryName);
                        xmlAgeCategory.SetAttribute("id", ageCategoryTournament.AgeCategoryId.ToString());
                        xmlAgeCategory.InnerText = ageCategoryTournament.AgeCategoryName;
                    }
                }

                xmlHeader.AppendChild(xmlAgeCategory);

                XmlElement xmlCustomers = doc.CreateElement("Customers");

                // Now we shall add customers
                if (listOfCustomers != null)
                {
                    foreach (Customer customer in listOfCustomers)
                    {
                        XmlElement xmlCustomer = doc.CreateElement("Customer");
                        XmlElement xmlCustomerName = doc.CreateElement("CustomerName");
                        xmlCustomerName.SetAttribute("Id", customer.Id.ToString());
                        xmlCustomerName.SetAttribute("RemoteCustomerId", customer.RemoteCustomerId.ToString());
                        xmlText = doc.CreateTextNode(customer.Name);
                        xmlCustomerName.AppendChild(xmlText);
                        xmlCustomer.AppendChild(xmlCustomerName);
                        xmlCustomers.AppendChild(xmlCustomer);
                    }
                }

                xmlHeader.AppendChild(xmlCustomers);

                // Adding information about the sport
                XmlElement xmlSport = doc.CreateElement("Sport");
                xmlText = doc.CreateTextNode("Fotball");
                xmlSport.AppendChild(xmlText);
                xmlHeader.AppendChild(xmlSport);

                NffDatafileCreator.Logger.Info("Done creating Organization information for the XML-file");

                xmlRoot.AppendChild(xmlHeader);

                NffDatafileCreator.Logger.Info("Creating the Matches/Match elements in the XML-file");
                XmlElement xmlMatches = doc.CreateElement("Matches");

                if (_nffDatafileCreator.RenderResult)
                {
                    // Adding a check so that we are not adding matches that does not have a result yet to the XML
                    List<Match> filteredMatches = (from m in matches where m.HomeTeamGoals != null select m).ToList();

                    if (filteredMatches.Count == 0)
                    {
                        return null;
                    }

                    foreach (Match match in filteredMatches)
                    {
                        CreateMatchStructure(match, doc, xmlMatches, true);
                    }
                }
                else
                {
                    // When creating a schedule things are different. Then we might want to have todays matches and yesterdays matches and so on...
                    List<Match> scheduledMatches = matches.Where(match => match.HomeTeamGoals == null).ToList();

                    if (scheduledMatches.Any())
                    {
                        // foreach (Match match in matches.Where(match => match.HomeTeamGoals == null))
                        foreach (Match match in scheduledMatches)
                        {
                            CreateMatchStructure(match, doc, xmlMatches, false);
                        }
                    }
                    else
                    {
                        return null;
                    }
                }

                XmlElement xmlStandings = null;
                if (_nffDatafileCreator.RenderResult)
                {
                    NffDatafileCreator.Logger.Debug("Creating the XML-element for the standings");

                    xmlStandings = doc.CreateElement("Standings");
                }

                if (listOfStandings != null)
                {
                    Standing = true;

                    // Looping the TournanemtTableTeam
                    foreach (TournamentTableTeam standing in listOfStandings)
                    {
                        XmlElement xmlStandingTeam = doc.CreateElement("Team");
                        xmlStandingTeam.SetAttribute("Id", standing.TeamId.ToString());
                        xmlStandingTeam.SetAttribute("TablePosition", standing.TablePosition.ToString());
                        XmlElement xmlTeamName = doc.CreateElement("Name");
                        xmlText = doc.CreateTextNode(standing.TeamNameInTournament.Trim());
                        xmlTeamName.AppendChild(xmlText);
                        xmlStandingTeam.AppendChild(xmlTeamName);

                        XmlElement xmlTablePosition = doc.CreateElement("Position");
                        xmlText = doc.CreateTextNode(standing.TablePosition.ToString());
                        xmlTablePosition.AppendChild(xmlText);

                        XmlElement xmlPointsTotal = doc.CreateElement("Points");
                        xmlPointsTotal.SetAttribute("Total", standing.TotalPoints.ToString());

                        XmlElement xmlGoalDifferenceTotal = doc.CreateElement("GoalDifferences");
                        xmlGoalDifferenceTotal.SetAttribute("Total", standing.TotalGoalDifference.ToString());

                        XmlElement xmlGoalsTotal = doc.CreateElement("Goals");

                        string scoresTotal = string.Empty;

                        string againstTotal = string.Empty;

                        if (standing.HomeGoalsScored != null)
                        {
                            if (standing.AwayGoalsScored != null)
                            {
                                scoresTotal =
                                    (standing.HomeGoalsScored.Value + standing.AwayGoalsScored.Value).ToString();
                            }
                        }

                        if (standing.HomeGoalsAgainst != null)
                        {
                            againstTotal =
                                (standing.HomeGoalsAgainst.Value + standing.AwayGoalsAgainst.Value).ToString();
                        }

                        xmlGoalsTotal.SetAttribute("TotalScored", scoresTotal);
                        xmlGoalsTotal.SetAttribute("TotalAgainst", againstTotal);

                        int matchesTotal = standing.AwayMatchesPlayed.Value + standing.HomeMatchesPlayed.Value;
                        XmlElement xmlMatchesTotal = doc.CreateElement("Matches");
                        xmlMatchesTotal.SetAttribute("Total", matchesTotal.ToString());

                        // ---- Home Matches
                        XmlElement xmlHomeMatches = doc.CreateElement("HomeMatches");
                        xmlHomeMatches.SetAttribute("Won", standing.HomeMatchesWon.ToString());
                        xmlHomeMatches.SetAttribute("Draw", standing.HomeMatchesDraw.ToString());
                        xmlHomeMatches.SetAttribute("Lost", standing.HomeMatchesLost.ToString());
                        xmlMatchesTotal.AppendChild(xmlHomeMatches);

                        XmlElement xmlHomeGoalsScored = doc.CreateElement("Home");
                        xmlHomeGoalsScored.SetAttribute("Scored", standing.HomeGoalsScored.ToString());
                        xmlHomeGoalsScored.SetAttribute("Against", standing.HomeGoalsAgainst.ToString());
                        xmlGoalsTotal.AppendChild(xmlHomeGoalsScored);

                        XmlElement xmlPointsHome = doc.CreateElement("HomePoints");
                        xmlText = doc.CreateTextNode(standing.HomePoints.ToString());
                        xmlPointsHome.AppendChild(xmlText);
                        xmlPointsTotal.AppendChild(xmlPointsHome);

                        XmlElement xmlGoalDifferenceHome = doc.CreateElement("GoalDifferenceHome");
                        xmlText = doc.CreateTextNode(standing.HomeGoalDifference.ToString());
                        xmlGoalDifferenceHome.AppendChild(xmlText);
                        xmlGoalDifferenceTotal.AppendChild(xmlGoalDifferenceHome);

                        // ---- Away Matches 
                        XmlElement xmlAwayMatches = doc.CreateElement("AwayMatches");
                        xmlAwayMatches.SetAttribute("Won", standing.AwayMatchesWon.ToString());
                        xmlAwayMatches.SetAttribute("Draw", standing.AwayMatchesDraw.ToString());
                        xmlAwayMatches.SetAttribute("Lost", standing.AwayMatchesLost.ToString());
                        xmlMatchesTotal.AppendChild(xmlAwayMatches);

                        XmlElement xmlAwayGoalsScored = doc.CreateElement("Away");
                        xmlAwayGoalsScored.SetAttribute("Scored", standing.AwayGoalsScored.ToString());
                        xmlAwayGoalsScored.SetAttribute("Against", standing.AwayGoalsAgainst.ToString());
                        xmlGoalsTotal.AppendChild(xmlAwayGoalsScored);

                        XmlElement xmlPointsAway = doc.CreateElement("AwayPoints");
                        xmlText = doc.CreateTextNode(standing.AwayPoints.ToString());
                        xmlPointsAway.AppendChild(xmlText);
                        xmlPointsTotal.AppendChild(xmlPointsAway);

                        XmlElement xmlAwayMatchesPlayed = doc.CreateElement("AwayMatchesPlayed");
                        xmlText = doc.CreateTextNode(standing.AwayMatchesPlayed.ToString());
                        xmlAwayMatchesPlayed.AppendChild(xmlText);

                        XmlElement xmlGoalDifferenceAway = doc.CreateElement("GoalDifferenceAway");
                        xmlText = doc.CreateTextNode(standing.AwayGoalDifference.ToString());
                        xmlGoalDifferenceAway.AppendChild(xmlText);
                        xmlGoalDifferenceTotal.AppendChild(xmlGoalDifferenceAway);

                        xmlStandingTeam.AppendChild(xmlPointsTotal);
                        xmlStandingTeam.AppendChild(xmlGoalsTotal);
                        xmlStandingTeam.AppendChild(xmlMatchesTotal);

                        xmlStandings.AppendChild(xmlStandingTeam);
                    }
                }

                NffDatafileCreator.Logger.Debug("Done creating the XML-element for the standings");
                xmlRoot.AppendChild(xmlMatches);

                if (listOfStandings != null)
                {
                    xmlRoot.AppendChild(xmlStandings);
                }

                xmlMainRoot.AppendChild(xmlRoot);
                doc.AppendChild(xmlMainRoot);
                NffDatafileCreator.Logger.Debug("Done Creating the XML-structure");

                return doc;
            }
            catch (XmlException xmlexception)
            {
                NffDatafileCreator.Logger.Debug(xmlexception.Message);
                NffDatafileCreator.Logger.Debug(xmlexception);
            }
            catch (Exception exception)
            {
                NffDatafileCreator.Logger.Debug(exception.Message);
                NffDatafileCreator.Logger.Debug(exception);
            }

            return doc;
        }

        /// <summary>
        /// The create match structure.
        /// </summary>
        /// <param name="match">
        /// The match.
        /// </param>
        /// <param name="doc">
        /// The doc.
        /// </param>
        /// <param name="xmlMatches">
        /// The xml matches.
        /// </param>
        /// <param name="renderResult">
        /// The render result.
        /// </param>
        private void CreateMatchStructure(Match match, XmlDocument doc, XmlElement xmlMatches, bool renderResult)
        {
            NffDatafileCreator.Logger.Debug("Creating the Match elements with ID " + match.MatchId + " in the XML-file");

            // Creating the match element with attribute
            XmlElement xmlMatch = doc.CreateElement("Match");
            xmlMatch.SetAttribute("Id", match.MatchId.ToString());
            DateTime matchDate = Convert.ToDateTime(match.MatchStartDate);
            xmlMatch.SetAttribute("startdate", matchDate.ToShortDateString());
            xmlMatch.SetAttribute("starttime", matchDate.ToShortTimeString());

            if (match.HomeTeamName == string.Empty)
            {
                string foo = "just here so that I can stop it!";
            }

            // DONE: Need to check if match is to be created or not

            // DONE: We need to store the information about the district/Zone somewhere.

            // DONE: Create the <hometeam><name>structure</name><goals></goals></hometeam>
            XmlElement xmlHomeTeam = doc.CreateElement("HomeTeam");
            xmlHomeTeam.SetAttribute("Id", match.HomeTeamId.ToString());
            XmlNode xmlHomeTeamName = doc.CreateElement("Name");
            XmlText xmlText = doc.CreateTextNode(match.HomeTeamName.Trim());
            xmlHomeTeamName.AppendChild(xmlText);
            xmlHomeTeam.AppendChild(xmlHomeTeamName);

            XmlElement xmlAwayTeam = doc.CreateElement("AwayTeam");
            xmlAwayTeam.SetAttribute("Id", match.AwayTeamId.ToString());
            XmlNode xmlAwayTeamName = doc.CreateElement("Name");

            // create <name> tag 
            xmlText = doc.CreateTextNode(match.AwayTeamName.Trim());
            xmlAwayTeamName.AppendChild(xmlText);
            xmlAwayTeam.AppendChild(xmlAwayTeamName);

            if (renderResult)
            {
                MatchResult[] matchResults = match.MatchResultList;

                foreach (MatchResult result in matchResults)
                {
                    // ReSharper disable PossibleInvalidOperationException
                    XmlElement xmlMatchResult = doc.CreateElement("MatchResult");
                    xmlMatchResult.SetAttribute("type", result.ResultTypeName);
                    if (result.HomeTeamGoals != null)
                    {
                        xmlMatchResult.SetAttribute("hometeam", result.HomeTeamGoals.Value.ToString());
                    }

                    xmlMatchResult.SetAttribute("awayteam", result.AwayTeamGoals.Value.ToString());
                    xmlMatch.AppendChild(xmlMatchResult);

                    // ReSharper restore PossibleInvalidOperationException
                }

                XmlElement xmlGoals;
                if (match.AwayTeamGoals != null)
                {
                    string awayTeamGoals = match.AwayTeamGoals.Value.ToString();
                    xmlGoals = doc.CreateElement("Totalgoals");
                    xmlText = doc.CreateTextNode(awayTeamGoals);
                    xmlGoals.AppendChild(xmlText);

                    xmlAwayTeam.AppendChild(xmlGoals);
                }

                if (match.HomeTeamGoals != null)
                {
                    string homeTeamGoals = match.HomeTeamGoals.Value.ToString();

                    xmlText = doc.CreateTextNode(homeTeamGoals);
                    xmlGoals = doc.CreateElement("Totalgoals");
                    xmlGoals.AppendChild(xmlText);

                    xmlHomeTeam.AppendChild(xmlGoals);
                }

                XmlElement xmlMatchDate = doc.CreateElement("MatchDate");
                XmlElement xmlMatchTime = doc.CreateElement("MatchTime");

                if (match.MatchStartDate.HasValue)
                {
                    DateTime justDate = match.MatchStartDate.Value;
                    DateTime justTime = match.MatchStartDate.Value;

                    // xmlMatchDate.SetAttribute("Date", match.MatchStartDate.Value.ToString());
                    xmlMatchDate.SetAttribute("Date", justDate.ToShortDateString());

                    // xmlMatchTime.SetAttribute("Time", match.MatchStartDate.Value.ToString());
                    xmlMatchTime.SetAttribute("Time", justTime.ToLongTimeString());

                    // Add readable value in text-node
                    xmlText = doc.CreateTextNode(match.MatchStartDate.Value.ToLongTimeString());
                    xmlMatchTime.AppendChild(xmlText);

                    // Add readable value in text-node
                    xmlText = doc.CreateTextNode(match.MatchStartDate.Value.ToLongDateString());
                    xmlMatchDate.AppendChild(xmlText);
                }
                else
                {
                    xmlText = doc.CreateTextNode("No time");
                    xmlMatchTime.AppendChild(xmlText);

                    xmlText = doc.CreateTextNode("No Date");
                    xmlMatchDate.AppendChild(xmlText);
                }

                xmlMatch.AppendChild(xmlMatchDate);
                xmlMatch.AppendChild(xmlMatchTime);

                XmlElement xmlPlayers = doc.CreateElement("Players");
                XmlElement xmlPlayer;
                XmlElement xmlPlayerName;
                if (match.HomeTeamPlayers.Any())
                {
                    MatchFacts = true;
                    xmlPlayers.SetAttribute("Clubid", match.HomeTeamId.ToString());

                    foreach (Player player in match.HomeTeamPlayers)
                    {
                        xmlPlayer = doc.CreateElement("Player");
                        xmlPlayerName = doc.CreateElement("PlayerName");
                        string firstName = string.Empty;
                        string lastName = string.Empty;

                        if (player.PlayerId != null)
                        {
                            xmlPlayer.SetAttribute("id", player.PlayerId.ToString());
                        }

                        if (player.Position != null)
                        {
                            xmlPlayer.SetAttribute("position", player.Position);
                        }

                        if (player.FirstName != null)
                        {
                            xmlPlayerName.SetAttribute("FirstName", player.FirstName);
                            firstName = player.FirstName;
                        }

                        if (player.SurName != null)
                        {
                            xmlPlayerName.SetAttribute("LastName", player.SurName);
                            lastName = player.SurName;
                        }

                        xmlText = doc.CreateTextNode(firstName + " " + lastName);

                        xmlPlayerName.AppendChild(xmlText);
                        xmlPlayer.AppendChild(xmlPlayerName);

                        xmlPlayers.AppendChild(xmlPlayer);
                    }
                }

                xmlMatch.AppendChild(xmlPlayers);

                if (match.AwayTeamPlayers.Any())
                {
                    xmlPlayers = doc.CreateElement("Players");
                    xmlPlayers.SetAttribute("Clubid", match.AwayTeamId.ToString());

                    foreach (Player player in match.AwayTeamPlayers)
                    {
                        if (player.FirstName != null)
                        {
                            xmlPlayer = doc.CreateElement("Player");
                            xmlPlayer.SetAttribute("id", player.PlayerId.ToString());

                            if (player.Position != null)
                            {
                                xmlPlayer.SetAttribute("position", player.Position);
                            }

                            xmlPlayerName = doc.CreateElement("PlayerName");
                            xmlPlayerName.SetAttribute("FirstName", player.FirstName);
                            xmlPlayerName.SetAttribute("LastName", player.SurName);

                            xmlText = doc.CreateTextNode(player.FirstName + " " + player.SurName);

                            xmlPlayerName.AppendChild(xmlText);
                            xmlPlayer.AppendChild(xmlPlayerName);

                            xmlPlayers.AppendChild(xmlPlayer);
                        }
                    }

                    xmlMatch.AppendChild(xmlPlayers);
                }

                // Can we get events also
                if (match.MatchEventList != null)
                {
                    // Has been moved outside foreach so that the structure is like this:
                    // events
                    // event
                    // eventtype
                    // player
                    // and so on...
                    // Fix so that player has same structure as player in match/team structure...
                    XmlElement xmlEvents = doc.CreateElement("Events");
                    foreach (MatchEvent events in match.MatchEventList)
                    {
                        XmlElement xmlEvent = doc.CreateElement("Event");
                        xmlEvent.SetAttribute("id", events.MatchEventId.ToString());
                        xmlEvent.SetAttribute("TeamId", events.TeamId.ToString());

                        XmlElement xmlEventType = doc.CreateElement("EventType");
                        switch (events.MatchEventType)
                        {
                            case "Bytte ut":
                                xmlEventType.SetAttribute("name", "bytte");
                                xmlEventType.SetAttribute("value", "ut");
                                xmlEventType.SetAttribute("guid", events.ConnectedToEvent);
                                xmlEventType.SetAttribute("minute", events.Minute.ToString());
                                break;

                            case "Bytte inn":
                                xmlEventType.SetAttribute("name", "bytte");
                                xmlEventType.SetAttribute("value", "inn");
                                xmlEventType.SetAttribute("guid", events.ConnectedToEvent);
                                xmlEventType.SetAttribute("minute", events.Minute.ToString());
                                break;

                            case "Straffem�l":
                                xmlEventType.SetAttribute("name", "goal");
                                xmlEventType.SetAttribute("value", "Straffem�l");
                                xmlEventType.SetAttribute("minute", events.Minute.ToString());
                                xmlEventType.SetAttribute(
                                    "homegoal", events.HomeGoals.HasValue ? events.HomeGoals.ToString() : "0");
                                xmlEventType.SetAttribute(
                                    "awaygoal", events.AwayGoals.HasValue ? events.AwayGoals.ToString() : "0");
                                break;

                            case "Spillem�l":
                                xmlEventType.SetAttribute("name", "goal");
                                xmlEventType.SetAttribute("value", "Spillem�l");
                                xmlEventType.SetAttribute("minute", events.Minute.ToString());
                                xmlEventType.SetAttribute(
                                    "homegoal", events.HomeGoals.HasValue ? events.HomeGoals.ToString() : "0");
                                xmlEventType.SetAttribute(
                                    "awaygoal", events.AwayGoals.HasValue ? events.AwayGoals.ToString() : "0");
                                break;

                            case "Advarsel":
                                xmlEventType.SetAttribute("name", "warning");
                                xmlEventType.SetAttribute("value", events.SecondYellowCard ? "Utvisning" : "Advarsel");
                                xmlEventType.SetAttribute("minute", events.Minute.ToString());
                                MatchEvent matchEvent = new MatchEvent();

                                break;
                        }

                        XmlElement xmlEventPlayer = doc.CreateElement("Player");
                        xmlEventPlayer.SetAttribute("id", events.PlayerId.ToString());

                        XmlElement xmlEventPlayerName = doc.CreateElement("Name");
                        if (events.PlayerName != null)
                        {
                            xmlEventPlayerName.SetAttribute("full", events.PlayerName);
                            xmlText = doc.CreateTextNode(events.PlayerName);
                        }
                        else
                        {
                            xmlText = doc.CreateTextNode(string.Empty);
                        }

                        xmlEventPlayerName.AppendChild(xmlText);
                        xmlEventPlayer.AppendChild(xmlEventPlayerName);
                        xmlEventType.AppendChild(xmlEventPlayer);
                        xmlEvent.AppendChild(xmlEventType);
                        xmlEvents.AppendChild(xmlEvent);

                        // Adding the events to the Match element
                        xmlMatch.AppendChild(xmlEvents);

                        // Logging the different values.
                        NffDatafileCreator.Logger.Debug("Event.AwayGoals: " + events.AwayGoals);
                        NffDatafileCreator.Logger.Debug("Event.Comment: " + events.Comment);
                        NffDatafileCreator.Logger.Debug("Event.ConnectedToEvent: " + events.ConnectedToEvent);
                        NffDatafileCreator.Logger.Debug("Event.ExtensionData: " + events.ExtensionData);
                        NffDatafileCreator.Logger.Debug("Event.HomeGoals: " + events.HomeGoals);
                        NffDatafileCreator.Logger.Debug("Event.MatchEventId: " + events.MatchEventId);

                        NffDatafileCreator.Logger.Debug("Event.MatchEventType: " + events.MatchEventType);
                        NffDatafileCreator.Logger.Debug("Event.MatchEventTypeId: " + events.MatchEventTypeId);

                        NffDatafileCreator.Logger.Debug("Event.Minute: " + events.Minute);
                        NffDatafileCreator.Logger.Debug("Event.PersonInfoHidden: " + events.PersonInfoHidden);
                        NffDatafileCreator.Logger.Debug("Event.PlayerId: " + events.PlayerId);
                        NffDatafileCreator.Logger.Debug("Event.PlayerName: " + events.PlayerName);

                        NffDatafileCreator.Logger.Debug("Event.SecondYellowCard: " + events.SecondYellowCard);
                        NffDatafileCreator.Logger.Debug("Event.TeamId: " + events.TeamId);
                        NffDatafileCreator.Logger.Debug("Event.TeamName: " + events.TeamName);
                    }
                }

                NffDatafileCreator.Logger.Debug(
                "ID " + match.MatchId + " Matches: " + match.HomeTeamName + " - " + match.AwayTeamName);
            }

            // sb.AppendLine(Line);
            XmlElement xmlStadium = doc.CreateElement("Stadium");

            if (match.StadiumName != null)
            {
                xmlStadium.SetAttribute("attendance", match.Spectators.ToString());

                // sb.AppendLine("Stadion: " + match.StadiumName);
                xmlText = doc.CreateTextNode(match.StadiumName);
                xmlStadium.AppendChild(xmlText);
            }

            XmlElement xmlReferees = doc.CreateElement("Referees");

            if (match.Referees.Length > 0)
            {
                // sb.AppendLine("Dommer: " + match.Referees.);
                List<Referee> referees = match.Referees.ToList();

                foreach (Referee referee in referees)
                {
                    if (referee.FirstName != null)
                    {
                        if (referee.FirstName.Trim() != string.Empty)
                        {
                            var xmlReferee = doc.CreateElement("Referee");
                            xmlReferee.SetAttribute("FirstName", referee.FirstName);
                            xmlReferee.SetAttribute("LastName", referee.SurName);
                            xmlReferee.SetAttribute("Type", referee.RefereeType);
                            xmlReferee.SetAttribute("Club", referee.RefereeClub);
                            xmlText = doc.CreateTextNode(referee.FirstName + " " + referee.SurName);
                            xmlReferee.AppendChild(xmlText);

                            xmlReferees.AppendChild(xmlReferee);
                        }
                    }
                }
            }

            xmlMatch.AppendChild(xmlReferees);

            xmlMatch.AppendChild(xmlHomeTeam);
            xmlMatch.AppendChild(xmlAwayTeam);
            xmlMatch.AppendChild(xmlStadium);
            xmlMatches.AppendChild(xmlMatch);
        }
    }
}