// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NFFGathererComponent.Designer.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the NffGathererComponent type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.ComponentModel;
using System.IO;
using System.Timers;
using NTB.SportsData.Service.Domain.Enums;

namespace NTB.SportsData.Service.Components.Nff
{
    /// <summary>
    /// The nff gatherer component.
    /// </summary>
    public partial class NffGathererComponent
    {
        /// <summary>
        /// The Enabled status for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="Enabled"/></remarks>
        protected bool enabled;

        /// <summary>
        /// The operation mode for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="OperationMode"/></remarks>
        protected OperationMode operationMode;

        /// <summary>
        /// The polling style for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="PollStyle"/></remarks>
        protected PollStyle pollStyle;

        /// <summary>
        /// The maintanance mode for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="OperationMode"/></remarks>
        protected MaintenanceMode maintenanceMode;
        
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// The files watcher.
        /// </summary>
        private FileSystemWatcher filesWatcher;

        /// <summary>
        /// The service timer.
        /// </summary>
        private Timer ServiceTimer;

        /// <summary>
        /// The poll timer.
        /// </summary>
        private Timer pollTimer;

        /// <summary>
        /// The background worker 1.
        /// </summary>
        private BackgroundWorker backgroundWorker1;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ServiceTimer = new System.Timers.Timer();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.pollTimer = new System.Timers.Timer();
            ((System.ComponentModel.ISupportInitialize)(this.ServiceTimer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pollTimer)).BeginInit();
            // 
            // ServiceTimer
            // 
            this.ServiceTimer.Interval = 2000D;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // pollTimer
            // 
            this.pollTimer.Interval = 60000D;
            this.pollTimer.Elapsed += new System.Timers.ElapsedEventHandler(this.pollTimer_Elapsed);
            ((System.ComponentModel.ISupportInitialize)(this.ServiceTimer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pollTimer)).EndInit();

        }
    }
}
