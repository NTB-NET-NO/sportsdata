// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NFFDatafileCreator.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the NffDatafileCreator type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Components.Nff
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Xml;
    using NTB.SportsData.Service;
    using NTB.SportsData.Service.Components.Factories;
    using NTB.SportsData.Service.Components.Interfaces;
    using NTB.SportsData.Service.Components.Repositories;
    using NTB.SportsData.Service.Domain.Classes;
    using NTB.SportsData.Service.Domain.Enums;
    using log4net;

    using NTB.SportsData.Components.Nff;
    using NTB.SportsData.Service.Components.TeamSport;
    using NTB.SportsData.Utilities;

    // Adding support for log4net

    /// <summary>
    /// The nff datafile creator.
    /// </summary>
    public class NffDatafileCreator : IDataFileCreator 
    {
        /// <summary>
        /// Static logger
        /// </summary>
        public static readonly ILog Logger = LogManager.GetLogger(typeof(NffDatafileCreator));

        /// <summary>
        /// The district id.
        /// </summary>
        public int DistrictId;

        /// <summary>
        /// The _nff push datafile creator.
        /// </summary>
        private readonly NffPushDatafileCreator _nffPushDatafileCreator;

        /// <summary>
        /// The _date duration.
        /// </summary>
        private int _dateDuration;

        /// <summary>
        /// Initializes a new instance of the <see cref="NffDatafileCreator"/> class.
        /// </summary>
        public NffDatafileCreator()
        {
            {
                // Set up logger
                log4net.Config.XmlConfigurator.Configure();

                if (!LogManager.GetRepository().Configured)
                {
                    log4net.Config.BasicConfigurator.Configure();
                }

                _nffPushDatafileCreator = new NffPushDatafileCreator();

                // _nffXmlDocument = new NffXmlDocument(this);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether standings.
        /// </summary>
        public bool Standings { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether match facts.
        /// </summary>
        public bool MatchFacts { get; set; }

        #region getters and setters

        /// <summary>
        /// Gets or sets the output folder.
        /// </summary>
        public string OutputFolder { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether update database.
        /// </summary>
        public bool UpdateDatabase { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether create xml.
        /// </summary>
        public bool CreateXml { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether render result.
        /// </summary>
        public bool RenderResult { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether schedule message.
        /// </summary>
        public bool ScheduleMessage { get; set; }

        /// <summary>
        /// Gets or sets the date start.
        /// </summary>
        public DateTime DateStart { get; set; }

        /// <summary>
        /// Gets or sets the date end.
        /// </summary>
        public DateTime DateEnd { get; set; }

        /// <summary>
        /// Gets or sets the customers.
        /// </summary>
        public List<Customer> Customers { get; set; }

        /// <summary>
        /// Gets or sets the job name.
        /// </summary>
        public string JobName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether total schedule.
        /// </summary>
        public bool TotalSchedule { get; set; }

        /// <summary>
        /// Gets or sets the duration.
        /// </summary>
        public int Duration
        {
            get
            {
                return _dateDuration;
            }

            set
            {
                if (value > 0)
                {
                    _dateDuration = value;
                }
            }
        }

        /// <summary>
        /// Gets the seasons.
        /// </summary>
        /// <exception cref="ArgumentNullException">
        /// If we get an Argument Null Exception we will report it
        /// </exception>
        public List<Season> Seasons
        {
            get
            {
                var facade = new FacadeFactory().GetFacade(SportId);

                var seasons = new List<Season>();

                // Get the differnet stuff that I need
                var list = facade.GetSeasons();
                Logger.Debug("Getting seasons!");

                if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]) == false)
                {
                    List<Season> filteredSeason = (from s in list where s.SeasonActive select s).ToList();
                    seasons.AddRange(filteredSeason);

                }
                else
                {
                    Season filteredSeason =
                        (from s in list
                         where s.SeasonActive && s.SeasonStartDate <= DateTime.Today && s.SeasonEndDate >= DateTime.Today
                         select s).Single();

                    seasons.Add(filteredSeason);
                }

                return seasons;
            }

            internal set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }

                Seasons = value;
            }
        }

        /// <summary>
        /// Gets the nff push datafile creator.
        /// </summary>
        public NffPushDatafileCreator NffPushDatafileCreator
        {
            get { return _nffPushDatafileCreator; }
        }

        /// <summary>
        /// Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }
        
        #endregion getters and setters

        /// <summary>
        /// This method is used to configure the datafile object. It shall check if we have rows in Tournament and Districts Databases
        /// </summary>
        /// <returns>
        /// Returns true if we were able to configure the component or dll
        /// </returns>
        public bool Configure(int sportId)
        {
            try
            {
                Logger.Debug("Configuring DatafileCreator");

                TableDataMapper dataTableMapper = new TableDataMapper();

                // First we check when this tournament was last checked - if we don't have to configure, then we return
                if (dataTableMapper.GetTableCheck() != null)
                {
                    // We set a value and return
                    dataTableMapper.SetTableCheck();
                    return true;
                }

                var facade = new FacadeFactory().GetFacade(sportId);

                var configurator = new DataFileConfigurator();
                configurator.ConfigureAgeCategory(sportId);
                configurator.ConfigureSeason(sportId);
                configurator.ConfigureDistricts(sportId);

                List<District> districts = facade.GetDistricts();

                DistrictDataMapper districtDataMapper = new DistrictDataMapper();
                // Check the districts-table. If we get a false-return, we are stopping configuration
                if (districtDataMapper.CheckDistrictsTable(districts, sportId) != true)
                {
                    return false;
                }

                configurator.ConfigureMunicipalities(districts, sportId);

                
                foreach (District district in districts)
                {
                    configurator.ConfigureTournaments(district, sportId);
                    configurator.ConfigureMatches(district, sportId);
                }

                return true;
            }
            catch (Exception e)
            {
                Logger.Error("An exception has occured: " + e);
                return false;
            }
        }

        public List<Match> CreateTotalSchedule(int sportId, List<Season> seasons)
        {
            throw new NotImplementedException();
        }

        public void CreateDataFile(List<Customer> customers, int eventId, DateTime startDateTime, DateTime endDateTime, int sportId)
        {
            throw new NotImplementedException();
        }

        public void CreateDataFileMaintenance(int eventId, int sportId)
        {
            throw new NotImplementedException();
        }

        public AgeCategory GetAgeCategoryByTournament(Tournament tournament, int sportId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Getting a list of matches to be used to get data from API
        /// </summary>
        /// <param name="matchDateTime"></param>
        /// <param name="sportId"></param>
        /// <returns></returns>
        public List<Match> GetMatchesByDate(DateTime matchDateTime, int sportId)
        {
            // first we check against database, then we check against API
            var matchDataMapper = new MatchDataMapper();
            return matchDataMapper.GetMatchesByDate(matchDateTime, sportId);
        }

        /// <summary>
        ///     Get list of customers who shall receive these sports data
        /// </summary>
        /// <param name="tournament"></param>
        /// <returns></returns>
        public List<Customer> GetCustomerList(Tournament tournament)
        {
            var customerDataMapper = new CustomerDataMapper();
            return customerDataMapper.GetListOfCustomersByTournament(tournament);
        }


        /// <summary>
        /// The create customer data file.
        /// </summary>
        /// <param name="customerId">
        /// The customer id.
        /// </param>
        /// <param name="remoteCustomerNumber">
        /// The remote customer number.
        /// </param>
        /// <param name="xmlNodeList">
        /// The xml node list.
        /// </param>
        /// <param name="startDateTime">
        /// The start date time.
        /// </param>
        /// <param name="endDateTime">
        /// The end date time.
        /// </param>
        public void CreateCustomerDataFile(
            int customerId,
            int remoteCustomerNumber,
            XmlNodeList xmlNodeList,
            DateTime startDateTime,
            DateTime endDateTime)
        {
            Logger.Debug("We are in CreateCustomerDataFile!");

            // Creating the customer Object
            List<Customer> customers = new List<Customer>();

            // Creating the SportsDatabase object
            // NffSportsDataDatabase db = new NffSportsDataDatabase();
            CustomerDataMapper customerDataMapper = new CustomerDataMapper();
            Logger.Debug("We have gotten database object");

            Customer customer = customerDataMapper.Get(customerId);

            customers.Add(customer);

            RenderResult = true;

            try
            {
                var facade = new FacadeFactory().GetFacade(SportId);
                List<AgeCategory> ageCategory = 
                    facade.GetAgeCategories();

                foreach (XmlNode tournamentNode in xmlNodeList)
                {
                    // Checking that we don't have an empty attribute
                    if (tournamentNode.Attributes == null)
                    {
                        continue;
                    }

                    int tournamentId = Convert.ToInt32(tournamentNode.Attributes["id"].Value);

                    // Getting the Tournament Object based on the TournamentId
                    Tournament tournament = facade.GetTournament(tournamentId);

                    Logger.Debug("Tournament:" + tournament.TournamentName + ".");
                    Logger.Debug("Done getting Tournament Object");

                    AgeCategory filteredAgeCategory = (from act in ageCategory
                                                                 where act.AgeCategoryId == tournament.AgeCategoryId
                                                                 select act).Single();

                    Logger.Info("Checking for Creating Datafile for tournament: " + tournamentId);

                    // Now that we have some information about the tournament, we can do more
                    try
                    {
                        List<Match> matches =
                            facade.GetMatchesByTournament(
                                tournamentId, true, true, true, true, null);

                        for (DateTime currentDate = DateTime.Parse(startDateTime.ToShortDateString());
                             endDateTime.CompareTo(currentDate) > 0;
                             currentDate = currentDate.AddDays(1.0))
                        {
                            List<Match> filteredMatches =
                                (from m in matches
                                 where m.MatchStartDate != null && m.MatchStartDate.Value.ToShortDateString() == currentDate.ToShortDateString()
                                 select m).ToList();

                            if (filteredMatches.Count == 0)
                            {
                                Logger.Info(
                                    "No matches found in tournament " + tournamentId + " on date: " + currentDate);
                                continue;
                            }

                            // Creating the Standings object that we will be using when creating XML
                            List<TournamentTableTeam> standings = null;

                            // Checking if this tournament is one that 
                            if (tournament.PublishTournamentTable)
                            {
                                Logger.Debug("Getting the Standings");

                                try
                                {
                                    standings =
                                        new List<TournamentTableTeam>(facade.GetTournamentStanding(tournamentId));
                                }
                                catch (Exception exception)
                                {
                                    Logger.Debug(exception.Message);
                                    if (exception.Message == "No tournament standings found.")
                                    {
                                        Logger.Info("No standings found for tournament " + tournament.TournamentName);
                                    }
                                }
                            }
                            else
                            {
                                Logger.Info(
                                    "The standings from tournament " + tournament.TournamentName
                                    + " cannot publish results!");
                            }

                            Logger.Debug("Creating XML-structure!");

                            // Creating the XMLDocument
                            
                            XmlDocument doc = new XmlDocumentCreator().CreateXmlStructure(
                                filteredMatches, tournament, filteredAgeCategory, standings, false, customers);

                            // Creating the filename
                            List<Filename> fileNames = NffXmlDocument.CreateFileName(
                                tournament.DistrictId, tournament.TournamentId, currentDate, SportId);

                            if (doc != null)
                            {
                                // Now creating the XML-file
                                Logger.Debug("Creating XML-File");
                                NffXmlDocument.CreateXmlFile(
                                    doc,
                                    tournament.TournamentName,
                                    tournament.TournamentId,
                                    fileNames,
                                    tournament.DistrictId);
                            }
                        }
                    }
                    catch (Exception exception)
                    {
                        Logger.Error(exception.Message);
                        Logger.Error(exception.StackTrace);

                        if (exception.InnerException != null)
                        {
                            Logger.Error(exception.InnerException.Message);
                            Logger.Error(exception.InnerException.StackTrace);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
        }

        /// <summary>
        /// The create customer data file.
        /// </summary>
        /// <param name="customerId">
        /// The customer id.
        /// </param>
        /// <param name="remoteCustomerId">
        /// The remote Customer Id.
        /// </param>
        /// <param name="tournamentId">
        /// The tournament id.
        /// </param>
        /// <param name="selectedStartDate">
        /// The start date.
        /// </param>
        /// <param name="selectedEndDate">
        /// The end date.
        /// </param>
        public void CreateCustomerDataFile(int customerId, int remoteCustomerId, int tournamentId, DateTime selectedStartDate, DateTime selectedEndDate)
        {
            Logger.Debug("We are in CreateCustomerDataFile!");

            // Creating the customer Object
            List<Customer> customers = new List<Customer>();
            
            // Creating the SportsDatabase object
            CustomerDataMapper customerDataMapper = new CustomerDataMapper();
            Logger.Debug("We have gotten database object");

            Customer customer = customerDataMapper.Get(customerId);

            customers.Add(customer);

            RenderResult = true;
            
            try
            {
                var facade = new FacadeFactory().GetFacade(SportId);
                // Getting the Tournament Object based on the TournamentId
                Tournament tournament = facade.GetTournament(tournamentId);
                List<AgeCategory> ageCategoryTournament =
                    facade.GetAgeCategories().ToList();

                AgeCategory filteredAgeCategory = (from act in ageCategoryTournament
                                                             where act.AgeCategoryId == tournament.AgeCategoryId
                                                            select act).Single();

                Logger.Debug("Tournament:" + tournament.TournamentName + ".");
                Logger.Debug("Done getting Tournament Object");

                // Now that we have some information about the tournament, we can do more
                List<Match> matches = facade.GetMatchesByTournament(tournamentId, true, true, true, true, null).ToList();

                Logger.Info("No matches found in tournament " + tournamentId + " on date: " + selectedStartDate);
                List<Match> filteredMatches =
                    (from m in matches where m.MatchStartDate == selectedStartDate select m).ToList();

                if (filteredMatches.Count == 0)
                {
                    return;
                }

                // Creating the Standings object that we will be using when creating XML
                List<TournamentTableTeam> standings = null;

                // Checking if this tournament is one that 
                if (tournament.PublishTournamentTable)
                {
                    Logger.Debug("Getting the Standings");

                    try
                    {
                        standings = new List<TournamentTableTeam>(facade.GetTournamentStanding(tournamentId));
                    }
                    catch (Exception exception)
                    {
                        Logger.Debug(exception.Message);
                        if (exception.Message == "No tournament standings found.")
                        {
                            Logger.Info("No standings found for tournament " + tournament.TournamentName);
                        }
                    }
                }
                else
                {
                    Logger.Info("The standings from tournament " + tournament.TournamentName + " cannot publish results!");
                }

                Logger.Debug("Creating XML-structure!");

                // Creating the XMLDocument
                XmlDocument doc = NffXmlDocument.CreateXmlStructure(filteredMatches, tournament, filteredAgeCategory, standings, false, customers);

                // Creating the filename
                List<Filename> fileNames = NffXmlDocument.CreateFileName(tournament.DistrictId, tournament.TournamentId, selectedStartDate, SportId);

                if (doc != null)
                {
                    // Now creating the XML-file
                    Logger.Debug("Creating XML-File");
                    NffXmlDocument.CreateXmlFile(
                        doc,
                        tournament.TournamentName,
                        tournament.TournamentId,
                        fileNames,
                        tournament.DistrictId);
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
        }

        /// <summary>
        ///     returns a list of matches fetched from remote data source
        /// </summary>
        /// <param name="tournamentId"></param>
        /// <param name="tournamentDate"></param>
        /// <returns></returns>
        public List<Match> GetMatchesByTournament(int tournamentId, DateTime tournamentDate)
        {
            var facade = new FacadeFactory().GetFacade(SportId);

            try
            {
                // Getting the Tournament Object based on the TournamentId
                Tournament tournament = facade.GetTournament(tournamentId);

                Logger.Debug("Tournament:" + tournament.TournamentName + ".");
                Logger.Debug("Done getting Tournament Object");

                // Now that we have some information about the tournament, we can do more
                List<Match> matches =
                    facade.GetMatchesByTournament(tournamentId, true, true, true, true, tournamentDate.Date).ToList();

                // Filtering matches so we know they are for today...
                List<Match> filteredMatches =
                    (from m in matches where m.MatchStartDate == tournamentDate.Date select m).ToList();

                return filteredMatches;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                return null;
            }
        }

        public void CreateDataFile(List<Customer> customers, int eventId, DateTime startDateTime, DateTime endDateTime)
        {
            Logger.Debug("We are in CreateTournamentDataFile!");

            RenderResult = true;

            var facade = new FacadeFactory().GetFacade(SportId);

            try
            {
                // Getting the Tournament Object based on the TournamentId
                Tournament tournament = facade.GetTournament(eventId);

                Logger.Debug("Tournament:" + tournament.TournamentName + ".");
                Logger.Debug("Done getting Tournament Object");

                // Now that we have some information about the tournament, we can do more
                List<Match> matches = facade.GetMatchesByTournament(eventId, true, true, true, true, startDateTime).ToList();

                for (DateTime currentDate = DateTime.Parse(startDateTime.ToShortDateString());
                     endDateTime.CompareTo(currentDate) > 0;
                     currentDate = currentDate.AddDays(1.0))
                {
                    List<Match> filteredMatches =
                        (from m in matches
                         where m.MatchStartDate != null && m.MatchStartDate.Value.ToShortDateString() == currentDate.ToShortDateString()
                         select m).ToList();

                    if (filteredMatches.Count == 0)
                    {
                        Logger.Info("No matches found in tournament " + eventId + " on date: " + currentDate);
                        continue;
                    }

                    // Creating the Standings object that we will be using when creating XML
                    List<TournamentTableTeam> standings = null;

                    // Checking if this tournament is one that 
                    if (tournament.PublishTournamentTable)
                    {
                        Logger.Debug("Getting the Standings");

                        try
                        {
                            standings =
                                new List<TournamentTableTeam>(
                                    facade.GetTournamentStanding(eventId));
                        }
                        catch (Exception exception)
                        {
                            Logger.Debug(exception.Message);
                            if (exception.Message == "No tournament standings found.")
                            {
                                Logger.Info("No standings found for tournament " + tournament.TournamentName);
                            }
                        }
                    }
                    else
                    {
                        Logger.Info(
                            "The standings from tournament " + tournament.TournamentName + " cannot publish results!");
                    }

                    Logger.Debug("Creating XML-structure!");

                    AgeCategory ageCategoryTournament = facade.GetAgeCategories().Single(ageCategory => ageCategory.AgeCategoryId == tournament.AgeCategoryId);

                    // Creating the XMLDocument
                    XmlDocument doc = NffXmlDocument.CreateXmlStructure(
                        filteredMatches, tournament, ageCategoryTournament, standings, false, customers);

                    // Creating the filename
                    List<Filename> fileNames = NffXmlDocument.CreateFileName(
                        tournament.DistrictId, tournament.TournamentId, currentDate, SportId);

                    if (doc == null)
                    {
                        continue;
                    }

                    // Now creating the XML-file
                    Logger.Debug("Creating XML-File");
                    NffXmlDocument.CreateXmlFile(
                        doc, tournament.TournamentName, tournament.TournamentId, fileNames, tournament.DistrictId);
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
        }

        /// <summary>
        /// The create data file maintenance.
        /// </summary>
        /// <param name="matchId">
        /// The match id.
        /// </param>
        public void CreateDataFileMaintenance(int matchId)
        {
            var facade = new FacadeFactory().GetFacade(SportId);

            Logger.Debug("We are in CreateDataFileMaintenance!");

            // Creating the SportsDatabase object
            Logger.Debug("We have gotten database object");

            // This could mean that we don't have the data in our database, then we should check with the service
            Tournament tournament = new Tournament();
            int tournamentId = 0;
            
            Match match = null;

            MatchDataMapper matchDataMapper = new MatchDataMapper();

            try
            {
                // Now we that have an ID, we shall get todays matches
                Logger.Debug("Getting the Matches Object and putting it into the Match list using TournamentId");
                match = facade.GetMatchByMatchId(matchId);
                
                // Check if match is in the database
                match.SportId = SportId;
                matchDataMapper.FindMatchByMatch(match);

                Logger.Debug("This is match match.TournamentAgeCategoryId: " + match.TournamentAgeCategoryId);

                Logger.Debug("Getting Match object and using this to get the tournamentid: " + match.TournamentId);

                tournamentId = match.TournamentId;
                Logger.Debug("Now getting the Tournament Object based on the Tournament ID: " + tournamentId);

                // Getting the Tournament Object based on the TournamentId
                tournament = facade.GetTournament(tournamentId);

                Logger.Debug("Tournament:" + tournament.TournamentName + ".");
                Logger.Debug("Done getting Tournament Object");
            }
            catch (Exception exception)
            {
                if (exception.Message == "No matches found.")
                {
                    matchDataMapper.UpdateMatchNotFound(match, tournament);
                }

                Logger.Error(exception.Message);

                Logger.Error(exception.StackTrace);

                // Consider this one and remove if it gives to many e-mails
                Mail.SendException(exception);
            }

            List<Customer> customers = new List<Customer>();

            // Checking if the tournament is a national team tournament
            if (tournament.NationalTeamTournament == false)
            {
                // Now I can check against the database, because I now know that the match exists... 
                CustomerDataMapper customerDataMapper = new CustomerDataMapper();
                customers = customerDataMapper.GetListOfCustomersByMatch(match);
            }
            else
            {
                Customer customer = new Customer
                {
                    Name = "NTB"
                };

                customers.Add(customer);
            }

            Logger.Debug("Number of Customers: " + customers.Count());

            if (customers.Any())
            {
                Logger.Debug("Number of customers: " + customers.Count().ToString());
                foreach (Customer customer in customers)
                {
                    Logger.Debug("Customer: " + customer.Name);
                }

                // Seems kinda odd to ask for this when we already have it in the match object
                if (match != null)
                {
                    tournamentId = match.TournamentId;
                }

                Logger.Debug("Getting TournamentId: " + tournamentId);

                try
                {
                    Logger.Debug("publish result: " + tournament.PublishResult);
                    if (tournament.PublishResult)
                    {
                        Logger.Debug("publish result is true");

                        // Finding out if we are to get referee, squad and match events for this tournament
                        int? agecatid = tournament.AgeCategoryId;

                        List<AgeCategory> tournamentages = new List<AgeCategory>(facade.GetAgeCategories());

                        AgeCategory filteredAgeCategories =
                            tournamentages.Find(t =>
                                                t.AgeCategoryId == agecatid &&
                                                t.MaxAge >= 12);

                        // Setting some variables to false
                        bool getSquad = false;
                        bool getReferee = false;
                        bool getEvents = false;

                        // We only want squad, referee and events from mathces that are in the AgeCategory Toppfotball
                        Logger.Debug("Checking if we shall have more data");

                        if (filteredAgeCategories != null)
                        {
                            // Do I need to do this as I am filtering below?
                            if (filteredAgeCategories.MaxAge >= 12)
                            {
                                // Checking if this is a toppfotball classification
                                if (filteredAgeCategories.AgeCategoryName == "Toppfotball")
                                {
                                    Logger.Debug("Toppfotball!");
                                    getSquad = true;
                                    getReferee = true;
                                    getEvents = true;
                                }

                                Logger.Debug("Getting all matches in this tournament for this day!");
                                try
                                {
                                    List<Match> matches = new List<Match>(facade.GetMatchesByTournament(tournamentId, getSquad, getReferee, true, getEvents, null));

                                    // I guess I need to have some Linq-filter here
                                    {
                                        var filteredMatches =
                                            matches.FindAll(m =>
                                                            m.MatchStartDate == DateTime.Today);

                                        // If we have hits, we shall 
                                        if (filteredMatches.Any())
                                        {
                                            Logger.Debug("Number of hits: " + filteredMatches.Count());

                                            // And populating it with filteredMatches instead
                                            matches = filteredMatches;
                                        }
                                    }

                                    // Creating the Standings object that we will be using when creating XML
                                    List<TournamentTableTeam> standings = null;

                                    // Checking if this tournament is one that 
                                    if (tournament.PublishTournamentTable)
                                    {
                                        Logger.Debug("Getting the Standings");

                                        try
                                        {
                                            standings = new List<TournamentTableTeam>(facade.GetTournamentStanding(tournamentId));
                                        }
                                        catch (Exception exception)
                                        {
                                            Logger.Debug(exception.Message);
                                            if (exception.Message == "No tournament standings found.")
                                            {
                                                Logger.Info("No standings found for tournament " + tournament.TournamentName);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Logger.Info("The standings from tournament " + tournament.TournamentName + " cannot publish results!");
                                    }

                                    Logger.Debug("Creating XML-structure!");

                                    // Creating filename
                                    DistrictId = tournament.DistrictId;

                                    // Creating the XMLDocument
                                    XmlDocument doc = NffXmlDocument.CreateXmlStructure(matches, tournament, filteredAgeCategories, standings, false, customers);

                                    // Creating the filename
                                    List<Filename> fileNames = NffXmlDocument.CreateFileName(tournament.DistrictId, tournament.TournamentId, null, SportId);

                                    if (doc != null)
                                    {
                                        // Now creating the XML-file
                                        Logger.Debug("Creating XML-File");
                                        NffXmlDocument.CreateXmlFile(
                                            doc,
                                            tournament.TournamentName,
                                            tournament.TournamentId,
                                            fileNames,
                                            DistrictId);
                                    }
                                }
                                catch (Exception exception)
                                {
                                    if (exception.Message == "No matches found.")
                                    {

                                        // sportsDataDatabase.UpdateMatchNotFound(match, tournament);
                                    }
                                    else
                                    {
                                        Logger.Error(exception.Message);
                                        Logger.Error(exception.StackTrace);

                                        // Consider this one and remove if it sends to many mails
                                        Mail.SendException(exception);
                                    }
                                }
                            } // End of MaxAge-check
                        } // End of object-check
                    }
                    else
                    {
                        Logger.Info("The results from tournament " + tournament.TournamentName + " cannot publish results!");
                    }
                }
                catch (Exception exception)
                {
                    Logger.Info("We have received an exception while doing match or tournament lookup");
                    Logger.Info("====================================================================");
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
            }
            else
            {
                Logger.Debug("No customers is receiving this result");
            }
        }

        #region createDataFileByDistrict

        /// <summary>
        /// The create data file by district.
        /// </summary>
        public void CreateDataFileByDistrict()
        {
            Logger.Debug("================================");
            Logger.Debug("== In CreateDataFileByDistric ==");
            Logger.Debug("================================");

            var facade = new FacadeFactory().GetFacade(SportId);

            List<District> districts;
            try
            {
                districts = facade.GetDistricts();
            }
            catch (Exception e)
            {
                Logger.Error("An exception has occured!", e);
                
                Logger.Info("Cannot get Districts. Connection to server might be down");

                var districtDataMapper = new DistrictDataMapper();

                // Getting the districts from the database... (backup)
                districts = districtDataMapper.GetAll().ToList();
            }

            try
            {
                // During testing we are to use last season (season 2010)
                Logger.Debug("Getting Age Categories!");

                List<AgeCategory> filteredAgeCategories = GetAgeCategoryTournament();

                Logger.Debug("filteredAgeCategories.Length: " + filteredAgeCategories.Count());

                foreach (District district in districts)
                {
                    // Getting todays matches
                    List<Match> matches = null;

                    var districtName = district.DistrictName;

                    DistrictId = Convert.ToInt32(district.DistrictId);

                    Logger.Debug("DistictID: " + DistrictId + " Name: " + districtName);

                    // Getting tournaments based on districtID and SeasonID
                    bool includeSquad = false;

                    const bool includeReferees = false;

                    bool includeResults = false;

                    bool includeEvents = false;

                    if (RenderResult)
                    {
                        includeResults = RenderResult;
                        includeSquad = true;
                        includeEvents = true;
                    }

                    if (Duration == 0)
                    {
                        /*
                         * If we add timestamp you will get changes after the date you are adding. If you use NULL the timestamp is not being taken care of.
                         * 
                         */
                        Logger.Debug("Duration is zero. Getting matches by District!");
                        try
                        {
                            // We might get that there aren't any matches, so we don't do much if there is such an incident.
                            if (DateStart == null)
                            {
                                DateStart = DateTime.Today;
                                DateEnd = DateStart;
                            }

                            if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                            {
                                DateStart = DateTime.Today.AddDays(-1);
                                DateEnd = DateTime.Today.AddDays(-1);
                            }   

                            matches = facade.GetMatchesByDistrict(DistrictId, DateStart, DateEnd, includeSquad, includeReferees, includeResults, includeEvents, DateStart).ToList();

                            foreach (Match match in matches)
                            {
                                MatchDataMapper dataMapper = new MatchDataMapper();
                                dataMapper.FindMatchByMatch(match);
                            }
                        }
                        catch (Exception exception)
                        {
                            if (exception.Message.ToLower() != "no matches found.")
                            {
                                Logger.Error(exception.Message);
                                Logger.Error(exception.StackTrace);
                            }
                        }
                    }

                    if (Duration > 0)
                    {
                        Logger.Debug("Duration is bigger than zero. Getting matches by DateInterval!");
                        try
                        {
                            if (DateStart == null)
                            {
                                DateStart = DateTime.Today.AddDays(-1);
                                DateEnd = DateTime.Today;
                            }

                            // We might get no-matches-found error
                            
                            matches = facade.GetMatchesByDateInterval(DateStart, DateEnd, DistrictId, includeSquad, includeReferees, includeResults, includeEvents).ToList();
                        }
                        catch (Exception exception)
                        {
                            if (exception.Message.ToLower() != "no matches found.")
                            {
                                Logger.Error(exception.Message);
                                Logger.Error(exception.StackTrace);
                            }
                        }
                    }

                    try
                    {
                        /*
                         * If we add timestamp you will get changes after the date you are adding. If you use NULL the timestamp is not being taken care of.
                         * 
                         */
                        foreach (Season season in Seasons)
                        {
                            try
                            {
                                Logger.Info("Getting TournamentsbyDistrict: " + districtName + ". Season: " + season.SeasonId);

                                List<Tournament> tournaments =
                                    new List<Tournament>(
                                        facade.GetTournamentsByDistrict(DistrictId, season.SeasonId));

                                // We are looping through the Age Categories
                                foreach (AgeCategory ageCategoryTournament in filteredAgeCategories)
                                {
                                    List<Tournament> filteredTournaments =
                                        tournaments.FindAll(
                                            t =>
                                            t.AgeCategoryId == ageCategoryTournament.AgeCategoryId);

                                    Logger.Debug("ageCategoryTournament: " + ageCategoryTournament.AgeCategoryName);

                                    if (!filteredTournaments.Any())
                                    {
                                        continue;
                                    }

                                    foreach (Tournament tournament in filteredTournaments)
                                    {
                                        Tournament currentTournament = tournament;

                                        List<Match> filteredMatches = (from m in matches
                                                                       where
                                                                           m.MatchStartDate != null && (m.TournamentId == currentTournament.TournamentId
                                                                                                        && m.MatchStartDate.Value.Date <= DateStart.Date
                                                                                                        && m.MatchStartDate.Value.Date  >= DateEnd.Date)
                                                                       select m).ToList();

                                        AgeCategory filteredAgeCategory =
                                            (from act in filteredAgeCategories
                                             where act.AgeCategoryId == currentTournament.AgeCategoryId
                                             select act).Single();

                                        // If we get matches, we shall create the datafile.
                                        if (filteredMatches.Any())
                                        {
                                            Logger.Debug("Total matches found: " + filteredMatches.Count());
                                            NffXmlDocument.CreateDataFile(
                                                tournament,
                                                filteredMatches,
                                                RenderResult,
                                                SportId,
                                                filteredAgeCategory);
                                        }
                                    }
                                }
                            }
                            catch (Exception exception)
                            {
                                Logger.Error(exception.Message);
                                if (exception.Message.ToLower() != "no tournaments found.")
                                {
                                    Logger.Error(exception.StackTrace);
                                }
                            }
                        }
                    }
                    catch (Exception exception)
                    {
                        Logger.Error(exception);
                        Logger.Error(exception.StackTrace);
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                Logger.Error(exception.StackTrace);
            }
        }

        

        #endregion createDataFileByDistrict

        /// <summary>
        /// Function to get the districtname based on the getDistricts-method in the webservice.
        /// </summary>
        /// <param name="districtId">integer to be used to find the right name of the district</param>
        /// <returns>
        /// Returns either the district name or an empty string
        /// </returns>
        public District GetDistrict(int districtId)
        {
            try
            {
                var facade = new FacadeFactory().GetFacade(SportId);
                var districts = facade.GetDistricts();
                
                foreach (District district in districts)
                {
                    if (district.DistrictId == districtId)
                    {
                        return district.DistrictName;
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Info(exception.Message);
                Logger.Info(exception.StackTrace);
            }

            return string.Empty;
        }

        /// <summary>
        /// The create total schedule.
        /// </summary>
        public void CreateTotalSchedule()
        {
            var facade = new FacadeFactory().GetFacade(SportId);


            try
            {
                var districts = facade.GetDistricts();

                List<AgeCategory> tournamentAgeCategory = facade.GetAgeCategories();
                List<AgeCategory> filteredAgeCategories = new List<AgeCategory>();
                filteredAgeCategories.AddRange(facade.GetAgeCategories());

                foreach (District district in districts)
                {
                    foreach (Season season in Seasons)
                    {
                        foreach (AgeCategory ageCategoryTournament in filteredAgeCategories)
                        {
                            List<Tournament> tournaments =
                                facade.GetTournamentsByDistrict(district.DistrictId, season.SeasonId).ToList();

                            List<Tournament> filteredTournaments =
                                tournaments.FindAll(
                                    t => t.AgeCategoryId == ageCategoryTournament.AgeCategoryId);

                            foreach (Tournament tournament in filteredTournaments)
                            {
                                List<Match> matches =
                                    facade.GetMatchesByTournament(tournament.TournamentId, false, false, false, false)
                                          .ToList();

                                AgeCategory filteredAgeCategory = (from act in tournamentAgeCategory
                                                                   where act.AgeCategoryId == tournament.AgeCategoryId
                                                                   select act).Single();
                                NffXmlDocument.CreateDataFile(tournament, matches, RenderResult, SportId,
                                                              filteredAgeCategory);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error("An exception has occured!", e);

                Logger.Info("Cannot get Districts. Connection to server might be down");
            }
        }
        
        /// <summary>
        /// The get age category tournament.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        private List<AgeCategory> GetAgeCategoryTournament()
        {
            var facade = new FacadeFactory().GetFacade(SportId);

            List<AgeCategory> tournamentAgeCategory = facade.GetAgeCategories();
            List<AgeCategory> filteredAgeCategories =
                (from act in tournamentAgeCategory where act.MinAge >= 12 select act).ToList();

            return filteredAgeCategories;
        }

        public List<TournamentTableTeam> GetTournamentStanding(Tournament tournament, int sportId)
        {
            // This we have to get from the API
            var facade = new FacadeFactory().GetFacade(sportId);
            return facade.GetTournamentStanding(tournament.TournamentId);
        }

        public MessageType CreateDocumentType(bool renderResult, bool standing)
        {
            throw new NotImplementedException();
        }

        public Tournament GetTournamentById(int tournamentId, int sportId)
        {
            // This we have to get from the API
            var facade = new FacadeFactory().GetFacade(sportId);

            return facade.GetTournament(tournamentId);
        }

        public Organization GetOrganizationBySportId(int sportId)
        {
            // This we shall get from the database I think
            var repository = new OrganizationDataMapper();
            return repository.GetOrgIdBySportId(sportId);
        }
    }
}
