﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ComponentConfigurator.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The component configurator.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Components
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Threading;
    using System.Xml;

    using log4net;
    using log4net.Config;

    using NTB.SportsData.Service.Components.QuartzScheduler;
    using NTB.SportsData.Service.Domain.Classes;
    using NTB.SportsData.Service.Domain.Enums;

    using Quartz;

    /// <summary>
    /// The component configurator.
    /// </summary>
    public class ComponentConfigurator
    {
        /// <summary>
        ///     Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ComponentConfigurator));

        /// <summary>
        /// The _team sport component.
        /// </summary>
        private readonly TeamSportComponent teamSportComponent;

        /// <summary>
        /// Initializes static members of the <see cref="ComponentConfigurator"/> class.
        /// </summary>
        static ComponentConfigurator()
        {
            // Set up logger
            XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                BasicConfigurator.Configure();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentConfigurator"/> class.
        /// </summary>
        /// <param name="teamSportComponent">
        /// The team sport component.
        /// </param>
        public ComponentConfigurator(TeamSportComponent teamSportComponent)
        {
            this.teamSportComponent = teamSportComponent;
        }

        /// <summary>
        /// The configure.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        /// <returns>
        /// The <see cref="ComponentConfiguration"/>.
        /// </returns>
        /// <exception cref="ArgumentException">
        /// </exception>
        /// <exception cref="NotSupportedException">
        /// </exception>
        public ComponentConfiguration Configure(XmlNode configNode)
        {
            // Creating the ComponentConfiguration object
            var componentConfiguration = new ComponentConfiguration();

            // Setting Instance Name
            Logger.Debug("Node: " + configNode.Name);

            PollStyle pollStyle;
            if (configNode.Attributes != null)
            {
                componentConfiguration.InstanceName = configNode.Attributes.GetNamedItem("Name").ToString();
                Logger.Info("Configuring " + componentConfiguration.InstanceName);

                

                // Basic configuration sanity check
                if (configNode.Name != "TeamSportComponent" || configNode.Attributes.GetNamedItem("Name") == null)
                {
                    throw new ArgumentException("The XML configuration node passed is invalid", "configNode");
                }

                if (Thread.CurrentThread.Name == null)
                {
                    Thread.CurrentThread.Name = configNode.Attributes.GetNamedItem("Name").ToString();
                }

                

                #region Basic operation

                // Getting the OperationMode which tells if this what type of OperationMode this is
                try
                {
                    componentConfiguration.OperationMode = (OperationMode)Enum.Parse(typeof(OperationMode), configNode.Attributes["OperationMode"].Value, true);
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing OperationMode values in XML configuration node", ex);
                }

                if (configNode.Attributes != null && configNode.Attributes["MaintenanceMode"] != null)
                {
                    try
                    {
                        componentConfiguration.MaintenanceMode = (MaintenanceMode)Enum.Parse(typeof(MaintenanceMode), configNode.Attributes["MaintenanceMode"].Value, true);
                    }
                    catch (Exception ex)
                    {
                        ThreadContext.Stacks["NDC"].Pop();
                        throw new ArgumentException("Invalid or missing OperationMode values in XML configuration node", ex);
                    }
                }

                // Getting the PollStyle which tells if this what type of PollStyle this is
                try
                {
                    componentConfiguration.PollStyle = (PollStyle)Enum.Parse(typeof(PollStyle), configNode.Attributes["PollStyle"].Value, true);
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing PollStyle values in XML configuration node", ex);
                }

                // Getting the ScheduleType which tells if this what type of scheduling we are doing
                try
                {
                    if (configNode.Attributes != null && configNode.Attributes["ScheduleType"] != null)
                    {
                        componentConfiguration.ScheduleType = (ScheduleType)Enum.Parse(typeof(ScheduleType), configNode.Attributes["ScheduleType"].Value, true);
                    }
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing ScheduleType values in XML configuration node", ex);
                }
            }

            #endregion

            // Some values that we need.

            // This boolean variable is used to tell us that the database shall be populated or not
            componentConfiguration.PopulateDatabase = false;

            const bool purge = false;

            componentConfiguration.CreateXml = false;

            // This string is used to set the name of the job
            // Getting the name of this Component instance
            try
            {
                if (configNode.Attributes != null)
                {
                    componentConfiguration.Name = configNode.Attributes["Name"].Value;
                    componentConfiguration.Enabled = Convert.ToBoolean(configNode.Attributes["Enabled"].Value);
                }

                ThreadContext.Stacks["NDC"].Push(componentConfiguration.InstanceName);

                // This value is used to tell that we shall insert into database... I just wonder if we shall do it
                // on the schedule-level... Well, we try here first
                // Done: Consider using schedule-level to decide if we are to insert into database or not

                // Check if we are to insert the data into the database
                if (configNode.Attributes != null && configNode.Attributes["InsertDatabase"] != null)
                {
                    componentConfiguration.PopulateDatabase = Convert.ToBoolean(configNode.Attributes["InsertDatabase"].Value);
                }

                // Check if we are to create an XML-File
                if (configNode.Attributes != null && configNode.Attributes["XML"] != null)
                {
                    componentConfiguration.CreateXml = Convert.ToBoolean(configNode.Attributes["XML"].Value);
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to configure this job instance!", ex);
            }

            // Getting and setting the Federation Id and the Sport Id
            try
            {
                var federationNode = configNode.SelectSingleNode("Federation");

                if (federationNode != null && federationNode.Attributes["orgId"] != null)
                {
                    if (federationNode.Attributes["orgId"] != null)
                    {
                        componentConfiguration.FederationId = Convert.ToInt32(federationNode.Attributes["orgId"].Value);
                    }

                    if (federationNode.Attributes["sportId"] != null)
                    {
                        componentConfiguration.SportId = Convert.ToInt32(federationNode.Attributes["sportId"].Value);
                    }
                }
            }
            catch (Exception exception)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing Federation if and sport id values in XML configuration node", exception);
            }

            

            try
            {
                // Find the file folder to work with
                // Done: This could be a nodelist though - need to check if we get more than one result
                var folderNode = configNode.SelectSingleNode("Folders/Folder[@Type='XMLOutputFolder']");

                if (folderNode != null)
                {
                    componentConfiguration.FileOutputFolder = folderNode.InnerText;
                }

                Logger.InfoFormat("Gatherer job - Polling: {0} / File output folder: {1} / Enabled: {2}", Enum.GetName(typeof(PollStyle), componentConfiguration.PollStyle), componentConfiguration.FileOutputFolder, componentConfiguration.Enabled);
            }
            catch (Exception exception)
            {
                Logger.Fatal("Not possible to get output-folder", exception);
            }

            

            if (componentConfiguration.Enabled)
            {
                #region File folders to access

                // Checking if file folders exists
                this.CreateOutputFolder(componentConfiguration);

                #endregion

                // Creating Instance Name
                try
                {
                    // Switch on pollstyle
                    switch (componentConfiguration.PollStyle)
                    {
                        case PollStyle.Continous:
                            SetContinousInterval(configNode, componentConfiguration);
                            break;

                        case PollStyle.PushSubscribe:

                            // Code for Pubnub push technology
                            componentConfiguration.Channel = "THE_FIKS_PRODUCTION_CHANNEL";

                            componentConfiguration.SubscriberKey = ConfigurationManager.AppSettings["pubnub_subscriberkey"];

                            break;

                        case PollStyle.CalendarPoll:

                            // Getting the times for this job
                            var schedules = new List<Schedule>();

                            var nodeList = configNode.SelectNodes("../TeamSportComponent[@Name='" + componentConfiguration.InstanceName + "']/Schedules/Schedule");

                            var jobs = nodeList.Count;
                            Logger.Info("Number of jobs: " + jobs);

                            foreach (XmlNode node in nodeList)
                            {
                                var schedule = new Schedule();
                                if (node.Attributes["Id"] != null)
                                {
                                    schedule.ScheduleId = node.Attributes["Id"].Value;
                                }

                                if (node.Attributes["Time"] != null)
                                {
                                    schedule.Time = node.Attributes["Time"].Value;
                                }

                                if (node.Attributes["Results"] != null)
                                {
                                    schedule.Results = Convert.ToBoolean(node.Attributes["Results"].Value);
                                }

                                if (node.Attributes["Duration"] != null)
                                {
                                    schedule.Duration = Convert.ToInt32(node.Attributes["Duration"].Value);
                                }

                                if (node.Attributes["DayOfWeek"] != null)
                                {
                                    schedule.DayOfWeek = Convert.ToInt32(node.Attributes["DayOfWeek"].Value);
                                }

                                if (node.Attributes["Week"] != null)
                                {
                                    schedule.Week = Convert.ToInt32(node.Attributes["Week"].Value);
                                }

                                if (node.Attributes["Offset"] != null)
                                {
                                    schedule.Offset = Convert.ToInt32(node.Attributes["Offset"].Value);
                                }

                                if (node.Attributes["PushSchedule"] != null)
                                {
                                    schedule.PushSchedule = Convert.ToBoolean(node.Attributes["PushSchedule"].Value);
                                }

                                schedules.Add(schedule);
                            }

                            componentConfiguration.Schedules = schedules;

                            break;

                        case PollStyle.Scheduled:
                            throw new NotSupportedException("Invalid polling style for this job type");

                        case PollStyle.FileSystemWatch:
                            throw new NotSupportedException("Invalid polling style for this job type");

                        default:

                            // Unsupported pollstyle for this object
                            throw new NotSupportedException("Invalid polling style for this job type (" + componentConfiguration.PollStyle + ")");
                    }
                }
                catch (Exception
                    ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing PollStyle-specific values in XML configuration node: " + ex.Message, ex);
                }
            }

            // Finish configuration
            ThreadContext.Stacks["NDC"].Pop();
            componentConfiguration.Configured = true;

            return componentConfiguration;
        }

        /// <summary>
        /// The create output folder.
        /// </summary>
        /// <param name="componentConfiguration">
        /// The component configuration.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        private void CreateOutputFolder(ComponentConfiguration componentConfiguration)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The job builder creator.
        /// </summary>
        /// <param name="componentConfiguration">
        /// The component configuration.
        /// </param>
        /// <param name="scheduleId">
        /// The schedule id.
        /// </param>
        /// <param name="operationMode">
        /// The operation mode.
        /// </param>
        /// <returns>
        /// The <see cref="IJobDetail"/>.
        /// </returns>
        private static IJobDetail JobBuilderCreator(ComponentConfiguration componentConfiguration, string scheduleId, OperationMode operationMode)
        {
            IJobDetail jobDetail = null;

            var identity = "job_" + componentConfiguration.InstanceName + "_" + scheduleId;
            if (operationMode == OperationMode.Gatherer)
            {
                jobDetail = JobBuilder.Create<TeamSportJobBuilder>().WithIdentity(identity, "groupTeamSport").WithDescription(componentConfiguration.InstanceName).Build();
            }
            else if (operationMode == OperationMode.Purge)
            {
                jobDetail = JobBuilder.Create<PurgeSportsData>().WithIdentity(identity, "groupTeamSport").WithDescription(componentConfiguration.InstanceName).Build();
            }

            return jobDetail;
        }

        /// <summary>
        /// The set continous interval.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        /// <param name="componentConfiguration">
        /// The component configuration.
        /// </param>
        private static void SetContinousInterval(XmlNode configNode, ComponentConfiguration componentConfiguration)
        {
            if (configNode.Attributes != null)
            {
                componentConfiguration.Interval = Convert.ToInt32(configNode.Attributes["Interval"].Value) * 1000;
            }

            if (componentConfiguration.Interval == 0)
            {
                componentConfiguration.Interval = 5000;
            }
        }
    }
}