﻿using System.Collections.Generic;
using NTB.SportsData.Service.Domain.Classes;
using NTB.SportsData.Service.Domain.Enums;
using NTB.SportsData.Utilities;

namespace NTB.SportsData.Service.Components
{
    public class ComponentConfiguration
    {
        public bool Configured { get; set; }
        public string Name { get; set; }
        public bool CreateXml { get; set; }
        public bool InsertDatabase { get; set; }
        public bool Enabled { get; set; }
        public bool Results { get; set; }
        public bool PopulateDatabase { get; set; }
        public bool DataBasePopulated { get; set; }
        public string FileOutputFolder { get; set; }
        public ScheduleType ScheduleType { get; set; }
        public PollStyle PollStyle { get; set; }
        public MaintenanceMode MaintenanceMode { get; set; }
        public OperationMode OperationMode { get; set; }
        public int Interval { get; set;}
        public string Channel { get; set; }
        public string SubscriberKey { get; set; }
        public string InstanceName { get; set; }
        public int SportId { get; set; }
        public int FederationId { get; set; }
        public List<Schedule> Schedules { get; set; }
    }
}
