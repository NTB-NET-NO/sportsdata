﻿

namespace NTB.SportsData.Service.Components
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Configuration;
    using System.Linq;
    using System.Net.Security;
    using System.Security.Cryptography.X509Certificates;
    using System.Threading;
    using System.Xml;
    
    using log4net;

    using NTB.SportsData.Service.Components.Factories;
    using NTB.SportsData.Service.Components.Interfaces;
    using NTB.SportsData.Service.Components.Nff;
    using NTB.SportsData.Service.Components.Repositories;
    using NTB.SportsData.Service.Components.TeamSport;
    using NTB.SportsData.Service.Domain.Classes;
    using NTB.SportsData.Service.Domain.Enums;
    using NTB.SportsData.Service.Facade.Interfaces;
    using NTB.SportsData.Utilities;
    using NTB.SportsData.Utilities.Interfaces;

    using Quartz;
    using Quartz.Impl;

    public partial class TeamSportComponent : Component, IBaseSportsDataInterfaces
    {
        static TeamSportComponent()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }
        public TeamSportComponent()
        {
            _componentConfigurator = new ComponentConfigurator(this);

            InitializeComponent();
        }

        /// <summary>
        /// The Configured job name for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="InstanceName"/></remarks>
        protected string Name;

        /// <summary>
        /// Flag to indicate sucessful configure. Instance will not start polling nor handle messages if not properly Configured
        /// </summary>
        /// <remarks>Holds the Configured status of the instance. <c>True</c> means successfully Configured</remarks>
        protected bool Configured = false;

        /// <summary>
        /// Error-Retry flag
        /// </summary>
        /// <remarks>The flag is being set if a network or EWS error is encountered. Current operations are being aborted for later retry.</remarks>
        protected bool ErrorRetry = false;

        /// <summary>
        /// Send Email-notifications when a new messages is processed
        /// </summary>
        /// <remarks>Set to an email address. Multiple adresses are supported, separate with <c>;</c></remarks>
        protected string EmailNotification;

        /// <summary>
        /// Subject for email notifications
        /// </summary>
        /// <remarks>The subject is built during processing and used when sending the email when processing completes</remarks>
        protected string EmailSubject;

        /// <summary>
        /// Body for email notifications
        /// </summary>
        /// <remarks>The body is built during processing and used when sending the email when processing completes</remarks>
        protected string EmailBody;

        /// <summary>
        /// The schedule type.
        /// </summary>
        protected ScheduleType ScheduleType;

        #region Polling settings

        /// <summary>
        /// schedulerFactory to be used to create scheduled tasks
        /// </summary>
        protected StdSchedulerFactory SchedulerFactory = new StdSchedulerFactory();

        /// <summary>
        /// The scheduler.
        /// </summary>
        protected IScheduler Scheduler;

        /// <summary>
        /// The Configured interval for this instance, used for Continous polling
        /// </summary>
        /// <remarks>
        ///   <para>Indicates the interval time in seconds for <c>Continous</c>polling. 60 means that the job runs every minute.</para>
        ///   <para>Default value: <c>60</c></para>
        /// </remarks>
        protected int Interval = 60;

        /// <summary>
        /// The testing tournament.
        /// </summary>
        protected bool TestingTournament = Convert.ToBoolean(ConfigurationManager.AppSettings["TestingTournament"]);

        /// <summary>
        /// The testing tournament id.
        /// </summary>
        protected int TestingTournamentId = Convert.ToInt32(ConfigurationManager.AppSettings["TestingTournamentId"]);

        #endregion

        #region File folders

        /// <summary>
        /// Output file folder
        /// </summary>
        /// <remarks>
        /// File folder where files shall be plased after being created. 
        /// </remarks>
        protected string FileOutputFolder;

        /// <summary>
        /// Error file folder
        /// </summary>
        /// <remarks>
        ///   <para>File folder where failing files are stored.</para>
        ///   <para>If this is not set, failing files are deleted instead of saved.</para>
        /// </remarks>
        protected string FileErrorFolder;

        /// <summary>
        /// File folder where completed files are stored
        /// </summary>
        /// <remarks>
        ///   <para>File folder where completed files are archived. Subdirectories for years, months and dates are created.</para>
        ///   <para>If this is not set, imported files are deleted instead of archived.</para>
        /// </remarks>
        protected string FileDoneFolder;

        #endregion


        /// <summary>
        /// The season id.
        /// </summary>
        protected int SeasonId = 0;

        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(NffGathererComponent));

        /// <summary>
        /// The _data file creator.
        /// </summary>
        private readonly DataFileCreator _dataFileCreator = new DataFileCreator();

        /// <summary>
        /// Busy status event
        /// </summary>
        private readonly AutoResetEvent _busyEvent = new AutoResetEvent(true);

        /// <summary>
        /// Exit control event
        /// </summary>
        private readonly AutoResetEvent _stopEvent = new AutoResetEvent(false);

        /// <summary>
        /// The result field that we use to store if this is a result file or not.
        /// </summary>
        private bool _result;

        #region Push_PubNub

        /// <summary>
        /// The _pubnub.
        /// </summary>
        private PushNetwork _pubnub;

        /// <summary>
        /// The _channel.
        /// </summary>
        private string _channel = string.Empty;

        /// <summary>
        /// The Populate Database field - tells if the job shall populate the database or not
        /// </summary>
        protected bool PopulateDatabase;

        /// <summary>
        /// The Create XML field - tells if we are to create an XML file or not
        /// </summary>
        protected bool CreateXml;

        private readonly ComponentConfigurator _componentConfigurator;

        #endregion Push_PubNub

        public TeamSportComponent(IContainer container)
        {
            _componentConfigurator = new ComponentConfigurator(this);
            container.Add(this);

            InitializeComponent();
        }

        #region Common Components variables (no need to check)

        /// <summary>
        /// Gets the name of the instance.
        /// </summary>
        /// <value>The name of the instance.</value>
        /// <remarks>The Configured instance job name.</remarks>
        public string InstanceName
        {
            get { return Name; }
        }

        /// <summary>
        /// Gets the operation mode. 
        /// </summary>
        /// <value>The operation mode.</value>
        /// <remarks><c>Gatherer</c> is the only valid mode, its hard coded for this component.</remarks>
        public OperationMode OperationMode
        {
            get { return OperationMode.Gatherer; }
        }

        /// <summary>
        /// Gets the poll style.
        /// </summary>
        /// <value>The poll style.</value>
        /// <remarks>Contionous, Scheduled and FileSystemWatch are valid for <c>Gatherer</c> objects.</remarks>
        public PollStyle PollStyle
        {
            get { return pollStyle; }
        }

        #endregion

        /// <summary>
        /// Gets a value indicating whether the Enabled status of the instance.
        /// </summary>
        /// <value>The Enabled status.</value>
        /// <remarks><c>True</c> if the job instance is Enabled.</remarks>
        public bool Enabled
        {
            get { return enabled; }
        }

        /// <summary>
        /// Gets or sets the component state.
        /// </summary>
        public ComponentState ComponentState { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether database populated.
        /// </summary>
        public bool DatabasePopulated { get; set; }

        /// <summary>
        /// Gets or sets the maintenance mode.
        /// </summary>
        public MaintenanceMode MaintenanceMode { get; set; }

        /// <summary>
        /// Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        /// Gets or sets the federation id.
        /// </summary>
        public int FederationId { get; set; }

        public ComponentConfigurator ComponentConfigurator
        {
            get { return _componentConfigurator; }
        }

        #region Certificate
        /**
         * This region contains functions and other information that is needed to 
         * connect to the test and probably also the stage webservice-server
         */

        /// <summary>
        /// The validate server certificate.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="certificate">
        /// The certificate.
        /// </param>
        /// <param name="chain">
        /// The chain.
        /// </param>
        /// <param name="sslPolicyErrors">
        /// The ssl policy errors.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool ValidateServerCertificate(
            object sender,
            X509Certificate certificate,
            X509Chain chain,
            SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        #endregion

        public void Configure(XmlNode configNode)
        {
            var configurator = _componentConfigurator.Configure(configNode);


            SetUpPushNetwork(configurator.SubscriberKey);

        }

        public void SetUpPushNetwork(string subscriberKey)
        {
            try
            {
                Logger.Debug("Setting up Pubnub");

                // Done: Add subscription string to app.config

                PushNetwork pubNubTeamSport = new PubnubNff(string.Empty, subscriberKey, string.Empty, false);

                Logger.Debug("Done setting up Pubnub");
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception);
            }

        }

        /// <summary>
        /// The num day of week.
        /// </summary>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        private static int NumDayOfWeek()
        {
            // If we use the DateTimeOffset it means that we have to find out if the day has passed or not.
            // So we have to find out which day it is today
            DayOfWeek today = DateTime.Today.DayOfWeek;
            int numDayOfWeek = 0;
            switch (today.ToString())
            {
                case "Monday":
                    numDayOfWeek = 1;
                    break;
                case "Tuesday":
                    numDayOfWeek = 2;
                    break;

                case "Wednesday":
                    numDayOfWeek = 3;
                    break;

                case "Thursday":
                    numDayOfWeek = 4;
                    break;

                case "Friday":
                    numDayOfWeek = 5;
                    break;

                case "Saturday":
                    numDayOfWeek = 6;
                    break;

                case "Sunday":
                    numDayOfWeek = 0;
                    break;
            }

            return numDayOfWeek;
        }

        public void Start()
        {
            throw new NotImplementedException();
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Logger.Info("backgroundWorker1_ProgressChanged - Not implemented");
            throw new NotImplementedException();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            Logger.Debug("Running BackgroundWorker, checking for Configured state");
            if (Configured)
            {
                BackgroundWorker worker = sender as BackgroundWorker;

                while (worker != null && !worker.CancellationPending)
                {
                    Logger.Info("Calling PubNubNFF");

                    // Setting up the call to pubnub
                    _pubnub.Subscribe(_channel, PushCallback);

                    Logger.Info("Done calling PubNubNFF");
                }
            }
        }

        private bool PushCallback(PubNubMessage message)
        {
            // TODO: We shall try and move this code to DataFileCreator or some other class
            Logger.Info("Running callback method!");

            Logger.Info("MessageType: " + message.MessageType);
            Logger.Info("ObjectId: " + message.ObjectId);
            Logger.Info("ObjectType: " + message.ObjectType);
            Logger.Debug("TimeStamp: " + message.TimeStamp);

            // TODO: Work more on the JSON deserializion... Choose the ServiceStack.Text library
            // var jsonObj = JsonSerializer.DeserializeFromString<Dictionary<string, object>>>(message);

            // Consider doing this as a method on it's own, if it is an object, we can call the same method...
            switch (message.MessageType.ToLower())
            {
                case "update":

                    FooTournament(message);
                    break;

                case "insert":
                    int matchId = 0;

                    _dataFileCreator.RenderResult = true;

                    if (message.ObjectType.ToLower() == "matchresult")
                    {
                        foreach (KeyValuePair<string, string> kvp in message.Properties)
                        {
                            Logger.Info("key: " + kvp.Key + " - value: " + kvp.Value);

                            // I don't bother with the result, as I am to get that from calling the NFF WebService
                            if (kvp.Key.ToLower() != "matchid")
                            {
                                continue;
                            }

                            matchId = Convert.ToInt32(kvp.Value);

                            Logger.Info("MatchId: " + matchId);
                        }

                        /*
                         * First we find out from which tournament this result is in. If anyone needs these
                         * results we shall continue, if not we shall exit. 
                         * 
                         * We start by using the MatchID to find the tournament, which we either get from the database
                         * or we get it from the service.
                         */

                        /* 
                        * What we shall do now is to get the result, then get the table standings 
                        * Then we find out if there are anyone out
                        */

                        // Now we create the datafile from MatchId
                        Logger.Info("We are calling the Push method to get and create the datafile");
                        this._dataFileCreator.NffPushDatafileCreator.MatchId = matchId;

                        this._dataFileCreator.OutputFolder = FileOutputFolder;

                        this._dataFileCreator.NffPushDatafileCreator.CreateDataFileFromPush(SportId);
                    }

                    break;
            }

            Logger.Info("Done running callback method!");

            Logger.Info("Setting keep listening to true!");

            return true;
        }

        private void FooTournament(PubNubMessage message)
        {
            var matchDataMapper = new MatchDataMapper();

            switch (message.ObjectType)
            {
                case "Match":
                    var matchId = Convert.ToInt32(message.ObjectId);
                    if (matchId > 0)
                    {
                        matchDataMapper.Get(matchId);
                        // todo: fix this so it works...
                        matchDataMapper.CheckMatches(new List<Match>());
                    }

                    break;

                case "Tournament":
                    // Todo: This shall be moved to the DataFile Creator
                    var tournamentId = Convert.ToInt32(message.ObjectId);
                    if (tournamentId > 0)
                    {
                        // This is where we get more information from the web service and call TournamentDataMapper and so on
                        IFacade facade = new FacadeFactory().GetFacade(SportId);
                        facade.GetTournament(tournamentId); // todo: Code not implemented yet!
                    }

                    break;

                case "MatchResult":

                    foreach (KeyValuePair<string, string> kvp in message.Properties)
                    {
                        Logger.Info("key: " + kvp.Key + " - value: " + kvp.Value);

                        // I don't bother with the result, as I am to get that from calling the NFF WebService
                        if (kvp.Key.ToLower() != "matchid")
                        {
                            continue;
                        }

                        matchId = Convert.ToInt32(kvp.Value);

                        // We are to use the matchId to change the data in the database
                        if (matchId <= 0)
                        {
                            continue;
                        }

                        // Checking the matchId again(?)
                        matchDataMapper.CheckMatches(new List<Match>());

                        Match match = new Match
                            {
                                MatchId = matchId
                            };

                        // Get customers that shall have these data
                        var customerDataMapper = new CustomerDataMapper();
                        List<Customer> customers =
                            customerDataMapper.GetListOfCustomersByMatch(match);

                        if (!customers.Any())
                        {
                            continue;
                        }

                        // I believe I have to do this as a separate thread
                        // Now we shall take the data and create a datafile
                        _dataFileCreator.Customers = customers;

                        Logger.Debug("Number of customers: " + customers.Count());
                        foreach (Customer customer in customers)
                        {
                            Logger.Debug("Customer: " + customer.Name);
                        }

                        // Now we create the datafile from MatchId
                        Logger.Info("We are calling the Push method to get and create the datafile");
                        _dataFileCreator.NffPushDatafileCreator.MatchId = matchId;

                        _dataFileCreator.OutputFolder = FileOutputFolder;

                        _dataFileCreator.NffPushDatafileCreator.CreateDataFileFromPush(SportId);
                    }

                    break;
            }

            return;
        }

        /// <summary>
        ///     Creating the data file
        /// </summary>
        private void CreateDataFile()
        {
            /**
             * This method shall be called from following methods:
             *  Push 
             *  EventTimer
             *  FileSystemWatcher
             * 
             * 
             * We shall then model the data in DataFileCreator class
             * Then send the model to the "view" - XML Document Creator
             */


            /**
             * Modeling the data
             */

            // Creating the DataFileCreator object
            DataFileCreator dataFileCreator = new DataFileCreator
                {
                    OutputFolder = FileOutputFolder,
                    JobName = Name,
                    SportId = SportId
                };

            Logger.Debug("Outputfolder: " + FileOutputFolder);

            if (PopulateDatabase)
            {
                Logger.Debug("We shall insert data in the database");
                dataFileCreator.UpdateDatabase = true;
            }

            // This parameter tells us if we are to add results-tags to the XML
            Logger.Debug("We shall also render result");
            dataFileCreator.RenderResult = true;

            DateTime eventStartDate;
            DateTime eventEndDate;
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
            {
                eventStartDate = DateTime.Parse("2012-03-30");
                eventEndDate = DateTime.Parse("2012-03-31");
            }
            else
            {
                eventStartDate = DateTime.Today.Date;
                eventEndDate = DateTime.Today.Date;

                Logger.Debug("Date Start: " + dataFileCreator.DateStart);
                Logger.Debug("Date End: " + dataFileCreator.DateEnd);
            }

            Logger.Debug("We are about to create XML File");
            dataFileCreator.CreateXml = CreateXml;

            // Getting a list of matches we shall get from the data sources
            List<Match> matches = dataFileCreator.GetMatchesByDate(eventStartDate, SportId);

            // If we don't get any matches, then we shall return / end process
            if (!matches.Any())
            {
                return;
            }

            // Filter the matches, so we have matches from same tournament 

            IEnumerable<Tournament> tournaments = from m in matches
                                                  group m by m.TournamentId
                                                  into g
                                                  select new Tournament()
                                                      {
                                                          TournamentId = g.Key
                                                      };

            // We are creating one file per tournament
            
            foreach (Tournament tournament in tournaments)
            {
                // Get the customers for this tournament
                List<Customer> customers = dataFileCreator.GetCustomerList(tournament);

                // Getting the information needed for the Tournament
                Tournament tournamentMetaData = dataFileCreator.GetTournamentById(tournament.TournamentId, SportId);

                // Get the age category for the tournament
                AgeCategory ageCategory = dataFileCreator.GetAgeCategoryByTournament(tournament, SportId);

                // We shall also get the list of standings
                List<TournamentTableTeam> standings = dataFileCreator.GetTournamentStanding(tournament, SportId);

                // Now that we have the customers, we shall get the matches with results
                List<Match> matchResults = dataFileCreator.GetMatchesByTournament(tournament.TournamentId,
                                                                             DateTime.Today.Date);

                // We need to find if there are standings here
                
                District district = dataFileCreator.GetDistrict(tournamentMetaData.DistrictId);

                Organization organization = dataFileCreator.GetOrganizationBySportId(SportId);

                // note: need to fix false here
                bool renderResult = Convert.ToBoolean(from m in matchResults
                                                      where m.HomeTeamGoals != null
                                                      select m);

                // Todo: We are to get the standing bool and not set true
                var documentType = dataFileCreator.CreateDocumentType(renderResult, true);

                // When we have the customers and todays matches for this tournament, we shall get what?
                var xmlDocumentStructureCreator = new XmlDocumentStructureCreator();

                var xmlDocumentCreator = new XmlDocumentCreator();
                // Creating the customerStructure
                xmlDocumentCreator.XmlCustomerStructure = xmlDocumentStructureCreator.CreateCustomerStructure(customers);

                xmlDocumentCreator.XmlTournamentMetaInfoStructure =
                    xmlDocumentStructureCreator.CreateTournamentMetaInfoStructure(tournamentMetaData);

                xmlDocumentCreator.XmlAgeCategoryStructure = xmlDocumentStructureCreator.CreateAgeCategoryStructure(tournament, ageCategory);

                xmlDocumentCreator.XmlStandingStructure = xmlDocumentStructureCreator.CreateStandingStructure(standings);

                xmlDocumentCreator.XmlDocumentType = xmlDocumentStructureCreator.CreateDocumentTypeStructure(documentType);
                
                xmlDocumentCreator.XmlMatchStructure = xmlDocumentStructureCreator.CreateMatchStructure(matchResults, renderResult);

                xmlDocumentCreator.XmlDistrictElement = xmlDocumentStructureCreator.CreateDistrictElement(district);

                xmlDocumentCreator.XmlSportStructure = xmlDocumentStructureCreator.CreateSportStructure(organization);

                xmlDocumentCreator.XmlOrganizationStructure =
                    xmlDocumentStructureCreator.CreateOrganizationStructure(organization);

                var filename = xmlDocumentCreator.CreateFileName(renderResult, DateTime.Today.Date, district.DistrictId,
                                                  tournament.TournamentId, organization);

                xmlDocumentCreator.XmlFileStructure = xmlDocumentStructureCreator.CreateFileStructure(filename);

                

            }






            /**
             * Creating the output
             */
        }

        private void pollTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            /*
             * We can consider adding getting data here if the time (day + hour + minute) hits. Otherwize we shall do nothing.
             * 
             */
            ThreadContext.Properties["JOBNAME"] = InstanceName;

            Logger.Debug("NFFGathererComponent::pollTimer_Elapsed() hit");
            Logger.Debug("We have the following settings: Interval: " + pollTimer.Interval);

            // _busyEvent.WaitOne();
            pollTimer.Stop();

            switch (pollStyle)
            {
                // For continuous and scheduled do a simple folder item traversal
                case PollStyle.Scheduled:
                case PollStyle.Continous:
                    if (operationMode == OperationMode.Maintenance)
                    {
                        var maintenance = new NffMaintenance(maintenanceMode);
                    }

                    if (operationMode == OperationMode.Gatherer)
                    {
                        // Doing the actual creation of data file
                        CreateDataFile();
                    }

                    pollTimer.Start();

                    break;

                case PollStyle.CalendarPoll:

                    // @done: need to do some work here
                    Logger.Debug("Disabling pollTimer");
                    pollTimer.Enabled = false;

                    break;

                case PollStyle.PushSubscribe:

                    // Subscriber key =             sub-4fb246b0-982a-11e1-ad0b-e19db246ca40
                    // Staging channel =          THE_FIKS_STAGING_CHANNEL

                    // Done: We need to check the service state
                    pollTimer.Start();
                    break;

                /*
                 * MÅ legge inn i databasen om denne turneringen er push eller pull enablet.
                 * Sjekke om vi skal bruke den andre løsningen for push...
                 * Anders går over om dagens nye løsning er 1:1 gammel løsning.
                 */
            }
        }
    }
}
