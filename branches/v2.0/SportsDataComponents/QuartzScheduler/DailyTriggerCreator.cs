﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DailyTriggerCreator.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Components.QuartzScheduler
{
    using Quartz;

    /// <summary>
    /// The daily trigger creator.
    /// </summary>
    internal class DailyTriggerCreator
    {
        /*
        /// <summary>
        /// The trigger builder creator.
        /// </summary>
        /// <param name="componentConfiguration">
        /// The component configuration.
        /// </param>
        /// <param name="scheduleId">
        /// The schedule id.
        /// </param>
        /// <param name="stringCronExpression">
        /// The string cron expression.
        /// </param>
        /// <returns>
        /// The <see cref="ITrigger"/>.
        /// </returns>
        public static ITrigger TriggerBuilderCreator(ComponentConfiguration componentConfiguration, string scheduleId, string stringCronExpression)
        {
            ITrigger trigger = TriggerBuilder.Create().WithIdentity(componentConfiguration.InstanceName + "_" + scheduleId, "groupTeamSport").WithDescription(componentConfiguration.InstanceName).WithCronSchedule(stringCronExpression).Build();

            return trigger;
        }
        */
    }
}