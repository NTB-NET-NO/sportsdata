﻿namespace NTB.SportsData.Service.Components.QuartzScheduler
{
    using System;
    using System.Xml;

    using log4net;

    using NTB.SportsData.Service.Domain.Enums;

    using Quartz;

    class JobBuilderCreator
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(JobBuilderCreator));

        private static IJobDetail DataMapper (bool totalSchedule, DateTime dateTimeOffset,
                                                    ComponentConfiguration componentConfiguration, bool scheduleMessage,
                                                    int duration, bool purge, string scheduleId, IJobDetail jobDetail)
        {
            // Creating the jobDetail 


            if (componentConfiguration.FileOutputFolder != null)
            {
                jobDetail.JobDataMap["OutputFolder"] = componentConfiguration.FileOutputFolder;
            }

            jobDetail.JobDataMap["InsertDatabase"] = componentConfiguration.PopulateDatabase;
            jobDetail.JobDataMap["ScheduleType"] = componentConfiguration.ScheduleType.ToString();
            jobDetail.JobDataMap["Results"] = componentConfiguration.Results;
            jobDetail.JobDataMap["OperationMode"] = componentConfiguration.OperationMode.ToString();
            jobDetail.JobDataMap["ScheduleMessage"] = scheduleMessage;
            jobDetail.JobDataMap["Duration"] = duration;
            jobDetail.JobDataMap["Purge"] = purge;
            jobDetail.JobDataMap["CreateXML"] = componentConfiguration.CreateXml;
            jobDetail.JobDataMap["DateOffset"] = dateTimeOffset;
            jobDetail.JobDataMap["JobName"] = "job_" + componentConfiguration.InstanceName + "_" + scheduleId;
            jobDetail.JobDataMap["TotalSchedule"] = totalSchedule;
            jobDetail.JobDataMap["SportId"] = componentConfiguration.SportId;

            return jobDetail;
        }

        private static string CreaterJobDetailIdentity(ComponentConfiguration componentConfiguration, string scheduleId)
        {
            return "job_" + componentConfiguration.InstanceName + "_" + scheduleId;
        }

        private static void Creator(XmlNodeList nodeList, bool totalSchedule, int offset,
                                              DateTime dateTimeOffset,
                                              ComponentConfiguration componentConfiguration, bool scheduleMessage,
                                              int duration,
                                              bool purge, ScheduleType scheduleType)
        {
            foreach (XmlNode node in nodeList)
            {
                // Creating the offset time 
                if (node.Attributes["Total"] != null)
                {
                    totalSchedule = Convert.ToBoolean(node.Attributes["Total"].Value);
                }
                // If the value is larger or less than 0
                if (offset > 0 || offset < 0)
                {
                    if (offset > 0)
                    {
                        dateTimeOffset = DateTime.Today.AddDays(offset);
                    }
                    else if (offset < 0)
                    {
                        TimeSpan fromDaysOffset = TimeSpan.FromDays(offset);
                        dateTimeOffset = DateTime.Today.Subtract(fromDaysOffset);
                    }
                }
                string scheduleId = node.Attributes["Id"].Value;
                // Creating the offset time 
                if (node.Attributes["Offset"] != null)
                {
                    offset = Convert.ToInt32(node.Attributes["Offset"].Value);
                }
                // If the value is larger or less than 0
                if (offset > 0 || offset < 0)
                {
                    if (offset > 0)
                    {
                        dateTimeOffset
                            =
                            DateTime.Today.AddDays
                                (offset);
                    }
                    else if (
                        offset < 0)
                    {
                        TimeSpan fromDaysOffset = TimeSpan.FromDays(offset);

                        dateTimeOffset = DateTime.Today.Subtract(fromDaysOffset);
                    }
                }

                // Splitting the time into two values so that we can create a cron job
                string[] scheduleTimeArray =
                    node.Attributes["Time"].Value.Split(new[] { ':' });

                // If minutes contains two zeros (00), we change it to one zero (0) 
                // If not, scheduler won't understand
                if (scheduleTimeArray[1] == "00")
                {
                    scheduleTimeArray[1] = "0";
                }

                // Doing the same thing for hours
                if (scheduleTimeArray[0] == "00")
                {
                    scheduleTimeArray[0] = "0";
                }

                // Checking if there is a setting for results
                if (node.Attributes["Results"] != null)
                {
                    componentConfiguration.Results = Convert.ToBoolean(node.Attributes["Results"].Value);
                    scheduleMessage = !componentConfiguration.Results;
                }

                // Checking if there is a setting for Duration
                if (node.Attributes["Duration"] != null)
                {
                    duration = Convert.ToInt32(node.Attributes["Duration"].Value);
                }

                /*
                                             * Creating cron expressions and more. 
                                             * This is the cron syntax:
                                             *  Seconds
                                             *  Minutes
                                             *  Hours
                                             *  Day-of-Month
                                             *  Month
                                             *  Day-of-Week
                                             *  Year (optional field)
                                             */

                // Creating the daily cronexpression
                string stringCronExpression = "0 " +
                                              scheduleTimeArray[1] + " " +
                                              scheduleTimeArray[0] + " " +
                                              "? " +
                                              "* " +
                                              "* ";

                // Setting up the CronTrigger
                Logger.Debug("Setting up the CronTrigger with following pattern: " + stringCronExpression);

                ITrigger dailyCronTrigger = DailyTriggerCreator.TriggerBuilderCreator(componentConfiguration, scheduleId,
                                                                                      stringCronExpression);

                Logger.Debug("dailyCronTrigger: " + dailyCronTrigger.Description);

                var dailyJobDetail = JobBuilderCreator(totalSchedule, dateTimeOffset, componentConfiguration,
                                                       scheduleMessage,
                                                       duration, purge, scheduleId);

                if (dailyJobDetail == null)
                {
                    continue;
                }
                Logger.Debug("Setting up and starting dailyJobDetail job " + dailyJobDetail.Description +
                             " using trigger : " + dailyCronTrigger.Description);

                // componentConfiguration.Scheduler.ScheduleJob(dailyJobDetail, dailyCronTrigger);
            }
        }
    }
}
