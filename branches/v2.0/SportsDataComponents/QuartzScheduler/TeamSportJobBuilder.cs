// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TeamSportJobBuilder.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Components.QuartzScheduler
{
    using System;

    using log4net;

    using Quartz;

    /// <summary>
    /// The team sport job builder.
    /// </summary>
    internal class TeamSportJobBuilder : IJob
    {
        /*
        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(TeamSportJobBuilder));

        /// <summary>
        /// Initializes static members of the <see cref="TeamSportJobBuilder"/> class.
        /// </summary>
        static TeamSportJobBuilder()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        /// <summary>
        /// The execute.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public void Execute(IJobExecutionContext context)
        {
            Logger.Info("Executing JobExecutionContext" + context.JobDetail.Description);

            var dateOffset = DateTime.Now;

            // Mapping information sent from Component
            var dataMap = context.JobDetail.JobDataMap;

            // Populate variables to be used to 
            try
            {
                // Cleaning up the code a bit. having all variable implementations here
                var outputFolder = dataMap.GetString("OutputFolder");

                var jobName = dataMap.GetString("JobName");

                var operationMode = dataMap.GetString("OperationMode");

                var insertDatabase = dataMap.GetBoolean("InsertDatabase");

                var scheduleType = dataMap.GetString("ScheduleType");

                var results = dataMap.GetBoolean("Results");

                var scheduleMessage = dataMap.GetBoolean("ScheduleMessage");

                var duration = dataMap.GetInt("Duration");

                var createXml = dataMap.GetBoolean("CreateXML");

                if (dataMap.Keys.Contains("DateOffset"))
                {
                    dateOffset = dataMap.GetDateTime("DateOffset");
                }

                if (dataMap.Keys.Contains("TotalSchedule"))
                {
                    dataMap.GetBoolean("TotalSchedule");
                }

                if (dataMap.Keys.Contains("SportId"))
                {
                    dataMap.GetInt("SportId");
                }

                // And all logging here
                Logger.Debug("jobName: " + jobName);
                Logger.Debug("OutputFolder: " + outputFolder);
                Logger.Debug("OperationMode: " + operationMode);
                Logger.Debug(insertDatabase ? "InsertDatabase: true" : "InsertDatabase: false");
                Logger.Debug("ScheduleType: " + scheduleType);
                Logger.Debug(results ? "Results: true" : "Results: false");
                Logger.Debug(scheduleMessage ? "ScheduleMessage: true" : "ScheduleMessage: false");
                Logger.Debug("Duration: " + duration);
                Logger.Debug(createXml ? "CreateXML: true" : "CreateXML: false");
                Logger.Debug("DateOffset: " + dateOffset.ToShortDateString());
            }
            catch (Exception exception)
            {
                Logger.Debug(exception);
                Logger.Debug(exception);
            }
        }
        */

        public void Execute(IJobExecutionContext context)
        {
            throw new NotImplementedException();
        }
    }
}