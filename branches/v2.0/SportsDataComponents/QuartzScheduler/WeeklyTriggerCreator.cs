﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WeeklyTriggerCreator.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Components.QuartzScheduler
{
    using System;

    using Quartz;

    /// <summary>
    /// The weekly trigger creator.
    /// </summary>
    internal class WeeklyTriggerCreator
    {
        /// <summary>
        /// The trigger builder creator.
        /// </summary>
        /// <param name="componentConfiguration">
        /// The component configuration.
        /// </param>
        /// <param name="scheduleId">
        /// The schedule id.
        /// </param>
        /// <param name="dateTimeOffset">
        /// The date time offset.
        /// </param>
        /// <returns>
        /// The <see cref="ITrigger"/>.
        /// </returns>
        /*
        public static ITrigger TriggerBuilderCreator(ComponentConfiguration componentConfiguration, string scheduleId, DateTimeOffset dateTimeOffset)
        {
            // Creating the weekly Trigger using the stuff that we've calculated
            ITrigger trigger =
                TriggerBuilder.Create().WithDescription(componentConfiguration.InstanceName)
                .WithIdentity(componentConfiguration.InstanceName + "_" + scheduleId, "GroupTeamSport").StartAt(dateTimeOffset)
                .WithSchedule(calendarIntervalSchedule.WithIntervalInWeeks(Convert.ToInt32(scheduleWeek))).Build();

            return trigger;
        }
        */
    }
}