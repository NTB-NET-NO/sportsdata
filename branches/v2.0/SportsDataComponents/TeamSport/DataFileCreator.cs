﻿using System;
using System.Collections.Generic;
using System.Linq;
using NTB.SportsData.Service.Components.Factories;
using NTB.SportsData.Service.Components.Interfaces;
using NTB.SportsData.Service.Components.Repositories;
using NTB.SportsData.Service.Domain.Classes;
using NTB.SportsData.Service.Domain.Enums;
using log4net;

namespace NTB.SportsData.Service.Components.TeamSport
{
    class DataFileCreator : IDataFileCreator
    {
        /// <summary>
        /// Static logger
        /// </summary>
        public static readonly ILog Logger = LogManager.GetLogger(typeof(DataFileCreator));

        public DataFileCreator()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();

            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        public string OutputFolder { get; set; }

        public string JobName { get; set; }

        public int SportId { get; set; }

        public bool UpdateDatabase { get; set; }

        public bool RenderResult { get; set; }

        public bool Configure(int sportId)
        {
            try
            {
                Logger.Debug("Configuring DatafileCreator");

                TableDataMapper dataTableMapper = new TableDataMapper();

                // First we check when this tournament was last checked - if we don't have to configure, then we return
                if (dataTableMapper.GetTableCheck() != null)
                {
                    // We set a value and return
                    dataTableMapper.SetTableCheck();
                    return true;
                }

                var facade = new FacadeFactory().GetFacade(sportId);

                var configurator = new DataFileConfigurator();
                configurator.ConfigureAgeCategory(sportId);
                configurator.ConfigureSeason(sportId);
                configurator.ConfigureDistricts(sportId);

                List<District> districts = facade.GetDistricts();

                DistrictDataMapper districtDataMapper = new DistrictDataMapper();
                // Check the districts-table. If we get a false-return, we are stopping configuration
                if (districtDataMapper.CheckDistrictsTable(districts, sportId) != true)
                {
                    return false;
                }

                configurator.ConfigureMunicipalities(districts, sportId);


                foreach (District district in districts)
                {
                    configurator.ConfigureTournaments(district, sportId);
                    configurator.ConfigureMatches(district, sportId);
                }

                return true;
            }
            catch (Exception e)
            {
                Logger.Error("An exception has occured: " + e);
                return false;
            }
        }

        public List<Match> CreateTotalSchedule(int sportId, List<Season> seasons)
        {
            var facade = new FacadeFactory().GetFacade(sportId);

            try
            {
                var districts = facade.GetDistricts();

                List<Match> matches = new List<Match>();
                List<AgeCategory> filteredAgeCategories = new List<AgeCategory>();
                filteredAgeCategories.AddRange(facade.GetAgeCategories());

                foreach (District district in districts)
                {
                    foreach (Season season in seasons)
                    {
                        foreach (AgeCategory ageCategoryTournament in filteredAgeCategories)
                        {
                            List<Tournament> tournaments =
                                facade.GetTournamentsByDistrict(district.DistrictId, season.SeasonId).ToList();

                            List<Tournament> filteredTournaments =
                                tournaments.FindAll(
                                    t => t.AgeCategoryId == ageCategoryTournament.AgeCategoryId);

                            foreach (Tournament tournament in filteredTournaments)
                            {
                                matches.AddRange(facade.GetMatchesByTournament(tournament.TournamentId, false, false, false, false, DateTime.Today.Date)
                                          .ToList());
                            }
                        }
                    }
                }

                return matches;
            }
            catch (Exception e)
            {
                Logger.Error("An exception has occured!", e);

                Logger.Info("Cannot get Districts. Connection to server might be down");

                return null;
            }
        }

        public MessageType CreateDocumentType(bool renderResult, bool standing)
        {
            if (standing)
            {
                return MessageType.SportResult;
            }

            return !renderResult ? MessageType.SportSchedule : MessageType.SportMessage;
        }

        public void CreateDataFile(List<Customer> customers, int eventId, DateTime startDateTime, DateTime endDateTime, int sportId)
        {
            Logger.Debug("We are in CreateCustomerDataFile!");

            // Creating the SportsDatabase object
            CustomerDataMapper customerDataMapper = new CustomerDataMapper();
            Logger.Debug("We have gotten database object");

            try
            {
                var facade = new FacadeFactory().GetFacade(sportId);
                // Getting the Tournament Object based on the TournamentId
                Tournament tournament = facade.GetTournament(eventId);
                List<AgeCategory> ageCategoryTournament =
                    facade.GetAgeCategories().ToList();

                AgeCategory filteredAgeCategory = (from act in ageCategoryTournament
                                                   where act.AgeCategoryId == tournament.AgeCategoryId
                                                   select act).Single();

                Logger.Debug("Tournament:" + tournament.TournamentName + ".");
                Logger.Debug("Done getting Tournament Object");

                // Now that we have some information about the tournament, we can do more
                List<Match> matches = facade.GetMatchesByTournament(eventId, true, true, true, true, DateTime.Today.Date).ToList();

                Logger.Info("No matches found in tournament " + eventId + " on date: " + startDateTime);
                List<Match> filteredMatches =
                    (from m in matches where m.MatchStartDate == startDateTime select m).ToList();

                if (filteredMatches.Count == 0)
                {
                    return;
                }

                // Creating the Standings object that we will be using when creating XML
                List<TournamentTableTeam> standings = null;

                // Checking if this tournament is one that 
                if (tournament.PublishTournamentTable)
                {
                    Logger.Debug("Getting the Standings");

                    try
                    {
                        standings = new List<TournamentTableTeam>(facade.GetTournamentStanding(eventId));
                    }
                    catch (Exception exception)
                    {
                        Logger.Debug(exception.Message);
                        if (exception.Message == "No tournament standings found.")
                        {
                            Logger.Info("No standings found for tournament " + tournament.TournamentName);
                        }
                    }
                }
                else
                {
                    Logger.Info("The standings from tournament " + tournament.TournamentName + " cannot publish results!");
                }

                Logger.Debug("Creating XML-structure!");

                // Creating the XMLDocument
                // XmlDocument doc = NffXmlDocument.CreateXmlStructure(filteredMatches, tournament, filteredAgeCategory, standings, false, customers);

                // Creating the filename
                // List<Filename> fileNames = NffXmlDocument.CreateFileName(tournament.DistrictId, tournament.TournamentId, selectedStartDate, SportId);

                //if (doc != null)
                //{
                //    // Now creating the XML-file
                //    Logger.Debug("Creating XML-File");
                //    NffXmlDocument.CreateXmlFile(
                //        doc,
                //        tournament.TournamentName,
                //        tournament.TournamentId,
                //        fileNames,
                //        tournament.DistrictId);
                //}
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
        }

        public void CreateDataFileMaintenance(int eventId, int sportId)
        {
            throw new NotImplementedException();
        }

        public AgeCategory GetAgeCategoryByTournament(Tournament tournament, int sportId)
        {
            var facade = new FacadeFactory().GetFacade(sportId);
            List<AgeCategory> tournamentAgeCategory = facade.GetAgeCategories();
            return (from act in tournamentAgeCategory
                    where act.AgeCategoryId == tournament.AgeCategoryId
                    select act).Single();
        }

        public List<Customer> GetCustomerList(int customerId)
        {
            throw new NotImplementedException();
        }

        public List<Match> GetMatchesByDate(DateTime matchDateTime, int sportId)
        {
            throw new NotImplementedException();
        }

        public List<Match> GetMatchesByTournament(int tournamentId, DateTime tournamentDate)
        {
            return new List<Match>();
        }

        public District GetDistrict(int districtId)
        {
            throw new NotImplementedException();
        }

        public Tournament GetTournamentById(int tournamentId, int sportId)
        {
            throw new NotImplementedException();
        }

        public List<TournamentTableTeam> GetTournamentStanding(Tournament tournament, int sportId)
        {
            throw new NotImplementedException();
        }
    }
}
