﻿using System;
using System.Xml;
using NTB.SportsData.Service.Components.Interfaces;
using NTB.SportsData.Service.Domain.Classes;
using NTB.SportsData.Service.Domain.Enums;
using log4net;

namespace NTB.SportsData.Service.Components.TeamSport
{
    class XmlDocumentCreator : IXmlDocumentCreator
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(XmlDocumentCreator));

        public XmlDocumentCreator()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();

            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }
        /// <summary>
        /// Gets or sets a value indicating whether standing.
        /// </summary>
        public bool Standing { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether match facts.
        /// </summary>
        public bool MatchFacts { get; set; }

        /// <summary>
        /// Gets or sets a value for Render Result.
        /// </summary>
        public bool RenderResult { get; set; }

        public XmlDocument XmlCustomerStructure { get; set; }

        public XmlDocument XmlTournamentStructure { get; set; }

        public XmlDocument XmlTournamentMetaInfoStructure { get; set; }

        public XmlDocument XmlDistrictElement { get; set; }

        public XmlDocument XmlMatchStructure { get; set; }

        public XmlDocument XmlStandingStructure { get; set; }
        
        public XmlDocument XmlSportStructure { get; set; }

        public XmlDocument XmlAgeCategoryStructure { get; set; }

        public XmlDocument XmlOrganizationStructure { get; set; }

        public XmlDocument XmlFileStructure { get; set; }

        public XmlDocument XmlDocumentType { get; set; }

        public MessageType CreateDocumentType(bool renderResult)
        {
            throw new NotImplementedException();
        }

        public Filename CreateFileName(bool renderResult, DateTime? dateTime, int districtId, int tournamentId, Organization organization)
        {
            Filename fileName = new Filename {PostFix = "Res", MessageType = CreateDocumentType(renderResult)};

            // We need to check if the file has a table or not

            if (Standing)
            {
                fileName.PostFix += "Tab"; // Making the postfix ResTab
            }

            if (!renderResult)
            {
                fileName.PostFix = "Sched";
            }

            // We have not found a document produced for this tournament
            // We generate the filename, and then just the stem
            DateTime todayFile = DateTime.Today;
            if (dateTime != null)
            {
                todayFile = Convert.ToDateTime(dateTime);
            }

            // We are not adding XML to the filename, this will be added when we are creating the rest
            fileName.FullName = "NTB_SportsData_" + todayFile.Year + todayFile.Month.ToString().PadLeft(2, '0')
                              + todayFile.Day.ToString().PadLeft(2, '0') + "_" + organization.OrganizationNameShort + "_D" + districtId + "_T" + tournamentId
                              + "_" + fileName.PostFix;

            fileName.Version = 1;

            Logger.Info("Filename: " + fileName);
            return fileName;
        }

        public XmlDocument CreateCompleteDocumentStructure()
        {
            XmlDocument doc = new XmlDocument();
            XmlElement xmlRoot = doc.CreateElement("SportsData");
            XmlTournamentMetaInfoStructure.AppendChild(XmlDistrictElement);
            XmlTournamentStructure.AppendChild(XmlTournamentMetaInfoStructure);

            // Adding the Tournament structure to the XML-file
            
            XmlElement xmlHeader = doc.CreateElement("Header");
            xmlHeader.AppendChild(XmlOrganizationStructure);
            xmlHeader.AppendChild(XmlAgeCategoryStructure);
            xmlHeader.AppendChild(XmlCustomerStructure);
            xmlHeader.AppendChild(XmlSportStructure);
            xmlHeader.AppendChild(XmlDocumentType);
            
            xmlRoot.AppendChild(XmlTournamentStructure);

            doc.AppendChild(xmlRoot);
            return doc;
        }

        public void WriteXmlDocument(XmlDocument xmlDocument, Filename filename)
        {
            throw new NotImplementedException();
        }
    }
}
