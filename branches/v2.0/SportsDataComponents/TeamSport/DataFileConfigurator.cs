﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataFileConfigurator.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The data file configurator.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Components.TeamSport
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;

    using log4net;

    using NTB.SportsData.Service.Components.Factories;
    using NTB.SportsData.Service.Components.Interfaces;
    using NTB.SportsData.Service.Components.Repositories;
    using NTB.SportsData.Service.Domain.Classes;

    /// <summary>
    /// The data file configurator.
    /// </summary>
    internal class DataFileConfigurator : IDataFileConfigurator
    {
        /// <summary>
        /// The logger.
        /// </summary>
        public static readonly ILog Logger = LogManager.GetLogger(typeof(DataFileConfigurator));

        /// <summary>
        /// Initializes a new instance of the <see cref="DataFileConfigurator"/> class.
        /// </summary>
        public DataFileConfigurator()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();

            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        /// <summary>
        /// The configure age category.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        public void ConfigureAgeCategory(int sportId)
        {
            Logger.Debug("Populating/checking AgeCategoryTable");
            var facade = new FacadeFactory().GetFacade(sportId);
            var ageCategories = facade.GetAgeCategories();

            var ageCategoryDataMapper = new AgeCategoryDataMapper();
            var notFoundAgeCategories = ageCategoryDataMapper.CheckAgeCategories(ageCategories, sportId);

            // If we have a list of age categories, we shall add this to the database
            if (notFoundAgeCategories.Any())
            {
                ageCategoryDataMapper.InsertAll(notFoundAgeCategories);
            }
        }

        /// <summary>
        /// The configure season.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        public void ConfigureSeason(int sportId)
        {
            var facade = new FacadeFactory().GetFacade(sportId);
            var seasons = facade.GetSeasons().Where(x => x.SeasonActive.Equals(true)).ToList();

            var seasonDataMapper = new SeasonDataMapper();
            if (!seasonDataMapper.CheckSeasons(seasons).Any())
            {
                seasonDataMapper.InsertAll(seasons);
            }
        }

        /// <summary>
        /// The configure municipalities.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void ConfigureMunicipalities(int sportId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The configure districts.
        /// </summary>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        public void ConfigureDistricts(int sportId)
        {
            var districtDataMapper = new DistrictDataMapper();

            // Check the districts-table. If we get a false-return, we are stopping configuration
            var facade = new FacadeFactory().GetFacade(sportId);

            var items = facade.GetDistricts();
            if (districtDataMapper.CheckDistrictsTable(items, sportId) != true)
            {
                var districts = districtDataMapper.CheckDistrictsTableByDistrict(items, sportId);

                if (districts.Any())
                {
                    districtDataMapper.InsertAll(districts);
                }
            }
        }

        /// <summary>
        /// The configure tournaments.
        /// </summary>
        /// <param name="district">
        /// The district.
        /// </param>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        public void ConfigureTournaments(District district, int sportId)
        {
            try
            {
                var facade = new FacadeFactory().GetFacade(sportId);
                var dataMapper = new TournamentDataMapper();
                var seasonDataMapper = new SeasonDataMapper();
                var seasons = seasonDataMapper.GetAll();

                Logger.Info("Checking tournaments for district " + district.DistrictName);

                if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                {
                    foreach (var season in seasons)
                    {
                        facade.GetTournamentsByDistrict(district.DistrictId, season.SeasonId);
                    }
                }
                else
                {
                    foreach (var season in seasons)
                    {
                        try
                        {
                            Logger.Debug("Checking for tournaments with following parameters: district.DistrictId" + district.DistrictId + ", seasonId: " + season.SeasonId);

                            var tournaments = facade.GetTournamentsByDistrict(district.DistrictId, season.SeasonId);

                            dataMapper.InsertAll(tournaments);
                        }
                        catch (Exception e)
                        {
                            Logger.Info(e.Message);
                            Logger.Debug(e.StackTrace);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Info(e.Message);
                Logger.Info(e.StackTrace);
            }
        }

        /// <summary>
        /// The configure matches.
        /// </summary>
        /// <param name="district">
        /// The district.
        /// </param>
        /// <param name="sportId">
        /// The sport id.
        /// </param>
        public void ConfigureMatches(District district, int sportId)
        {
            try
            {
                var facade = new FacadeFactory().GetFacade(sportId);
                var dataMapper = new MatchDataMapper();
                if (dataMapper.GetNumberOfMatches() != 0)
                {
                    return;
                }

                // This is populated so that we can have the push subscription working
                var matches = new List<Match>(facade.GetMatchesByDistrict(district.DistrictId, DateTime.Today, DateTime.Today, false, false, false, false, null));

                // We need to check if the match table contains any of the matches
                dataMapper.CheckMatches(matches);
                dataMapper.InsertAll(matches);
            }
            catch (Exception e)
            {
                Logger.Debug(e);
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="districts">
        /// </param>
        /// <param name="sportId">
        /// </param>
        public void ConfigureMunicipalities(List<District> districts, int sportId)
        {
            var facade = new FacadeFactory().GetFacade(sportId);

            var municipalities = new List<Municipality>();

            // First check if we have Municipalities in the database
            var municipalityDataMapper = new MunicipalityDataMapper();
            var result = municipalityDataMapper.CheckMunicipalityTable(sportId);

            if (result)
            {
                return;
            }

            foreach (var district in districts)
            {
                municipalities.AddRange(facade.GetMunicipalitiesByDistrict(district.DistrictId));
            }

            var notFoundMunicipalities = municipalityDataMapper.CheckMunicipalityTableAgainstMunicipalities(municipalities);

            // If we get a list, we shall insert these values
            if (!notFoundMunicipalities.Any())
            {
                municipalityDataMapper.InsertAll(notFoundMunicipalities);
            }
        }
    }
}