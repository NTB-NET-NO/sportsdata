﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainServiceComponent.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Timers;
using System.Xml;
using NTB.SportsData.Service.Components.Interfaces;
using NTB.SportsData.Service.Components.Nff;
using NTB.SportsData.Service.Components.Nif;
using NTB.SportsData.Service.Domain.Enums;
using log4net;
using NTB.SportsData.Utilities;

namespace NTB.SportsData.Service.Components
{
    /// <summary>
    /// The main service component.
    /// </summary>
    public partial class MainServiceComponent : Component
    {
        #region Instance variables and control data

        /// <summary>
        /// Name of the file that contains the job configuration set
        /// </summary>
        /// <remarks>The name of the job config XML file is stored here.</remarks>
        protected string ConfigSet = string.Empty;

        /// <summary>
        /// Enable watching the config set for changes and reconfigure at runtime
        /// </summary>
        /// <remarks>config setting that defines if we are wathcing the config set for changes/auto reconfigure or not.</remarks>
        protected bool WatchConfigSet = false;

        /// <summary>
        /// List of currently Configured jobs
        /// </summary>
        /// <remarks>Internal Dictionary to keep track of running worker jobs.</remarks>
        protected Dictionary<string, IBaseSportsDataInterfaces> JobInstances =
            new Dictionary<string, IBaseSportsDataInterfaces>();

        /// <summary>
        /// Notification handler service host
        /// </summary>
        /// <remarks>
        /// This si the actuall service host that controls the notification object <see>
        ///                                                                            <cref>ewsNotify</cref>
        ///                                                                        </see>
        /// </remarks>
        protected ServiceHost ServiceHost = null;

        #endregion

        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger;

        /// <summary>
        /// Initializes static members of the <see cref="MainServiceComponent"/> class.
        /// </summary>
        static MainServiceComponent()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }

            Logger = LogManager.GetLogger(typeof(MainServiceComponent));

            if (Thread.CurrentThread.Name == null)
            {
                Thread.CurrentThread.Name = "MainServiceComponent";
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainServiceComponent"/> class. 
        /// Initializes a new instance of the class
        /// </summary>
        public MainServiceComponent()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainServiceComponent"/> class.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        public MainServiceComponent(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        #region Control functions

        /// <summary>
        /// The configure.
        /// </summary>
        public void Configure()
        {
            try
            {
                // NDC.Push("CONFIG");
                ThreadContext.Stacks["NDC"].Push("CONFIG");

                // Checking if a directory exists or not
                DirectoryInfo di = new DirectoryInfo(ConfigurationManager.AppSettings["OutPath"]);

                if (!di.Exists)
                {
                    di.Create();
                }

                // Read params 
                ConfigSet = ConfigurationManager.AppSettings["ConfigurationSet"];
                WatchConfigSet = Convert.ToBoolean(ConfigurationManager.AppSettings["WatchConfiguration"]);

                // Logging
                Logger.InfoFormat("{0} : {1}", "ConfigurationSet", ConfigSet);
                Logger.InfoFormat("{0} : {1}", "WatchConfiguration", WatchConfigSet);

                // Load configuration set
                XmlDocument config = new XmlDocument();
                config.Load(ConfigSet);

                // Setting up the watcher
                configFileWatcher.Path = Path.GetDirectoryName(ConfigSet);
                configFileWatcher.Filter = Path.GetFileName(ConfigSet);

                // Clearing all jobInstances before we populate them again. 
                JobInstances.Clear();

                // Creating NFF Components
                XmlNodeList nodes =
                    config.SelectNodes("/ComponentConfiguration/NFFComponents/NFFComponent[@Enabled='True']");
                if (nodes != null)
                {
                    Logger.InfoFormat("NFFComponent job instances found: {0}", nodes.Count);

                    bool checkDatabase = false;
                    foreach (XmlNode nd in nodes)
                    {
                        IBaseSportsDataInterfaces nffComponent = new NffGathererComponent();
                        if (checkDatabase)
                        {
                            nffComponent.DatabasePopulated = true;
                        }
                        else
                        {
                            checkDatabase = nffComponent.DatabasePopulated;
                        }

                        nffComponent.Configure(nd);

                        Logger.Debug("Adding " + nffComponent.InstanceName);
                        JobInstances.Add(nffComponent.InstanceName, nffComponent);
                    }
                }

                // ... then create NIF components
                nodes = config.SelectNodes("/ComponentConfiguration/NIFComponents/NIFComponent[@Enabled='True']");
                if (nodes != null)
                {
                    Logger.InfoFormat("NIFComponent job instances found: {0}", nodes.Count);

                    bool checkDatabase = false;
                    foreach (XmlNode nd in nodes)
                    {
                        IBaseSportsDataInterfaces nifComponent = new NifGathererComponent();
                        if (checkDatabase)
                        {
                            nifComponent.DatabasePopulated = true;
                        }
                        else
                        {
                            checkDatabase = nifComponent.DatabasePopulated;
                        }

                        nifComponent.Configure(nd);

                        Logger.Debug("Adding " + nifComponent.InstanceName);
                        JobInstances.Add(nifComponent.InstanceName, nifComponent);
                    }
                }

                // ... then create Purge components
                nodes = config.SelectNodes("/ComponentConfiguration/PurgeComponent[@Enabled='True']");
                if (nodes != null)
                {
                    Logger.InfoFormat("PurgeComponent job instances found: {0}", nodes.Count);

                    foreach (XmlNode nd in nodes)
                    {
                        IBaseSportsDataInterfaces purgeComponent = new PurgeComponent();
                        purgeComponent.Configure(nd);

                        Logger.Debug("Adding " + purgeComponent.InstanceName);
                        JobInstances.Add(purgeComponent.InstanceName, purgeComponent);
                    }
                }

                // ... then create distribution components
                nodes = config.SelectNodes("/ComponentConfiguration/DistributionComponent[@Enabled='True']");
                if (nodes == null)
                {
                    return;
                }

                Logger.InfoFormat("DistributionComponent job instances found: {0}", nodes.Count);

                foreach (XmlNode nd in nodes)
                {
                    IBaseSportsDataInterfaces purgeComponent = new PurgeComponent();
                    purgeComponent.Configure(nd);

                    Logger.Debug("Adding " + purgeComponent.InstanceName);
                    JobInstances.Add(purgeComponent.InstanceName, purgeComponent);
                }

                // ... then create Maintenance components
                nodes = config.SelectNodes("/ComponentConfiguration/MaintenanceComponents/MaintenanceComponent[@Enabled='True']");
                if (nodes != null)
                {
                    Logger.InfoFormat("MaintenanceComponent job instances found: {0}", nodes.Count);

                    foreach (XmlNode nd in nodes)
                    {
                        IBaseSportsDataInterfaces maintenanceComponent = new MaintenanceComponent();
                        maintenanceComponent.Configure(nd);

                        Logger.Debug("Adding " + maintenanceComponent.InstanceName);
                        JobInstances.Add(maintenanceComponent.InstanceName, maintenanceComponent);
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                Mail.SendException(exception);
            }
            finally
            {
                // ThreadContext.Stacks["NDC"].Pop();
                NDC.Pop();
            }
        }

        /// <summary>
        /// Starts the main service instance.
        /// </summary>
        /// <remarks>
        /// The function will set up polling and events to Start all processing jobs.
        /// </remarks>
        public void Start()
        {
            NDC.Push("START");

            try
            {
                // Start instances
                Logger.Debug("Number of jobs to start: " + JobInstances.Count.ToString());

                // Looping jobs
                foreach (
                    KeyValuePair<string, IBaseSportsDataInterfaces> kvp in JobInstances.Where(kvp => kvp.Value.Enabled))
                {
                    Logger.Info("Starting " + kvp.Value.InstanceName);
                    kvp.Value.Start();
                }

                // Starting maintenance
                Logger.Info("Starting maintenance timer");
                maintenanceTimer.Start();

                // Watch the config file
                configFileWatcher.EnableRaisingEvents = WatchConfigSet;
            }
            catch (Exception ex)
            {
                Logger.Fatal("An error has occured. Could not Start service", ex);
                Logger.Fatal(ex.StackTrace);
                Mail.SendException(ex);
            }
            finally
            {
                NDC.Pop();
            }
        }

        /// <summary>
        /// The pause.
        /// </summary>
        public void Pause()
        {
            maintenanceTimer.Stop();
            configFileWatcher.EnableRaisingEvents = false;

            // Stop instances
            foreach (KeyValuePair<string, IBaseSportsDataInterfaces> kvp in JobInstances.Where(kvp => kvp.Value.Enabled))
            {
                Logger.Info("Stopping " + kvp.Value.InstanceName);
                kvp.Value.Stop();
            }
        }

        /// <summary>
        /// The stop.
        /// </summary>
        public void Stop()
        {
            Pause();

            // Kill notification service handler
            if (ServiceHost == null)
            {
                return;
            }

            ServiceHost.Close();
            ServiceHost = null;
        }

        #endregion

        /// <summary>
        /// The maintenance timer_ elapsed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void maintenanceTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            MDC.Set("JOBNAME", "MainWorkerJob");

            Logger.Debug("In maintenanceTimer_Elapsed");

            /*
             * Not sure what this shall do yet, 
             * but I would believe that getting some data would be an idea...
             */
            try
            {
                maintenanceTimer.Stop();

                // We are checking if the jobs are doing what they are supposed to do
                foreach (KeyValuePair<string, IBaseSportsDataInterfaces> kvp in
                    JobInstances.Where(kvp => kvp.Value.Enabled)
                                .Where(kvp => kvp.Value.ComponentState == ComponentState.Halted))
                {
                    Logger.Info("The component is in Halted state, even though it is Enabled");
                    Logger.Debug("We are starting the component!");

                    kvp.Value.Start();
                }

                maintenanceTimer.Start();

                // Watch the config file
                if (!configFileWatcher.EnableRaisingEvents)
                {
                    configFileWatcher.EnableRaisingEvents = WatchConfigSet;
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat(
                    "MainServiceComponent::maintenanceTimer_Elapsed() failed to renew PushSubscription: " + ex.Message, 
                    ex);
                Mail.SendException(ex);
            }
        }

        /// <summary>
        /// Reconfigures everything from an updated config set
        /// </summary>
        /// <param name="sender">
        /// The source of the event.
        /// </param>
        /// <param name="e">
        /// File change params
        /// </param>
        /// <remarks>
        /// <para>
        /// When the configuration is updated on disk, this function triggers a complete job reload.
        /// </para>
        /// <para>
        /// Note that app.config is also reloaded, but refreshed config settings only apply to <c>AppSettings&gt;</c> here, not the other sections of the config file.
        /// </para>
        /// </remarks>
        private void configFileWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            ThreadContext.Properties["JOBNAME"] = "MainServiceWorker";

            // Outer-try-final to prevent dupe events
            try
            {
                // Stopping file watcher
                configFileWatcher.EnableRaisingEvents = false;
                Logger.Info("MainServiceComponent::configFileWatcher_Changed() hit. Will reconfigure.");

                ThreadContext.Stacks["NDC"].Push("RECONFIGURE");

                // Pause everything
                Pause();

                // Give it a little break
                Thread.Sleep(5000);

                // Reconfigure
                ConfigurationManager.RefreshSection("applicationSettings");
                ConfigurationManager.RefreshSection("appSection");
                Configure();

                Start();
            }
            catch (Exception ex)
            {
                Logger.Fatal("NTBSportsData reconfiguration failed - TERMINATING!", ex);
                Mail.SendException(ex);
                throw;
            }
            finally
            {
                ThreadContext.Stacks["NDC"].Pop();
                configFileWatcher.EnableRaisingEvents = WatchConfigSet;
            }
        }
    }
}