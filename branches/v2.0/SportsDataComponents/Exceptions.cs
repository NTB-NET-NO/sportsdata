﻿using System;

namespace NTB.SportsData.Service.Components
{
    
    class NFFWSComponentException : Exception
    {
             /// <summary>
        /// Initializes a new instance of the <see cref="NFFWSComponentException"/> class.
        /// </summary>
        /// <remarks>Specifies an error message only.</remarks>
        /// <param name="message">The error message</param>
        public NFFWSComponentException (string message)
            : base(message)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="NFFWSComponentException"/> class.
        /// </summary>
        /// <remarks>
        /// Allows for specification of both an error message and an inner exception
        /// </remarks>
        /// <param name="message">The error message.</param>
        /// <param name="exception">Inner exception to wrap.</param>
        public NFFWSComponentException(string message, Exception exception)
            : base(message, exception)
        { }
    
    }

    class NIFWSComponentException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NFFWSComponentException"/> class.
        /// </summary>
        /// <remarks>Specifies an error message only.</remarks>
        /// <param name="message">The error message</param>
        public NIFWSComponentException(string message)
            : base(message)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SportsDataException"/> class.
        /// </summary>
        /// <remarks>
        /// Allows for specification of both an error message and an inner exception
        /// </remarks>
        /// <param name="message">The error message.</param>
        /// <param name="exception">Inner exception to wrap.</param>
        public NIFWSComponentException(string message, Exception exception)
            : base(message, exception)
        { }

    }

    class SportsDataException : Exception
    {
         /// <summary>
        /// Initializes a new instance of the <see cref="SportsDataException"/> class.
        /// </summary>
        /// <remarks>Specifies an error message only.</remarks>
        /// <param name="message">The error message</param>
        public SportsDataException (string message)
            : base(message)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SportsDataException"/> class.
        /// </summary>
        /// <remarks>
        /// Allows for specification of both an error message and an inner exception
        /// </remarks>
        /// <param name="message">The error message.</param>
        /// <param name="exception">Inner exception to wrap.</param>
        public SportsDataException(string message, Exception exception)
            : base(message, exception)
        { }
    }

    class NFFMetaServiceException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FrontendException"/> class.
        /// </summary>
        /// <remarks>Specifies an error message only.</remarks>
        /// <param name="message">The error message</param>
        public NFFMetaServiceException (string message)
            : base(message)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="FrontendException"/> class.
        /// </summary>
        /// <remarks>
        /// Allows for specification of both an error message and an inner exception
        /// </remarks>
        /// <param name="message">The error message.</param>
        /// <param name="exception">Inner exception to wrap.</param>
        public NFFMetaServiceException(string message, Exception exception)
            : base(message, exception)
        { }
    }

    class NFFOrganizationServiceException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FrontendException"/> class.
        /// </summary>
        /// <remarks>Specifies an error message only.</remarks>
        /// <param name="message">The error message</param>
        public NFFOrganizationServiceException(string message)
            : base(message)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="FrontendException"/> class.
        /// </summary>
        /// <remarks>
        /// Allows for specification of both an error message and an inner exception
        /// </remarks>
        /// <param name="message">The error message.</param>
        /// <param name="exception">Inner exception to wrap.</param>
        public NFFOrganizationServiceException(string message, Exception exception)
            : base(message, exception)
        { }
    }


    class NFFTournamentServiceException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FrontendException"/> class.
        /// </summary>
        /// <remarks>Specifies an error message only.</remarks>
        /// <param name="message">The error message</param>
        public NFFTournamentServiceException(string message)
            : base(message)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="FrontendException"/> class.
        /// </summary>
        /// <remarks>
        /// Allows for specification of both an error message and an inner exception
        /// </remarks>
        /// <param name="message">The error message.</param>
        /// <param name="exception">Inner exception to wrap.</param>
        public NFFTournamentServiceException(string message, Exception exception)
            : base(message, exception)
        { }
    }
}
