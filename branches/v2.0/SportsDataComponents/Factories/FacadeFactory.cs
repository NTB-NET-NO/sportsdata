﻿using NTB.SportsData.Service.Facade.Interfaces;
using NTB.SportsData.Service.Facade.Soccer;
using NTB.SportsData.Service.Facade.Sports;

namespace NTB.SportsData.Service.Components.Factories
{
    /// <summary>
    ///     This factory is used to return some sort of facade layer. This based on sport id
    /// </summary>
    class FacadeFactory
    {
        public IFacade GetFacade(int sportId)
        {
            if (sportId == 16)
            {
                return new SoccerFacade();
            }

            return new SportsFacade();
        }
    }
}
