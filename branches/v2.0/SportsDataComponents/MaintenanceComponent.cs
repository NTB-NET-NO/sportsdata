﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MaintenanceComponent.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The maintenance component.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Threading;
using System.Xml;
using NTB.SportsData.Service.Components.Interfaces;
using NTB.SportsData.Service.Components.Nff;
using NTB.SportsData.Service.Domain.Enums;
using log4net;

namespace NTB.SportsData.Service.Components
{
    // Adding support for Log4Net

    // Adding Nif namespace
    // using NTB.SportsData.Components.Nif;

    /// <summary>
    /// The maintenance component.
    /// </summary>
    public partial class MaintenanceComponent : Component, IBaseSportsDataInterfaces
    {
        /// <summary>
        /// The Configured interval for this instance, used for Continous polling
        /// </summary>
        /// <remarks>
        ///   <para>Indicates the interval time in seconds for <c>Continous</c>polling. 60 means that the job runs every minute.</para>
        ///   <para>Default value: <c>60</c></para>
        /// </remarks>
        protected int Interval = 60;

        /// <summary>
        /// The Configured job name for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="InstanceName"/></remarks>
        protected string Name;

        /// <summary>
        /// Flag to indicate sucessful configure. Instance will not start polling nor handle messages if not properly Configured
        /// </summary>
        /// <remarks>Holds the Configured status of the instance. <c>True</c> means successfully Configured</remarks>
        protected bool Configured = false;

        /// <summary>
        /// Error-Retry flag
        /// </summary>
        /// <remarks>The flag is being set if a network or EWS error is encountered. Current operations are being aborted for later retry.</remarks>
        protected bool ErrorRetry = false;

        /// <summary>
        /// Send Email-notifications when a new messages is processed
        /// </summary>
        /// <remarks>Set to an email address. Multiple adresses are supported, separate with <c>;</c></remarks>
        protected string EmailNotification;

        /// <summary>
        /// Subject for email notifications
        /// </summary>
        /// <remarks>The subject is built during processing and used when sending the email when processing completes</remarks>
        protected string EmailSubject;

        /// <summary>
        /// Body for email notifications
        /// </summary>
        /// <remarks>The body is built during processing and used when sending the email when processing completes</remarks>
        protected string EmailBody;

        /// <summary>
        /// Defining the Schedule Type for this Component
        /// </summary>
        protected ScheduleType ScheduleType;

        /// <summary>
        /// Input file filter
        /// </summary>
        /// <remarks>
        ///   <para>Defines what file types are being read from <see cref="FileInputFolder"/>.</para>
        ///   <para>Default value: <c>NTBIDType.None</c></para>
        /// </remarks>
        protected string FileFilter = "*.xml";

        /// <summary>
        /// Watch subdirs for new files as well
        /// </summary>
        /// <remarks>
        ///   <para>Defines if the instance should look in subdirectories within <see cref="FileInputFolder"/>.</para>
        ///   <para>Default value: <c>False</c></para>
        /// </remarks>
        protected bool IncludeSubdirs = false;

        /// <summary>
        /// The output folder.
        /// </summary>
        protected string OutputFolder = string.Empty;

        /// <summary>
        /// Buffer size for the file watcher
        /// </summary>
        /// <remarks>
        ///   <para>Defines the internal buffer size the FileSystemWatcher when using this polling.
        /// Should not be set to high. The instance will fall back to contious polling on buffer overflows.</para>
        ///   <para>Default value: <c>4096</c></para>
        /// </remarks>
        protected int BufferSize = 4096;

        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(MaintenanceComponent));

        /// <summary>
        /// Busy status event
        /// </summary>
        private readonly AutoResetEvent _busyEvent = new AutoResetEvent(true);

        /// <summary>
        /// Exit control event
        /// </summary>
        private readonly AutoResetEvent _stopEvent = new AutoResetEvent(false);

        /// <summary>
        /// Initializes static members of the <see cref="MaintenanceComponent"/> class.
        /// </summary>
        static MaintenanceComponent()
        {
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MaintenanceComponent"/> class.
        /// </summary>
        public MaintenanceComponent()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MaintenanceComponent"/> class.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        public MaintenanceComponent(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        /// <summary>
        /// Gets a value indicating whether the Enabled status of the instance.
        /// </summary>
        /// <value>The Enabled status.</value>
        /// <remarks><c>True</c> if the job instance is Enabled.</remarks>
        public bool Enabled
        {
            get { return enabled; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether database populated.
        /// </summary>
        public bool DatabasePopulated { get; set; }

        /// <summary>
        /// Gets the instance name.
        /// </summary>
        /// <summary>
        /// Gets the name of the instance.
        /// </summary>
        /// <value>The name of the instance.</value>
        /// <remarks>The Configured instance job name.</remarks>
        public string InstanceName
        {
            get { return Name; }
        }

        /// <summary>
        /// Gets the operation mode. 
        /// </summary>
        /// <value>The operation mode.</value>
        /// <remarks><c>Gatherer</c> is the only valid mode, its hard coded for this component.</remarks>
        public OperationMode OperationMode
        {
            get { return OperationMode.Gatherer; }
        }

        /// <summary>
        /// Gets the poll style.
        /// </summary>
        /// <value>The poll style.</value>
        /// <remarks>Contionous, Scheduled and FileSystemWatch are valid for <c>Gatherer</c> objects.</remarks>
        public PollStyle PollStyle
        {
            get { return pollStyle; }
        }

        /// <summary>
        /// Gets or sets the component state.
        /// </summary>
        public ComponentState ComponentState { get; set; }

        /// <summary>
        /// Gets or sets the maintenance mode.
        /// </summary>
        public MaintenanceMode MaintenanceMode { get; set; }

        /// <summary>
        /// Gets or sets the Federation Id.
        /// </summary>
        protected int FederationId { get; set; }

        /// <summary>
        /// Gets or sets the Sport Id.
        /// </summary>
        protected int SportId { get; set; }

        /// <summary>
        /// Gets or sets the file input folder.
        /// </summary>
        protected string FileInputFolder { get; set; }

        /// <summary>
        /// Gets or sets the file done folder.
        /// </summary>
        protected string FileDoneFolder { get; set; }

        /// <summary>
        /// Gets or sets the file error folder.
        /// </summary>
        protected string FileErrorFolder { get; set; }

        /// <summary>
        /// Gets or sets the file output folder.
        /// </summary>
        protected string FileOutputFolder { get; set; }

        /// <summary>
        /// Gets or sets the poll delay.
        /// </summary>
        protected int PollDelay { get; set; }

        /// <summary>
        /// The configure.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        /// <exception cref="ArgumentException">
        /// Returns an exception if we cannot configure the component
        /// </exception>
        public void Configure(XmlNode configNode)
        {
            // Basic configuration sanity check
            if (configNode.Attributes != null
                && (configNode == null || configNode.Name != "MaintenanceComponent"
                    || configNode.Attributes.GetNamedItem("Name") == null))
            {
                throw new ArgumentException("The XML configuration node passed is invalid", "configNode");
            }

            if (Thread.CurrentThread.Name == null && configNode.Attributes != null)
            {
                Thread.CurrentThread.Name = configNode.Attributes.GetNamedItem("Name").ToString();
            }

            #region Basic config

            // Getting the name of this Component instance
            try
            {
                if (configNode.Attributes != null)
                {
                    this.Name = configNode.Attributes["Name"].Value;
                    this.enabled = Convert.ToBoolean(configNode.Attributes["Enabled"].Value);
                    this.FederationId = Convert.ToInt32(configNode.Attributes["FederationId"].Value);
                    this.SportId = Convert.ToInt32(configNode.Attributes["SportId"].Value);
                }

                ThreadContext.Stacks["NDC"].Push(InstanceName);

                if (configNode.Attributes != null && configNode.Attributes["InsertDatabase"] != null)
                {
                    DatabasePopulated = Convert.ToBoolean(configNode.Attributes["InsertDatabase"].Value);
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to configure this job instance!", ex);
            }

            // Basic operation
            try
            {
                if (configNode.Attributes != null)
                {
                    this.operationMode = (OperationMode) Enum.Parse(typeof(OperationMode), configNode.Attributes["OperationMode"].Value, true);
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing OperationMode values in XML configuration node", ex);
            }

            try
            {
                if (configNode.Attributes != null)
                {
                    this.pollStyle =
                        (PollStyle)Enum.Parse(typeof(PollStyle), configNode.Attributes["PollStyle"].Value, true);
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing PollStyle values in XML configuration node", ex);
            }

            // Setting PollDelay to default three seconds
            this.PollDelay = 3;
            try
            {
                if (configNode.Attributes != null)
                {
                    this.PollDelay = Convert.ToInt32(configNode.Attributes["Delay"].Value);
                }
            }
            catch (Exception ex)
            {
                ThreadContext.Stacks["NDC"].Pop();
                throw new ArgumentException("Invalid or missing PollStyle values in XML configuration node", ex);
            }

            #endregion
            
            try
            {
                // Find the file folder to work with
                XmlNode folderNode = configNode.SelectSingleNode("Folders/FileInputFolder");

                // configNode.SelectSingleNode("Folder").Attributes("XMLOutputFolder");
                if (folderNode != null)
                {
                    FileInputFolder = folderNode.InnerText;

                    // Check if folder exists
                    DirectoryInfo di = new DirectoryInfo(FileInputFolder);
                    if (!di.Exists)
                    {
                        di.Create();
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Fatal("Not possible to get input-folder", exception);
            }

            // Find the file folder to work with
            try
            {
                XmlNode folderNode = configNode.SelectSingleNode("Folders/FileOutputFolder");

                // configNode.SelectSingleNode("Folder").Attributes("XMLOutputFolder");
                if (folderNode != null)
                {
                    FileOutputFolder = folderNode.InnerText;

                    // Check if folder exists
                    DirectoryInfo di = new DirectoryInfo(FileOutputFolder);
                    if (!di.Exists)
                    {
                        di.Create();
                    }
                }
                /*
                Logger.InfoFormat(
                    "Converter job - Polling: {0} / File input folder: {1} / Enabled: {2}",
                    Enum.GetName(typeof(PollStyle), pollStyle),
                    FileInputFolder,
                    enabled);
                 */
            }
            catch (Exception exception)
            {
                Logger.Fatal("Not possible to get output-folder", exception);
            }

            // Find the file folder to work with
            try
            {
                XmlNode folderNode = configNode.SelectSingleNode("Folders/FileErrorFolder");

                if (folderNode != null)
                {
                    FileErrorFolder = folderNode.InnerText;

                    // Check if folder exists
                    DirectoryInfo di = new DirectoryInfo(FileErrorFolder);
                    if (!di.Exists)
                    {
                        di.Create();
                    }
                }
                /*
                Logger.InfoFormat(
                    "Converter job - Polling: {0} / File input folder: {1} / Enabled: {2}",
                    Enum.GetName(typeof(PollStyle), pollStyle),
                    FileInputFolder,
                    enabled);
                 */
            }
            catch (Exception exception)
            {
                Logger.Fatal("Not possible to get error-folder", exception);
            }

            // Find the file folder to work with
            try
            {
                XmlNode folderNode = configNode.SelectSingleNode("Folders/FileDoneFolder");

                if (folderNode != null)
                {
                    FileDoneFolder = folderNode.InnerText;

                    // Check if folder exists
                    DirectoryInfo di = new DirectoryInfo(FileDoneFolder);
                    if (!di.Exists)
                    {
                        di.Create();
                    }
                }
                /*
                Logger.InfoFormat(
                    "Converter job - Polling: {0} / File input folder: {1} / Enabled: {2}",
                    Enum.GetName(typeof(PollStyle), pollStyle),
                    FileInputFolder,
                    enabled);
                 */
            }
            catch (Exception exception)
            {
                Logger.Fatal("Not possible to get done-folder", exception);
            }

            #region Set up polling

            try
            {
                // Switch on pollstyle
                switch (pollStyle)
                {
                    case PollStyle.Continous:
                        Interval = Convert.ToInt32(configNode.Attributes["Interval"].Value);
                        if (Interval == 0)
                        {
                            Interval = 5000;
                        }

                        pollTimer.Interval = Interval * 1000; // short startup - interval * 1000;
                        Logger.DebugFormat("Poll interval: {0} seconds", Interval);

                        break;

                    case PollStyle.Scheduled:

                        /* 
                        schedule = configNode.Attributes["Schedule"].Value;
                        TimeSpan s = Utilities.GetScheduleInterval(schedule);
                        logger.DebugFormat("Schedule: {0} Calculated time to next run: {1}", schedule, s);
                        pollTimer.Interval = s.TotalMilliseconds;
                        */
                        throw new NotSupportedException("Invalid polling style for this job type");

                    case PollStyle.FileSystemWatch:

                        // Check for config overrides
                        if (configNode.Attributes.GetNamedItem("BufferSize") != null)
                        {
                            BufferSize = Convert.ToInt32(configNode.Attributes["BufferSize"].Value);
                        }

                        Logger.DebugFormat("FileSystemWatcher buffer size: {0}", BufferSize);

                        // Set values
                        filesWatcher.Path = FileInputFolder;
                        filesWatcher.Filter = FileFilter;
                        filesWatcher.IncludeSubdirectories = IncludeSubdirs;
                        filesWatcher.InternalBufferSize = BufferSize;

                        // Do not start the event watcher here, wait until after the startup cleaning job
                        pollTimer.Interval = 5000; // short startup;

                        break;

                    default:

                        // Unsupported pollstyle for this object
                        throw new NotSupportedException("Invalid polling style for this job type");
                }

                Configured = true;
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                Configured = false;
            }

            #endregion
        }

        /// <summary>
        /// The start.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// If the component has not been implemented, an exception will occur
        /// </exception>
        public void Start()
        {
            if (!Configured)
            {
                throw new SportsDataException("MaintenanceComponent is not properly Configured. MaintenanceComponent::Start() Aborted!");
            }

            if (!Enabled)
            {
                throw new SportsDataException("MaintenanceComponent is not Enabled. MaintenanceComponent::Start() Aborted!");
            }

            Logger.Debug("In Start() " + InstanceName + ".");

            _stopEvent.Reset();
            _busyEvent.Set();

            ComponentState = ComponentState.Running;

            pollTimer.Start();
        }

        /// <summary>
        /// The stop.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// If the component has not been implemented, an exception will occur
        /// </exception>
        public void Stop()
        {
            if (!Configured)
            {
                throw new SportsDataException("NFFWSComponent is not properly Configured. NFFComponent::Stop() Aborted");
            }

            if (!Enabled)
            {
                throw new SportsDataException("NFFWSComponent is not properly Configured. NFFComponent::Stop() Aborted");
            }

            // Signal Stop
            _stopEvent.Set();

            // Stop events
            filesWatcher.EnableRaisingEvents = false;

            if (!_busyEvent.WaitOne(30000))
            {
                Logger.InfoFormat(
                    "Instance did not complete properly. Data may be inconsistent. Job: {0}", InstanceName);
            }

            ComponentState = ComponentState.Halted;

            // Kill polling
            pollTimer.Stop();
        }

        /// <summary>
        /// The files watcher_ created.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void filesWatcher_Created(object sender, FileSystemEventArgs e)
        {
            ThreadContext.Properties["JOBNAME"] = InstanceName;
            Logger.DebugFormat("MaintenanceComponent::filesWatcher_Created() hit with file data: {0}", e.FullPath);

            if (PollDelay > 0)
            {
                // We take the value and multiply with thousand - to get the seconds... 
                int pollTime = PollDelay * 1000;

                Logger.Debug("We are waiting: " + PollDelay + " Seconds before starting to maintain!");

                Thread.Sleep(pollTime);
            }

            try
            {
                // Wait for the busy state
                _busyEvent.WaitOne();

                // Adding files to the list and then sending that list to the converter
                // @done: Create a method that takes string as argument also
                // @done: change XMLToNITFConverter so that it does not loop, but does only one file at the time
                List<string> files = new List<string> { e.FullPath };

                if (FederationId == 365)
                {
                    NffMaintenance nffMaintentance = new NffMaintenance
                        {
                            OutputFolder = FileOutputFolder
                        };

                    foreach (string file in files)
                    {
                        try
                        {
                            nffMaintentance.TournamentRouter(file, SportId);

                            // Moving to done folder
                            Logger.Info("Moving file to done folder");

                            FileInfo fileInfo = new FileInfo(file);
                            string filename = fileInfo.Name;

                            string moveFilename = FileDoneFolder + @"\" + filename;
                            FileInfo fileInfoMove = new FileInfo(moveFilename);

                            if (fileInfoMove.Exists)
                            {
                                fileInfoMove.Delete();
                            }

                            fileInfo.MoveTo(moveFilename);

                            _busyEvent.Set();
                        }
                        catch (Exception exception)
                        {
                            Logger.Error(exception.Message);
                            Logger.Error(exception.StackTrace);

                            // Moving to error folder
                            Logger.Info("Moving file to error folder");

                            FileInfo fileInfo = new FileInfo(file);
                            string filename = fileInfo.Name;

                            string moveFilename = FileErrorFolder + @"\" + filename;
                            FileInfo fileInfoMove = new FileInfo(moveFilename);

                            if (fileInfoMove.Exists)
                            {
                                fileInfoMove.Delete();
                            }

                            fileInfo.MoveTo(moveFilename);
                        }
                    }
                }
                else
                {
                    // TODO: Write the code for maintenance of tournaments that comes from NIF services
                    // NifMaintenance nifMaintenance = new NifMaintenance();
                }
            }
            catch (Exception exception)
            {
                filesWatcher.EnableRaisingEvents = false;
                Logger.Debug("Something happened while doing filesWatcher_Created!", exception);
                filesWatcher.EnableRaisingEvents = true;
            }
        }

        /// <summary>
        /// The poll timer_ elapsed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void pollTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            /*
             * We can consider adding getting data here if the time (day + hour + minute) hits. Otherwize we shall do nothing.
             * 
             */
            ThreadContext.Properties["JOBNAME"] = InstanceName;

            Logger.Debug("MaintenanceComponent::pollTimer_Elapsed() hit");
            Logger.Debug("We have the following settings: Interval: " + pollTimer.Interval);

            ThreadContext.Properties["JOBNAME"] = InstanceName;
            Logger.DebugFormat("MaintenanceComponent::pollTimer_Elapsed() hit");

            // Reset flags and pollers
            _busyEvent.WaitOne();
            pollTimer.Stop();

            try
            {
                Logger.Debug("InputFolder for " + InstanceName + " : " + FileInputFolder);

                // Process any waiting files
                List<string> files = new List<string>(Directory.GetFiles(FileInputFolder, "*.xml", SearchOption.TopDirectoryOnly));

                if (FederationId == 365)
                {
                    NffMaintenance nffMaintentance = new NffMaintenance
                                                         {
                                                          OutputFolder = FileOutputFolder
                                                         };

                    foreach (string file in files)
                    {
                        try
                        {
                            // nffMaintentance.SendTournamentsToClient(file);
                            nffMaintentance.TournamentRouter(file, SportId);

                            // Moving to done folder
                            Logger.Info("Moving file to done folder");

                            FileInfo fileInfo = new FileInfo(file);
                            string filename = fileInfo.Name;

                            string moveFilename = FileDoneFolder + @"\" + filename;
                            FileInfo fileInfoMove = new FileInfo(moveFilename);

                            if (fileInfoMove.Exists)
                            {
                                fileInfoMove.Delete();
                            }

                            fileInfo.MoveTo(moveFilename);
                        }
                        catch (Exception exception)
                        {
                            Logger.Error(exception.Message);
                            Logger.Error(exception.StackTrace);

                            // Moving to error folder
                            Logger.Info("Moving file to error folder");

                            FileInfo fileInfo = new FileInfo(file);
                            string filename = fileInfo.Name;

                            string moveFilename = FileErrorFolder + @"\" + filename;
                            FileInfo fileInfoMove = new FileInfo(moveFilename);

                            if (fileInfoMove.Exists)
                            {
                                fileInfoMove.Delete();
                            }

                            fileInfo.MoveTo(moveFilename);
                        }
                    }
                }
                else
                {
                    // TODO: Write the code for maintenance of tournaments that comes from NIF services
                    // NifMaintenance nifMaintenance = new NifMaintenance();
                }

                // Some logging
                Logger.DebugFormat("MaintenanceComponent::pollTimer_Elapsed() Maintenance items scheduled: {0}", files.Count);
                
                // Enable the event listening
                if (pollStyle == PollStyle.FileSystemWatch && !ErrorRetry)
                {
                    Logger.Debug("Setting filesWatcher.EnableRaisingEvents to true");
                    filesWatcher.EnableRaisingEvents = true;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("MaintenanceComponent::pollTimer_Elapsed() error: " + ex.Message, ex);
            }

            // Check error state
            if (ErrorRetry)
            {
                filesWatcher.EnableRaisingEvents = false;
            }

            // Restart
            pollTimer.Interval = Interval * 1000;
            if (pollStyle != PollStyle.FileSystemWatch
                || (pollStyle == PollStyle.FileSystemWatch && filesWatcher.EnableRaisingEvents == false))
            {
                pollTimer.Start();
            }

            _busyEvent.Set();
        }
    }
}
