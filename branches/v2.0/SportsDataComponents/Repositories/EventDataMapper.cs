﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.SportsData.Service.Components.Interfaces;
using NTB.SportsData.Service.Domain.Classes;
using log4net;

namespace NTB.SportsData.Service.Components.Repositories
{
    class EventDataMapper : IRepository<SportEvent>, IDisposable, IEventDataMapper
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(EventDataMapper));

        public EventDataMapper()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();

            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        public int InsertOne(SportEvent domainobject)
        {
            try
            {
                using (
                    SqlConnection sqlConnection = new SqlConnection(
                        ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        sqlConnection.Open();
                    }

                    SqlCommand sqlCommand = new SqlCommand("Service_InsertMatches", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Clear();

                    sqlCommand.Parameters.Add(new SqlParameter("@EventId", domainobject.EventId));

                    sqlCommand.Parameters.Add(new SqlParameter("@ActivityId", domainobject.ActivityId));

                    sqlCommand.Parameters.Add(new SqlParameter("@EventName", domainobject.EventName));

                    sqlCommand.Parameters.Add(new SqlParameter("@Location", domainobject.EventLocation));

                    sqlCommand.Parameters.Add(new SqlParameter("@DateStart", domainobject.EventDateStart));

                    sqlCommand.Parameters.Add(new SqlParameter("@DateEnd", domainobject.EventDateEnd));

                    sqlCommand.Parameters.Add(new SqlParameter("@TimeStart", domainobject.EventTimeStart));

                    sqlCommand.Parameters.Add(new SqlParameter("@TimeEnd", domainobject.EventTimeEnd));

                    sqlCommand.Parameters.Add(new SqlParameter("@OrganizerId", domainobject.EventOrganizerId));

                    sqlCommand.ExecuteNonQuery();

                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }

                    return 0;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);

                return 0;
            }
        }

        public void InsertAll(List<SportEvent> domainobject)
        {
            foreach (SportEvent sportEvent in domainobject)
            {
                InsertOne(sportEvent);
            }
        }

        public void Update(SportEvent domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(SportEvent domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<SportEvent> GetAll()
        {
            throw new NotImplementedException();
        }

        public SportEvent Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
