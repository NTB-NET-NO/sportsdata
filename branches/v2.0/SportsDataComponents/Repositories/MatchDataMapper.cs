﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.SportsData.Service.Components.Interfaces;
using NTB.SportsData.Service.Domain.Classes;
using log4net;

namespace NTB.SportsData.Service.Components.Repositories
{
    class MatchDataMapper : IRepository<Match>, IDisposable, IMatchDataMapper
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(MatchDataMapper));

        public MatchDataMapper()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();

            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        public int InsertOne(Match domainobject)
        {
            try
            {
                using (
                    SqlConnection sqlConnection = new SqlConnection(
                        ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        sqlConnection.Open();
                    }

                        SqlCommand sqlCommand = new SqlCommand("Service_InsertMatches", sqlConnection)
                            {
                                CommandType = CommandType.StoredProcedure
                            };

                        sqlCommand.Parameters.Clear();

                        sqlCommand.Parameters.Add(new SqlParameter("@MatchId", domainobject.MatchId));

                        sqlCommand.Parameters.Add(new SqlParameter("@HomeTeam", domainobject.HomeTeamName));

                        sqlCommand.Parameters.Add(new SqlParameter("@AwayTeam", domainobject.AwayTeamName));

                        sqlCommand.Parameters.Add(new SqlParameter("@Date", domainobject.MatchStartDate));

                        sqlCommand.Parameters.Add(new SqlParameter("@Time", domainobject.MatchStartDate));

                        sqlCommand.Parameters.Add(new SqlParameter("@Downloaded", domainobject.Downloaded));

                        sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", domainobject.TournamentId));

                        sqlCommand.Parameters.Add(new SqlParameter("@Sportid", domainobject.SportId));

                        sqlCommand.ExecuteNonQuery();

                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }

                    return 0;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);

                return 0;
            }
        }

        public void InsertAll(List<Match> domainobject)
        {
            foreach (Match match in domainobject)
            {
                match.Downloaded = 0;

                InsertOne(match);
            }
        }

        public void Update(Match domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Match domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Match> GetAll()
        {
            throw new NotImplementedException();
        }

        public Match Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<int> FindMissingMatches()
        {
            List<int> missingMatches = new List<int>();
            using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
            {
                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                    sqlConnection.Open();
                }

                SqlCommand sqlCommand = new SqlCommand("Service_GetMissingMatches", sqlConnection);

                sqlCommand.Parameters.Clear();

                sqlCommand.Parameters.Add(new SqlParameter("@MatchDate", DateTime.Today));

                sqlCommand.CommandType = CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    while (sqlDataReader.Read())
                    {
                        missingMatches.Add(Convert.ToInt32(sqlDataReader["Matchid"]));
                    }
                }

                return missingMatches;
            }
        }

        public List<int> FindAllMissingMatches()
        {
            List<int> missingMatches = new List<int>();
            using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
            {
                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                    sqlConnection.Open();
                }

                SqlCommand sqlCommand = new SqlCommand("Service_GetMissingMatches", sqlConnection);

                sqlCommand.Parameters.Clear();

                sqlCommand.CommandType = CommandType.StoredProcedure;

                SqlDataReader reader = sqlCommand.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        missingMatches.Add(Convert.ToInt32(reader["Matchid"]));
                    }
                }

                return missingMatches;
            }
        }

        /// <summary>
        /// Update match not found is used when we have checked the match, but still cannot get information from NFF
        /// </summary>
        /// <param name="match">
        /// Match object 
        /// </param>
        /// <param name="tournament">
        /// Tournament object
        /// </param>
        public void UpdateMatchNotFound(Match match, Tournament tournament)
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        sqlConnection.Open();
                    }

                    SqlCommand sqlCommand = new SqlCommand("Service_UpdateMissingMatch", sqlConnection);

                    // DONE: When connection is moved, add this code
                    sqlCommand.Parameters.Clear();

                    sqlCommand.Parameters.Add(new SqlParameter("@MatchId", match.MatchId));

                    sqlCommand.Parameters.Add(new SqlParameter("@LastChecked", DateTime.Today));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.ExecuteNonQuery();

                    // DONE: Move this closing to outside the loop
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
        }

        /// <summary>
        /// The insert match not found.
        /// </summary>
        /// <param name="match">
        /// The match.
        /// </param>
        /// <param name="tournament">
        /// The tournament.
        /// </param>
        public void InsertMatchNotFound(Match match, Tournament tournament)
        {
            try
            {
                using (
                    SqlConnection sqlConnection = new SqlConnection(
                        ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        sqlConnection.Open();
                    }

                    SqlCommand sqlCommand = new SqlCommand("Service_InsertMissingMatch", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    // DONE: When connection is moved, add this code
                    sqlCommand.Parameters.Clear();

                    sqlCommand.Parameters.Add(new SqlParameter("@MatchId", match.MatchId));

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", match.TournamentId));

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentNo", tournament.TournamentNumber));

                    sqlCommand.Parameters.Add(new SqlParameter("@MatchDate", match.MatchStartDate));

                    sqlCommand.Parameters.Add(new SqlParameter("@LastChecked", DateTime.Today));

                    sqlCommand.ExecuteNonQuery();

                    // DONE: Move this closing to outside the loop
                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
        }

        /// <summary>
        /// Method to find out if the matches table is empty or not. 
        /// Returns the number of matches we have stored in the database
        /// </summary>
        /// <returns>Integer value</returns>
        public int GetNumberOfMatches()
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        sqlConnection.Open();
                    }

                    SqlCommand sqlCommand = new SqlCommand("Service_GetNumberOfMatches", sqlConnection)
                    {
                        CommandType
                            =
                            CommandType
                            .StoredProcedure
                    };

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    int numberOfMatches = 0;
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            if (sqlDataReader["NumberOfMatches"] != DBNull.Value)
                            {
                                numberOfMatches = Convert.ToInt32(sqlDataReader["NumberOfMatches"].ToString());
                            }
                        }

                        return numberOfMatches;
                    }

                    sqlConnection.Close();
                    sqlConnection.Dispose();
                    return 0;
                }
            }
            catch (SqlException sqlexception)
            {
                Logger.Error(sqlexception);
                Logger.Error(sqlexception);
                return 0;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                Logger.Error(exception);
                return 0;
            }
        }

        public bool FindMatchByMatch(Match match)
        {
            using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
            {
                SqlCommand sqlCommand = new SqlCommand("Service_GetMatchByMatchId", sqlConnection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                sqlCommand.Parameters.Add(new SqlParameter("@MatchId", match.MatchId));

                sqlCommand.Parameters.Add(new SqlParameter("@SportId", match.SportId));

                // opening the first connection
                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                    sqlConnection.Open();
                }

                // Creating the SQL-reader.
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (sqlDataReader.HasRows)
                {
                    return true;
                }
            }

            return false;
        }

        public List<Match> CheckMatches(List<Match> matches)
        {
            return matches.Where(match => !FindMatchByMatch(match)).ToList();
        }

        /// <summary>
        ///     Returns list of matches for selected day for selected sport
        /// </summary>
        /// <param name="matchDateTime"></param>
        /// <param name="sportId"></param>
        /// <returns></returns>
        public List<Match> GetMatchesByDate(DateTime matchDateTime, int sportId)
        {
            using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"])
                )
            {
                SqlCommand sqlCommand = new SqlCommand("[Service_GetMatchesByDate]", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                sqlCommand.Parameters.Add(new SqlParameter("@MatchDateTime", matchDateTime));

                sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (!sqlDataReader.HasRows)
                {
                    return null;
                }

                var matches = new List<Match>();
                while (sqlDataReader.Read())
                {
                    Match match = new Match
                        {
                            MatchId = Convert.ToInt32(sqlDataReader["MatchId"]),
                            HomeTeamName = sqlDataReader["HomeTeam"].ToString(),
                            AwayTeamName = sqlDataReader["AwayTeam"].ToString(),
                            TournamentId = Convert.ToInt32(sqlDataReader["TournamentId"]),
                            SportId = sportId
                        };

                    matches.Add(match);
                }

                return matches;
            }
        }
    }
}
