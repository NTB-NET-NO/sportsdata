﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.SportsData.Service.Components.Interfaces;
using NTB.SportsData.Service.Domain.Classes;
using log4net;

namespace NTB.SportsData.Service.Components.Repositories
{
    class TournamentDataMapper : IRepository<Tournament>, IDisposable, ITournamentDataMapper
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(TournamentDataMapper));

        public TournamentDataMapper()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();

            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        public int InsertOne(Tournament domainobject)
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    // Open the connection to the database
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        sqlConnection.Open();
                    }

                    try
                    {
                        // Creating the Sql-Command using the Stored Procedure: Service_InsertTournament
                        SqlCommand sqlCommand = new SqlCommand("Service_InsertTournament", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                        // Telling the system that the Query is a StoredProcedure
                        int pushChanges = 0; // false
                        if (domainobject.PushChanges)
                        {
                            pushChanges = 1;
                        }

                        sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", domainobject.TournamentId));

                        sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", domainobject.DistrictId));

                        sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", domainobject.SeasonId));

                        sqlCommand.Parameters.Add(new SqlParameter("@TournamentName", domainobject.TournamentName));

                        sqlCommand.Parameters.Add(new SqlParameter("@Push", pushChanges));

                        sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryId", domainobject.AgeCategoryId));

                        sqlCommand.Parameters.Add(new SqlParameter("@GenderId", domainobject.GenderId));

                        sqlCommand.Parameters.Add(new SqlParameter("@TournamentTypeId", domainobject.TournamentTypeId));

                        sqlCommand.Parameters.Add(new SqlParameter("@TournamentNumber", domainobject.TournamentNumber));

                        // Done: Need to set this in the Config job
                        sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));

                        // This is for football/soccer and is hard coded, not good...

                        // We must populate the database
                        sqlCommand.ExecuteNonQuery();

                        sqlConnection.Close();

                        return 0;
                    }
                    catch (Exception e)
                    {
                        Logger.Error(e.Message);
                        Logger.Error(e.StackTrace);
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
            return 0;
        }

        public void InsertAll(List<Tournament> domainobject)
        {
            foreach (Tournament tournament in domainobject)
            {
                InsertOne(tournament);
            }
        }

        public void Update(Tournament domainobject)
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        sqlConnection.Open();
                    }

                    SqlCommand sqlCommand = new SqlCommand("Service_UpdateTournament", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", domainobject.TournamentId));

                    sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", domainobject.DistrictId));

                    sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", domainobject.SeasonId));

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId)); // hardcoded to soccer

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentName", domainobject.TournamentName));

                    sqlCommand.Parameters.Add(new SqlParameter("@Push", domainobject.PushChanges));

                    sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryId", domainobject.AgeCategoryId));

                    sqlCommand.Parameters.Add(new SqlParameter("@GenderId", domainobject.GenderId));

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentTypeId", domainobject.TournamentTypeId));

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentNumber", domainobject.TournamentNumber));

                    sqlCommand.ExecuteNonQuery();

                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }
        }

        public void Delete(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Tournament> GetAll()
        {
            throw new NotImplementedException();
        }

        public Tournament Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Tournament GetTournamentByMatchId(int matchId)
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        sqlConnection.Open();
                    }

                    Tournament tournament = new Tournament();
                    SqlCommand sqlCommand = new SqlCommand("Service_GetTournamentIdByMatchId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@MatchId", matchId));

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            if (sqlDataReader["TournamentId"] != DBNull.Value)
                            {
                                tournament.TournamentId = Convert.ToInt32(sqlDataReader["TournamentId"].ToString());
                            }
                        }
                    }

                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }

                    return tournament;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
                return new Tournament();
            }
        }

        public bool CheckTournamentById(int tournamentId, int sportId)
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        sqlConnection.Open();
                    }

                    Logger.Info("Checking if Tournament Database Table is populated with correct values");

                    SqlCommand sqlCommand = new SqlCommand("Service_GetTournamentByTournamentId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", tournamentId));

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        Logger.Info("The query returned no rows. we are adding Tournament Information!");
                        sqlDataReader.Close();
                        return false;
                    }

                    sqlDataReader.Close();
                    
                    sqlCommand.Dispose();

                    sqlConnection.Close();

                    return true;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return false;
            }
        }

        // todo: Check this method - is it in use?
        public bool CheckTournamentTable(List<Tournament> tournaments, int sportId)
        {
            throw new NotImplementedException();
        }
    }
}
