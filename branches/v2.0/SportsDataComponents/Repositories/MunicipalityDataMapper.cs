﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.SportsData.Service.Components.Interfaces;
using NTB.SportsData.Service.Domain.Classes;
using log4net;

namespace NTB.SportsData.Service.Components.Repositories
{
    class MunicipalityDataMapper : IRepository<Municipality>, IDisposable, IMunicipalityDataMapper 
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(MunicipalityDataMapper));

        public MunicipalityDataMapper()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();

            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        public int InsertOne(Municipality domainobject)
        {
            using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"])
                )
            {
                
                // Using Stored Procedure: Service_InsertMunicipalities 
                SqlCommand sqlCommand = new SqlCommand("Service_InsertMunicipalities", sqlConnection)
                    {
                        Connection = sqlConnection,
                    };

                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                sqlCommand.Parameters.Add(new SqlParameter("@MunicipalityId", domainobject.MunicipalityId));

                sqlCommand.Parameters.Add(new SqlParameter("@MunicipalityName", domainobject.MunicipalityName));

                sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", domainobject.DistrictId));

                sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));

                // We must populate the database
                sqlCommand.ExecuteNonQuery();

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return 0;
            }
        }

        public void InsertAll(List<Municipality> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Municipality domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Municipality domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Municipality> GetAll()
        {
            throw new NotImplementedException();
        }

        public Municipality Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public bool CheckMunicipalityTable(int sportId)
        {
            using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"])
                )
            {
                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                // We are checking if the current ID and the district ID is in the database
                Logger.Info("Checking Municipality database table");

                // Using Stored Procedure: Service_GetMunicipalities
                SqlCommand sqlCommand = new SqlCommand("[Service_GetMunicipalitiesBySportId]", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure,
                    };

                sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                // If we don't get a hit, we shall insert into the database
                return sqlDataReader.HasRows;
            }
        }

        public List<Municipality> CheckMunicipalityTableAgainstMunicipalities(List<Municipality> municipalities)
        {
            List<Municipality> notFoundMunicipality = new List<Municipality>();
            using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
            {
                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                // We are checking if the current ID and the district ID is in the database
                Logger.Info("Checking Municipality database table");

                foreach (Municipality municipality in municipalities)
                {
                    // Using Stored Procedure: Service_GetMunicipalities
                    SqlCommand sqlCommand = new SqlCommand("[Service_GetMunicipalitiesBySportId]", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure,
                        };

                    sqlCommand.Parameters.Add(new SqlParameter("@MunicipalityId", municipality.MunicipalityId));

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    // If we don't get a hit, we shall insert into the database
                    if (!sqlDataReader.HasRows)
                    {
                        notFoundMunicipality.Add(municipality);

                    }

                    sqlDataReader.Close();

                    sqlCommand.Dispose();
                }
                
                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return notFoundMunicipality;
            }
        }

        public bool CheckMunicipaltityTable(List<Municipality> municipalities, District district, int sportId)
        {
            Logger.Info("Inserting data in Municipality database table for district " + district.DistrictName);

            /**
             * We are looping through the list of municipalities which depends on the district we are looping over
             * 
             */
            foreach (Municipality municipality in municipalities)
            {
                try
                {
                    using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                    {
                        if (sqlConnection.State != ConnectionState.Open)
                        {
                            sqlConnection.Open();
                        }

                        // We are checking if the current ID and the district ID is in the database
                        Logger.Info("Checking Municipality database table");

                        // Using Stored Procedure: Service_GetMunicipalities
                        SqlCommand sqlCommand = new SqlCommand("[Service_GetMunicipalitiesBySportId]", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure,
                        };

                        sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                        sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", district.DistrictId));

                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                        // If we don't get a hit, we shall insert into the database
                        if (!sqlDataReader.HasRows)
                        {
                            // Using Stored Procedure: Service_InsertMunicipalities 
                            SqlCommand commandInsert = new SqlCommand("Service_InsertMunicipalities", sqlConnection)
                            {
                                Connection = sqlConnection,
                            };

                            commandInsert.Parameters.Add(new SqlParameter("@MunicipalityId", municipality.MunicipalityId));

                            commandInsert.Parameters.Add(new SqlParameter("@MunicipalityName", municipality.MunicipalityName));

                            commandInsert.Parameters.Add(new SqlParameter("@DistrictId", district.DistrictId));

                            commandInsert.Parameters.Add(new SqlParameter("SportId", sportId));

                            // We must populate the database
                            commandInsert.ExecuteNonQuery();
                        }

                        sqlDataReader.Close();
                        sqlCommand.Dispose();

                        sqlConnection.Close();
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                    return false;
                }

                return true;
            }

            return true;
        }
    }
}
