﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.SportsData.Service.Components.Interfaces;
using NTB.SportsData.Service.Domain.Classes;
using log4net;

namespace NTB.SportsData.Service.Components.Repositories
{
    class AgeCategoryDataMapper : IRepository<AgeCategory>, IDisposable, IAgeCategoryDataMapper
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(AgeCategoryDataMapper));

        public AgeCategoryDataMapper()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();

            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        public int InsertOne(AgeCategory domainobject)
        {
            string connectionString = ConfigurationManager.AppSettings["ConnectionString"];
            SqlConnection sqlConnection = new SqlConnection(connectionString);

            // Open the connection to the database
            sqlConnection.Open();

            // We must insert
            SqlCommand sqlCommand = new SqlCommand("Service_InsertAgeCategory", sqlConnection)
            {
                CommandType = CommandType.StoredProcedure
            };

            // Setting CommandType to StoredProcedure
            sqlCommand.Parameters.Add(
                new SqlParameter("@AgeCategoryId", domainobject.AgeCategoryId));

            sqlCommand.Parameters.Add(new SqlParameter("@MinAge", domainobject.MinAge));

            sqlCommand.Parameters.Add(new SqlParameter("@MaxAge", domainobject.MaxAge));

            sqlCommand.Parameters.Add(
                new SqlParameter("@AgeCategoryName", domainobject.AgeCategoryName));

            sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));

            // We must populate the database
            sqlCommand.ExecuteNonQuery();

            if (sqlConnection.State == ConnectionState.Open)
            {
                sqlConnection.Close();
            }

            return 0;
        }

        public void InsertAll(List<AgeCategory> domainobject)
        {
            foreach (AgeCategory ageCategory in domainobject)
            {
                InsertOne(ageCategory);
            }
        }

        public void Update(AgeCategory domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(AgeCategory domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<AgeCategory> GetAll()
        {
            throw new NotImplementedException();
        }

        public AgeCategory Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<AgeCategory> CheckAgeCategories(List<AgeCategory> ageCategories, int sportId)
        {
            try
            {
                List<AgeCategory> notFoundAgeCategories = new List<AgeCategory>();

                using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    Logger.Info(
                        "Starting check of AgeCategories table and inserting necessary categories into database");

                    foreach (AgeCategory ageCategory in ageCategories)
                    {
                        Logger.Info("Checking databasetable for: " + ageCategory.AgeCategoryName);
                        SqlCommand sqlCommand = new SqlCommand("Service_GetAgeCategoryByCategoryId", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                        sqlCommand.Parameters.Add(new SqlParameter("@AgeCategoryId", ageCategory.AgeCategoryId));

                        sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                        // If we have not found this age category in the database, we shall add it to the list of not found.
                        // This so we can add this to the database table later
                        if (!sqlDataReader.HasRows)
                        {
                            foreach (AgeCategory agereturnCategory in ageCategories)
                            {
                                ageCategory.SportId = sportId;

                                notFoundAgeCategories.Add(agereturnCategory);
                            }
                        }
                        sqlDataReader.Close();
                    }

                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }

                    return notFoundAgeCategories;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return new List<AgeCategory>();
            }
        }
    }
}
