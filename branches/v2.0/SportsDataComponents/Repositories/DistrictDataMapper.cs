﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.SportsData.Service.Components.Interfaces;
using NTB.SportsData.Service.Domain.Classes;
using log4net;

namespace NTB.SportsData.Service.Components.Repositories
{
    class DistrictDataMapper : IRepository<District>, IDisposable, IDistrictDataMapper 
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SeasonDataMapper));

        public DistrictDataMapper()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();

            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        public int InsertOne(District domainobject)
        {
            using (
                SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"])
                )
            {
                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                Logger.Info("Checking Districts database table");

                SqlCommand sqlCommand = new SqlCommand("Service_InsertDistricts", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", domainobject.DistrictId));
                sqlCommand.Parameters.Add(new SqlParameter("@DistrictName", domainobject.DistrictName));
                sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));

                sqlCommand.ExecuteNonQuery();

                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
            return 0;
        }

        public void InsertAll(List<District> domainobject)
        {
            foreach (District district in domainobject)
            {
                InsertOne(district);
            }
        }

        public void Update(District domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(District domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<District> GetAll()
        {
            using (
                SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"])
                )
            {
                // check if the sql Connection is open or not
                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.Open();
                }

                Logger.Info("Get Districts database table");

                SqlCommand sqlCommand = new SqlCommand("Service_GetDistricts", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                sqlCommand.CommandType = CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                if (!sqlDataReader.HasRows)
                {
                    return null;
                }

                List<District> districts = new List<District>();
                while (sqlDataReader.Read())
                {
                    var district = new District
                        {
                            DistrictName = sqlDataReader["DistrictName"].ToString(),
                            DistrictId = Convert.ToInt32(sqlDataReader["DistrictId"]),
                        };
                    districts.Add(district);
                }

                return districts.AsQueryable();
            }

        }

        public District Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public bool CheckDistrictsTable(List<District> districts, int sportId)
        {
            
            try
            {
                using (
                    SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"])
                    )
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    Logger.Info("Checking Districts database table");

                    SqlCommand sqlCommand = new SqlCommand("Service_GetDistricts", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return false;
                    }

                    return true;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                return false;
            }
        }

        public List<District> CheckDistrictsTableByDistrict(List<District> districts, int sportId)
        {
            try
            {
                List<District> notFoundDistricts = new List<District>();
                using (
                    SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"])
                    )
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    foreach (District district in districts)
                    {
                        SqlCommand sqlCommand = new SqlCommand("[Service_GetDistrictByDistrictId]", sqlConnection)
                            {
                                CommandType = CommandType.StoredProcedure
                            };
                        sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));
                        sqlCommand.Parameters.Add(new SqlParameter("@DistrictId", district.DistrictId));

                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                        if (sqlDataReader.HasRows) continue;

                        district.SportId = sportId;
                        notFoundDistricts.Add(district);
                    }

                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return notFoundDistricts;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                return null;
            }
        }
    }
}
