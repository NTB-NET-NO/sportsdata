﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.SportsData.Service.Components.Interfaces;
using NTB.SportsData.Service.Domain.Classes;
using log4net;

namespace NTB.SportsData.Service.Components.Repositories
{
    class OrganizationDataMapper : IRepository<Organization>, IDisposable, IOrganizationDataMapper 
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(AgeCategoryDataMapper));

        public OrganizationDataMapper()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();

            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        public int InsertOne(Organization domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Organization> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Organization domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Organization domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Organization> GetAll()
        {
            throw new NotImplementedException();
        }

        public Organization Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Organization GetOrgIdBySportId(int sportId)
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }
                    
                    SqlCommand sqlCommand = new SqlCommand("[Service_GetOrganizationBySportId]", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", sportId));

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    var organization = new Organization();
                    if (sqlDataReader.HasRows)
                    {

                        while (sqlDataReader.Read())
                        {
                            organization.OrganizationId = Convert.ToInt32(sqlDataReader["OrgId"]);
                            organization.OrganizationName = sqlDataReader["OrgName"].ToString();
                            organization.SingleSport = Convert.ToInt32(sqlDataReader["SingleSport"]);
                            organization.TeamSport = Convert.ToInt32(sqlDataReader["TeamSport"]);
                            organization.SportName = sqlDataReader["Sport"].ToString();
                            organization.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                        }
                    }

                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }

                    return organization;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return new Organization();
            }
        }
    }
}
