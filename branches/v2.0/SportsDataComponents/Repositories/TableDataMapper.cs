﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.SportsData.Service.Components.Interfaces;
using NTB.SportsData.Service.Domain.Classes;

namespace NTB.SportsData.Service.Components.Repositories
{
    class TableDataMapper : IRepository<DataBaseTable>, IDisposable, ITableDataMapper

    {
        public int InsertOne(DataBaseTable domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<DataBaseTable> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(DataBaseTable domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(DataBaseTable domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<DataBaseTable> GetAll()
        {
            throw new NotImplementedException();
        }

        public DataBaseTable Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public DateTime? GetTableCheck()
        {
            using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
            {
                SqlCommand sqlCommand = new SqlCommand("Service_GetLastTableCheck", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                // Opening the connection
                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                // If we have no rows, then we insert todays datetime
                if (!sqlDataReader.HasRows)
                {
                    // Closing the datareader
                    sqlDataReader.Close();

                    // Closing the connection
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }

                    return null;
                }

                DateTime? checkDate = new DateTime();
                while (sqlDataReader.Read())
                {
                    checkDate = Convert.ToDateTime(sqlDataReader["LastTableCheck"]);
                }

                // Closing the connection
                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return checkDate;
            }
        }

        public void SetTableCheck()
        {
            using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
            {
                SqlCommand sqlCommand = new SqlCommand("Service_InsertLastTableCheck", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                // Opening the connection
                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                // Running the query
                sqlCommand.ExecuteNonQuery();

                // Closing the connection
                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }
    }
}
