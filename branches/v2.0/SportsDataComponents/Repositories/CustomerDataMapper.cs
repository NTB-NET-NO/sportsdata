﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.SportsData.Service.Components.Interfaces;
using NTB.SportsData.Service.Domain.Classes;
using NTB.SportsData.Utilities;
using log4net;

namespace NTB.SportsData.Service.Components.Repositories
{
    class CustomerDataMapper : IRepository<Customer>, IDisposable, ICustomerDataMapper 
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(CustomerDataMapper));

        public CustomerDataMapper()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();

            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        public int InsertOne(Customer domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Customer> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Customer domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Customer domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Customer> GetAll()
        {
            throw new NotImplementedException();
        }

        public Customer Get(int id)
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        sqlConnection.Open();
                    }

                    SqlCommand sqlCommand = new SqlCommand("Service_GetCustomerById", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlCommand.Parameters.Add(new SqlParameter("@CustomerId", id));

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    Customer customer = new Customer();
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            if (sqlDataReader["CustomerId"] != DBNull.Value)
                            {
                                customer.Id = Convert.ToInt32(sqlDataReader["CustomerId"].ToString());
                            }

                            if (sqlDataReader["CustomerName"] != DBNull.Value)
                            {
                                customer.Name = sqlDataReader["CustomerName"].ToString();
                            }

                            if (sqlDataReader["RemoteCustomerId"] != DBNull.Value)
                            {
                                customer.RemoteCustomerId = Convert.ToInt32(sqlDataReader["RemoteCustomerId"].ToString());
                            }
                        }
                    }

                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }

                    return customer;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return null;
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Get list of customers by tournament.
        /// </summary>
        /// <param name="tournament">
        ///     The Tournament object
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Customer> GetListOfCustomersByTournament(Tournament tournament)
        {
            try
            {
                using (
                    SqlConnection sqlConnection = new SqlConnection(
                        ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        sqlConnection.Open();
                    }

                    SqlCommand sqlCommand = new SqlCommand("Service_GetCustomersFromTournamentNumberAndSportId", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", tournament.TournamentId));

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", tournament.SportId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    /**
                     * What we are to do: 
                     * First we get the TournamentId and JobId. We then use this to find
                     * the customers that shall have these data.
                     * We are to return a list of customernames
                     * 
                     */
                    List<Customer> getListOfCustomers = new List<Customer>();
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            Customer customer = new Customer();
                            if (sqlDataReader["CustomerName"] != DBNull.Value)
                            {
                                customer.Name = sqlDataReader["CustomerName"].ToString();
                            }

                            if (sqlDataReader["CustomerId"] != DBNull.Value)
                            {
                                customer.Id = Convert.ToInt32(sqlDataReader["CustomerId"].ToString());
                            }

                            if (sqlDataReader["RemoteCustomerId"] != DBNull.Value)
                            {
                                customer.RemoteCustomerId = Convert.ToInt32(
                                    sqlDataReader["RemoteCustomerId"].ToString());
                            }

                            getListOfCustomers.Add(customer);
                        }

                        // Filter the outcome of the database
                        return getListOfCustomers.Distinct().ToList();
                    }

                    // logger.Debug("No customers needs these data");
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["livetesting"]))
                    {
                        Customer customer = new Customer { Name = "NTB", Id = 0, RemoteCustomerId = 44967 };
                        getListOfCustomers.Add(customer);
                        return getListOfCustomers;
                    }

                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                    {
                        Customer customer = new Customer { Name = "NTB", Id = 0, RemoteCustomerId = 44967 };
                        getListOfCustomers.Add(customer);
                        return getListOfCustomers;
                    }

                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }

                    // Returns the Customer-list
                    return getListOfCustomers;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                Mail.SendException(exception);

                return null;
            }
        }

        public Customer[] GetListOfCustomersByMatchId(int matchId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method shall be renamed to something else. It shall also return a list of customers
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<Customer> GetListOfCustomersByMatch(Match match)
        {
            try
            {
                using (
                    SqlConnection sqlConnection = new SqlConnection(
                        ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        sqlConnection.Open();
                    }

                    SqlCommand sqlCommand = new SqlCommand("Service_GetCustomersFromMatchIdAndSportId", sqlConnection);

                    sqlCommand.Parameters.Add(new SqlParameter("@MatchId", match.MatchId));

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", match.SportId));

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    /**
                     * What we are to do: 
                     * First we get the TournamentId and JobId. We then use this to find
                     * the customers that shall have these data.
                     * We are to return a list of customernames
                     * 
                     */
                    List<Customer> getListOfCustomers = new List<Customer>();
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            Customer customer = new Customer();
                            if (sqlDataReader["CustomerName"] != DBNull.Value)
                            {
                                customer.Name = sqlDataReader["CustomerName"].ToString();
                            }

                            if (sqlDataReader["CustomerId"] != DBNull.Value)
                            {
                                customer.Id = Convert.ToInt32(sqlDataReader["CustomerId"].ToString());
                            }

                            if (sqlDataReader["RemoteCustomerId"] != DBNull.Value)
                            {
                                customer.RemoteCustomerId = Convert.ToInt32(
                                    sqlDataReader["RemoteCustomerId"].ToString());
                            }

                            getListOfCustomers.Add(customer);
                        }

                        // Filter the outcome of the database
                        return getListOfCustomers.Distinct().ToList();
                    }

                    // logger.Debug("No customers needs these data");
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["livetesting"]))
                    {
                        Customer customer = new Customer { Name = "NTB", Id = 0, RemoteCustomerId = 44967 };
                        getListOfCustomers.Add(customer);
                        return getListOfCustomers;
                    }

                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["testing"]))
                    {
                        Customer customer = new Customer { Name = "NTB", Id = 0, RemoteCustomerId = 44967 };
                        getListOfCustomers.Add(customer);
                        return getListOfCustomers;
                    }

                    if (sqlConnection.State != ConnectionState.Closed)
                    {
                        sqlConnection.Close();
                    }

                    // Returns the Customer-list
                    return getListOfCustomers;
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                Mail.SendException(exception);

                return null;
            }
        }
    }
}
