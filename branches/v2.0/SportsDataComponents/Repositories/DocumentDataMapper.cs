﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.SportsData.Service.Components.Interfaces;
using NTB.SportsData.Service.Domain.Classes;
using log4net;

namespace NTB.SportsData.Service.Components.Repositories
{
    internal class DocumentDataMapper : IRepository<Document>, IDisposable, IDocumentDataMapper
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof (DocumentDataMapper));

        public DocumentDataMapper()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();

            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        public int InsertOne(Document domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Document> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Document domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Document domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Document> GetAll()
        {
            throw new NotImplementedException();
        }

        public Document Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public int DeleteDocuments()
        {
            try
            {
                using (
                    SqlConnection sqlConnection = new SqlConnection(
                        ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
                        sqlConnection.Open();
                    }
                    SqlCommand sqlCommand = new SqlCommand("DELETE FROM Document", sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    return sqlDataReader.RecordsAffected;
                }
            }
            catch (SqlException sqlexception)
            {
                Logger.Error(sqlexception);
                return 0;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                return 0;
            }
        }

        public void UpdateDocumentByFilename(Filename filename, int tournamentId)
        {

            using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"])
                )
            {
                var sqlCommand = new SqlCommand("Service_UpdateDocument", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                // Setting the DocumentName
                Logger.Debug("DocumentVersion: " + filename.Version);
                sqlCommand.Parameters.Add(new SqlParameter("@DocumentVersion", filename.Version));

                // Setting the TournamentId
                Logger.Debug("TournamentId: " + tournamentId);
                sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", tournamentId));

                // Setting the DocumentName
                sqlCommand.Parameters.Add(new SqlParameter("@DocumentCreated", DateTime.Now));

                // Open the connection
                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                // Run the query
                sqlCommand.ExecuteNonQuery();

                // Closing connection
                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
            }
        }


        public Filename GetDocumentByTournament(Tournament tournament)
        {
            using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"])
                )
            {
                var filename = new Filename();


                SqlCommand sqlCommand = new SqlCommand("Service_GetDocumentByTournamentId", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                Logger.Debug("GetDocumentByTournamentId: " + tournament.TournamentId);
                sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", tournament.TournamentId));

                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                // We return null in case we don't have a document in the database
                if (!sqlDataReader.HasRows)
                {
                    return null;
                }


                while (sqlDataReader.Read())
                {
                    filename.Version = Convert.ToInt32(sqlDataReader["DocumentVersion"]) + 1;
                    filename.FullName = sqlDataReader["DocumentName"].ToString();
                }
                
                // Closing connection
                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }

                return filename;
            }
        }
    }
}
