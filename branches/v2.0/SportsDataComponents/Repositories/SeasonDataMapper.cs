﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.SportsData.Service.Components.Interfaces;
using NTB.SportsData.Service.Domain.Classes;
using log4net;

namespace NTB.SportsData.Service.Components.Repositories
{
    class SeasonDataMapper : IRepository<Season>, IDisposable, ISeasonDataMapper
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SeasonDataMapper));

        public SeasonDataMapper()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();

            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        public int InsertOne(Season domainobject)
        {
            try
            {
                string connectionString = ConfigurationManager.AppSettings["ConnectionString"];
                SqlConnection sqlConnection = new SqlConnection(connectionString);

                // Open the connection to the database
                sqlConnection.Open();

                SqlCommand sqlCommand = new SqlCommand("Service_InsertSeasons", sqlConnection)
                {
                    CommandType = CommandType.StoredProcedure
                };

                sqlCommand.Parameters.Add(new SqlParameter("@SeasonName", domainobject.SeasonName));

                sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", domainobject.SeasonId));

                sqlCommand.Parameters.Add(new SqlParameter("@SeasonActive", domainobject.SeasonActive));

                sqlCommand.Parameters.Add(
                    new SqlParameter("@SeasonStartDate", domainobject.SeasonStartDate));

                sqlCommand.Parameters.Add(
                    new SqlParameter("@SeasonEndDate", domainobject.SeasonEndDate));

                sqlCommand.Parameters.Add(
                    new SqlParameter("@SeasonDistributed", DBNull.Value));

                // We must populate the database
                sqlCommand.ExecuteNonQuery();

                sqlConnection.Close();

                return 0;
            }
            catch (SqlException exception)
            {
                Logger.Error(exception);
                return 0;
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                return 0;
            }
        }

        public void InsertAll(List<Season> domainobject)
        {
            foreach (Season season in domainobject)
            {
                InsertOne(season);
            }
        }

        public void Update(Season domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Season domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Season> GetAll()
        {
            throw new NotImplementedException();
        }

        public Season Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<Season> CheckSeasons(List<Season> seasons)
        {
            try
            {
                List<Season> notFoundSeasons = new List<Season>();
                using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }

                    // string SQLQuery = "SELECT SeasonID FROM Seasons";
                    SqlCommand sqlCommand = new SqlCommand("Service_GetSeasons", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return seasons;
                    }
                    sqlDataReader.Close();
                    
                    sqlCommand.Dispose();

                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }

                    foreach (Season season in seasons)
                    {
                        sqlCommand = new SqlCommand("Service_GetSeasons", sqlConnection)
                            {
                                CommandType = CommandType.StoredProcedure,
                            };

                        sqlCommand.Parameters.Add(new SqlParameter("@SeasonId", season.SeasonId));

                        SqlDataReader dataReader = sqlCommand.ExecuteReader();

                        if (!dataReader.HasRows)
                        {
                            notFoundSeasons.Add(season);
                        }

                        sqlCommand.Dispose();

                        dataReader.Close();
                    }
                }

                return notFoundSeasons;
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
                return null;
            }
        }
    }
}
