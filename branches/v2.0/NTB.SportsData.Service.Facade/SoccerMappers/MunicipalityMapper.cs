﻿using Glue;
using NTB.SportsData.Service.Common;
using NTB.SportsData.Service.Services.NFF.NFFProdService;

namespace NTB.SportsData.Service.Facade.SoccerMappers
{
    public class MunicipalityMapper : BaseMapper<Municipality, Domain.Classes.Municipality>
    {
        protected override void SetUpMapper(Mapping<Municipality, Domain.Classes.Municipality> mapper)
        {
            mapper.Relate(x=>x.Id, y=>y.MunicipalityId);
            mapper.Relate(x => x.Name, y => y.MunicipalityName);
        }
    }
}
