﻿using Glue;
using NTB.SportsData.Service.Common;
using NTB.SportsData.Service.Services.NFF.NFFProdService;

namespace NTB.SportsData.Service.Facade.SoccerMappers
{
    public class DistrictMapper : BaseMapper<District, Domain.Classes.District>
    {
        protected override void SetUpMapper(Mapping<District, Domain.Classes.District> mapper)
        {
            mapper.Relate(x => x.DistrictId, y => y.DistrictId);
            mapper.Relate(x => x.DistrictName, y=>y.DistrictName);
        }
    }
}
