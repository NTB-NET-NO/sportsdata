﻿using Glue;
using NTB.SportsData.Service.Common;
using NTB.SportsData.Service.Services.NFF.NFFProdService;

namespace NTB.SportsData.Service.Facade.SoccerMappers
{
    public class TournamentTableTeamMapper : BaseMapper<TournamentTableTeam, Domain.Classes.TournamentTableTeam>
    {
        protected override void SetUpMapper(Mapping<TournamentTableTeam, Domain.Classes.TournamentTableTeam> mapper)
        {
            mapper.Relate(x=>x.Adjustments, y=>y.Adjustments);
            mapper.Relate(x => x.AwayGoalDifference, y => y.AwayGoalDifference);
            mapper.Relate(x => x.AwayGoalsAgainst, y => y.AwayGoalsAgainst);
            mapper.Relate(x => x.AwayGoalsScored, y => y.AwayGoalsScored);
            mapper.Relate(x => x.AwayMatchesDraw, y => y.AwayMatchesDraw);
            mapper.Relate(x => x.AwayMatchesLost, y => y.AwayMatchesLost);
            mapper.Relate(x => x.AwayMatchesPlayed, y => y.AwayMatchesPlayed);
            mapper.Relate(x => x.AwayMatchesWon, y => y.AwayMatchesWon);
            mapper.Relate(x => x.AwayPoints, y => y.AwayPoints);
            mapper.Relate(x => x.HomeGoalDifference, y => y.HomeGoalDifference);
            mapper.Relate(x => x.HomeGoalsAgainst, y => y.HomeGoalsAgainst);
            mapper.Relate(x => x.HomeGoalsScored, y => y.HomeGoalsScored);
            mapper.Relate(x => x.HomeMatchesDraw, y => y.HomeMatchesDraw);
            mapper.Relate(x => x.HomeMatchesLost, y => y.HomeMatchesLost);
            mapper.Relate(x => x.HomeMatchesPlayed, y => y.HomeMatchesPlayed);
            mapper.Relate(x => x.HomeMatchesWon, y => y.HomeMatchesWon);
            mapper.Relate(x => x.HomePoints, y => y.HomePoints);
            mapper.Relate(x => x.TablePosition, y => y.TablePosition);
            mapper.Relate(x => x.TeamId, y => y.TeamId);
            mapper.Relate(x => x.TeamName, y => y.TeamName);
            mapper.Relate(x => x.TeamNameInTournament, y => y.TeamNameInTournament);
            mapper.Relate(x => x.TotalGoalDifference, y => y.TotalGoalDifference);
            mapper.Relate(x => x.TotalGoalsAgainst, y => y.TotalGoalsAgainst);
            mapper.Relate(x => x.TotalGoalsScored, y => y.TotalGoalsScored);
            mapper.Relate(x => x.TotalMatchesDraw, y => y.TotalMatchesDraw);
            mapper.Relate(x => x.TotalMatchesLost, y => y.TotalMatchesLost);
            mapper.Relate(x => x.TotalMatchesPlayed, y => y.TotalMatchesPlayed);
            mapper.Relate(x => x.TotalMatchesWon, y => y.TotalMatchesWon);
            mapper.Relate(x => x.TotalPoints, y => y.TotalPoints);
            mapper.Relate(x => x.TournamentId, y => y.TournamentId);
            mapper.Relate(x => x.TournamentTeamId, y => y.TournamentTeamId);
        }
    }
}
