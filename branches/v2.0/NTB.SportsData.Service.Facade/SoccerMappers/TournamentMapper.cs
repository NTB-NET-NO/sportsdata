﻿using Glue;
using NTB.SportsData.Service.Common;
using NTB.SportsData.Service.Services.NFF.NFFProdService;

namespace NTB.SportsData.Service.Facade.SoccerMappers
{
    public class TournamentMapper : BaseMapper<Tournament, Domain.Classes.Tournament>
    {
        protected override void SetUpMapper(Mapping<Tournament, Domain.Classes.Tournament> mapper)
        {
            mapper.Relate(x => x.AgeCategoryId, y=>y.AgeCategoryId);
            mapper.Relate(x => x.DistrictId, y => y.DistrictId);
            mapper.Relate(x => x.Division, y => y.Division);
            mapper.Relate(x => x.GenderId, y => y.GenderId);
            mapper.Relate(x => x.NationalTeamTournament, y => y.NationalTeamTournament);
            mapper.Relate(x => x.PushChanges, y => y.PushChanges);
            mapper.Relate(x => x.SeasonId, y => y.SeasonId);
            mapper.Relate(x => x.TournamentId, y => y.TournamentId);
            mapper.Relate(x => x.TournamentName, y => y.TournamentName);
            mapper.Relate(x => x.TournamentNumber, y => y.TournamentNumber);
            mapper.Relate(x => x.TournamentTypeId, y => y.TournamentTypeId);
            mapper.Relate(x => x.StartDateFirstTournamentRound, y => y.StartDateFirstTournamentRound);
            mapper.Relate(x => x.PublishTournamentTable, y=>y.PublishTournamentTable);

        }
    }
}
