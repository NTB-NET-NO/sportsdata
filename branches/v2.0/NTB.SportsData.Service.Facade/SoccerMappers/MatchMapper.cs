﻿using Glue;
using NTB.SportsData.Service.Services.NFF.NFFProdService;
using NTB.SportsData.Service.Common;

namespace NTB.SportsData.Service.Facade.SoccerMappers
{
    public class MatchMapper : BaseMapper<Match, Domain.Classes.Match>
    {
        protected override void SetUpMapper(Mapping<Match, Domain.Classes.Match> mapper)
        {
            mapper.Relate(x => x.AwayTeamName, y => y.AwayTeamName);
            mapper.Relate(x => x.MatchStartDate, y => y.MatchStartDate);
            mapper.Relate(x => x.HomeTeamName, y => y.HomeTeamName);
            mapper.Relate(x => x.MatchId, y => y.MatchId);
            mapper.Relate(x => x.TournamentId, y => y.TournamentId);
            mapper.Relate(x => x.TournamentName, y => y.TournamentName);
            mapper.Relate(x => x.TournamentAgeCategoryId, y => y.TournamentAgeCategoryId);
        }
    }
}
