﻿using Glue;
using NTB.SportsData.Service.Common;
using NTB.SportsData.Service.Domain.Classes;
using NTB.SportsData.Service.Services.NIF.NIFProdServices;

namespace NTB.SportsData.Service.Facade.SoccerMappers
{
    class EventCompleteMapper : BaseMapper<EventComplete, Domain.Classes.SportEventComplete>
    {
        protected override void SetUpMapper(Mapping<EventComplete, SportEventComplete> mapper)
        {
            mapper.Relate(x => x.Classes, y=>y.Classes);
            mapper.Relate(x => x.Entries, y => y.Entries);
            mapper.Relate(x => x.Event, y => y.Event);
        }
    }
}
