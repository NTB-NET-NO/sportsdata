﻿using Glue;
using NTB.SportsData.Service.Common;
using NTB.SportsData.Service.Services.NFF.NFFProdService;

namespace NTB.SportsData.Service.Facade.SportMappers
{
    public class SeasonMapper : BaseMapper<Season, Domain.Classes.Season>
    {
        protected override void SetUpMapper(Mapping<Season, Domain.Classes.Season> mapper)
        {
            mapper.Relate(x => x.SeasonId, y => y.SeasonId);
            mapper.Relate(x => x.SeasonNameLong, y => y.SeasonName);
            mapper.Relate(x => x.SeasonStartDate, y => y.SeasonStartDate);
            mapper.Relate(x => x.SeasonEndDate, y => y.SeasonEndDate);
            mapper.Relate(x => x.Ongoing, y => y.SeasonActive);
        }
    }
}
