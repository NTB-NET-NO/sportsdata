﻿using Glue;
using NTB.SportsData.Service.Common;
using NTB.SportsData.Service.Services.NIF.NIFProdServices;

namespace NTB.SportsData.Service.Facade.SportMappers
{
    public class TournamentMapper : BaseMapper<Tournament, Domain.Classes.Tournament>
    {
        protected override void SetUpMapper(Mapping<Tournament, Domain.Classes.Tournament> mapper)
        {
            mapper.Relate(x => x.ClassCodeId, y=>y.AgeCategoryId);
            mapper.Relate(x => x.Division, y => y.Division);
            mapper.Relate(x => x.SeasonId, y => y.SeasonId);
            mapper.Relate(x => x.TournamentId, y => y.TournamentId);
            mapper.Relate(x => x.TournamentName, y => y.TournamentName);
            mapper.Relate(x => x.FromDate, y => y.StartDateFirstTournamentRound);
        }
    }
}
