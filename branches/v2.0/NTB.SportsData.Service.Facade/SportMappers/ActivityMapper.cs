﻿using Glue;
using NTB.SportsData.Service.Common;
using NTB.SportsData.Service.Services.NIF.NIFProdServices;

namespace NTB.SportsData.Service.Facade.SportMappers
{
    public class ActivityMapper : BaseMapper<Activity, Domain.Classes.Activity>
    {
        protected override void SetUpMapper(Mapping<Activity, Domain.Classes.Activity> mapper)
        {
            
        }
    }
}
