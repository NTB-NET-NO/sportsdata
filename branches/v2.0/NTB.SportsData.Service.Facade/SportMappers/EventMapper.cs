﻿using Glue;
using NTB.SportsData.Service.Common;
using NTB.SportsData.Service.Domain.Classes;
using NTB.SportsData.Service.Services.NIF.NIFProdServices;

namespace NTB.SportsData.Service.Facade.SportMappers
{
    public class EventMapper : BaseMapper<Event, SportEvent>
    {
        protected override void SetUpMapper(Mapping<Event, SportEvent> mapper)
        {
            mapper.Relate(x => x.ActivityId, y=>y.ActivityId);
            mapper.Relate(x => x.ActivityName, y => y.ActivityName);
            mapper.Relate(x => x.AdministrativeOrgId, y => y.AdministrativeOrgId);
            mapper.Relate(x => x.AdministrativeOrgName, y => y.AdministrativeOrgName);
            mapper.Relate(x => x.EventId, y => y.EventId);
            mapper.Relate(x => x.EventName, y => y.EventName);
            mapper.Relate(x => x.EventTypeName, y => y.EventTypeName);
            mapper.Relate(x => x.EventTypeId, y => y.EventTypeId);
            mapper.Relate(x => x.Location, y => y.EventLocation);
            mapper.Relate(x => x.ArrangingOrgId, y => y.EventOrganizerId);
            mapper.Relate(x => x.ArrangingOrgName, y => y.EventOrganizerName);
            mapper.Relate(x => x.StartDate, y => y.EventDateStart);
            mapper.Relate(x => x.EndDate, y => y.EventDateEnd);
        }
    }
}
