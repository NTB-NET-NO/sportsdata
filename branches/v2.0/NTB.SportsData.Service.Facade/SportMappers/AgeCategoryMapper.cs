﻿using Glue;
using NTB.SportsData.Service.Common;
using NTB.SportsData.Service.Services.NFF.NFFProdService;

namespace NTB.SportsData.Service.Facade.SportMappers
{
    public class AgeCategoryMapper : BaseMapper<AgeCategoryTournament, Domain.Classes.AgeCategory>
    {
        protected override void SetUpMapper(Mapping<AgeCategoryTournament, Domain.Classes.AgeCategory> mapper)
        {
            mapper.Relate(x => x.AgeCategoryId, y => y.AgeCategoryId);
            mapper.Relate(x => x.AgeCategoryName, y => y.AgeCategoryName);
            mapper.Relate(x => x.MaxAge, y => y.MaxAge);
            mapper.Relate(x => x.MinAge, y => y.MinAge);
        }
    }
}
