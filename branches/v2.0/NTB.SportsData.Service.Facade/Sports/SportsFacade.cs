﻿using System;
using System.Collections.Generic;
using System.Linq;
using NTB.SportsData.Service.Domain.Classes;
using NTB.SportsData.Service.Facade.Interfaces;
using NTB.SportsData.Service.Facade.SportMappers;
using NTB.SportsData.Service.Services.NIF.Interfaces;
using NTB.SportsData.Service.Services.NIF.Repositories;

namespace NTB.SportsData.Service.Facade.Sports
{
    public class SportsFacade : IFacade
    {
        private readonly IActivityDataMapper _activityDataMapper;
        private readonly IEventDataMapper _eventDataMapper;
        private readonly ITournamentDataMapper _tournamentDataMapper;

        public SportsFacade()
        {
            _activityDataMapper = new ActivityRepository();
            _eventDataMapper = new EventRepository();
            _tournamentDataMapper = new TournamentRepository();
        }
        public Tournament GetTournamentByTournamentId(int tournamentId)
        {
            throw new NotImplementedException();
        }

        public Tournament GetTournament(int tournamentId)
        {
            throw new NotImplementedException();
        }

        public List<Tournament> GetTournamentsBySeasonId(int seasonId)
        {
            throw new NotImplementedException();
        }

        public List<Tournament> GetTournamentsByDistrict(int districtId, int seasonId)
        {
            var mapper = new TournamentMapper();

            var result = _tournamentDataMapper.GetTournamentByDistrict(districtId, seasonId);

            return result.Select(row => mapper.Map(row, new Tournament())).ToList();
        }

        public List<TournamentTableTeam> GetTournamentStanding(int tournamentId)
        {
            throw new NotImplementedException();
        }

        public List<Match> GetMatchesByTournament(int tournamentId, bool includeSquad, bool includeReferees, bool includeResults,
                                           bool includeEvents, DateTime? dateTime)
        {
            throw new NotImplementedException();
        }

        public Match GetMatchByMatchId(int matchId)
        {
            throw new NotImplementedException();
        }

        public List<Match> GetMatchesByDateInterval(DateTime dateStart, DateTime dateEnd, int districtId, bool includeSquad,
                                             bool includeReferees, bool includeResults, bool includeEvents)
        {
            throw new NotImplementedException();
        }

        public List<Match> GetMatchesByDateInterval(DateTime dateStart, DateTime dateEnd, int districtId, int includeSquad,
                                             int includeReferees, int includeResults, int includeEvents)
        {
            throw new NotImplementedException();
        }

        public List<Match> GetMatchesByDistrict(int districtId, DateTime matchStartDate, DateTime matchEndDate, bool includeSquad,
                                         bool includeReferees, bool includeResults, bool includeEvents, DateTime? dateTime)
        {
            throw new NotImplementedException();
        }

        public List<AgeCategory> GetAgeCategories()
        {
            throw new NotImplementedException();
        }

        public List<Season> GetSeasons()
        {
            throw new NotImplementedException();
        }

        public List<District> GetDistricts()
        {
            throw new NotImplementedException();
        }

        public List<Municipality> GetMunicipalitiesByDistrict(int districtId)
        {
            throw new NotImplementedException();
        }

        public Activity GetActivityByOrgId(int orgId, int sportId)
        {
            var mapper = new ActivityMapper();

            var result = _activityDataMapper.GetActivityByOrgId(orgId, sportId);

            return mapper.Map(result, new Activity());
        }

        public List<SportEvent> GetEventsBySportId(int sportId)
        {
            var mapper = new EventMapper();
            var result = _eventDataMapper.GetEventsBySportId(sportId);

            return result.Select(row => mapper.Map(row, new SportEvent())).ToList();
        }

        public SportEvent GetEventByEventId(int eventId)
        {
            var mapper = new EventMapper();

            var result = _eventDataMapper.GetEventByEventId(eventId);

            return mapper.Map(result, new SportEvent());
        }

        public SportEventComplete GetSingleEventComplete(int eventId)
        {
            var mapper = new EventCompleteMapper();

            var result = _eventDataMapper.GetSingleEventComplete(eventId);

            return mapper.Map(result, new SportEventComplete());
        }

        public List<Activity> GetAllFirstLevelActivities(int parentActivityId)
        {
            var mapper = new ActivityMapper();

            var result = _activityDataMapper.GetAllFirstLevelActivities(parentActivityId);

            return result.Select(row => mapper.Map(row, new Activity())).ToList();
        }
    }
}
