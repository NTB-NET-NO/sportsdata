﻿using System;
using System.Collections.Generic;
using System.Linq;
using NTB.SportsData.Service.Domain.Classes;
using NTB.SportsData.Service.Facade.Interfaces;
using NTB.SportsData.Service.Facade.SoccerMappers;
using NTB.SportsData.Service.Services.NFF.Interfaces;
using NTB.SportsData.Service.Services.NFF.Repositories;

namespace NTB.SportsData.Service.Facade.Soccer
{
    public class SoccerFacade : IFacade
    {
        private readonly IMatchDataMapper _matchDataMapper;
        private readonly IAgeCategoryDataMapper _ageCategoryDataMapper;
        private readonly ISeasonDataMapper _seasonDataMapper ;
        private readonly IDistrictDataMapper _districtDataMapper;
        private readonly ITournamentDataMapper _tournamentDataMapper;
        private readonly IMunicipalityDataMapper _municipalityDataMapper;

        public SoccerFacade()
        {
            _matchDataMapper = new MatchRepository();
            _ageCategoryDataMapper = new AgeCategoryRepository();
            _seasonDataMapper = new SeasonRepository();
            _districtDataMapper = new DistrictRepository();
            _tournamentDataMapper = new TournamentRepository();
            _municipalityDataMapper = new MunicipalityRepository();

        }
        public Tournament GetTournament(int tournamentId)
        {
            var result = _tournamentDataMapper.GetTournament(tournamentId);

            var mapper = new TournamentMapper();

            return mapper.Map(result, new Tournament());
        }

        public List<Tournament> GetTournamentsBySeasonId(int seasonId)
        {
            throw new NotImplementedException();
        }

        public List<Tournament> GetTournamentsByDistrict(int districtId, int seasonId)
        {
            var result = _tournamentDataMapper.GetTournamentsByDistrict(districtId, seasonId);

            var mapper = new TournamentMapper();

            return result.Select(row => mapper.Map(row, new Tournament())).ToList();
        }

        public List<TournamentTableTeam> GetTournamentStanding(int tournamentId)
        {
            var result = _tournamentDataMapper.GetTournamentStanding(tournamentId);

            var mapper = new TournamentTableTeamMapper();

            return result.Select(row => mapper.Map(row, new TournamentTableTeam())).ToList();
        }

        public List<Match> GetMatchesByTournament(int tournamentId, bool includeSquad, bool includeReferees, bool includeResults,
                                           bool includeEvents, DateTime? todayDateTime)
        {
            var result =
                _matchDataMapper.GetMatchesByTournament(tournamentId, includeSquad, includeReferees, includeResults,
                                                        includeEvents, todayDateTime);

            var mapper = new MatchMapper();

            return result.Select(row => mapper.Map(row, new Match())).ToList();
        }

        public Match GetMatchByMatchId(int matchId)
        {
            var result = _matchDataMapper.GetMatch(matchId);

            var mapper = new MatchMapper();

            return mapper.Map(result, new Match());
        }

        public List<Match> GetMatchesByDateInterval(DateTime dateStart, DateTime dateEnd, int districtId, bool includeSquad,
                                             bool includeReferees, bool includeResults, bool includeEvents)
        {
            var result = _matchDataMapper.GetMatchesByDateInterval(dateStart, dateEnd, districtId, includeSquad,
                                                                   includeReferees, includeResults, includeEvents);

            var mapper = new MatchMapper();

            return result.Select(row => mapper.Map(row, new Match())).ToList();
        }

        public List<Match> GetMatchesByDistrict(int districtId, DateTime matchStartDate, DateTime matchEndDate, bool includeSquad,
                                         bool includeReferees, bool includeResults, bool includeEvents, DateTime? todayDateTime)
        {
            var result = _matchDataMapper.GetMatchesByDistrict(districtId, matchStartDate, matchEndDate, includeSquad,
                                                               includeReferees, includeResults, includeEvents, todayDateTime);

            var mapper = new MatchMapper();

            return result.Select(row => mapper.Map(row, new Match())).ToList();
        }

        public List<AgeCategory> GetAgeCategories()
        {
            var mapper = new AgeCategoryMapper();

            var result = _ageCategoryDataMapper.GetAgeCategoriesTournament();

            return result.Select(row => mapper.Map(row, new AgeCategory())).ToList();
        }

        public List<Season> GetSeasons()
        {
            var mapper = new SeasonMapper();

            var result = _seasonDataMapper.GetSeasons();

            return result.Select(row => mapper.Map(row, new Season())).ToList();
        }

        public List<District> GetDistricts()
        {
            var mapper = new DistrictMapper();

            var result = _districtDataMapper.GetDistricts();

            return result.Select(row => mapper.Map(row, new District())).ToList();
        }

        public List<Municipality> GetMunicipalitiesByDistrict(int districtId)
        {
            var mapper = new MunicipalityMapper();

            var result = _municipalityDataMapper.GetMunicipalitiesByDistrict(districtId);

            return result.Select(row => mapper.Map(row, new Municipality())).ToList();
        }

        public Activity GetActivityByOrgId(int orgId, int sportId)
        {
            throw new NotImplementedException();
        }

        public List<SportEvent> GetEventsBySportId(int sportId)
        {
            throw new NotImplementedException();
        }

        public SportEvent GetEventByEventId(int eventId)
        {
            throw new NotImplementedException();
        }

        SportEventComplete IFacade.GetSingleEventComplete(int eventId)
        {
            throw new NotImplementedException();
        }

        public List<Activity> GetAllFirstLevelActivities(int parentActivityId)
        {
            throw new NotImplementedException();
        }

        public SportEvent GetSingleEventComplete(int eventId)
        {
            throw new NotImplementedException();
        }
    }
}
