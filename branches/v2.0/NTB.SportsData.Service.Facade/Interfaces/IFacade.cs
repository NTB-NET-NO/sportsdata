﻿using System;
using System.Collections.Generic;
using NTB.SportsData.Service.Domain.Classes;

namespace NTB.SportsData.Service.Facade.Interfaces
{
    /// <summary>
    ///     This is the facade to be used by both NIF and NFF facades
    /// </summary>
    public interface IFacade
    {
        Tournament GetTournament(int tournamentId);
        List<Tournament> GetTournamentsBySeasonId(int seasonId);

        List<Tournament> GetTournamentsByDistrict(int districtId, int seasonId);
        List<TournamentTableTeam> GetTournamentStanding(int tournamentId);

        List<Match> GetMatchesByTournament(int tournamentId, bool includeSquad, bool includeReferees, bool includeResults,
                                              bool includeEvents, DateTime? todayDateTime);
        Match GetMatchByMatchId(int matchId);
        List<Match> GetMatchesByDateInterval(DateTime dateStart, DateTime dateEnd, int districtId, bool includeSquad, bool includeReferees, bool includeResults, bool includeEvents);
            
        List<Match> GetMatchesByDistrict(int districtId, DateTime matchStartDate, DateTime matchEndDate,
                                        bool includeSquad, bool includeReferees, bool includeResults,
                                        bool includeEvents, DateTime? todayDateTime);
        List<AgeCategory> GetAgeCategories();
        List<Season> GetSeasons();
        List<District> GetDistricts();

        List<Municipality> GetMunicipalitiesByDistrict(int districtId);

        // Some NIF Specific methods
        Activity GetActivityByOrgId(int orgId, int sportId);
        List<SportEvent> GetEventsBySportId(int sportId);
        SportEvent GetEventByEventId(int eventId);
        SportEventComplete GetSingleEventComplete(int eventId);
        List<Activity> GetAllFirstLevelActivities(int parentActivityId);
    }
}
