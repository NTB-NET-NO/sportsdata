// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataBaseTable.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   The data base table.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Domain.Classes
{
    using System;

    /// <summary>
    /// The data base table.
    /// </summary>
    public class DataBaseTable
    {
        /// <summary>
        /// Gets or sets the data base table updated.
        /// </summary>
        public DateTime DataBaseTableUpdated { get; set; }
    }
}