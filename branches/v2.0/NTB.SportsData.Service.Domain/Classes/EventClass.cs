﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventClass.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   The event class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Domain.Classes
{
    using System;
    using System.Collections.Generic;

    using NTB.SportsData.Service.Domain.Enums;

    /// <summary>
    /// The event class.
    /// </summary>
    public class EventClass
    {
        /// <summary>
        /// Gets or sets the event id.
        /// </summary>
        public int EventId { get; set; }

        /// <summary>
        /// Gets or sets the allowed from age.
        /// </summary>
        public int AllowedFromAge { get; set; }

        /// <summary>
        /// Gets or sets the allowed to age.
        /// </summary>
        public int AllowedToAge { get; set; }

        /// <summary>
        /// Gets or sets the date limit.
        /// </summary>
        public DateTime DateLimit { get; set; }

        /// <summary>
        /// Gets or sets the exercises.
        /// </summary>
        public List<EventExercise> Exercises { get; set; }

        /// <summary>
        /// Gets or sets the from age.
        /// </summary>
        public int FromAge { get; set; }

        /// <summary>
        /// Gets or sets the to age.
        /// </summary>
        public int ToAge { get; set; }

        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        public Gender Gender { get; set; }

        /// <summary>
        /// Gets or sets the org id.
        /// </summary>
        public int OrgId { get; set; }
    }
}