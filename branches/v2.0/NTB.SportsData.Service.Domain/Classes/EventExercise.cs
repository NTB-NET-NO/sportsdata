﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EventExercise.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   The event exercise.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Domain.Classes
{
    using System;

    /// <summary>
    /// The event exercise.
    /// </summary>
    public class EventExercise
    {
        /// <summary>
        /// Gets or sets the event exercise id.
        /// </summary>
        public int EventExerciseId { get; set; }

        /// <summary>
        /// Gets or sets the event exercise name.
        /// </summary>
        public string EventExerciseName { get; set; }

        /// <summary>
        /// Gets or sets the entry id.
        /// </summary>
        public int EntryId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is relay.
        /// </summary>
        public bool IsRelay { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether selected.
        /// </summary>
        public bool Selected { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the start time.
        /// </summary>
        public int StartTime { get; set; }
    }
}