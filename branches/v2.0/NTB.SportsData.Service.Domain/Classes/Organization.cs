﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Organization.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   The organization.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Domain.Classes
{
    /// <summary>
    /// The organization.
    /// </summary>
    public class Organization
    {
        /// <summary>
        /// Gets or sets the organization id.
        /// </summary>
        public int OrganizationId { get; set; }

        /// <summary>
        /// Gets or sets the organization name.
        /// </summary>
        public string OrganizationName { get; set; }

        /// <summary>
        /// Gets or sets the single sport.
        /// </summary>
        public int SingleSport { get; set; }

        /// <summary>
        /// Gets or sets the team sport.
        /// </summary>
        public int TeamSport { get; set; }

        /// <summary>
        /// Gets or sets the sport name.
        /// </summary>
        public string SportName { get; set; }

        /// <summary>
        /// Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        /// Gets or sets the organization name short.
        /// </summary>
        public string OrganizationNameShort { get; set; }
    }
}