﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Activity.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   The activity.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Domain.Classes
{
    /// <summary>
    /// The activity.
    /// </summary>
    public class Activity
    {
        /// <summary>
        /// Gets or sets the activity id.
        /// </summary>
        public int ActivityId { get; set; }

        /// <summary>
        /// Gets or sets the activity name.
        /// </summary>
        public string ActivityName { get; set; }
    }
}