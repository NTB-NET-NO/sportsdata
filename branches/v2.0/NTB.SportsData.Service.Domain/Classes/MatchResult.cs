// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchResult.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   The match result.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Domain.Classes
{
    /// <summary>
    /// The match result.
    /// </summary>
    public class MatchResult
    {
        /// <summary>
        /// Gets or sets the match result id.
        /// </summary>
        public int MatchResultId { get; set; }

        /// <summary>
        /// Gets or sets the result type name.
        /// </summary>
        public string ResultTypeName { get; set; }

        /// <summary>
        /// Gets or sets the home team goals.
        /// </summary>
        public int? HomeTeamGoals { get; set; }

        /// <summary>
        /// Gets or sets the away team goals.
        /// </summary>
        public int? AwayTeamGoals { get; set; }
    }
}