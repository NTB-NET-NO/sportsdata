// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Player.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   The player.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Domain.Classes
{
    /// <summary>
    /// The player.
    /// </summary>
    public class Player
    {
        /// <summary>
        /// Gets or sets the player id.
        /// </summary>
        public int? PlayerId { get; set; }

        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        public string Position { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the sur name.
        /// </summary>
        public string SurName { get; set; }
    }
}