// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Tournament.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   The tournament.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Domain.Classes
{
    using System;

    /// <summary>
    /// The tournament.
    /// </summary>
    public class Tournament
    {
        /// <summary>
        /// Gets or sets the age category id.
        /// </summary>
        public int AgeCategoryId { get; set; }

        /// <summary>
        /// Gets or sets the district id.
        /// </summary>
        public int DistrictId { get; set; }

        /// <summary>
        /// Gets or sets the gender id.
        /// </summary>
        public int GenderId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether push changes.
        /// </summary>
        public bool PushChanges { get; set; }

        /// <summary>
        /// Gets or sets the tournament id.
        /// </summary>
        public int TournamentId { get; set; }

        /// <summary>
        /// Gets or sets the tournament number.
        /// </summary>
        public string TournamentNumber { get; set; }

        /// <summary>
        /// Gets or sets the tournament name.
        /// </summary>
        public string TournamentName { get; set; }

        /// <summary>
        /// Gets or sets the tournament type id.
        /// </summary>
        public int TournamentTypeId { get; set; }

        /// <summary>
        /// Gets or sets the season id.
        /// </summary>
        public int SeasonId { get; set; }

        /// <summary>
        /// Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        /// Gets or sets the division.
        /// </summary>
        public int Division { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether national team tournament.
        /// </summary>
        public bool NationalTeamTournament { get; set; }

        /// <summary>
        /// Gets or sets the start date first tournament round.
        /// </summary>
        public DateTime StartDateFirstTournamentRound { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether publish tournament table.
        /// </summary>
        public bool PublishTournamentTable { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether publish result.
        /// </summary>
        public bool PublishResult { get; set; }
    }
}