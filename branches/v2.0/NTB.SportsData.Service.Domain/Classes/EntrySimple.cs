﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EntrySimple.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   The entry simple.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Domain.Classes
{
    using System;

    /// <summary>
    /// The entry simple.
    /// </summary>
    public class EntrySimple
    {
        /// <summary>
        /// Gets or sets the entry id.
        /// </summary>
        public int EntryId { get; set; }

        /// <summary>
        /// Gets or sets the birth date.
        /// </summary>
        public DateTime BirthDate { get; set; }

        /// <summary>
        /// Gets or sets the chip number.
        /// </summary>
        public int? ChipNumber { get; set; }

        /// <summary>
        /// Gets or sets the class exercise id.
        /// </summary>
        public int ClassExerciseId { get; set; }

        /// <summary>
        /// Gets or sets the class id.
        /// </summary>
        public int ClassId { get; set; }

        /// <summary>
        /// Gets or sets the class name.
        /// </summary>
        public string ClassName { get; set; }

        /// <summary>
        /// Gets or sets the competitor id.
        /// </summary>
        public int CompetitorId { get; set; }

        /// <summary>
        /// Gets or sets the event id.
        /// </summary>
        public int EventId { get; set; }

        /// <summary>
        /// Gets or sets the event name.
        /// </summary>
        public string EventName { get; set; }

        /// <summary>
        /// Gets or sets the exercise id.
        /// </summary>
        public int ExerciseId { get; set; }

        /// <summary>
        /// Gets or sets the exercise name.
        /// </summary>
        public string ExerciseName { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the remote code.
        /// </summary>
        public int? RemoteCode { get; set; }

        /// <summary>
        /// Gets or sets the nationality.
        /// </summary>
        public string Nationality { get; set; }

        /// <summary>
        /// Gets or sets the org id.
        /// </summary>
        public int OrgId { get; set; }

        /// <summary>
        /// Gets or sets the org name.
        /// </summary>
        public string OrgName { get; set; }

        /// <summary>
        /// Gets or sets the person id.
        /// </summary>
        public int PersonId { get; set; }

        /// <summary>
        /// Gets or sets the person org id.
        /// </summary>
        public int? PersonOrgId { get; set; }

        /// <summary>
        /// Gets or sets the person org name.
        /// </summary>
        public string PersonOrgName { get; set; }

        /// <summary>
        /// Gets or sets the place.
        /// </summary>
        public string Place { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the team id.
        /// </summary>
        public int? TeamId { get; set; }
    }
}