// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchEvent.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   The match event.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Domain.Classes
{
    /// <summary>
    /// The match event.
    /// </summary>
    public class MatchEvent
    {
        /// <summary>
        /// Gets or sets the match event id.
        /// </summary>
        public int MatchEventId { get; set; }

        /// <summary>
        /// Gets or sets the match event type.
        /// </summary>
        public string MatchEventType { get; set; }

        /// <summary>
        /// Gets or sets the team id.
        /// </summary>
        public int TeamId { get; set; }

        /// <summary>
        /// Gets or sets the connected to event.
        /// </summary>
        public string ConnectedToEvent { get; set; }

        /// <summary>
        /// Gets or sets the minute.
        /// </summary>
        public int Minute { get; set; }

        /// <summary>
        /// Gets or sets the home goals.
        /// </summary>
        public int? HomeGoals { get; set; }

        /// <summary>
        /// Gets or sets the away goals.
        /// </summary>
        public int? AwayGoals { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether second yellow card.
        /// </summary>
        public bool SecondYellowCard { get; set; }

        /// <summary>
        /// Gets or sets the player id.
        /// </summary>
        public int PlayerId { get; set; }

        /// <summary>
        /// Gets or sets the player name.
        /// </summary>
        public string PlayerName { get; set; }

        /// <summary>
        /// Gets or sets the comment.
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Gets or sets the extension data.
        /// </summary>
        public string ExtensionData { get; set; }

        /// <summary>
        /// Gets or sets the match event type id.
        /// </summary>
        public int MatchEventTypeId { get; set; }

        /// <summary>
        /// Gets or sets the person info hidden.
        /// </summary>
        public string PersonInfoHidden { get; set; }

        /// <summary>
        /// Gets or sets the team name.
        /// </summary>
        public string TeamName { get; set; }
    }
}