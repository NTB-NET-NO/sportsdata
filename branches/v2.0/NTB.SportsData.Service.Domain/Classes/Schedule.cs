﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Schedule.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   The schedule.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Domain.Classes
{
    /// <summary>
    /// The schedule.
    /// </summary>
    public class Schedule
    {
        /// <summary>
        /// Gets or sets the schedule id.
        /// </summary>
        public string ScheduleId { get; set; }

        /// <summary>
        /// Gets or sets the time.
        /// </summary>
        public string Time { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether results.
        /// </summary>
        public bool Results { get; set; }

        /// <summary>
        /// Gets or sets the offset.
        /// </summary>
        public int Offset { get; set; }

        /// <summary>
        /// Gets or sets the duration.
        /// </summary>
        public int Duration { get; set; }

        /// <summary>
        /// Gets or sets the day of week.
        /// </summary>
        public int DayOfWeek { get; set; }

        /// <summary>
        /// Gets or sets the week.
        /// </summary>
        public int Week { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether push schedule.
        /// </summary>
        public bool PushSchedule { get; set; }
    }
}