// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Municipality.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   The municipality.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Domain.Classes
{
    /// <summary>
    /// The municipality.
    /// </summary>
    public class Municipality
    {
        /// <summary>
        /// Gets or sets the municipality id.
        /// </summary>
        public int MunicipalityId { get; set; }

        /// <summary>
        /// Gets or sets the municipality name.
        /// </summary>
        public int MunicipalityName { get; set; }

        /// <summary>
        /// Gets or sets the district id.
        /// </summary>
        public int DistrictId { get; set; }

        /// <summary>
        /// Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }
    }
}