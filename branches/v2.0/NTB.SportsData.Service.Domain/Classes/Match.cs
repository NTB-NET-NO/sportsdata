// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Match.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   The match.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Domain.Classes
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The match.
    /// </summary>
    public class Match
    {
        /// <summary>
        /// Gets or sets the match id.
        /// </summary>
        public int MatchId { get; set; }

        /// <summary>
        /// Gets or sets the away team id.
        /// </summary>
        public int AwayTeamId { get; set; }

        /// <summary>
        /// Gets or sets the away team name.
        /// </summary>
        public string AwayTeamName { get; set; }

        /// <summary>
        /// Gets or sets the home team id.
        /// </summary>
        public int HomeTeamId { get; set; }

        /// <summary>
        /// Gets or sets the home team name.
        /// </summary>
        public string HomeTeamName { get; set; }

        /// <summary>
        /// Gets or sets the match start date.
        /// </summary>
        public DateTime? MatchStartDate { get; set; }

        /// <summary>
        /// Gets or sets the match end date.
        /// </summary>
        public DateTime? MatchEndDate { get; set; }

        /// <summary>
        /// Gets or sets the tournament id.
        /// </summary>
        public int TournamentId { get; set; }

        /// <summary>
        /// Gets or sets the tournament number.
        /// </summary>
        public string TournamentNumber { get; set; }

        /// <summary>
        /// Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        /// Gets or sets the tournament name.
        /// </summary>
        public string TournamentName { get; set; }

        /// <summary>
        /// Gets or sets the tournament age category id.
        /// </summary>
        public int TournamentAgeCategoryId { get; set; }

        /// <summary>
        /// Gets or sets the downloaded.
        /// </summary>
        public int Downloaded { get; set; }

        /// <summary>
        /// Gets or sets the referees.
        /// </summary>
        public Referee[] Referees { get; set; }

        /// <summary>
        /// Gets or sets the match event list.
        /// </summary>
        public List<MatchEvent> MatchEventList { get; set; }

        /// <summary>
        /// Gets or sets the stadium name.
        /// </summary>
        public string StadiumName { get; set; }

        /// <summary>
        /// Gets or sets the spectators.
        /// </summary>
        public int Spectators { get; set; }

        /// <summary>
        /// Gets or sets the tournament round number.
        /// </summary>
        public int TournamentRoundNumber { get; set; }

        /// <summary>
        /// Gets or sets the home team goals.
        /// </summary>
        public int? HomeTeamGoals { get; set; }

        /// <summary>
        /// Gets or sets the away team goals.
        /// </summary>
        public int? AwayTeamGoals { get; set; }

        /// <summary>
        /// Gets or sets the match result list.
        /// </summary>
        public MatchResult[] MatchResultList { get; set; }

        /// <summary>
        /// Gets or sets the home team players.
        /// </summary>
        public Player[] HomeTeamPlayers { get; set; }

        /// <summary>
        /// Gets or sets the away team players.
        /// </summary>
        public Player[] AwayTeamPlayers { get; set; }
    }
}