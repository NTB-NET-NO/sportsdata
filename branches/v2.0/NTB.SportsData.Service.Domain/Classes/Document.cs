// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Document.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   The document.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Domain.Classes
{
    using System;

    /// <summary>
    /// The document.
    /// </summary>
    public class Document
    {
        /// <summary>
        /// Gets or sets the document id.
        /// </summary>
        public int DocumentId { get; set; }

        /// <summary>
        /// Gets or sets the document name.
        /// </summary>
        public string DocumentName { get; set; }

        /// <summary>
        /// Gets or sets the document created date.
        /// </summary>
        public DateTime DocumentCreatedDate { get; set; }
    }
}