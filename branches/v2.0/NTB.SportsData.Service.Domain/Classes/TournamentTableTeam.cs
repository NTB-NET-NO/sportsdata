// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TournamentTableTeam.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   The tournament table team.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Domain.Classes
{
    /// <summary>
    /// The tournament table team.
    /// </summary>
    public class TournamentTableTeam
    {
        /// <summary>
        /// Gets or sets the tournament id.
        /// </summary>
        public int TournamentId { get; set; }

        /// <summary>
        /// Gets or sets the adjustments.
        /// </summary>
        public int Adjustments { get; set; }

        /// <summary>
        /// Gets or sets the away goal difference.
        /// </summary>
        public int AwayGoalDifference { get; set; }

        /// <summary>
        /// Gets or sets the away goals against.
        /// </summary>
        public int? AwayGoalsAgainst { get; set; }

        /// <summary>
        /// Gets or sets the away goals scored.
        /// </summary>
        public int? AwayGoalsScored { get; set; }

        /// <summary>
        /// Gets or sets the away matches draw.
        /// </summary>
        public int AwayMatchesDraw { get; set; }

        /// <summary>
        /// Gets or sets the away matches lost.
        /// </summary>
        public int AwayMatchesLost { get; set; }

        /// <summary>
        /// Gets or sets the away matches played.
        /// </summary>
        public int AwayMatchesPlayed { get; set; }

        /// <summary>
        /// Gets or sets the away matches won.
        /// </summary>
        public int AwayMatchesWon { get; set; }

        /// <summary>
        /// Gets or sets the away points.
        /// </summary>
        public int AwayPoints { get; set; }

        /// <summary>
        /// Gets or sets the home goal difference.
        /// </summary>
        public int HomeGoalDifference { get; set; }

        /// <summary>
        /// Gets or sets the home goals against.
        /// </summary>
        public int? HomeGoalsAgainst { get; set; }

        /// <summary>
        /// Gets or sets the home goals scored.
        /// </summary>
        public int? HomeGoalsScored { get; set; }

        /// <summary>
        /// Gets or sets the home matches draw.
        /// </summary>
        public int HomeMatchesDraw { get; set; }

        /// <summary>
        /// Gets or sets the home matches lost.
        /// </summary>
        public int HomeMatchesLost { get; set; }

        /// <summary>
        /// Gets or sets the home matches played.
        /// </summary>
        public int HomeMatchesPlayed { get; set; }

        /// <summary>
        /// Gets or sets the home matches won.
        /// </summary>
        public int HomeMatchesWon { get; set; }

        /// <summary>
        /// Gets or sets the home points.
        /// </summary>
        public int HomePoints { get; set; }

        /// <summary>
        /// Gets or sets the table position.
        /// </summary>
        public int TablePosition { get; set; }

        /// <summary>
        /// Gets or sets the team id.
        /// </summary>
        public int TeamId { get; set; }

        /// <summary>
        /// Gets or sets the team name.
        /// </summary>
        public string TeamName { get; set; }

        /// <summary>
        /// Gets or sets the team name in tournament.
        /// </summary>
        public string TeamNameInTournament { get; set; }

        /// <summary>
        /// Gets or sets the total goal difference.
        /// </summary>
        public int TotalGoalDifference { get; set; }

        /// <summary>
        /// Gets or sets the total goals against.
        /// </summary>
        public int TotalGoalsAgainst { get; set; }

        /// <summary>
        /// Gets or sets the total goals scored.
        /// </summary>
        public int TotalGoalsScored { get; set; }

        /// <summary>
        /// Gets or sets the total matches draw.
        /// </summary>
        public int TotalMatchesDraw { get; set; }

        /// <summary>
        /// Gets or sets the total matches lost.
        /// </summary>
        public int TotalMatchesLost { get; set; }

        /// <summary>
        /// Gets or sets the total matches played.
        /// </summary>
        public int TotalMatchesPlayed { get; set; }

        /// <summary>
        /// Gets or sets the total matches won.
        /// </summary>
        public int TotalMatchesWon { get; set; }

        /// <summary>
        /// Gets or sets the total points.
        /// </summary>
        public int TotalPoints { get; set; }

        /// <summary>
        /// Gets or sets the tournament team id.
        /// </summary>
        public int TournamentTeamId { get; set; }
    }
}