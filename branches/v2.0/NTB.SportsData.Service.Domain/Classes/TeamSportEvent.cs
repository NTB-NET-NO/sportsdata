// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TeamSportEvent.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   The team sport event.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Domain.Classes
{
    /// <summary>
    /// The team sport event.
    /// </summary>
    public class TeamSportEvent
    {
    }
}