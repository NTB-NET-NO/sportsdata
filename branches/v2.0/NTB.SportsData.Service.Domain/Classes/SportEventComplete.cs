﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SportEventComplete.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   The sport event complete.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Domain.Classes
{
    /// <summary>
    /// The sport event complete.
    /// </summary>
    public class SportEventComplete
    {
        /// <summary>
        /// Gets or sets the classes.
        /// </summary>
        public EventClass Classes { get; set; }

        /// <summary>
        /// Gets or sets the entries.
        /// </summary>
        public EntrySimple Entries { get; set; }

        /// <summary>
        /// Gets or sets the event.
        /// </summary>
        public SportEvent Event { get; set; }
    }
}