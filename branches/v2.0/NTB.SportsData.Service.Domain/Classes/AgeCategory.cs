// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AgeCategory.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   The age category.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Domain.Classes
{
    /// <summary>
    /// The age category.
    /// </summary>
    public class AgeCategory
    {
        /// <summary>
        /// Gets or sets the age category id.
        /// </summary>
        public int AgeCategoryId { get; set; }

        /// <summary>
        /// Gets or sets the age category name.
        /// </summary>
        public string AgeCategoryName { get; set; }

        /// <summary>
        /// Gets or sets the min age.
        /// </summary>
        public int MinAge { get; set; }

        /// <summary>
        /// Gets or sets the max age.
        /// </summary>
        public int MaxAge { get; set; }

        /// <summary>
        /// Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }
    }
}