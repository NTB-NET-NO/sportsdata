// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Referee.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   The referee.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Domain.Classes
{
    /// <summary>
    /// The referee.
    /// </summary>
    public class Referee
    {
        /// <summary>
        /// Gets or sets the referee id.
        /// </summary>
        public int RefereeId { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the sur name.
        /// </summary>
        public string SurName { get; set; }

        /// <summary>
        /// Gets or sets the referee type.
        /// </summary>
        public string RefereeType { get; set; }

        /// <summary>
        /// Gets or sets the referee club.
        /// </summary>
        public string RefereeClub { get; set; }
    }
}