// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Club.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   The club.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Domain.Classes
{
    /// <summary>
    /// The club.
    /// </summary>
    public class Club
    {
        /// <summary>
        /// Gets or sets the club id.
        /// </summary>
        public int ClubId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }
    }
}