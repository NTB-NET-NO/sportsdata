// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Season.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   The season.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Domain.Classes
{
    using System;

    /// <summary>
    /// The season.
    /// </summary>
    public class Season
    {
        /// <summary>
        /// Gets or sets the season id.
        /// </summary>
        public int SeasonId { get; set; }

        /// <summary>
        /// Gets or sets the season name.
        /// </summary>
        public int SeasonName { get; set; }

        /// <summary>
        /// Gets or sets the season start date.
        /// </summary>
        public DateTime SeasonStartDate { get; set; }

        /// <summary>
        /// Gets or sets the season end date.
        /// </summary>
        public DateTime SeasonEndDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether season active.
        /// </summary>
        public bool SeasonActive { get; set; }
    }
}