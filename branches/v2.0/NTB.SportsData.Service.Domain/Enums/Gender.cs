﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Gender.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   The gender.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Domain.Enums
{
    /// <summary>
    /// The gender.
    /// </summary>
    public enum Gender
    {
        /// <summary>
        /// The male.
        /// </summary>
        Male = 1, 

        /// <summary>
        /// The female.
        /// </summary>
        Female = 2, 

        /// <summary>
        /// The all.
        /// </summary>
        All = 3, 

        /// <summary>
        /// The unknown.
        /// </summary>
        Unknown = 5
    }
}