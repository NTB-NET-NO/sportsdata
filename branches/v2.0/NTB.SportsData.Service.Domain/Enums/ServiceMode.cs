// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceMode.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS
// </copyright>
// <summary>
//   Used to tell if the service component is running or has signaled that everything has to stop
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.SportsData.Service.Domain.Enums
{
    /// <summary>
    /// Used to tell if the service component is running or has signaled that everything has to stop
    /// </summary>
    public enum ServiceMode
    {
        /// <summary>
        /// Running mode is when everything is OK
        /// </summary>
        Running, 

        /// <summary>
        /// If the program is signaling halting, the service shall stop. 
        /// </summary>
        Halting
    }
}