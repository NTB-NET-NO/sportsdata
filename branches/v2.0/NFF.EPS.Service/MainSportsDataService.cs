﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainSportsDataService.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The main ntb sports data service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using NTB.SportsData.Service.Components;

namespace NTB.SportsData.Service
{
    using System;
    using System.ServiceProcess;
    using System.Timers;

    // Adding Log4Net - logging support
    using log4net;

    // Adding reference to components

    /// <summary>
    /// The main ntb sports data service.
    /// </summary>
    public partial class MainNtbSportsDataService : ServiceBase
    {
        /// <summary>
        /// The service.
        /// </summary>
        protected MainServiceComponent Service = null;

        /// <summary>
        /// The _logger.
        /// </summary>
        private static ILog logger = LogManager.GetLogger(typeof(MainNtbSportsDataService));

        /// <summary>
        /// Initializes a new instance of the <see cref="MainNtbSportsDataService"/> class.
        /// </summary>
        public MainNtbSportsDataService()
        {
            // log4net.Config.XmlConfigurator.Configure();
            // MDC.Set("JOBNAME", "NTB.SportsData.Service");
            ThreadContext.Properties["JOBNAME"] = "NTB.SportsData.Service";
            log4net.Config.XmlConfigurator.Configure();

            logger = LogManager.GetLogger(typeof(MainNtbSportsDataService));

            logger.Info("In MainNTBSportsDataService - starting up");

            // Creating the new MainServiceComponent
            Service = new MainServiceComponent();

            // Initialising this main component
            InitializeComponent();
        }

        /// <summary>
        /// The on start.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        protected override void OnStart(string[] args)
        {
            try
            {
                logger.Info("NTB SportsDataService starting...");
                _startupConfigTimer.Start();
            }
            catch (Exception ex)
            {
                logger.Fatal("NTBSportsDataService DID NOT START properly - Terminating!", ex);
                throw;
            }
        }

        /// <summary>
        /// The on stop.
        /// </summary>
        protected override void OnStop()
        {
            try
            {
                logger.Info("Stopping service");
                Service.Stop();

                _startupConfigTimer.Enabled = false;
            }
            catch (Exception ex)
            {
                logger.Fatal("NTB SporstDataService did not stop properly - Terminating!", ex);
                throw;
            }
        }

        /// <summary>
        /// The service timer_ elapsed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ServiceTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _startupConfigTimer.Stop();

            try
            {
                Service.Configure();
                Service.Start();
                logger.Info("MainSportsDataService Started!");
            }
            catch (Exception ex)
            {
                logger.Fatal("MainSportsDataService DID NOT START properly - TERMINATING!", ex);

                throw;
            }
        }
    }
}