﻿namespace NTB.SportsData.Service
{
    partial class MainNtbSportsDataService
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._startupConfigTimer = new System.Timers.Timer();
            this.ServiceEventLog = new System.Diagnostics.EventLog();
            ((System.ComponentModel.ISupportInitialize)(this._startupConfigTimer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ServiceEventLog)).BeginInit();
            // 
            // _startupConfigTimer
            // 
            this._startupConfigTimer.Interval = 2000D;
            this._startupConfigTimer.Elapsed += new System.Timers.ElapsedEventHandler(this.ServiceTimer_Elapsed);
            // 
            // MainNTBSportsDataService
            // 
            this.ServiceName = "Service1";
            ((System.ComponentModel.ISupportInitialize)(this._startupConfigTimer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ServiceEventLog)).EndInit();

        }

        #endregion

        private System.Timers.Timer _startupConfigTimer;
        private System.Diagnostics.EventLog ServiceEventLog;

    }
}
