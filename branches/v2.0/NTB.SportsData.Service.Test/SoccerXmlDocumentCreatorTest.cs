﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using System;
using System.Collections.Generic;
using NTB.SportsData.Service.Components.Nff;

namespace NTB.SportsData.Service.Test
{
    
    
    /// <summary>
    ///This is a test class for SoccerXmlDocumentCreatorTest and is intended
    ///to contain all SoccerXmlDocumentCreatorTest Unit Tests
    ///</summary>
    [TestClass]
    public class SoccerXmlDocumentCreatorTest
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for CreateDistrictElement
        ///</summary>
        [TestMethod]
        public void CreateDistrictElementTest()
        {
            SoccerXmlDocumentCreator target = new SoccerXmlDocumentCreator();
            District district = new District
                {
                    DistrictId = 1,
                    DistrictName = "Norges Fotballforbund",
                    SportId = 1
                }; 
            
            XmlDocument doc = target.CreateDistrictElement(district);
            Assert.IsTrue(doc.DocumentElement != null && doc.DocumentElement.Name == "District");
        }

        /// <summary>
        ///A test for CreateDistrictElement
        ///</summary>
        [TestMethod]
        public void CheckCorrectDistrictName()
        {
            SoccerXmlDocumentCreator target = new SoccerXmlDocumentCreator();
            District district = new District
            {
                DistrictId = 1,
                DistrictName = "Norges Fotballforbund",
                SportId = 1
            };
            
            XmlDocument doc = target.CreateDistrictElement(district);
            const string path = "//District/@Name";
            const string expected = "Norges Fotballforbund";

            var selectSingleNode = doc.SelectSingleNode(path);
            string result = string.Empty;
            if (selectSingleNode != null)
            {
                result = selectSingleNode.Value;
            }
            Assert.IsTrue(doc.SelectSingleNode(path) != null);

            Assert.AreEqual(result, expected);


        }

        /// <summary>
        ///A test for CreateCustomerStructure
        ///</summary>
        [TestMethod]
        public void Test_Customers_CheckCorrectness()
        {
            SoccerXmlDocumentCreator target = new SoccerXmlDocumentCreator();
            List<Customer> customers = new List<Customer>();
            Customer customer = new Customer
            {
                Id = 1,
                Name = "NTB",
                RemoteCustomerId = 1234
            };
            customers.Add(customer);

            XmlDocument doc = target.CreateCustomerStructure(customers);

            const string path = "//Customers/Customer/CustomerName";
            
            Assert.IsTrue(doc.SelectSingleNode(path) != null);

            
        }


        /// <summary>
        ///A test for CreateCustomerStructure
        ///</summary>
        [TestMethod]
        public void CreateCustomerStructureTest()
        {
            SoccerXmlDocumentCreator target = new SoccerXmlDocumentCreator(); 
            List<Customer> customers = new List<Customer>();
            Customer customer = new Customer
                {
                    Id = 1,
                    Name = "NTB",
                    RemoteCustomerId = 1234
                };
            customers.Add(customer);

            XmlDocument doc = target.CreateCustomerStructure(customers);

            const string path = "//Customers/Customer/CustomerName";
            const string expected = "NTB";

            var selectSingleNode = doc.SelectSingleNode(path);
            string result = string.Empty;
            if (selectSingleNode != null)
            {
                result = selectSingleNode.InnerText;
            }
            Assert.IsTrue(doc.SelectSingleNode(path) != null);

            Assert.AreEqual(expected, result);
        }
    }
}
