﻿using NTB.SportsData.Service.Services.NFF.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NTB.SportsData.Service.Services.NFF.NFFProdService;
using System.Collections.Generic;

namespace NTB.SportsData.Service.Test
{
    
    
    /// <summary>
    ///This is a test class for TournamentRepositoryTest and is intended
    ///to contain all TournamentRepositoryTest Unit Tests
    ///</summary>
    [TestClass()]
    public class TournamentRepositoryTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetTournamentStanding
        ///</summary>
        [TestMethod()]
        public void GetTournamentStandingTest()
        {
            TournamentRepository target = new TournamentRepository(); // TODO: Initialize to an appropriate value
            const int tournamentId = 134365; // TODO: Initialize to an appropriate value
            List<TournamentTableTeam> expected = null; // TODO: Initialize to an appropriate value
            List<TournamentTableTeam> actual = target.GetTournamentStanding(tournamentId);
            Assert.AreEqual(expected, actual);
        }
    }
}
