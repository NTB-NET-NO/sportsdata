﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using NTB.SportsData.Service.Services.NIF.Interfaces;
using NTB.SportsData.Service.Services.NIF.NIFProdServices;
using log4net;

namespace NTB.SportsData.Service.Services.NIF.Repositories
{
    public class TournamentRepository :IRepository<SeasonTournament>, IDisposable, ITournamentDataMapper
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(OrganizationRepository));

        private readonly TournamentServiceClient _serviceClient = new TournamentServiceClient();

        private readonly RegionServiceClient _regionServiceClient = new RegionServiceClient();

        public TournamentRepository()
        {
            if (_serviceClient.ClientCredentials != null)
            {
                _serviceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                _serviceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];
            }

            if (_regionServiceClient.ClientCredentials != null)
            {
                _regionServiceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                _regionServiceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];
            }

            // Set up logger
            log4net.Config.XmlConfigurator.Configure();

            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }
        public int InsertOne(SeasonTournament domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<SeasonTournament> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(SeasonTournament domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(SeasonTournament domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<SeasonTournament> GetAll()
        {
            throw new NotImplementedException();
        }

        public SeasonTournament Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<Tournament> GetTournamentsByDistrict(int districtId, int seasonId)
        {
            var regionRequest = new RegionRequest
                {
                    ParentId = districtId
                };

            var regionResponse = _regionServiceClient.GetLocalCouncilByCountyId(regionRequest);

            string localCouncilIds = string.Empty;
            if (regionResponse.Success)
            {
                localCouncilIds = string.Join(",", regionResponse.Regions.ToList());
            }
            var searchParams = new TournamentRegionsSearchParams
                {
                    SeasonId = seasonId,
                    LocalCouncilIds = localCouncilIds
                };
            var request = new TournamentRegionsSearchRequest(searchParams);
            var response = _serviceClient.SearchTournamentByRegions(request);

            if (response.Success)
            {
                return response.Tournaments.ToList();
            }

            return null;
        }

        public List<SeasonTournament> GetTournamentsBySeasonId(int seasonId)
        {
            var request = new SeasonIdRequest2
                {
                    SeasonId = seasonId
                };

            var response = _serviceClient.GetSeasonTournaments(request);

            if (response.Success)
            {
                return response.SeasonTournament.ToList();
            }

            return new List<SeasonTournament>();
        }

        public List<Tournament> GetTournamentByDistrict(int districtId, int seasonId)
        {
            throw new NotImplementedException();
        }
    }
}
