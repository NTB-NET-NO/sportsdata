﻿using System;
using System.Collections.Generic;
using System.Linq;
using NTB.SportsData.Service.Services.NIF.Interfaces;
using NTB.SportsData.Service.Services.NIF.NIFProdServices;

using log4net;

namespace NTB.SportsData.Service.Services.NIF.Repositories
{
    public class ActivityRepository : IRepository<Activity>, IDisposable, IActivityDataMapper
    {
        private readonly ActivityServiceClient _serviceClient = new ActivityServiceClient();

        private static readonly ILog Logger = LogManager.GetLogger(typeof(ActivityRepository));

        public ActivityRepository()
        {
            if (_serviceClient.ClientCredentials != null)
            {
                _serviceClient.ClientCredentials.UserName.UserName = "";
                _serviceClient.ClientCredentials.UserName.Password = "";
            }

            // Set up logger
            log4net.Config.XmlConfigurator.Configure();

            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        public int InsertOne(Activity domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Activity> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Activity domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Activity domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Activity> GetAll()
        {
            throw new NotImplementedException();
        }

        public Activity Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Activity GetActivityByOrgId(int orgId, int sportId)
        {
            ActivitiesByOrgIdRequest request = new ActivitiesByOrgIdRequest
            {
                Id = orgId
            };

            var response = _serviceClient.GetActivitiesByOrg(request);

            if (!response.Success)
            {
                return null;
            }

            Activity[] activities = response.Activities;

            Activity activity = (from a in activities where a.OrgIdOwner == sportId select a).Single();

            return activity;
        }

        public List<Activity> GetAllFirstLevelActivities(int parentActivityId)
        {
            var request = new EmptyRequest14();
            
            var response = _serviceClient.GetAllFirstLevelActivities(request);

            if (response.Success)
            {
                var activities = (from r in response.Activities
                           where r.ParentActivityId == parentActivityId
                               select r).ToList();
                return activities;
            }

            return null;
        }
    }
}
