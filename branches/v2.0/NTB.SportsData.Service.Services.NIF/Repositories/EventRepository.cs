﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using NTB.SportsData.Service.Services.NIF.Interfaces;
using NTB.SportsData.Service.Services.NIF.NIFProdServices;

using log4net;

namespace NTB.SportsData.Service.Services.NIF.Repositories
{
    public class EventRepository : IRepository<EventComplete>, IDisposable, IEventDataMapper
    {
        private readonly EventServiceClient _serviceClient = new EventServiceClient();
        
        public EventRepository()
        {
            if (_serviceClient.ClientCredentials != null)
            {
                _serviceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                _serviceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];
            }


            // Set up logger
            log4net.Config.XmlConfigurator.Configure();

            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }
        public int InsertOne(EventComplete domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<EventComplete> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(EventComplete domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(EventComplete domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<EventComplete> GetAll()
        {
            throw new NotImplementedException();
        }

        public EventComplete Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public EventComplete GetSingleEventComplete(int eventId)
        {
            EventRequest eventRequest = new EventRequest
                {
                    Id = eventId
                };
            
            EventCompleteResponse eventResponse = _serviceClient.GetSingleEventComplete(eventRequest);

            if (eventResponse.Success)
            {
                return eventResponse.Event;
            }

            return null;
        }

        public List<Event> GetEventsBySportId(int sportId)
        {
            // Then we shall only create event list and store this in the database
            EventSearchRequest searchRequest = new EventSearchRequest
            {
                ActivityId = sportId
            };

            EventSearchResponse searchResponse = _serviceClient.SearchEvents(searchRequest);

            var events = new List<Event>();
            if (searchResponse.Success)
            {
                foreach (EventSearchResult eventSearchResult in searchResponse.Events)
                {
                    var sportEvent = new Event
                        {
                            ActivityId = eventSearchResult.ActivityId,
                            ActivityName = eventSearchResult.ActivityName,
                            ArrangingOrgName = eventSearchResult.ArrangingOrgName,
                            StartDate = eventSearchResult.StartDate,
                            EndDate = eventSearchResult.EndDate,
                            Location = eventSearchResult.Location
                        };

                    events.Add(sportEvent);
                }
                
            }

            return events;
        }

        public Event GetEventByEventId(int eventId)
        {
            // Then we shall only create event list and store this in the database
            EventRequest request = new EventRequest
                {
                    Id = eventId
                };

            var response = _serviceClient.GetSingleEvent(request);

            if (response.Success)
            {
                return response.Event;
            }

            return null;
        }
    }
}