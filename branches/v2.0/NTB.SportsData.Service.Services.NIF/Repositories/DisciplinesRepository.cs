﻿using System.Configuration;
using System.Linq;
using NTB.SportsData.Service.Services.NIF.NIFProdServices;
using log4net;

namespace NTB.SportsData.Service.Services.NIF.Repositories
{
    public class DisciplinesRepository
    {
        private readonly OrgServiceClient _serviceClient = new OrgServiceClient();
        public DisciplinesRepository()
        {
            if (_serviceClient.ClientCredentials != null)
            {
                _serviceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                _serviceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];
            }


            // Set up logger
            log4net.Config.XmlConfigurator.Configure();

            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        public string GetDisciplines(int federationId)
        {
            var request = new EmptyRequest();
            FederationResponse response = _serviceClient.GetFederations(request);
            if (!response.Success)
            {
                return string.Empty;
            }

            Federation federation =
                response.Federations.ToList().Single(filteredFederations => filteredFederations.OrgId == federationId);

            return federation.Disciplines;
        }
    }
}
