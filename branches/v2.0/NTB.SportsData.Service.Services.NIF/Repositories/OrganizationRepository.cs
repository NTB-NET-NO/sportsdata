﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using NTB.SportsData.Service.Domain.Classes;
using NTB.SportsData.Service.Services.NIF.Interfaces;
using NTB.SportsData.Service.Services.NIF.NIFProdServices;
using log4net;

namespace NTB.SportsData.Service.Services.NIF.Repositories
{
    public class OrganizationRepository : IRepository<Organization>, IDisposable, IOrganizationDataMapper
    {
        private readonly OrgServiceClient _serviceClient = new OrgServiceClient();

        private static readonly ILog Logger = LogManager.GetLogger(typeof(OrganizationRepository));

        public OrganizationRepository()
        {
            if (_serviceClient.ClientCredentials != null)
            {
                _serviceClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["NIFServiceUsername"];
                _serviceClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["NIFServicePassword"];
            }

            // Set up logger
            log4net.Config.XmlConfigurator.Configure();

            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        public int InsertOne(Organization domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Organization> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Organization domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Organization domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Organization> GetAll()
        {
            throw new NotImplementedException();
        }

        public Organization Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<Org> GetOrganizationsByOrgId(int orgId)
        {
            int[] orgIds = new[] {orgId};
            var request = new OrgIdsRequest
                {
                    Ids = orgIds
                };

            var response = _serviceClient.GetOrganisations(request);

            if (response.Success)
            {
                return response.Orgs.ToList();
            }

            return null;
        }

        public Org GetOrganizationByOrgId(int orgId)
        {
            return GetOrganizationsByOrgId(orgId).Single(x => x.Id == orgId);
        }
    }
}
