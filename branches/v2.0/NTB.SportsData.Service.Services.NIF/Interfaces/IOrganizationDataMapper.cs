﻿using System.Collections.Generic;
using NTB.SportsData.Service.Services.NIF.NIFProdServices;

namespace NTB.SportsData.Service.Services.NIF.Interfaces
{
    public interface IOrganizationDataMapper
    {
        List<Org> GetOrganizationsByOrgId(int orgId);
        Org GetOrganizationByOrgId(int orgId);
    }
}
