﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsData.Service.Services.NIF.NIFProdServices;

namespace NTB.SportsData.Service.Services.NIF.Interfaces
{
    public interface IActivityDataMapper
    {
        Activity GetActivityByOrgId(int orgId, int sportId);
        List<Activity> GetAllFirstLevelActivities(int parentActivityId);
    }
}
