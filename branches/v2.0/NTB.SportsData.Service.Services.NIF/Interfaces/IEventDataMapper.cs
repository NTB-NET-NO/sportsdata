﻿using System.Collections.Generic;
using NTB.SportsData.Service.Services.NIF.NIFProdServices;

namespace NTB.SportsData.Service.Services.NIF.Interfaces
{
    public interface IEventDataMapper
    {
        EventComplete GetSingleEventComplete(int eventId);
        List<Event> GetEventsBySportId(int sportId);
        Event GetEventByEventId(int eventId);
    }
}
