﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.SportsData.Service.Services.NIF.NIFProdServices;

namespace NTB.SportsData.Service.Services.NIF.Interfaces
{
    public interface ITournamentDataMapper
    {
        List<SeasonTournament> GetTournamentsBySeasonId(int seasonId);
        List<Tournament> GetTournamentByDistrict(int districtId, int seasonId);
    }
}
